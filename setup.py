#!/usr/bin/env python

from distutils.core import setup

setup(name='wild',
      version='1.0',
      description='Libraries for general purpose',
      author='Ignacio Ibarra',
      author_email='ibarra.ignacio@gmail.com',
      url='',
      packages=['lib']
     )