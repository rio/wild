'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.TFBinding.TFBindingModel import TFBindingModel

class ModelGroup():


    def __init__(self, models_list, labels):

        self.models = models_list
        self.labels = labels


    @staticmethod
    def get_model_group(seqs, y, queries=['1mer', '1mer+2mer', '1mer+2mer+3mer']):

        model_codes = {'1mer': TFBindingModel.get_1mer_model,
                       '1mer+2mer': TFBindingModel.get_1mer_2mer_model,
                       '1mer+2mer+3mer': TFBindingModel.get_1mer_2mer_model,
                       }
        models = []
        for k in queries:
            models.append(model_codes[k](seqs, y)),
        labels = queries # '1mer+2mer+3mer+shape']
        return ModelGroup(models, labels)

    def predict(self, seqs):
        scores = pd.DataFrame()
        scores['sequence'] = [seqs] if not isinstance(seqs, list) else seqs

        print scores
        for model, lab in zip(self.models, self.labels):
            print 'scoring with model', lab
            next = model.predict(seqs)
            print 'scores', next
            scores[lab] = [next] if isinstance(next, str) else next
            # print scores
        print scores.shape
        return scores

