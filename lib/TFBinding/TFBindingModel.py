'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.ShapeAnalyzer import ShapeAnalyzer
from lib.SequenceMethods import SequenceMethods
from lib.MachineLearning import MachineLearning
from sklearn.linear_model import Ridge
from lib.SELEX.SELEXAnalyzer import SELEXAnalyzer

class TFBindingModel():
    def __init__(self, X, y, parser_function=None, **kwargs):
        y = np.array(y)
        gbr = Ridge(random_state=kwargs.get('random_state', 500))
        self.model = gbr.fit(X, y)
        assert parser_function is not None
        self.parser_function = parser_function

    @staticmethod
    def get_1mer_model(seqs, y, **kwargs):
        X = np.array(ShapeAnalyzer.get_X_1mer(seqs))
        return TFBindingModel(X, y, parser_function=ShapeAnalyzer.get_X_1mer, **kwargs)

    @staticmethod
    def get_1mer_2mer_model(seqs, y, **kwargs):
        X = ShapeAnalyzer.get_X_1mer_2mer(seqs)
        return TFBindingModel(X, y, parser_function=ShapeAnalyzer.get_X_1mer_2mer, **kwargs)

    @staticmethod
    def get_1mer_2mer_3mer_model(seqs, y, **kwargs):
        X = ShapeAnalyzer.get_X_1mer_2mer_3mer(seqs)
        return TFBindingModel(X, y, parser_function=ShapeAnalyzer.get_X_1mer_2mer_3mer, **kwargs)

    @staticmethod
    def get_1mer_n_shape_model(seqs, y, **kwargs):
        X = ShapeAnalyzer.get_X_1mer_shape_flanks(seqs)
        return TFBindingModel(X, y, parser_function=ShapeAnalyzer.get_X_1mer_shape_flanks, **kwargs)

    @staticmethod
    def get_kmer_matches(df, pattern):
        len_kmer_table = len(list(df['seq'])[0])
        if len(pattern) < len_kmer_table:
            new_pattern = None
            for seq in df['seq']:
                matches = SequenceMethods.get_matches(pattern, seq)
                matches = matches[matches['strand'] == '+'].sort_values('n.matches', ascending=False).head(1)
                best_match = list(matches['n.matches'])[0]
                best_position = list(matches['position'])[0]
                print best_position, best_match
                if best_position == len(matches):
                    print matches
                    len_diff = len_kmer_table - len(pattern)
                    new_pattern = ("N" * best_position) + pattern + ('N' * (len_diff - best_position))
                    print new_pattern
                if new_pattern is not None:
                    break
            assert new_pattern is not None
            pattern = new_pattern
            print 'new pattern updated to', new_pattern
        return df['seq'].apply(SequenceMethods.get_matches, args=[pattern]), pattern

    def predict(self, seqs):
        if isinstance(seqs, list):
            return self.model.predict(self.parser_function(seqs))
        return self.model.predict(self.parser_function([seqs]))[0]

    @staticmethod
    def get_mismatches_thr(motif):
        selex = SELEXAnalyzer()
        score1 = selex.get_allowed_mismatches_threshold(motif)
        score1 = max(score1 - 1, 0)
        thr1 = max(score1, 0) + 1
        thr2 = thr1 - 1
        return thr1, thr2

    def get_r2_score(self, seqs, y):
        return MachineLearning.get_r2_score(self.parser_function(seqs), np.array(y), self.model)

    def get_kfold_cv(self, seqs, y, **kwargs):
        X = np.array(self.parser_function(seqs))
        y = np.array(y)
        return MachineLearning.KFoldRidgeRegression(X, y)



