

import pandas as pd
from os.path import basename, join, exists
from os import system

class ENADownloader():
    def download(self, ena_bulk_table_path, output_dir, keywords=None,
                 startswith=False):
        '''
        Given a set of files (ENA entry files), download them individually into an output directory
        :param ena_bulk_table_path:
        :param output_dir:
        :return:
        '''
        df = pd.DataFrame.from_csv(ena_bulk_table_path, sep='\t', index_col=False)
        df['fastq_ftp'] = [f for f in df['submitted_ftp']]
        # print df.columns
        df = df[['study_accession', 'sample_accession', 'experiment_accession', 'fastq_ftp']]

        # df = df[df['fastq_ftp'].str.contains('ZeroCycle')]
        df['filename'] = [basename(f) for f in df['fastq_ftp']]
        df = df.reset_index(drop=True)

        # download files
        counter = 0
        missing_files = 0
        for i, r in df.iterrows(): # this is a for loop
            p = r['fastq_ftp']
            output_path = join(output_dir, basename(p))
            if keywords is not None:
                accept = False
                for k in keywords:
                    if not startswith:
                        if k in p:
                            accept = True
                    else:
                        code = p.split("/")[-1]
                        if code.startswith(k):
                            # print code, k
                            accept = True
                if not accept:
                    continue
                else:
                    counter += 1

            if not exists(output_path):
                missing_files += 1
            if not exists(output_path):
                cmd1 = 'wget -O ' + output_path + " " + p
                print(cmd1)
                system(cmd1)

        print("# total files", counter)
        print("# missing files", missing_files)