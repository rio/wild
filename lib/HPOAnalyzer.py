'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *


class HPOAnalyzer:

    @staticmethod
    def get_hpo_phen_to_gene_table():
        # What is the overlap between terms and Genes?
        hpo_genes = DataFrameAnalyzer.read_tsv('/g/scb2/zaugg/rio/data/hpo/ALL_SOURCES_ALL_FREQUENCIES_phenotype_to_genes.txt',
                                               skiprows=1, columns=['hpo.id', 'hpo.name', 'gene.id', 'gene.name'])
        return hpo_genes

    @staticmethod
    def get_hpo_phen_to_gene_dict():
        hpo_genes = DataFrameAnalyzer.read_tsv(
            '/g/scb2/zaugg/rio/data/hpo/ALL_SOURCES_ALL_FREQUENCIES_phenotype_to_genes.txt',
            skiprows=1, columns=['hpo.id', 'hpo.name', 'gene.id', 'gene.name'])
        return {k: set(grp['gene.name']) for k, grp in hpo_genes.groupby('hpo.id')}