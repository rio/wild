'''
Created on 

DESCRIPTION

@author: ignacio
'''
# from lib.utils import *
from IPython.core.magic import (register_line_magic)
from IPython.terminal.magics import Magics, magics_class, line_magic

try:
    __IPYTHON__
    @magics_class
    class MyMagics(Magics):
        "Magics that hold additional state"

        def __init__(self, shell):
            # You must call the parent constructor
            super(MyMagics, self).__init__(shell)

        @register_line_magic
        def p(self):
            ipython.magic("%paste")


    from IPython import get_ipython
    ipython = get_ipython()
    magics = MyMagics(ipython)
    ipython.register_magics(MyMagics)
except:
    print('Not importing magic statements...')