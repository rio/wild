

from lib.utils import *

import rpy2
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
from rpy2.robjects.vectors import FloatVector, StrVector
pandas2ri.activate() # to convert pandas to R dataframe

class RFacade:
    @staticmethod
    def get_bh_pvalues(pvals):
        """
        Return a list with BH-corrected p-values
        """
        # r-libraries
        from rpy2.robjects.packages import importr
        from rpy2.robjects.vectors import FloatVector

        stats = importr('stats')
        p_adjust = stats.p_adjust(FloatVector(pvals), method = 'BH')
        return p_adjust

    @staticmethod
    def plot_sankey(df, output_path):
        src_path = "/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/make_sankey_plot.R"
        r = rpy2.robjects.r
        r.source(src_path)
        r['plot_sankey_plot'](df, output_path)

    @staticmethod
    def get_wilcox_test_pval(pos, neg, alternative='g'):
        # return train and test element
        # print 'calculating R^2'
        r = rpy2.robjects.r
        pval = r['wilcox.test'](FloatVector(pos), FloatVector(neg),
                                alternative=alternative)[2][0]
        return pval

    @staticmethod
    def get_wilcox_effect_size(pos, neg, alternative='g', **kwargs):
        r = rpy2.robjects.r
        # print 'importing function'
        src_path = "/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/wilcoxon_effect_size.R"
        r.source(src_path)
        pval = r['rFromWilcox'](FloatVector(pos), FloatVector(neg), alternative=alternative)

    @staticmethod
    def get_mcfadden_r2(X, y):

        r = rpy2.robjects.r
        X['y'] = y
        target = str(X.columns[-1])
        r_glm = r['glm']('%s ~ .' % target, data = X, family = 'binomial')

        from rpy2.robjects.packages import importr
        stats = importr('DescTools')

        print(stats.PseudoR2(r.glm)[0])



    @staticmethod
    def get_rds(rds_path):
        readRDS = robjects.r['readRDS']
        df = readRDS(rds_path)
        df = pandas2ri.ri2py(df)
        return df
        # do something with the dataframe

    @staticmethod
    def get_cor_test_pval(x, y, alternative='two.sided'):
        # return train and test element
        # print 'calculating R^2'
        r = rpy2.robjects.r
        return r['cor.test'](FloatVector(x), FloatVector(y), alternative=alternative)[2][0]

    @staticmethod
    def load_rda_from_path(rda_path):
        from rpy2.robjects.packages import importr
        base = importr('base')
        base.load(rda_path)
        rdf_List = base.mget(base.ls())
        # ITERATE THROUGH LIST OF R DFs
        pydf_dict = {}
        for i, f in enumerate(base.names(rdf_List)):
            pydf_dict[f] = pandas2ri.ri2py_dataframe(rdf_List[i])
        print('# of dataframes', len(list(pydf_dict.items())))
        for k, v in list(pydf_dict.items()):
            continue
            print((v.head()))

        return v

    @staticmethod
    def get_gsea_result(df, species=10090, plot=False, output_path=None, **kwargs):
        r = rpy2.robjects.r
        # print 'importing function'
        src_path = "/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/fgsea.R"
        r.source(src_path)
        input_df = rpy2.robjects.pandas2ri.py2ri(df)
        res = r['get_fgsea'](input_df, **kwargs)

        if(plot):
            print('plotting...')
            assert output_path is not None
            r['plot_fgsea'](input_df, output_path=output_path, **kwargs)
        return pandas2ri.ri2py(res)

    @staticmethod
    def get_stringdb_enrichments(names, species=10090, **kwargs):
        r = rpy2.robjects.r
        # print 'importing function'
        src_path = "/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/STRINGdb.R"
        r.source(src_path)
        res = r['get_string_enrichments'](rpy2.robjects.vectors.StrVector(list(names)),
                                          species=10090, **kwargs)
        return pandas2ri.ri2py(res)

    @staticmethod
    def get_pval_asterisks(pvals, vert=False,
                           symbols=["****", "***", "**", '*'],
                           thresholds=[.0001, .001, .01, .05]):
        if vert:
            return ["\n".join(symbols[0]) if pval <= thresholds[0] else
                    ("\n".join(symbols[1]) if pval < thresholds[1] else
                     ("\n".join(symbols[2]) if pval < thresholds[2] else
                      "\n".join(symbols[3]) if pval < thresholds[3] else ""))
                    for pval in pvals]

        return [symbols[0] if pval <= thresholds[0] else
                (symbols[1]if pval < thresholds[1] else
                 (symbols[2] if pval < thresholds[2] else
                  symbols[3] if pval < thresholds[3] else ""))
                for pval in pvals]

    @staticmethod
    def get_prop_test_pval(na, nb, alternative='two.sided'):
        r = rpy2.robjects.r
        pval = r['prop.test'](na, na + nb,
                              alternative=alternative)[2][0]
        return pval

    @staticmethod
    def get_binom_test_pval(na, nb, alternative='two.sided'):
        r = rpy2.robjects.r
        pval = r['binom.test'](na, na + nb,
                              alternative=alternative)[2][0]
        return pval


    @staticmethod
    def get_gof_pvalue(frequencies_int, method='poisson'):
        r = rpy2.robjects.r
        src_path = join(os. path. dirname(__file__), "rscripts", 'goodfit.R')
        r.source(src_path)
        return r['get_gof_pvalue'](rpy2.robjects.vectors.IntVector(frequencies_int), type=method)[0]


    @staticmethod
    def plot_scatter_ggrepel(df, output_path=None, xlab='x', ylab='y',
                             w=3, h=3, title='title', xmin=None, xmax=None, ymin=None, ymax=None,
                             scale_size=True, coord_fixed=False, ggrepel_fontsize=4,
                             breaks_color_range=[-3, 3], add_sizes_bar=False,
                             size_lab='size', color_text_values=None, log=True,
                             color_lab='color_lab', segment_size=0.5, box_padding=0.2,
                             size_breaks=[1, 2, 5, 10, 20], yticks=None, xticks=None,
                             size_breaks_labs=['1', '2', '5', '10', '>=20'],
                             rscripts_path=None,
                             **kwargs):
        r = rpy2.robjects.r
        # print 'importing function'
        src_path = "/g/scb2/zaugg/rio/workspace/wild/lib/rscripts/plot_scatter_ggrepel.R"
        if not exists(src_path):
            src_path = "/mnt/c/Users/Ignacio/Dropbox/workspace/wild/lib/rscripts/plot_scatter_ggrepel.R"
            if not exists(src_path):
                src_path = join(rscripts_path, 'plot_scatter_ggrepel.R')
        assert exists(src_path)

        r.source(src_path)

        if not 'size' in df:
            df['size'] = kwargs.get('size_default', 1.0)
        if not 'color' in df:
            df['color'] = 'blue'
        if not 'shape' in df:
            df['shape'] = 'a'
        if not 'label' in df:
            df['label'] = ''
        if not 'text' in df:
            df['text'] = ''
        if not 'fill' in df:
            df['fill'] = 'fill'

        if xmin is None or xmax is None:
            xmin, xmax = int(min(df['x']) - 1), int(max(df['x']) + 1)
        if ymin is None or ymax is None:
            ymin, ymax = int(min(df['y']) - 1), int(max(df['y']) + 1)


        size_breaks = rpy2.robjects.vectors.FloatVector(size_breaks) if size_breaks is not None else None
        print(size_breaks)
        if size_breaks_labs is None and size_breaks is not None:
            size_breaks_labs = [str(vi) for vi in size_breaks]
            size_breaks_labs[-1] = ">=" + size_breaks_labs[-1]

        size_breaks_labs = rpy2.robjects.vectors.StrVector(size_breaks_labs) if size_breaks_labs is not None else None


        if breaks_color_range is not None:
            breaks_color_range = rpy2.robjects.vectors.FloatVector(breaks_color_range)
        # print size_breaks
        # print size_breaks_labs

        input_df = rpy2.robjects.conversion.py2rpy(df)
        # input_df = rpy2.robjects.pandas2ri.py2ri(df)

        # print type(input_df)
        # print 'breaks', size_breaks
        # print 'break_labels', size_breaks_labs

        if output_path is not None and not output_path.endswith("." + kwargs.get('format', 'pdf')):
            output_path = output_path + "." + kwargs.get('format', 'pdf')

        print("output path \n%s" % (output_path))
        if kwargs.get('save_tmp', False):
            DataFrameAnalyzer.to_tsv_gz(df, '/tmp/test_ggrepel.tsv.gz')

        if log:
            print('calling ggrepel')
            print(type(input_df))
            print(type(xlab))
            print(type(ylab))
            print(type(output_path))
            print(type(w))
            print(type(h))
            print(type(title))
            print(type(size_breaks), size_breaks)
            print(type(size_breaks_labs), size_breaks_labs)

        if color_text_values is not None:
            color_text_values = rpy2.robjects.vectors.StrVector(color_text_values)
        else:
            color_text_values = rpy2.robjects.vectors.StrVector(['gray', 'red'])


        if log:
            print(xlab)
            print(ylab)
            print(output_path)
            print(ymin, ymax)
            print(xmin, xmax)
            print('ggrepel fontsize:', ggrepel_fontsize)
            print(breaks_color_range)
            print(size_breaks)
            print(size_breaks_labs)
            print(color_text_values)
            print(color_lab)
            print(size_lab)
            print(coord_fixed)

        yticks = rpy2.robjects.vectors.FloatVector(yticks) if yticks is not None else -1
        xticks = rpy2.robjects.vectors.FloatVector(xticks) if xticks is not None else -1

        print(yticks)
        print(xticks)
        # print input_df.head()
        print('calling method...')
        out =  r['plot_performances'](input_df, xlab, ylab, output_path,
                                     w=w, h=h, title=title,
                                     ymin=ymin, ymax=ymax, xmin=xmin, xmax=xmax,
                                     ggrepel_fontsize=ggrepel_fontsize,
                                     size_breaks=size_breaks,
                                     segment_size=segment_size,
                                     box_padding=box_padding, yticks=yticks, xticks=xticks,
                                     breaks_color_range=breaks_color_range,
                                     size_breaks_labs=size_breaks_labs, color_text_values=color_text_values,
                                     color_lab=color_lab, size_lab=size_lab,
                                     # end of method,
                                     add_sizes_bar=add_sizes_bar,
                                     coord_fixed=coord_fixed, **kwargs)
        if kwargs.get('save', True):
            print('output generated at')
            print(abspath(output_path))
        return out

    @staticmethod
    def plot_multiple_ggrepel(plots, output_path, nrow=2, ncol=2, w=20, h=20):
        print('# of plots', len(plots))
        print('if you have the viewport error, please check the figure size (ie w, h)...')
        r = rpy2.robjects.r
        # print 'importing function'
        src_path = "/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/plot_scatter_ggrepel.R"
        if not exists(src_path):
            src_path = "C:\\Users\\ignacio\\Dropbox\\Eclipse_Projects\\wild\\lib\\rscripts\\plot_scatter_ggrepel.R"
        r.source(src_path)
        # print 'import done...'
        return r['plot_multiple'](plots, output_path, nrow=nrow, ncol=ncol, w=w, h=h)

    @staticmethod
    def get_blank_panel():
        r = rpy2.robjects.r
        src_path = "/g/scb/zaugg/rio/EclipseProjects/wild/lib/rscripts/plot_scatter_ggrepel.R"
        if not exists(src_path):
            r.source(src_path)
        return r['get_blank_panel']

    @staticmethod
    def get_lrtest(X1, X2, y):
        from rpy2.robjects.packages import importr
        from rpy2.robjects.vectors import FloatVector

        stats = importr('stats')
        base = importr('base')
        lmtest = importr('lmtest')

        robjects.globalenv["y"] = y

        models = []
        for x in [X1, X2]:
            query = "y ~ "
            for ci in range(x.shape[1]):
                robjects.globalenv["x" + str(ci)] = FloatVector(list(x[:, ci]))
                query += "x" + str(ci)
                if ci + 1 != x.shape[1]:
                    query += " + "
            print(query)
            lm = stats.lm(query)
            models.append(lm)

        pvalue = list(lmtest.lrtest(models[0], models[1]))[-1][-1]
        print(pvalue)
        return pvalue

    @staticmethod
    def get_pam_silhouette_score(df, k):
        r = rpy2.robjects.r
        p = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/clustering.R'
        r.source(p)
        input_df = rpy2.robjects.pandas2ri.py2ri(df)
        return r['pam_silhouette'](df, k)[0]

    @staticmethod
    def write_entrezid_from_genenames(genes_names, output_path, dataset='mouse',
                                      filter_code="external_gene_name"):
        # write gene names to output file, for converting them into entrez IDs
        genes_path = tempfile.mkstemp()[1]
        writer = open(genes_path , "w")
        for gene in genes_names:
            writer.write(str(gene) + "\n")
        writer.close()

        # other filter options include
        # "external_gene_name"
        # "ensembl_gene_id"

        rbin = "/g/software/bin/Rscript"
        if not exists(rbin):
            rbin = 'Rscript'

        cmd = " ".join([rbin,
                         join(RSCRIPTS_DIR, "00_convert_gene_names_to_entrez.R"),
                         genes_path, output_path, filter_code])
        print(cmd)
        system(cmd)

        print(output_path)

    @staticmethod
    def get_glm_logistic(df):
        r = rpy2.robjects.r
        pval = r['binom.test']('log_yi ~ ', data=df,
                              family='poisson(link=log)')
        return pval

        fit = glm(regressionFormula,
                  data=data,
                  family=poisson(link="log"))
    @staticmethod
    def pam_clustering(input_path, output_path):
        # write gene names to output file, for converting them into entrez IDs
        rbin = "/g/scb/zaugg/rio/miniconda2/bin/Rscript"
        if not exists(rbin):
            rbin = 'Rscript'

        cmd = " ".join([rbin,
                         join(RSCRIPTS_DIR, "clustering_by_pam.R"),
                         input_path, output_path])
        print(cmd)
        system(cmd)

    # PAM, hclust and kmeans are interrogated in this function
    @staticmethod
    def get_pam_clustering(df, kmax, kmin=None):
        print('pam clustering...')
        r = rpy2.robjects.r
        p = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/clustering.R'
        r.source(p)
        input_df = rpy2.robjects.pandas2ri.py2ri(df)
        if kmin is None:
            kmin = 2
        res = r['pam_clustering'](df, kmax, kmin=kmin)

        out = pd.DataFrame()
        for ci, ki in enumerate(range(kmin - 1, kmax)):
            print(ci, ki)
            out[ki + 1] = pandas2ri.ri2py(res[ci + 1])
        return out

    @staticmethod
    def get_clustering_stability_measures(df, k):
        r = rpy2.robjects.r
        p = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/clustering_clValid.R'
        r.source(p)
        input_df = rpy2.robjects.pandas2ri.py2ri(df)
        res = r['calculate_stability_measures'](df, k)
        return pandas2ri.ri2py(res)

    @staticmethod
    def get_hclust_silhouette(df, kstart, kend):
        r = rpy2.robjects.r
        p = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/clustering.R'
        r.source(p)
        input_df = rpy2.robjects.pandas2ri.py2ri(df)
        res = r['hclust_silhouette'](df, kstart, kend)
        return pandas2ri.ri2py(res)

    @staticmethod
    def get_fcm_clustering(df, k):
        r = rpy2.robjects.r
        p = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/clustering.R'
        r.source(p)
        input_df = rpy2.robjects.pandas2ri.py2ri(df)
        output = r['fcm_clustering'](df, k)
        print(output)
        clusters, silhouette_score = list(map(int, output[:-1])), output[-1]
        return clusters, silhouette_score

    @staticmethod
    def biomart_convert(input, output, dataset='hsapiens_gene_ensembl',
                        code='external_gene_name'):
        # other filter options include
        # "external_gene_name"
        # "ensembl_gene_id"

        rbin = "/g/software/bin/Rscript"
        if not exists(rbin):
            rbin = 'Rscript'
        cmd = " ".join([rbin,
                         join(RSCRIPTS_DIR, "00_biomart_convert_generic.R"),
                         '-f', input, '-o', output, '-g', dataset, '-c', code])
        print(cmd)
        system(cmd)
        print(output)
        return output

    @staticmethod
    def get_snp_coordinates(snps, bkp_path=None, overwrite=True):
        if overwrite or bkp_path is None or not exists(bkp_path):
            print('importing getSnpsFunction')
            r = rpy2.robjects.r
            print('# of snps in input', len(snps))
            print(snps[:3])
            r.source('/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/convert_snps_to_coordinates.R')
            snps = rpy2.robjects.vectors.StrVector(snps)
            r = rpy2.robjects.r
            res = r['getSNPCoordinates'](snps)
            print(res.head())
            res = pandas2ri.ri2py(res)
            DataFrameAnalyzer.to_tsv_gz(res, bkp_path)
        return DataFrameAnalyzer.read_tsv_gz(bkp_path)


    @staticmethod
    def get_qvals(pvals, pvals_random, fast=True):
        '''
        Implementation defined step by step as
        http://brainchronicle.blogspot.de/2012/12/computing-empirical-pfdr-in-r.html?m=1

        :param pvals_random:
        :return:
        '''
        qvals = []

        R = float(len(pvals_random) / len(pvals)) # proportion between random and real p-values

        if not fast:
            for i in range(len(pvals)):
                print(i, len(pvals))
                qvals.append(float(sum([1 if p < pvals[i] else 0 for p in pvals_random])))
        else: # fast although memory intensive
            # print pvals[:5]
            sorted_pvals = sorted([[i, pi] for i, pi in enumerate(pvals)], key=lambda x: x[-1])
            j = 0 # counter to keep track of qval highest random p-value tracked so far
            sorted_pvals_rand = sorted(pvals_random)
            for i, pi in sorted_pvals:
                n_lower_than_pi = j
                # print i, 'next pval', pi
                while sorted_pvals_rand[j] <= pi and j + 1 < len(sorted_pvals_rand):
                    n_lower_than_pi += 1
                    j += 1
                qvals.append(n_lower_than_pi / R)
                # print i, pi, n_lower_than_pi
            # reassign qvalues back to normal
            # print sorted_pvals[:5]
            for i in range(len(sorted_pvals)):
                sorted_pvals[i] += [qvals[i]]
            # print sorted_pvals[:5]
            qvals = [q[-1] for q in sorted(sorted_pvals, key=lambda x: x[0])]

        qvals = [q / len(qvals) for q in qvals]
        return qvals

    @staticmethod
    def get_fdr(df):
        '''
        Calculate an FDR
        :param df: labels (1, 0) have to be defined
        :return:
        '''
        fdr_by_score = {}
        tp = 0
        fp = 0
        fdrs = []

        for ri, r in df.iterrows():
            label = r.values
            tp += 1 if int(label) == 1 else 0
            fp += 1 if int(label) == 0 else 0
            fdr = float(fp) / float(fp + tp)
            fdrs.append(fdr)

        return fdrs

    @staticmethod
    def get_bh_pvaluespvalues_python(p):

        """Benjamini-Hochberg p-value correction for multiple hypothesis testing."""

        p = np.asfarray(p)
        by_descend = p.argsort()[::-1]
        by_orig = by_descend.argsort()
        steps = float(len(p)) / np.arange(len(p), 0, -1)
        q = np.minimum(1, np.minimum.accumulate(steps * p[by_descend]))
        return q[by_orig]

    @staticmethod
    def get_bh_pvalues_python_df(df):
        """Benjamini-Hochberg p-value correction for multiple hypothesis testing."""
        p = np.concatenate(np.asfarray(df))
        by_descend = p.argsort()[::-1]
        by_orig = by_descend.argsort()
        steps = float(len(p)) / np.arange(len(p), 0, -1)
        q = np.minimum(1, np.minimum.accumulate(steps * p[by_descend]))
        q = q[by_orig].reshape(df.shape[0], df.shape[1])
        df = pd.DataFrame(q, index=df.index, columns=df.columns)
        return df

    @staticmethod
    def intersect_ranges(path1, path2, output_path=None):
        """
        Given three paths, we intersect the ranges and return a common table with all the intersections found
        :param path1:
        :param path2:
        :param output_path:
        :return:
        """
        if output_path is None:
            output_path = tempfile.mkstemp()[1]
        rbin = "/g/software/bin/Rscript"
        if not exists(rbin):
            rbin = 'Rscript'
        print('running')
        args = [rbin, join(RSCRIPTS_DIR, "intersect.R"), '--p1', path1,
                '--p2', path2, '--output', output_path]
        print(" ".join(args))
        system(" ".join(args))
        return output_path

    @staticmethod
    def intersect_ranges_df(df1, df2, output_path=None):
        """
        Given two dataframes, we intersect the ranges and return a path for a common table
        :param df1:
        :param df2:
        :param output_path:
        :return:
        """

        tmp1, tmp2 = tempfile.mkstemp()[1], tempfile.mkstemp()[1]

        print('preparing input files...')
        df1.to_csv(tmp1, header=False, sep='\t', compression='gzip', index=None)
        df2.to_csv(tmp2, header=False, sep='\t', compression='gzip', index=None)
        print('table 1 shape', df1.shape)
        print('table 2 shape', df2.shape)

        return RFacade.intersect_ranges(tmp1, tmp2, output_path)

    @staticmethod
    def run_topGO(fg, bg, debug=True, species='mouse'):
        r = rpy2.robjects.r

        print(len(fg), len(bg))
        if debug:
            print('saving tmp backups...')
            DataFrameAnalyzer.to_tsv(pd.DataFrame(list(fg)), '/tmp/fg.tsv', header=None)
            DataFrameAnalyzer.to_tsv(pd.DataFrame(list(bg)), '/tmp/bg.tsv', header=None)
        print('importing function')
        r.source('/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/topGO/run_topGO_' + species + '.R')
        print('calling main function...')
        res = r['run_topGO'](StrVector(list(fg)), StrVector(list(bg)))
        go_df, sig_genes_df = res
        return pandas2ri.ri2py(go_df), pandas2ri.ri2py(sig_genes_df)