'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.DESeq2Analyzer import DESeq2Analyzer
from lib.SequenceMethods import SequenceMethods
class DiffBindAnalyzer():

    @staticmethod
    def run(df, fg, bg, output_dir, sample_data_path=None, counts_bkp='counts.Rda'):
        # prepare output dir and move dataframe to that position

        # the column treatment has to exist to conduct a comparison
        if not exists(output_dir):
            mkdir(output_dir)

        if sample_data_path is None:
            sample_data_path = abspath(join(output_dir, "sample_data.tsv"))
            if exists(sample_data_path):
                remove(sample_data_path)

        print(df)
        print(sample_data_path)
        DataFrameAnalyzer.to_tsv(df, sample_data_path, sep=',')

        script_path = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/diffbind_run_basic_contrast.R'
        cmd = ' '.join([DESeq2Analyzer.get_rscript_cluster_path(), script_path, '--samples', sample_data_path,
                        '--fg', fg, '--bg', bg,
                        '--outdir', output_dir, '--outcounts', counts_bkp])
        print(cmd)

        system(cmd)

        return DiffBindAnalyzer.get_contrast_result(fg, bg, output_dir)

    @staticmethod
    def get_contrast_result_path(fg, bg, output_dir):
        output_path = join(output_dir, '%s_vs_%s.tsv.gz' % (fg, bg))
        return output_path

    @staticmethod
    def get_contrast_result(fg, bg, output_dir):
        # assuming the output was created successfully, return it
        output_path = DiffBindAnalyzer.get_contrast_result_path(fg, bg, output_dir)
        res = DataFrameAnalyzer.read_tsv_gz(output_path)
        res = res.rename(columns={'CHR':'chr', 'START': 'start', 'END': 'end'})
        res = SequenceMethods.parse_range2coordinate(res, ['chr', 'start', 'end'], 'k')
        return res
