'''
Created on 6/15/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.ClusterProfiler import ClusterProfiler
from lib.RFacade import RFacade
import rpy2
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
from rpy2.robjects.vectors import FloatVector, StrVector
pandas2ri.activate() # to convert pandas to R dataframe
from lib.HumanTFs import HumanTFs
class GOAnalyzer:

    @staticmethod
    def run_GREAT(bed_coordinates, bg=None, genome='hg19', category=None): #  base_url='http://great.stanford.edu/public/cgi-bin'):
        r = rpy2.robjects.r
        src_path = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/run_GREAT.R'
        r.source(src_path)

        fg = rpy2.robjects.pandas2ri.py2ri(bed_coordinates)
        res, job_data = None, None
        if bg is not None:
            res, job_data = r['runGREATAnalysis'](fg, bg=bg, species=genome) # base_url=base_url)
        else:
            res, job_data = r['runGREATAnalysis'](fg, species=genome) # base_url=base_url)

        res = pandas2ri.ri2py(res)

        res['Hyper_QValue'] = RFacade.get_bh_pvalues(res['Hyper_Raw_PValue'])

        if 'Binom_QValue' in res:
            res['Binom_QValue'] = RFacade.get_bh_pvalues(res['Binom_Raw_PValue'])
            res['Z'] = (((-np.log10(res['Binom_QValue'])) ** 2) +
                        ((-np.log10(res['Hyper_QValue'])) ** 2)) ** .5
            res = GOAnalyzer.add_best_qval_and_FE(res)

        return [res, job_data]

    @staticmethod
    def submit_GREAT_job(bed_coordinates, bg=None, genome='hg19', category=None):
        r = rpy2.robjects.r
        src_path = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/run_GREAT.R'
        r.source(src_path)

        bg = rpy2.robjects.pandas2ri.py2ri(bg) if bg is not None else None
        job_data = r['submitJob'](rpy2.robjects.pandas2ri.py2ri(bed_coordinates), bg=None,
                                  species=genome) # base_url=base_url)
        return job_data

    @staticmethod
    def get_tss_genes(genome='hg19', **kwargs):
        from lib.MyGeneAnalyzer import MyGeneAnalyzer
        tss = MyGeneAnalyzer.get_gene_tss({'mm10': 'mouse', 'hg19': 'human'}[genome], **kwargs)
        tss = tss.rename(
            columns={'chr': 'chromosome', 'tss': 'transcription start site', 'pos': 'strand', 'SYMBOL': 'geneName'})
        tss = tss[['chromosome', 'transcription start site', 'strand', 'geneName']]
        tss['strand'] = np.where(tss['strand'], '-', '+')
        tss['transcription start site'] = tss['transcription start site'].astype(int)
        return tss

    @staticmethod
    def prepare_tss_in_great(genome='hg19'):
        output_path = '/g/scb2/zaugg/rio/data/greatTools/data/%s/TSS.in' % genome
        if not exists(output_path):
            from lib.MyGeneAnalyzer import MyGeneAnalyzer
            if not exists(output_path):
                tss = GOAnalyzer.get_tss_genes(genome=genome)
                DataFrameAnalyzer.to_tsv(tss, output_path, header=None)

    @staticmethod
    def get_GREAT_peaks_by_ontology(job_data, **kwargs): #  ontology=None, termID=None):
        r = rpy2.robjects.r
        res = r['getPeaksByOntology'](job_data, **kwargs)
        res = pandas2ri.ri2py(res)
        return res

    @staticmethod
    def get_simple_great_df(res):
        # clean to make it easily visible
        for k in ['Binom_Genome_Fraction', 'Z', 'Binom_QValue', 'Hyper_QValue', 'Binom_Fold_Enrichment',
                  'Hyper_Fold_Enrichment', 'Hyper_Gene_Set_Coverage', 'Binom_Gene_Set_Coverage',
                  'Binom_Region_Set_Coverage', 'Hyper_Region_Set_Coverage', 'Hyper_Total_Genes', 'Hyper_Total_Expected',
                  'Hyper_Expected',
                  'Binom_Expected', 'Binom_Observed_Region_Hits', 'Hyper_Observed_Gene_Hits',
                  'Hyper_Term_Gene_Coverage',
                  'Binom_Raw_PValue', 'Binom_Adjp_BH', 'Hyper_Raw_PValue', 'Hyper_Adjp_BH', 'Binom_Adjp_BH']:
            if k in res:
                del res[k]
        return res

    @staticmethod
    def get_children_terms_recursive(go_id):
        from goatools.base import download_go_basic_obo
        obo_fname = download_go_basic_obo()
        from goatools.base import download_ncbi_associations
        gene2go = download_ncbi_associations()
        from goatools.obo_parser import GODag
        obodag = GODag("go-basic.obo")
        def rec_iter(lst):
            if len(lst.children) == 0:
                return set([lst.id])
            next = set()
            for chi in lst.children:
                next = next.union(rec_iter(chi))
            return set([lst.id]).union(next)
        cell_diff_terms = set()
        """Collect levels in a Group of GO Terms."""
        recs = obodag.get(go_id)
        children_terms = rec_iter(recs)
        return children_terms

    @staticmethod
    def get_GO_depth_level(go_id_list):
        from goatools.base import download_go_basic_obo
        obo_fname = download_go_basic_obo()
        from goatools.base import download_ncbi_associations
        gene2go = download_ncbi_associations()
        from goatools.obo_parser import GODag
        obodag = GODag("go-basic.obo")

        """Collect levels in a Group of GO Terms."""
        recs = [obodag.get(GO) for GO in go_id_list]
        level_dict = dict()
        for rec in recs:
            if not rec.is_obsolete:
                level_dict.setdefault(rec.id, {})
                level_dict[rec.id].setdefault(rec.namespace, [])
                level_dict[rec.id][rec.namespace] = rec.level
        return level_dict

    @staticmethod
    def add_best_qval_and_FE(res):
        res['Best_QValue'] = res[['Binom_QValue', 'Hyper_QValue']].min(axis=1)
        res['Best_FE'] = res[['Binom_Fold_Enrichment', 'Hyper_Fold_Enrichment']].max(axis=1)
        return res

    @staticmethod
    def run_go_three_way(fg_ensembl, bg_ensembl, fg_symbol, bg_symbol, species,
                         run_clusterProfiler=True, run_topGO=True, run_stringDB=True,
                         df_clusterProfiler=None, **kwargs):
        print('clusterProfiler...')
        df_clusterProfiler = None
        if run_clusterProfiler and df_clusterProfiler is None:
            df_clusterProfiler = ClusterProfiler.run_clusterProfiler_bash(fg_ensembl, bg_ensembl, species='mouse', **kwargs)

        if run_topGO or run_stringDB:
            from lib.RFacade import RFacade
            print('topGO...')
            df_topGO = RFacade.run_topGO(fg_ensembl, bg_ensembl, species=species)[0]

            print('string...')


            if df_clusterProfiler is not None:
                df_clusterProfiler['GO.ID'] = df_clusterProfiler.index
                df_clusterProfiler['id'] = 'clusterProfiler'
                df_clusterProfiler['sig.score'] = df_clusterProfiler['p.adjust']
            df_topGO['sig.score'] = df_topGO['Fisher.elim']
            df_topGO['id'] = 'topGO'

            df_stringGO = None
            if run_stringDB:
                df_stringGO = RFacade.get_stringdb_enrichments(fg_symbol)
                df_stringGO['GO.ID'] = df_stringGO['term_id']
                df_stringGO['sig.score'] = df_stringGO['pvalue_fdr']
                df_stringGO['id'] = 'stringGO'

            # collect all description
            term_by_id = dict((list(DataFrameAnalyzer.get_dict(df_clusterProfiler, 'GO.ID', 'Description').items()) if run_stringDB else []) +
                              list(DataFrameAnalyzer.get_dict(df_topGO, 'GO.ID', 'Term').items()) +
                              (list(DataFrameAnalyzer.get_dict(df_stringGO, 'GO.ID', 'term_description').items()) if run_stringDB else []))
            res = pd.concat([next[['GO.ID', 'sig.score', 'id']].reset_index(drop=True) for next in
                             [df_clusterProfiler if run_clusterProfiler else None, df_topGO, df_stringGO if run_stringDB else None]
                             if next is not None])

            hm = res.pivot('GO.ID', 'id', 'sig.score')
            hm['GO.ID'] = hm.index
            hm['name'] = hm['GO.ID'].map(term_by_id)
            hm['best'] = hm[hm.columns[:3]].min(axis=1)
            hm = hm.sort_values('best', ascending=True)

            return [df_clusterProfiler, df_topGO, df_stringGO, hm]
        else:
            return df_clusterProfiler