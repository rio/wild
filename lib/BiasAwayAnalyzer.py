

from os.path import exists, join
from os import system, remove, mkdir
import gzip

class BiasAwayAnalyzer:
    def split_sequences_into_training_testing_batches(self, fg_fasta_path,
                                                      biasaway_bg_fasta_path, k,
                                                      output_dir,
                                                      pos_prefix='',
                                                      neg_prefix='',
                                                      mode="g",
                                                      compress=False,
                                                      **kwargs):
        """
        Prepare a k-fold training testing grouping of files, according to our criteria
        :param fasta_path:
        :param bed_path:
        :param k:
        :param output_dir:
        :return:
        """

        stopat = kwargs.get('stopat')
        query_ki = kwargs.get('query_ki', None)
        overwrite = kwargs.get('overwrite', False)

        fastas = [r for r in open(fg_fasta_path)]
        print(fg_fasta_path)
        if ":" in fastas[0]:
            beds = [r.split(":")[0].replace(">", "") + "\t" + "\t".join(r.strip().split(":")[1].split("-")) + "\n"
                    for r in [fa for i, fa in enumerate(fastas) if i % 2 == 0]]
        else:
            beds = [r.split("_")[0].replace(">", "") + "\t" +
                    r.split("_")[1] + "\t" + r.strip().split("_")[2] if '_' in r else str(i)
                for r in [fa for i, fa in enumerate(fastas) if i % 2 == 0]]

        print(len(fastas), len(beds))

        assert len(fastas) == (len(beds) * 2)
        entries = [[beds[i], fastas[i * 2: i * 2 + 2]] for i in
                   range(len(beds))]

        positives_output_dir = join(output_dir, 'foreground')
        negatives_output_dir = join(output_dir, 'background')

        for d in [positives_output_dir, negatives_output_dir]:
            if not exists(d):
                mkdir(d)

        print(len(fastas))
        print(len(entries))

        n = len(entries) / k
        for ki in range(k):


            if query_ki is not None and query_ki != ki:
                continue
            print('running BiasAway', ki, 'of', k)

            i, j = n * ki, n * (ki + 1)

            train = entries[:i] + entries[j:]  # not between i and j
            test = entries[i: j]  # all the elements between i and j
            # print ki, i, j, len(train), len(test)
            for set_id, rows in zip(('train', 'test'), [train, test]):
                # special case: if k == 1, then all goes into a single file
                if k == 1 and set_id == 'train':
                    continue

                # write fasta and bed files correspondingly
                fg_fasta_path, fg_bed_path = [join(positives_output_dir,
                                             set_id + "_" + str(ki)
                                             + ext) for ext in ('.fa', '.bed')]
                # FASTA
                if not exists(fg_fasta_path):
                    writer = open(fg_fasta_path, 'w')
                    for e in rows:
                        writer.write("".join(e[1]))
                    writer.close()
                # BED
                if not exists(fg_bed_path):
                    writer = open(fg_bed_path, 'w')
                    print(len(rows))
                    for e in rows:
                        writer.write(e[0].strip() + "\n")
                    writer.close()

                # Our correspondent negative sequences are created using BiasAway
                bg_fasta_path, bg_bed_path = [join(negatives_output_dir,
                                                   set_id + "_" + str(ki)
                                                   + ext + (".gz" if compress else ""))
                                              for ext in ('.fa', '.bed')]
                pdf_output_basename = bg_fasta_path.replace(".fa", '')

                print(fg_fasta_path)
                print(fg_bed_path)
                print(bg_fasta_path)
                print(bg_bed_path)


                python_path = "/home/ignacio/anaconda2/bin/python"
                python_path = python_path if exists(python_path) else '/home/rio/zaugglab/rio/miniconda2/bin/python'

                bias_away_path = "/home/ignacio/Dropbox/Eclipse_Projects/zaugglab/lib/BiasAway/BiasAway.py"
                if not exists(bias_away_path):
                    bias_away_path = "/g/scb/zaugg/rio/EclipseProjects/zaugglab/lib/BiasAway/BiasAway.py"
                assert exists(bias_away_path)

                if overwrite or not exists(bg_fasta_path) or len([r for r in open(bg_fasta_path)]) < 5:
                    print(overwrite)

                    if not compress:
                        cmd = " ".join([python_path, bias_away_path, mode,
                                        "-f", fg_fasta_path] +
                                       (["-b", biasaway_bg_fasta_path] if mode == "g" else []) +
                                       ['--pdfbasename', pdf_output_basename] +
                                        ['>', bg_fasta_path])
                        print(cmd)

                        system(cmd)
                        # convert fasta to BED
                        rows = [r.strip() for r in open(bg_fasta_path) if
                                r.startswith(">")]
                        rows = [r[1:].replace(":", "\t").replace("-", "\t") for r in
                                rows]
                        writer = open(bg_bed_path, 'w')
                        for r in rows:
                            writer.write(r + "\n")
                        writer.close()
                    else:
                        cmd = " ".join([python_path, bias_away_path, mode,
                                        "-f", fg_fasta_path] +
                                       (["-b", biasaway_bg_fasta_path] if mode == "g" else []) +
                                        ['|', "gzip", ">", bg_fasta_path])
                        print(cmd)

                        system(cmd)

                        # convert fasta to BED
                        rows = [r.strip() for r in gzip.open(bg_fasta_path) if
                                r.startswith(">")]
                        rows = [r[1:].replace(":", "\t").replace("-", "\t") for
                                r in
                                rows]
                        writer = gzip.open(bg_bed_path, 'w')
                        for r in rows:
                            writer.write(r + "\n")
                        writer.close()

                # convert fg_fasta and fg_bed into gzip
                if compress:
                    for p in fg_fasta_path, fg_bed_path:
                        gzip_path = p + ".gz"
                        with gzip.open(gzip_path, 'w') as writer:
                            for r in open(p):
                                writer.write(r)
                        remove(p)


            if stopat is not None and ki + 1 >= stopat:
                break

    def run(self, fasta_path, bg_path, output_path, mode='g', **kwargs):
        overwrite = kwargs.get('overwrite', True)
        assert exists(bg_path)

        # define python path and BiasAway path
        python_path = "/home/ignacio/anaconda2/bin/python"
        python_path = python_path if exists(
            python_path) else '/home/rio/zaugglab/rio/miniconda2/bin/python'
        bias_away_path = "/home/ignacio/Dropbox/Eclipse_Projects/zaugglab/lib/BiasAway/BiasAway.py"
        if not exists(bias_away_path):
            bias_away_path = "/g/scb/zaugg/rio/EclipseProjects/zaugglab/lib/BiasAway/BiasAway.py"

        compress = kwargs.get('compress', False) or output_path.endswith('.gz')
        if overwrite or not exists(output_path):
            print(overwrite)
            cmd = " ".join([python_path, bias_away_path, mode,
                            "-f", fasta_path] +
                           (["-b",
                             bg_path] if mode == "g" else []) +
                           ['--pdfbasename', output_path.replace(".gz", "").replace(".fa", "")] +
                           (["|", 'gzip'] if compress else []) +
                           ['>', output_path])
            print(cmd)
            system(cmd)