
import sys

from os.path import exists
from sklearn.metrics import roc_auc_score, average_precision_score
from sklearn.model_selection import KFold
import pandas as pd
from sklearn.linear_model import Ridge
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import RandomForestRegressor
import numpy as np
from sklearn.metrics.regression import r2_score, mean_squared_error

class MachineLearning:
    @staticmethod
    def KFoldGradientBoostingClassifier(X, y, k=10, **kwargs):
        kfold_i = 1
        kf = KFold(n_splits=k, shuffle=True, **kwargs)
        res = []
        for train, test in kf.split(X):
            X_train, X_test = X[train], X[test]
            y_train, y_test = y[train], y[test]

            # for v in [y_train, y_test]:
                # for i in range(0, 2):
                    # print i, sum([vi == i for vi in v])

            xgboost_path = '/g/scb/zaugg/zaugg_shared/Programs/xgboost/python-package'
            if exists(xgboost_path):
                sys.path.append(xgboost_path)
            from xgboost import XGBClassifier

            clf = XGBClassifier(nthread=1)

            model = clf.fit(X_train, y_train)
            auroc_train = roc_auc_score(y_train, model.predict(X_train))
            auroc_test = roc_auc_score(y_test, model.predict(X_test))
            t = [kfold_i, X_train.shape[0], X_test.shape[0], auroc_train, auroc_test]
            res.append(t)
            kfold_i += 1

        res = pd.DataFrame(res, columns=['ki', 'n.train', 'n.test', 'AUROC.train', 'AUROC.test'])
        return res

    @staticmethod
    def TrainGradientBoostingClassifier(X, y):
        sys.path.append('/g/scb/zaugg/zaugg_shared/Programs/xgboost/python-package/')
        from xgboost import XGBClassifier

        clf = XGBClassifier(nthread=1)
        model = clf.fit(X, y)
        return model

    @staticmethod
    def TrainRidgeRegressor(X, y, random_state=500):
        ridge = Ridge(random_state=random_state)
        model = ridge.fit(X, y)
        return model

    @staticmethod
    def get_r2_score(X, y, clf):
        return r2_score(y, clf.predict(X))

    @staticmethod
    def get_r2_score_ridge(X, y):
        clf = MachineLearning.TrainRidgeRegressor(X, y)
        return MachineLearning.get_r2_score(X, y, clf)

    @staticmethod
    def KFoldGradientBoostingRegressor(X, y, k=10, random_state=500):
        kfold_i = 1
        kf = KFold(n_splits=k, shuffle=True, random_state=random_state)
        res = []
        for train, test in kf.split(X):
            # test and train the augmented model
            # print 'next model', lab
            # print ''
            X_train, X_test, y_train, y_test = X[train], X[test], y[train], y[test]
            gbr = GradientBoostingRegressor(random_state=random_state)
            model = gbr.fit(X_train, y_train)
            y_pred_train = model.predict(X_train)
            r2_train = r2_score(y_train, y_pred_train)
            mse_train = mean_squared_error(y_train, y_pred_train)

            y_pred = model.predict(X_test)
            r2_test = r2_score(y_test, y_pred)

            # also, measure the accuracy of our predictions
            mse_test = mean_squared_error(y_test, y_pred)
            t = [kfold_i, X.shape[0], len(X_train), len(X_test),
                 r2_train, r2_test, mse_train, mse_test]
            res.append(t)
        kfold_i += 1

        columns = ['kfold', 'n',
                   'n.train', 'n.test', 'r2.train', 'r2.test', 'mse.train', 'mse.test']

        res = pd.DataFrame(res, columns=columns)
        return res

    @staticmethod
    def KFoldRandomForestRegressor(X, y, k=10, random_state=500, log=False):
        kfold_i = 1
        kf = KFold(n_splits=k, shuffle=True, random_state=random_state)
        res = []
        for train, test in kf.split(X):
            if log:
                print('Kfold %i...' % kfold_i)
            # test and train the augmented model
            # print 'next kfold', kfold_i
            # print ''
            X_train, X_test, y_train, y_test = X[train], X[test], y[train], y[test]
            gbr = RandomForestRegressor(random_state=random_state)
            model = gbr.fit(X_train, y_train)
            y_pred_train = model.predict(X_train)
            r2_train = r2_score(y_train, y_pred_train)
            mse_train = mean_squared_error(y_train, y_pred_train)

            y_pred = model.predict(X_test)
            r2_test = r2_score(y_test, y_pred)

            # also, measure the accuracy of our predictions
            mse_test = mean_squared_error(y_test, y_pred)
            t = [kfold_i, X.shape[0], len(X_train), len(X_test),
                 r2_train, r2_test, mse_train, mse_test]
            res.append(t)
            kfold_i += 1

        columns = ['kfold', 'n',
                   'n.train', 'n.test', 'r2.train', 'r2.test', 'mse.train', 'mse.test']

        res = pd.DataFrame(res, columns=columns)
        return res

    @staticmethod
    def get_1mer_model(seqs, y, random_state=500):
        from lib.ShapeAnalyzer import ShapeAnalyzer
        X = np.array(ShapeAnalyzer.get_X_1mer(seqs))
        y = np.array(y)
        gbr = Ridge(random_state=random_state)
        model = gbr.fit(X, y)
        return model

    @staticmethod
    def get_zero_one_norm(v):
        v = np.array(v)
        return (v - min(v)) / (max(v) - min(v))

    @staticmethod
    def get_SVC_single_fit_scores(X, y, random_state=500, scramble_cols=False, svm_model=None):
        from sklearn import svm
        from sklearn.multiclass import OneVsRestClassifier
        X = np.array(X)
        y = np.array(y)

        if svm_model is None:
            svm_model = svm.LinearSVC(random_state=random_state)

        print('fitting OneVsRestClassifier')
        classifier = OneVsRestClassifier(svm_model)
        clf = classifier.fit(X, y)
        return clf.decision_function(X)

    @staticmethod
    def KFoldLogit(X, y, k=10, random_state=500, label=None, id=None, position=None):
        kfold_i = 1
        kf = KFold(n_splits=k, shuffle=True, random_state=random_state)
        res = []

        from multiprocessing import Manager
        manager = Manager()
        kfold_out = manager.dict()

        def calculate_kfoldi(X_train, X_test, y_train, y_test, kfold_i):
            from sklearn.linear_model import LogisticRegression
            classifier = LogisticRegression(random_state=0, solver='lbfgs', multi_class='multinomial').fit(X, y)
            clf = classifier.fit(X_train, y_train)
            auroc_train, auprc_train = MachineLearning.get_auroc_n_auprc(y_train, clf.decision_function(X_train))

            y_pred = clf.decision_function(X_test)
            from sklearn.metrics import roc_curve, auc
            auroc_test, auprc_test = MachineLearning.get_auroc_n_auprc(y_test, y_pred)
            # also, measure the accuracy of our predictions
            mse_test = mean_squared_error(y_test, y_pred)
            t = [kfold_i, X.shape[0], len(X_train), len(X_test),
                 auroc_train, auprc_train, auroc_test, auprc_test]
            kfold_out[kfold_i] = t

        # print 'calculating permutations...'
        input = [[X[train], X[test], y[train], y[test]] for train, test in kf.split(X)]
        input = [input[ki] + [ki] for ki in range(k)]

        from lib.ThreadingUtils import ThreadingUtils
        ThreadingUtils.run(calculate_kfoldi, input, k)

        columns = ['kfold', 'n',
                   'n.train', 'n.test', 'auroc.train', 'auprc.train', 'auroc.test', 'auprc.test']

        res = pd.DataFrame([kfold_out[ki] for ki in list(kfold_out.keys())], columns=columns)
        if label is not None:
            res['label'] = label
        if id is not None:
            res['id'] = id
        if position is not None:
            res['position'] = position
        return res


    @staticmethod
    def KFoldSVC(X, y, k=10, random_state=500, label=None, id=None, position=None,
                 svm_model=None):
        kfold_i = 1
        kf = KFold(n_splits=k, shuffle=True, random_state=random_state)
        res = []

        from multiprocessing import Manager
        manager = Manager()
        kfold_out = manager.dict()

        def calculate_kfoldi(X_train, X_test, y_train, y_test, svm_model, kfold_i):
            from sklearn.multiclass import OneVsRestClassifier

            classifier = OneVsRestClassifier(svm_model)
            clf = classifier.fit(X_train, y_train)
            auroc_train, auprc_train = MachineLearning.get_auroc_n_auprc(y_train, clf.decision_function(X_train))


            y_pred = clf.decision_function(X_test)
            from sklearn.metrics import roc_curve, auc
            auroc_test, auprc_test = MachineLearning.get_auroc_n_auprc(y_test, y_pred)
            # also, measure the accuracy of our predictions
            mse_test = mean_squared_error(y_test, y_pred)
            t = [kfold_i, X.shape[0], len(X_train), len(X_test),
                 auroc_train, auprc_train, auroc_test, auprc_test]
            kfold_out[kfold_i] = t

        # print 'calculating permutations...'
        from sklearn import svm
        if svm_model is None:
            svm_model = svm.LinearSVC(random_state=random_state)
        input_svc = [[X[train], X[test], y[train], y[test], svm_model] for train, test in kf.split(X)]
        input_svc = [input_svc[ki] + [ki] for ki in range(k)]

        from lib.ThreadingUtils import ThreadingUtils
        ThreadingUtils.run(calculate_kfoldi, input_svc, k)

        columns = ['kfold', 'n',
                   'n.train', 'n.test', 'auroc.train', 'auprc.train', 'auroc.test', 'auprc.test']

        res = pd.DataFrame([kfold_out[ki] for ki in list(kfold_out.keys())], columns=columns)
        if label is not None:
            res['label'] = label
        if id is not None:
            res['id'] = id
        if position is not None:
            res['position'] = position
        return res


    @staticmethod
    def KFoldRidgeRegression(X, y, k=10, random_state=500, label=None, id=None, position=None):
        kfold_i = 1
        kf = KFold(n_splits=k, shuffle=True, random_state=random_state)
        res = []
        for train, test in kf.split(X):
            # print kfold_i
            # test and train the augmented model
            # print 'next model', lab
            # print ''
            X_train, X_test, y_train, y_test = X[train], X[test], y[train], y[test]

            # print 'random state', random_state
            gbr = Ridge(random_state=random_state)
            model = gbr.fit(X_train, y_train)
            y_pred_train = model.predict(X_train)
            r2_train = r2_score(y_train, y_pred_train)
            mse_train = mean_squared_error(y_train, y_pred_train)

            y_pred = model.predict(X_test)
            r2_test = r2_score(y_test, y_pred)

            # also, measure the accuracy of our predictions
            mse_test = mean_squared_error(y_test, y_pred)
            t = [kfold_i, X.shape[0], len(X_train), len(X_test),
                 r2_train, r2_test, mse_train, mse_test]
            res.append(t)
            kfold_i += 1

        columns = ['kfold', 'n',
                   'n.train', 'n.test', 'r2.train', 'r2.test', 'mse.train', 'mse.test']

        res = pd.DataFrame(res, columns=columns)
        if label is not None:
            res['label'] = label
        if id is not None:
            res['id'] = id
        if position is not None:
            res['position'] = position
        return res

    @staticmethod
    def get_auroc_wilcoxon(y_true, y_pred):
        # preds_pos < - preds[target == 1];
        # num_pos = as.numeric(length(preds_pos))
        # preds_neg < - preds[target == 0];
        # num_neg = length(preds_neg)
        # rankings < - data.table::frank(c(preds_pos, preds_neg))
        # auc < - (sum(rankings[1:num_pos]) - num_pos * (num_pos + 1) / 2) / (num_pos * num_neg)
        ranks = [rki for rki, yi in zip(np.rank(y_pred), y_true) if yi == 1]
        print(ranks)
        exit()


    @staticmethod
    def get_auroc_n_auprc(y_true, y_pred):
        auroc = roc_auc_score(y_true, y_pred)
        auprc = average_precision_score(y_true, y_pred)
        return auroc, auprc

    @staticmethod
    def get_auroc_n_auprc_posnneg(y_pos, y_neg):
        y_true = [1 for i in range(len(y_pos))] + [0 for i in range(len(y_neg))]
        y_pred = list(y_pos) + list(y_neg)
        auroc = roc_auc_score(y_true, y_pred)
        auprc = average_precision_score(y_true, y_pred)
        return auroc, auprc

    @staticmethod
    def assess_model(model, X_test, y_test):
        y_pred = model.predict(X_test)
        return MachineLearning.get_auroc_n_auprc(y_test, y_pred)