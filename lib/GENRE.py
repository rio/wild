'''
Created on 11/29/2017, 2017

@author: Ignacio Ibarra Del Rio

Description:
'''

from lib.utils import *
from lib.MyGeneAnalyzer import MyGeneAnalyzer

print('PLEASE CHECK THAT GENRE BINS MUST BE RUN WITH PYTHON 2.x.')

class GENRE:


    def run(self, fg_fasta, bg_fasta, glossary='glossary-kmer',**kwargs):
        """
        run genre with the provided set of inputs and outputs
        filenames will be created in the same directory where fasta files are located
        :param fg_fasta:
        :param bg_fasta:
        :param glossary:
        :return:
        """
        tmp_curdir = abspath('.')

        assert (os.path.abspath(os.path.join(fg_fasta, os.pardir)) ==
                os.path.abspath(os.path.join(bg_fasta, os.pardir)))

        genre_dir = kwargs.get('genre_dir', '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/genre')
        glossary_bin = join(genre_dir, 'bin/glossary/glossary')

        assert exists(genre_dir)
        chdir(genre_dir)
        cmd = " ".join([glossary_bin, fg_fasta, bg_fasta, glossary])
        print(cmd)
        system(cmd)

        chdir(tmp_curdir)

    @staticmethod
    def get_ppm_from_module(module_id):
        basedir = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/genre/glossary_description/Glossary Modules'
        p = join(basedir, 'Glossary_module_%s.xlsx' % module_id)
        ppm = pd.read_excel(p, sheet_name='PWM').transpose()
        ppm.columns = [i for i in range(ppm.shape[1])]
        return ppm

    def map_tf_to_module(self, tf_name, supp1_path):

        print(tf_name)

    @staticmethod
    def prepare_bg(bed_path_or_coordinates, output_dir, bg='hg19_dflt',
                   genre_seed=None, **kwargs):
        assert output_dir is not None

        tmp_curdir = abspath('.')

        mult = kwargs.get('mult', None)

        bed_path = None
        if isinstance(bed_path_or_coordinates, pd.DataFrame):
            if not exists(output_dir):
                mkdir(output_dir)
            bed_path = join(output_dir, 'fg.bed')
            DataFrameAnalyzer.to_tsv(bed_path_or_coordinates, bed_path, header=None)
        else:
            bed_path = bed_path_or_coordinates


        if output_dir is not None:
            if not exists(output_dir):
                mkdir(output_dir)

            if abspath(bed_path) != abspath(join(output_dir, basename(bed_path))):
                copy2(bed_path, join(output_dir, basename(bed_path)))
            bed_path = join(output_dir, basename(bed_path))

        genre_dir = kwargs.get('genre_dir', '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/genre')
        genre_bin = join(genre_dir, 'bin/GENRE/GENRE')

        assert exists(genre_dir)
        chdir(genre_dir)

        print(bed_path)
        expected_output_path = join(output_dir,
                                    basename(bed_path).replace(".bed", '') + "_" + bg.replace("_", '-') + "BG",
                                    basename(bed_path).replace(".bed", '') + "_" + bg.replace("_", '-') + "BG.bed")

        genre_seed = str(49472047) if not isinstance(genre_seed, int) else str(genre_seed)
        if not exists(expected_output_path) or kwargs.get('overwrite', True):
            cmd = " ".join([genre_bin, '-seed', genre_seed] + (['-mult', str(mult)] if mult is not None else []) +
                           [bg, bed_path])
            print(cmd)
            system(cmd)
        else:
            print('GENRE bed exists or it is not allowed to overwrite')

        chdir(tmp_curdir)

        fa_fg = join(output_dir,
                     basename(bed_path).replace(".bed", '') + "_" + bg.replace("_", '-') + "BG",
                     basename(bed_path).replace(".bed", '.fa'))
        fa_bg = expected_output_path.replace(".bed", '.fa')
        print(exists(fa_fg), fa_fg)
        print(exists(fa_bg), fa_bg)

        return fa_fg, fa_bg

    @staticmethod
    def mad(arr):
        """ Median Absolute Deviation: a "Robust" version of standard deviation.
            Indices variabililty of the sample.
            https://en.wikipedia.org/wiki/Median_absolute_deviation
        """
        arr = np.ma.array(
            arr).compressed()  # should be faster to not use masked arrays.
        med = np.median(arr)
        return np.median(np.abs(arr - med))

    @staticmethod
    def get_genre_genes_by_module():
        module_genes_bkp = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/genre/glossary_description/glossary_genes_bkp.tsv'
        if not exists(module_genes_bkp):
            modules_description_dir = join('/g/scb2/zaugg/rio/EclipseProjects/wild',
                                           'lib/genre/glossary_description/Glossary Modules')
            module_filenames = [f for f in listdir(modules_description_dir) if f.endswith('.xlsx')]
            table = []
            for module_filename in module_filenames:
                print(module_filename)
                module_name = module_filename.replace("Glossary_module_", "").replace(".xlsx", '')
                # if module_name != 'NR2E':
                #     continue
                print(module_filename)
                output_path = join("../../data/figures/e15_expr_by_kmer_module", module_name)
                # if exists(output_path + ".pdf"):
                #     continue
                p = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/genre/glossary_description/Glossary Modules/Glossary_module_' + module_name + '.xlsx'
                df = pd.read_excel(p, index_col=0)
                # print df
                selected_genes = set([k.split(".")[0].capitalize() for k in df.columns])
                mouse = MyGeneAnalyzer.get_ensembl_by_symbol(selected_genes, species='mouse')
                human = MyGeneAnalyzer.get_ensembl_by_symbol(selected_genes, species='human')
                mouse = set() if mouse is None else list(mouse.values())
                human = set() if human is None else list(human.values())

                homolog_mouse = MyGeneAnalyzer.get_homologs_mouse_from_human(human)
                homolog_human = MyGeneAnalyzer.get_homologs_human_from_mouse(mouse)
                for k, v in homolog_human.items():
                    table.append([module_name, 'mouse', k])
                    table.append([module_name, 'human', v])
                for k, v in homolog_mouse.items():
                    table.append([module_name, 'mouse', v])
                    table.append([module_name, 'human', k])



            res = pd.DataFrame(table, columns=['module.name', 'species', 'ensembl'])
            res = res[~pd.isnull(res['ensembl'])]

            DataFrameAnalyzer.to_tsv(res, module_genes_bkp)
        return DataFrameAnalyzer.read_tsv(module_genes_bkp)

    @staticmethod
    def get_module_names():
        return {s.replace(".kmer", "") for s in
                listdir('/g/scb2/zaugg/rio/EclipseProjects/wild/lib/genre/data/glossary/motifs/glossary-kmer')}

    @staticmethod
    def get_kmers_by_module():
        kmers_dir = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/genre/data/glossary/motifs/glossary-kmer'

        kmers_by_module = {}
        for i, f in enumerate(listdir(kmers_dir)):
            # print i, f
            p = join(kmers_dir, f)
            tf_id = f.replace(".kmer", "")
            kmers = DataFrameAnalyzer.read_tsv(p, header=None)
            for kmer in kmers[0]:
                if not tf_id in kmers_by_module:
                    kmers_by_module[tf_id] = set()
                kmers_by_module[tf_id].add(kmer)
        return kmers_by_module