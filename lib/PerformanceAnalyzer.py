
import pandas as pd
from sklearn.metrics import average_precision_score, precision_recall_curve,\
    roc_auc_score, roc_curve

class PerformanceAnalyzer:

    def get_performances(self, scores_pos, scores_neg):
        df = pd.DataFrame([[1, si] for si in scores_pos] + [[0, si] for si in scores_neg],
                          columns=['set', 'method'])
        t = df[["set", 'method']]
        labels, scores = t['set'].values.tolist(), t['method'].values.tolist()
        fpr, tpr, roc_thresholds = roc_curve(labels, scores)
        auroc = roc_auc_score(labels, scores)
        auprc = average_precision_score(labels, scores)
        precision, recall, _ = precision_recall_curve(labels, scores)
        return tpr, fpr, "%.3f" % auroc, precision, recall, "%.3f" % auprc
