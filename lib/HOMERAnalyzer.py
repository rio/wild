
from lib.utils import *
from lib.FastaAnalyzer import FastaAnalyzer
from lib.SequenceMethods import SequenceMethods

class HOMERAnalyzer:


    def run_go(self, entrez_ids_input, output_dir, organism='mouse', **kwargs):

        bg = kwargs.get("bg", None)
        execute = kwargs.get("execute", False)

        homer_command = ["findGO.pl", entrez_ids_input, organism, output_dir]

        if bg is not None:
            homer_command += ["-bg", bg]

        homer_command = " ".join(homer_command)
        print(homer_command)

        if execute:
            system(homer_command)

    @staticmethod
    def run_denovo_motif_enrichment(fasta, output_dir, **kwargs):
        homer_command = ["findMotifs.pl", fasta, "fasta", output_dir,
                         "-noknown" , "-nogo"]

        homer_command = " ".join(homer_command)
        print(homer_command)
        system(homer_command)

    @staticmethod
    def get_scores_homer(sequences, ppm_homer_path,
                         tmp_fasta=None,
                         strand="both", log=False, mscore=True,
                         get_best_sequence=False,
                         working_dir=None, as_dataframe=False):
        """
        Using HOMER suite to score a set of sequences using a reference PWM motif.

        Report scores accordingly

        If mscore == True, the method will report the best score per sequence

        @:param as_dataframe: Try to return a pandas dataframe with all data, if possible
        """

        last_working_directory = abspath(os.curdir)
        if working_dir is not None:
            os.chdir(working_dir)
        # add homer HOMER path to PATH if not found
        if not "homer/bin" in os.environ["PATH"]:
            homer_path = "/home/ignacio/Software/homer/bin"
            os.environ["PATH"] += os.pathsep + homer_path
            # add the cluster path
            homer_path = "/home/rio/zaugglab/rio/Software/homer/bin"
            os.environ["PATH"] += os.pathsep + homer_path

        # convert sequences into fasta
        if sequences != None and tmp_fasta is None:
            tmp_fasta = tempfile.mkstemp()[1]
            FastaAnalyzer.write_fasta_from_sequences(sequences, tmp_fasta)

        # motif width
        ppm = pd.read_csv(ppm_homer_path,
                          skiprows=1, header=None, sep='\t',
                          compression='gzip' if
                          ppm_homer_path.endswith(".gz") else 'infer').transpose()
        w = ppm.shape[1]


        if ppm_homer_path.endswith('.gz'):
            temp_ppm = tempfile.mkstemp()[1]
            with open(temp_ppm, 'w') as writer:
                for r in gzip.open(ppm_homer_path):
                    writer.write(r)
            ppm_homer_path = temp_ppm

        # homer2 find -s <seq file> -m <motif file>
        assert exists(tmp_fasta)

        cmd = ["homer2", "find", "-i", tmp_fasta, "-m", ppm_homer_path,
               "-mscore",
               "-strand", strand, "-offset", "0"]
        if get_best_sequence:
            cmd = ["homer2", "find", "-i", tmp_fasta, "-m", ppm_homer_path,
                   "-mscore",
                   "-offset", "0", "-strand", strand]

        print(" ".join(cmd))
        stdoutdata, stderrdata = sp.Popen(cmd, stdout=sp.PIPE,
                                          stderr=sp.PIPE).communicate()

        output = [r for r in stdoutdata.split("\n") if len(r) != 0]

        print("# output lines", len(output))
        print(stderrdata)

        # remove tmp fasta file
        # remove(tmp_fasta)
        if log:
            for r in output:
                print(r)

        if get_best_sequence:
            best_scores = {}
            for i, r in enumerate(output[::-1]):
                # retrieve the sequence according to the strand and offset,
                # given HOMER rules for defining them (right to left if +, left to right if -)
                split = r.split("\t")
                # print split
                # print sequences[i][0]
                # print split[0], str(sequences[i][0])
                # assert str(split[0]) == str(sequences[i][0])
                strand = split[4]
                position = int(split[1])
                start = None
                if strand == "+":
                    start = position
                elif strand == "-":
                    start = position + 1 - w
                end = start + w
                seq = sequences[i][start:end]
                if strand == "-":
                    seq = SequenceMethods.get_complementary_seq(seq)
                header, score = split[0], float(split[-1])
                if not header in best_scores:
                    best_scores[header] = [start, strand, sequences[i],
                                           SequenceMethods.get_complementary_seq(
                                               sequences[i]),
                                           seq, score]
                elif score >= best_scores[header][1]:
                    best_scores[header] = [start, strand, sequences[i],
                                           SequenceMethods.get_complementary_seq(
                                               sequences[i]),
                                           seq, score]

            if working_dir is not None:
                os.chdir(last_working_directory)

            if as_dataframe:
                df = pd.DataFrame(
                    [[header] + best_scores[header] for header in best_scores],
                    columns=['id', 'position', 'strand', 'seq.fwd',
                             'seq.rev', 'motif', 'score'])
                df['id'] = df['id'].astype(int)
                df = df.sort_values('id', ascending=True)
                return df

            return best_scores

        # =======================================================================
        # For some reason homer2's output generate all scores in inverse order
        # w.r.t. fasta input
        # =======================================================================
        else:
            best_scores = [t.split("\t") for t in output if len(t) > 5][::-1]

            best_scores = [t[:-1] + [float(t[-1])] for t in best_scores]
            # best_scores = [t[:2] + t[4:] for t in best_scores]
            # delete tmp fasta file after running homer successfully.
            # remove(tmp_fasta)
            if working_dir is not None:
                os.chdir(last_working_directory)
            return best_scores
