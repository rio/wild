'''
Created on 6/2/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.SequenceMethods import SequenceMethods


class GWASAnalyzer:

    @staticmethod
    def get_basedir():
        basedir = 'C:\\Users\\ignacio\\Dropbox\\Eclipse_Projects\\zaugglab\\moritz_collaboration\\data'
        if not exists(basedir):
            basedir = "/mnt/c/Users/ignacio/Dropbox/Eclipse_Projects/zaugglab/moritz_collaboration/data"
        basedir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data_2018-07-09_00-00-00/data'
        return basedir

    @staticmethod
    def get_gwas_data():
        basedir = GWASAnalyzer.get_basedir()
        data_path = join(basedir, "gwas_catalog_v1.0-associations_e92_r2018-06-25.tsv")
        print(exists(data_path), data_path)
        snp_coordinates_path = join(basedir, "gwas_snp_coordinates.tsv")
        df = pd.read_csv(data_path, index_col=None, sep='\t')
        return df

    @staticmethod
    def get_gwas_coordinates():
        basedir = GWASAnalyzer.get_basedir()
        bed_path = join(basedir, "gwas_and_flanks.bed")
        fasta_path = bed_path.replace(".bed", ".fa")
        snp_coordinates_path = join(basedir, "gwas_snp_coordinates.tsv")
        if not exists(snp_coordinates_path):
            df = GWASAnalyzer.get_gwas_data()
            snps = df[['SNPS', 'CHR_ID', 'CHR_POS']]
            snps_path = join(basedir, "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data_2018-07-09_00-00-00/data/snps.txt")
            writer = open(snps_path, 'w')
            for r in snps['SNPS']:
                writer.write(r + "\n")
            from lib.RFacade import RFacade
            print(snps.head())
            res = RFacade.get_snp_coordinates(list(set(snps['SNPS'])), snp_coordinates_path)
            print(res.head())

        print(snp_coordinates_path)
        df = DataFrameAnalyzer.read_tsv_gz(snp_coordinates_path, index_col=None, sep='\t')
        df['start'] = df['chrom_start'] - 10
        df['end'] = df['chrom_start'] + 10
        df['chr_name'] = 'chr' + df['chr_name']
        df = SequenceMethods.parse_range2coordinate(df, ['chr_name', 'start', 'end'], 'k')
        return df

    @staticmethod
    def get_trait_mapping_associations():
        basedir = GWASAnalyzer.get_basedir()
        p = join(basedir, 'gwas_catalog_trait-mappings_r2018-06-25.tsv')
        return DataFrameAnalyzer.read_tsv(p)
