'''
Created on Jun 2, 2016

@author: ignacio
'''

from os import system
from os import remove
import tempfile
from os.path import exists
import gzip
from lib.DataFrameAnalyzer import DataFrameAnalyzer
class PeaksAnalyzer():
    '''
    classdocs
    '''
        
    def insersect_chip_seq_peaks(self, bed_path1, bed_path2, output_path,
                                 compress=False, use_first_summit=False,
                                 additional_args=['-u'], **kwargs):
        """
        Given two bed paths, get the number of intersections intesect and remove
        """
        # print "running moods..."    
        # run BED intersection with selected ChIP-seq peaks

        only_a = kwargs.get("only_a")
        bedtools_path = "/usr/bin/bedtools"
        bedtools_path = '/g/funcgen/bin/bedtools'
        if not exists(bedtools_path):
            bedtools_path = "/g/software/bin/bedtools"
        bedtools_path = bedtools_path if exists(bedtools_path) else 'bedtools'

        log = kwargs.get('log', False)
        compress = True if output_path.endswith(".gz") else compress

        # default: this is the instersection between peak datasets
        mode = kwargs.get('mode', 'intersect') # other modes: closest
        args = [bedtools_path, mode, "-a", bed_path1, "-b",
                bed_path2] +  [arg for arg in additional_args] + [">", output_path]
        if only_a: # we want peaks that belong to TF1 only
            args = [bedtools_path, mode, "-a", bed_path1, "-b",
                    bed_path2, "-v", ">", output_path]
        if compress:
            args = args[:-2] + ["|", "gzip", ">", output_path]
        args = " ".join(args)

        # print args
        print(args)

        system(args)

        # get amount of intersected peaks
        is_gz = bed_path1.endswith(".gz")
        nA = sum([1 for line in (gzip.open(bed_path1) if is_gz else open(bed_path1))])
        is_gz = bed_path2.endswith(".gz")
        nB = sum([1 for line in (gzip.open(bed_path2) if is_gz else open(bed_path2))])
        nIntersections = sum([1 for line in (gzip.open(output_path) if compress else open(output_path))])
        # remove(output_path)

        if kwargs.get('remove_beds', True):
            if log:
                print('remove beds', kwargs.get('remove_beds'))
            remove(bed_path1)
            remove(bed_path2)
        else:
            print('maintain input beds...')
        return [nA, nB, nIntersections]

    @staticmethod
    def intersect_coordinates(coordinates_df1, coordinates_df2, output_path=None, **kwargs):
        bed1 = tempfile.mkstemp()[1] if kwargs.get('bed_path1') is None else kwargs.get('bed_path1')
        bed2 = tempfile.mkstemp()[1] if kwargs.get('bed_path2') is None else kwargs.get('bed_path2')
        output_temp = tempfile.mkstemp()[1] if output_path is None else output_path
        DataFrameAnalyzer.to_tsv(coordinates_df1, bed1, header=None, log=kwargs.get('log', False))
        DataFrameAnalyzer.to_tsv(coordinates_df2, bed2, header=None, log=kwargs.get('log', False))

        peaks_analyzer = PeaksAnalyzer()
        for k in ['bed_path1', 'bed_path2']:
            if k in kwargs:
                del kwargs[k]
        return peaks_analyzer.insersect_chip_seq_peaks(bed1, bed2, output_temp, **kwargs) + [output_temp]

    @staticmethod
    def intersect_coordinates_from_ids(ids1, ids2, output_path=None, **kwargs):
        import pandas as pd
        from lib.SequenceMethods import SequenceMethods
        df1 = pd.DataFrame(ids1, columns=['k1'])
        df2 = pd.DataFrame(ids2, columns=['k2'])
        df1 = SequenceMethods.parse_coordinate2range(df1, df1['k1'], ['chr', 'start', 'end'])
        df2 = SequenceMethods.parse_coordinate2range(df2, df2['k2'], ['chr', 'start', 'end'])
        out = PeaksAnalyzer.intersect_coordinates(df1[['chr','start','end','k1']],
                                                  df2[['chr','start','end','k2']], output_path=output_path, **kwargs)
        print(out)
        out = DataFrameAnalyzer.read_tsv(out[-1], columns=['chr.1', 'start.1', 'end.1', 'k1', 'chr.2', 'start.2', 'end.2', 'k2'],
                                         header=None)
        return out


    def convert_bam_to_bed(self, input_path_bam, output_path_sorted_bed):
        cmd = " ".join(['samtools', 'view', '-bS', input_path_bam, "|",
                        'bedtools', 'bamtobed' , '|', 'sort -n -k 2',  '>',
                        output_path_sorted_bed])
        print(cmd)
        system(cmd)


    @staticmethod
    def get_ranges_overlap(x, y):
        return len(list(range(max(x[0], y[0]), min(x[-1], y[-1]) + 1)))

    def count_reads_per_window(self, input_bed, input_bam, output_counts_path,
                               tmp_bed_path=None):

        # the first step is to convert our bam file to a temporary bed

        if not exists(tmp_bed_path):
            tmp_bed_path = tempfile.mkstemp()[1]

        # this step is very slow (run using a background file if possible)
        self.convert_bam_to_bed(input_bam, tmp_bed_path)

        ## Count the number of reads per window in the FNR ChIP-seq library

        cmd = " ".join(['bedtools', 'intersect', '-a', input_bed,
                        '-b', tmp_bed_path, '-c', '-sorted', '>',  output_counts_path])
        print(cmd)
        system(cmd)


    def intersect_multiple_files(self, bed_paths, **kwargs):
        '''
        Intersect multiple peaks files using AND or OR
        (default: AND)
        :return:
        '''

        compress = kwargs.get("compress", False)
        output_path = kwargs.get("output_path", None)

        bedtools_path = "/usr/bin/bedtools"
        if not exists(bedtools_path):
            bedtools_path = "/g/software/bin/bedtools"
        bedtools_path = bedtools_path if exists(bedtools_path) else 'bedtools'

        if output_path is None:
            output_path = tempfile.mkstemp()[1]
        # default: this is the intersection between peak datasets
        args = [bedtools_path, "intersect", "-a", bed_paths[0], "-b"] + [bed_paths[1], "-u"]
        if len(bed_paths) >= 3:
            for next in bed_paths[2:]:
                args += ["|", bedtools_path, 'intersect', "-a", "stdin", "-b", next, "-u"]
        # print args
        if compress:
            args = args + ["|", "gzip", ">", output_path]
        else:
            args = args + [">", output_path]
        args = " ".join(args)
        # print args
        system(args)
        return output_path

    def get_n_peaks(self, p):
        is_gzip = p.endswith('.gz')
        stream = gzip.open(p) if is_gzip else open(p)
        return len([line for line in stream])

    def merge_multiple_files(self, bed_paths, **kwargs):
        '''
        Intersect multiple peaks files using AND or OR
        (default: AND)
        :return:
        '''

        lines = [line for bed_path in bed_paths for line in gzip.open(bed_path)]
        tmpinput = tempfile.mkstemp()[1]
        with open(tmpinput, 'w') as writer:
            for line in lines:
                writer.write(line)
        writer.close()

        # sort file before starting
        tmpinputsorted = tempfile.mkstemp()[1]
        sort_args = ["sort", "-k1,1", "-k2,2n", tmpinput, ">", tmpinputsorted]
        # print '# sorting before running...'
        # print " ".join(sort_args)
        system(" ".join(sort_args))
        compress = kwargs.get("compress", False)
        output_path = kwargs.get("output_path", None)

        bedtools_path = "/usr/bin/bedtools"
        if not exists(bedtools_path):
            bedtools_path = "/g/software/bin/bedtools"
        bedtools_path = bedtools_path if exists(bedtools_path) else 'bedtools'

        if output_path is None:
            output_path = tempfile.mkstemp()[1]
        # default: this is the intersection between peak datasets
        args = [bedtools_path, "merge", "-i", tmpinputsorted]
        # print args
        if compress:
            args = args + ["|", "gzip", ">", output_path]
        else:
            args = args + [">", output_path]
        args = " ".join(args)
        system(args)
        # print args
        return output_path




