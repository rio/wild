'''
Created on Feb 13th, 2017

Implemented to load and score sequences using by Ling et al. MLR

@author: ignacio
'''

import pandas as pd
from scipy.stats import pearsonr
import numpy as np

class MLR():
    def __init__(self, mlr_means_path=None, intercept=None, coefficients=None, ids=None):

        if mlr_means_path is not None:
            rows = [r for r in open(mlr_means_path)]
            assert rows[0].startswith('Intercept')
            self.intercept = float(rows[0].split("\t")[1])
            self.a = [float(r.split("\t")[1]) for r in rows[1:]]
            self.ids = [r.split("\t")[0] for r in rows[1:]]
        else:
            self.intercept = intercept
            self.a = coefficients
            self.ids = ids

        self.df = pd.DataFrame([[idi, ai] for idi, ai in zip(self.ids, self.a)],
                               columns=['ID', 'coefficient'])

    def get_one_hot_encoding(self, seq):
        return [int(ci == k) for ci in seq for k in ['A', 'C', 'G', 'T']]

    def get_y_from_x_dataframe(self, x_df, positions=None):
        return [self.get_y(xi.values) for i, xi in x_df.iterrows()]

    def get_y(self, x, positions=None):
        '''
        :param x: a vector with all values that will be used for scoring
        :return:
        '''

        if len(self.a) != len(x):
            print(len(self.a), len(x))
            assert len(self.a) == len(x)

        if positions is None:
            score = sum([ai * xi for ai, xi, k in zip(self.a, x, self.ids)]) + self.intercept
        else:
            score = sum([self.a[i] * x[i] for i in positions]) + self.intercept
        return score

    def print_n_features(self):
        for second_order in (False, True):
            for k in ['A', 'C', 'T', 'G', 'MGW', 'HELT', 'ROLL', 'PROT']:
                n = len([c for c in self.ids if
                         ((c.startswith(k) and not "x" in c and not second_order) or
                          ((c.startswith(k) and "x" in c and second_order)))])
                if n > 0:
                    print("#", k, '1st-order' if not second_order else '2nd-order',\
                        len([c for c in self.ids if
                             ((c.startswith(k) and not "x" in c and not second_order) or
                            ((c.startswith(k) and "x" in c and second_order)))]))
        print('# features', len(self.a))

    def get_best_sequence(self):
        '''
        Given a MLR, get its best sequence according to all presented features
        :return: a string with the best nucleotides
        '''
        best_s = ''
        seq_length = len([ci for ci in self.ids if ci[0] == "T" and ci[-1].isdigit()])
        for i in range(seq_length):
            options = [[nt[0], coefficient] for nt, coefficient in zip(self.ids, self.a)
                       if nt[0] in {"A", "T", "C", "G"} and nt[-1].isdigit() and int(nt[1:]) - 1 == i]
            best_s += sorted(options, key=lambda x: -x[-1])[0][0]

        return best_s

    @staticmethod
    def get_sequence_coeffs_correlation(mlr1, mlr2):
        '''
        Given two MLR-models, one simple and one augmented (or two simple, or two augmented)
        return thw PCC between the sequence coefficients
        :param mlr1:
        :param mlr2:
        :return:
        '''
        coeffs1 = [ai for ai, k in zip(mlr1.a, mlr1.ids)
                   if k[0] in {'A', "C", 'G', "T"}]
        coeffs2 = [ai for ai, k in zip(mlr2.a, mlr2.ids)
                   if k[0] in {'A', "C", 'G', "T"}]
        return pearsonr(coeffs1, coeffs2)

    def get_n_sequence_features(self):
        return len([ai for ai, k in zip(self.a, self.ids)
                    if k[0] in {'A', "C", 'G', "T"}])

    def get_best_mlr_score(self, sequences):
        '''
        Given a sequence, break it down into all it sequence components and
        calculate the best score across all conformations. Return the best
        position, direction, sequence and score
        :param sequence:
        :return:
        '''

        # the amount of features always are N * 4 the amount of nucleotide positions
        w = self.get_n_sequence_features() / 4
        best_scores = []
        for i, s in enumerate(sequences):
            if i % 100 == 0:
                print(i, len(sequences))
            slicing_coordinates = []
            for xi in range(len(s) - w + 1):
                start = xi
                end = start + w
                slicing_coordinates.append([start, end, s[start: end], '+'])
                slicing_coordinates.append([start, end, self.get_complementary_seq(s[start: end]), '-'])

            scores = [t + [self.get_y(self.get_one_hot_encoding(t[2]))]
                      for t in slicing_coordinates]
            best_scores.append(sorted(scores, key=lambda x: x[-1])[-1])
        return best_scores

    def get_complementary_nt(self, nt):
        if nt == "A":
            return "T"
        if nt == "T":
            return "A"
        if nt == "C":
            return "G"
        if nt == "G":
            return "C"
        if nt == "N":
            return "N"

    def get_complementary_seq(self, s):
        return "".join([self.get_complementary_nt(nt) for nt in s][::-1])