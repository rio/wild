'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *

class SCAtlasMouse:

    def __init__(self):
        self.basedir = '/g/scb2/zaugg/rio/data/atlas.gs.washington'
        self.df = self.get_da_peaks()
        self.mdata = self.get_metadata()
        self.cluster_ids, self.subcluster_ids = self.get_cluster_names()
        self.cell_labels = set(self.mdata['cell_label'])


    def get_da_peaks(self):
        if hasattr(self, 'df'):
            return self.df
        df = DataFrameAnalyzer.read_tsv(join(self.basedir, 'atac_matrix.binary.da_results.txt'))
        return df

    def get_cluster_names(self):
        if hasattr(self, 'cluster_ids'):
            return self.cluster_ids, self.subcluster_ids
        cluster_ids = set()
        subcluster_ids = set()
        print('grouping clusters and subclusters')
        for cluster, grp in self.df.groupby('cluster'):
            cluster_ids.add(cluster)
            for subcluster, grp2 in grp.groupby('subset_cluster'):
                subcluster_ids.add(str(cluster) + "_" + str(subcluster))
        return cluster_ids, subcluster_ids

    def get_da_peaks_cluster(self, cluster):
        return self.df[self.df['cluster'] == cluster]

    def get_da_peaks_by_subcluster(self, cluster, subcluster):
        df = self.df[self.df['cluster'] == cluster]
        return df[df['subset_cluster'] == subcluster]

    def get_metadata(self):
        if hasattr(self, 'mdata'):
            return self.mdata
        return DataFrameAnalyzer.read_tsv(join(self.basedir, 'cell_metadata.txt'))

    def get_cells_by_label(self, cell_label):
        return self.mdata[self.mdata['cell_label'] == cell_label]