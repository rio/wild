'''
Created on Jan 29, 2016

This is a facade to prepare and run bsub queries

@author: ignacio
'''
import sys
import tempfile
from os import makedirs, system
from os.path import basename, join, exists
import argparse
import subprocess


class BSubFacade:

    def get_user_input(self):
        user_input = int(sys.argv[1]) if len(sys.argv) > 1 else None
        return user_input

    def create_job_array_script_slurm(self, job_name,
                                slurm_log_dir="/scratch/rio/bsub_output/", **kwargs):
        '''
        This script assumes that we have a script (job_basename) that will
        give a particular output. This script will be called with a counter as
        argument, as many times as the n_jobs variable indicates.

        The src are saved in the tmp directory

        :param job_name:
        :return:
        '''

        n_jobs_max = kwargs.get("n_jobs_max", None)
        n_jobs = kwargs.get("n_jobs", 100)
        mem = kwargs.get("mem", 10000)
        n_group = kwargs.get('ngroup', 1)
        queue = kwargs.get('queue', '1day')
        time = kwargs.get('time', None) # '1:00:00' str
        qos = kwargs.get('qos', 'normal')
        ncores = kwargs.get('ncores', 1)
        python_bin = kwargs.get('python_bin', 'python')

        commands = kwargs.get("commands", None)
        # print commands
        # make an output directory by the date of the
        # day we created the job

        # print job_namee
        JOB_NAME = job_name

        s = """#!/bin/bash
#SBATCH -J """ + JOB_NAME + """
#SBATCH --mem=""" + str(mem) + """
#SBATCH --qos=""" + qos + """
#SBATCH --nice=10000
#SBATCH -p """ + queue + """
""" + (("#SBATCH -t " + time) if time is not None else '') + """ 
#SBATCH -n """ + str(ncores) + (("""
#SBATCH --cpus-per-task   3         # number of CPUs per task
#SBATCH --gres=gpu:1                # number of GPUs requested""") if queue == 'gpu' else '') + """
#SBATCH -o  """ + slurm_log_dir + JOB_NAME + "_output_%a.txt" + """
#SBATCH -e  """ + slurm_log_dir + JOB_NAME + "_error_%a.txt" + """

echo "STARTING JOB"
""" + kwargs.get('preexec', '') +\
            kwargs.get('module', '') +\
            ("%s """ % python_bin + JOB_NAME + ".py ${SLURM_ARRAY_TASK_ID}" +
             (" --ngroup " + str(n_group) if commands is None else " " + " ".join(commands))) + """
echo "FINISHED JOB"

        """

        if not exists(slurm_log_dir):
            makedirs(slurm_log_dir)

        writer = open(JOB_NAME + "_array_slurm.sh", "w")
        writer.write(s)
        writer.write("\n")
        s = '# (SLURM):\n#Please submit your script with the following command...\n'
        n_jobs_max_str = "" if n_jobs_max == None else "%" + str(n_jobs_max)

        # do not remove the initial hash symbol of this comment, otherwise you will
        # run this code recursively
        bsub = '# sbatch ' + "--array=" +\
               str(kwargs.get('start', str(1))) +\
               "-" + str(n_jobs) + n_jobs_max_str + " " + job_name + "_array_slurm.sh"
        bsub += ' > ' + job_name + '_array_slurm.log'
        s += bsub
        print('')
        print((s.replace("# ", "")))
        print('Alternatively, run the following command')
        sbatch_filename = 'sbatch_' + job_name + '.sh'
        print(('./%s' % sbatch_filename + "\n"))
        writer_cmd = open('sbatch_' + job_name + '.sh', 'w')
        writer_cmd.write(bsub.replace("# ", "") + "\n")
        slurm_log_filename = job_name + '_array_slurm.log'
        writer_cmd.write('cat ' + slurm_log_filename + '\n')
        writer_cmd.close()
        system('chmod +x %s' % sbatch_filename)
        writer.write(s + "\n")
        writer.close()

        # make the scancel job
        scancel_filename = 'scancel_%s.sh' % job_name
        writer = open(scancel_filename, 'w')
        writer.write("jobid=`cat %s " % slurm_log_filename + "| sed -e 's/Submitted batch job //g'`\n")
        writer.write("scancel $jobid\n")
        writer.write('echo \"The following ID was cancelled...\"\n')
        writer.write('echo $jobid\n')
        writer.close()
        system('chmod +x %s' % scancel_filename)


    def create_job_array_script(self, job_name,
                                bsub_output_dir="/scratch/rio/bsub_output/",
                                **kwargs):
            '''
            This script assumes that we have a script (job_basename) that will
            give a particular output. This script will be called with a counter as
            argument, as many times as the n_jobs variable indicates.

            The src are saved in the tmp directory

            :param job_name:
            :return:
            '''

            n_jobs_max = kwargs.get("n_jobs_max", None)
            n_jobs = kwargs.get("n_jobs", 100)
            mem = kwargs.get("mem", 10000)
            n_group = kwargs.get('ngroup', 1)

            commands = kwargs.get("commands", None)
            # print commands
            # make an output directory by the date of the
            # day we created the job

            # print job_name
            JOB_NAME = job_name

            s = """#!/bin/bash
        #BSUB -J """ + JOB_NAME + """
        #BSUB -M """ + str(mem) + """ -R "rusage[mem=""" + str(mem) + """] span[hosts=1]"
        #BSUB -n 1
        #BSUB -o  """ + bsub_output_dir + JOB_NAME + "_output_%I.txt" + """
        #BSUB -e  """ + bsub_output_dir + JOB_NAME + "_error_%I.txt" + """

        echo "I, ${USER}, am running on host ${HOSTNAME}"

        echo "STARTING JOB"
        """ + (
            "python """ + JOB_NAME + ".py ${LSB_JOBINDEX} --ngroup " + str(n_group) if commands is None else "\n".join(
                commands)) + """
        echo "FINISHED JOB"

                """

            if not exists(bsub_output_dir):
                makedirs(bsub_output_dir)

            writer = open(JOB_NAME + "_array_bsub.sh", "w")
            writer.write(s)
            writer.write("\n")
            s = '# (BSUB):\n#Please submit your script with the following command...\n'
            n_jobs_max_str = "" if n_jobs_max == None else "%" + str(n_jobs_max)

            # do not remove the initial hash symbol of this comment, otherwise you will
            # run this code recursively
            bsub = '# bsub -J \"myArray[1-' + str(
                n_jobs) + ']' + n_jobs_max_str + '\" < ' + job_name + "_array_bsub.sh\n"
            s += bsub
            print('')
            print(( s.replace("# ", "")))
            print('Alternatively, run the following command')
            print('./submit_job_bsub.sh')
            writer_cmd = open('submit_job_bsub.sh', 'w')
            writer_cmd.write(bsub.replace("# ", ""))
            writer_cmd.close()
            system('chmod +x submit_job_bsub.sh')
            writer.write(s)
            writer.close()

    @staticmethod
    def create_job(job_name, n_jobs=1, offset=0,
                   bsub_output_dir='/scratch/rio/bsub_output', **kwargs):
        '''
        This script assumes that we have a script (job_basename) that will
        give a particular output. This script will be called with a counter as
        argument, as many times as the n_jobs variable indicates.

        The src are saved in the tmp directory

        :param job_name:
        :param n_jobs:
        :return:
        '''

        JOB_NAME = job_name
        # print join(bsub_output_dir, basename(JOB_NAME) + "_output_$X.txt")
        cmd = kwargs.get('cmd', None)
        python_bin = "/g/scb/zaugg/rio/miniconda2/bin/python"

        s = """#!/bin/bash
#BSUB -J """ + JOB_NAME + """
#BSUB -M 5000 -R "rusage[mem=5000] span[hosts=1]"
#BSUB -n 1
#BSUB -o  """ + join(bsub_output_dir, basename(JOB_NAME) + "_output_$X.txt") + """
#BSUB -e  """ + join(bsub_output_dir, basename(JOB_NAME) + "_error_$X.txt") + """

echo "I, ${USER}, am running on host ${HOSTNAME}"

echo "STARTING JOB"
""" + (("""cd ../../src
""" + python_bin + """ """ + JOB_NAME + ".py" + """ $X
cd ../tmp/bsub
""") if cmd is None else\
            ("\n".join([python_bin + " " + cmd]) if isinstance(cmd, str)
             else "\n".join([python_bin + " " + cmdi for cmdi in cmd]) + "\n")) + """

echo "FINISHED JOB"

# Submit with bsub < BSUB_TEMPLATE.sh
# Check status with bjobs
# Check output during run with bpeek JOBID

        """
        script_path = None
        for file_i in [str(i + offset) for i in range(0, n_jobs)]:
            script_path = join(bsub_output_dir, JOB_NAME + "_" + file_i + ".sh")
            writer = open(script_path, "w")
            new_s = s.replace("$X", file_i)
            writer.write(new_s)
            writer.write("\n")
            writer.close()
            # print new_s
        return script_path

    @staticmethod
    def prepare_slurm_jobarray_script(script_path, function, **kwargs):
        job_name = basename(script_path).replace(".py", "")
        bsub_facade = BSubFacade()
        user_input = kwargs.get('jobid', bsub_facade.get_user_input())
        if user_input is None:
            bsub_output_dir = kwargs.get('slurm_log_dir', '/scratch/rio/')
            # print bsub_output_dir
            if not exists(bsub_output_dir):
                makedirs(bsub_output_dir)
            i = 0
            while not exists(bsub_output_dir):
                bsub_output_dir = "../" + bsub_output_dir
                print((bsub_output_dir, exists(bsub_output_dir)))
                if exists(bsub_output_dir):
                    break
                i += 1
                if i > 2:
                    break
            bsub_facade.create_job_array_script_slurm(job_name, **kwargs)
        else:
            run = kwargs.get('run', False)
            if run:
                print(("running job\nUser input", user_input))
                function(user_input - 1, **kwargs)

    @staticmethod
    def prepare_bsub_jobarray_script(script_path, function, **kwargs):
        job_name = basename(script_path).replace(".py", "")
        bsub_facade = BSubFacade()
        user_input = kwargs.get('jobid', bsub_facade.get_user_input())
        jobs_log_dir = kwargs.get('slurm_log_dir', None)
        assert jobs_log_dir is not None
        if user_input is None:
            bsub_output_dir = kwargs.get('bsub_output_dir', '/scratch/rio')
            slurm_output_dir = kwargs.get('slurm_output_dir', bsub_output_dir)
            i = 0
            while not exists(bsub_output_dir):
                bsub_output_dir = "../" + bsub_output_dir
                print((jobs_log_dir, exists(bsub_output_dir)))
                if exists(bsub_output_dir):
                    break
                i += 1
                if i > 2:
                    break
            if not exists(jobs_log_dir):
                bsub_facade.create_job_array_script(job_name, **kwargs)
            else:
                bsub_facade.create_job_array_script(job_name, **kwargs)
        else:
            run = kwargs.get('run', False)
            if run:
                print(("running job\nUser input", user_input))
                function(user_input - 1, **kwargs)

    @staticmethod
    def prepare_multifunction_nohup(script_path, n_jobs, n_cores, software='python', reverse=True,
                                    args=''):
        print((n_jobs, n_cores, n_jobs / n_cores))
        commands = []
        for n in range(n_jobs / n_cores):
            n = n * n_cores
            start = n + 1
            end = start + (n_cores - 1)
            if end + n_cores > n_jobs:
                end = n_jobs
            s = '''nohup sh -c \'for i in {%i..%i}; do echo $i; %s %s $i %s; done > /tmp/nohup_output_$i\' &''' %\
                (start if not reverse else end, end if not reverse else start, software, script_path, args)
            commands.append(s)

        output_cmd = script_path.replace(".py", '_nohup_submission.sh')
        writer = open(output_cmd, 'w')
        for s in commands:
            print (s)
            writer.write(s + "\n")
        writer.close()
        system('chmod +x %s' % output_cmd)
        print ('execute by running')
        print(('./%s'  % output_cmd))

    @staticmethod
    def get_n_running():
        print ('# running?')
        cmd = 'sacct | grep RUNNING | grep -v extern | wc -l'
        result = subprocess.check_output(cmd, shell=True)
        print(('# jobs RUNNING %i' % int(result)))
        return int(result)

    @staticmethod
    def get_n_running_and_pending():
        return BSubFacade.get_n_running(), BSubFacade.get_n_pending()

    @staticmethod
    def get_n_pending():
        print ('# pending?')
        cmd = 'sacct | grep PENDING | grep -v extern | wc -l'
        result = subprocess.check_output(cmd, shell=True)
        print(('# jobs PENDING %i' % int(result)))
        return int(result)

    @staticmethod
    def get_n_files(directory, fmt):
        cmd = 'ls -ltrsh %s ' % directory + ' | grep \"%s\" | wc -l' % fmt
        result = subprocess.check_output(cmd, shell=True)
        return int(result)


    @staticmethod
    def prepare_jobarray_scripts(script_path, function, **kwargs):
        job_name = basename(script_path).replace(".py", "")
        bsub_facade = BSubFacade()
        parser = argparse.ArgumentParser(description='Process arguments')
        parser.add_argument('jobid', type=int,
                            help='an integer for the accumulator', default=-1)
        parser.add_argument('--ngroup', '-g', default=None, type=int,
                            help='amount to run per group')
        parser.add_argument('--time', '-t', default=None, type=str,
                            help='amount to run per group')
        parser.add_argument('--njobsmax', '-j', default=None, type=str,
                            help='max jobs to run at the same time')
        parser.add_argument('--qos', '-q', default=None, type=str,
                            help='max jobs to run at the same time')
        parser.add_argument('--run', default=1, type=int,
                            help='amount to run per group')
        parser.add_argument('--query', default=None, type=str,
                            help='specific text to be used for selection when analyzing files')
        parser.add_argument('--j', default=None, type=int,
                            help='specific subindex for a given jobid (e.g. kfold i, replicate, etc.)')
        parser.add_argument('--debug', default=0, type=int,
                            help='Specify if running with debug parameters (verbose, test files)')

        args = parser.parse_args()

        if args.ngroup == None:
            args.ngroup = kwargs.get('ngroup', None)
            if 'ngroup' in kwargs:
                del kwargs['ngroup']
        elif args.ngroup is not None and kwargs.get('ngroup', None) is not None:
            if 'ngroup' in kwargs:
                del kwargs['ngroup']

        if args.time is not None:
            kwargs['time'] = args.time
        if args.njobsmax is not None:
            kwargs['n_jobs_max'] = args.njobsmax
        if args.qos is not None:
            kwargs['qos'] = args.qos

        if args.jobid <= 0:
            if kwargs.get('bsub', False):
                BSubFacade.prepare_bsub_jobarray_script(script_path, function, run=False,
                                                        jobid=None, ngroup=args.ngroup, **kwargs)
            BSubFacade.prepare_slurm_jobarray_script(script_path, function, run=False,
                                                     jobid=None, ngroup=args.ngroup, **kwargs)
        else:
            print(("running job\nUser input", args.jobid))
            function(args.jobid - 1, ngroup=args.ngroup, run=args.run,
                     query=args.query, debug=args.debug, j=args.j, **kwargs)






