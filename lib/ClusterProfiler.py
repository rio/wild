'''
Created on 6/15/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *


class ClusterProfiler:
    @staticmethod
    def run_clusterProfiler(fg, bg, species='mouse', remove_geneid=True):
        import rpy2
        import rpy2.robjects as robjects
        from rpy2.robjects import pandas2ri
        from rpy2.robjects.vectors import FloatVector, StrVector
        pandas2ri.activate()  # to convert pandas to R dataframe
        # if you're running with miniconda'R in the cluster this method will fail. Use the BASH version instead that calls
        # slurm's Rscript
        r = rpy2.robjects.r
        print(len(fg), len(bg))
        r.source('/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/run_clusterProfiler.R')
        print('calling main function...')
        res = r['run_clusterProfilerGO'](StrVector(list(fg)), StrVector(list(bg)), species=species, remove_geneid=remove_geneid)
        res = pandas2ri.ri2py(res)
        if res.shape[0] == 0:
            return None
        return res

    # this version has to be run by default
    @staticmethod
    def run_clusterProfiler_bash(fg, bg, species='mouse', remove_geneid=True, run_as_script=True, **kwargs):
        fg_path = tempfile.mkstemp()[1]
        bg_path = tempfile.mkstemp()[1]

        output_path = tempfile.mkstemp()[1] if kwargs.get('output_clusterProf') is None else kwargs.get('output_clusterProf')

        DataFrameAnalyzer.to_tsv_gz(pd.DataFrame(list(fg)), fg_path)
        DataFrameAnalyzer.to_tsv_gz(pd.DataFrame(list(bg)), bg_path)

        # bin = '/g/easybuild/x86_64/CentOS/7/nehalem/software/R/3.4.3-foss-2017b-X11-20171023/bin/Rscript'
        bin = 'Rscript' # '/g/scb2/zaugg/rio/miniconda2/bin/Rscript'
        rscript_path = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/run_clusterProfiler_bash.R'
        print(bin)
        if run_as_script:
            bin = '/g/easybuild/x86_64/CentOS/7/haswell/software/R/3.5.1-foss-2017b-X11-20171023/bin/Rscript'
        cmd = ' '.join([bin, rscript_path, '--fg', fg_path, '--bg', bg_path, '--species', species, '--output', output_path])
        print(cmd)
        if run_as_script:
            bash_path = tempfile.mkstemp()[1] if kwargs.get('bash_path_clusterProf') is None else kwargs.get('bash_path_clusterProf')
            writer = open(bash_path, 'w')
            lines = ['module load R', 'module load GLib', 'which R', 'which Rscript', cmd, 'module unload R']
            writer.writelines("\n".join(lines) + "\n")
            writer.close()
            print('run script')
            system('chmod +x ' + bash_path)
            print(exists(bash_path), bash_path)
            print('skip?', kwargs.get('skip'))
            if kwargs.get('skip') is not None:
                pass
            else:
                system("sh " + bash_path)
            # remove(bash_path)
        else:
            system(cmd)

        if kwargs.get('skip') is None or exists(output_path):
            res = DataFrameAnalyzer.read_tsv_gz(output_path)
            return res