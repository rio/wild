'''
Created on Jun 8, 2016

@author: ignacio
'''
from numpy import average, exp
from math import log
from numpy.random import normal

class NonConsensusBindingAnalyzer:
    def get_non_consensus_score_two_states(self, s, k):
        """
        Calculate the score according to a two states model, for a given
        sequence binding to a long sequence at random position, with two
        possible states 1 (A/T), or 0 (C/G).
        """
        u = 0
        for i in range(0, len(s) - k + 1):
            si = s[i: i + k]
            score = sum([1 if nt == "C" or nt == "G" else 0 for nt in si])
            u += score
        return u
    
    def get_F_score_average(self, dna_sequence, n_replicates=250,
                            M=8):
        """
        Assuming a random binder with KA, KT, KC and KG values obtained
        from Gaussian probability distributions, calculate the average F value by
        individually calculating the F score n_replicates times
        """
        
        scores = []
        for i in range(n_replicates):
            f_score = self.get_f_score(dna_sequence, M=M)
            scores.append(f_score)
        return average(scores)
    
    def get_f_score_average_c(self, dna_sequences_path, M=8, n_replicates=250):
        """
        Get the F-score for a set of sequences, using a C script
        """
        binary_path = " ".join([dna_sequences_path, "-m", str(M), "-N", str(n_replicates)]) 
        print(binary_path)
    
    def get_f_score(self, dna_sequence, M=8):
        """
        Calculate the F score for a DNA binder
        """ 
        
        # P(Ki) = 2 * kb * T
        # where
        #     kb = 0.0019872041 kcal / mol
        #     T = 298
        kbT = 0.0019872041 * 298 
        mean, std = 0.0, 2 * kbT
        
        # define random constants using the random values from a gaussian distribution
        KA, KT, KC, KG = [normal(mean, std) for i in range(4)]
        score_by_nt = {k: v for k, v in zip("ATCG", [KA, KT, KC, KG])}
        
        u_scores = []
        L = len(dna_sequence)
        for i in range(L - M + 1):
            u = 0.0
            s = dna_sequence[i : i + M]
            
            for j in range(len(s)):
                if s[j] in score_by_nt:
                    u += score_by_nt[s[j]]
            u_scores.append(-u)
        
        # calculate Z 
        Z = sum([exp(-u / (kbT)) for u in u_scores])
        F = -kbT * log(Z)
        return F
    

    def test(self):
        #===============================================================================
        # Test: check for sequences that are reported in the publication as a test case 
        #===============================================================================
        sequences_weak = ["TATCTACGACTACATCACTGACATACGAGTACCAAC",
                          "AGGTTGCCGCTCTGAGCCGTGCGTCTGCGCTGCGTC",
                          "ATGCAAGGAGTAACGGCTGAAGTCGAGATCGAGAGC",
                          "CCGTACCGCGGATCGGCATCTCGGCACGACGCATGG",
                          "ACGGCACTAGGCATGCATCGACCAGTAAGACATCGG"]
        sequences_strong = ["TTTACTGTACCCCCCCATCCGTGGGCGGTAAAATAA",
                            "GGAGGGGATTCTTTATCCAGCCCCCCCGATACGAAA",
                            "CCACCCCAGGTGGGAGTTACTTTTTTTCAGATCAAA",
                            "TTATTTTCCTGAACAAAAACGAGGGCGCGACCCCAC",
                            "TAAAATTAGGATTGCCGGAGGTGCCCCCTTTTTGTG"]
        scores = []
        import matplotlib.pyplot as plt
        print("High Free Energy")
        for seq in sequences_weak:
            print(seq, self.get_F_score_average(seq))
        print("Low Free Energy")
        for seq in sequences_strong:
            print(seq, self.get_F_score_average(seq))
            
        plt.hist([self.get_F_score_average(s) for s in sequences_weak], color="b")
        plt.hist([self.get_F_score_average(s) for s in sequences_strong], color="r")
        plt.xlabel("Free energy (kbT)")
        plt.ylabel("Events")
        plt.show()

if __name__ == "__main__":
    non_consensus_binding = NonConsensusBindingAnalyzer()
    non_consensus_binding.test()