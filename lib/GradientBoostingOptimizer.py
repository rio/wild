'''
Created on May 31, 2016

@author: ignacio
'''

import numpy as np
import sklearn
from numpy import average, median, concatenate, append, reshape
from random import shuffle, randint
from sklearn import ensemble
from sklearn.metrics import mean_squared_error, precision_recall_curve, auc
from sklearn import datasets
from lib.pyroc import ROCData
import matplotlib.pyplot as plt
from scipy.stats.mstats_basic import linregress
import sys
from lib.path_functions import *
import pandas as pd
from lib.DiMOAnalyzer import DiMOAnalyzer
from lib.SNPAnalyzer import SNPAnalyzer
from lib.ShapeAnalyzer import ShapeAnalyzer
from lib.FastaAnalyzer import FastaAnalyzer
from lib.MotifConverter import MotifConverter
from lib.MachineLearning import MachineLearning
from lib.BindingSitesAnalyzer import BindingSitesAnalyzer
from xgboost import XGBClassifier
from sklearn.metrics import roc_auc_score
import numpy as np
import pickle
from sklearn.model_selection import KFold

class GradientBoostingOptimizer:

    def calculate_scores_and_fit(self, seqs_pos, seqs_neg, pwm_path, working_dir,
                                 file_basename, bp_flanking=4, train_test_ratio=9,
                                 use_dimo_pwm=False):
        '''
        Perform the pipeline for calculating + and negative scores of DiMO optimization, PWM annotation and
        prediction of binding sites (HOMER), shape annotation, and Motif results
        :param fasta_pos:
        :param fasta_neg:
        :param pwm_path:
        :return:
        '''
        if use_dimo_pwm:
            homer_input_pwm_path = self.optimize_homer_pwm_with_dimo(seqs_pos, seqs_neg,
                                                                     pwm_path, working_dir)
        else:
            homer_input_pwm_path = pwm_path

        # calculate features for both random and positive sequences
        working_dir_homer = working_dir
        if not exists(working_dir_homer):
            # print working_dir_homer
            mkdir(working_dir_homer)

        # print 'preparing to run HOMER2 (find routine)...'
        snp_analyzer = SNPAnalyzer()
        homer_input_pwm_path = abspath(homer_input_pwm_path)

        tmp_fasta_fg = abspath(join(working_dir_homer, 'tmp_fasta_fg.fa'))
        tmp_fasta_bg = abspath(join(working_dir_homer, 'tmp_fasta_bg.fa'))
        FastaAnalyzer.write_fasta_from_sequences(seqs_pos, tmp_fasta_fg)
        FastaAnalyzer.write_fasta_from_sequences(seqs_neg, tmp_fasta_bg)


        df, df_random = [snp_analyzer.get_scores_homer(seqs, homer_input_pwm_path,
                                                       as_dataframe=True,
                                                       get_best_sequence=True,
                                                       working_dir=working_dir_homer,
                                                       tmp_fasta=tmp_fasta,
                                                       log=False)
                         for seqs, tmp_fasta in zip([seqs_pos, seqs_neg], [tmp_fasta_fg, tmp_fasta_bg])]

        # add BOUND/UNBOUND labels
        print('adding labels...')
        df.insert(0, 'bound', np.repeat([1], df.shape[0], axis=0))
        df_random.insert(0, 'bound', np.repeat([0], df.shape[0], axis=0))

        # get flanking sequence
        print('adding flakning sequences...')
        print('pos...')
        df['flanking'] = snp_analyzer.get_flanking_regions_home_motif(df, bp_flanking)
        print('neg...')
        df_random['flanking'] = snp_analyzer.get_flanking_regions_home_motif(df_random, bp_flanking)

        print(df.head())
        # filter out sequences that do not have the length motif + flanking * 2
        print('filtering out by flanking sequences...')
        motif_width = snp_analyzer.get_motif_width_pfm_homer(pwm_path)

        # concat both tables into unique
        df['y'] = 1.0
        df_random['y'] = 0.0

        df = pd.concat([df, df_random])
        df = df.sample(frac=1).reset_index(drop=True)
        y = df['y']
        del df['y']

        shape_analyzer = ShapeAnalyzer()
        df_shapes = shape_analyzer.get_shapes_from_DNAShapeR(list(df['motif']), infer_flanks=False,
                                                             normalize=1, overwrite=True,                                                             output_prefix=join(working_dir, file_basename + ".shapes"))
        df = pd.concat([df[['bound', 'score']].reset_index(drop=True), df_shapes], axis=1)

        fg_df = df[df['bound'] == 1]
        bg_df = df[df['bound'] == 0]
        del fg_df['bound']
        del bg_df['bound']

        print(fg_df.shape)
        print(bg_df.shape)

        # combine features
        res = None
        for label in ['seq', 'seq+shape']:

            print(label)
            fg = np.array([[score] + (list(v) if 'seq+shape' in label else [])
                           for score, v in
                           zip(fg_df['score'], [r.values for ri, r in fg_df[fg_df.columns[1:]].iterrows()])])
            bg = np.array([[score] + (list(v) if 'seq+shape' in label else [])
                           for score, v in
                           zip(bg_df['score'], [r.values for ri, r in bg_df[bg_df.columns[1:]].iterrows()])])
            print(fg.shape)
            print(bg.shape)
            X = np.concatenate([fg, bg])
            y = np.array(y)
            next_res = MachineLearning.KFoldGradientBoostingClassifier(X, y)
            next_res['model'] = label
            res = next_res if res is None else pd.concat([res, next_res])

        for model, grp in res.groupby('model'):
            print(model, grp.median())
        print(res)

    def optimize_homer_pwm_with_dimo(self, seqs_pos, seqs_neg, pwm_path, working_dir,
                                     output_path, motif_format='HOMER', **kwargs):
        '''
        Give a set of sequences, optimize using DiMO. Return a path to the optimized
        PWM
        :param seqs_pos:
        :param seqs_neg:
        :param pwm_path:
        :param working_dir:
        :return:
        '''
        # print pwm_path
        ppm_id = basename(pwm_path).replace('.ppm', "").replace(".pfm", "")

        # In this step we decide if optimizing our PWM with dimo
        # print 'running DiMO'
        if not exists(working_dir):
            mkdir(working_dir)

        ppm_path_dimo = join(working_dir, ppm_id + ".ppm")
        # print 'PWM DiMO exists:', exists(ppm_path_dimo)
        output_dimo_basename = "output_TF"

        dimo_analyzer = DiMOAnalyzer()
        print(output_path, exists(output_path))

        overwrite = kwargs.get('overwrite', False)
        if kwargs.get('overwrite') or not exists(output_path):
            print(overwrite, 'dimo')
            dimo_analyzer.convert_ppm_homer_to_dimo(pwm_path,
                                                    ppm_path_dimo)

            # print working_dir
            # print ppm_path_dimo
            dimo_analyzer.optimize_ppm(seqs_pos, seqs_neg,
                                       ppm_path_dimo,
                                       working_dir=working_dir,
                                       output_basename=output_dimo_basename)

        # convert back our optimized
        # print exists(optimized_ppm_jaspar_path)
        dimo_pfm_end_path = join(working_dir, output_dimo_basename + "_END.pfm")
        if exists(dimo_pfm_end_path):
            dimo_analyzer.convert_ppm_dimo_to_homer(dimo_pfm_end_path, output_path, header=ppm_id)
            motif_converter = MotifConverter()
            motif_converter.convert_dimo_to_jaspar(dimo_pfm_end_path, output_path)

        return output_path

    def fit_kfold(self, input_table, plot_simulation=False, feature_labels=None,
            log=False, k_fold=None):
        """
        X is a vector that contains a list of list, with a certain amount of features
        
        Y is the output signal
        """

        k_fold = 5 if k_fold == None else k_fold
        #=======================================================================
        # K-fold cross-validation: divide into subgroups for training and testing
        #=======================================================================
        output_table = []
        for i in range(k_fold):
            print("# k-fold", i + 1, end=' ') 
            # classify using a gradient boosting classifier
            params = {'n_estimators': 1000, 'max_depth': 4, 'min_samples_split': 1,
                      'learning_rate': 0.01}
            clf = ensemble.GradientBoostingClassifier() # **params)
    
            if log:
                print("Shuffling training and testing set...")
            vector_positions = [j for j in range(len(input_table))]
            shuffle(vector_positions)

            # recover the coordinate names after shuffling
            sequence_headers = [input_table[vi][0] for vi in vector_positions]
            
            input_table = [input_table[j] for j in vector_positions] 
                
            # the PWM score and the shape parameters are our X vectors
            X = np.array([t[1:] for t in input_table], dtype=np.float32)

            # CONTROL: optimize using PWM scores only, and no DNA shape features
            # X = np.array([t[2] for t in gradient_boosting_classifier_input_table])
            # the labels (1 or 0) is our Y vector

            y = np.array([t[0] for t in input_table])
            X = X.astype(np.float32)

            # define position for our testing set
            testing_start, testing_end = i * X.shape[0] / k_fold, (i + 1) * X.shape[0] / k_fold
            
            X_train = [xi for xi in X[0:testing_start]]
            X_train += [xi for xi in X[testing_end:]]
            X_train = np.array(X_train)
            
            y_train = append(y[0:testing_start], y[testing_end:])
            X_test, y_test = X[testing_start:testing_end], y[testing_start:testing_end]
            
            is_binary = len({yi for yi in y}) == 2

            # print y_train, len(y_train)
            # print X_train, len(X_train)

            ###############################################################################
            # Load data    
            ###############################################################################
            # Fit regression model
            params = {'n_estimators': len(input_table[0][1:]),
                      'max_depth': 4,
                      'learning_rate': 0.01}
            clf = ensemble.GradientBoostingClassifier(**params)
            if log:
                print("Fitting Gradient Boosting Regressor to data...")

            clf.fit(X_train, y_train)
            mse = mean_squared_error(y_test, clf.predict(X_test))
            if log:
                print("# features", len(X_test[0]))
                print(("MSE: %.4f" % mse))
            
            ###############################################################################
            # Plot training deviance
            # compute test set deviance
            test_score = np.zeros((params['n_estimators'],), dtype=np.float64)
            
            performance_metrics = []
            auprcs = []
            for j, y_pred in enumerate(clf.staged_predict(X_test)):
                test_score[j] = clf.loss_(y_test, y_pred)
                
                if is_binary:
                    precision, recall, thresholds = precision_recall_curve(y_test, y_pred)    
                    auprcs.append(auc(recall, precision))
                    performance_metrics.append(ROCData([[k, yi]  for k, yi in zip(y_test, y_pred)]).auc())
                else:
                    r = linregress(y_test, y_pred)[2]
                    performance_metrics.append(r)
                
            table_predictions = sorted([[label_i, yi_test, yi_pred] for label_i, yi_test, yi_pred in
                     zip(sequence_headers, y, [yi for yi in clf.staged_predict(X)][-1])], key=lambda x: -x[2])
                
            # plt.scatter(y_pred, y_test)
            # plt.show()
        
            if plot_simulation:
                plt.figure(figsize=(12, 6))
                plt.subplot(1, 3, 1)
                plt.title('Deviance')
                plt.plot(np.arange(params['n_estimators']) + 1, clf.train_score_, 'b-',
                         label='Training Set Deviance')
                plt.plot(np.arange(params['n_estimators']) + 1, test_score, 'r-',
                         label='Test Set Deviance')
                plt.legend(loc='lower left')
                plt.xlabel('Boosting Iterations')
                plt.ylabel('Deviance')
                
                ###############################################################################
                # Plot ROC AUC improvement vs. time
                plt.subplot(1, 3, 2)
                plt.plot([j for j in range(len(performance_metrics))], performance_metrics)
                plt.xlabel('Boosting Iterations')
                label = "AUC" if is_binary else "R^2"
                plt.ylabel(label)
                
                ###############################################################################
                # Plot feature importance
                plt.subplot(1, 3, 3)
                feature_importance = clf.feature_importances_
                # plt.plot([xi for xi in range(len(feature_importance))], feature_importance)
                # make importances relative to max importance
                feature_importance = 100.0 * (feature_importance / feature_importance.max())
                sorted_idx = np.argsort(feature_importance)
                pos = np.arange(sorted_idx.shape[0]) + .5
                plt.barh(pos, feature_importance[sorted_idx], align='center')
                if feature_labels != None:
                    plt.yticks(pos, [feature_labels[i] for i in sorted_idx],
                               fontsize=10)
                plt.xlabel('Relative Importance')
                plt.title('Variable Importance')
                plt.show()
                plt.close()
            
            delta_metric = performance_metrics[-1] - performance_metrics[0]
            
            if log:
                if is_binary:
                    print("AUPRC =", auprcs[-1])
                    print("AUROC =", performance_metrics[-1])
                    print("Delta AUC =", delta_metric)

            output_table.append([i + 1, performance_metrics[0],
                                 performance_metrics[-1], delta_metric,
                                 auprcs[0], auprcs[-1], auprcs[-1] - auprcs[0]])

            print(k_fold, performance_metrics[0], performance_metrics[-1], delta_metric,\
                auprcs[0], auprcs[-1],  auprcs[-1] - auprcs[0])

        df = pd.DataFrame([t for t in output_table],
                          columns=['k-fold', 'auc.ref', 'auc.improved', 'delta.auc',
                                   'auprc.ref', 'auprc.improved', 'delta.auprc'])
        return df


    def fit_single(self, input_table, train_test_ratio=0.9):
        # classify using a gradient boosting classifier
        clf = ensemble.GradientBoostingClassifier()  # **params)

        vector_positions = [j for j in range(len(input_table))]
        shuffle(vector_positions)

        # recover the coordinate names after shuffling
        sequence_headers = [input_table[vi][0] for vi in vector_positions]

        input_table = [input_table[j] for j in vector_positions]

        # the PWM score and the shape parameters are our X vectors
        X = np.array([t[1:] for t in input_table], dtype=np.float32)

        # CONTROL: optimize using PWM scores only, and no DNA shape features
        # X = np.array([t[2] for t in gradient_boosting_classifier_input_table])
        # the labels (1 or 0) is our Y vector

        y = np.array([t[0] for t in input_table])
        X = X.astype(np.float32)

        # define position for our testing set
        nrow = X.shape[0]
        testing_start = int(nrow * train_test_ratio / (train_test_ratio + 1))

        X_train = [xi for xi in X[0:testing_start]]
        X_train = np.array(X_train)

        y_train = y[0:testing_start]
        X_test, y_test = X[testing_start:], y[testing_start:]

        is_binary = len({yi for yi in y}) == 2

        # print y_train, len(y_train)
        # print X_train, len(X_train)

        ###############################################################################
        # Load data
        ###############################################################################
        # Fit regression model
        params = {'n_estimators': len(input_table[0][1:]),
                  'max_depth': 4,
                  'learning_rate': 0.01}
        clf = ensemble.GradientBoostingClassifier(**params)
        clf.fit(X_train, y_train)
        mse = mean_squared_error(y_test, clf.predict(X_test))

        ###############################################################################
        # Plot training deviance
        # compute test set deviance
        test_score = np.zeros((params['n_estimators'],), dtype=np.float64)

        performance_metrics = []
        auprcs = []
        for j, y_pred in enumerate(clf.staged_predict(X_test)):
            test_score[j] = clf.loss_(y_test, y_pred)

            if is_binary:
                precision, recall, thresholds = precision_recall_curve(y_test, y_pred)
                auprcs.append(auc(recall, precision))
                performance_metrics.append(
                    ROCData([[k, yi] for k, yi in zip(y_test, y_pred)]).auc())
            else:
                r = linregress(y_test, y_pred)[2]
                performance_metrics.append(r)

        table_predictions = sorted(
            [[label_i, yi_test, yi_pred] for label_i, yi_test, yi_pred in
             zip(sequence_headers, y, [yi for yi in clf.staged_predict(X)][-1])],
            key=lambda x: -x[2])

        # plt.scatter(y_pred, y_test)
        # plt.show()
        delta_metric = performance_metrics[-1] - performance_metrics[0]

        npos = len([yi for yi in y if yi == 1])
        nneg = len([yi for yi in y if yi == 0])
        values = [[npos, nneg, X_train.shape[0], X_test.shape[0], performance_metrics[0],
                           performance_metrics[-1], delta_metric,
                           auprcs[0], auprcs[-1], auprcs[-1] - auprcs[0]]]
        df = pd.DataFrame(values,
                          columns=['n.pos', 'n.neg', 'n.train', 'n.test', 'auc.ref', 'auc.improved', 'delta.auc',
                                   'auprc.ref', 'auprc.improved', 'delta.auprc'])
        return df