'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.ReadMapper import ReadMapper
from lib.SequenceMethods import SequenceMethods
from lib.FeatureCounts import FeatureCounts
class BAMAnalyzer:


    @staticmethod
    def get_coverage_per_position(coordinates_df, bam_path, output_coverages=None, compress_bedtools_out=True):
        bed_path = tempfile.mkstemp()[1]
        DataFrameAnalyzer.to_tsv(coordinates_df, bed_path, header=None)
        if output_coverages is None:
            output_coverages = tempfile.mkstemp()[1]
        cmd = ' '.join(["/g/funcgen/bin/bedtools", 'coverage', '-d', '-a',
                        bed_path, '-b', bam_path, '| gzip' if compress_bedtools_out else '', '>', output_coverages])
        print(cmd)
        system(cmd)
        print('done calculating coverage. Reading dataframe...')
        if compress_bedtools_out:
            m = DataFrameAnalyzer.read_tsv_gz(output_coverages, header=None, columns=['chr', 'start', 'end', 'position', 'counts'])
        else:
            m = DataFrameAnalyzer.read_tsv(output_coverages, header=None, columns=['chr', 'start', 'end', 'position', 'counts'])

        print('converting coordinates into ranges...')
        m = SequenceMethods.parse_range2coordinate(m, ['chr','start','end'], 'coordinate')
        print('removing coverage bkp path...')
        remove(output_coverages)
        print('done...')
        return m

    @staticmethod
    def featureCounts(coordinates, bampath, **kwargs):
        counts_per_peak = FeatureCounts.get_counts(coordinates, bampath, **kwargs)
        counts_per_peak.columns = list(counts_per_peak.columns[:-1]) + ['counts']
        libsize = BAMAnalyzer.get_libsize(bampath)
        counts_per_peak['filename'] = basename(bampath).split(".")[0]
        counts_per_peak['cpm'] = counts_per_peak['counts'] / float(libsize) * 1e6
        return counts_per_peak

    @staticmethod
    def get_coverage_peaks(coordinates_df, bam_path, output_coverages=None):
        bed_path = tempfile.mkstemp()[1]
        DataFrameAnalyzer.to_tsv(coordinates_df, bed_path, header=None)
        if output_coverages is None:
            output_coverages = tempfile.mkstemp()[1]
        cmd =  ' '.join(["/g/funcgen/bin/bedtools", 'coverage', '-a',
                         bed_path, '-b', bam_path, '| gzip >', output_coverages])
        print(cmd)
        system(cmd)
        print(output_coverages)
        m = DataFrameAnalyzer.read_tsv_gz(output_coverages, columns=['chr', 'start', 'end', 'counts', 'length.1', 'length.2', 'factor'])
        m = m[m.columns[:4]]
        m = SequenceMethods.parse_range2coordinate(m, ['chr','start','end'], 'coordinate')
        return m

    @staticmethod
    def sort(bampath):
        output_bam_sort = bampath.replace(".bam", ".sorted.bam")
        args = ["~/zaugglab/rio/miniconda2/bin/samtools", "sort", "-T", tempfile.mkstemp()[1],
                "-o", output_bam_sort, bampath]
        print(" ".join(args))
        cmd = " ".join(args)
        system(cmd)
        return output_bam_sort

    @staticmethod
    def index(bampath):
        output_bam_index = bampath.replace(".bam", ".bam.bai")
        args = ["~/zaugglab/rio/miniconda2/bin/samtools", "index", bampath, output_bam_index]
        print(" ".join(args))
        cmd = " ".join(args)
        system(cmd)

    @staticmethod
    def bedtobam(bedpath, overwrite=False):
        """
        This method is in charge of handling files related to RoadMap, which cannot be used unless those are bam
        :param bedpath:
        :param overwrite:
        :return:
        """
        hg19 = '/g/scb2/zaugg/zaugg_shared/annotations/hg19/referenceGenome/hg19.fa.fai'
        output_bam = bedpath.replace(".bed.gz", '.bam').replace(".bed", '.bam')

        print(exists(output_bam), output_bam)
        if not exists(output_bam):
            cmd = ' '.join(['bedtools' , 'bedtobam', '-i', bedpath, '-g', hg19, '>', output_bam])
            print(cmd)
            system(cmd)
        else:
            print(exists(output_bam), output_bam)
        return output_bam

    @staticmethod
    def sort_and_index(bampath):
        sorted_bam_path = BAMAnalyzer.sort(bampath)
        BAMAnalyzer.index(sorted_bam_path)

    @staticmethod
    def get_libsize(bampath):
        return ReadMapper.get_libsize(bampath)
