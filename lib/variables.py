from .path_functions import *

# PDBe base urls
# full list available from http://www.ebi.ac.uk/pdbe/api/doc/
base_url = 'http://www.ebi.ac.uk/'
pde_url = base_url + 'pdbe/'
api_base_pdbe = pde_url + 'api/'
mappings_url = api_base_pdbe + 'mappings/'
molecules_url = api_base_pdbe + 'pdb/entry/molecules/'
summary_url = api_base_pdbe + 'pdb/entry/summary/'
ligand_summary_url = api_base_pdbe + 'pdb/compound/summary/'

# custom things added by Ignacio

# pfam
pfam_url = mappings_url + 'pfam/'


# UniProt IDs
# proteins
proteins_url = base_url + "proteins/api/"
api_base_uniprot = proteins_url + "features?offset=0&size=100&"

# HG19 paths
hg19_path_local = "/home/ignacio/Dropbox/hg19/hg19.fa"
hg19_path_cluster = "/home/rio/zaugglab2/rio/data/hg19/hg19.fa"

# common data directory (common to all projects we are working
common_data_dir = "../../../common_data_dir"
# assert exists(common_data_dir)

# DeepBind software
deepbind_bin_path='/home/ignacio/Software/deepbind/deepbind'

# chromatin peaks path
histone_peaks_dir = "../data"
if not exists(histone_peaks_dir):
    histone_peaks_dir = '/g/scb/zaugg/rio/data/localQTLs/RMatrices'