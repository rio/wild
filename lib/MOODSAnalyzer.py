'''
Created on Jan 6, 2016

Methods for invoking and running MOODS faster

@author: ignacio
'''

'''
Created on Dec 25, 2015

@author: ignacio
'''


import MOODS
import os
from os.path import join, exists
from lib.FastaAnalyzer import FastaAnalyzer
from os import mkdir, listdir
import gzip

class MOODSAnalyzer():
    def run(self, multifasta_path, matrix_directory, output_path,
            matrix_filter_keys=None, join_sequences=True, special_header=None,
            pval=0.0001, starti=-1, endi=-1):
        """
        Scan a multifasta
        """
        
        fasta_analyzer = FastaAnalyzer()
        headers, sequences = list(zip(*fasta_analyzer.get_fastas_from_file(multifasta_path,
                                                                      uppercase=True)))

        if join_sequences:
            sequences = ["NNNNNNNNNNNNNNNNNNNN".join(sequences)]
        matrix_names = [filename for filename in os.listdir(matrix_directory)]

        if matrix_filter_keys is not None:
            accept = [0 for i in matrix_names]
            for k in matrix_filter_keys:
                for i in range(len(matrix_names)):
                    accept[i] = 1 if k in matrix_names[i] else accept[i]
            matrix_names = [f for f, selected in zip(matrix_names, accept) if selected]

        matrices = [MOODS.load_matrix(join(matrix_directory, filename))
                    for filename in matrix_names]

        # get matrices that we have not scanned, yet

        print("Sequences found:", len(sequences), "# headers", len(headers))
        if starti != -1 and endi != -1:
            matrices = matrices[starti: endi]

        print("# motifs", len(matrices))

        for header, seq, counter in zip(headers, sequences, list(range(len(sequences)))):
            print(counter, header, len(seq))
            # if "N" in seq:
            #    continue        
            # search with the default scoring model
            print(counter)
            if counter % 100 == 0:
                print(counter)
            print('searching...')
            results = MOODS.search(seq, matrices, pval, both_strands=True)
            print("# of hits",  len(results))
            for (matrix, matrix_name, result) in zip(matrices, matrix_names, results):
                l = len(matrix[0])
                writer = gzip.open(output_path, "a")
                # save hits to output files    
                # omit print details about found motifs
                for (pos, score) in sorted(result, 
                                           key=lambda pos_score: pos_score[0] if pos_score[0] >= 0 else len(seq) + pos_score[0]):
                    strand = "+"
                    hitseq = seq[pos:pos + l]
                    if pos < 0:
                        pos = len(seq) + pos
                        strand = "-"
                        hitseq = "".join(reversed([{'A' : 'T', 'T' : 'A', 'C' : 'G', 'G' : 'C', 'N': "N"}[s] for s in hitseq.upper()]))
                        if 'N' in hitseq:
                            continue
                    writer.write("\t".join([matrix_name.split(".")[0],
                                           special_header if special_header is not None else header,
                                           str(pos), strand, hitseq, "%0.3f" % score]) + "\n")
                writer.close()
