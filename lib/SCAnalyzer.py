'''
Created on 3/30/2020

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.plot_utils import *
import scanpy
import scanpy.api as sc
import seaborn as sb
from matplotlib.colors import ListedColormap

class SCAnalyzer:
    def plot_umap_by_gene(adata, df, gene_symbol, output_dir='../../data/figures/umap_by_gene', basename_file='',
                          normed=True, expr=None, color=None, subtitle=None, ax=None, save=True, xlab='UMAP1',
                          ylab='UMAP2', edgecolors_by_color=None, sizes_by_color=None, size_max=5.0, size_min=1.0,
                          **kwargs):
        cmap = kwargs.get('cmap', plt.cm.get_cmap('viridis'))
        expr = df[gene_symbol] if (expr is None and gene_symbol in df) else expr if color is None else color
        idx_by_idxkey= {idxkey: idx for idx, idxkey in zip(range(len(expr.index)), expr.index)}
        if normed:
            expr = expr / expr.sum()

        # sort to show most intense values on top
        expr = color if color is not None else expr
        idx, expr = zip(*sorted([[ei, e] for ei, e in expr.iteritems()], key=lambda x: x[1]))
        xy = adata.obsm['X_umap'][pd.Series(idx).map(idx_by_idxkey),]
        # xy = adata.obsm['X_umap']
        print('min/max')
        print(min(expr), max(expr))
        fig = plt.figure() if kwargs.get('fig') is None else kwargs.get('fig')
        ax = plt.subplot() if ax is None else ax

        print('rasterized?', kwargs.get('rasterized', False))
        if not kwargs.get('show_legend'):
            print('plotting with mode default...')
            sizes = [size_max if c != '#bdbdbd' else size_min for c in expr]
            sc_obj = plt.scatter(xy[:, 0], xy[:, 1], s=sizes, lw=0.2, edgecolors=[edgecolors_by_color[c] for c in expr],
                                 c=expr, vmin=min(expr) if kwargs.get('cmap') is None else None,
                                 label=kwargs.get('label'), rasterized=kwargs.get('rasterized', False),
                                 vmax=max(expr) if kwargs.get('cmap') is None else None,
                                 cmap=cmap)  # if color is None else None)
        else:
            print('plotting with mode legend...')
            label = kwargs.get('label')
            palette = kwargs.get('palette')
            label_by_i = dict(map(reversed, palette.items()))
            labels_uniq = list(set(label))
            for labi in labels_uniq:
                print('plotting by legend. Next =', labi, kwargs.get('palette')[labi])
                x = xy[:, 0][[label_by_i[e] == labi for e in expr]]
                y = xy[:, 1][[label_by_i[e] == labi for e in expr]]
                sc_obj = plt.scatter(x, y, s=kwargs.get('s', 0.2),
                                     # c=kwargs.get('palette')[labi],
                                     vmin=min(expr) if kwargs.get('cmap') is None else None,
                                     label=labi, rasterized=kwargs.get('rasterized', False),
                                     vmax=max(expr) if kwargs.get('cmap') is None else None,
                                     cmap=cmap)  # if color is None else None)
            lgnd = plt.legend(prop={'size': 6}, bbox_to_anchor=[1.025, 1], frameon=False)
            for handle in lgnd.legendHandles:
                handle.set_sizes([6.0])

        remove_all_ticks(ax)
        plt.xlabel(xlab, fontsize=kwargs.get('lab_fontsize', 6))
        plt.ylabel(ylab, fontsize=kwargs.get('lab_fontsize', 6))
        plt.xticks([])
        plt.yticks([])

        if kwargs.get('show_legend'):
            lgdn = plt.legend(fontsize=8, labels=labels_uniq, handles=sc_obj.legend_elements()[0], frameon=False,
                              markerscale=10)
            # ax.legend(markerscale=10)
        title = kwargs.get('title', '')
        if title is None:
            title = '# cells = %i, # genes = %i' % (adata.shape[0], adata.shape[1])
        plt.title(title + ("\n" + subtitle if subtitle is not None else ''), fontsize=kwargs.get('fontsize_title', 8))
        if kwargs.get('show_cbar', False):

            # plt.subplots_adjust(right=0.75)
            if kwargs.get('relocate_cbar', False):
                cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
                cbar = plt.colorbar(sc_obj, cax=cbar_ax)
                cbar.ax.set_ylabel(kwargs.get('label_cbar', '%s %s' % (gene_symbol, '[normed]' if normed else '')),
                                              rotation=270,
                                   fontsize=kwargs.get('cbar_fontsize', 8))
                cbar.ax.tick_params(labelsize=kwargs.get('cbar_fontsize', 8))
            else:
                cbar = plt.colorbar(sc_obj)
                cbar.ax.set_ylabel(kwargs.get('label_cbar', '%s %s' % (gene_symbol, '[normed]' if normed else '')),
                                              rotation=270,
                                   fontsize=kwargs.get('cbar_fontsize', 8))
                cbar.ax.tick_params(labelsize=kwargs.get('cbar_fontsize', 8))

        if save:
            plt.subplots_adjust(right=.75)
            plt.savefig(join(output_dir, ((basename_file + "_") if len(basename_file) > 0 else '') + "%s.%s" %
                             (gene_symbol, kwargs.get('format', 'pdf'))),
                        dpi=kwargs.get('dpi', 250))
            print('saved at...')
            print(abspath(join(output_dir, ((basename_file + "_") if len(basename_file) > 0 else '') + "%s.%s" %
                               (gene_symbol, kwargs.get('format', 'pdf')))))
            plt.close()

    def plot_qc(adata, output_dir, filename='qc', color_gene='mt_frac', **kwargs):
        lab_size = 8
        tick_size = 8
        ax = plt.subplot(3, 2, 1)
        ax = sc.pl.scatter(adata, 'n_counts', 'n_genes', color=color_gene, show=False, legend_fontweight=50, ax=ax,
                           size=10)
        ax.set_title('Fraction mitochondrial counts', fontsize=lab_size)
        ax.set_xlabel("Count depth", fontsize=lab_size)
        ax.set_ylabel("Number of genes", fontsize=lab_size)
        ax.tick_params(labelsize=tick_size)
        ax.axhline(kwargs.get('ngenes_thr', 250), 0, 1, color='red', lw=.6)
        ax.axvline(kwargs.get('count_depth_thr', 1250), 0, 1, color='red', lw=.6)
        remove_top_n_right_ticks(ax)
        fig = plt.gcf()
        cbar_ax = fig.axes[-1]
        cbar_ax.tick_params(labelsize=tick_size)
        f1 = ax.get_figure()

        ax = plt.subplot(3, 2, 2)
        p2 = sb.distplot(adata.obs['n_counts'], kde=False)
        p2.set_xlabel("Count depth", fontsize=lab_size)
        p2.set_ylabel("Frequency", fontsize=lab_size)
        p2.tick_params(labelsize=tick_size)
        ax.axvline(kwargs.get('count_depth_thr', 1250), 0, 1, color='red', lw=.6)
        plt.xticks([100, 1000, 2000, 5000, 10000, 20000],
                   rotation=45, ha='right')
        remove_top_n_right_ticks(ax)
        plt.xlim([0, 20000])

        ax = plt.subplot(3, 2, 3)

        p3 = sb.distplot(adata.obs['n_genes'], kde=False)
        p3.set_xlabel("Number of genes", fontsize=lab_size)
        p3.set_ylabel("Frequency", fontsize=lab_size)
        plt.xticks([100, 200, 500, 1000, 2000, 5000, 10000],
                   rotation=45, ha='right')
        plt.xlim([0, 5000])

        p3.tick_params(labelsize=tick_size)
        p3.axvline(kwargs.get('ngenes_thr', 250), 0, 1, color='red', lw=.6)
        plt.tight_layout()
        remove_top_n_right_ticks(ax)

        ax = plt.subplot(3, 2, 4)
        p4 = sb.distplot(adata.obs['n_counts'][adata.obs['n_counts'] < 4000], kde=False, bins=60)
        p4.set_xlabel("Count depth", fontsize=lab_size)
        p4.set_ylabel("Frequency", fontsize=lab_size)
        p4.tick_params(labelsize=tick_size)
        p4.axvline(kwargs.get('count_depth_thr', 1250), 0, 1, color='red', lw=.6)
        plt.tight_layout()
        remove_top_n_right_ticks(ax)

        ax = plt.subplot(3, 2, 5)
        count_data = adata.obs['n_counts'].copy(deep=True)
        count_data.sort_values(inplace=True, ascending=False)
        order = range(1, len(count_data) + 1)
        p5 = plt.semilogy(order, count_data, 'b-')
        plt.gca().axhline(1500, 0, 1, color='red')
        plt.xlabel("Barcode rank", fontsize=lab_size)
        plt.ylabel("Count depth", fontsize=lab_size)
        plt.tick_params(labelsize=tick_size)
        remove_top_n_right_ticks(ax)
        # f4 = p4.get_figure()f
        plt.tight_layout()

        # Cells per gene
        ax = plt.subplot(3, 2, 6)
        p6 = sb.distplot(adata.var['cells_per_gene'][adata.var['cells_per_gene'] < 100], kde=False, bins=60)
        p6.set_xlabel("Number of cells", fontsize=lab_size)
        p6.set_ylabel("Frequency", fontsize=lab_size)
        p6.set_title('Cells per gene', fontsize=lab_size)
        p6.tick_params(labelsize=tick_size)
        remove_top_n_right_ticks(ax)
        plt.tight_layout()
        savefig(join(output_dir, filename))
        plt.close()

