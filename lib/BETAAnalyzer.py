'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.MyGeneAnalyzer import MyGeneAnalyzer
from lib.REMAPAnalyzer import REMAPAnalyzer

class BETAAnalyzer():


    @staticmethod
    def run(peaks, expr, genome, output_dir, ref=None, da=500):


        if not exists(output_dir):
            mkdir(output_dir)

        # save peaks
        bed_path = join(output_dir, 'peaks.bed')
        DataFrameAnalyzer.to_tsv(peaks[peaks.columns[:3]], bed_path, header=None)

        # save expr
        expr_path = join(output_dir, 'expr.tsv')
        if not expr.columns[0].startswith('#'):
            expr = expr.rename(columns={expr.columns[0]: '#' + expr.columns[0]})
        DataFrameAnalyzer.to_tsv(expr, expr_path)


        if ref is not None:
            ref_path = join(output_dir, 'ref_transcriptome.tsv')
            DataFrameAnalyzer.to_tsv(ref, ref_path)
            cmd = "BETA basic -p %s -e %s -k BSF -g %s -r %s --da %i -n basic -o %s" % (bed_path, expr_path, genome,
                                                                                         ref_path, da, output_dir)
        else:
            cmd = "BETA basic -p %s -e %s -k BSF -g %s --da %i -n basic -o %s" % (bed_path, expr_path, genome, da, output_dir)

        print(cmd)
        system(cmd)

    @staticmethod
    def parse_ensembl_as_decoy_refseq(ensembl):
        return ['NM_' + s[-6:] for s in ensembl]

    @staticmethod
    def parse_decoy_refseq_to_ensembl(decoy_refseq):
        return ['ENSG00000' + s[-6:] for s in decoy_refseq]


    @staticmethod
    def get_genes_organism_decoy_refseq(organism):
        tss = MyGeneAnalyzer.get_gene_tss(organism) # mouse or human

        tss['#ID'] = BETAAnalyzer.parse_ensembl_as_decoy_refseq(tss['ENSEMBL'].astype(str))

        tss['pos'] = np.where(tss['pos'] == 1, '-', '+')
        tss = tss[['#ID', 'chr', 'pos', 'start', 'end', 'SYMBOL']]
        tss.columns = ['#ID', 'chr', 'strand', 'txstart', 'txend', 'genesymbol']
        return tss

    @staticmethod
    def get_test_expr_file(peaks, organism, expr_thr=1.3, direction=None): # up/down/both
        '''
        Generate a decoy peaks file that treats close genes as UP, DOWN, or BI-DIRECTIONAL
        :param peaks:
        :param organism:
        :param direction:
        :return:
        '''

        if direction is None:
            print('provide direction up/down/both')
        tss = MyGeneAnalyzer.get_gene_tss(organism)
        close = REMAPAnalyzer.get_closest_tss_gene(peaks[['chr', 'start', 'end']], tss)

        close = close[close['distance'] != -1]
        close = close.sort_values('distance')
        close['log2FC'] = list(range(close.shape[0], 0, -1))
        close['log2FC'] = (close['log2FC'] - np.mean(close['log2FC'])) / np.std(close['log2FC'])

        close = close.drop_duplicates('ensembl')
        expr = pd.DataFrame()
        expr['#ID'] = close['ensembl']
        expr['log2FC'] = close['log2FC']

        expr['adj.P.Val'] = np.where((expr['log2FC'] > expr_thr), 0.0001, 1.0)

        # add the ones without a target
        not_found = pd.DataFrame()
        not_found['#ID'] = list(set(tss['ENSEMBL']) - set(expr['#ID']))
        not_found['log2FC'] = 0.0
        not_found['adj.P.Val'] = 1.0

        if direction == 'down':
            expr['log2FC'] *= -1.0
        if direction == 'both':
            expr['log2FC'] *= [log2FC * (1.0 if np.random.randint(0, 2) else -1.0) for log2FC in expr['log2FC']]

        expr = pd.concat([expr, not_found]).reset_index(drop=True)

        expr['#ID'] = BETAAnalyzer.parse_ensembl_as_decoy_refseq(expr['#ID'].astype(str))
        return expr[['#ID', 'log2FC', 'adj.P.Val']]




if __name__ == '__main__':
    from lib.BETAAnalyzer import BETAAnalyzer
    from lib.REMAPAnalyzer import REMAPAnalyzer


    peaks = REMAPAnalyzer.get_remap_peaks('HNF4A')
    # expr = DataFrameAnalyzer.read_tsv('/g/scb2/zaugg/zaugg_shared/Programs/BETA_test_data/AR_diff_expr.tsv')

    peaks = peaks.sample(1000).sort_values(['chr', 'start'], ascending=[True, True])
    expr = BETAAnalyzer.get_test_expr_file(peaks, 'human', expr_thr=1.3, direction='both')
    ref = BETAAnalyzer.get_genes_organism_decoy_refseq('human')

    BETAAnalyzer.run(peaks, expr, 'hg19', '/tmp/BETA_test', ref=ref)
