'''
Created on 4/1/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *

class SignatureGenesAnalyzer:

    @staticmethod
    def get_signature_genes_by_tissue():
        genes_by_tissue = '../../data/gene_subsets_five_tissues_1000_genes.tsv'
        genes_df = DataFrameAnalyzer.read_tsv(genes_by_tissue)
        genes_df = {k: set(genes_df[k]) for k in genes_df.columns}
        # add custom names from Butte's data
        butte_dir = '../../data/gene_sets_custom'
        for f in listdir(butte_dir):
            p = join(butte_dir, f)
            ids = {s.strip() for s in open(p)}
            # print f, len(ids), ids
            genes_df[f.replace(".txt", '')] = ids

        # load IDs for Pancreas and Heart from GTEx directly
        for tissue_id in ['Pancreas', 'Heart - Left Ventricle']:
            p = '../../data/1000_genes_gtex_tissues/$1.tsv.gz'
            next_df = DataFrameAnalyzer.read_tsv_gz(p.replace("$1", tissue_id))
            genes_df[tissue_id.replace('Pancreas', 'pancreas').replace("Heart - Left Ventricle", 'Heart')] = set(next_df['Gene stable ID'])

        return genes_df