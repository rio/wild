'''
Created on Feb 2, 2016

This is a class to run MEME in a consistent way. Methods are given to call
software faster 

@author: ignacio
'''

import ntpath
from os import system
import os
import sys

class MEMEAnalyzer:
    def run(self, multifasta, output_dir=None,
            biomolecule="dna", maxsize=100000,
            minw=-1, maxw=-1):
        
        if output_dir == None:
            filename = ntpath.basename(multifasta)
            basename_no_ext = filename.split(".")[0]
            output_dir = multifasta.replace(filename, basename_no_ext + "_meme")
            
        meme = "/home/ignacio/meme/bin/meme"
        cmd = " ".join([meme, multifasta, "-o", output_dir,
                        "-" + biomolecule, "-mod", "zoops",
                        "-maxsize", str(maxsize), "-nmotifs", "3",
                        "-revcomp"])
        if minw != -1:
            cmd += " -minw " + str(minw) 
        if maxw != -1:
            cmd += " -maxw " + str(maxw) 
            
        system(cmd)
    
    def get_subset_top_n(self, elements, n=100, ascendent=False):
        """
        Given a set of elements, return the top-n according to a score present
        in the last column of our list
        """
        return sorted(elements, key=lambda x: x[-1], reverse=ascendent is False)[:n]
        
    def create_hmm_from_sequences(self, fasta_path, output_path, order=4):
        """
        Use fasta-get-markov to generate models from a fasta path
        """
        cmd = " ".join("fasta-get-markov", "-m", str(order),
                       fasta_path, ">", output_path)
        system(cmd)
        
    def write_fastas_to_file(self, fastas, output_path):
        writer = open(output_path, "w")
        for header, seq in fastas:
            writer.write(">" + header + "\n" + seq + "\n")
        writer.close() 
        
    def get_fastas_from_file(self, fasta_path):
        fastas = []
        seq = None
        header = None
        for r in open(fasta_path):
            r = r.strip()
            if r.startswith(">"):
                if seq != None and header != None:
                    fastas.append([header, seq])
                seq = ""
                header = r[1:]
            else:
                if seq != None:
                    seq += r
                else:
                    seq = r
        
        # append last fasta read by method
        fastas.append([header, seq])
        
        return fastas