'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
import pyBigWig

class BigWigAnalyzer:
    @staticmethod
    def get_tracks(bigwig_path, coordinates):
        '''
        Given a set of coordinates in the format chr:start-end, return the signals tracks
        :param bigwig_path:
        :param coordinates:
        :return:
        '''
        table = {}
        bw = pyBigWig.open(bigwig_path)
        for t in coordinates:
            chr = t.split(":")[0]
            start, end = list(map(int, t.split(":")[1].split("-")))
            vals = bw.values(chr, start, end)
            table[t] = vals
        bw.close()
        return table

    @staticmethod
    def get_sum_data(bigwig_path):
        '''
        :param bigwig_path:
        :return:
        '''
        bw = pyBigWig.open(bigwig_path)
        bw.sumData()
        bw.close()

    @staticmethod
    def get_headers(bigwig_path):
        '''
        :param bigwig_path:
        :return:
        '''
        bw = pyBigWig.open(bigwig_path)
        headers = bw.header()
        bw.close()
        return headers
