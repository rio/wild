

from os import chdir, mkdir
from os.path import abspath, exists
from os import system
import pandas as pd
import numpy as np

class x3DNAAnalyzer():


    @staticmethod
    def analyze(input_abs_path, output_dir):

        return_dir = abspath(".")

        if not exists(output_dir):
            mkdir(output_dir)
        # move to output_dir
        print('changing dir to', output_dir)
        chdir(output_dir)

        cmd = " ".join(["find_pair", input_abs_path,
                        "stdout", "|", "analyze", "stdin"])
        print(cmd)
        system(cmd)
        chdir(return_dir)


    @staticmethod
    def get_groove_values(out_path, vdw_constant=5.8):

        start_k = 'Protein-induced Bending in DNA.'
        end_k = '**********************************************'
        start = False
        sel = []
        for line in open(out_path):
            if start_k in line:
                start = True
            if start and line.startswith(end_k):
                start = False
            if start:
                sel.append(line.strip())
        sel = [[si for si in s.split(" ") if len(si) != 0] for s in sel]
        values = [[float(vi) - vdw_constant
                   if vi != '---' else np.nan for vi in v[2:]] for v
                  in sel[4:]]
        nucleotides = [v[1][0] for v in sel[4:]]
        res = []
        for nt, vals in zip(nucleotides, values):
            res.append([nt] + vals)
        res = pd.DataFrame(res, columns=['nt', 'MGW.pp', 'MGW.refined',
                                         'major.groove.width.pp',
                                         'major.groove.width.refined'])

        return res