

from os.path import join
import gzip
import sys
import subprocess as sp
import random
from lib.SequenceMethods import SequenceMethods

class FastQAnalyzer:

    def filter_short_reads(self, fastq_input_path, fastq_output_path, skip_length=None):
        '''
        Read a FastQ file and omit reads that are shorter than a given threshold
        :param fastq_path:
        :param skip_length:
        :return:
        '''
        counter = 0
        skipped = 0
        print('reading file...')
        writer = open(fastq_output_path, "w")
        info = []
        reject = False
        for line in open(fastq_input_path):
            line = line.strip()
            info.append(line)
            if (counter - 1) % 4 == 0:
                read = line
                # skip if the read is too short
                if skip_length is not None and len(line) <= skip_length:
                    reject = True
                    skipped += 1
                    if skipped % 100 == 0:
                        print('# skipped so far', skipped)
            # wrote once every four lines
            if (counter - 3) % 4 == 0:
                if not reject:
                    for r in info:
                        writer.write(r + "\n")
                info = []
                reject = False
            counter += 1
        writer.close()
        print("# reads skipped:", skipped)

    @staticmethod
    def split_two_sets(p, seed=1000):
        seqs = FastQAnalyzer.get_sequences(p)
        random.seed(seed)
        random.shuffle(seqs)
        return seqs[:len(seqs)/2], seqs[len(seqs)/2:]

    def count_occurences_kmer(self, kmer, fastq_paths, complementary=True):
        '''
        Given a Kmer and a list of fastq paths, get the occurences of that kmer in those files
        :param kmer:
        :param fastq_paths:
        :param complementary:
        :return:
        '''
        total = 0
        for p in fastq_paths:
            # print 'ocurrences of', kmer, 'in fastq files...'
            print(kmer, p)
            o = sp.Popen(["zcat", p], stdout=sp.PIPE)
            if complementary:
                kmer_rev = SequenceMethods.get_complementary_seq(kmer)
                o = sp.Popen(['grep', "-E", kmer + "|" + kmer_rev],
                             stdin=o.stdout, stdout=sp.PIPE)
            else:
                o = sp.Popen(['grep', kmer], stdin=o.stdout, stdout=sp.PIPE)
            o = sp.Popen(['wc', '-l'], stdin=o.stdout, stdout=sp.PIPE).communicate()
            n = int(o[0])
            # print p, n
            total += n

        return total


    @staticmethod
    def get_sequences(fastq_path, stop_at=None, log=True, remove_N=False, n_hop=4, counter=0):
        reads = []

        reader = gzip.open if fastq_path.endswith('.gz') else open
        for line in reader(fastq_path):
            line = line.strip()
            if (counter - 1) % n_hop == 0:
                if remove_N:
                    if not 'N' in line:
                        reads.append(line)
                else:
                    reads.append(line)
                if stop_at != None and len(reads) >= stop_at:
                    break
                if len(reads) % 500000 == 0:
                    if log:
                        print('# reads so far', len(reads))
            counter += 1
        print("# of reads accepted", len(reads))
        return reads

    def group_sequences_by_kmer(self, sequences, **kwargs):
        '''
        :param sequences:
        :return:
        '''
        use_complementary = kwargs.get('count_complementary', False)
        counts = {}
        for s in sequences:
            counts[s] = counts[s] + 1 if s in counts else 1
            if use_complementary:
                cmp_seq = SequenceMethods.get_complementary_seq(s)
                counts[cmp_seq] = counts[cmp_seq] + 1 if cmp_seq in counts else 1
        return counts

    def print_kmers(self, kmers, **kwargs):
        stopat = kwargs.get('stopat', None)
        for i, k in enumerate(sorted(list(kmers.keys()), key=lambda x: -kmers[x])):
            print(i, k, kmers[k])
            if stopat is not None and i >= stopat:
                break

    def write_sequences_as_kmers(self, sequences, output_path, **kwargs):
        counts = self.group_sequences_by_kmer(sequences, **kwargs)
        compress = kwargs.get('compress', False)
        writer = open(output_path, 'w') if not compress else gzip.open(output_path, 'w')
        print('writing kmers to', output_path)
        for i, k in enumerate(sorted(list(counts.keys()), key=lambda x: -counts[x])):
            writer.write('\t'.join(map(str, [k, counts[k]])) + "\n")
        writer.close()

    @staticmethod
    def trim_fastq(input, output, nleft=0, nright=0, nseqs=None):
        print('reading headers/seqs/symbols/flags...')
        headers = FastQAnalyzer.get_sequences(input, stop_at=nseqs if (nseqs != -1 and nseqs != None) else None, counter=1)
        seqs = FastQAnalyzer.get_sequences(input, stop_at=nseqs if (nseqs != -1 and nseqs != None) else None)
        symbols = FastQAnalyzer.get_sequences(input, stop_at=nseqs if (nseqs != -1 and nseqs != None) else None, counter=-1)
        flags = FastQAnalyzer.get_sequences(input, stop_at=nseqs if (nseqs != -1 and nseqs != None) else None, counter=-2)
        seqs = [s[nleft:len(s) - nright] for s in seqs]
        flags = [f[nleft:len(s) - nright] for f in flags]

        # are all sequences same length
        print('SEQ lengths:', set([len(s) for s in seqs]))
        assert len(set([len(s) for s in seqs])) == 1
        # do sequences contain N?
        seqs_with_n = [s for s in seqs if 'N' in s]
        print('# seqs with N', len(seqs_with_n), seqs_with_n[:3])
        writer = gzip.open(output, 'w')
        for t in zip(headers, seqs, symbols, flags):
            [writer.write(ti + "\n") for ti in t]
        writer.close()
        print('written to output...')
        print(output)

    @staticmethod
    def replace_fastq_sequences(input, output, seqs=None, nleft=0, nright=0, nseqs=None):
        print('reading headers/seqs/symbols/flags...')
        headers = FastQAnalyzer.get_sequences(input, stop_at=nseqs if (nseqs != -1 and nseqs != None) else None, counter=1)

        if seqs is None:
            print('reading sequences from main input...')
            seqs = FastQAnalyzer.get_sequences(input, stop_at=nseqs if (nseqs != -1 and nseqs != None) else None)

        symbols = FastQAnalyzer.get_sequences(input, stop_at=nseqs if (nseqs != -1 and nseqs != None) else None, counter=-1)
        flags = FastQAnalyzer.get_sequences(input, stop_at=nseqs if (nseqs != -1 and nseqs != None) else None, counter=-2)

        seqs = [s[nleft:len(s) - nright] for s in seqs]
        flags = [f[nleft:len(s) - nright] for f in flags]

        # are all sequences same length
        print('SEQ lengths:', set([len(s) for s in seqs]))
        assert len(set([len(s) for s in seqs])) == 1
        # do sequences contain N?
        seqs_with_n = [s for s in seqs if 'N' in s]
        print('# seqs with N', len(seqs_with_n), seqs_with_n[:3])
        writer = gzip.open(output, 'w')
        for t in zip(headers, seqs, symbols, flags):
            [writer.write(ti + "\n") for ti in t]
        writer.close()
        print('written to output...')
        print(output)


# python path_to_script.py path_to_fastq_input path_to_fastq 4
if __name__ == '__main__':
    path1 = sys.argv[1]
    path2 = sys.argv[2]
    skip = int(sys.argv[3])
    fastq_analyzer = FastQAnalyzer()
    fastq_analyzer.filter_short_reads(path1, path2, skip)