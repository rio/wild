
from lib.utils import *


class StarFacade:

    def run(self, pos_path, neg_path, alpha=0.05):

        binary = "/home/rio/data/star/star-v.1.0/star"
        cmd = " ".join([binary, pos_path, neg_path, str(alpha)])
        print(cmd)
        system(cmd)