


# pandas column printing
print('import pandas..')
import pandas as pd
from lib.DataFrameAnalyzer import DataFrameAnalyzer
from lib.NotesFacade import NotesFacade
from lib.ThreadingUtils import ThreadingUtils
import multiprocessing
from multiprocessing import Manager

pd.set_option('display.width', 1000)
pd.set_option('display.max_columns', 30)

import lib
print('importing path functions...')
from lib.path_functions import *

# importing statistical functions
print('importing stat functions...')
from lib.stat_utils import *

try:
    import ipdb
except ImportError:
    print('ipdb is not installed and cannot be imported...')

print('importing misc functions..')
from lib.BSubFacade import BSubFacade

import numpy as np
from numpy import average, median # we always need to calculate average and medians

from lib.gzutils import *
import pickle

# Things that are specific to python 2.x
import sys
if sys.version.startswith('2'):
    import pickle
    # custom magics for pasting
elif sys.version.startswith('3'):
    import importlib
    from importlib import reload


from lib import custommagics


import tempfile
import sys
import subprocess as sp

import random
from random import shuffle

from itertools import combinations, permutations, product, combinations_with_replacement, repeat
print('importing done...')




def stop():
    assert 1 > 2

def is_gzip(path):
    GZIP_MAGIC_NUMBER = "1f8b"
    f = open(path)
    return f.read(2).encode("hex") == GZIP_MAGIC_NUMBER

import datetime
def get_time_now():
    return datetime.datetime.now()

def get_date_yyyymmdd():
    date = datetime.datetime.today().strftime('%Y-%m-%d').replace("-", '')
    return date

import time
def get_file_age_seconds(filepath):
    return time.time() - os.path.getmtime(filepath)
def get_file_age_minutes(filepath):
    return get_file_age_seconds(filepath) / 60.0

def get_timespan_seconds(t0):
    t1 = datetime.datetime.now()
    return (t1 - t0).total_seconds()
def get_timespan_minutes(t0):
    return "%.1f" % (get_timespan_seconds(t0) / 60.0)

import math
def nCr(n,r):
    f = math.factorial
    return f(n) / f(r) / f(n-r)
