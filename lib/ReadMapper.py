


from lib.utils import *

class ReadMapper:
    @staticmethod
    def map_reads_to_genome_unpaired(fastq_gz_path, output_dir=None, genome='mm10', fastq_gz_path_2=None,
                                     ncores=15, **kwargs):
        if isinstance(fastq_gz_path, list):
            fastq_gz_path, fastq_gz_path_2 = fastq_gz_path

        bin = kwargs.get('bin', None)
        f = basename(fastq_gz_path)
        basedir = os.path.abspath(os.path.join(fastq_gz_path, os.pardir))
        print(basedir)
        # align using mm10
        # genome_dir = "/g/scb/zaugg/rio/data/mm10"
        tmp_dir = os.environ['TMPDIR'] if 'TMPDIR' in os.environ else '/tmp'

        suffix_idx = None
        if genome == 'mm10' and kwargs.get('bin', None) == 'bowtie':
            genome_index_dir = '/g/scb2/zaugg/zaugg_shared/annotations/mm10/Bowtie'
            genome_index = join(tmp_dir, "mm10_colorspace.index")
            print(genome_index)
            mapped_dir = join(basedir, 'mapped_%s' % genome)
            suffix_idx = '.ebwt'
        elif genome == 'mm10':
            genome_index_dir = '/g/scb2/zaugg/zaugg_shared/annotations/mm10/Bowtie2_new_useThis'
            genome_index = join(tmp_dir, "mm10")
            print(genome_index)
            mapped_dir = join(basedir, 'mapped_%s' % genome)
            suffix_idx = '.bt2'
        elif genome == 'hg19':
            genome_index_dir = '/g/scb2/zaugg/zaugg_shared/annotations/hg19/referenceGenome/Bowtie2'
            genome_index = join(tmp_dir, "hg19")
            suffix_idx = '.bt2'
            mapped_dir = join(basedir, 'mapped_%s' % genome)

        if output_dir is not None:
            mapped_dir = output_dir

        # here we save the output data as a tmp file
        tmp_output_dir = join(tmp_dir, 'mapped_%s_%s' % (genome, kwargs.get('file_basename', basename(f))))

        print(genome_index_dir)
        if not exists(tmp_output_dir):
            mkdir(tmp_output_dir)

        print('temp output files will be written to')
        print(tmp_output_dir)

        basename_sam = kwargs.get('basename_sam', basename(f).replace("_1", "") + ".sam")
        output_sam = join(tmp_output_dir, basename_sam)

        trim_adapters = kwargs.get('trim_adapters', False)
        if kwargs.get(trim_adapters, False):
            output_sam = output_sam.replace(".sam", '.trimmed.sam')

        print('please check this directory to track output files before copying back...')
        print(output_sam)

        for genome_index_file in listdir(genome_index_dir):
            print(genome_index_file)
            if not genome_index_file.endswith(suffix_idx):
                continue
            # move index copy to TMPDIR (faster)
            print(tmp_dir)
            fast_genome_index = join(tmp_dir, basename(genome_index_file))
            if not exists(fast_genome_index):
                print('copying bt2 to tmp...')
                print('source', genome_index_file)
                print('target', fast_genome_index)
                copy2(join(genome_index_dir, genome_index_file),
                      fast_genome_index)
                print(fast_genome_index)
                print('file copied')

        if not exists(output_sam):
            if trim_adapters:
                cmd = ' '.join(['trim_adapters', fastq_gz_path, fastq_gz_path_2])
                print('trimming adapters')
                print(cmd)
                fastq_gz_path = fastq_gz_path.replace(".fastq.gz", ".trimmed.fastq.gz")
                fastq_gz_path_2 = fastq_gz_path_2.replace(".fastq.gz", ".trimmed.fastq.gz")
                # execute command only if trimmed paths are not found.
                if not exists(fastq_gz_path) or not exists(fastq_gz_path_2):
                    system(cmd)

            if fastq_gz_path_2 is not None: # Paired data
                cmd = ' '.join(
                    ['/g/funcgen/bin/bowtie2' if bin is None else bin,
                     '-x', genome_index, '-p', '%i' % ncores, '-1', fastq_gz_path, '-2', fastq_gz_path_2,
                    # '-u', '1000',
                    "1>", output_sam, '2>', output_sam + ".log"])
            elif bin is not None: # bowtie
                cmd = ' '.join(
                    ['bowtie', '-C', '-S', '-p', '%i' % ncores, genome_index, fastq_gz_path, ">", output_sam])
            else: # single end data
                cmd = ' '.join(
                    ['/g/funcgen/bin/bowtie2' if bin is None else bin,
                     '-x', genome_index, '-p', '%i' % ncores, '-U', fastq_gz_path,
                    # '-u', '1000',
                    "1>", output_sam, '2>', output_sam + ".log"])
            print(cmd)
            system(cmd)

            print(exists(output_sam), output_sam)

        output_bam = output_sam.replace(".sam", ".bam")
        if not exists(output_bam):
            args = ["~/zaugglab/rio/miniconda2/bin/samtools", "view", "-Sb", output_sam, ">", output_bam]
            print(" ".join(args))
            cmd = " ".join(args)
            system(cmd)

        # remove the SAM intermediate file (big size. Annoying to keep in multiple analyses.
        remove(output_sam)

        # sort and index bam files
        print(output_bam)
        output_bam_sort = output_bam.replace(".bam", ".sorted.bam")
        output_bam_index = output_bam_sort.replace(".bam", ".bam.bai")
        if not exists(output_bam_sort):
            args = ["~/zaugglab/rio/miniconda2/bin/samtools", "sort", "-T", tempfile.mkstemp()[1],
                    "-o", output_bam_sort, output_bam]
            print(" ".join(args))
            cmd = " ".join(args)
            system(cmd)
            print('removing unsorted bam file...noh')
            if exists(output_bam):
                remove(output_bam)
            args = ["~/zaugglab/rio/miniconda2/bin/samtools", "index", output_bam_sort, output_bam_index]
            print(" ".join(args))
            cmd = " ".join(args)
            system(cmd)
        # in the last step, copy everything back to our data directory
        for f in listdir(tmp_output_dir):
            print('copying to output directory', f)
            src = join(tmp_output_dir, f)
            dest = join(mapped_dir, f)
            print(src)
            print(dest)
            copy2(src, dest)


    @staticmethod
    def get_libsize(bampath):
        output_path = bampath + '.libSize.txt'
        if not exists(output_path):
            cmd = ' '.join(['samtools', 'view' , '-c' , '-F', '260', bampath, ' > ', output_path])
            print(cmd)
            system(cmd)
        libsize = int([r for r in open(output_path)][0])
        return libsize