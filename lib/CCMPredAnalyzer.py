'''
Created on 8/1/2019

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.FastaAnalyzer import FastaAnalyzer

class CCMPREDAnalyzer():

    @staticmethod
    def run(aln_path_or_sequences, output_path, remove_input=True, threads=1, cuda_device=None):


        if isinstance(aln_path_or_sequences, list):
            aln_path = tempfile.mkstemp()[1]
            FastaAnalyzer.write_fasta_from_sequences(aln_path_or_sequences, aln_path, add_headers=False)
        else:
            aln_path = aln_path_or_sequences
        bin_path = '/g/scb2/zaugg/zaugg_shared/Programs/CCMpred/bin/ccmpred'
        cmd = " ".join([bin_path, '-t' if (cuda_device is None) else '-d',
                        str(threads if cuda_device is None else cuda_device),
                        aln_path, output_path])
        print(cmd)
        system(cmd)

        if isinstance(aln_path_or_sequences, list) and remove_input:
            remove(aln_path)

        df = DataFrameAnalyzer.read_tsv(output_path, header=None)
        df = df[df.columns[:-1]]
        remove(output_path)
        return df

