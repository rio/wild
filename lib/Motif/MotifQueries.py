
from MotifDatabase import MotifDatabase
from MotifContrastFunction import MotifContrastFunction

class MotifQueries:
    # DEFINE MOTIF DATABASES AND COMPARISONS
    # These are the encapsulated function methods to be used for contrasts during this script (Strategy Pattern)
    AUROC, ODDS_RATIO, FOLD_CHANGE, GLM_LOGIT = MotifContrastFunction.get_auroc, \
                                                MotifContrastFunction.get_odds_ratio, \
                                                MotifContrastFunction.get_fold_change,\
                                                MotifContrastFunction.get_r_glm_logistic_pvalue
    # here we buld the relationship between our motif db and the metrics that can be obtained for each one


    # human queries are the same
    hs_queries = {'JASPAR': [MotifDatabase.JASPAR2018_VERTEBRATES,
                MotifDatabase.JASPAR2018_ANNOTATION_HUMAN,
                [AUROC]],
               'CISBP': [MotifDatabase.CISBP_HUMAN,
               MotifDatabase.CISBP_annotation_human,
               [AUROC]],
               'HOCOMOCO': [MotifDatabase.HOCOMOCOv11_full_HUMAN_mono_homer_format_stringent,
                  MotifDatabase.HOCOMOCOv11_annotation_human,
                  [AUROC, ODDS_RATIO, FOLD_CHANGE]],
                'GENRE': [MotifDatabase.GENRE_GLOSSARY,
                None,
               [GLM_LOGIT, AUROC, ODDS_RATIO, FOLD_CHANGE]]}


    queries_by_genome = {'mm10': {'JASPAR' : [MotifDatabase.JASPAR2018_VERTEBRATES,
                        MotifDatabase.JASPAR2018_ANNOTATION_MOUSE,
                        [AUROC]],
                                  'CISBP': [MotifDatabase.CISBP_MOUSE,
                                            MotifDatabase.CISBP_annotation_mouse,
                                            [AUROC]],
                                  'HOCOMOCO':  [MotifDatabase.HOCOMOCOv11_full_MOUSE_mono_homer_format_stringent,
                                                MotifDatabase.HOCOMOCOv11_annotation_mouse,
                                                [AUROC, ODDS_RATIO, FOLD_CHANGE]],
                                  'GENRE': [MotifDatabase.GENRE_GLOSSARY,
                                            None,
                                            [GLM_LOGIT, AUROC, ODDS_RATIO, FOLD_CHANGE]]},
                         'hg19': hs_queries,
                         'hg38': hs_queries}