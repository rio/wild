

import pandas as pd

class MotifContrast:

    @staticmethod
    def run(motif_hits, function=None, contrast_order=None, **kwargs):
        # define the contrast order
        if contrast_order is None:
            assert len(set(motif_hits['group'])) == 2
            contrast_order = list(set(motif_hits['group']))
            print 'contrast (foreground/background)', contrast_order
        id1, id2 = contrast_order
        
        # assign labels according to the contrast order
        motif_hits['label'] = (motif_hits['group'] == id1).astype(int)
        
        # by default we'll crash if user does not define a metric for comparison
        if function is None:
            print 'please provide a function (see MotifContrastFunction)'
            assert function is None
        
        res = []
        
        counter = 0
        n_motifs = len(set(motif_hits['motif.id']))
        for motif_id, grp in motif_hits.groupby('motif.id'):
            counter += 1
            if counter % 50 == 0:
                print 'Done', counter, 'of', n_motifs, '...'
            a = grp[grp['group'] == id1].sort_values('score', ascending=False).drop_duplicates('header')
            b = grp[grp['group'] == id2].sort_values('score', ascending=False).drop_duplicates('header')

            na = a.shape[0]
            nb = b.shape[0]
            input = pd.concat([a, b])            
            
            # assess bias using generic method
            try:
                out = function(input, alternative=kwargs.get('alternative', None), **kwargs)
            except Exception:
                return None
            out['motif.id'] = motif_id
            res.append(out)
            
        res = pd.concat(res)
        return res.sort_values('score', ascending=False)
        
            
        