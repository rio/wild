'''
Created on Feb 10, 2016

@author: ignacio
'''

from lib.utils import *
from matplotlib import gridspec
from matplotlib import figure
import subprocess
from os import listdir, remove, system, mkdir, rmdir
import gzip
from os.path import join, exists
from random import shuffle
from shutil import move

class MotifCoenrichmentAnalyzer:
    
    def __init__(self):
        self.bed_tables = {}
        
    def get_distances(self, all_positions, threshold=100):
        distances = []
        for i in range(len(all_positions)):
            a, label_a = all_positions[i]
            if label_a != "a":
                continue
    
            # check for all sorrounding positions left and right, until out of options
            j = i - 1
            if j < 0:
                continue
            b, label_b = all_positions[j]
            distance = abs(a - b)
    
            # left position cases
            while distance <= threshold:
                if label_a != label_b:
                    distances.append([a, b, distance])
                j -= 1
                if j < 0:
                    break
                b, label_b = all_positions[j]
                distance = abs(a - b)
    
            # right position cases
            j = i + 1
            if j >= len(all_positions):
                continue
            b, label_b = all_positions[j]
            distance = a - b
            while abs(distance) <= threshold:
                if label_a != label_b:
                    distances.append([a, b, distance])
                j += 1
                if j >= len(all_positions):
                    break
                b, label_b = all_positions[j]
                distance = a - b
            
        return distances

    def generate_randomized_fasta(self, input_path, output_path):
        fasta = self.get_fastas_from_file(input_path)
        seq = fasta[0][1]
        # shuffle sequence
        writer = open(output_path, "w")
        j = 0
        print "writing to output:", output_path, "..."
        while j * 50 < len(seq):
            next_line = list(seq[j * 50: (j + 1) * 50])
            shuffle(next_line)
            next_line = "".join(next_line)
            writer.write(seq[j * 50: (j + 1) * 50] + "\n")
            j += 1
        writer.close()

    def get_fastas_from_file(self, fasta_path, as_dict=False):
        fastas = []
        seq = None
        header = None
        for r in open(fasta_path):
            r = r.strip()
            if r.startswith(">"):
                if seq != None and header != None:
                    fastas.append([header, seq])
                seq = ""
                header = r[1:]
            else:
                if seq != None:
                    seq += r
                else:
                    seq = r
        
        # append last fasta read by method
        fastas.append([header, seq])
        
        if as_dict:
            return {h: s for h, s in fastas}

        return fastas

    ### get motif IDs from TF name
    def get_motifid(self, tf_name):
        path = "/home/ignacio/Documents/zaugglab/Homo_sapiens_2015_06_12_9-24_am/TF_Information.txt"
        rows = [r.split("\t") for r in open(path) if tf_name in r.split("\t")[6]]
        rows = {r[3] for r in rows if r[8] == "D"}
        return rows
    
    def get_hit_positions_moods_output(self, moods_output_path):
        cmd = [ 'zcat', moods_output_path ]
        output = subprocess.Popen( cmd, stdout=subprocess.PIPE ).communicate()[0].split("\n")
        positions = []
        for r in output:
            split = r.split("|")
            if len(split) >= 2:
                positions.append(int(r.split("|")[0]))
        return positions
    
    def generate_non_redundant_table(self, distances_table):
        """
        Filter a new table using as reference values that taken from the reference
        table, applying rules to reduce redundancy
        """
        
        # Rule 1: ++ with positive distance == -- with negative distance
        distances_table = [["+", "+", -r[2]] if r[0] == "-" and r[1] == "-" and r[2] < 0 else r for r in distances_table] 
        return distances_table


    def show_hqtls_motif_sequences_at_distance(self, path, 
                                               category, edge_to_edge_distance,
                                               motif1_width=-1, motif2_width=-1):
        """
        Show sequences that contain composite motifs for given cases
        """
        snps_table = self.get_snps_table()
        fastas = self.get_hqtl_fastas()

        table = [r.split("\t") for r in open(path)]
        table = self.classify_by_architecture(table, motif1_width, motif2_width)
        table = {r[0]: r for r in table if r[-2] == edge_to_edge_distance and r[-1] == category}
        
        counts_by_mark = {}
        for snp_id in table:
            cm = table[snp_id]
            chromosome, position, start, end, mark = snps_table[snp_id]
            seq = fastas[chromosome + ":" + str(start) + "-" + str(end)].upper()
            motif1_start, motif1_end = int(cm[1]) - start, int(cm[1]) - start + motif1_width
            motif1_strand = cm[2]
            motif2_start, motif2_end = int(cm[3]) - start, int(cm[3]) - start + motif2_width
            motif2_strand = cm[4]
            if not mark in counts_by_mark:
                counts_by_mark[mark] = 1
            counts_by_mark[mark] += 1
             
            motif1_seq = seq[motif1_start: motif1_end]
            motif2_seq = seq[motif2_start: motif2_end]
            
            # print snp_id, snps_table[snp_id]
            print cm
            print (motif1_start - min(motif1_start, motif2_start)) * " " + motif1_seq
            print seq[min(motif1_start, motif2_start): max(motif1_end, motif2_end)]
            print (motif2_start - min(motif1_start, motif2_start)) * " " + motif2_seq
            print (position - start - min(motif1_start, motif2_start) - 1) * " " + "*"
            print ""
        print len(table)
        print counts_by_mark


    def classify_by_architecture_df(self, distances_df, w1, w2):
        """
        Classify our motifs in one out of four categories, according to the way
        these are organized

        (a) 1-> 2->
        (b) 2-> 1->
        (c) <-1 2->
        (d) 2-> <-1

        Also, calculate from now on the EDGE-TO-EDGE distance
        """
        architectures = []
        for ri, r in distances_df.iterrows():
            p1, p2 = r['start_x'], r['start_y']
            strand1, strand2 = r['strand_x'], r['strand_y']
            start1, end1 = p1, p1 + w1 - 1
            start2, end2 = p2, p2 + w2 - 1

            # array = ["" for j in range(0, max(end1, end2) - min(start1, start2))]
            category = None
            # motif 1 is before 2
            if start1 <= start2:
                edge_to_edge_distance = start2 - end1 - 1
                if strand1 == "+" and strand2 == "+":
                    category = "a"
                elif strand1 == "+" and strand2 == "-":
                    category = "d"
                elif strand1 == "-" and strand2 == "+":
                    category = "c"
                elif strand1 == "-" and strand2 == "-":
                    category = "b"
            # motif 2 is before 1: b or d
            elif start2 < start1:
                edge_to_edge_distance = start1 - end2 - 1
                if strand1 == "+" and strand2 == "+":
                    category = "b"
                elif strand1 == "+" and strand2 == "-":
                    category = "c"
                elif strand1 == "-" and strand2 == "+":
                    category = "d"
                elif strand1 == "-" and strand2 == "-":
                    category = "a"
            else:
                raise Exception("Orientation not found...")

            architectures.append([edge_to_edge_distance, category])
        distances_df['distance'] = [t[0] for t in architectures]
        distances_df['category'] = [t[1] for t in architectures]
        return distances_df

    def probabilities_to_counts(self, probabilities, sum_counts=10000):
        counts = []
        remaining_counts = sum_counts
        for i, p in enumerate(probabilities):
            curr_counts = min(int(p * sum_counts), remaining_counts)
            
            # last column: add the remaining reads (normally 1-3 more)
            if i == 3:
                curr_counts = remaining_counts

            remaining_counts -= curr_counts
            counts.append(curr_counts)
        return counts   
    
    def get_hqtl_fastas(self):
        """
        """
        fastas_dir = "/home/ignacio/Documents/zaugglab/localQTLs/snp_fastas"
        
        output = {}
        for f in listdir(fastas_dir):
            path = join(fastas_dir, f)
            fastas_dict = self.get_fastas_from_file(path, as_dict=True)
            for k in fastas_dict:
                output[k] = fastas_dict[k]
        return output

    def get_sequences_for_cm_hits(self, hits_table, motif1_width, motif2_width,
                                  stop_at_chromosome=None):
        """
        Given a table with PWM pair hits and a reference to its widths, return
        a table that include queried sequences
        """
        hg19_dir = "/home/ignacio/Documents/zaugglab/hg19"
        output_table = []
        print "reading chromosome data..."
        for chromosome in range(1, 23):
            if stop_at_chromosome is not None and chromosome > stop_at_chromosome:
                break
            print chromosome
            chromosome_path = join(hg19_dir, "chr" + str(chromosome) + ".fa")
            fasta = self.get_fastas_from_file(chromosome_path)
            seq = fasta[0][1]
            for t in hits_table:
                motif1_start, motif1_end = t[1], t[1] + motif1_width - 1
                motif2_start, motif2_end = t[3], t[3] + motif2_width - 1
                if t[0] == chromosome:
                    next_seq = seq[min(motif1_start, motif2_start): max(motif1_end, motif2_end) + 1]
                    output_table.append(t + [next_seq])
                    # print next_seq, motif1_width, motif2_width
        return output_table
    
    def generate_close_motifs_file(self, motif1_moods_file, motif2_moods_file,
                                   motif1_width, motif2_width,
                                   compress_gz=True, distance_threshold=100):
        """
        Intersect files using C routine. Output file contain recovered motifs
        genome-wide
        """
        f1, f2 = motif1_moods_file, motif2_moods_file
        
        moods_output_dir = "/home/ignacio/Documents/zaugglab/moods_genome_scanning/output"
        hqtls_overlap_dir="/home/ignacio/Documents/zaugglab/moods_genome_scanning/hqtls_overlap"
        binary = "/home/ignacio/Dropbox/ProjectsC/MOODSOutputAnalyzer/src/moodsOutputAnalyzer"
        moods_motif_pairs_dir = "/home/ignacio/Documents/zaugglab/moods_genome_scanning/close_motif_pairs"
        
        # get motifs width (are a necessary argument
        motif_id1, motif_id2 = [f.replace(".gz", "") for f in [f1, f2]]
        
        # the final output is a hQTL file: if this one exists we omit
        # scanning of composite motifs vs hqtl sequences:
        output_path_hqtl_dir = join(hqtls_overlap_dir, motif_id1)
        if not exists(output_path_hqtl_dir):
            mkdir(output_path_hqtl_dir)
        output_path_hqtls  = join(output_path_hqtl_dir, motif_id2)
        if exists(output_path_hqtls):
            remove(output_path_hqtls)
            # continue
    
        # get motifs that are close in each cromosome
        path1 = join(moods_output_dir, "chr$i", f1)
        path2 = join(moods_output_dir, "chr$i", f2)
        output_dir = join(moods_motif_pairs_dir, motif_id1)
        if not exists(output_dir):
            mkdir(output_dir)
        output_path = join(output_dir, motif_id2)
        output_path_gz = join(output_dir, f2)
        if exists(output_path_gz):
            remove(output_path_gz)
        for k in map(str, range(1, 23)):
            p1, p2 = [p.replace("$i", k) for p in (path1, path2)]
            if exists(p1) and exists(p2):
                print k
                cmd = " ".join([binary, p1, p2, k, 
                                str(distance_threshold), ">>", output_path])
                print cmd
                system(cmd)

        # once all files are concatenated, convert file into gz
        if compress_gz:
            print "compressing file..."
            cmd = " ".join(["gzip", output_path])
            system(cmd)
            # give the user the GZ path, so we can further analyze it
            return output_path_gz
    
    
    def intersect_cm_with_hqtls(self, motif1_id, motif2_id):
        hQTLs_bed_dir = "/home/ignacio/Documents/zaugglab/localQTLs/snp_beds/"
        hQTLs_intersect_binary = "/home/ignacio/Dropbox/ProjectsC/MOODSOutputAnalyzer/src/cmMotifsVsHQTLs"
        hqtls_overlap_dir="/home/ignacio/Documents/zaugglab/moods_genome_scanning/hqtls_overlap"
        moods_motif_pairs_dir = "/home/ignacio/Documents/zaugglab/moods_genome_scanning/close_motif_pairs"

        output_path_hqtl_dir = join(hqtls_overlap_dir, motif1_id)
        output_path_hqtls  = join(output_path_hqtl_dir, motif2_id)
        if exists(output_path_hqtls):
            remove(output_path_hqtls)

        # get motif widths 1 and 2
        pwms_dir = "/home/ignacio/Documents/zaugglab/Homo_sapiens_2015_06_12_9-24_am/pwms_all_motifs"
        # get motifs width (are a necessary argument
        w1, w2 = [self.get_motif_width(join(pwms_dir, k + ".txt"))
                  for k in [motif1_id, motif2_id]]
        
        input_path = join(moods_motif_pairs_dir, motif1_id, motif2_id + ".gz")
        
        assert exists(input_path)
        
        print "intersecting with hQTLs..."        
        cmd = " ".join([hQTLs_intersect_binary, hQTLs_bed_dir, 
                        input_path, str(w1), str(w2), ">>", output_path_hqtls])
        print cmd
        system(cmd)
            
        # remove GZ
        remove(input_path)
        rmdir(join(moods_motif_pairs_dir, motif1_id))     
    
    def get_sequences_from_bed(self, bed_path, input_fasta, output_fasta):
        """
        Given a BED path and a FASTA path, get FASTAS sequences from it.
        """
        args = ["fastaFromBed", "-bed", bed_path, "-fi", input_fasta,
                "-fo", output_fasta]
        args = " ".join(args)
        system(args)
        
    def intersect_cm_distances_table_with_localqtl(self, table, threshold=50,
                                                   motif1_width=None,
                                                   motif2_width=None):
        """
        Intersect found motifs with QTL sequences, if both are close to this
        """
        beds_dir = "/home/ignacio/Documents/zaugglab/localQTLs/snp_beds"
        fastas_dir = "/home/ignacio/Documents/zaugglab/localQTLs/snp_fastas"
        
        # get data by chromosome and by marker used
        filtered_chromosomes = {t[0] for t in table}
        
        counter = 0
        output_table = []
        
        cm_motifs_by_chr = {}
        for t  in table:
            chr_id = t[0]
            if not chr_id in cm_motifs_by_chr:
                cm_motifs_by_chr[chr_id] = []
            cm_motifs_by_chr[chr_id].append(t)

        for f in listdir(beds_dir):
            # print f
            chr_id = f.split(".")[1][3:]
            mark = f.split(".")[0]
            
            if mark != "H3K4ME1":
                continue
            # get data from table
            if chr_id in filtered_chromosomes:
                
                # avoid loading tables again for n-th case.
                start_end_table = None
                if not f in self.bed_tables:
                    bed_path = join(beds_dir, f)
                    rows = [r.strip().split("\t")[1:] for r in open(bed_path)]          
                    start_end_table = [[int(r[0]), int(r[1]), r[2]] for r in rows]
                    start_end_table.sort(key=lambda x: x[0])
                    self.bed_tables[f] = start_end_table
                else:
                    start_end_table = self.bed_tables[f]

                # fastas = self.get_fastas_from_file(join(fastas_dir, f.replace(".bed", ".fasta")),
                #                                    as_dict=True)      
                
                last_cm_motif_position = 0                
                curr_cm_motifs = cm_motifs_by_chr[chr_id]
                for start, end, snp_id in start_end_table:
                    curr_cm_motifs = curr_cm_motifs[last_cm_motif_position:]
                    for i, cm_motif in enumerate(curr_cm_motifs):
                        p1, p2 = int(cm_motif[1]), int(cm_motif[3])
                        motifs_distance = int(cm_motif[-1])
                        snp_position = (start + end) / 2
                        d1 = p1 - snp_position
                        d2 = p2 - snp_position
                        if abs(d1) <= threshold and abs(d2) <= threshold:
                            k = "chr" + chr_id + ":" + str(start) + "-" + str(end)
                            # flanking_sequence = fastas[k]
                            motif1_strand = cm_motif[3]
                            motif2_strand = cm_motif[4]
                            # motif1_width, motif2_width = len(motif1_seq), len(motif2_seq)
                            
                            overlap_snp = 0
                            
                            overlaps_motif1 = p1 <= snp_position and p1 + motif1_width - 1 >= snp_position
                            overlaps_motif2 = p2 <= snp_position and p2 + motif2_width - 1 >= snp_position
                            if(overlaps_motif1 or overlaps_motif2):
                                overlap_snp = 1
                            # print snp_id, snp_position, p1, p2
                            output_table.append([mark, snp_id, 
                                                 p1, motif1_strand,
                                                 p2, motif2_strand, overlap_snp])
                            
                            # if chr_id == "3" and overlap_snp:
                            #    print mark, cm_motif, motifs_distance
                            #    print d1, d2, start, end, snp_position, snp_id, overlap_snp
                            # self.print_snp_flanking_with_motifs(flanking_sequence, start,
                            #                                     motif1_seq, p1, motif1_strand,
                            #                                     motif2_seq, p2, motif2_strand)
                            if overlap_snp:
                                counter += 1
                            
                            last_cm_motif_position = i
                                # import sys; sys.exit()
                            
                        # motif positions are sorted increasingly
                        # so get out if we made it further to the right
                        if p1 > snp_position + 500 and p2 > snp_position + 500:
                            break
        # print counter
        return output_table
    
    def get_motif_width(self, motif_path):
        """
        It returns the width  of the analyzed motifs, as an integer
        The "10" number permits to filter empty lines 
        """
        return len([r for r in open(motif_path)
                    if len(r) > 10 and not r.startswith("Pos")])
        
    def get_motif_width_encode_id(self, encode_id):
        """
        Given an ENCODE table ID, return the width of the motif
        """
        pwm_dir = "/home/ignacio/Documents/zaugglab/encode_motifs/pwm_encode"
        path = join(pwm_dir, encode_id + ".txt")
        return self.get_motif_width(path)
    
    def intersect_peaks_with_motifs(self, motifs_bed_path, chip_seq_bed_path,
                                    motif_position_column=11):
        """
        Given two bed paths, intesect and remove
        """
        # print "running moods..."    
        intersected_bed_peaks = "/home/ignacio/Desktop/bed_path.bed"
        # run BED intersection with selected ChIP-seq peaks
        args = ["bedtools", "intersect", "-a", chip_seq_bed_path, "-b", motifs_bed_path,
                "-wo", "-sorted", ">>", intersected_bed_peaks]
        args = " ".join(args)        
        # print args
        system(args)
            
        # get amount of intersected peaks
        n_motifs = sum([1 for line in open(motifs_bed_path)])
        n_chip_seq = sum([1 for line in open(chip_seq_bed_path)])
        n_intersected_peaks = len(self.get_motif_positions_from_intersected_bed(intersected_bed_peaks,
                                                                                motif_position_column=motif_position_column).keys())
        
        remove(intersected_bed_peaks)
        
        return [n_motifs, n_chip_seq, n_intersected_peaks]
        
    def create_bed_file_from_moods_output(self, motifs_dir, tf_id, bed_path):
        """
        Given an ENCODE motif, create a BED path that contain all motif instances
        """
        # Scan to find motifs that are present in ChIP-seq peaks
        # create a single BED file that contains all moods motifs
        
        # chromosomes 1 to 22 are considered only        
        for chromosome in map(str, range(1, 23)):
            moods_path = join(motifs_dir, "chr" + chromosome, tf_id + ".gz")
            if not exists(moods_path):
                continue
            
            # print moods_path
            # create temporal bed files for all motifs that we are studying
            self.convert_moods_motifs_to_bed(moods_path, bed_path,
                                             chromosome, append=True)

    def get_motif_positions_from_intersected_bed(self, bed_path, motif_position_column=11):
        """
        Given a BED path that indicates 1) ChIP-seq peak sequences and
        2) motif positions, return a dictionary indicating the relationships
        between peaks and motifs.
        """
        rows = [r.strip().split("\t") for r in open(bed_path)]
        table = []
        for r in rows:
            table.append([r[0] + ":" + r[1] + "-" + r[2], int(r[-3])])
        output_dict = {}
        for k, v in table:
            if not k in output_dict:
                output_dict[k] = []
            output_dict[k].append(v)
        return output_dict

    def get_motif_width_pfm(self, pfm_path):
        print pfm_path
        """
        Return the amount of column in pfm file (4 x N values)
        """
        return len(open(pfm_path).readlines()[0].strip().split("\t"))
    
    def get_motifs_table_all_chromosomes(self, base_dir, motif_id):
        """
        Check the output of MOODS for all chromosomes, and it returns tables per
        chromosome, for all high-scoring motifs found
        """        
        tables_by_chromosome = {}
        for chromosome in map(str, range(1, 23)):
            tables_by_chromosome[chromosome] = []
            next_path = join(base_dir, "chr" + chromosome, motif_id + ".gz")
            if exists(next_path):
                tables_by_chromosome[chromosome] = [r.strip().split("\t") 
                                                    for r in open(next_path)]
        return tables_by_chromosome
        
    def convert_moods_motifs_to_bed(self, moods_path, bed_path, chromosome,
                                    append=True, additional_labels=[],
                                    print_command=False):
        
        # print moods_path
        # print bed_path
        
        if len(additional_labels) == 0:
            args = ["zcat", moods_path, "|", "awk", "-F", "\"|\"",
                    "\'{print \"chr" + chromosome + "\\t\"$1\"\\t\"$1+1\"\\t\"$2\"\\t\"$3\"\\t\"$4}\'"]
        else:
            args = ["zcat", moods_path, "|", "awk", "-F", "\"|\"",
                    "\'{print \"chr" + chromosome + "\\t\"$1\"\\t\"$1+1\\t$2\"\\t" +
                    "".join([lab + "\\t" for lab in additional_labels])[:-2] + "\"}\'"]
                    
        if bed_path != "STDOUT":
            args += ["|", "gzip", ">>" if append else ">", bed_path]

        args = " ".join(args)
        system(args)
        
    def get_complementary_nt(self, nt):
        if nt == "A":
            return "T"
        if nt == "T":
            return "A"
        if nt == "C":
            return "G"
        if nt == "G":
            return "C"
        if nt == "N":
            return "N"
       
    def get_snps_table(self):
        """
        Load SNP references to CHR, start and end position
        """
        snps_table = {}
        beds_dir = "/home/ignacio/Documents/zaugglab/localQTLs/snp_beds"
        for f in listdir(beds_dir):
            mark = f.split(".")[0]
            for r in open(join(beds_dir, f)):
                chromosome, start, end, snp_id = r.strip().split("\t")
                start, end = int(start), int(end)
                snp_position = (start + end) / 2
                snps_table[snp_id] = [chromosome, snp_position, start, end, mark]
        return snps_table
                
    def get_complementary_seq(self, s):
        return "".join([self.get_complementary_nt(nt) for nt in s][::-1])

    def print_snp_flanking_with_motifs(self, dna_seq, dna_start,
                                       motif1_seq, motif1_start, motif1_strand,
                                       motif2_seq, motif2_start, motif2_strand):
        """
        
        """
        s1 = motif1_seq if motif1_strand == "+" else self.get_complementary_seq(motif1_seq)
        s2 = motif2_seq if motif2_strand == "+" else self.get_complementary_seq(motif2_seq)

        print " " * (len(dna_seq) / 2) + "*"
        print " " * (motif1_start - dna_start) + s1
        print dna_seq
        print " " * (motif2_start - dna_start) + s2
        
        return
    
    def get_number_of_lines(self, path, is_gz=False):
        if is_gz:
            zcat = subprocess.Popen(["zcat", path], stdout=subprocess.PIPE)
            output = subprocess.check_output(('wc', '-l'), stdin=zcat.stdout)
            return int(output)
        return sum(1 for line in open(path))
    
    def get_n_intersections(self, bed_path1, bed_path2, log=False):
        """
        Given two sorted BED paths, get the amount of common peaks between both files
        """
        args = ["bedtools", "intersect", "-a", bed_path1, "-b", bed_path2]
        if log:
            print " ".join(args)
        bedtools = subprocess.Popen(args, stdout=subprocess.PIPE)
        output = subprocess.check_output(('wc', '-l'), stdin=bedtools.stdout)
        return int(output)
    
    def get_intersections_same_start(self, bed_path1, bed_path2, log=False):
        """
        Given two sorted BED paths, get the amount of common peaks between both files
        """
        bedtools = ["bedtools", "intersect", "-a", bed_path1, "-b", bed_path2, "-wb"]
        out1 = subprocess.Popen(bedtools, stdout=subprocess.PIPE)
        
        awk = ["awk", "-F", r"\t",
               "BEGIN {OFS=\"\t\"}{if($1 == $7 && $2 == $8 && $4 == $12){print $1,$2,$4,$5,$6,$11,$12}}"]
        if log:
            print " | ".join([" ".join(bedtools), " ".join(awk)])
        out2 = subprocess.Popen(awk, stdin=out1.stdout, stdout=subprocess.PIPE).communicate()[0]
        
        # convert awk output into a python table        
        table = [t for t in [r.split("\t") for r in out2.split("\n")] if len(t) > 1]
        return table
    
    def get_encode_motifs_table(self, home_dir="LOCAL"):
        """
        Read table that relates ID numbers to protein names, experiments, cell lines, etc.
        """
        
        encode_table_path = "/home/ignacio/Documents/zaugglab/encode_motifs/ids_table.tsv"
        if home_dir == "CLUSTER":
            encode_table_path = "/home/rio/zaugglab/rio/encode_motifs/ids_table.tsv"
        encode_table = [r.strip() for r in open(encode_table_path)]
        encode_tfs = {}
        for r in encode_table:
            split = r.split("\t")
            encode_tfs[split[0]] = " ".join(split[1:])
        return encode_tfs
    
    def sort_bed_file(self, input_path, output_path=None):
        if output_path is not None:
            system(" ".join(["sort", "-k", "1,1", "-k2,2n", input_path, ">", output_path]))
        else:
            system(" ".join(["sort", "-k", "1,1", "-k2,2n", input_path, "-o", input_path]))
        
    def get_coenriched_motifs(self, coenriched_motifs_path):        
        rows = [r.strip().split("\t") for r in gzip.open(coenriched_motifs_path)]
        rows = [[r[0], int(r[1]), r[2], int(r[3]), r[4]] for r in rows]
        return rows
    
    def get_best_coenriched_motifs(self, coenriched_motifs_summary_path, stop_at=-1):
        rows = [r.strip().split("\t") for r, i in
                zip(gzip.open(coenriched_motifs_summary_path),
                    range(stop_at if stop_at != -1 else 10000))]

        rows = [[r[0], int(r[1]), int(r[2]), int(r[3]), float(r[4]), float(r[5])]
                for r in rows]
        return rows

    def intersect_composite_motif_coordinates_with_bed(self, distances_table, bed_path,
                                                       motif_position_column=11):
        """
        Get a list of bed entries for all coordinate pairs that are present in the
        bed path 
        """
        
        tmp_bed_path = "tmp_bed.bed"
        # convert distances table into a moods output file
        writer = open(tmp_bed_path, "w")
        for t in distances_table:
            a, b = [t[1], t[3]] if t[1] < t[3] else [t[3], t[1]]
            writer.write("\t".join(map(str, ["chr" + t[0], a, b])) + "\n")
        writer.close()

        # sort
        self.sort_bed_file(tmp_bed_path, "tmp_bed2.bed")
        remove(tmp_bed_path)
        move("tmp_bed2.bed", tmp_bed_path)

        n_motifs, n_peaks, n_hits = self.intersect_peaks_with_motifs(tmp_bed_path, bed_path,
                                                                     motif_position_column=motif_position_column)
        
        remove(tmp_bed_path)
        return [n_motifs, n_peaks, n_hits]
        import sys; sys.exit()
                
        # read bed file
        rows = [r.split("\t") for r in open(bed_path)]
        bed_table = {k: [] for k in range(1, 21)}
        for r in rows:
            k = r[0].replace("chr", "")
            if k == "Y" or k == "X":
                continue
            k = int(k)
            if k in bed_table:
                bed_table[k].append([int(r[1]), int(r[2])])
        
        intersect_peaks = []
        for chromosome in range(1, 21):
            by_chromosome = filter(lambda x: x[0] == str(chromosome), distances_table)
            bed_at_chromosome = bed_table[chromosome]
            # print chromosome, len(by_chromosome), len(bed_at_chromosome)
              
                      
            # print by_chromosome[0]
            for entry in by_chromosome:
                start, end = entry[1], entry[3]
                start, end = [start, end] if start < end else [end, start]
                for bed_start, bed_end in bed_at_chromosome:
                    if bed_start <= start and end <= bed_end:
                        intersect_peaks.append([chromosome, bed_start, bed_end])
        return intersect_peaks
       
    def get_random_peaks_from_bed(self, bed_path, n, output_path):
        rows = [r for r in open(bed_path)]
        flags = [1 for i in range(n)] + [0 for i in range(len(rows) - n)]
        shuffle(flags)
        writer = open(output_path, "w")
        for line, flag in zip(rows, flags):
            if flag:
                writer.write(line)
        writer.close()
        
    def get_random_subset_from_list(self, elements, n):
        flags = [1 for i in range(n)] + [0 for i in range(len(elements) - n)]
        shuffle(flags)
        new_elements = [line for line, flag in zip(elements, flags) if flag == 1]
        return new_elements
                 
    def classify_by_architecture(self, distances_table, motif1_width, motif2_width):
        """
        Classify our motifs in one out of four categories, according to the way
        these are organized
        
        (a) 1-> 2->
        (b) 2-> 1->
        (c) <-1 2->
        (d) 2-> <-1
        
        Also, calculate from now on the EDGE-TO-EDGE distance
        """
        for i in xrange(len(distances_table)):
            t = distances_table[i]
            p1, p2 = t[1], t[3]
            strand1, strand2 = t[2], t[4]
            start1, end1 = p1, p1 + motif1_width - 1
            start2, end2 = p2, p2 + motif2_width - 1
            
            # array = ["" for j in range(0, max(end1, end2) - min(start1, start2))]
            category = None
            # motif 1 is before 2
            if start1 <= start2:
                edge_to_edge_distance = start2 - end1 - 1
                if strand1 == "+" and strand2 == "+":
                    category = "a"
                elif strand1 == "+" and strand2 == "-":
                    category = "d"
                elif strand1 == "-" and strand2 == "+":
                    category = "c"
                elif strand1 == "-" and strand2 == "-":
                    category = "b"
            # motif 2 is before 1: b or d
            elif start2 < start1:
                edge_to_edge_distance = start1 - end2 - 1
                if strand1 == "+" and strand2 == "+":
                    category = "b"
                elif strand1 == "+" and strand2 == "-":
                    category = "c"
                elif strand1 == "-" and strand2 == "+":
                    category = "d"
                elif strand1 == "-" and strand2 == "-":
                    category = "a"
            else:
                raise Exception("Orientation not found...")
            
            distances_table[i] = t + [edge_to_edge_distance, category]
        return distances_table
    
    def get_significant_cm_motifs_table(self):
        encode_tfs = self.get_encode_motifs_table()
        best_hits_path = join("/home/ignacio/Dropbox/Eclipse_Projects/zaugglab",
                              "composite_motifs_analysis/output/best_cm_hits_non_overlapping.tsv.gz")
        table = [r.split("\t") for r in gzip.open(best_hits_path)]
        final_table = []
        for t in table:
            id1, id2 = t[0:2]
            category, overlap = t[2], int(t[3])
            freq, random_freq, z_score, pval = int(t[4]), int(t[5]), float(t[-2]), float(t[-1])
            tf1, tf2 = [encode_tfs[v].split(" ")[0] for v in [id1, id2]]
            final_table.append([tf1, tf2, id1, id2, category, overlap, freq,
                                random_freq, z_score, pval])
        return final_table
        
    def get_motif_coenrichment_table(self):
        encode_tfs = self.get_encode_motifs_table()
        root_dir = "/home/ignacio/Documents/zaugglab/moods_genome_scanning/close_motif_pairs_summary/positive"
        final_table = []
        for d in listdir(root_dir):
            next_dir = join(root_dir, d)
            for f in listdir(next_dir):
                next_path = join(next_dir, f)
                table = [r.split("\t") for r in gzip.open(next_path)]
                for t in table:
                    category, distance, freq, pval = t[0], int(t[1]), int(t[2]), float(t[-1])
                    if abs(distance) <= 8:
                        id1, id2 = d, f.replace(".gz", "")
                        if id1 in encode_tfs and id2 in encode_tfs:
                            tf1, tf2 = [encode_tfs[v].split(" ")[0] for v in [id1, id2]]
                            if tf1.split("_")[0] == tf2.split("_")[0]:
                                continue
                            final_table.append([tf1, tf2, id1, id2, category,
                                                distance, freq, pval])                    
        final_table.sort(key = lambda x: x[-1])
        return final_table
    
    def close_motifs_to_cm_table(self, close_motifs_path, motif_width1, motif_width2,
                                 use_cbinary=True, only_architecture=False, location="LOCAL"):
        """
        Take a bunch of close motifs and convert them into a table that
        indicates the architecture for each pair
        """
        distances_table = []
        w1, w2 = motif_width1, motif_width2
        if use_cbinary:
            binary = "/home/ignacio/Dropbox/ProjectsC/MOODSOutputAnalyzer/src/classifyByCMArchitecture"
            if location == "CLUSTER":
                binary = join("/scratch/rio/moods_genome_scanning/ProjectsC", "MOODSOutputAnalyzer/src/classifyByCMArchitecture")
            cmd = [binary, close_motifs_path, w1, w2]
            print cmd            
            output = subprocess.Popen( map(str, cmd), stdout=subprocess.PIPE ).communicate()[0]
            if only_architecture:
                distances_table = [r[0:5] + [int(r[5]), r[6]]
                                   for r in [r.split("\t") for r in output.split("\n") if len(r) != 0]]
            else:
                distances_table = [[int(r[5]), r[6]]
                                   for r in [r.split("\t") for r in output.split("\n") if len(r) != 0]]
        else: # classify using python method
            table = [r for r in [s.strip().split("\t") for s in gzip.open(close_motifs_path)]]
            table = [[r[0], int(r[1]), r[2], int(r[3]), r[4]] for r in table]
            distances_table = self.classify_by_architecture(table, w1, w2)
        return distances_table
        
    def plot_coenrichments(self, distances_table, threshold_min=-25,
                           threshold_max=50,
                           restart_figure=True, show=False,
                           random_distances_table=None, label="",
                           show_histograms=False, output_path=None):
        """
        Count the # of hits found for each PWM pair genome wide
        
        random_distances_table refers to a randomized set of distances
        """        
        # lines plot
        if show:
            fig = plt.figure(figsize=(20, 10))
            if show_histograms:
                gs = gridspec.GridSpec(4, 2, width_ratios=[9, 3])
            else:
                gs = gridspec.GridSpec(4, 1)
            counter = 0
        
        output_table = []
        
        # distances_table = self.generate_non_redundant_table(distances_table)
        max_y = 0
        for category, title in zip(["a", "b", "c", "d"], 
                                   ["1-> 2->", "2-> 1->", "<-1 2->", "1-> <-2"]):
            distances = [r[-2] for r in distances_table if r[-1] == category]
            print distances
            x = [i for i in range(threshold_min, threshold_max, 1)]
            y = {i: 0 for i in range(threshold_min, threshold_max, 1)}
            for d in distances:
                if d in y:
                    y[d] += 1
            y = [y[d] for d in range(threshold_min, threshold_max, 1)]
            max_y = max(max_y, max(y))
            
        for category, title in zip(["a", "b", "c", "d"], 
                                   ["1-> 2->", "2-> 1->", "<-1 2->", "1-> <-2"]):

            distances = [r[-2] for r in distances_table if r[-1] == category]
            x = [i for i in range(threshold_min, threshold_max, 1)]
            y = {i: 0 for i in range(threshold_min, threshold_max, 1)}
            for d in distances:
                if d in y:
                    y[d] += 1
            y = [y[d] for d in range(threshold_min, threshold_max, 1)]
            
            # random distances (CONTROl)
            random_distances, x_rand, y_rand = None, None, None
            if random_distances_table != None:
                random_distances = [r[-2] for r in random_distances_table if r[-1] == category]
                y_rand = {i: 0 for i in range(threshold_min, threshold_max, 1)}
                for d in random_distances:
                    if d in y_rand:
                        y_rand[d] += 1
                y_rand = [y_rand[d] for d in range(threshold_min, threshold_max, 1)]

            if y_rand != None:
                for xi, yi, yi_rand in zip(x, y, y_rand):
                    output_table.append([category, xi, yi, yi_rand])
            else:
                for xi, yi in zip(x, y):
                    output_table.append([category, xi, yi])
                                
            # this keeps a ranking of the best frequencies that we have found
            ranked_table = [[xi, yi] for xi, yi in zip(x, y)]
            ranked_table.sort(key=lambda x: x[-1], reverse=True)
            # get the cutoffs for the top 1% and the top 5% of the found enrichments
            y_sorted = sorted(y, reverse=True)
            positions = [i for i in range(len(y))]
            # define the point in which reported distance as the top 5% of out data
            cdf_y = [float(i + 1) / len(y) for i in range(len(y))]
            cutoff_1p, cutoff_5p = [y_sorted[max([xi for xi, pi in zip(positions, cdf_y) if pi < t])]
                                    for t in (0.1, 0.5)]
        
            if show:
                ax0 = plt.subplot(gs[counter])
                counter += 1
                ax0.plot(x, y, color="blue")
                if y_rand != None:
                    ax0.plot(x, y_rand, color="orange")
                [ax0.annotate(str(t[0]), xy=[t[0], t[1]], fontsize=18,
                              ha="center") for t in ranked_table[:5]]
                plt.title(title + ", N=" + str(len(distances)), fontsize=18)
                plt.axhline(cutoff_1p, ls="--", color="r", lw=1.0)
                plt.axhline(cutoff_5p, ls=":", color="r", lw=2.0)
                
                fontsize_ticks = 17
                for tick in ax0.xaxis.get_major_ticks():
                    tick.label.set_fontsize(fontsize_ticks) 
                for tick in ax0.yaxis.get_major_ticks():
                    tick.label.set_fontsize(fontsize_ticks) 

                plt.xlim([threshold_min, threshold_max])
                plt.ylim([0, max_y * 1.1])
                if counter != 4:
                    ax0.axes.get_xaxis().set_visible(False)
                plt.ylabel("# events", fontsize=18)
                print counter
                if counter == 4 and not show_histograms:
                    plt.xlabel('Edge to edge distance between motifs')
                
                # histogram
                if show_histograms:
                    ax0 = plt.subplot(gs[counter])
                    print y
                    ax0.hist(y, bins=20, orientation="horizontal", color="g",
                             alpha=0.7)
                    plt.axhline(cutoff_1p, ls="--", color="r", lw=1.0)
                    plt.axhline(cutoff_5p, ls=":", color="r", lw=2.0)
                    plt.ylim([0, max_y * 1.1])
                    # plt.xlim([0, 1])
                    for tick in ax0.xaxis.get_major_ticks():
                        tick.label.set_fontsize(fontsize_ticks)
                    for tick in ax0.yaxis.get_major_ticks():
                        tick.label.set_fontsize(fontsize_ticks)
                    # ax0.yaxis.tick_right()
                    # ax0.yaxis.set_ticks_position('both')
                    # plt.xlabel()
                    # ax0.axes.get_xaxis().set_visible(False)
                    ax0.axes.get_yaxis().set_visible(False)
                

        if show:
            if show_histograms:
                fig.text(0.43 if show_histograms else 0.5, 0.02 if show_histograms else 0.0,
                         'Edge to edge distance between motifs',
                         ha='center', va='center', fontsize=18)
            if show_histograms:
                fig.text(0.86, 0.02,
                         '# architectures',
                         ha='center', va='center', fontsize=18)
            fig.suptitle(label)
            fig.tight_layout()
            if output_path is not None:
                savefig(output_path)
                plt.close()
            else:
                plt.show()
            
        return output_table
        
        
    def get_motif_files(self, output_dir):
        """
        get all the motifs that are listed at least once in out output directory
        """
        motif_hit_moods_files = set()
        for d in listdir(output_dir):
            chr_dir = join(output_dir, d)
            for f in listdir(chr_dir):
                motif_hit_moods_files.add(f)
        return motif_hit_moods_files