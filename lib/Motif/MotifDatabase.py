

from lib.utils import *

class MotifDatabase:
    motifs_dir = '/g/scb2/zaugg/zaugg_shared/data/motifs'

    # GENRE (8mer modules) - metazoan
    GENRE_GLOSSARY = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/genre/data/glossary/motifs/glossary-kmer'

    # JASPAR2018 - vertebrates
    JASPAR2018_VERTEBRATES = join(motifs_dir, 'JASPAR2018/vertebrates_homer_all.txt')
    JASPAR2018_ANNOTATION_MOUSE = join(motifs_dir, 'JASPAR2018/vertebrates_homer_all_annotation_mouse.txt')
    JASPAR2018_ANNOTATION_HUMAN = join(motifs_dir, 'JASPAR2018/vertebrates_homer_all_annotation_human.txt')

    # mm10
    # HOCOMOCO mouse
    HOCOMOCOv11_full_MOUSE_mono_homer_format_stringent = join(motifs_dir,
                                                              'HOCOMOCO/mouse/HOCOMOCOv11_full_MOUSE_mono_homer_format_0.0001.motif')
    HOCOMOCOv11_full_MOUSE_mono_homer_format_medium = join(motifs_dir,
                                                           'HOCOMOCO/mouse/HOCOMOCOv11_full_MOUSE_mono_homer_format_0.0005.motif')
    HOCOMOCOv11_full_MOUSE_mono_homer_format_weak = join(motifs_dir,
                                                         'HOCOMOCO/mouse/HOCOMOCOv11_full_MOUSE_mono_homer_format_0.001.motif')
    HOCOMOCOv11_annotation_mouse = join(motifs_dir, 'HOCOMOCO/mouse/HOCOMOCOv11_full_annotation_MOUSE_mono_w_ensembl.tsv')
    # CISBP - mouse
    CISBP_MOUSE = join(motifs_dir, 'CISBP/mouse/pwms_all_motifs_homer_all.txt')
    CISBP_annotation_mouse = join(motifs_dir, 'CISBP/mouse/TF_Information_all_motifs_plus.txt')

    # hg19
    # HOCOMOCO HUMAN
    HOCOMOCOv11_full_HUMAN_mono_homer_format_stringent = join(motifs_dir,
                                                              'HOCOMOCO/human/HOCOMOCOv11_full_HUMAN_mono_homer_format_0.0001.motif')
    HOCOMOCOv11_annotation_human = join(motifs_dir,
                                        'HOCOMOCO/human/HOCOMOCOv11_full_annotation_HUMAN_mono_w_ensembl.tsv')
    # CISBP - HUMAN
    CISBP_HUMAN = join(motifs_dir, 'CISBP/human/pwms_all_motifs_homer_all.txt')
    CISBP_annotation_human = join(motifs_dir, 'CISBP/human/TF_Information_all_motifs_plus.txt')


