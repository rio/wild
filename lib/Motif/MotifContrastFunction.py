

from lib.utils import *
from lib.MachineLearning import MachineLearning
from scipy.stats import fisher_exact
import pandas as pd

class MotifContrastFunction:

    @staticmethod
    def get_r_glm_logistic_pvalue(input, alternative='greater', **kwargs):
        '''
        Return a chisq and a pvalue of a model that assesses label ~ score + gc.content versus label ~ gc.content (binomial)
        :param input:
        :param alternative:
        :param kwargs:
        :return:
        '''
        # RFacade related functions
        print 'importing rpy2'
        import rpy2
        import rpy2.robjects as robjects
        from rpy2.robjects import pandas2ri
        from rpy2.robjects.vectors import FloatVector, StrVector
        pandas2ri.activate()  # to convert pandas to R dataframe

        covariates = kwargs.get('covariates', None)
        if covariates is None:
            print 'this function requires covariates to be defined.' \
                  'Please add them to the input, calculate them locally, or remove this function from the library'
            return Exception
        # add covariate to input dataframe before calling R
        covariates_dict = DataFrameAnalyzer.get_dict(covariates, 'header', 'gc.content')
        input['gc.content'] = input['header'].map(covariates_dict)
        r = rpy2.robjects.r
        if(not r['exists']('glm_logistic')[0]):
            print 'importing function'
            r.source('/g/scb/zaugg/rio/EclipseProjects/wild/lib/rscripts/motif_contrasts.R')
        loglk, pval = r['glm_logistic'](pandas2ri.py2ri(input))
        return pd.DataFrame([['glm_logit', loglk, pval]],
                            columns=['score.type', 'score', 'p.val'])

    @staticmethod
    def get_auroc(input, alternative=None, **kwargs):
        from lib.RFacade import RFacade
        score_k = 'score'
        na = input[input['label'] == 1].shape[0]
        nb = input[input['label'] == 0].shape[0]
        auroc, auprc = MachineLearning.get_auroc_n_auprc(input['label'], input[score_k])
        pval = RFacade.get_wilcox_test_pval(input[input['label'] == 1][score_k],
                                            input[input['label'] == 0][score_k],
                                            alternative='two.sided' if alternative is None else alternative)
        return pd.DataFrame([[na, nb, 'AUROC', auroc, pval]],
                            columns=['na', 'nb', 'score.type', 'score', 'p.val'])
    
    
    @staticmethod
    def get_fold_change(input, alternative=None, **kwargs):
        a = input[(input['label'] == 1) & (input['select.motif.p.val'] == 1)].shape[0]
        b = input[(input['label'] == 1) & (input['select.motif.p.val'] == 0)].shape[0]
        c = input[(input['label'] == 0) & (input['select.motif.p.val'] == 1)].shape[0]
        d = input[(input['label'] == 0) & (input['select.motif.p.val'] == 0)].shape[0]
        
        alternative='two-sided' if alternative is None else alternative
        score, pval = list(fisher_exact([[a, b], [c, d]], alternative=alternative))
        ratio1, ratio2 = (a + 1) / (a + b + 1.0), (c + 1.0) / (c + d + 1.0)
        fc = None
        if ratio1 >= ratio2:
            fc = ratio1 / ratio2 - 1
        else:
            fc = -(((ratio2 / ratio1)) - 1)

        return pd.DataFrame([['fold.change', fc, pval]],
                            columns=['score.type', 'score', 'p.val'])
                            
    @staticmethod
    def get_odds_ratio(input, alternative=None, **kwargs):
        a = input[(input['label'] == 1) & (input['select.motif.p.val'] == 1)].shape[0]
        b = input[(input['label'] == 1) & (input['select.motif.p.val'] == 0)].shape[0]
        c = input[(input['label'] == 0) & (input['select.motif.p.val'] == 1)].shape[0]
        d = input[(input['label'] == 0) & (input['select.motif.p.val'] == 0)].shape[0]
        
        alternative='two-sided' if alternative is None else alternative
        score, pval = list(fisher_exact([[a, b], [c, d]], alternative=alternative))
        return pd.DataFrame([[a, b, c, d, 'odds.ratio', score, pval]],
                            columns=['a', 'b', 'c', 'd', 'score.type', 'score', 'p.val'])