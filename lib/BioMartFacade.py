
from lib.utils import *

class BioMart:


    def get_ensg(self, gene_names, output_path):
        '''
        Given a table with gene names, get a table with ENSG
        :param gene_names:
        :param output_path:
        :return:
        '''

        tmppath = tempfile.mkstemp()[1]

        writer = open(tmppath, 'w')
        for g in gene_names:
            writer.write("\n")

