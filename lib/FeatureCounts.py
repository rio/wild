'''
Created on 7/22/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *


class FeatureCounts:

    @staticmethod
    def get_counts(coordinates, bam_path, tmp_input=None, tmp_output=None, fmt='SAF', n_threads=1):

        if tmp_input is None or not exists(tmp_input):
            tmp_input = tempfile.mkstemp()[1]
        if tmp_output is None or not exists(tmp_output):
            tmp_output = tempfile.mkstemp()[1]

        coordinates = coordinates[coordinates.columns[:3]].reset_index(drop=True)
        coordinates['id'] =  coordinates.index
        coordinates = coordinates[['id'] + list(coordinates.columns[:3])]
        coordinates['strand'] = '+'
        DataFrameAnalyzer.to_tsv(coordinates, tmp_input, header=None)

        cmd = ' '.join(['featureCounts', '-F', fmt, '-T', str(n_threads),
                        '-a',  tmp_input, '-o', tmp_output, bam_path])
        print(cmd)
        system(cmd)
        out = DataFrameAnalyzer.read_tsv(tmp_output, skiprows=1)

        # clean output
        remove(tmp_input)
        remove(tmp_output)

        if exists(tmp_input + ".summary"):
            remove(tmp_input + ".summary")
        if exists(tmp_output + ".summary"):
            remove(tmp_output + ".summary")

        # print out.head()
        return out