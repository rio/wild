'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.BigWigAnalyzer import BigWigAnalyzer

from lib.BAMAnalyzer import BAMAnalyzer
class RoadMapAnalyzer:

    @staticmethod
    def get_bigwig_paths(histone_mark):
        basedir = '/g/scb2/zaugg/rio/data/Roadmap/fold_enrichment'
        d = join(basedir, histone_mark)
        assert exists(d)

        df = pd.read_excel('../../data/jul2013.roadmapData.qc.xlsx')
        tissue_by_ind = DataFrameAnalyzer.get_dict(df, 'Epigenome ID (EID)', 'GROUP')
        paths = []
        for f in listdir(d):
            if not f.endswith('bigwig'):
                continue
            bigwig_path = join(d, f)
            paths.append(bigwig_path)
        return paths

    @staticmethod
    def get_tissue_names():
        basedir = '/g/scb2/zaugg/rio/data/Roadmap/by_experiment/H3K4me3'
        return {d for d in listdir(basedir)}

    @staticmethod
    def get_bams_by_mark_n_tissue(mark, tissues=None):
        basedir = '/g/scb2/zaugg/rio/data/Roadmap/by_experiment/' + mark
        if not exists(basedir):
            print(exists(basedir), basedir)
            assert exists(basedir)
        bam_paths = []
        if isinstance(tissues, str):
            tissues = {tissues}
        for d in listdir(basedir):
            if tissues is not None and not d in tissues:
                continue
            for f in listdir(join(basedir, d)):
                if not f.endswith('.sorted.bam'):
                    continue
                bam_paths.append(join(basedir, d, f))
        return bam_paths

    @staticmethod
    def get_tracks(histone_mark, coordinates, query_tissues=None, query_ind=None, bymean=False):
        '''
        Get query_tracks in all tissues
        :param histone_mark:
        :param coordinates:
        :param query_tissues:
        :return:
        '''
        basedir = '/g/scb2/zaugg/rio/data/Roadmap/fold_enrichment'
        d = join(basedir, histone_mark)
        assert exists(d)

        df = pd.read_excel('../../data/jul2013.roadmapData.qc.xlsx')
        tissue_by_ind = DataFrameAnalyzer.get_dict(df, 'Epigenome ID (EID)', 'GROUP')
        table = []
        for f in listdir(d):
            if not f.endswith('bigwig'):
                continue
            try:
                bigwig_path = join(d, f)
                ind = f.split('-')[0]
                if query_tissues is not None and not tissue_by_ind[ind] in query_tissues:
                    continue
                if query_ind is not None and not ind in query_ind:
                    continue
                print('getting tracks from')
                print(exists(bigwig_path), bigwig_path, 'N=%i' % len(coordinates))
                tracks = BigWigAnalyzer.get_tracks(bigwig_path, coordinates)
                print('done...')
                for k in tracks:
                    if bymean:
                        fold_enrichments = tracks[k]
                        table.append([ind, k, 'mean', average(fold_enrichments), tissue_by_ind[ind]])
                    else:
                        print(len(tracks[k]))
                        for pi, fold_enrichment in enumerate(tracks[k]):
                            table.append([ind, k, pi, fold_enrichment, tissue_by_ind[ind]])
            except Exception:
                print('failed...')
                assert 1 > 2
                continue

        cols = ['individual', 'coordinate', 'position', 'fold.enrichment', 'tissue']
        if bymean:
            cols[2] = 'mean'
        df = pd.DataFrame(table, columns=cols)
        return df

        # get mylt1 tracks