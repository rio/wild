
import tempfile
import gzip

def uncompress_and_get_tmp_path(gz_path, **kwargs):
    ignore_header = kwargs.get('ignore_header', None)
    ignore_empty =  kwargs.get('ignore_empty', False)
    replace_dict = kwargs.get("replace_dict", None)
    skiprows = kwargs.get('skiprows', -1)
    tmppath = tempfile.mkstemp()[1]
    with open(tmppath, 'w') as writer:
        for i, line in enumerate(gzip.open(gz_path)):
            if i < skiprows:
                continue
            if ignore_header is not None:
                if isinstance(ignore_header, str) and line.startswith(ignore_header):
                    continue
                elif isinstance(ignore_header, list):
                    skip = False
                    for k in ignore_header:
                        if line.startswith(k):
                            skip = True
                    if skip:
                        continue
            if ignore_empty and len(line.strip()) == 0:
                continue
            if replace_dict is not None:
                for k in replace_dict:
                    if k in line:
                        line = line.replace(k, replace_dict[k])

            writer.write(line)
    return tmppath
