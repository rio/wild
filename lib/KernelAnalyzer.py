
# implementation of method to run seq + shape kernel classifier (Wenxiu, Yang, Rohs and Noble)
from lib.utils import *

class KernelClassifier:
    def run_seq_n_shape_svm(self, sequences_path, labels_path, output_path, k=4,
                            cvfold=10, shape=False, max_feat_num=None,
                            nokmer=False, random_shape=False):
        args = list(map(str, ["python", 'svm-sequence-shape.py',
                         "-x", sequences_path, "-y", labels_path,
                         "-k", k, "-o", output_path, "-c", cvfold]))

        if shape:
            args += ["-s", "shape"]
        if max_feat_num is not None:
            args += ["-f", str(max_feat_num)]
        if nokmer:
            args += ['--nokmer']
        if random_shape:
            args += ['--randomshape']

        print(' '.join(args))
        system(" ".join(args))

    def scrambled_shape_features(self, shape_features_path, output_path):

        df = pd.read_csv(shape_features_path, sep='\t', index_col=None)

        print(df.head())
        print(df.columns)
        for k in df.columns:
            if k.endswith("ave"):
                df[k] = np.random.permutation(df[k])
        df.to_csv(output_path, sep='\t', index=None)
        exit()