
from os.path import exists, join
from os import makedirs
import sys
from lib.utils import *
from os import system
import argparse
from .DNAshapedTFBS.argparsing import *
from lib.DNAshapedTFBS.shapes import *
from lib.DNAshapedTFBS.DNAshapedTFBS import find_pssm_hits, train_classifier
from lib.BSubFacade import BSubFacade
import tempfile
import time

class DNAAugmentedModelsAnalyzer():

    def run_training_and_testing(self,
                                 pos_fasta_train, pos_bed_train,
                                 neg_fasta_train, neg_bed_train,
                                 pos_fasta_test, pos_bed_test,
                                 neg_fasta_test, neg_bed_test,
                                 motif_path_jaspar, motif_id, ki,
                                 big_wig_dir,
                                 output_dir,
                                 bsub_output_dir="../output/bsub_output",
                                 **kwargs):


        all_bed_paths, all_fasta_paths = [kwargs.get(k, None) for k in ('all_beds', "all_fastas")]

        in_scores_pos, in_scores_neg = [kwargs.get(k, None) for k in ('in_scores_pos', "in_scores_neg")]

        print(all_bed_paths)
        print(all_fasta_paths)
        stop_at = kwargs.get("stop_at", -1)
        create_bsubmit = kwargs.get('create_bsubmit', False)
        tmp_paths = kwargs.get('tmp_paths', None)
        scale = kwargs.get("scale", 1)
        bsub_output_dir = join(bsub_output_dir, motif_id)
        if not exists(bsub_output_dir):
            mkdir(bsub_output_dir)


        for p in [pos_fasta_train, pos_bed_train, neg_fasta_train, neg_bed_train,
                  pos_fasta_test, neg_bed_test, neg_fasta_test, neg_bed_test]:
            if not exists(p):
                print(p)
                assert exists(p)

        cmds_ref = []
        cmds_aug = []
        # Assuming the user wants to score sequences
        assert exists(output_dir)
        ref_classifier_path = join(output_dir,
                                   '{0}.ref.pkl'.format(motif_id + "_" + str(ki)))
        augmented_classifier_path = join(output_dir, '{0}.aug.pkl'.format(
            motif_id + "_" + str(ki)))

        arguments = self.get_args_train_seq_model(pos_fasta_train, pos_bed_train,
                                                  neg_fasta_train, neg_bed_train,
                                                  motif_path_jaspar, ref_classifier_path,
                                                  stop_at=stop_at,
                                                  in_scores_pos=in_scores_pos,
                                                  in_scores_neg=in_scores_neg,
                                                  scale=scale)
        # print arguments

        print('here...')
        line = arguments.strip()
        split = line.split(" ")
        sys.argv = [sys.argv[0]] + split

        print(sys.argv)

        print(" ".join(sys.argv))
        if True or not exists(ref_classifier_path):
            if create_bsubmit:
                sys.argv = ["../../lib/DNAshapedTFBS/DNAshapedTFBS.py"] + split
                cmds_ref.append(" ".join(sys.argv))
            # running training these arguments
            else:
                print('training seq model...')
                arguments = arg_parsing()
                arguments.func(arguments)


        arguments = self.get_args_train_seqnshape_model(pos_fasta_train, pos_bed_train,
                                                        neg_fasta_train, neg_bed_train,
                                                        motif_path_jaspar, augmented_classifier_path,
                                                        big_wig_dir=big_wig_dir,
                                                        stop_at=stop_at,
                                                        in_scores_neg=in_scores_neg,
                                                        in_scores_pos=in_scores_pos,
                                                        scale=scale)
        line = arguments.strip()
        split = line.split(" ")
        sys.argv = [sys.argv[0]] + split

        if True or not exists(augmented_classifier_path):
            print('Training Seq and DNA-shape classifier..')
            # running training these arguments
            if create_bsubmit:
                sys.argv = ["../../lib/DNAshapedTFBS/DNAshapedTFBS.py"] + split
                cmds_aug.append(" ".join(sys.argv))
            # running training these arguments
            else:
                print('training seq+shape model...')
                arguments = arg_parsing()
                arguments.func(arguments)


        print(augmented_classifier_path)


        # assert exists(augmented_classifier_path)

        ref_predictions_path = join(output_dir, '{0}.ref.scores'.format(
            motif_id + "_" + str(ki) + ".fg"))
        ref_predictions_path_bg = join(output_dir, '{0}.ref.scores'.format(
            motif_id + "_" + str(ki) + ".bg"))
        aug_predictions_path = join(output_dir, '{0}.aug.scores'.format(
            motif_id + "_" + str(ki) + ".fg"))
        aug_predictions_path_bg = join(output_dir, '{0}.aug.scores'.format(
            motif_id + "_" + str(ki) + ".bg"))

        for fasta_test, bed_test, in_scores, p1, p2, p3, p4 in [
            [pos_fasta_test, pos_bed_test, in_scores_pos,
             ref_predictions_path, aug_predictions_path,
             True, True],
            [neg_fasta_test, neg_bed_test, in_scores_neg,
             ref_predictions_path_bg,
             aug_predictions_path_bg, None, None]]:

            if True or not exists(p1):
                print('applyPSSM (+) sequences (seq-only model)', motif_id)
                arguments = self.get_args_apply_seq_model(fasta_test, bed_test, motif_path_jaspar,
                                                         ref_classifier_path, p1, in_scores=in_scores,
                                                          scale=scale)

                line = arguments.strip()
                split = line.split(" ")
                sys.argv = [sys.argv[0]] + split

                if create_bsubmit:
                    sys.argv = ["../../lib/DNAshapedTFBS/DNAshapedTFBS.py"] + split
                    cmds_ref.append(" ".join(sys.argv))
                else:
                    print(sys.argv)
                    sys.argv = [sys.argv[0]] + split
                    sys.argv = split
                    print('curr args...')
                    print(sys.argv)
                    print('parsing arguments...')
                    arguments = arg_parsing()

                    print(arguments)
                    arguments.func(arguments)

            # score all sequences using applyPSSM
            if p3 is not None and isinstance(all_fasta_paths, list):
                print(all_fasta_paths)
                for all_fasta, all_bed in zip(all_fasta_paths, all_bed_paths):
                    print('applyPSSM (+) sequences', motif_id)
                    category = basename(all_fasta.split("_")[-1]).replace(".fa", "")
                    p3 = join(output_dir, motif_id + "_" + category + ".ref.scores.gz")
                    print(p3)

                    if not exists(p3):
                        arguments = self.get_args_apply_seq_model(all_fasta, all_bed,
                                                                 motif_path_jaspar,
                                                                 ref_classifier_path, p3,
                                                                  scale=scale)
                        line = arguments.strip()
                        split = line.split(" ")
                        sys.argv = [sys.argv[0]] + split

                        if create_bsubmit:
                            sys.argv = ["../../lib/DNAshapedTFBS/DNAshapedTFBS.py"] + split
                            cmds_ref.append(" ".join(sys.argv))
                        else:
                            sys.argv = [sys.argv[0]] + split
                            print(sys.argv)
                            sys.argv = split
                            arguments = arg_parsing()
                            arguments.func(arguments)

            if True or not exists(p2):
                # scoring using our PSSM in test sequences
                print('applying (seq+shape)', motif_id)
                arguments = self.get_args_apply_seqnshape_model(fasta_test, bed_test,
                                                                motif_path_jaspar,
                                                                augmented_classifier_path,
                                                                p2,
                                                                big_wig_dir=big_wig_dir,
                                                                in_scores=in_scores,
                                                                scale=scale)
                line = arguments.strip()
                split = line.split(" ")
                sys.argv = [sys.argv[0]] + split
                if create_bsubmit:
                    sys.argv = ["../../lib/DNAshapedTFBS/DNAshapedTFBS.py"] + split
                    cmds_aug.append(" ".join(sys.argv))
                else:
                    print(sys.argv)
                    arguments = arg_parsing()
                    # print arguments
                    arguments.func(arguments)

                # scoring using our PSSM in all sequences (whole dataset

            # score all sequences using applyPSSM
            if p4 is not None and isinstance(all_fasta_paths, list):
                print('applyPSSM (+) sequences', motif_id)
                for all_fasta, all_bed in zip(all_fasta_paths, all_bed_paths):
                    p4 = join(output_dir, basename(all_fasta.replace(".fa", ".aug.scores.gz")))
                    print(p4)
                    if not exists(p4):
                        arguments = self.get_args_apply_seqnshape_model(all_fasta, all_bed,
                                                                        motif_path_jaspar,
                                                                        augmented_classifier_path,
                                                                        p4,
                                                                        big_wig_dir=big_wig_dir,
                                                                        scale=scale)
                        line = arguments.strip()
                        split = line.split(" ")
                        print(sys.argv)

                        if create_bsubmit:
                            sys.argv = ["../../lib/DNAshapedTFBS/DNAshapedTFBS.py"] + split
                            cmds_aug.append(" ".join(sys.argv))
                        else:
                            sys.argv = [sys.argv[0]] + split
                            print(sys.argv)
                            arguments = arg_parsing()
                            arguments.func(arguments)
            if create_bsubmit:
                # cmds_ref = [prefix + cmd + ("\"" if len(prefix) != 0 else "")
                #             for cmd in cmds_ref for prefix in ("echo \"", "")]
                # cmds_aug = [prefix + cmd + ("\"" if len(prefix) != 0 else "")
                #             for cmd in cmds_aug  for prefix in ("echo \"", "")]
                # for cmd_ref in cmds_ref:
                #     print cmd_ref
                # for cmd_aug in cmds_aug:
                #     print cmd_aug
                script_ref = BSubFacade.create_job(tmp_paths[0] if tmp_paths is not None else tempfile.mkstemp()[1],
                                                   cmd=cmds_ref,
                                                   bsub_output_dir=bsub_output_dir)
                script_aug = BSubFacade.create_job(tmp_paths[1] if tmp_paths is not None else tempfile.mkstemp()[1],
                                                   cmd=cmds_aug,
                                                   bsub_output_dir=bsub_output_dir)
                print(tmp_paths)
                print(script_ref)
                print(script_aug)

        if create_bsubmit:
            bsub_bin = "/usr/share/lsf/9.1/linux2.6-glibc2.3-x86_64/bin/bsub"
            bsub_bin = "bsub" if not exists(bsub_bin) else bsub_bin
            # time.sleep(3)
            system(bsub_bin + " < " + script_ref)
            # time.sleep(3)
            system(bsub_bin + ' < ' + script_aug)


    def get_args_apply_seq_model(self, fasta, bed, jaspar_path,
                                 classifier_path, output_path, in_scores=None,
                                 scale=1):
        arguments = ("applyPSSM -f {0} " + \
                     "-i {1} -I {2} " + \
                     "-c {3} -o {4}{5} --threshold 0.0").format(jaspar_path,
                                                  fasta,
                                                  bed,
                                                  classifier_path,
                                                  output_path,
                                                                " -n " if scale else "")

        if in_scores is not None:
            arguments += ( " --in_scores {0}").format(in_scores)

        return arguments

    def get_args_train_seq_model(self, pos_fasta, pos_bed, neg_fasta, neg_bed,
                                 jaspar_path, output_classifier_path, stop_at=None,
                                 in_scores_pos=None, in_scores_neg=None,
                                 scale=1):
        args = ("trainPSSM -f {0} " + \
                 "-i {1} -I {2} " + \
                 "-b {3} -B {4} " + \
                 "-o {5}{6}").format(jaspar_path,
                                     pos_fasta,
                                     pos_bed,
                                     neg_fasta,
                                     neg_bed,
                                     output_classifier_path,
                                     " -n " if scale else "")
        if stop_at != -1:
            args += ' -max ' + str(stop_at)

        if in_scores_pos is not None and in_scores_neg is not None:
            args += ( " --in_scores_pos {0} --in_scores_neg {1}").format(in_scores_pos,
                                                                         in_scores_neg)

        return args

    def get_args_train_seqnshape_model(self, pos_fasta, pos_bed, neg_fasta, neg_bed,
                                       jaspar_path, output_classifier_path, big_wig_dir=None,
                                       stop_at=None, in_scores_pos=None, in_scores_neg=None,
                                       scale=1):
        helt, mgw, prot, roll, helt2, mgw2, prot2, roll2 = [join(big_wig_dir,
                                                                 'hg19' + "." + k + ".wig.bw")
                                                            for k in
                                                            ["HelT", "MGW", "ProT",
                                                             "Roll", "HelT.2nd",
                                                             "MGW.2nd",
                                                             "ProT.2nd",
                                                             "Roll.2nd"]]
        arguments = ("trainPSSM -f {0} " + \
                     "-i {1} -I {2} " + \
                     "-b {3} -B {4} " + \
                     "-o {5} " + \
                     "-1 {6} {7} {8} {9} " + \
                     "-2 {10} {11} {12} {13}{14}").format(jaspar_path,
                                                         pos_fasta,
                                                         pos_bed,
                                                         neg_fasta,
                                                         neg_bed,
                                                         output_classifier_path,
                                                         helt, mgw, prot, roll,
                                                         helt2, mgw2, prot2, roll2,
                                                          " -n " if scale else "")

        if in_scores_pos is not None and in_scores_neg is not None:
            arguments += ( " --in_scores_pos {0} --in_scores_neg {1}").format(in_scores_pos,
                                                                         in_scores_neg)
        if stop_at != -1:
            arguments += ' -max ' + str(stop_at)
        return arguments

    def get_args_apply_seqnshape_model(self, fasta, bed, jaspar_path,
                                       classifier_path, output_path,
                                       big_wig_dir=None, in_scores=None,
                                       scale=1):

        helt, mgw, prot, roll, helt2, mgw2, prot2, roll2 = [join(big_wig_dir,
                                                                 'hg19' + "." + k + ".wig.bw")
                                                            for k in
                                                            ["HelT", "MGW", "ProT",
                                                             "Roll", "HelT.2nd",
                                                             "MGW.2nd",
                                                             "ProT.2nd",
                                                             "Roll.2nd"]]

        arguments = ("applyPSSM -f {0} " + \
                     "-i {1} -I {2} " + \
                     "-c {3} -o {4} " + \
                     "-1 {5} {6} {7} {8} " + \
                     "-2 {9} {10} {11} {12}{13} --threshold 0.0").format(
                                            jaspar_path,
                                            fasta,
                                            bed,
                                            classifier_path,
                                            output_path,
                                            helt, mgw, prot, roll,
                                            helt2, mgw2, prot2, roll2,
                                            " -n " if scale else "")

        if in_scores is not None:
            arguments += ( " --in_scores {0}").format(in_scores)

        return arguments

    def generate_and_write_scores(self, fasta_path, bed_path, motif_path, output_path,
                                  return_cmd=False, scale=1):
        '''
        Using mathelier's pipeline we generated the respective scores in our sequences
        :param fasta_path:
        :param bed_path:
        :param motif_path:
        :param output_path:
        :return:
        '''

        if return_cmd:
            cmd = " ".join([__file__.replace(".pyc", ".py"),
                            fasta_path, bed_path, motif_path, output_path])
            return cmd

        big_wig_dir = "/g/scb/zaugg/rio/data/shape_data_gbshape"
        pssm = get_jaspar_pssm(motif_path, False)
        print(pssm)
        hits = find_pssm_hits(pssm, fasta_path)

        # here, we have to get the DNA-shapes for our cases
        helt, mgw, prot, roll, helt2, mgw2, prot2, roll2 = [
            join(big_wig_dir,
                 'hg19' + "." + k + ".wig.bw") for k in
            ["HelT", "MGW", "ProT", "Roll",
             "HelT.2nd", "MGW.2nd", "ProT.2nd", "Roll.2nd"]]
        hits_shapes = get_shapes(hits, bed_path,
                                 [helt, mgw, prot, roll],
                                 [helt2, mgw2, prot2, roll2],
                                 0, scale,
                                 fasta_file=fasta_path)

        scores = [hits, hits_shapes]
        # the scores are concatenated and saved as a single file
        cPickle.dump(scores, open(output_path, "wb"))

if __name__ == "__main__":
    import sys
    analyzer = DNAAugmentedModelsAnalyzer()
    script, fasta_path, bed_path, motif_path, output_path = sys.argv
    analyzer.generate_and_write_scores(fasta_path, bed_path, motif_path, output_path)
