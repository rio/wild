'''
Created on Apr 19, 2016

@author: ignacio
'''
from os import listdir
from lib.utils import *
from lib.DataFrameAnalyzer import DataFrameAnalyzer
from lib.PeaksAnalyzer import PeaksAnalyzer

class HQTLAnalyzer():
    
    def get_hqtls_snps_by_coordinate(self):
        #===============================================================================
        # Load coordinate-> SNP association
        #===============================================================================
        snps_table = {}
        snps_bed_dir = "/home/ignacio/Documents/zaugglab/localQTLs/snp_beds"
        for f in listdir(snps_bed_dir):
            bed_path = join(snps_bed_dir, f)
            for r in open(bed_path):
                split = r.strip().split("\t")
                coordinate = split[0] + ":" + split[1] + "-" + split[2]
                snp_id = split[3]
                snps_table[coordinate] = snp_id
                
        return snps_table
    
    def get_sequences_from_bed(self, bed_path, input_fasta, output_fasta):
        """
        Given a BED path and a FASTA path, get FASTAS sequences from it.
        """
        args = ["fastaFromBed", "-bed", bed_path, "-fi", input_fasta,
                "-fo", output_fasta]
        args = " ".join(args)
        system(args)
        
    def get_fastas_from_file(self, fasta_path, as_dict=False):
        fastas = []
        seq = None
        header = None
        for r in open(fasta_path):
            r = r.strip()
            if r.startswith(">"):
                if seq != None and header != None:
                    fastas.append([header, seq])
                seq = ""
                header = r[1:]
            else:
                if seq != None:
                    seq += r
                else:
                    seq = r
        
        # append last fasta read by method
        fastas.append([header, seq])
        
        if as_dict:
            return {h: s for h, s in fastas}
        return fastas

    @staticmethod
    def get_hQTLs_table(mark=None):
        if mark is None:
            df = pd.concat([HQTLAnalyzer.get_hQTLs_table(mark=mark)
                            for mark in HQTLAnalyzer.get_hmark_names()]).reset_index(drop=True)
            df['peak.id'] = df['mod'] + "_" + df['gene'].astype(str)
            return df
        else:
            p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/hQTL_vs_augmented_models/data/hqtls_matrices/SNPQTLmatrix.$hmark.gz'
            p = p.replace('$hmark', mark)
            print(exists(p), p)
            df = DataFrameAnalyzer.read_tsv_gz(p)
            df['peak.id'] = df['mod'] + "_" + df['gene'].astype(str)
            return df

    @staticmethod
    def get_peak_coordinates(mark=None):
        if mark is None:
            df = pd.concat([HQTLAnalyzer.get_peak_coordinates(mark=mark)
                            for mark in HQTLAnalyzer.get_hmark_names()]).reset_index(drop=True)
            return df
        else:
            p = '/g/scb/zaugg/zaugg/hQTL/peakAnnotation/peakLocations.ALL.$mark.txt'
            p = p.replace('$mark', mark)
            df = DataFrameAnalyzer.read_tsv(p, sep=' ', index_col=0)
            df['id'] = df.index
            df['mark'] = mark
            df['peak.id'] = df['mark'] + "_" + df['id'].astype(str)
            return df

    @staticmethod
    def get_hmark_names():
        return ['H3K27AC', 'H3K4ME1', 'H3K4ME3', 'dhs', 'RNA']

    @staticmethod
    def get_phen_matrix(mark='H3K27AC'):
        print('reading tsv.gz path for phenotypes (mark=%s)' % mark)
        p = "/home/rio/data/localQTLs/RMatrices/hMat.norm.ALL.$mark.peer_lS_$peer.tsv.gz"
        p = p.replace("$mark", mark).replace("$peer", '10' if mark == 'RNA' else '5')
        df = DataFrameAnalyzer.read_tsv_gz(p)
        return df
        
    def get_hpeak_ids_by_snp(self):
        """
        Get IDs as defined by Grubert et al 2015
        """
        # load HPEAK_IDs table (SNP_ID to HPEAK_IDS
        hpeak_to_snp_path = "/home/ignacio/Documents/zaugglab/localQTLs/RMatrices/hpeaks_to_snp.tsv"
        rows = [r.strip().split("\t") for r in open(hpeak_to_snp_path).readlines()][1:]
        # individual ids
        snp_to_hpeak = {}
        for r in rows:
            snp, hpeak_id = r[0], r[1] + "_" + r[2]
            if not snp in snp_to_hpeak:
                snp_to_hpeak[snp] = []
            snp_to_hpeak[snp].append(hpeak_id)            
        return snp_to_hpeak

    @staticmethod
    def get_distalQTLs():
        from lib.RFacade import RFacade
        basedir = '/g/scb2/zaugg/zaugg_shared/data/distalQTLs'
        all = []

        tmp_path = '/tmp/all_distal_QTLs.tsv.gz'
        if not exists(tmp_path):
            print('reading distalQTLs data')
            for f in listdir(basedir):
                if 'permstates' in f or not f.endswith('.rda'):
                    continue
                p = join(basedir, f)
                print(p)
                df = RFacade.load_rda_from_path(p)
                df['mark'] = f.split(".")[6]
                all.append(df)
            res = pd.concat(all)
            DataFrameAnalyzer.to_tsv_gz(res, tmp_path)
        return DataFrameAnalyzer.read_tsv_gz(tmp_path)

    @staticmethod
    def intersect_peaks_vs_hQTLs(peaks_path, hmark='H3K27AC'):
        marks = HQTLAnalyzer.get_hQTLs_table(hmark)
        marks['chr'] = 'chr' + marks['chr'].astype(str)
        peaks_analyzer = PeaksAnalyzer()
        tmp_marks = tempfile.mkstemp()[1]
        output_bedtools = tempfile.mkstemp()[1]
        marks['snp.position.end'] = marks['snp.position'] + 1
        DataFrameAnalyzer.to_tsv(marks[marks['pass.pvalTH'] == 'pass'][['chr',
                                                                        'snp.position',
                                                                        'snp.position.end']],
                                 tmp_marks, header=None)
        nA, nB, nIntersections = peaks_analyzer.insersect_chip_seq_peaks(tmp_marks,
                                                                         peaks_path,
                                                                         output_bedtools,
                                                                         additional_args=['-wao'])

        print(nA, nB, nIntersections)

    @staticmethod
    def get_motif_vs_hQTLs():
        p = join('/g/scb/zaugg/rio/EclipseProjects/zaugglab/hQTL_vs_augmented_models',
                 'data/new_tables_oana/motif_matches/MotifAnalysisTable.txt.gz')
        df = DataFrameAnalyzer.read_tsv_gz(p)
        return df

    @staticmethod
    def convert_rda_files_to_tsv_gz(hmark='H3K27AC'):
        script = join('/g/scb/zaugg/rio/EclipseProjects/zaugglab/composite_motifs/composite_motifs_vs_hqtls',
                      'src/02_1_prepare_chromatin_marks_signals_table.R')
        system('Rscript ' + script)