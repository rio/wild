
'''
Created on Feb 3rd, 2017

Basically a library of methods to get quickly DNA shape features from DNAShapeR

@author: ignacio
'''

from lib.utils import *
# from lib.plot_utils import *
# from lib.RFacade import RFacade
from lib.FastaAnalyzer import FastaAnalyzer
# from lib.motif_plotter import ConsensusMotifPlotter
from os import system, remove
from random import shuffle
from numpy import mean
from math import fabs
import math
# from sklearn.externals import joblib
# import seaborn as sns
# from matplotlib.colors import ListedColormap
from lib.SequenceMethods import SequenceMethods
from lib.MachineLearning import MachineLearning
from sys import exit

class ShapeAnalyzer():

    def get_inferred_seq_length(self, x_seq_n_shape):
        '''
        Calculate the numbers of nucleotide positions according to the number
        of shape features presented
        :param x_shape:
        :return:
        '''

        # get all the nucleotides that have either zero or one in groups of four,
        # until the last one
        seq_len = 0
        while seq_len < len(x_seq_n_shape):
            if sum(x_seq_n_shape[seq_len * 4: seq_len * 4 + 4]) != 1:
                break
            seq_len += 1

        assert seq_len < len(x_seq_n_shape)
        return seq_len

    def get_subvectors(self, x_shape, seq_len):
        '''
        Given an x vector with concatenated shape features, return the
        eight respective vectors used for building the shape features
        :param x_shape:
        :param motif_length:
        :param scales shape: these values are standard deviations defined by Yang
        Its usage is just optional
        :return:
        '''

        # print seq_len
        feat_len_diffs = (4, 3, 4, 3, 5, 4, 5, 4) # differences in length between shape and seq features
        feature_lenghts = [seq_len - i for i in feat_len_diffs]

        # print feature_lenghts
        assert sum(feature_lenghts) == len(x_shape)
        curr_start = 0
        new_x_shape = []

        # print start
        for feat_len in feature_lenghts:
            # print feat_len
            curr_end = curr_start + feat_len
            x_shapei = x_shape[curr_start: curr_end]
            # print curr_start, curr_end, len(x_shapei), x_shapei
            curr_start += feat_len
            new_x_shape.append(x_shapei)

        assert len([xi for x in new_x_shape for xi in x]) == len(x_shape)
        return new_x_shape

        # the shape is more difficult

    @staticmethod
    def get_features_by_code(df):
        fwd, rev = 'seq', 'seq.reverse'
        df['seq.reverse'] = df['seq'].apply(SequenceMethods.get_complementary_seq)
        feats_by_code = {}
        for direction in [fwd, rev]:
            output_dir_dna_shape = tempfile.mkdtemp()
            if not exists(output_dir_dna_shape):
                mkdir(output_dir_dna_shape)

            feats_by_code['1mer:%s' % direction] = ShapeAnalyzer.get_X_1mer(df[direction])
            feats_by_code['1mer+2mer:%s' % direction] = ShapeAnalyzer.get_X_1mer_2mer(df[direction])
            feats_by_code['1mer+3mer:%s' % direction] = ShapeAnalyzer.get_X_1mer_3mer(df[direction])
            feats_by_code['3merE2:%s' % direction] = ShapeAnalyzer.get_X_3merE2(df[direction])
            feats_by_code['1mer+2mer+3mer:%s' % direction] = ShapeAnalyzer.get_X_1mer_2mer_3mer(df[direction])

            shape_feats_order = ['1-MGW', '1-Roll', '1-ProT', '1-HelT', '2-MGW', '2-Roll', '2-ProT', '2-HelT', ]
            feats_by_code['shape:%s' % direction] = ShapeAnalyzer.get_shapes_from_DNAShapeR(list(df[direction]),
                                                                                            overwrite=True,
                                                                                            output_prefix=output_dir_dna_shape + "/shapes",
                                                                                            infer_flanks=False,
                                                                                            shapes=shape_feats_order,
                                                                                            remove_fa_afterwards=True)
            feats_by_code['shape.no.flanks:%s' % direction] = ShapeAnalyzer.get_shapes_from_DNAShapeR(
                list(df[direction]),
                overwrite=True,
                output_prefix=output_dir_dna_shape + "/shapes",
                infer_flanks=False,
                shapes=shape_feats_order,
                remove_fa_afterwards=True)
            clean_tmp(pattern=output_dir_dna_shape)

            feats_by_code['1mer+shape:%s' % direction] = pd.concat([feats_by_code['1mer:%s' % direction],
                                                                    feats_by_code['shape.no.flanks:%s' % direction]],
                                                                   axis=1)
            feats_by_code['1mer+shape+3merE2:%s' % direction] = pd.concat([feats_by_code['1mer:%s' % direction],
                                                                           feats_by_code[
                                                                               'shape.no.flanks:%s' % direction],
                                                                           feats_by_code['3merE2:%s' % direction]],
                                                                          axis=1)
        return feats_by_code

    @staticmethod
    def get_performances(fg_kmers, code=None, feats_by_code=None):

        if feats_by_code is None:
            df = fg_kmers
            df['seq.reverse'] = df['seq'].apply(SequenceMethods.get_complementary_seq)
            feats_by_code = ShapeAnalyzer.get_features_by_code(fg_kmers)

        affinities = np.array(fg_kmers['y'])
        table = []
        for palindromic in [True, False]:
            for model in ['1mer', '1mer+shape', '1mer+shape+3merE2', '1mer+2mer+3mer']:
                if not model + (":%s" % 'seq') in feats_by_code:
                    continue

                X = None
                if not palindromic:
                    X = feats_by_code[model + (":%s" % 'seq')]
                else:
                    X1 = feats_by_code[model + (":%s" % 'seq')]
                    X2 = feats_by_code[model + (":%s" % 'seq.reverse')]
                    assert X1.shape == X2.shape
                    X = (X1 + X2) / 2.0

                perf = MachineLearning.KFoldRidgeRegression(np.array(X), affinities, label=model)
                table.append(['code', X.shape[0], model, palindromic, np.mean(perf['r2.test'])])
                print(table[-1])

        res = pd.DataFrame(table, columns=['code' if code is None else code,
                                           'n', 'model', 'palindromic', 'r2.test.average'])
        return res

    def get_seq_n_shape_vectors(self, sequences, core_motif,
                                r_script='02_1_convert_fasta_to_shape.R',
                                scale_shape_values=True, check_palindromic=True):

        is_palindromic_core_motif = False
        if check_palindromic:
            is_palindromic_core_motif = self.is_palindromic(core_motif)

        assert len({len(s) for s in sequences}) == 1

        M = len(sequences[0])
        n_flanking = (M - len(core_motif)) / 2
        # we get the values directly from the affinity matrices

        # test sequence parsing
        # compare if all kmer conversions make sense according to the palindromic filters, etc.
        x_seq_test = []
        print('checking if sequence vectors are consistently parsed...')
        # parse all kmers into binary codes according to the palindromic rules.
        for seq in sequences:
            if check_palindromic and (is_palindromic_core_motif):
                values = self.get_one_hot_encoding_palindromic_core_motif(seq)
            else:
                values = self.get_one_hot_encoding(seq)
            x_seq_test.append([float(vi) for vi in values])


        # get shape features
        df = self.get_shapes(sequences, r_script,
                             scale_shape_values=scale_shape_values)


        df_shape = df[[i for i in range(len(x_seq_test[0]), df.shape[1])]]
        labels_seq = df.columns[:len(x_seq_test[0])]
        # print labels_seq
        # print x_shape.head()

        # concatenate shape elements with binary seq values verified for
        # palindromics
        df_seq = pd.DataFrame(x_seq_test, columns=labels_seq)
        df_seq_n_shape = pd.concat([df_seq, df_shape], axis=1)
        return df_seq_n_shape


    def score_seqs_with_shape_boosted_model(self, seqs, model,
                                            shape_output_prefix=None,
                                            overwrite=False):

        seq_features = [SequenceMethods.get_one_hot_encoding(s) for s in seqs]
        shape_features = self.get_shapes_from_DNAShapeR(seqs,
                                                        output_prefix=shape_output_prefix,
                                                        overwrite=overwrite,
                                                        normalize=True)
        X_aug = np.array([seq_features[ri] + list(shapei)
                          for ri, shapei in shape_features.iterrows()])

        scores = model.predict(X_aug)
        return scores



    def has_identical_entries_but_shuffled(self, df1, df2):
        """
        Given two dataframes for which we know its assignment by the index in each,
        verify that the values in each column, after sorting by index
        :param df1:g
        :param df2:
        :return:
        """

        df1 = df1.sort_index()
        df2 = df2.sort_index()
        for c in df1.columns:
            v1 = list(df1[c])
            v2 = list(df2[c])
            print(c)
            print(v1[:5])
            print(v2[:5])
            print(sum(v1) == sum(v2))

        exit()

    @staticmethod
    def get_names_default_feat_ids():
        return ['1-MGW', '1-HelT', '1-ProT', '1-Roll', '2-MGW', '2-HelT', '2-ProT', '2-Roll']

    @staticmethod
    def get_names_all_feat_ids(prefix='12'):
        all_feats = []
        for name in ['MGW', 'HelT', 'ProT', 'Roll', 'EP', 'Stretch', 'Tilt',
                     'Buckle', 'Shear', 'Opening', 'Rise', 'Shift', 'Stagger', 'Slide']:
            if prefix != None:
                for id in '12':
                    all_feats.append(id + "-" + name)
            else:
                all_feats.append(name)
        return all_feats

    @staticmethod
    def get_shapes_from_DNAShapeR(sequences,
                                  shapes=['1-MGW', '1-HelT', '1-ProT', '1-Roll', '1-EP',
                                            '2-MGW', '2-HelT', '2-ProT', '2-Roll', '2-EP'],
                                  normalize=False, gzip=True,
                                  output_prefix='./', save_all=False,
                                  overwrite=False,
                                  infer_flanks=False,
                                  fasta_tmp_path=None, remove_fa_afterwards=True,
                                  run=True, **kwargs):


        rscript_dir = "/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts"
        if not exists(rscript_dir):
            rscript_dir = '/home/ignacio/Dropbox/Eclipse_Projects/wild/lib/rscripts'
            rscript_dir = '/mnt/c/Users/ignacio/Dropbox/Eclipse_Projects/wild/lib/rscripts'
        if not exists(rscript_dir):
            rscript_dir = "C:\\Users\\ignacio\\Dropbox\\Eclipse_Projects\\wild\\lib\\rscripts"

        rscript = join(rscript_dir, 'get_dna_shape.R')

        if fasta_tmp_path is None:
            fasta_tmp_path = tempfile.mkstemp()[1]

        FastaAnalyzer.write_fasta_from_sequences(sequences, fasta_tmp_path)
        # print 'input fa path'
        # print fasta_tmp_path
        # system('head ' + fasta_tmp_path)

        # if any of the expected output files already exists, then omit running DNAshapeR
        expected_out_paths = [output_prefix + "-" + shape + ".tsv" + ('.gz' if gzip else "")
                              for shape in shapes]
        done = True
        for p in expected_out_paths:
            if not exists(p):
                done = False


        module_path = '/g/easybuild/x86_64/CentOS/7/nehalem/software/R/3.4.0-foss-2016b/lib64/R'

        if not done or overwrite:
            if not exists(rscript_dir):
                print(rscript_dir)
            assert exists(rscript_dir)
            # print 'running DNAShapeR...'
            # print '(n.entries = ', len(sequences), ')'

            # r_script_path = '/g/easybuild/x86_64/CentOS/7/nehalem/software/R/3.4.3-foss-2017b-X11-20171023/bin/Rscript'
            r_script_path = '/g/scb2/zaugg/rio/miniconda2/bin/Rscript'
            if not exists(r_script_path):
                rscript_win = "C:\\Users\\ignacio\\Desktop\\Rscript_windows.lnk"
                if exists(rscript_win):
                    r_script_path = rscript_win

            if exists(r_script_path):
                if not run:
                    print('this function was explicitly asked to not run (run=False).' \
                          ' Find the input files (verify previous script and repeat...)')
                else:
                    # system('R RHOME')
                    if exists(module_path) and kwargs.get('use_spinoza_R', True):
                        print('loading module')
                        # assert 1 > 2
                        # system('module load R')
                        # os.environ['RHOME'] = '/g/easybuild/x86_64/CentOS/7/nehalem/software/R/3.4.3-foss-2017b-X11-20171023/lib64/R'
                        # os.environ['RHOME'] = '/g/easybuild/x86_64/CentOS/7/nehalem/software/R/3.4.0-foss-2016b/lib64/R'
                        # system('which Rscript')
                        # print os.environ['RHOME']
                        # system('R RHOME')
                        r_script_path = 'Rscript'
            else:
                r_script_path = 'Rscript'

            # print r_script_path
            use_config_file = kwargs.get('use_config_file', False)
            cmd = [r_script_path, rscript, '--shapes', ','.join(shapes),
                    '--normalize', str(int(normalize)), '--output_prefix', output_prefix,
                    '--gzip', str(int(gzip)), fasta_tmp_path,
                    '--inferflanks', str(int(infer_flanks))]

            if use_config_file:
                config_file = "../../data/config_get_DNAshape.txt"
                writer = open(config_file, 'w')
                for parm, value in zip(['shapes', 'normalize', 'output_prefix',
                                        'gzip', 'fasta_tmp_path', 'inferflanks'],
                                       [3, 5, 7, 9, 10, 12]):
                    writer.write(parm + "\t" + cmd[value] + "\n")
                writer.close()

            cmd = ' '.join(cmd)

            if run:
                # print cmd
                print('getting DNA shape features from input sequences...')
                print(cmd)
                # import ipdb; ipdb.set_trace()

                ShapeAnalyzer.run_dnashape_R(cmd, sh_full_path=kwargs.get('sh_script_path', None))
                # system(cmd)

                # assert 1 > 2


            # exit()
            if exists(r_script_path):
                if exists(module_path):
                    pass
                    # print 'unloading module'
                    # system('module unload Rscript')
                # os.environ['R_HOME'] = '/g/scb2/zaugg/rio/miniconda2/bin/R'
        else:
            print('shape features already exists. Skip DNAshape R...')

        # this dictionary keeps the number of indexes that we differentiate between
        # each shape feature, with respect to the number of seq features (ie substraction)
        offset = {'1-MGW': 4, '1-Roll': 3, '1-ProT': 4, '1-HelT': 3, '1-EP': 4,
                  '2-MGW': 5, '2-Roll': 4, '2-ProT': 5, '2-HelT': 4, '2-EP': 5}

        # concatenate and bring back the features
        df_final = None
        # print 'reading shape features...'
        for shape in shapes:
            p = output_prefix + "-" + shape + ".tsv" + ('.gz' if gzip else "")
            print('Reading next shape from output file', shape)
            print(p)
            # print p

            if not exists(p):
                print('One of the shape paths does not exists. Return None...')
                print(exists(p), p)
            df = pd.read_csv(p, sep='\t', header=None, index_col=None)
            # print df.head()
            order, shape_id = shape.split("-")
            shape_id = shape_id if order == "1" else shape_id + 'x' + shape_id
            cols = [shape_id.upper().replace("X", "x") + ".%02d" % (i + 3) for i in
                     range(len(sequences[0]) - offset[shape])]
            if infer_flanks:
                cols = [shape_id.upper().replace("X", "x") + ".%02d" % (i + 1) for i in
                        range(len(sequences[0]) - offset[shape] + 4)]


            # print df
            # print cols
            print(cols)
            # print df.head()
            diff = len(cols) - len(df.columns)
            if diff != 0:
                print(diff)
                assert diff % 2 == 0
                cols = cols[diff /2: -(diff / 2)] # + list(cols) + [np.nan for i in range(diff / 2)]
            df.columns = cols
            df_final = df if df_final is None else pd.concat([df_final, df], axis=1)

        for shape in shapes + ['1-EP']:
            # delete tmp files if they do exists
            tmp_file_shape = join(fasta_tmp_path + "." + shape[2:])
            # print exists(tmp_file_shape), tmp_file_shape
            if exists(tmp_file_shape):
                remove(tmp_file_shape)
        # remove tmp fasta file
        if exists(tmp_file_shape):
            remove(tmp_file_shape)

        if save_all:
            # save a version of all the features concatenated in the order the input was created
            df_final.to_csv(output_prefix + "-all.tsv", sep='\t', index=None)

        if remove_fa_afterwards and fasta_tmp_path.startswith('/tmp/'):
            clean_tmp(pattern=fasta_tmp_path)

        return df_final

        # fasta_tmp_path=shape_matrix_path)

    @staticmethod
    def run_dnashape_R(command, sh_full_path=None):
        s = "\n".join(['module load R', 'which R', 'which Rscript', command + "\n"])
        sh_full_path = tempfile.mkstemp()[1] if sh_full_path is None else sh_full_path
        writer = open(sh_full_path, 'w')
        for si in s:
            writer.write(si)
        writer.close()
        print(exists(sh_full_path), sh_full_path)
        system('chmod +x ' + sh_full_path)
        # system('.'  + ("/" if not sh_full_path.startswith('/') else '') + sh_full_path)
        print('sh ' + sh_full_path)
        system('sh ' + sh_full_path)
        # remove the script afterwards
        remove(sh_full_path)


    @staticmethod
    def get_rsquared_deltas_by_position_2mer_3mer(seqs, y):

        from lib.MLRGenerator import MLRGenerator
        from sklearn.linear_model import Ridge
        mlr = MLRGenerator()
        X_1mer_2mer_3mer = ShapeAnalyzer.get_X_1mer_2mer_3mer(list(seqs))
        next_rsq_delta_2mer_3mer = mlr.get_rsquared_deltas_higher_order(X_1mer_2mer_3mer, y,
                                                                        classifier=Ridge(random_state=500))
        return next_rsq_delta_2mer_3mer


    @staticmethod
    def get_tiled_X_1mer(seqs_by_position):
        n_positions = len(seqs_by_position)
        m = []
        # concatenate DNA-sequence features by position
        for i in range(n_positions):
            seqs = list(seqs_by_position[i])
            X = ShapeAnalyzer.get_X_1mer(seqs)
            X.columns = [ci.split(".")[0] + "." + str(i + int(ci.split(".")[1])) for j, ci in enumerate(X.columns)]
            m.append(X)
            print(X.head())
        X_1mer = pd.concat(m)
        return X_1mer

    @staticmethod
    def get_performances_yang_et_al():
        return pd.read_excel(join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding',
                                  'cap_selex_quantitative_analysis/data/yang_et_al/msb167238-sup-0002-tableev1.xlsx'),
                             sheet_name='TableEV1')

    @staticmethod
    def get_yang_et_al_kmers():
        df = pd.read_csv(
            "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/hQTL_vs_augmented_models/data/yang_2017_supp_table2.tsv",
            sep='\t', index_col=None)
        jobid = 0
        query = None

        data_dir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/data'
        encoded_files_dir = join(data_dir, 'encoded_feats_yang_et_al_2017/encoded-files')
        aff_files_dir = join(data_dir, 'encoded_feats_yang_et_al_2017/data-files')

        codes = {f.split(".")[0] for f in listdir(encoded_files_dir)}

        from multiprocessing import Manager
        manager = multiprocessing.Manager()
        return_dict = manager.dict()

        kmers_by_code = {}
        for i, ht_selex_code in enumerate(codes):
            print(i, ht_selex_code)
            affs_path = join(aff_files_dir, ht_selex_code + ".txt")
            aff = DataFrameAnalyzer.read_tsv(affs_path, sep=' ', header=None)
            # print aff.shape
            # print aff.head()
            # aff = aff[aff[2] >= 100]
            # print aff.shape[0]
            fg_kmers = aff[[0, 1, 2]]
            fg_kmers.columns = ['seq', 'y', 'counts']
            kmers_by_code[ht_selex_code] = fg_kmers
        return kmers_by_code


    @staticmethod
    def get_tiled_Xshape(seqs_by_position):
        n_positions = len(seqs_by_position)
        # concatenate DNA-shape features by position
        features_order = ['MGW', 'ROLL', 'PROT', 'HELT']
        feat_keys = [feat + symbol for symbol in '.x' for feat in features_order]
        shapes_by_name =  {}
        for i in range(n_positions):
            seqs = list(seqs_by_position[i])
            print(seqs[:5])
            output_dir_dna_shape = tempfile.mkdtemp()
            xshapes = ShapeAnalyzer.get_shapes_from_DNAShapeR(seqs, overwrite=True,
                                                              output_prefix=output_dir_dna_shape + "/shapes",
                                                              infer_flanks=True,
                                                              shapes=['1-MGW', '1-HelT', '1-ProT', '1-Roll',
                                                                      '2-MGW', '2-HelT', '2-ProT', '2-Roll'])
            for next_feat in feat_keys:
                X = xshapes[[c for c in xshapes if c.startswith(next_feat)]]
                print(next_feat, X.shape)
                print(X.head())
                print(list(X.columns))
                X.columns = [ci.split(".")[0] + "." + ("%02d" % (i + j + 1)) for j, ci in enumerate(X.columns)]
                if not next_feat in shapes_by_name:
                    shapes_by_name[next_feat] = []
                shapes_by_name[next_feat].append(X)

        print('shape feature names before concatenating...')
        for feat in shapes_by_name:
            print(len(shapes_by_name[feat]))
            for shapesi in shapes_by_name[feat]:
                print(shapesi.head())
            shapes_by_name[feat] = pd.concat(shapes_by_name[feat])
            print(type(shapes_by_name[feat]))
            print(list(range(shapes_by_name[feat].shape[1])))
            # shapes_by_name[feat].columns = [feat + str(i) for i in range(shapes_by_name[feat].shape[1])]


        X_shape = pd.concat([shapes_by_name[feat] for feat in shapes_by_name], axis=1)
        return X_shape



    @staticmethod
    def get_tiled_X_1mer_n_shape(seqs_by_position, std_mean=False):

        n_positions = len(seqs_by_position)

        X_1mer = ShapeAnalyzer.get_tiled_X_1mer(seqs_by_position)
        X_shape = ShapeAnalyzer.get_tiled_Xshape(seqs_by_position)

        print('tiled dataframe shape (final)')
        print(X_shape.head())

        X_1mer_n_shape = pd.concat([X_1mer, X_shape], axis=1)

        # convert NA positions into the mean of the column
        for c in X_1mer_n_shape:
            average_col = np.mean(X_1mer_n_shape[c])
            X_1mer_n_shape[c] = [average_col if np.isnan(ci) else ci for ci in X_1mer_n_shape[c]]

        # standarize the columns
        if std_mean:
            for c in X_1mer_n_shape:
                average_col = np.mean(X_1mer_n_shape[c])
                X_1mer_n_shape[c] -= average_col

        return X_1mer_n_shape

    @staticmethod
    def get_rsquared_deltas_by_position_dna_shape(seqs, y, output_dir_dna_shape=None, x1mer=None, xshapes=None, x1mer_n_shape=None):
        from lib.MLRGenerator import MLRGenerator
        from sklearn.linear_model import Ridge

        affinities = np.array(y)

        if x1mer_n_shape is None:
            if x1mer is None:
                x1mer = ShapeAnalyzer.get_X_1mer(seqs)
            if output_dir_dna_shape is None:
                output_dir_dna_shape = tempfile.mkdtemp()
            if xshapes is None:
                xshapes = ShapeAnalyzer.get_shapes_from_DNAShapeR(seqs, overwrite=True,
                                                                  output_prefix=output_dir_dna_shape + "/shapes",
                                                                  infer_flanks=True,
                                                                  shapes=['1-MGW', '1-HelT', '1-ProT', '1-Roll',
                                                                          '2-MGW', '2-HelT', '2-ProT', '2-Roll'])


        if x1mer_n_shape is None:
            x1mer_n_shape = pd.concat([x1mer, xshapes], axis=1)

        mlr = MLRGenerator()
        deltas = mlr.get_rsquared_deltas(x1mer_n_shape, affinities, classifier=Ridge(random_state=500))
        return deltas

    @staticmethod
    def get_shapes(seqs, output_dir_dna_shape=None, shapes=['1-MGW', '1-HelT', '1-ProT', '1-Roll',
                                                            '2-MGW', '2-HelT', '2-ProT', '2-Roll'],
                   infer_flanks=True,
                   **kwargs):
        if output_dir_dna_shape is None:
            output_dir_dna_shape = tempfile.mkdtemp()
        xshapes = ShapeAnalyzer.get_shapes_from_DNAShapeR(seqs, overwrite=True,
                                                          output_prefix=output_dir_dna_shape + "/shapes",
                                                          infer_flanks=infer_flanks,
                                                          shapes=shapes, **kwargs)
        return xshapes


    @staticmethod
    def get_model_coefficients_1mer_n_shape(seqs, y, output_dir_dna_shape=None):
        from lib.MLRGenerator import MLRGenerator
        from sklearn.linear_model import Ridge

        affinities = np.array(y)
        x1mer = ShapeAnalyzer.get_X_1mer(seqs)

        if output_dir_dna_shape is None:
            output_dir_dna_shape = tempfile.mkdtemp()

        xshapes = ShapeAnalyzer.get_shapes_from_DNAShapeR(seqs, overwrite=True,
                                                          output_prefix=output_dir_dna_shape + "/shapes",
                                                          infer_flanks=True,
                                                          shapes=['1-MGW', '1-HelT', '1-ProT', '1-Roll',
                                                                  '2-MGW', '2-HelT', '2-ProT', '2-Roll'])
        x1mer_n_shape = pd.concat([x1mer, xshapes], axis=1)
        x1mer_n_shape_res = MachineLearning.KFoldRidgeRegression(np.array(x1mer_n_shape), pd.DataFrame(affinities, columns=['y'])['y'])
        res = ShapeAnalyzer.get_model_coefficients_L2(np.array(x1mer_n_shape), affinities)
        res['feature.name'] = x1mer_n_shape.columns
        return res



    def get_shape_features_average(self, model, seq_len, output_path=None):
        '''
        Given a Seq+Seq model fitted in a set of sequences, return
        the vector that summarizes the shape features averaged across
        it.

        We have to pay attention to the amount of features, to realize
        if the model was fitted using sequences + flanks, or just sequences.
        :param model:
        :param len_core_motif:
        :return:
        '''
        print('done...')
        feat_imp = pd.Series(model.feature_importances_)

        print(len(feat_imp))
        shape_feat = feat_imp
        print(len(feat_imp), len(shape_feat))

        print(seq_len)

        # we flanking bases in each side
        n_positions = seq_len - 4

        # this is the way the shape features were read in the first place
        shapes = ['1-MGW', '1-HelT', '1-ProT', '1-Roll',
                  '2-MGW', '2-HelT', '2-ProT', '2-Roll']
        # this dictionary keeps the number of indexes that we differentiate between
        # each shape feature, with respect to the number of seq features (ie substraction)
        offset = {'1-MGW': 4, '1-Roll': 3, '1-ProT': 4, '1-HelT': 3,
                  '2-MGW': 5, '2-Roll': 4, '2-ProT': 5, '2-HelT': 4}

        print(len(shape_feat))
        table = []
        sum_nfeat = 0
        for shape_id in shapes:
            next_weights = shape_feat[:seq_len - offset[shape_id]]
            print(len(next_weights))
            sum_nfeat += len(next_weights)
            table.append(next_weights)
            # print next_weights
            shape_feat = shape_feat[seq_len - offset[shape_id]:]

        for t, shapeid in zip(table, shapes):
            print(shapeid, len(t), list(t))

        print(list(table[0]))
        print(list(table[2]))
        df_bpair = pd.DataFrame([list(table[0]), list(table[2])],
                                columns=[i for i in range(seq_len - 4)])
        df_bpair.index = shapes[0], shapes[2]

        hypothesis_id = "test.id"
        bpair_features_output = join("../data/shape_features",
                                     hypothesis_id + "_bpair.tsv.gz")
        if not exists(bpair_features_output):
            df_bpair.to_csv(bpair_features_output, compression='gzip',
                                   sep='\t', index=None)

        # Roll and HelT are shifted one number to the left
        entry1 = [ti for ti in table[1][1:-1]]
        print(len(entry1))
        entry3 = [ti for ti in table[3][1:-1]]
        df_bstep = pd.DataFrame([list(entry1),
                                 list(entry3)],
                                columns=[i for i in range(len(entry1))])
        bstep_features_output = join("../data/shape_features", str(hypothesis_id) + "_bstep.tsv.gz")
        if not exists(bstep_features_output):
            df_bstep.to_csv(bstep_features_output, compression='gzip',
                            sep='\t', index=None)

        # calculate averages by position
        average_by_position = []
        print(df_bpair.shape)
        print(df_bstep.shape)

        for i in range(df_bpair.shape[1]):
            values = list(df_bpair[i])
            if i < df_bstep.shape[1]:
                values += list(df_bstep[i])
            if i > 0:
                values += list(df_bstep[i - 1])
            average_by_position.append(average(values))

        if output_path is not None:
            # save averaged values by plot
            cPickle.dump(average_by_position, open(output_path, 'wb'))

        return average_by_position


    def get_one_hot_encoding_palindromic_core_motif(self, seq):
        # in the case of a palindromic core motif, que average the values of
        # fwd and reverse strand in the kmer
        fwd_encoded = self.get_one_hot_encoding(seq)
        rev_encoded = self.get_one_hot_encoding(self.get_complementary_seq(seq))
        return [((ai + bi) / 2.0) for ai, bi in zip(fwd_encoded, rev_encoded)]

    def get_one_hot_encoding(self, seq):
        return [int(ci == k) for ci in seq for k in ['A', 'C', 'G', 'T']]

    def one_hot_encoding_to_sequence(self, v):
        return "".join(['ACGT'[i] for i in [j % 4 for j in range(len(v)) if v[j]]])

    def save_multiple_shape_files(self, x, output_dir, basename, y=None,
                                  requested_codes=None):
        '''
        It saves the shape features into multiple TSV files according to the codes
        defined by Yang
        :param df:
        :param output_dir:
        :param basename:
        :return:
        '''

        codes = ['10000000000',
                 '00010000000', '00001000000', '00000100000', '00000010000',
                 '00000001000', '00000000100', '00000000010', '00000000001',
                 '10011111111', '00011111111']
        labels = ['1mer', 'MGW', 'ROLL', 'PROT', 'HELT',
                  'MGWx', 'ROLLx', 'PROTx', 'HELTx',
                  '1mer+shape', 'shape']

        for code, label in zip(codes, labels):
            print(code, label)
            if requested_codes is not None and not code in requested_codes:
                continue
            requested_labels = [k for k in x.columns
                                if k.startswith(label) or code == '10011111111']
            shape_keys = {'MGW', 'ROLL', 'PROT', 'HELT'}
            if code == '10000000000': # get sequence codes only
                requested_labels = [k for k in x.columns if not k.split(".")[0] in shape_keys
                                    and not k.split("x")[0] in shape_keys]
            if code == '00011111111': # get shape codes only
                requested_labels = [k for k in x.columns if k.split(".")[0] in shape_keys
                                    or k.split("x")[0] in shape_keys]

            selected = x[requested_labels]

            print(basename, code)
            print(output_dir)
            output_path = join(output_dir, basename + "." + code + ".gz")
            # print pd.concat([y, selected], axis=1).head()
            if y is not None:
                pd.concat([y, selected], axis=1).to_csv(output_path, header=False,
                                                        index=None, sep=' ',
                                                        compression='gzip')
            else:
                selected.to_csv(output_path, header=False, index=None, sep=' ',
                                compression='gzip')

    def get_seq_n_shape_region(self, x_seq, x_shape, start, length):
        '''
        Given seq and shape features concatenated as a matrix, return the correspondent
        seq and shape features for a position
        :param x_seq:
        :param x_shape:
        :param start:
        :param end:
        :return:
        '''

        # print 'here...'
        # print x_seq
        seq_len = len(x_seq) / 4
        # print seq_len
        new_x_seq = x_seq[start * 4: (start + length) * 4]
        new_x_shape = []

        # MGW
        # print '# shape features', len(x_shape)
        feat_len_diffs = (4, 3, 4, 3, 5, 4, 5, 4) # differences in length between shape and seq features
        feature_lengths = [seq_len - i for i in feat_len_diffs]

        # print feature_lenghts
        # print feature_lenghts
        # print sum(feature_lengths), len(x_shape)
        assert sum(feature_lengths) == len(x_shape)
        curr_start = start
        new_x_shape = []

        # print start, length
        # print "\t".join(map(str, x_shape))
        # print start
        for feat_len, d in zip(feature_lengths, feat_len_diffs):
            # print feat_len
            # print feat_len
            curr_end = curr_start + length - d
            # print curr_start, curr_end, feat_len
            x_shapei = x_shape[curr_start: curr_end]
            # print "\t".join(map(str, x_shapei))
            new_x_shape += x_shapei
            curr_start += feat_len

        return new_x_seq, new_x_shape
        # the shape is more difficult

    def truncate_colormap(self, cmap, minval=-1, maxval=1, n=100):
        import matplotlib.colors as colors
        new_cmap = colors.LinearSegmentedColormap.from_list(
            'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval,
                                                b=maxval),
            cmap(np.linspace(minval, maxval, n)))
        return new_cmap

    def align_short_on_long_model(self, model_paths, seq_lengths,
                                  align_left=True, n_flanks=2, curr_col=0, n_cols=1,
                                  color1=['Reds', 'Reds'], color2=['Blues', 'Blues'], titles=None,
                                  sequence_lists=None, output_basename=None,
                                  show=False,
                                  coef_paths=None,
                                  r2_changes_by_pos=None):
        # Receive models to plot
        # 1st one is always the longest
        # 2nd and 3er ones are shorter and shorter
        # len diff is established in each iteration

        n_elements = len(model_paths)
        n_rows = 16
        clfs = [cPickle.load(open(p, 'rb')) for p in model_paths]

        fig = plt.figure(figsize=(10, 10))
        # fig = plt.figure()
        i = 0

        averages_by_position = []
        std_by_position = []
        average_colors = []
        n_add_left_averages = []
        n_add_left, n_add_right = 0, 0

        for clf, seq_len, seqs, title, align_left_i in zip(clfs, seq_lengths,
                                             sequence_lists, titles,
                                                         align_left):

            print('next plotting', i)
            # sequence length difference alwayss with the reference (CAP)
            len_diff = seq_lengths[0] - seq_len

            # ht_selex_sequences have to be shifted len_diff positions towards one side
            # if align_left_i:
            #     seqs = [s + (" ") * len_diff for s in seqs]
            # else:
            #     seqs = [(" ") * len_diff + s for s in seqs]

            feat_imp = pd.Series(clf.coef_)
            seq_feat = list(feat_imp[:4 * seq_len])
            shape_feat = feat_imp[4 * seq_len:]
            # print len(seq_feat), list(seq_feat)
            # print len(shape_feat), list(shape_feat)
            # print len(seq_feat) + len(shape_feat), len(feat_imp)
            # wo flanking bases in each side
            n_positions = seq_len - 4

            n_add_left, n_add_right = 0, 0
            if i != 0:
                n_add_left = len_diff if not align_left_i else 0
                n_add_right = len_diff if align_left_i else 0

            # read the coefficients
            if coef_paths is not None:
                coefs = cPickle.load(open(coef_paths[i], 'rb'))
                # print coefs[coefs.columns[seq_len * 4:]].head()
                stdevs = list(coefs.std())
                # print stdevs
            table, df_bpair, df_bstep, df_bpair_std, df_bstep_std = self.get_dfbpair_n_dfbstep(shape_feat,
                                                                   seq_len,
                                                                   n_flanks,
                                                                   n_additional_flanks_left=0,
                                                                   n_additional_flanks_right=0,
                                                                   viz_scaled=False,
                                                                   shape_stdev=stdevs)
																		   
			# plot motif
            sns.set_style('white')
            cbp = ConsensusMotifPlotter.from_sequences(seqs,
                                                       colors=['gray'] * 4)

            if i == 0:
                ax = plt.subplot2grid((n_rows, n_cols),
                                      (0 + i * 4, curr_col), rowspan=2)
            else:
                print(i, 'n_cols:', seq_len, seq_lengths[0])

                ax = plt.subplot2grid((n_rows, seq_lengths[0]),
                                      (0 + 1 * 4, n_add_left), colspan=seq_len, rowspan=2)


            cbp.plot(ax) #  title=r['motif.id'] + "\n" +
            despine_all()
            plt.xticks([])
            plt.xlabel('')
            ax.text(.5, .8, title, horizontalalignment='center', transform=ax.transAxes)

            plt.ylabel('IC', rotation=0, fontsize=15)
            plt.axvline(0, color='black')
            # plt.axvline(seq_len, color='black')
            # plt.axhline(2.0, color='black')
            plt.subplots_adjust(hspace=0.0)
            despine_all()

            plt.yticks([0.0, 1.0, 2.0])
            if i != 0 and not align_left_i:
                plt.yticks([])
                plt.ylabel('')

            cbar_ax = fig.add_axes([.9, [0.75, 0.65, 0.55][i], .010, .10])
            # lower heatmap
            print(n_rows, 1 + (i * 3), curr_col)

            if i == 0:
                ax2 = plt.subplot2grid((n_rows, n_cols),
                                       ((i * 4) + 2, curr_col))
            else:
                ax2 = plt.subplot2grid((n_rows, seq_lengths[0]),
                                      (2 + 1 * 4, n_add_left), colspan=seq_len)

            max1 = max([max([r2i for r2i in r2.values if not math.isnan(r2i)])
                        for rj, r2 in df_bpair.iterrows()])
            max2 = max([max([r2i for r2i in r2.values if not math.isnan(r2i)])
                        for rj, r2 in df_bstep.iterrows()])

            # print max1, max2, max(max1, max2)

            masking_number = 0.000000001 # -1
            mask = df_bpair == masking_number

            # print color1[i]
            cm = plt.cm.get_cmap(color1[i])
            # print df_bpair
            ax2 = sns.heatmap(df_bpair.abs(), linewidth=.3, vmin=0,
                              vmax=max(max1, max2), ax=ax2,
                              cmap=cm, cbar_ax=cbar_ax,
                              cbar_kws={'label': 'Ridge (abs.)'})

            ax2 = sns.heatmap(df_bpair.abs(), mask=~mask, linewidths=.3,
                              cmap=ListedColormap(['white']), ax=ax2,
                              cbar=None, vmin=0, vmax=max(max1, max2))

            # ax2.set_aspect(1)
            plt.axvline(0, color='black')
            plt.axvline(df_bpair.shape[1], color='black')

            plt.yticks([.5, 1.5], ['MGW', 'ProT'])

            if i != 0 and not align_left_i:
                plt.yticks([])

            print(len(plt.gcf().axes))
            # cax = plt.gcf().axes[0 + (curr_col * 5)]
            # cax.tick_params(labelsize=7)

            # ax2.set_aspect(1)
            plt.yticks(rotation=0, fontsize=10)
            ax2.set_xticks([])
            # cbar_ax = fig.add_axes([.95, .125 * (i + 2), .010, .10])
            # top heatmap
            if i == 0:
                ax4 = plt.subplot2grid((n_rows, n_cols), (3 + (i * 4), curr_col))
            else:
                ax4 = plt.subplot2grid((n_rows, seq_lengths[0]),
                                      (3 + 1 * 4, n_add_left), colspan=seq_len)

            sns.axes_style('white')

            mask = df_bstep == masking_number
            # print mask
            ax4 = sns.heatmap(df_bstep.abs(), linewidth=.3, vmin=0,
                              vmax=max(max1, max2), ax=ax4,
                              cmap=color2[i], cbar_ax=cbar_ax,
                              cbar_kws={'label': 'Ridge (abs.)'})


            # print len(plt.gcf().axes)
            # print len(plt.gcf().axes), plt.gcf().axes
            # cax = plt.gcf().axes[2]
            # cax.tick_params(labelsize=7)
            # ax4.tick_params(labelsize=6)

            mask = ~mask
            ax4 = sns.heatmap(df_bstep.abs(), mask=mask, linewidth=.3,
                              cmap=ListedColormap(['white']), ax=ax4,
                              cbar=None, vmin=0, vmax=max(max1, max2))
            # ax4.set_aspect(1)
            plt.yticks(rotation=0, fontsize=8)
            plt.yticks([.5, 1.5], ['HelT', 'Roll'], rotation=0, fontsize=10)
            plt.xlim([0.5, df_bstep.shape[1] - 0.5])
            plt.xticks([])

            plt.axvline(0.5, color='black')
            plt.axvline(df_bstep.shape[1] - 0.5, color='black')
            plt.axhline(0.0, xmin=0, xmax=df_bstep.shape[1], color='black')

            plt.subplots_adjust(hspace=0.0)

            if i != 0 and not align_left_i:
                plt.yticks([])

            # ax4.set_aspect(1)

            # horizontal and vertical lines
            n_lines = df_bstep.shape[1]
            # print n_lines
            # print df_bstep.shape[1], factor, df_bstep.shape[1] / factor
            # for line_i in range(n_lines):
            #     # print i, n_lines * i
            #     if line_i % 2 != 0:
            #         plt.axvline((line_i) / 2, color='white', linewidth=1.0)

            # plt.axhline(1, color='white', linewidth=1.0)


            table, df_bpair, df_bstep = self.get_dfbpair_n_dfbstep(shape_feat,
                                                                   seq_len,
                                                                   n_flanks,
                                                                   viz_scaled=False)

            next_avr, next_std = self.get_averages_by_position(df_bpair, df_bstep,
                                                     absolute=True,
                                                     df_bpair_std=df_bpair_std,
                                                     df_bstep_std=df_bstep_std,
                                                     skip_value=masking_number)

            averages_by_position.append(next_avr)
            std_by_position.append(next_avr)
            # print df_bpair
            # print df_bstep
            # print next_avr
            average_colors.append(color1[i])
            n_add_left_averages.append(n_add_left)

            perc_max = max([max(v) for v in r2_changes_by_pos])
            if r2_changes_by_pos is not None:
                ax = None
                cbar_ax = fig.add_axes([.9, 0.35, .010, .10])
                if i == 0:
                    ax = plt.subplot2grid((n_rows, n_cols), (n_rows / 2 + i, curr_col),
                                          rowspan=1)
                else:
                    print(i, 'n_cols:', seq_len, seq_lengths[0])
                    ax = plt.subplot2grid((n_rows, seq_lengths[0]),
                                          (n_rows / 2 + i, n_add_left),
                                          colspan=seq_len, rowspan=1)

                # plot r2 change by position
                row = [masking_number] * n_flanks
                row = row + list(r2_changes_by_pos[i]) + row
                print(row)
                row = pd.DataFrame(row).transpose()
                print(row)
                mask = row == masking_number
                annot = pd.DataFrame([[("%.1f" % vi).replace("0.0", '')
                                       for vi in v] for vi, v in row.iterrows()])
                print(annot)
                sns.heatmap(row, ax=ax, cbar_ax=cbar_ax,
                            cbar_kws={'label': '%R' + "$^2$" + " change"},
                            vmin=0, vmax=perc_max, annot=annot,
                            fmt='',
                            cmap='Blues')
                plt.xticks([])
                ax = sns.heatmap(row, mask=~(mask), linewidth=.3,
                                 cmap=ListedColormap(['white']), ax=ax, cbar=None)
                plt.xticks([])
                plt.ylabel(titles[i], rotation=0, horizontalalignment='right',
                           verticalalignment='center')
                plt.yticks([])

            i += 1

        ax = plt.subplot2grid((n_rows, n_cols), (n_rows - 2, curr_col), rowspan=2)

        # for avr in averages_by_position:
        #     print avr

        for avr_i, avr in enumerate(averages_by_position):
            x = [i + 0.5 + (n_add_left_averages[avr_i]) for i in range(len(avr))]
            plt.errorbar(x, avr, c=['Purple', 'Orange', 'Red'][avr_i],
                     linestyle='--' if avr_i == 0 else '-',
                     yerr=[stdi / 2.0 for stdi in std_by_position[avr_i]])

        ax.spines['top'].set_visible(False)
        plt.xlim([0.5, len(averages_by_position[0]) - .5])
        plt.ylabel('Ridge coef.\n(abs. averaged)', fontsize=15)
        plt.xticks([i + 1 for i in range(len(averages_by_position[0]))])
        plt.xlim([0.5, len(averages_by_position[0]) - 0.5])


        # min_y = min([yi for y in averages_by_position for yi in y])
        # max_y = max([yi for y in averages_by_position for yi in y])

        # plt.yticks([0, max(fabs(min_y), fabs(max_y)) / 2.0,
        #             max(fabs(min_y), fabs(max_y))])

        plt.subplots_adjust(hspace=0.0, right=0.8, wspace=0.0)


        if output_basename is not None:
            print('saving...')
            savefig(output_basename, pdf=False)
        if show:
            plt.show()
        plt.close()
        exit()





    def get_complementary_nt(self, nt):
        if nt == "A":
            return "T"
        if nt == "T":
            return "A"
        if nt == "C":
            return "G"
        if nt == "G":
            return "C"
        if nt == "N":
            return "N"

    def get_complementary_seq(self, s):
        return "".join([self.get_complementary_nt(nt) for nt in s][::-1])

    def is_palindromic(self, s):
        return self.get_complementary_seq(s) == s

    def get_mismatches_palindromic(self, s):
        s2 = self.get_complementary_seq(s)
        return sum([1 for ai, bi in zip(s, s2) if ai != bi])

    def print_extended_hits(self, hits, positions, extension=0):
        """
        Print the extended hits to a temporary bed file.

        :returns: The name of the temporary file.
        :rtype: str

        """

        import tempfile
        import os
        fdescr, tmp_file = tempfile.mkstemp()
        os.close(fdescr)

        with open(tmp_file, 'w') as stream:
            for hit in hits:
                # print hit
                coordinate = hit[0].description
                score = hit[-1]
                strand = hit[3]
                # print 'coordinate'
                identifier = coordinate
                assert identifier in positions
                peak_chrom, peak_start, _ = positions[identifier]
                # print identifier, peak_chrom, peak_start
                chrom, start, end = self.extended_hit_pos(hit, peak_chrom,
                                                          peak_start, extension)
                if not chrom.startswith("chr"):
                    chrom = "chr{0}".format(chrom)
                if score >= 0.0 and score <= 1.0:
                    stream.write(
                        "{0}\t{1:d}\t{2:d}\t{3}\t{4:d}\t{5}\n".format(
                            chrom, start, end, identifier,
                            int(score * 100),
                            strand))
                else:
                    stream.write(
                        "{0}\t{1:d}\t{2:d}\t{3}\t{4:d}\t{5}\n".format(
                            chrom, start, end, identifier, 0,
                            strand))
        return tmp_file

    def extended_hit_pos(self, hit, peak_chrom, peak_start, extension=0):
        """ Extend the hit by 'extension' nt to compute DNAshape features. """
        hit_start, hit_end = hit[1:3]
        start = peak_start + hit_start - extension - 2  # BED
        end = peak_start + hit_end + extension - 1  # BED
        return peak_chrom, start, end

    def not_na(self, item):
        """ Remove NAs and empty values. """
        return not (item == "NA" or item == "")

    def randomize_sequence(self, sequence):
        nucleotides = [nt for nt in sequence]
        shuffle(nucleotides)
        return "".join(nucleotides)

    def scale01(self, values, mini=None, maxi=None, tol=1e-6):
        """ Scale the values in [0, 1]. """
        from numpy import amax, amin
        if not mini:
            mini = amin(values)
        if not maxi:
            maxi = amax(values)
        scaled_values = [(val - mini) / (maxi - mini + tol) for val in values]
        return scaled_values, mini, maxi

    def get_scores(self, in_file, shape=None, scaled=False):
        """ Get DNAshape values on single lines. """
        DNASHAPEINTER = {'HelT': (30.94, 38.05), 'MGW': (2.85, 6.20),
                         'ProT': (-16.51, -0.03), 'Roll': (-8.57, 8.64)}

        with open(in_file) as stream:
            scores = []
            for line in stream:
                values = [item for item in line.rstrip().split()[7].split(',')
                          if self.not_na(item)]
                values = [eval(value) for value in values]
                if scaled:
                    mini, maxi = DNASHAPEINTER[shape]
                    values, _, _ = self.scale01(values, mini, maxi)
                scores.append(values)
            return scores

    def get_positions_from_bed(self, bed_file):
        """ Get the positions of the sequences described in the bed file. """
        handle = gzip.open(bed_file) if '.gz' in bed_file else open(bed_file)
        with handle as stream:
            positions = {}
            for line in stream:
                spl = line.split()
                if len(spl) == 3 or len(spl) > 4:
                    spl = spl[:3]
                    spl += [spl[0] + ":" + spl[1] + "-" + spl[2]]
                positions[spl[3]] = (spl[0], eval(spl[1]) + 1, eval(spl[2]))
        return positions

    def get_shapes_from_bigwig(self, hits, bed_file, first_shape, second_shape, extension=0,
            scaled=False, use_bwtool=True, fasta_file=None):
        """ Retrieve DNAshape feature values for the hits. """
        bigwigs = first_shape + second_shape
        print('bigWig', bigwigs)
        print(bigwigs)
        import subprocess
        import os

        print(bed_file)

        BWTOOL = "bwtool"
        peaks_pos = self.get_positions_from_bed(bed_file)
        with open(os.devnull, 'w') as devnull:
            tmp_file = self.print_extended_hits(hits, peaks_pos, extension)

            shapes = ['HelT', 'ProT', 'MGW', 'Roll', 'HelT2', 'ProT2', 'MGW2',
                    'Roll2']

            hits_shapes = []
            for indx, bigwig in enumerate(bigwigs):
                # print shapes[indx]
                print(indx, 'running bwtool', bigwig)
                if bigwig:
                    out_file = '{0}.{1}'.format(tmp_file, shapes[indx])
                    if not exists(bigwig):
                        print('BigWig file not found')
                        print(bigwig)
                        print('Please, check connection to cluster, file existence, etc...')
                    cmd = [BWTOOL, 'ex', 'bed', tmp_file, bigwig, out_file]
                    print(' '.join(cmd))
                    subprocess.call(cmd, stdout=devnull)
                    if indx < 4:
                        hits_shapes.append(self.get_scores(out_file, shapes[indx], scaled))
                    else:
                        hits_shapes.append(self.get_scores(out_file, shapes[indx]))
            subprocess.call(['rm', '-f', '{0}.HelT'.format(tmp_file),
                '{0}.MGW'.format(tmp_file), '{0}.ProT'.format(tmp_file),
                '{0}.Roll'.format(tmp_file), '{0}.HelT2'.format(tmp_file),
                '{0}.MGW2'.format(tmp_file), '{0}.ProT2'.format(tmp_file),
                '{0}.Roll2'.format(tmp_file),tmp_file])

            return hits_shapes

    def get_shapes_from_bed(self, bed_file, first_shape, second_shape, extension=0,
            scaled=False, use_bwtool=True, fasta_file=None, overwrite=False):
        """ Retrieve DNAshape feature values for the hits. """
        bigwigs = first_shape + second_shape
        print('bigWig', bigwigs)
        print(bigwigs)
        import subprocess
        import os

        print(bed_file)

        BWTOOL = "bwtool"
        peaks_pos = self.get_positions_from_bed(bed_file)
        with open(os.devnull, 'w') as devnull:
            shapes = ['HelT', 'ProT', 'MGW', 'Roll', 'HelT2', 'ProT2', 'MGW2',
                    'Roll2']

            hits_shapes = []
            for indx, bigwig in enumerate(bigwigs):
                # print shapes[indx]
                print(indx, 'running bwtool', bigwig)
                if bigwig:
                    out_file = '{0}.{1}'.format(bed_file, shapes[indx])
                    if not exists(bigwig):
                        print('BigWig file not found')
                        print(bigwig)
                        print('Please, check connection to cluster, file existence, etc...')
                    cmd = [BWTOOL, 'ex', 'bed', bed_file, bigwig, out_file]
                    print(' '.join(cmd))
                    if not exists(out_file) or overwrite:
                        subprocess.call(cmd, stdout=devnull)

    @staticmethod
    def get_X_1mer(seqs):
        # print 'preparing 1mer dataframe...'
        for s in seqs:
            assert [sum([s.count(nt) for nt in 'ACGT']) == len(s)]
        seq_features = [SequenceMethods.get_one_hot_encoding(s) for s in seqs]
        columns = [nt + "." + ("%02d" % i) for i in range(len(seq_features[0]) / 4) for nt in 'ACGT']
        X_seq_df = pd.DataFrame(seq_features, columns=columns)
        # print 'done...'
        return X_seq_df

    @staticmethod
    def get_X_2mer(seqs):
        for s in seqs:
            assert [sum([s.count(nt) for nt in 'ACGT']) == len(s)]
        seq_features = [SequenceMethods.get_one_hot_encoding_dinucl(s) for s in seqs]
        dinucleotides = [a + b for a in ['A', 'C', 'G', 'T'] for b in ['A', 'C', 'G', 'T']]
        print(len(dinucleotides))
        X_seq_df = pd.DataFrame(seq_features, columns=[di_nt + "." + ("%02d" % i)
                                                       for i in range(len(seq_features[0]) / 16)
                                                       for di_nt in dinucleotides])
        return X_seq_df

    @staticmethod
    def get_X_3mer(seqs):
        for s in seqs:
            assert [sum([s.count(nt) for nt in 'ACGT']) == len(s)]
        seq_features = [SequenceMethods.get_one_hot_encoding_trinucl(s) for s in seqs]
        nucleotides = ['A', 'C', 'G', 'T']
        trinucleotides = [a + b + c for a in nucleotides for b in nucleotides for c in nucleotides]
        print(len(trinucleotides))
        X_seq_df = pd.DataFrame(seq_features, columns=[di_nt + "." + ("%02d" % i)
                                                       for i in range(len(seq_features[0]) / 64)
                                                       for di_nt in trinucleotides])
        return X_seq_df

    @staticmethod
    def get_X_3merE2(sequences):
        # include a model that takes into account 1mer+shape+3merE2 (end 2)
        E2_3mer_options = ["".join(c) for c in product('ACGT', repeat=3)]
        E2_3mer = []
        for s in sequences:
            feat = []
            left1, left2, right1, right2 = s[:3], s[1:4], s[-4:-1], s[-3:]
            for a in [left1, left2, right1, right2]:
                feat += [1 if a == opt else 0 for opt in E2_3mer_options]
            E2_3mer.append(feat)
        s = list(sequences)[0]
        X_3merE2 = pd.DataFrame(E2_3mer, columns=[(c + (".%.02d" % i)) for i in [0, 1, len(s) - 4, len(s) - 3] for c in E2_3mer_options])
        return X_3merE2

    @staticmethod
    def get_X_2merE3(sequences):
        # include a model that takes into account 1mer+shape+3merE2 (end 2)
        E2_2mer_options = ["".join(c) for c in product('ACGT', repeat=2)]
        E3_2mer = []
        for s in sequences:
            feat = []
            left1, left2, left3, right1, right2, right3 = s[:2], s[1:3], s[1:4], s[-4:-2], s[-3:-1], s[-2:]
            # print left1, left2, right1, right2
            for a in [left1, left2, left3, right1, right2, right3]:
                feat += [1 if a == opt else 0 for opt in E2_2mer_options]
            E3_2mer.append(feat)
        s = list(sequences)[0]
        X_2merE3 = pd.DataFrame(E3_2mer, columns=[(c + (".%.02d" % i)) for i in [0, 1, 2, len(s) - 5, len(s) - 4, len(s) - 3] for c in E2_2mer_options])
        return X_2merE3

    @staticmethod
    def get_X_2merE2(sequences):
        # include a model that takes into account 1mer+shape+3merE2 (end 2)
        E2_2mer_options = ["".join(c) for c in product('ACGT', repeat=2)]
        E2_2mer = []
        for s in sequences:
            feat = []
            left1, left2, right1, right2 = s[:2], s[1:3], s[-3:-1], s[-2:]
            # print left1, left2, right1, right2
            for a in [left1, left2, right1, right2]:
                feat += [1 if a == opt else 0 for opt in E2_2mer_options]
            E2_2mer.append(feat)
        s = list(sequences)[0]
        X_2merE2 = pd.DataFrame(E2_2mer, columns=[(c + (".%.02d" % i)) for i in [0, 1, len(s) - 4, len(s) - 3] for c in E2_2mer_options])
        return X_2merE2


    @staticmethod
    def get_X_1mer_2mer(seqs):
        # print 'preparing 1mer+2mer dataframe...'
        X_1mer = ShapeAnalyzer.get_X_1mer(seqs)
        X_2mer = ShapeAnalyzer.get_X_2mer(seqs)
        return pd.concat([X_1mer, X_2mer], axis=1)


    @staticmethod
    def get_X_1mer_3mer(seqs):
        # print 'preparing 1mer+2mer dataframe...'
        X_1mer = ShapeAnalyzer.get_X_1mer(seqs)
        X_3mer = ShapeAnalyzer.get_X_3mer(seqs)
        return pd.concat([X_1mer, X_3mer], axis=1)

    @staticmethod
    def get_X_1mer_2mer_3mer(seqs):
        # print 'preparing 1mer+2mer dataframe...'
        X_1mer = ShapeAnalyzer.get_X_1mer(seqs)
        X_2mer = ShapeAnalyzer.get_X_2mer(seqs)
        X_3mer = ShapeAnalyzer.get_X_3mer(seqs)
        return pd.concat([X_1mer, X_2mer, X_3mer], axis=1)

    def get_scanning_sequences(self, seqs, motif_length, double_strand=True):
        table = []
        for si, s in enumerate(seqs):
            for i in range(0, len(s) - motif_length + 1):
                next = s[i: i + motif_length]
                table.append([si, i, "+", next])
                table.append([si, i, "-", SequenceMethods.get_complementary_seq(next)])
        return pd.DataFrame(table, columns=['peak.id', 'position', 'strand', 'seq'])

    @staticmethod
    def get_X_1mer_shape_flanks(seqs, shape_output_prefix='/tmp/shape_test', run_dnashapeR=True,
                                **kwargs):
        # Calculate DNA-shape for test sequences collected from publications
        shape_analyzer = ShapeAnalyzer()
        shape_names_simple = ['1-MGW', '1-HelT', '1-ProT', '1-Roll',
                              '2-MGW', '2-HelT', '2-ProT', '2-Roll']

        # 1st five sequences
        for s in seqs[:5]:
            print(s)
        shape_features_flanks_inferred = shape_analyzer.get_shapes_from_DNAShapeR(seqs,
                                                                                  output_prefix=shape_output_prefix,
                                                                                  overwrite=run_dnashapeR,
                                                                                  shapes=shape_names_simple,
                                                                                  infer_flanks=True,
                                                                                  run=run_dnashapeR,
                                                                                  **kwargs)
        print(shape_features_flanks_inferred.shape)
        seq_features = [SequenceMethods.get_one_hot_encoding(s) for s in seqs]
        X_seq_df = pd.DataFrame(seq_features, columns=[nt + "." + ("%02d" % i)
                                                       for i in range(len(seq_features[0]) / 4)
                                                       for nt in 'ACGT'])
        X_aug_inferred_df = pd.concat([X_seq_df, shape_features_flanks_inferred], axis=1)
        return X_aug_inferred_df

    @staticmethod
    def get_model_coefficients_L2(X, y):
        clf = MachineLearning.TrainRidgeRegressor(X, y)
        res = pd.DataFrame()
        res['feature.position'] = list(range(X.shape[1]))
        res['coef'] = clf.coef_
        return res

    def get_shape_features_map(self, classifier_path):
        classifier = joblib.load(classifier_path)
        feat_imp = pd.Series(classifier.feature_importances_)
        len_motif = (len(feat_imp) - 1) / 8
        assert len_motif * 8 + 1 == len(feat_imp)
        dico = {'Importance': list([feat_imp[0]]) * len_motif +
                list(feat_imp[1:]), 'Feature': ['hit_score'] * len_motif +
                ['HelT'] * len_motif + ['ProT'] * len_motif + ['MGW'] *
                len_motif + ['Roll'] * len_motif + ['HelT2'] * len_motif +
                ['ProT2'] * len_motif + ['MGW2'] * len_motif + ['Roll2'] *
                len_motif, 'Position': list(range(len_motif)) + list(range(len_motif)) +
                list(range(len_motif)) + list(range(len_motif)) + list(range(len_motif)) +
                list(range(len_motif)) + list(range(len_motif)) + list(range(len_motif)) +
                list(range(len_motif))}

        print(dico)
        map = pd.DataFrame.from_dict(dico)
        map = map.pivot(index='Feature', columns='Position',
                        values='Importance')
        show_sequence_features = False
        if not show_sequence_features:
            map = map[:-1]
        map = map.drop(map.index[[1, 3, 5, 7]])
        return map

    def plot_shape_feature_importance(self, classifier_path):
        map = self.get_shape_features_map(classifier_path)
        ax = plt.subplot2grid((2, 1), (0, 0))
        means = list(map.mean())
        print(len(means))
        y_means = [mean(means[max(i - 3, 0): i + 3]) for i in range(len(means))]
        ax.plot([i + .5 for i in range(len(means))], y_means)
        plt.ylim(bottom=min(y_means), top=max(y_means) * 1.001)
        plt.xticks([i + .5 for i in range(len(means))],
                   [i + 1 if (i + 1) % 5 == 0 else "" for i in range(len(means))])
        plt.xlim([0, len(means)])
        plt.xlabel("Position")
        plt.ylabel("all weights\n(mean)")

        ax2 = plt.subplot2grid((2, 1), (1, 0))
        # ax = plt.subplot2grid((2, 1), (0, 1))

        print(map.shape)
        print(map.columns)


        max_feature = max([max(r2.values) for rj, r2 in map.iterrows()])
        sns.heatmap(map, linewidth=.5, vmin=0, vmax=max_feature, ax=ax2,
                    cmap="YlGnBu", cbar_ax=None)
        plt.xticks([p + 0.5 for p in range(map.shape[1])],
                   ["" for p in range(map.shape[1])])
        plt.yticks(rotation=0)
        plt.xlabel('')

    def get_norm01(self, x):
        return [(xi - min(x)) / (max(x) - min(x)) for xi in x]

    def get_labels(self, seq_len):
        cols =  [k + "." + "%02d" % (i + 1) for i in range(seq_len) for k in 'ACGT']
        cols += ["MGW." +  "%02d" % (i + 3) for i in range(seq_len - 4)]
        cols += ["ROLL." + "%02d" % (i + 3) for i in range(seq_len - 3)]
        cols += ["PROT." + "%02d" % (i + 3) for i in range(seq_len - 4)]
        cols += ["HELT." + "%02d" % (i + 3) for i in range(seq_len - 3)]
        cols += ["MGWxMGW." +  "%02d" % (i + 4) for i in range(seq_len - 5)]
        cols += ["ROLLxROLL." + "%02d" % (i + 3) for i in range(seq_len - 4)]
        cols += ["PROTxPROT." + "%02d" % (i + 4) for i in range(seq_len - 5)]
        cols += ["HELTxHELT." + "%02d" % (i + 3) for i in range(seq_len - 4)]
        return cols


    def get_averages_by_position(self, df_bpair, df_bstep, skip_value=-1,
                                 absolute=False,
                                 squared=False,
                                 df_bpair_std=None,
                                 df_bstep_std=None,
                                 by_bpair=False,
                                 by_bstep=False,
                                 **kwargs):
        average_by_position = []
        std_by_position = []

        if squared:
            # print df_bpair
            df_bpair = df_bpair.pow(2)
            df_bstep = df_bstep.pow(2)
            # print df_bpair
            skip_value *= skip_value

            # norm by position
            # print 'sum bpair', sum(df_bpair.sum())
            # print 'sum bstep', sum(df_bstep.sum())
            df_bpair = df_bpair / sum(df_bpair.sum())
            df_bstep = df_bstep / sum(df_bstep.sum())

        # print sum(df_bpair.sum())
        # print sum(df_bstep.sum())

        # print df_bpair
        # print df_bstep

        # print df_bpair_std

        cols_selected = df_bstep.shape[1]

        if not by_bpair and not by_bstep:
            by_bstep = True

        for i in range(df_bstep.shape[1] if by_bstep else df_bpair.shape[1]):
            values, values_std = [], []
            for j, v, e in zip([0, 1], [values, values_std],
                            [[df_bpair, df_bstep], [df_bpair_std, df_bstep_std]]):
                if e[0] is None or e[1] is None:
                    continue
                bpair, bstep = e

                if by_bstep:
                    v = list(bstep[i])
                    if i == 0:
                        v += list(bpair[0])
                    else:
                        if i < bpair.shape[1]:
                            v += list(bpair[i])
                        if i > 0:
                            v += list(bpair[i - 1])
                elif by_bpair:
                    v = list(bpair[i])
                    v += list(bpair[i])
                    v += list(bstep[i + 1])

                v = [vi for vi in v if vi != skip_value]

                if absolute:
                    if j == 0:
                        average_by_position.append(average([abs(vi) for vi in v]))
                    elif j == 1:
                        std_by_position.append(average([abs(vi) for vi in v]))
                else:
                    if j == 0:
                        average_by_position.append(average([vi for vi in v]))
                    elif j == 1:
                        std_by_position.append(average([vi for vi in v]))

        return [vi / sum(average_by_position) for vi in average_by_position], std_by_position

    def get_averages_by_position_from_clf(self, clf, seq_len, n_flanks, skip_value=-1,
                                          absolute=True, squared=True, **kwargs):

        feat_imp = None
        seq_feat = None
        shape_feat = None
        if hasattr(clf, 'coef_'):
            feat_imp = pd.Series(clf.coef_)
            seq_feat = list(feat_imp[:4 * seq_len])
            shape_feat = list(feat_imp[4 * seq_len:])
        else:
            feat_imp = pd.Series(clf.feature_importances_)
            seq_feat = [feat_imp[0]]
            shape_feat = list(feat_imp[1:])
            seq_len = len(shape_feat) / 8
            assert len(shape_feat) % 8 == 0

        table, df_bpair, df_bstep = self.get_dfbpair_n_dfbstep(shape_feat, seq_len, n_flanks,
                                                               viz_scaled=False)

        return self.get_averages_by_position(df_bpair, df_bstep, skip_value=skip_value,
                                             absolute=absolute, squared=squared, **kwargs)


    def get_dfbpair_n_dfbstep(self, shape_feat, seq_len, n_flanks,
                              viz_scaled=False, n_additional_flanks_left=0,
                              n_additional_flanks_right=0, shape_stdev=None):
        # this is the way the shape features were read in the first place
        shapes = ['1-MGW', '1-HelT', '1-ProT', '1-Roll',
                  '2-MGW', '2-HelT', '2-ProT', '2-Roll']
        # this dictionary keeps the number of indexes that we differentiate between
        # each shape feature, with respect to the number of seq features (ie substraction)
        offset = {'1-MGW': 4, '1-Roll': 3, '1-ProT': 4, '1-HelT': 3,
                  '2-MGW': 5, '2-Roll': 4, '2-ProT': 5, '2-HelT': 4}

        table = []
        table_stdev = []
        sum_nfeat = 0
        print(len(shape_feat))
        print(seq_len)

        for shape_id in shapes:
            next_weights = shape_feat[:seq_len - offset[shape_id] + 4]
            next_stdev = None
            if shape_stdev is not None:
                next_stdev = shape_stdev[:seq_len - offset[shape_id] + 4]

            # print len(next_weights)
            sum_nfeat += len(next_weights)
            if n_flanks != 0:
                table.append(list(np.repeat(0.000000001, n_flanks + n_additional_flanks_left)) +
                             list(next_weights) +
                             list(np.repeat(0.000000001, n_flanks + n_additional_flanks_right)))
                if next_stdev is not None:
                    table_stdev.append(list(np.repeat(0.000000001,
                                                n_flanks + n_additional_flanks_left)) +
                                 list(next_stdev) +
                                 list(np.repeat(0.000000001,
                                                n_flanks + n_additional_flanks_right)))

            else:
                table.append(next_weights)
                if next_stdev is not None:
                    table_stdev.append(next_stdev)

            # print next_weights
            shape_feat = shape_feat[seq_len - offset[shape_id] + 4:]
            if shape_stdev is not None:
                shape_stdev = shape_stdev[seq_len - offset[shape_id]:]
        #

        # for t, shapeid in zip(table, shapes):
        #     print shapeid, len(t), list(t)
        df_bpair = pd.DataFrame([list(table[0]), list(table[2])],
                                columns=[i for i in range(len(table[0]))])
        df_bstep = pd.DataFrame([list(table[1]), list(table[3])],
                                columns=[i for i in range(len(table[1]))])

        if len(table_stdev) != 0:
            df_bpair_std = pd.DataFrame([list(table_stdev[0]), list(table_stdev[2])],
                                    columns=[i for i in
                                             range(len(table_stdev[0]))])
            df_bstep_std = pd.DataFrame([list(table_stdev[1]), list(table_stdev[3])],
                                    columns=[i for i in
                                             range(len(table_stdev[1]))])

            return table, df_bpair, df_bstep, df_bpair_std, df_bstep_std
        return table, df_bpair, df_bstep

    def get_average_dataframe(self, df_list):
        assert len({df.shape[0] for df in df_list}) == 1
        assert len({df.shape[1] for df in df_list}) == 1

        sum_df = None
        for i in range(len(df_list)):
            if sum_df is None:
                sum_df = df_list[i]
                sum_df = df_list[i]
            else:
                sum_df += df_list[i]
                sum_df += df_list[i]
        df_avr = sum_df / len(df_list)

        return df_avr

    def get_std_dataframe(self, df_list):
        assert len({df.shape[0] for df in df_list}) == 1
        assert len({df.shape[1] for df in df_list}) == 1

        std_df = pd.DataFrame().reindex_like(df_list[0])
        for ci in range(df_list[0].shape[1]):
            for ri in range(df_list[0].shape[0]):
                values = [list(df[ci])[ri] for df in df_list]
                # print ci, ri, values, np.std(values)
                std_df.set_value(ri, ci, np.std(values))

        return std_df


    def plot_shape_features_with_motif(self, clf, seq_len,
                                       hypothesis_id,
                                       output_basename=None,
                                       ppm=None,
                                       show=False,
                                       n_flanks=0,
                                       averages_basename=None,
                                       use_weight_abs=False,
                                       close=True,
                                       n_cols=1, curr_col=0, fig=None,
                                       color1='Reds', color2='Blues', seqs=None,
                                       show_complementary=False, invivo=False):
        assert hypothesis_id is not None

        use_ppm = ppm is not None
        # feat_imp = pd.Series(clf.feature_importances_)

        feat_imp = None
        if isinstance(clf, list):
            if not invivo:
                feat_imp = [abs(ai - bi) for ai, bi in zip(clf[0].coef_, clf[1].coef_)]
            else:
                feat_imp = [pd.Series(next.feature_importances_) for next in clf]

        else:
            if not invivo:
                feat_imp = pd.Series(clf.coef_)
            else:
                feat_imp = pd.Series(clf.feature_importances_)

        if use_weight_abs:
            if not isinstance(list):
                feat_imp = feat_imp.abs()
            else:
                feat_imp = [next.abs() for next in feat_imp]

        if not invivo:
            if not isinstance(clf, list):
                seq_feat = list(feat_imp[:4 * seq_len])
                shape_feat = feat_imp[4 * seq_len:]
        else:
            if not isinstance(clf, list):
                seq_feat = [feat_imp[0]]
                shape_feat = list(feat_imp[1:])
                seq_len = len(shape_feat) / 8
                assert len(shape_feat) % 8 == 0
            else:
                seq_feat = []
                shape_feat = []
                print(len(feat_imp))
                for next in feat_imp:
                    next_seq_feat = [next[0]]
                    next_shape_feat = list(next[1:])
                    seq_len = len(next_shape_feat) / 8
                    assert len(next_shape_feat) % 8 == 0
                    seq_feat.append(next_seq_feat)
                    shape_feat.append(next_shape_feat)


        table, df_pair, df_step = None, None, None
        if not isinstance(clf, list):
            table, df_bpair, df_bstep = self.get_dfbpair_n_dfbstep(shape_feat,
                                                                   seq_len,
                                                                   n_flanks)

        else:

            table, df_bpair, df_bstep = list(zip(*[self.get_dfbpair_n_dfbstep(next_shape_feat,
                                                                         seq_len,
                                                                         n_flanks)
                                            for next_shape_feat in shape_feat]))
            # average across all features
            df_bpair = self.get_average_dataframe(df_bpair)
            df_bstep = self.get_average_dataframe(df_bstep)

        print(df_bpair)
        print(df_bpair.shape)

        print(df_bstep)
        print(df_bstep.shape)

        if fig is None:
            fig = plt.figure()

        cbar_ax = fig.add_axes([.95, .53, .010, .10])
        # lower heatmap
        ax2 = plt.subplot2grid((6, n_cols), (2, curr_col))
        max1 = max([max([r2i for r2i in r2.values if not math.isnan(r2i)])
                                for rj, r2 in df_bpair.iterrows()])
        print(df_bstep)
        max2 = max([max([r2i for r2i in r2.values if not math.isnan(r2i)])
                                for rj, r2 in df_bstep.iterrows()])

        print(max1, max2, max(max1, max2))

        masking_number = -1
        mask = df_bpair == masking_number

        print(df_bpair.shape)
        if show_complementary:
            df_bpair = df_bpair[df_bpair.columns[::-1]]
            print(df_bpair.shape)

        ax2 = sns.heatmap(df_bpair, linewidth=.1, vmin=0, vmax=max(max1, max2), ax=ax2,
                    cmap=color1, cbar_ax=cbar_ax, cbar_kws={'label': 'abs(' + r'$\omega$' + ')'})

        ax2 = sns.heatmap(df_bpair, mask=~mask, cmap=ListedColormap(['white']), ax=ax2,
                          cbar=None, vmin=0, vmax=max(max1, max2))
        plt.xlim([-.5, df_bpair.shape[1] + .5])

        ax2.annotate('MLR weights', xy=(2, 1), xytext=(-2.5, 0), rotation=90,
                     va='center', fontsize=15)

        print(len(plt.gcf().axes))
        cax = plt.gcf().axes[-2]
        cax.tick_params(labelsize=8)
        cax.set_ylabel('abs(' + r'$\omega$' + ')', fontsize=15)

        # ax2.tick_params(labelsize=6)

        # ax2.set_aspect(1)
        plt.yticks(rotation=0, fontsize=15)
        plt.yticks([yi + 0.5 for yi in range(df_bpair.shape[0])], ['MGW', 'HelT'])
        ax2.set_xticks([])
        cbar_ax = fig.add_axes([.95, .38, .010, .10])
        # top heatmap



        ax4 = plt.subplot2grid((6, n_cols), (3, curr_col))

        sns.axes_style('white')

        print(mask.shape)
        print(df_bstep.shape)
        df_bstep.columns = [i for i in range(df_bstep.shape[1])]
        mask = df_bstep == masking_number
        print(df_bstep.shape)
        # print mask
        if show_complementary:
            df_bstep = df_bstep[df_bstep.columns[::-1]]

        print(df_bstep)
        ax4 = sns.heatmap(df_bstep, linewidth=.1, vmin=0, vmax=max(max1, max2), ax=ax4,
                          cmap=color2, cbar_ax=cbar_ax, cbar_kws={'label': 'abs(' + r'$\omega$' + ')'})

        # print len(plt.gcf().axes)
        # print len(plt.gcf().axes), plt.gcf().axes
        # cax = plt.gcf().axes[2]
        # cax.tick_params(labelsize=20)
        # ax4.tick_params(labelsize=6)

        mask = ~mask
        print(mask.shape)
        print(df_bstep.shape)
        print(mask)
        print(df_bstep)
        ax4 = sns.heatmap(df_bstep, cmap=ListedColormap(['white']), ax=ax4,
                          cbar=None, vmin=0, vmax=max(max1, max2), mask=mask)

        # plt.xlim([0.5, df_bstep.shape[1] - 0.5])

        # plt.xlim([-1, df_bstep.shape[1]])

        print(plt.gcf().axes)
        cax = plt.gcf().axes[-2]
        cax.tick_params(labelsize=8)
        cax.set_ylabel('abs(' + r'$\omega$' + ')', fontsize=15)

        # ax4.set_aspect(1)
        plt.yticks(rotation=0, fontsize=15)
        plt.subplots_adjust(hspace=0.0)
        plt.yticks([yi + 0.5 for yi in range(df_bpair.shape[0])], ['ProT', 'Roll'])
        plt.yticks(rotation=0)
        ax4.set_xticks([])

        plot_motif = True
        if plot_motif:
            sns.set_style('white')
            print(ppm)

            if ppm is None and seqs is None: # make a ppm with the seq features
                rows = [seq_feat[i * 4: (i + 1) * 4] for i in range(seq_len)]
                probs = [[abs(pi) / sum([abs(pi) for pi in r]) for pi in r]
                         for r in rows]
                ppm = pd.DataFrame(probs, columns=['A', 'C', 'G', 'T']).transpose()
            if seqs is None:

                print('here...')
                print(ppm)
                if show_complementary:
                    ppm = ppm[ppm.columns[::-1]]
                    ppm.columns = [ci for ci in range(ppm.shape[1])]
                    ppm = ppm.reindex(["T", "G", "C", 'A'])
                    ppm.index = ['A', 'C', 'G', 'T']
                cbp = ConsensusMotifPlotter.from_ppm(ppm)
            else:
                if not isinstance(seqs[0], list):
                    cbp = ConsensusMotifPlotter.from_sequences(seqs)
                else:
                    cbp = ConsensusMotifPlotter.from_sequences(seqs[0],
                                                               other=seqs[1])



            ax = plt.subplot2grid((6, n_cols), (0, curr_col), rowspan=2)
            cbp.plot(ax) #  title=r['motif.id'] + "\n" +
                         #       r['M1_name'] + "_" + r['M2_name'])
            plt.xlabel('')

            plt.xlim([-.5, seq_len + .5])
            plt.ylabel('Bits', fontsize=15)

            print(hypothesis_id)
            plt.title(hypothesis_id)
            despine_all()
            ax.set_xticks([])

            table, df_pair, df_step, df_bpair_std, df_bstep_std = None, None, None, None, None
            if not isinstance(clf, list):
                table, df_bpair, df_bstep = self.get_dfbpair_n_dfbstep(
                    shape_feat,
                    seq_len,
                    n_flanks)
            else:

                table, df_bpair, df_bstep = list(zip(
                    *[self.get_dfbpair_n_dfbstep(next_shape_feat,
                                                 seq_len,
                                                 n_flanks)
                      for next_shape_feat in shape_feat]))

                # average across all features
                # average across all features
                df_bpair, df_bpair_std = self.get_average_dataframe(df_bpair),\
                                         self.get_std_dataframe(df_bpair)
                df_bstep, df_bstep_std = self.get_average_dataframe(df_bstep),\
                                         self.get_std_dataframe(df_bstep)


            average_by_position = []

            square_weights = True

            average_by_position, std_by_position = self.get_averages_by_position(df_bpair, df_bstep,
                                                                                 squared=square_weights,
                                                                                 df_bpair_std=df_bpair_std,
                                                                                 df_bstep_std=df_bstep_std)
            print(average_by_position)
            print(std_by_position)

            average_by_position_other = None
            average_by_position_ref = None
            # if isinstance(clf, list):
            #    average_by_position_ref, \
            #     average_by_position_other = [self.get_averages_by_position_from_clf(clfi,
            #                                                                     seq_len,
            #                                                                     0,
            #                                                                    squared=square_weights)
            #                                for clfi in clf]

            if show_complementary:
                average_by_position = average_by_position[::-1]
                std_by_position = std_by_position[::-1]
                if average_by_position_other is not None:
                    average_by_position_other = average_by_position_other[::-1]
                    average_by_position_ref = average_by_position_ref[::-1]

            smooth_curve = True
            if not smooth_curve:
                ax5 = plt.subplot2grid((4, n_cols), (3, curr_col))

                print(len(average_by_position), average_by_position)
                #
                if average_by_position_other is not None:
                    plt.plot([i + 1 for i in range(len(average_by_position_ref))],
                             average_by_position_ref)
                    plt.plot([i + 1 for i in range(len(average_by_position_other))],
                             average_by_position_other)
                    plt.title(", ".join(
                        [("%.3f" % vi) for vi in spearmanr(average_by_position_ref,
                                                           average_by_position_other)]))
                else:
                    print(average_by_position)
                    plt.plot([i + 0.5 for i in range(len(average_by_position))],
                             average_by_position, marker='o', color='#4daf4a')


                # plt.xlim([-1.5, len(average_by_position) + 1.5])
                plt.xlim([0, len(average_by_position)])
                despine_all()

                plt.xlabel('')
                plt.ylabel(r'$\omega$' + '$^2$', fontsize=15)
                plt.yticks(np.arange(0, max(average_by_position) + 0.001,
                                     max(average_by_position) / 3),
                           ["%.3f" % f for f in
                            np.arange(0, max(average_by_position) + 0.001,
                                      max(average_by_position) / 3)])
                # plt.locator_params(axis='y', nticks=3)
                plt.xticks([])
            else:
                # add smoothing curve
                w = 2
                n = len(average_by_position)
                print(average_by_position)
                smoothed = []
                for i in range(n):
                    next = average_by_position[max(0, i - w + 1): min(i + w, n)]
                    print(i, max(0, i - w + 1), min(i + w, n), next, mean(next))
                    smoothed.append(mean(next))

                print(smoothed)
                # smoothed = average_by_position
                # print average_by_position

                ax5 = plt.subplot2grid((4, n_cols), (3, curr_col))
                print(len(average_by_position))
                if average_by_position_other is None:
                    print(len(smoothed), smoothed)
                    if std_by_position is None or len(std_by_position) == 0:
                        plt.plot([i + .5 for i in range(len(smoothed))], smoothed, marker="o", color='#4daf4a')
                    else:
                        print('std', std_by_position)
                        plt.errorbar([i + .5 for i in range(len(smoothed))],
                                     smoothed, yerr=[stdi / 2.0 for stdi in std_by_position],
                                     marker="o", color='#4daf4a')
                else:
                    smoothed1, smoothed2 = [[mean(v[max(0, i - w): min(i + w, n)]) for i in range(n)] for v in
                                            (average_by_position_ref, average_by_position_other)]
                    plt.plot([i + .5 for i in range(len(smoothed))], smoothed1, color='#4daf4a')
                    plt.plot([i + .5 for i in range(len(smoothed))], smoothed2, color='#4daf4a')
                    plt.title(", ".join([("%.3f" % vi) for vi in spearmanr(smoothed1, smoothed2)]))

                print(len(smoothed))
                # plt.xlim([-1.5, len(smoothed) + 1.5])
                plt.xlim([0, len(smoothed)])
                # despine_all()
                plt.xlabel('')
                plt.ylabel(r'$\omega$' + '$^2$ (avr. norm.)', fontsize=15)
                plt.grid(True)
                # plt.locator_params(axis='y', nticks=3)
                # plt.yticks(np.arange(0, max(average_by_position) + 0.001,
                #                      max(average_by_position) / 3),
                #            ["%.3f" % f for f in
                #             np.arange(0, max(average_by_position) + 0.001,
                #                       max(average_by_position) / 3)])
                # plt.locator_params(axis='y', nticks=3)
                # plt.xticks([])

                plt.xlabel('shape feature position')
                xticks = [i + 1 for i in range(len(average_by_position))]
                plt.xticks([xi - 0.5 for xi in xticks], xticks)

            plt.subplots_adjust(hspace=0.0)
            # save averaged values
            if averages_basename is not None:
                cPickle.dump(average_by_position, open(averages_basename + "_normal.pkl", "w"))
                cPickle.dump(average_by_position, open(averages_basename + "_smoothed.pkl", "w"))

            # load hQTLs associated to this complex, by position
            cm_hits_dir = "../data/cm_overlap_w_qtls"
            path = join(cm_hits_dir, str(hypothesis_id) + ".tsv")
            cm_id = str(hypothesis_id)
            # skip empty files

            show_qtls = False
            if show_qtls:
                reject = False
                if os.path.getsize(path) == 0:
                    path = "../data/cm_overlap_w_qtls/1130792318.tsv"
                    reject = True
                hQTLs = pd.read_csv(path, sep='\t', index_col=None, header=None)
                hyp_id = int(str(hypothesis_id).split(".")[0])
                hQTLs['strand'] = hQTLs[6]
                hQTLs['snp.position'] = hQTLs[20]
                hQTLs['start'] = hQTLs[1]
                hQTLs['end'] = hQTLs[2]

                hQTLs['motif.position'] = [
                    t['snp.position'] - t['start'] if t['strand'] == '+'
                    else t[2] - t['snp.position'] for ti, t in hQTLs.iterrows()]
                if reject:
                    hQTLs = hQTLs.head(0)
                    print(hQTLs.shape)

                if hQTLs.shape[0] != 0:
                    ax6 = plt.subplot2grid((6, 1), (5, 0))
                    hQTLs['counts.at.position'] = [hQTLs[hQTLs['motif.position'] == c].shape[0]
                                                   for c in hQTLs['motif.position']]


                    hQTLs['id'] = hQTLs[0] + ":" + hQTLs['snp.position'].astype(str)
                    print(hQTLs.shape)
                    unique = hQTLs.drop_duplicates('id')
                    unique['counts.at.position'] = [unique[unique['motif.position'] == c].shape[0]
                                                   for c in unique['motif.position']]
                    print(unique.shape)
                    sns.countplot('motif.position', color='green', data=unique)
                    plt.xticks([i for i in range(seq_len)],
                               [i + 1 for i in range(seq_len)])
                    plt.xlim([-0.5, seq_len - 0.5])

                    yint = list(range(0, int(math.ceil(max(unique['counts.at.position'])) + 1)))
                    plt.yticks(yint)

                    despine_all()
                    plt.ylabel('hQTLs count')

                    print(hQTLs)
            # exit()

            if output_basename is not None:
                savefig(output_basename, pdf=False, dpi=100)

        if show:
            plt.show()
        # plt.show()

        if close:
            plt.close()
