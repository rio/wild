'''
Created on Nov 21, 2016

@author: ignacio
'''

from lib.utils import *
from .SNPAnalyzer import SNPAnalyzer
from .PeaksAnalyzer import PeaksAnalyzer
from scipy.stats import fisher_exact
import sys

class EnrichmentAnalyzer(object):

    def get_enrichment_from_bed(self, target_coordinates, filtered_coordinates,
                                reference_coordinates, label='id'):
        """
        Get the # of intersections between elements in three BED paths
        
        # target_coordinates: The coordinates we want to compare against
        # filterd_coordinates: A subset of reference coordinates, in which we will assess
        # enrichment against
        # reference coordinates: All the coordinates that will be considered.
        """
        peaks_analyzer = PeaksAnalyzer()
        nA, a, b = peaks_analyzer.insersect_chip_seq_peaks(target_coordinates,
                                                           filtered_coordinates,
                                                            "../tmp/tmp_intesections.bed")
        nA, c, d = peaks_analyzer.insersect_chip_seq_peaks(target_coordinates,
                                                           reference_coordinates,
                                                           "../tmp/tmp_intesections.bed")

        observations_matrix = [[d, c], [b, a]]
        line = "\t".join(map(str, [label, d, c, b, a] +
                             list(self.get_enrichment(observations_matrix))))
        
        print(line)

    def get_enrichment_from_matrix(self, observations_matrix, alternative='greater'):
        '''
        It calculates the enrichment of a in b w.r.t. c in d
        :param observations_matrix:
        :param alternative:
        :return: enrichment odds and p-value for the enrichment
        '''
        a = observations_matrix[1][1]
        b = observations_matrix[1][0]
        c = observations_matrix[0][1]
        d = observations_matrix[0][0]
        return fisher_exact([[d, c], [b, a]], alternative=alternative)

    def get_intersection_n(self, ids1, ids2):
        '''
        Give back the amount of elemetns that are common in ids1 and ids2, as a set
        :param ids1:
        :param ids2:
        :return:
        '''
        return len(ids1.intesection(ids2))

    @staticmethod
    def get_motif_hits(motif_ids=None):
        motifs_dir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data/motif_hits_cisbp_build_1.94d_mm10'

        if motif_ids is None:
            motif_ids = set()
            for f in listdir(motifs_dir):
                motif_ids.add(f.replace(".tsv.gz", ''))

        from lib.HumanTFs import HumanTFs
        from lib.MyGeneAnalyzer import MyGeneAnalyzer
        tfs = HumanTFs.get_tf_motifs_cisbp()
        ensembl_by_model = {m: set(grp['Ensembl ID']) for m, grp in tfs.groupby('CIS-BP ID')}
        tfname_by_ensembl = DataFrameAnalyzer.get_dict(tfs, 'Ensembl ID', 'HGNC symbol')

        human_orthologs = DataFrameAnalyzer.read_tsv("../../data/human_mm10_homologs.tsv", sep='\t')
        # print human_orthologs.head()
        engmus_by_enghuman = DataFrameAnalyzer.get_dict(human_orthologs, 'Gene stable ID', 'Mouse gene stable ID')

        # using new ENSEMBL identifiers to map between old and new db
        counter = -1
        print('# motifs able to be mapped', len(motif_ids))

        motif_hits = []
        for m in motif_ids:
            print(counter, 'next motif', m)
            counter += 1
            grp = DataFrameAnalyzer.read_tsv_gz(join(motifs_dir, m + ".tsv.gz"))
            grp.columns = ['motif.id', 'ensembl'] + list(grp.columns[2:])
            # put gene name into res column
            from lib.SequenceMethods import SequenceMethods
            tss = MyGeneAnalyzer.get_gene_tss('mouse', 2000)
            tss = SequenceMethods.parse_range2coordinate(tss)
            grp['gene.name'] = grp['ensembl'].map(DataFrameAnalyzer.get_dict(tss, 'range', 'SYMBOL'))
            motif_hits.append(grp)
        motif_hits = pd.concat(motif_hits)
        return motif_hits

    @staticmethod
    def get_motif_enrichments_by_pairwise_grouping(genes_by_grouping, motif_hits, label=None, column_gene='ensembl',
                                                   stopat=None, key_c1=None, query_keys=None):

        grouping_names = list(genes_by_grouping.keys())
        # print genes_df.keys()
        queries = [[c1, c2] for c1, c2 in product(grouping_names, repeat=2)]
        print('# queries', len(queries))
        print('calculating enrichment log2FC ...')

        motif_hits_by_ont = {ont: motif_hits[motif_hits[column_gene].isin(genes_by_grouping[ont])] for ont in grouping_names}
        # for c in motif_hits_by_ont:
        #     print(c, motif_hits_by_ont[c].shape[0])

        enrichments = []

        # import pdb; pdb.set_trace()
        hits_by_gene_by_c1 = {}
        hits_by_gene_by_c2 = {}

        for qi, q in enumerate(queries): # [:2000]):
            c1, c2 = q

            # print(c1, c2)
            k = c1 + ":" + c2


            if query_keys is not None and not k in query_keys:
                continue

            if qi % 1000 == 0:
                print(qi, 'out of', len(queries))

            if c1 == c2:
                t = [c1, c2, None, None, None, None, label, None, None, 1, 0.0, 1.0, 0, 1.0]
                enrichments.append(t)
                continue
            if key_c1 is not None and not key_c1 in c1:
                continue

            genes2 = set(genes_by_grouping[c2])
            genes1 = set(genes_by_grouping[c1])  # - genes2

            # print(c1, len(genes1), c2, len(genes2))
            # genes1_mus = {engmus_by_enghuman[g] for g in genes1 if g in engmus_by_enghuman}
            # genes2_mus = {engmus_by_enghuman[g] for g in genes2 if g in engmus_by_enghuman}
            # print grp.head()

            # print grp.shape[0]
            # print grp.head()
            # check in mouse and human genes, respectively
            # sel1 = motif_hits[motif_hits['ensembl'].isin(genes1_mus)]
            # if sel1.shape[0] == 0:

            # sel2 = motif_hits[motif_hits['ensembl'].isin(genes2_mus)]
            # if sel2.shape[0] == 0:

            hits1_by_gene = None
            # these variables need to be outside for loop
            sel1 = motif_hits_by_ont[c1]
            if not c1 in hits_by_gene_by_c1:
                # print(c1, sel1.shape[0])
                hits1_by_gene = dict(sel1[column_gene].value_counts())
                for gene_id in genes1:
                    if not gene_id in hits1_by_gene:
                        hits1_by_gene[gene_id] = 0
                hits_by_gene_by_c1[c1] = hits1_by_gene
            hits1_by_gene = hits_by_gene_by_c1[c1]
            # print(c1, len(hits1_by_gene))

            hits2_by_gene = None
            # these variables need to be outside for loop
            sel2 = motif_hits_by_ont[c2]
            if not c2 in hits_by_gene_by_c2:
                # print(c2, sel2.shape[0])
                hits2_by_gene = dict(sel2[column_gene].value_counts())
                for gene_id in genes2:
                    if not gene_id in hits2_by_gene:
                        hits2_by_gene[gene_id] = 0
                hits_by_gene_by_c2[c2] = hits2_by_gene
            hits2_by_gene = hits_by_gene_by_c2[c2]

            # print c1, len(genes1), sel1.shape[0], c2, len(genes2), sel2.shape[0]
            # hits1_by_gene = {}
            # for ensg_id, grp2 in sel1.groupby(column_gene):
            #     hits1_by_gene[ensg_id] = grp2.shape[0]
            # for gene_id in genes1:
            #     if not gene_id in hits1_by_gene:
            #         hits1_by_gene[gene_id] = 0
            # hits2_by_gene = {}
            # for ensg_id, grp2 in sel2.groupby(column_gene):
            #     hits2_by_gene[ensg_id] = grp2.shape[0]
            # for gene_id in genes2:
            #     if not gene_id in hits2_by_gene:
            #         hits2_by_gene[gene_id] = 0

            t_stat, pval_ttest = ttest_ind(list(hits1_by_gene.values()),
                                           list(hits2_by_gene.values()))

            scores1, scores2 = sel1['score'], sel2['score']
            a, b = len(set(sel1[column_gene])), len(genes1)
            c, d = len(set(sel2[column_gene])), len(genes2)
            # print(a, b, c, d)

            odd_ratio, pval = fisher_exact([[a, b], [c, d]], alternative='greater')
            log2FC = np.log2(odd_ratio)

            t = [c1, c2, a, len(genes1), c, len(genes2), label, \
                 median(scores1), median(scores2), odd_ratio,
                 -1 if log2FC == float("-inf") else (1 if log2FC == float("inf") else log2FC), pval, t_stat,
                 pval_ttest]

            # print odd_ratio, pval
            enrichments.append(t)
            # print(enrichments[-1])

            if stopat is not None and len(enrichments) >= stopat:
                break
            # print enrichments[-1]

        enrichments = pd.DataFrame(enrichments, columns=['a', 'b', 'n.a.w.motif', 'n.a', 'n.b.w.motif', 'n.b',
                                                         'motif', 'median.a', 'median.b', 'odds.ratio', 'log2FC',
                                                         'p.val',
                                                         'ttest.ind', 'p.val.ttest'])
        return enrichments





if __name__ == '__main__':
    from os.path import exists
    # test our script by loading some examples
    path = '../armando_snps/data/BRD2-CTCF.bed.gz'
    print(exists(path))
    enrichment_analyzer = EnrichmentAnalyzer()
    enrichment_analyzer.get_enrichment_from_bed(path, path, path)
    sys.exit()
    matrix = [[1000, 100], [1000, 120]]
    print(enrichment_analyzer.get_enrichment_from_matrix(matrix))