'''
Created on 11/3/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.SequenceMethods import SequenceMethods
from lib.PeaksAnalyzer import PeaksAnalyzer
from lib.FastaAnalyzer import FastaAnalyzer
from lib.REMAPAnalyzer import REMAPAnalyzer
from lib.GREATAnalyzer import GREATAnalyzer
from lib.OntologiesAnalyzer import OntologiesAnalyzer
import pybedtools
from pybedtools import BedTool

class GOCombinatorialBinding():

    @staticmethod
    def calculate_zscores_regions_vs_gos(peaks, group, thr, min_ngenes=10, max_ngenes=1000, stopat=None, query_ids=set(),
                                         ncores_perm=1, motif=None,
                                         zscore_column=None, zscore_perm=200, maxtimemin=10, priority_ids=set(),
                                         nperm_by_core=150, **kwargs):

        t_start = get_time_now()
        great = GREATAnalyzer()

        print("Priority IDs for this TF pair combination (positives): ", len(priority_ids))
        print('remaining IDs with mismatch thr=', thr, len(query_ids))
        print('remaining IDs with priority and mismatches thr=', thr, len(priority_ids))

        t0 = get_time_now()
        peaks['motif.selected'] = peaks['motif.score'] >= thr if not np.isnan(thr) else np.nan

        if not 'intersected' in peaks:
            peaks['intersected'] = np.nan
        great_res = great.run_great_local(peaks[['chr', 'start', 'end', 'k.summit', 'motif.selected', 'intersected']],
                                          # query_ids=query_ids, stopat=5,
                                          query_ids=query_ids, stopat=stopat, priority_ids=priority_ids,
                                          nperm_by_core=nperm_by_core, ncores_perm=ncores_perm,
                                          maxtimemin=maxtimemin * (0.33 if group == 'peaks' else 1.0),
                                          zscore_column=zscore_column,  # zscore_nperm,
                                          min_ngenes=min_ngenes, max_ngenes=max_ngenes, **kwargs)
        great_res['thr'] = thr
        great_res['group'] = group

        return great_res

    @staticmethod
    def get_spscore_results(ontology_id, overwrite=False, **kwargs):
        print(ontology_id)
        print('collecting paths...')
        d = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/combinatorial_binding_analysis_invivo/data/GREAT_zscores_downsampling_v2019/%s' % ontology_id
        paths = [join(d, d2, f) for d2 in listdir(d) for f in listdir(join(d, d2))]  # f.endswith('peaks.tsv.gz')]
        # paths = [join(d, d2, f) for d2 in listdir(d) for f in listdir(join(d, d2)) if f.endswith('peaks.tsv.gz')]

        sel_tf_pairs = set()
        sel_paths = []  # just one path by TF pair is necessary to get all info
        for p in paths:
            code = "_".join(basename(p).split("_")[:2])
            if not code in sel_tf_pairs:
                sel_paths.append(p)
                sel_tf_pairs.add(code)

        paths = sel_paths

        res = []

        # important for keeping output in a dictionary
        from multiprocessing import Manager
        manager = multiprocessing.Manager()
        return_dict = manager.dict()

        def get_res(p, pi, column_genes, column_peaks):
            tf1, tf2 = basename(p).split("_")[:2]
            if pi % 100 == 0:
                print('reading file # ', pi, ontology_id, tf1, tf2)
            scores = GOCombinatorialBinding.get_data_matrix(tf1 + "_" + tf2, ontology_id,
                                                            overwrite=overwrite, column_genes=column_genes,
                                                            column_peaks=column_peaks)
            return_dict[pi] = scores

        # paths = [p for p in paths if 'FOXO1' in p and 'ETV6' in p]
        N = len(paths) if kwargs.get('stopat', None) is None else kwargs.get('stopat', None)
        ncores = 20
        # define arugments
        print(kwargs.get('column_genes'))
        print(kwargs.get('column_peaks'))
        args_list = [[paths[pi], pi, kwargs.get('column_genes'), kwargs.get('column_peaks')]
                     for pi in range(len(paths[:N]))]
        # run!
        ThreadingUtils.run(get_res, args_list, ncores)

        print('concatenating...')
        res = pd.concat([return_dict[k] for k in list(return_dict.keys())])

        # res = res[~np.isnan(res['name'])]

        res = res[~np.isnan(res['peaks+kmers.genes']) & ~np.isnan(res['peaks+kmers.peaks'])]

        if 'peaks.peaks' in res:
            res = res[~np.isnan(res['peaks.peaks'])]
        if 'peaks.genes' in res:
            res = res[~np.isnan(res['peaks.genes'])]

        # add names to terms
        from lib.OntologiesAnalyzer import OntologiesAnalyzer
        ont_df, n_by_term = OntologiesAnalyzer.get_gos()
        name_by_id = DataFrameAnalyzer.get_dict(ont_df, 'id', 'name')
        res['name'] = res['go.id'].map(name_by_id)

        # replace by the column mean cases in which the observed value is inf or nan
        for c in res:
            if not np.issubdtype(res[c].dtype, np.number):
                continue
            mu = np.ma.masked_invalid(res[c]).mean()  # 0
            print('setting inf/nan columns to column mean', c, mu)
            res[c] = np.where(~np.isnan(res[c]), res[c], mu)
            res[c] = np.where(~np.isinf(res[c]), res[c], mu)
            print(c, average(res[c]))

        res['tf1'] = res['tf.pair'].str.split("_").str[0]
        res['tf2'] = res['tf.pair'].str.split("_").str[1]

        ontologies = OntologiesAnalyzer.get_gos()
        sel_ontology = ontologies[0][ontologies[0]['ontology.group'] == ontology_id]

        tfs_by_term = {goid + "_" + g for goid, grp in sel_ontology.groupby('id') for g in set(grp['gene.name'])}

        res['tf1.found'] = (res['go.id'] + "_" + res['tf1']).isin(tfs_by_term)
        res['tf2.found'] = (res['go.id'] + "_" + res['tf2']).isin(tfs_by_term)

        if ontology_id in {'DISEASES', 'HPO'} and False:
            obo = OntologiesAnalyzer.get_obos_by_ontology(ontology_id)[ontology_id]
            father_by_goid = {k: obo[k][-2] for k in obo}
            grandfather_by_goid = {k: (obo[obo[k][-2]][-2] if obo[k][-2] is not None else None)
                                   for k in obo}

            for tf in ['tf1', 'tf2']:
                res[tf + '.found.father'] = [(r['go.id'] in father_by_goid) and
                                             (father_by_goid[r['go.id']] is not None) and
                                             (father_by_goid[r['go.id']] + "_" + r[tf] in tfs_by_term)
                                             for ri, r in res.iterrows()]
                res[tf + '.found.grandfather'] = [(r['go.id'] in grandfather_by_goid) and
                                                  (grandfather_by_goid[r['go.id']] is not None) and
                                                  (grandfather_by_goid[r['go.id']] + "_" + r[tf] in tfs_by_term)
                                                  for ri, r in res.iterrows()]

        res['tf1.or.tf2'] = res['tf1.found'] | res['tf2.found']

        res['tf1.and.tf2'] = res['tf1.found'] & res['tf2.found']

        if ontology_id in {'DISEASES', 'HPO'} and False:
            res['tf1.and.tf2.father'] = (res['tf1.found'] | res['tf1.found.father']) & \
                                        (res['tf2.found'] | res['tf2.found.father'])
            #  | res['tf2.found.grandfather'])
            res['tf1.and.tf2.grandfather'] = (res['tf1.found'] | res['tf1.found.father'] | res[
                'tf1.found.grandfather']) & \
                                             (res['tf2.found'] | res['tf2.found.father'] | res['tf2.found.grandfather'])

        res['both'] = res['tf1.and.tf2'] | res['tf1.or.tf2']
        return res


    @staticmethod
    def update_query_and_priority_ids_from_previous_run(query_ids, priority_ids, output_path, thr, group='peaks+kmers'):
        previous_df = None
        if exists(output_path):
            print('load results from previous file...')
            print(abspath(output_path))
            try:
                previous_df = DataFrameAnalyzer.read_tsv_gz(output_path)
                print('previously saved dataframe contains %i entries' % previous_df.shape[0])
            except IOError:
                remove(output_path)
                previous_df = None
        if previous_df is not None:
            unique_df = None
            unique_dict = None
            if group == 'peaks':
                unique_dict = previous_df['id'].value_counts().to_dict()
                unique_df = previous_df.drop_duplicates('id')
                # remove singletons (do again)
            else: # tss or peaks+kmers
                unique_df = previous_df[(previous_df['thr'] == thr) & (previous_df['group'] == group)]
                unique_dict = unique_df['id'].value_counts().to_dict()

            unique_df = unique_df[unique_df['id'].map(unique_dict) == 2]

            if group == 'peaks':
                previous_df = previous_df[ previous_df['id'].map(unique_dict) == 2]

            query_ids = query_ids - set(unique_df['id'])
            priority_ids = priority_ids - set(unique_df['id'])

        # if we are able to get scores, prioritize also the IDs generated by other approaches
        tf1, tf2 = basename(output_path).split("_")[:2]
        ontology_id = 'DISEASES' if 'DISEASES' in output_path else 'GO' if 'GO' in output_path else 'HPO'

        try:
            scores = GOCombinatorialBinding.get_data_matrix(tf1 + "_" + tf2, ontology_id,
                                                            overwrite=True)
            print(scores.shape)
            missing_scores_goid = set(scores[np.isnan(scores[group + ".peaks"])]['go.id'])
            # print len(priority_ids)
            priority_ids = priority_ids.union(missing_scores_goid)
        except Exception as e:
            print('problem reading matrix..')
            print(e)

        # print len(priority_ids)
        # print priority_ids



        return previous_df, query_ids, priority_ids

    @staticmethod
    def get_tss_vs_motif(motif, motifs_tss_output_dir, overwrite=True, **kwargs):
        # repeat analysis with TSS
        print('scanning motifs vs TSS')
        # in chip seq
        bkp_motif_hits_tss = join(motifs_tss_output_dir, motif + ".tsv.gz")
        if not exists(bkp_motif_hits_tss):
            print('Motif', motif, len(motif))
            print('loading TSS seqs...')
            seqs_tss = FastaAnalyzer.get_fastas(join(
                '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/combinatorial_binding_analysis_invivo/data',
                'all_tss_human_2500kb.fa.gz'), uppercase=True)
            peaks_tss, seqs_tss = GOCombinatorialBinding.get_motif_hits_tss(return_sequences=True,
                                                                            motif=motif, seqs=seqs_tss, **kwargs)
            peaks_tss = peaks_tss[['chr', 'start', 'end', 'k.summit', 'motif.hit', 'motif.score']]
            DataFrameAnalyzer.to_tsv_gz(peaks_tss, bkp_motif_hits_tss)

        peaks_tss = DataFrameAnalyzer.read_tsv_gz(bkp_motif_hits_tss)
        return peaks_tss


    @staticmethod
    def get_peaks_intersection_remap(tf1, tf2, motif, motifs_remap_output_dir, **kwargs):
        # in chip seq
        seqs = None
        bkp_motif_hits = join(motifs_remap_output_dir, ('nomotif' if motif is None else motif) + ".tsv.gz")
        print(bkp_motif_hits)
        if not exists(bkp_motif_hits) or kwargs.get('overwrite', False):
            peaks, seqs = GOCombinatorialBinding.get_intersection_remap_peaks(tf1, tf2, return_sequences=True,
                                                                              motif=motif, seqs=seqs, **kwargs)
            # print peaks.head()
            if peaks.shape[1] > 0:
                peaks = peaks[[0, 9, 10, 11, 'motif.hit', 'motif.score']]
                peaks.columns = ['chr', 'start', 'end', 'k.summit', 'motif.hit', 'motif.score']
            DataFrameAnalyzer.to_tsv_gz(peaks, bkp_motif_hits)

        peaks = DataFrameAnalyzer.read_tsv_gz(bkp_motif_hits)
        return peaks


    @staticmethod
    def get_query_and_priority_ids(tf1, tf2, ontology_id, ontologies, tfs_by_term_and_ontology):
        query_ids = set(ontologies[0][ontologies[0]['ontology.group'] == ontology_id]['id'])

        print(ontology_id, len(query_ids))
        # n_by_id = {k: v for k, v in ontologies[0]['id'].value_counts().iteritems()}
        # genes_by_id = {k: set(grp['gene.name']) for k, grp in ontologies[0].groupby('id')}
        # query_ids = {disease_id for disease_id in query_ids if n_by_id[disease_id] < 1000 and n_by_id[disease_id] > 10}


        # query_ids = set(ontologies[0][ontologies[0]['name'].str.lower().str.contains('lymphoma')]['id'])
        # query_ids = {'DOID:1040'}

        print('preparing TFs by term...')
        tfs_by_term = tfs_by_term_and_ontology[ontology_id]

        query_ids_tf1 = {goid for goid in query_ids if goid + "_" + tf1 in tfs_by_term}
        query_ids_tf2 = {goid for goid in query_ids if goid + "_" + tf2 in tfs_by_term}

        if ontology_id != 'GO':
            obo = OntologiesAnalyzer.get_obos_by_ontology(ontology_id)[ontology_id]
            for leaf_goid in list(query_ids_tf1):
                if not leaf_goid in obo:
                    continue
                parents = OntologiesAnalyzer.get_obo_father_terms(obo, leaf_goid)
                query_ids_tf1 = query_ids_tf1.union(parents)
            for leaf_goid in list(query_ids_tf2):
                if not leaf_goid in obo:
                    continue
                parents = OntologiesAnalyzer.get_obo_father_terms(obo, leaf_goid)
                query_ids_tf2 = query_ids_tf2.union(parents)


        priority_ids = query_ids_tf1.union(query_ids_tf2)

        return set(query_ids), set(priority_ids)

    @staticmethod
    def get_queries_df(overwrite=False, subset_cap_tfs=None):

        queries_df_path = "../../data/queries_unbiased_full_with_motifs.tsv.gz"
        if subset_cap_tfs is not None:
            queries_df_path = "../../data/queries_unbiased_subset_with_motifs.tsv.gz"
        if not exists(queries_df_path) or overwrite:
            queries_bkp = "../../data/queries_unbiased_full.pkl"
            if subset_cap_tfs is not None:
                queries_bkp= "../../data/queries_unbiased_subset.pkl"

            if not exists(queries_bkp) or overwrite or subset_cap_tfs is not None:
                GOCombinatorialBinding.prepare_queries_path(queries_bkp, subset_cap_tfs=subset_cap_tfs)


            pairs = DataFrameAnalyzer.read_pickle(queries_bkp)
            print('# of queries (unique TF pairs)', len(pairs))

            # divide queries by ontologies
            new_pairs = []
            for p in pairs:
                # if p[0]['tf.cap'] == 'ARNTL' and p[1]['tf.cap'] == 'BHLHA15':
                #     print p
                # print list(p[0]), list(p[1])
                for ontology_id in ['DISEASES', 'HPO', 'GO']:
                    for group in ['peaks+kmers', 'peaks', 'tss']:
                        if p[0][2] is None and p[1][2] is None and group == 'tss':
                            continue
                        new_pairs.append(p + [ontology_id] + [group])

            pairs = new_pairs

            # pairs = [p for p in pairs if p[-1] == 'peaks']

            # remove duplicated queries
            queries_df = []
            for q in pairs:
                tf1, tf2, motif, ontology_id, group = str(q[0]['tf.remap']), str(q[1]['tf.remap']), q[2], q[-2], q[-1]
                code = "_".join(
                    [tf1, tf2, motif, ontology_id, group] if group != 'peaks' else [tf1, tf2, ontology_id, group])
                queries_df.append([q, code])
            queries_df = pd.DataFrame(queries_df, columns=['pairs', 'code'])
            queries_df = queries_df.drop_duplicates('code')
            pairs = [p for p in queries_df['pairs']]

            queries_df = []
            for p in pairs:
                t = list(p[0].values) + list(p[1].values) + p[2:]
                if t[-1] != 'peaks':
                    motif = t[-3]
                    motif_skip_n = "".join([nt for nt in motif if nt != 'N'])
                    for thr in range(len(motif_skip_n) - 1, len(motif_skip_n) - 3, -1):
                        queries_df.append(t + [thr])
                else:
                    queries_df.append(t + [np.nan])


            queries_df = pd.DataFrame(queries_df,
                                      columns=[c + ".1" for c in p[0].index] + [c + ".2" for c in p[1].index] + ['motif',
                                                                                                                 'ontology',
                                                                                                                 'model', 'motif.mismatches.thr'])
            queries_df['motif'] = np.where(queries_df['model'] == 'peaks', None, queries_df['motif'])

            DataFrameAnalyzer.to_tsv_gz(queries_df, queries_df_path)
        # add the motif matches and mismatches
        return DataFrameAnalyzer.read_tsv_gz(queries_df_path)

    @staticmethod
    def prepare_queries_path(queries_bkp, subset_cap_tfs=None):
        from lib.SELEX.CAPSELEXAnalyzer import CAPSELEXAnalyzer
        queries = CAPSELEXAnalyzer.get_queries_remap_vs_capselex()

        # queries = queries[queries['name.mismatches'] < 2].reset_index(drop=True)
        # queries = queries[queries['same']]

        cm = CAPSELEXAnalyzer.get_composite_motif_enrichments()

        pairs = []

        if subset_cap_tfs is not None:
            queries = queries[queries['tf.cap'].isin(subset_cap_tfs)].reset_index(drop=True)

        for ri, r1 in queries.iterrows(): # [(queries['tf.cap'] == 'BHLHA15') | (queries['tf.cap'] == 'ARNTL')].iterrows():
        # for ri, r1 in queries.iterrows():
            print(ri, queries.shape[0], len(pairs))
            for rj, r2 in queries.iterrows(): # [(queries['tf.cap'] == 'BHLHA15') | (queries['tf.cap'] == 'ARNTL')].iterrows():
            # for rj, r2 in queries.iterrows():
                if (ri < rj): # (r1['same'] or r2['same']):
                    pairs.append([r1, r2])

        new_pairs = []
        pairs_added= set()
        n_valid = 0

        non_redundant_motifs_by_fam_pair = {}
        for p in pairs:

            fam1 = p[0]['fam.cap'].replace('Forkhead', 'Fox')
            fam2 = p[1]['fam.cap'].replace('Forkhead', 'Fox')
            tf1 = p[0]['tf.remap']
            tf2 = p[1]['tf.remap']

            # if tf1 == 'ETV6' and tf2 == 'FOXO1':
            #     print p
            #     stop()
            if tf1 != None and tf2 != None and tf1 == tf2:
                continue

            n_valid += 1
            # print n_valid
            fam_pair = (fam1 + ":" + fam2) if (fam1 > fam2) else (fam2 + ":" + fam1).upper()
            if not fam_pair in non_redundant_motifs_by_fam_pair:
                sel_motifs = cm[(cm['families'].str.upper() == (fam1 + "_" + fam2).upper()) | (cm['families'].str.upper() == (fam2 + "_" + fam1).upper())]
                if sel_motifs.shape[0] == 0:
                    continue

                # reduce redundancy of motifs
                all = []
                motifs = [m.strip('N') for m in sel_motifs['seed']]
                # query_ids = None # {'HP:0003566'}

                # remove highly redundant motifs, opting for the one with the lowest information weight
                matches = pd.DataFrame(
                    [[m1, m2, SequenceMethods.get_matches(m1, m2, max_score=True)] for m1 in motifs for m2 in motifs if
                     len(m1) <= len(m2) and m1 != m2],
                    columns=['k1', 'k2', 'matches'])
                matches['matches'] = [
                    list(r['matches']['n.matches'])[0] if isinstance(r['matches'], pd.DataFrame) else r['matches'] for ri, r
                    in matches.iterrows()]
                matches['redundant'] = (matches['matches'] >= matches['k1'].str.len()) | (
                matches['matches'] >= matches['k2'].str.len())
                non_redundant_motifs = set(matches['k1']).union(set(matches['k2']))
                print(list(p[0]) + list(p[1]) + [len(non_redundant_motifs)])
                from lib.SELEX.SELEXAnalyzer import SELEXAnalyzer
                selex = SELEXAnalyzer()
                matches = matches.sort_values('redundant', ascending=True)
                for ri, r in matches.iterrows():
                    a, b = r['k1'], r['k2']
                    if r['redundant']:
                        w1, w2 = selex.get_weight_sequence(a), selex.get_weight_sequence(b)
                        if len(a) == len(b) and w1 <= w2:
                            if b in non_redundant_motifs:
                                non_redundant_motifs.remove(b)
                            non_redundant_motifs.add(a)
                        elif len(a) == len(b) and w1 >= w2:
                            if a in non_redundant_motifs:
                                non_redundant_motifs.remove(a)
                            non_redundant_motifs.add(b)
                    else:
                        non_redundant_motifs.add(a)
                        non_redundant_motifs.add(b)
                if len(non_redundant_motifs) == 0 and len(motifs) == 1:
                    non_redundant_motifs = motifs

                non_redundant_motifs_by_fam_pair[fam_pair] = non_redundant_motifs

            non_redundant_motifs = non_redundant_motifs_by_fam_pair[fam_pair]
            code = None
            if tf1 is None and tf2 is None:
                tf1_cap, tf2_cap = p[0]['tf.cap'], p[1]['tf.cap']
                code = tf1_cap + "_" + tf2_cap if tf1_cap > tf2_cap else tf2_cap + "_" + tf1_cap
            elif tf1 is not None and tf2 is not None:
                code = tf1 + "_" + tf2 if tf1 > tf2 else tf2 + "_" + tf1
            else:
                continue
            if not code in pairs_added:
                for motif in non_redundant_motifs:
                    new_pairs.append(p + [motif])
                pairs_added.add(code)

        final_pairs = new_pairs
        DataFrameAnalyzer.to_pickle(final_pairs, queries_bkp)


    @staticmethod
    def get_codes():
        basedir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/combinatorial_binding_analysis_invivo/data'
        output_dir_enrichment = join(data_dir, 'GREAT_results_enrichments_local')
        if not exists(output_dir_enrichment):
            mkdir(output_dir_enrichment)
        output_dir_peaks = join(data_dir, 'GREAT_results_peak_gene_associations')
        queries_by_code = {f.replace(".tsv.gz", ""): join(basedir, 'GREAT_peaks_input', f) for f in listdir(join(basedir, 'GREAT_peaks_input'))}
        # associate the motifs when necessary, from
        motifs_dir = join(data_dir, 'tss_vs_capselex/5000/motifs')
        motifs_by_code = {f.replace(".tsv.gz", ""): join(motifs_dir, f) for f in listdir(motifs_dir) if
                          not 'kmers' in f}
        query_codes = set(motifs_by_code.keys()).union(set(queries_by_code.keys()))
        print(len(query_codes))
        return query_codes

    @staticmethod
    def get_motifs_tss(tf1_tf2_motif, distance_bp=5000):
        output_dir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/combinatorial_binding_analysis_invivo/data/tss_vs_capselex'
        output_dir = join(output_dir, str(distance_bp), 'motifs')
        if not exists(output_dir):
            makedirs(output_dir)
        motifs_path = join(output_dir, tf1_tf2_motif + ".tsv.gz")
        if not exists(motifs_path):
            return None
        df = DataFrameAnalyzer.read_tsv_gz(motifs_path)

        df = SequenceMethods.parse_coordinate2range(df, df['description'], ['chr', 'start', 'end'])
        df = df[['description', 'chr', 'start', 'end', 'query', 'seq.hit', 'score']]
        df.columns = ['k.summit'] + list(df.columns[1:])
        df = df[df['score'] == max(df['score'])]
        df['motif.selected'] = True
        df['tf'] = None

        df['source'] = 'tss'
        return df

    @staticmethod
    def get_peaks(tf1_tf2_motif):
        basedir = '../../data/GREAT_peaks_input'
        p = join(basedir, tf1_tf2_motif + ".tsv.gz")
        print(exists(p), p)
        res = pd.DataFrame(columns=['chr', 'start', 'end', 'k.summit', 'intersected', 'tf'])
        if p is not None and exists(p):
            res = DataFrameAnalyzer.read_tsv_gz(p)
            res = SequenceMethods.parse_coordinate2range(res, res['k.summit'])

        res['source'] = 'remap'
        return res

    @staticmethod
    def get_peak_gene_associations(tf):
        peaks = DataFrameAnalyzer.read_tsv_gz('../../data/GREAT_results_peak_gene_associations/%s.tsv.gz' % tf)
        peaks = SequenceMethods.parse_range2coordinate(peaks, ['seqnames', 'start', 'end'], 'k')
        return peaks

    @staticmethod
    def get_great_enrichments(k):
        return DataFrameAnalyzer.read_tsv_gz('../../data/GREAT_results_enrichments_local/' + k + ".tsv.gz")


    @staticmethod
    def get_gene_by_input_region(peaks):
        reg_doms = DataFrameAnalyzer.read_tsv('/g/scb2/zaugg/rio/data/greatTools/data/regDoms.out', header=None)
        reg_doms = SequenceMethods.parse_range2coordinate(reg_doms, [0, 1, 2], 'regdom')
        # sort and count the amount of nucleotides in all considered reg_domains
        reg_doms = reg_doms.sort_values([reg_doms.columns[0], reg_doms.columns[1]], ascending=[True, True])
        a, b, n, inter_path = PeaksAnalyzer.intersect_coordinates(reg_doms,
                                                                  peaks[peaks.columns[:3]], additional_args=['-wo'])
        inters = DataFrameAnalyzer.read_tsv(inter_path, header=None)
        inters = SequenceMethods.parse_range2coordinate(inters, [7, 8, 9], 'remap')
        inters = SequenceMethods.parse_range2coordinate(inters, [0, 1, 2], 'regdom')
        inters = inters[['remap', 3]]
        inters.columns = ['k', 'gene.name']
        inters['k2'] = inters['k'] + inters['gene.name']
        inters = inters.drop_duplicates('k2').reset_index(drop=True)
        del inters['k2']
        return inters

    @staticmethod
    def get_peaks_n_motifs(tf1_tf2_motif, get_tss_motifs=True, get_peaks=True,
                           ref_tf=None, intersected=None, **kwargs):

        final_cols = ['chr', 'start', 'end', 'k.summit', 'source', 'motif.selected', 'tf']
        motifs_df, peaks = pd.DataFrame(columns=final_cols), pd.DataFrame(columns=final_cols)


        if get_tss_motifs:
            print('loading TSS motifs...')
            next_motifs = GOCombinatorialBinding.get_motifs_tss(tf1_tf2_motif)
            if next_motifs is not None:
                motifs_df = next_motifs

        if get_peaks:
            print('loading peaks...')
            peaks = GOCombinatorialBinding.get_peaks(tf1_tf2_motif)
            print(peaks.shape[0])
            tf1, tf2, motif = tf1_tf2_motif.split("_")
            if ref_tf is not None:
                peaks = peaks[peaks['tf'] == ref_tf]

            if intersected is not None:
                peaks = peaks[peaks['intersected'] == intersected]

        res = pd.concat([peaks, motifs_df[final_cols]]).reset_index(drop=True)
        res = res.drop_duplicates('k.summit')
        return res

    @staticmethod
    def get_all(k, nmax=450000):
        peaks = GOCombinatorialBinding.get_peaks_n_motifs(k)
        if peaks.shape[0] > nmax:
            random.seed(500)
            peaks = peaks.sample(nmax)
        return peaks

    @staticmethod
    def get_non_intersected_peaks(k, nmax=450000):
        peaks = GOCombinatorialBinding.get_peaks_n_motifs(k, intersected=False)
        if peaks.shape[0] > nmax:
            random.seed(500)
            peaks = peaks.sample(nmax)
        return peaks

    @staticmethod
    def get_intersected_peaks_tf(k, tf, nmax=450000, **kwargs):
        tf1, tf2, motif= k.split("_")
        peaks = GOCombinatorialBinding.get_peaks_n_motifs(k, intersected=True, ref_tf=tf, **kwargs)
        peaks['motif.selected'] = peaks['motif.score'] >= len(motif) - 5 if 'motif.score' in peaks else \
            peaks['motif.selected'] if 'motif.selected' in peaks else False  # for FOXO1/ETV6 this thr was moved from 2 to 5
        print(tf, peaks.shape[0])
        if peaks.shape[0] > nmax:
            random.seed(500)
            peaks = peaks.sample(nmax)
        return peaks

    @staticmethod
    def get_motif_enrichment_intersected_peaks(peaks, motif, thr, seqs=None):
        motif_hits = GOCombinatorialBinding.get_motif_score_from_coordinates(peaks, motif, genome='hg19', seqs=seqs)
        a, b, c, d = motif_hits[(motif_hits['motif.score'] >= thr) & (motif_hits['intersected'])].shape[0], \
                     motif_hits[(motif_hits['motif.score'] < thr) & (motif_hits['intersected'])].shape[0], \
                     motif_hits[(motif_hits['motif.score'] >= thr) & (~motif_hits['intersected'])].shape[0], \
                     motif_hits[(motif_hits['motif.score'] < thr) & (~motif_hits['intersected'])].shape[0]
        return [motif, a, b, c, d] + list(fisher_exact([[a, b], [c, d]], alternative='greater'))

    @staticmethod
    def get_motif_score_from_coordinates(peaks, motif, genome='hg19', seqs=None):
        from lib.FastaAnalyzer import FastaAnalyzer
        if seqs is None:
            seqs = zip(*FastaAnalyzer.get_sequences_from_bed(peaks, uppercase=True, genome=genome))[1]
        assert len(seqs) == peaks.shape[0]
        from lib.Motif.MotifAnalyzer import MotifAnalyzer
        motif_hits = MotifAnalyzer.find_pssm_hits_from_sequences(MotifAnalyzer.get_pssm_from_sequence(motif), seqs)
        peaks['motif.hit'] = motif_hits['seq.hit']
        peaks['motif.score'] = motif_hits['score']
        return peaks

    @staticmethod
    def get_motif_hits_tss(motif=None, genome='hg19', return_sequences=True, seqs=None, **kwargs):
        df = pd.DataFrame()
        df['k.summit'] = zip(*seqs)[0]
        df = SequenceMethods.parse_coordinate2range(df, df['k.summit'])

        if motif is not None:
            from lib.FastaAnalyzer import FastaAnalyzer
            if seqs is None:
                seqs = zip(*FastaAnalyzer.get_sequences_from_bed(df[[0, 3, 4, 5]], uppercase=True))[1]
            if not isinstance(motif, set):
                from lib.Motif.MotifAnalyzer import MotifAnalyzer

                pssm = MotifAnalyzer.get_pssm_from_sequence(motif)
                for nti, nt in enumerate(motif):
                    if nt == 'N' and kwargs.get('skip_motif_N', False):
                        pssm[nti] = 0
                motif_hits = MotifAnalyzer.find_pssm_hits_from_sequences(pssm, seqs)
                df['motif.hit'] = motif_hits['seq.hit']
                df['motif.score'] = motif_hits['score']
            else:
                print('scanning sequences using kmers-bag...')
                found = []
                for si, s in enumerate(seqs):
                    if si % 100 == 0:
                        print(si, len(seqs))
                    fwd, rev = s, SequenceMethods.get_complementary_seq(s)
                    accept = False
                    for k in motif:
                        if k in fwd or k in rev:
                            accept = True
                        if accept:
                            break
                    found.append(accept)
                df['motif.score'] = found

        if return_sequences:
            return df, seqs
        return df


    @staticmethod
    def get_intersection_remap_peaks(tf1, tf2, motif=None, genome='hg19', return_sequences=True, seqs=None,
                                     **kwargs):

        # import ipdb; ipdb.set_trace()
        try:
            if genome == 'hg19' and kwargs.get('peaks1') is None:
                a = REMAPAnalyzer.get_remap_peaks(tf1)
                b = REMAPAnalyzer.get_remap_peaks(tf2)
            else:
                a = kwargs.get('peaks1')
                b = kwargs.get('peaks2')

            cols = ['chr', 'start', 'end', 'summit.start', 'summit.end', 'k.summit']
            bed_a = BedTool.from_dataframe(a[[c for c in cols if c in a]])
            bed_b = BedTool.from_dataframe(b[[c for c in cols if c in b]])

            # print bed_a.shape
            # print bed_b.shape
            print('run bedtools intersect...')
            intersect_bed = bed_a.intersect(bed_b, wo=True)
            if len(intersect_bed) == 0:
                print('there are no common peaks between elements. Discard...')
                empty_df = pd.DataFrame(columns=['chr', 'start', 'end', 'summit.start', 'summit.end', 'k.summit'])
                if motif is None:
                    return empty_df
                else:
                    return empty_df, None

            a_and_b = intersect_bed.to_dataframe(disable_auto_names=True, header=None)
            before_chrM_removal = a_and_b.shape[0]
            a_and_b = a_and_b[a_and_b[0] != 'chrM']
            print('total size intersection (before and after chrM removal): %i, %i' % (before_chrM_removal, a_and_b.shape[0]))
            # pybedtools.cleanup(remove_all=True)

            if motif is not None:
                from lib.FastaAnalyzer import FastaAnalyzer
                if seqs is None:
                    seqs = zip(*FastaAnalyzer.get_sequences_from_bed(a_and_b[['chr', 'start', 'end', 'k.summit']],
                                                                     uppercase=True))[1]
                assert len(seqs) == a_and_b.shape[0]

                if not isinstance(motif, set):
                    from lib.Motif.MotifAnalyzer import MotifAnalyzer
                    pssm = MotifAnalyzer.get_pssm_from_sequence(motif)
                    for nti, nt in enumerate(motif):
                        if nt == 'N' and kwargs.get('skip_motif_N', False):
                            pssm[nti] = 0
                    print('calling motif mapping step...')
                    motif_hits = MotifAnalyzer.find_pssm_hits_from_sequences(pssm, seqs, **kwargs)
                    a_and_b['motif.hit'] = motif_hits['seq.hit']
                    a_and_b['motif.score'] = motif_hits['score']
                else:
                    print('scanning sequences using kmers-bag...')
                    found = []
                    for si, s in enumerate(seqs):
                        if si % 100 == 0:
                            print(si, len(seqs))
                        fwd, rev = s, SequenceMethods.get_complementary_seq(s)
                        accept = False
                        for k in motif:
                            if k in fwd or k in rev:
                                accept = True
                            if accept:
                                break
                        found.append(accept)
                    a_and_b['motif.score'] = found
            else:
                if kwargs.get('use_union', True):
                    print('using the full union of A/B peaks')
                    df_union = pd.concat([bed_a.to_dataframe(), bed_b.to_dataframe()])
                    col_summit = 'strand' if 'strand' in df_union else 'name'
                    df_union = df_union.rename(columns={'chrom': 'chr'})
                    df_union['intersected'] = df_union[col_summit].isin(a_and_b[5 if genome == 'hg19' else 3])
                    df_union = df_union.sort_values(['chr', 'start'], ascending=[True, True])
                    df_union = df_union[~df_union[col_summit].isin(a_and_b[11 if genome == 'hg19' else 7])]
                    df_union['k.summit'] = df_union[col_summit]
                    del df_union[col_summit]
                    return df_union
                else:
                    print('returning the TF1 peaks conditioned by TF2 1 or 0...')
                    a = bed_a.to_dataframe()
                    col_summit = 'strand' if 'strand' in a else 'name'
                    a = a.rename(columns={'chrom': 'chr'})
                    a['intersected'] = a[col_summit].isin(a_and_b[5 if genome == 'hg19' else 3])
                    a['k.summit'] = a[col_summit]
                    del a[col_summit]
                    return a

            if return_sequences:
                return a_and_b, seqs
            return a_and_b
        except Exception as e:
            print('intersection or motif mapping did not work. Please check...')
            print('Error')
            print(e)
            return pd.DataFrame(), seqs


    @staticmethod
    def write_genes(genes_series, genes_path='genes.txt', replace_dict=None):
        # print 'before ranking...'
        # print genes_series.head(50)
        genes_series = genes_series.rank(method='dense').astype(int)

        # print 'after ranking...'
        # print genes_series.head(50)

        if replace_dict is not None:
            for k in replace_dict:
                genes_series = genes_series.replace(k, replace_dict[k])

        # print 'after correction...'
        # print genes_series.head(50)

        # print genes_series[genes_series.columns[0]].value_counts()
        DataFrameAnalyzer.to_tsv(genes_series, genes_path, header=False, log=False)

    @staticmethod
    def get_genes_and_peaks_dataframe(all_remap_ids, gene_by_peak, genes_term):
        peaks = [p for p in gene_by_peak for g in gene_by_peak[p]]
        genes = [g for p in gene_by_peak for g in gene_by_peak[p]]
        genes = [g if g in genes_term else -1 for g in genes]

        peaks = pd.DataFrame(peaks, columns=['peaks']).rank(method='dense').astype(int)
        genes = pd.DataFrame(genes, columns=['genes']).rank(method='dense').astype(int)
        genes = genes.replace(1, -1)

        return pd.concat([peaks, genes], axis=1)


    @staticmethod
    def write_crountine_input(all_remap_ids, gene_by_peak, genes_term):
        # print 'writing Crountine input...'
        peaks = [p for p in gene_by_peak for g in gene_by_peak[p]]
        genes = [g for p in gene_by_peak for g in gene_by_peak[p]]
        genes = [g if g in genes_term else -1 for g in genes]
        # print len(peaks), len(set(peaks))
        # print len(genes), len(set(genes))
        d = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/combinatorial_binding_analysis_invivo/src/peaks_downsampling_intersection_c'
        GOCombinatorialBinding.write_genes(pd.DataFrame(genes, columns=['genes']), join(d, 'genes.txt'), replace_dict={1: -1})
        GOCombinatorialBinding.write_genes(pd.DataFrame(peaks, columns=['peaks']), join(d, 'peaks.txt'))



    @staticmethod
    def get_results_from_directory(basedir_or_paths_list, ontology_id=None, stopat=None, ncores=10, k_endswith=None,
                                   ontologies=None, k_thr=None, query_tf_pairs=None):
        if ontology_id is None:
            print('please select and ontology database (DISEASES/GO/HPO)...')
            assert 1 > 2
        res = []
        paths = None
        if isinstance(basedir_or_paths_list, list):
            paths = basedir_or_paths_list
        else:
            paths = [join(basedir_or_paths_list, f) for f in listdir(basedir_or_paths_list)]
        if k_endswith is not None:
            paths = [p for p in paths if p.endswith(k_endswith)]
        if query_tf_pairs is not None:
            # print len(paths)
            paths = [p for p in paths if "_".join(basename(p).replace(".tsv.gz", '').split("_")[:2]) in query_tf_pairs]
            # print len(paths)

        print('# paths %i' % len(paths))
        from multiprocessing import Manager
        manager = multiprocessing.Manager()
        return_dict = manager.dict()

        def get_res(p, pi):
            f = basename(p)
            df = DataFrameAnalyzer.read_tsv_gz(p)
            # df = df[(df['binom.pval'] < 0.np.isnan(05) & (df['z.score.genes'] > 3))]

            if k_thr is not None:
                if np.isnan(k_thr):
                    df = df[np.isnan(df['thr'])]
                else:
                    df = df[df['thr'] == k_thr]

            df = df[~np.isinf(df['z.score.genes'])]
            print(f, df.shape[0])
            df['filename'] = f
            df['minus.log10.pval'] = -np.log10(df['binom.pval'])
            df['x'] = df['minus.log10.pval']
            df['y'] = df['z.score.genes']

            df = df[~np.isnan(df['x'])]
            df = df[~np.isnan(df['y'])]
            df = df[~np.isnan(df['z.score.peaks'])]

            # df['z'] = (df['x'] ** 2) + (df['y'] ** 2)
            df = df.sort_values('z.score.genes', ascending=False).drop_duplicates('id')
            # df['z'] = np.where(df['y'] < 0, 0, df['z'])
            # df['rank'] = df['z'].rank(ascending=False)
            next = df[['id', 'name', 'binom.pval',
                       'z.score.genes', 'z.score.peaks']]

            next.is_copy = False
            next['tf.pair'] = f.replace(".tsv.gz", '')
            return_dict[pi] = next

        N = len(paths) if stopat is None else stopat
        output_list = [None for i in range(len(paths[:N]))]

        print('N:', stopat)
        ThreadingUtils.run(get_res, [[paths[pi], pi] for pi in range(len(paths[:N]))], ncores)
        res = pd.concat([return_dict[k] for k in list(return_dict.keys())])
        print('# of rows after concatenation', res.shape[0])

        res['tf1'] = res['tf.pair'].str.split("_").str[0]
        res['tf2'] = res['tf.pair'].str.split("_").str[1]

        sel_ontology = None
        if ontologies is None:
            print('reading ontologies')
            ontologies = OntologiesAnalyzer.get_gos()
            sel_ontology = ontologies[0][ontologies[0]['ontology.group'] == ontology_id]
        else:
            sel_ontology = ontologies

        tfs_by_term = {goid + "_" + g for goid, grp in sel_ontology.groupby('id') for g in set(grp['gene.name'])}
        res['tf1.found'] = (res['id'] + "_" + res['tf1']).isin(tfs_by_term)
        res['tf2.found'] = (res['id'] + "_" + res['tf2']).isin(tfs_by_term)
        res['tf1.or.tf2'] = res['tf1.found'] | res['tf2.found']
        res['tf1.and.tf2'] = res['tf1.found'] & res['tf2.found']

        return res

    @staticmethod
    def get_paths_positives(ontology_id, association_type='double'):
        basedir = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding',
                 'combinatorial_binding_analysis_invivo/data/GREAT_zscores_downsampling_v2019/%s' % ontology_id)

        print('reading ontologies')
        ontologies = OntologiesAnalyzer.get_gos()
        sel_ontology = ontologies[0][ontologies[0]['ontology.group'] == ontology_id]

        for f in listdir(basedir):
            tf1, tf2 = f.split("_")
            sela = set(sel_ontology[sel_ontology['gene.name'] == tf1]['id'])
            selb = set(sel_ontology[sel_ontology['gene.name'] == tf2]['id'])
            ncommon = sela.intersection(selb)
            print(tf1, tf2, len(sela), len(selb), len(ncommon))

    @staticmethod
    def get_performances(res, feats_groups, label_positives='tf1.and.tf2', method=None):
        if method is None:
            print('give a method as kwargs (e.g. MachineLearning.KFoldSVC)')
        all = []
        for feats in feats_groups:
            print('next feats: %s', feats, "/".join(feats))
            auroc_res = method(np.array(res[feats]), np.array(res[label_positives]))
            auroc_res['feats'] = "/".join(feats)
            all.append(auroc_res)
        return pd.concat(all).reset_index(drop=True)

    @staticmethod
    def run_croutine_permutation_downsampling(ngenes_hits, npeaks_hits, npeaks_downsample, nperm):
        bin = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/combinatorial_binding_analysis_invivo/src/peaks_downsampling_intersection_c/intersection'
        cmd = ' '.join(map(str, [bin, ngenes_hits, npeaks_hits, npeaks_downsample, nperm]))
        print(cmd)
        system(cmd)

    @staticmethod
    def get_scores_by_tf_pair(tf_pair, ontology_id):
        basedir = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding',
                       'combinatorial_binding_analysis_invivo/data/GREAT_zscores_downsampling_v2019/%s' % ontology_id, tf_pair)
        print(exists(basedir), basedir)
        all = []

        all_df = []
        for f in listdir(basedir):
            if not 'peaks.tsv.gz' in f:
                tf1, tf2, motif, ontology_id, group = f.replace(".tsv.gz", '').split("_")
            else:
                tf1, tf2, ontology_id, group = f.replace(".tsv.gz", '').split("_")
                motif = None
            p = join(basedir, f)
            # print f, tf1, tf2, motif, ontology_id, group
            df = DataFrameAnalyzer.read_tsv_gz(p)
            df = df[df['z.calc.approach'] == 'z.transform']
            X = pd.DataFrame()
            X['z.score.genes'] = df['z.score.genes']
            X['z.score.peaks'] = df['z.score.peaks']
            X['tf1'] = tf1
            X['tf2'] = tf2
            X['motif'] = motif
            X['motif.thr'] = df['thr']
            X['ontology.group'] = ontology_id
            X['go.id'] = df['id']
            X['group'] = group
            X['genes.term'] = df['n.genes.term']
            X['genes.hits'] = df['n.genes.hits']
            X['peaks.term'] = df['n.peaks.total']
            X['peaks.hits'] = df['n.peaks.hits']
            all.append(X)
            all_df.append(df)
        all = pd.concat(all).reset_index(drop=True)
        all_df = pd.concat(all_df).reset_index(drop=True)
        return all

    @staticmethod
    def get_best_motif_by_goid(tf_pair, ontology_id, overwrite=False, skipinf=True, skipnan=True):
        output_dir = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding',
                          'combinatorial_binding_analysis_invivo/data/GREAT_zscores_downsampling_v2019/input_ml/%s' % ontology_id)
        if not exists(output_dir):
            mkdir(output_dir)
        output_path = join(output_dir, tf_pair + ".tsv.gz")

        X = GOCombinatorialBinding.get_scores_by_tf_pair(tf_pair, ontology_id)
        X['full.id'] = X['go.id'] + "_" + X['group']

        if skipinf:
            X = X[~np.isinf(X['z.score.genes']) & ~np.isinf(X['z.score.peaks'])]
        if skipnan:
            X = X[~np.isnan(X['z.score.genes']) & ~np.isnan(X['z.score.peaks'])]
        X = X.sort_values('z.score.genes', ascending=False).drop_duplicates('full.id')
        return DataFrameAnalyzer.get_dict(X[(X['group'] == 'peaks+kmers')], 'go.id', 'motif')

    @staticmethod
    def get_data_matrix(tf_pair, ontology_id, overwrite=False, skipinf=True, skipnan=True, **kwargs):
        output_dir = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding',
                          'combinatorial_binding_analysis_invivo/data/GREAT_zscores_downsampling_v2019/input_ml/%s' % ontology_id)
        if not exists(output_dir):
            mkdir(output_dir)
        output_path = join(output_dir, tf_pair + ".tsv.gz")

        if overwrite or not exists(output_path):
            X = GOCombinatorialBinding.get_scores_by_tf_pair(tf_pair, ontology_id)
            X['full.id'] = X['go.id'] + "_" + X['group']

            if skipinf:
                X = X[~np.isinf(X['z.score.genes']) & ~np.isinf(X['z.score.peaks'])]
            if skipnan:
                X = X[~np.isnan(X['z.score.genes']) & ~np.isnan(X['z.score.peaks'])]
            X = X.sort_values('z.score.genes', ascending=False).drop_duplicates('full.id')

            # print X.shape
            print(X.head())

            m1 = X.pivot('go.id', 'group', 'z.score.genes') # kwargs.get('column_genes', 'z.score.genes'))
            m2 = X.pivot('go.id', 'group', 'z.score.peaks') # kwargs.get('column_peaks', 'z.score.peaks'))

            print(m1.head())
            print(m2.head())

            m1.columns = [c + ".genes" for c in m1.columns]
            m2.columns = [c + ".peaks" for c in m2.columns]

            assert m1.index.equals(m2.index)
            X = pd.concat([m1, m2], axis=1)
            X['tf.pair'] = tf_pair
            X['ontology'] = ontology_id
            X['go.id'] = X.index

            # if trying to N for genes and peaks, do not save the matrix
            # kwargs.get('column_genes') print kwargs.get('column_peaks')
            DataFrameAnalyzer.to_tsv_gz(X, output_path)

        return DataFrameAnalyzer.read_tsv_gz(output_path)

    @staticmethod
    def define_fdr_threshold(ontology_id, feats=['peaks+kmers.genes', 'peaks+kmers.peaks', 'peaks.genes', 'peaks.peaks']):
        from sklearn.multiclass import OneVsRestClassifier
        import numpy as np
        from sklearn import svm

        res = GOCombinatorialBinding.get_spscore_results(ontology_id)
        pos = res[res['tf1.and.tf2']][feats + ['tf1.and.tf2']]
        neg = res[(~res['tf1.and.tf2'] & ~res['tf1.and.tf2'])][feats + ['tf1.and.tf2']].sample(pos.shape[0] * 2)

        X = pd.concat([pos[feats], neg[feats]])
        y = pd.concat([pos['tf1.and.tf2'], neg['tf1.and.tf2']])
        X = np.array(X)
        y = np.array(y)

        svm_model = svm.SVC(random_state=500, probability=True)

        print('fitting OneVsRestClassifier')
        classifier = OneVsRestClassifier(svm_model)
        clf = classifier.fit(X, y)
        clf.decision_function(X)

        # scatter_simple(list(y), list(clf.decision_function(X)), '/tmp/test.png')

        from lib.MachineLearning import MachineLearning
        svm_model = svm.SVC(random_state=500, probability=True)
        clf = classifier.fit(X, y)
        pos_probability = clf.predict_proba(X[y == 1])

        nperm = 10
        perm_probabilities = []
        for ni in range(nperm):
            print(ni)
            random.shuffle(y)
            classifier = OneVsRestClassifier(svm_model)
            svm_model = svm.SVC(random_state=500, probability=True)
            clf = classifier.fit(X, y)
            probabiliy = clf.predict_proba(X[y == 1])
            print(probabiliy)
            perm_probabilities.append(probabiliy)

        for i in range(len(pos_probability[0])):
            print(i)
            pos = [p[i] for p in pos_probability]
            neg = [p[i] for perm in perm_probabilities for p in perm]
            print(average(pos), average(neg))

            pos = sorted(pos, reverse=True)
            neg = sorted(neg, reverse=True)

            tp = 0
            fp = 0
            x = []
            fdr_vector = []

            fp_scores = list(neg)
            fpi = 0
            n_total_pos = X[y == 1].shape[0]
            for ri, pos_score in enumerate(pos):
                tp += 1

                while fpi < len(fp_scores) and pos_score < fp_scores[fpi]:
                    fpi += 1
                    fp += 1
                    if fpi == len(fp_scores):
                        break

                next_fp = fp / float((len(fp_scores) / n_total_pos))
                next_fdr = (next_fp) / (next_fp + float(tp))
                # if tp > 500 and  next_fdr > 0.15:
                #     break
                print(pos_score, tp, fp, next_fdr)
                # print r[col_svm_score], tp, fp, next_fdr
                x.append(pos_score)
                fdr_vector.append(next_fdr)

            # scatter_simple(x, fdr_vector, output_path='/tmp/fdr_%i.png' % i)
