'''
Created on 3/10/2019

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.MyGeneAnalyzer import MyGeneAnalyzer

class STRING:

    @staticmethod
    def get_human_interactions():
        df = DataFrameAnalyzer.read_tsv_gz('/g/scb2/zaugg/rio/data/STRING/9606.protein.links.v11.0.txt.gz', sep=' ')
        df['ensp.1'] = df['protein1'].str.split(".").str[1]
        df['ensp.2'] = df['protein2'].str.split(".").str[1]

        ensg_by_ensp = MyGeneAnalyzer.get_ensemblgene_by_ensemblprotein(
            list(set(df['ensp.1']).union(set(df['ensp.2']))))

        symbol_by_ensg = MyGeneAnalyzer.get_symbol_by_ensembl(list(ensg_by_ensp.values()))
        df['ensg.1'] = df['ensp.1'].map(ensg_by_ensp)
        df['ensg.2'] = df['ensp.2'].map(ensg_by_ensp)
        df['symbol.1'] = df['ensg.1'].map(symbol_by_ensg)
        df['symbol.2'] = df['ensg.2'].map(symbol_by_ensg)
        return df
