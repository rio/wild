
import tempfile
from os import system, listdir, mkdir
from os.path import exists, join
from lib.FastaAnalyzer import FastaAnalyzer
import pickle
import gzip

class GenomeTools:
    def get_sequences_from_hg19(self, coordinates_dataframe):
        # print coordinates_dataframe
        """
        It retrieves sequences using HG19 as a reference, and the current motif width

        If motif width is None, it is assumed that the coordinates are given as
        input at positions 1 and 2
        """
        # write HOMER motifs to tmp BED file. This one will be used later for validation
        output_bed = tempfile.mkstemp()[1]
        writer = open(output_bed, "w")
        for i, r in coordinates_dataframe.iterrows():
            chromosome, start, end = [r[k] for k in ['chromosome', 'start', 'end']]
            writer.write("\t".join(map(str, [chromosome, start, end])) + "\n")
        writer.close()

        # intersect with hg19 fasta sequence
        # print "retrieving sequences from hg19..."
        hg19_fasta = "/home/ignacio/Dropbox/hg19/hg19.fa"
        if not exists(hg19_fasta):
            hg19_fasta = "/home/rio/zaugglab/rio/data/hg19/hg19.fa"
        if not exists(hg19_fasta):
            hg19_fasta = '/home/rio/Dropbox/hg19/hg19.fa'

        output_fasta = tempfile.mkstemp()[1]
        self.get_sequences_from_bed(output_bed, hg19_fasta, output_fasta)

        # print output_fasta
        # print exists(output_fasta)
        # for r in open(output_fasta):
        #     print r
        sequences = self.get_fastas_from_file(output_fasta)

        # print output_fasta
        # print sequences
        sequences = [[r[0], r[1].upper()] for r in sequences]
        assert coordinates_dataframe.shape[0] == len(sequences)
        coordinates_dataframe['coordinate'] = [t[0] for t in sequences]
        coordinates_dataframe['seq'] = [t[1] for t in sequences]

        return coordinates_dataframe

    def create_bed_file(self, coordinates_table, bed_output_path):
        """
        CHROMOSOME_ID    START    END
        """
        # write HOMER motifs to tmp BED file. This one will be used later for validation
        is_gzip = bed_output_path.endswith(".gz")
        writer = gzip.open(bed_output_path, "w") if is_gzip else open(bed_output_path, "w")
        for r in coordinates_table:
            writer.write("\t".join(map(str, [r[0], int(r[1]), int(r[2])])) + "\n")
        writer.close()

    def get_sequences_from_personal_genome(self, coordinates_table,
                                           personal_genome_path, as_dict=False,
                                           output_fasta=None,
                                           overwrite=True,
                                           tmp_dir="../tmp",
                                           return_sequences=True):
        """
        It retrieves sequences using a personal genome as
        """

        if output_fasta is not None and exists(output_fasta):
            return
        if output_fasta is not None and exists(output_fasta.replace(".fa", ".gz")):
            return

        # =======================================================================
        # # write HOMER motifs to tmp BED file. This one will be used later for validation
        # =======================================================================
        output_bed = tempfile.mkstemp()[1]
        self.create_bed_file(coordinates_table, output_bed)

        # intersect with hg19 fasta sequence
        if output_fasta == None:
            output_fasta = tempfile.mkstemp()[1]

        self.get_sequences_from_bed(output_bed, personal_genome_path,
                                    output_fasta)


        if return_sequences:
            sequences = self.get_fastas_from_file(output_fasta, as_dict=False)
            # print sequences
            for r, c in zip(sequences, coordinates_table):
                header = c[0] + ":" + str(c[1]) + "-" + str(c[2])
                assert r[0] == header
            sequences = [[r[0], r[1].upper()] for r in sequences]
            assert len(coordinates_table) == len(sequences)
            sequences_table = [h + s for h, s in zip(coordinates_table, sequences)]
            # print sequences_table
            for h in sequences_table:
                assert str(h[0]) + ":" + str(h[1]) + "-" + str(h[2]) == h[3]
            sequences_table = [h[0:4] + [h[-1]] for h in sequences_table]
            sequences_table = [h[0:4] + [
                self.get_complementary_seq(h[-1]) if h[2] == "-" else h[-1]]
                               for h in sequences_table]
            return sequences_table

    def get_sequences_from_personal_genomes(self, coordinates,
                                            personal_genomes_dir=None,
                                            output_path=None):
        """
        Given a set of coordinates and a personal genomes directory, extract
        all sequences for all invididuals.

        Note: Sequences might be different, and that is the reason we need to
        extract them all in a custom way.

        Output path let us retrieve sequences faster in case we have already
        generated an equivalent file in a previous run.
        """

        if personal_genomes_dir is None:
            personal_genomes_dir = join(
                "/home/ignacio/scb_cluster/zaugg_shared",
                "data/genomes_chromovar3D/chromovar3d.stanford.edu",
                "genomes/personal_genomes_fasta")

        # =======================================================================
        # Load from previously generated file
        # =======================================================================
        output_table = None
        if output_path is not None and exists(output_path):
            output_table = pickle.load(open(output_path, "rb"))
        else:
            # ===================================================================
            # Generate sequences table by reading personal genomes, one by one
            # ===================================================================
            output_table = []
            for f in listdir(personal_genomes_dir):
                if not f.endswith('.fa'):
                    continue
                personal_genome_path = join(personal_genomes_dir, f)
                print(personal_genome_path)
                seqs = self.get_sequences_from_personal_genome(coordinates,
                                                               personal_genome_path)
                output_table += seqs
                print(output_table[0])
                exit()
            if output_path is not None:
                pickle.dump(output_table, open(output_path))
        return output_table


    def get_sequences_from_personal_genomes_remote_dir(self, coordinates_table,
                                                       output_sequences_dir,
                                                       personal_genomes_dir=None,
                                                       remote_wd=None,
                                                       job_basename="MYJOB",
                                                       run_job=True,
                                                       scp_fastas_from_server=False,
                                                       stop_at=None,
                                                       tmp_dir="../tmp"):
        """
        Create a bed file that will be used for extracting sequences from a set
        of personal genomes located in the cluster at

        /home/rio/zaugglab/zaugg_shared/data/genomes_chromovar3D/
        chromovar3d.stanford.edu/genomes/personal_genomes_fasta
        """

        # delete the following lines
        username = "rio"
        bsub = "/shared/ibm/platform_lsf/9.1/linux2.6-glibc2.3-x86_64/bin/bsub"

        if personal_genomes_dir is None:
            personal_genomes_dir = join(
                "/home/ignacio/scb_cluster/zaugg_shared",
                "data/genomes_chromovar3D/chromovar3d.stanford.edu",
                "genomes/personal_genomes_fasta")

        if run_job:
            # create directory in webserver if it does not exists
            if remote_wd is not None:
                if not exists(remote_wd):
                    mkdir(remote_wd)

            # create tmp.bed table
            output_bed = join(tmp_dir, "tmp.bed")
            self.create_bed_file(coordinates_table, output_bed)

            # extract sequences from personal genomes (list gz files)
            genomes_files = [f for f in listdir(personal_genomes_dir) if
                             f.endswith(".fa")]

            # =======================================================================
            # Prepare a bash job that will extract sequences for each of our individuals
            # and store them in our directories
            # =======================================================================
            bash_cmd = ""
            for i, f in enumerate(genomes_files):
                if not f.endswith(".fa"):
                    continue
                basename = ".".join(f.split(".")[:2])
                output_fasta_local = join(output_sequences_dir,
                                          basename + ".fa")
                output_fasta_local_gz = output_fasta_local + ".gz"

                # do not run again if we already have
                if exists(output_fasta_local) or exists(output_fasta_local_gz):
                    continue

                print(basename)

                fasta_to_bed_cmd = " ".join(["fastaFromBed", "-bed",
                                             output_bed, "-fi",
                                             join(personal_genomes_dir, f),
                                             "-fo", output_fasta_local])

                # ===============================================================
                # Run command to extract sequences to local directory
                # ===============================================================
                print(i, fasta_to_bed_cmd)
                system(fasta_to_bed_cmd)
                bash_cmd += fasta_to_bed_cmd + "\n"

        # =======================================================================
        # At this point, the directory output_sequences_dir should have the
        # extracted files
        # =======================================================================


        # once the file it is in our local directory, compress it to save space
        if len([f for f in listdir(output_sequences_dir) if
                f.endswith(".fa")]) > 0:
            system(" ".join(["gzip", join(output_sequences_dir, "*.fa"), ";",
                             "rm", join(output_sequences_dir, "*.fa")]))

        # =======================================================================
        # Load sequences from output files
        # =======================================================================
        sequences_by_individual = {}
        fasta_files = [f for f in listdir(output_sequences_dir)]
        for i, f in enumerate(fasta_files):
            if stop_at is not None and i >= stop_at:
                break
            print("reading sequences # ", i + 1, "/", len(fasta_files), f)
            # genome ID + paternal/maternal
            basename = ".".join(f.split(".")[:2])
            fastas = self.get_fastas_from_file(join(output_sequences_dir, f))
            fastas = {h: s.upper() for h, s in fastas}
            sequences_by_individual[basename] = fastas
        return sequences_by_individual

    def get_sequences_from_bed(self, bed_path, input_fasta, output_fasta):
        """
        Given a BED path and a FASTA path, get FASTAS sequences from it.
        """
        bin = '/g/software/bin/fastaFromBed'
        bin = bin if exists(bin) else 'fastaFromBed'
        args = [bin, "-bed", bed_path, "-fi", input_fasta,
                "-fo", output_fasta]
        print(args)
        print(" ".join(args))
        args = " ".join(args)
        system(args)

    def get_fastas_from_file(self, fasta_path, as_dict=False,
                             uppercase=False, stop_at=None, na_remove=False):

        fasta_analyzer = FastaAnalyzer()
        return fasta_analyzer.get_fastas_from_file(fasta_path, as_dict,
                                                   uppercase, stop_at, na_remove)

    def get_complementary_nt(self, nt):
        if nt == "A":
            return "T"
        if nt == "T":
            return "A"
        if nt == "C":
            return "G"
        if nt == "G":
            return "C"
        if nt == "N":
            return "N"

    def get_complementary_seq(self, s):
        return "".join([self.get_complementary_nt(nt) for nt in s][::-1])

    def convert_bed_hg18_to_hg19(self, input_bed, output_bed, unmapped_path=None):
        '''
        Convert hg18 to hg19 using liftovert
        :param input_bed:
        :param output_bed:
        :param chain_path:
        :param unmapped_path:
        :return:
        '''

        unmapped_path = output_bed + ".unmapped"
        bin = "/g/software/bin/liftOver"
        cmd = " ".join([bin, input_bed, chain_path, output_bed, unmapped_path])
