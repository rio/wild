#!/usr/bin/env bash
p=`cat $1 | grep "SBATCH -e" | sed -e "s/#SBATCH -e  //g" | sed -e "s/%a/$2/g"`
cat $p