#!/usr/bin/env bash
p=`cat $1 | grep "SBATCH -o" | sed -e "s/#SBATCH -o  //g" | sed -e "s/%a/$2/g"`
cat $p