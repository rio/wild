


from lib.utils import *
from lib.SNPAnalyzer import SNPAnalyzer
from sys import exit
import random
from os import listdir
from numpy import mean
from lib.FastQAnalyzer import FastQAnalyzer
from lib.DataFrameAnalyzer import DataFrameAnalyzer
# from lib.MachineLearning import MachineLearning
from lib.ShapeAnalyzer import ShapeAnalyzer
import pandas as pd
import numpy as np
from numpy import median
from lib.path_functions import *
import tempfile
import itertools
import gzip
from lib.SequenceMethods import SequenceMethods
from lib.FastaAnalyzer import FastaAnalyzer
from lib.Motif.MotifConverter import MotifConverter
from lib.DNAshapedTFBS.DNAshapedTFBS import find_pssm_hits
from itertools import islice
from lib.kmers_counter.KMerCounter import KMerCounter

class SELEXAnalyzer():


    def __init__(self):
        self.iupac_table = {"A": {"A"}, "C": {"C"}, "T": {"T"}, "G": {"G"},
                            "R": {"A", 'G'}, "Y": {"C", 'T'},
                            "W": {"G", 'C'}, "S": {"A", 'T'},
                            "K": {"G", 'T'}, "M": {"A", 'C'},
                            "B": {"C", 'G', 'T'},
                            "D": {"A", 'G', 'T'},
                            "H": {"C", 'G', 'T'},
                            "V": {"A", 'C', 'G'},
                            "N": {"C", 'G', 'T', 'A'}}

        self.nuc_dic = {'A', 'C', 'G', 'T'}

    def contains_non_canonical_base(self, s):
        return sum([1 for nt in s if nt in {'A', 'C', 'G', 'T'}]) != len(s)

    def get_allowed_mismatches_threshold(self, motif):
        contains_non_canonical_bases = self.contains_non_canonical_base(motif)
        len_core = -1
        if contains_non_canonical_bases:
            len_core = sum([1.0 / len(self.iupac_table[nt]) for nt in motif])
            # print len_core
        else:
            len_core = len(motif)
        return int((len_core - 4) / 2) + 1

    def get_weight_sequence(self, motif):
        contains_non_canonical_bases = self.contains_non_canonical_base(motif)
        len_core = -1
        if contains_non_canonical_bases:
            len_core = sum([1.0 / len(self.iupac_table[nt]) for nt in motif])
            # print len_core
        else:
            len_core = len(motif)
        return len_core

    @staticmethod
    def simulate_kmers(L, N, pattern=None, mutation_prob=None, position_pattern=None):
        seqs = [SequenceMethods.get_random_sequence(L) for n in range(N)]
        assert pattern is None or len(pattern) <= L
        counts = [len(seqs) - si for si in range(len(seqs))]
        # paste a pattern into the k-mers
        if pattern is not None:
            for i in range(len(seqs)):
                next_mutation_prob = (counts[0] - counts[i]) / float(counts[0]) if mutation_prob is None else mutation_prob
                # print mutation_prob
                next = SequenceMethods.get_mutated_sequence_prob(pattern, next_mutation_prob)

                pi = random.randint(0, len(seqs[i]) - len(next)) if position_pattern is None else position_pattern
                seqs[i] = seqs[i][:pi] + next + seqs[i][pi + len(next):]
        df = pd.DataFrame()
        df['seq'] = seqs
        df['counts'] = counts
        return df

    @staticmethod
    def get_datasets_with_replicates():
        print '\nTECHNICAL REPRODUCIBILITY STEP...'
        bkp_path = "../../data/datasets_capselex_with_replicates.tsv.gz"
        queries = DataFrameAnalyzer.read_tsv_gz(bkp_path)
        return set(queries['filename'])

    @staticmethod
    def get_technical_reproducibility(tf_pair_id, filename, kmers):
        print '\nTECHNICAL REPRODUCIBILITY STEP...'
        bkp_path = "../../data/datasets_capselex_with_replicates.tsv.gz"
        queries = DataFrameAnalyzer.read_tsv_gz(bkp_path)
        print filename


        # query_keys = {k for k, grp in queries.groupby('tf.pair.id')}
        # print len(query_keys)

        cycle_code = list(queries[queries['filename'].str.contains(filename)]['cycle'])[0]
        grp = queries[(queries['tf.pair.id'] == tf_pair_id) & (queries['cycle'] == cycle_code)]

        print '# nrows', grp.shape[0]
        assert grp.shape[0] == 2

        mm_order = 5
        k_fixed_length = 10

        print 'matching tables'
        f1, f2 = grp['filename']
        df1, next_IG1 = SELEXAnalyzer.calculate_kmer_enrichments(f1, dataset_type='CAP-SELEX',
                                                                 k=k_fixed_length, mm_order=mm_order,
                                                                 count_reverse=True)
        df2, next_IG2 = SELEXAnalyzer.calculate_kmer_enrichments(f2, dataset_type='CAP-SELEX',
                                                                 k=k_fixed_length, mm_order=mm_order,
                                                                 count_reverse=True)

        m = df1[['seq', 'affinity']].merge(df2[['seq', 'affinity']], on='seq')

        print 'calculating correlations...'
        m = m[m['seq'].isin(kmers)]
        from sklearn.metrics import r2_score
        a, b = m['affinity_x'].astype(float), m['affinity_y'].astype(float)
        from lib.MachineLearning import MachineLearning
        r2_ab = MachineLearning.get_r2_score_ridge(np.array(a).reshape(-1, 1), list(b))
        r2_ba = MachineLearning.get_r2_score_ridge(np.array(b).reshape(-1, 1), list(a))
        out = list([pearsonr(a, b)[0], spearmanr(a, b)[0], r2_score(a, b), r2_ab, r2_ba])
        return out

        # only assess kmers that were selected by the enrichment approach

    def get_matches(self, a, b, use_degenerative_code=False):
        assert len(a) == len(b)

        counts = 0
        seq_len = len(a)

        if use_degenerative_code:
            for pi in range(len(a)):
                ai, bi = a[pi], b[pi]
                if bi in self.nuc_dic:
                    if ai == bi:
                        counts += 1
                else:  # the else condition evaluates for a nt belonging to a degenerate code
                    if ai in self.iupac_table[bi]:
                        counts += 1
        else:
            for pi in range(len(a)):
                ai, bi = a[pi], b[pi]
                if ai == bi:
                    counts += 1
        return counts

    @staticmethod
    def get_models_group(filename, reference_kmer, mm_order=5, mismatches_thr=2, dataset_type='CAP-SELEX',
                         queries=['1mer', '1mer+2mer', '1mer+2mer+3mer']):
        from lib.TFBinding.ModelGroup import ModelGroup
        from lib.TFBinding.TFBindingModel import TFBindingModel
        fg_kmers_capselex, next_IG = SELEXAnalyzer.calculate_kmer_enrichments(filename,
                                                                              dataset_type=dataset_type,
                                                                              k=len(reference_kmer), mm_order=mm_order,
                                                                              count_reverse=True)
        fg_kmers_capselex['n.matches'] = fg_kmers_capselex['seq'].apply(SequenceMethods.get_matches, args=[reference_kmer])
        X = fg_kmers_capselex[fg_kmers_capselex['n.matches'] >= len(reference_kmer) - mismatches_thr]
        model_group = ModelGroup.get_model_group(X['seq'], X['affinity'], queries=queries)
        return model_group

    def group_by_mwords(self, hits, core_motif, n_flanking):
        snp_analyzer = SNPAnalyzer()
        mwords = {}
        for hit in hits:
            seq, seq_rev, strand, pos, mword = hit[:-1]
            start, end = pos - n_flanking, pos + len(core_motif) + n_flanking
            mword = seq[start: end]
            if len(mword) < len(core_motif) + 2 * n_flanking:
                continue
            if strand == "-":
                mword = snp_analyzer.get_complementary_seq(mword)
            if not mword in mwords:
                mwords[mword] = 0
            mwords[mword] += 1

        return mwords

    def get_mwords(self, sequences, core_motif, n_flanking, allowed_mismatches=2):
        hits = self.get_coremotif_matches(sequences, core_motif,
                                          allowed_mismatches=allowed_mismatches)
        mwords = self.group_by_mwords(hits, core_motif, n_flanking)
        return mwords

    @staticmethod
    def get_datasets_description_capselex():
        from lib.SELEX.CAPSELEXAnalyzer import CAPSELEXAnalyzer
        return CAPSELEXAnalyzer.get_datasets_description()

    @staticmethod
    def get_composite_motif_enrichments():
        from lib.SELEX.CAPSELEXAnalyzer import CAPSELEXAnalyzer
        return CAPSELEXAnalyzer.get_composite_motif_enrichments()

    @staticmethod
    def get_kmers_subset(kmers_df, kmer_pattern, counts_thr=None, max_mismatches_thr=None):
        if max_mismatches_thr is None:
            selex = SELEXAnalyzer()
            max_mismatches_thr = selex.get_allowed_mismatches_threshold(kmer_pattern)
        print next, len(kmer_pattern)

        sel = kmers_df[kmers_df['counts'] >= counts_thr] if counts_thr is not None else kmers_df

        sel['n.matches'] = sel['seq'].apply(SequenceMethods.get_matches, args=[kmer_pattern])
        sel = sel[sel['n.matches'] >= len(kmer_pattern) - max_mismatches_thr]
        return sel

    @staticmethod
    def calculate_kmer_enrichments(fastq_filename, mm_order=5, k=9, counts_thr=0, max_homopolymer=-1,
                                   dataset_type=None, count_reverse=False, **kwargs): # 'CAP-SELEX'): # or HT-SELEx

        if dataset_type is None:
            print 'please indicate dataset type (HT-SELEX, CAP-SELEX)'
            return

        filename = fastq_filename
        filename_no_ext = filename.replace(".fastq.gz", '')

        R0_selex_dir = kwargs.get('R0_selex_dir', None)
        fg_selex_dir = kwargs.get('fg_selex_dir', None)
        fg, bg, nt_code, cycle = [None] * 4

        # formats for cycle and nt.codes are slighly different so CAP and HT SELEX datasets are processed independently
        if dataset_type == 'CAP-SELEX':
            nt_code = filename.replace(".fastq.gz", '').split("_")[-1]
            cycle = int(filename.split("_")[2])
            if fg_selex_dir is not None:
                fg = join(fg_selex_dir, filename)
            else:
                fg = join('/g/scb2/zaugg/rio/data/cap_selex_datasets_jolma_2015/fastq', filename)
            if R0_selex_dir is not None:
                bg = join(R0_selex_dir, 'ZeroCycle_' + nt_code + '_0_0.fastq.gz')
            else:
                bg = '/g/scb2/zaugg/rio/data/PRJEB20112/ZeroCycle_' + nt_code + '_0_0.fastq.gz'
        elif dataset_type == 'HT-SELEX':
            nt_code = filename.replace(".fastq.gz", '').split("_")[1]
            cycle = int(filename.split("_")[3].replace(".fastq.gz", ''))
            htselex_data_dir = '/g/scb2/zaugg/rio/data/htselex_data_jolma_2013'
            if fg_selex_dir is not None:
                fg = join(fg_selex_dir, filename)
            else:
                fg = join(htselex_data_dir, filename)
            if R0_selex_dir is not None:
                bg = join(R0_selex_dir, 'ZeroCycle_' + nt_code + '_0_0.fastq.gz')
            else:
                bg = join(htselex_data_dir, 'ZeroCycle_' + nt_code + '_0_0.fastq.gz')

        print exists(fg), fg
        print exists(bg), bg
        if not exists(fg) or not exists(bg):
            print 'one of the files is missing (FG/BG). This query cannot be processed'

            if not exists(bg):
                print 'R0 file has not been found. Please check directory or download CAP/HT-SELEX background files...'
                stop()

        res = []

        bkp_dir = '/g/scb2/zaugg/rio/data/selex_analyses_output'
        mm_bkp_dir = join(bkp_dir, 'markov_models')
        if not exists(mm_bkp_dir):
            mkdir(mm_bkp_dir)

        n_tfs_in_filename = {'HT-SELEX': 1, 'CAP-SELEX': 2}[dataset_type]
        tf_output_dir = "_".join(filename.split("_")[:n_tfs_in_filename])
        output_dir = join(bkp_dir, tf_output_dir, filename.replace(".fastq.gz", ''))
        if not exists(output_dir):
            makedirs(output_dir)

        # print 'mm_bkp_dir', print mm_bkp_dir
        fg_kmers, info_gain_df = SELEXAnalyzer.get_info_gain(fg, bg, k, mm_order, counts_thr, debug=False,
                                                             max_homopolymer=max_homopolymer, cycle=cycle,
                                                             bkp_dir=output_dir, remove_N=True,
                                                             count_reverse=count_reverse,
                                                             mm_bkp_dir=mm_bkp_dir,
                                                             **kwargs)

        return [fg_kmers, info_gain_df]

    @staticmethod
    def get_mean_relative_affinity_by_pattern(aff, pattern, aff_colname='affinity'):
        return np.nanmean(aff[aff['seq'].str.contains(pattern)][aff_colname])

    def get_coremotif_matches(self, sequences, core_motif, allowed_mismatches=2):
        '''
        Given a set of sequences return the best motif per sequence. Use decision
        rules based on Yang 2017 (MSB)
        :param sequences:
        :param core_motif:
        :param allowed_mismatches:
        :return:
        '''
        snp_analyzer = SNPAnalyzer()
        counter = 0

        print allowed_mismatches
        hits = []
        n_pattern_found = 0
        n_pattern_selected = 0
        core_len = len(core_motif)
        score1 = core_len - allowed_mismatches

        print "Score1 threshold", score1
        use_degenerative_code = self.contains_non_canonical_base(core_motif)
        n_kmer_found = 0
        for si, s in enumerate(sequences):
            seq_len = len(s)
            if si % 10000 == 0:
                print 'Done', si, 'out of', len(sequences)
            # calculate and select according to score1: Number of matches
            fwd, rev = s, snp_analyzer.get_complementary_seq(s)

            scores_fwd = [["+", i, fwd[i: i + core_len],
                           self.get_matches(fwd[i: i + core_len], core_motif,
                                       use_degenerative_code=use_degenerative_code)]
                          for i in range(0, seq_len - core_len + 1)]
            scores_rev = [["-", i, snp_analyzer.get_complementary_seq(
                fwd[i: i + core_len]),
                           self.get_matches(snp_analyzer.get_complementary_seq(
                               fwd[i: i + core_len]),
                                       core_motif,
                                       use_degenerative_code=use_degenerative_code)]
                          for i in range(0, seq_len - core_len + 1)]

            selected = scores_fwd + scores_rev
            selected = [sel for sel in selected if sel[-1] >= score1]

            if len(selected) == 0:
                continue

            # we can have a palindromic motif. In that case, we select if the
            # amount of matches is two (they both occured in the same position,
            # in forward and in reverse)
            # print selected
            score2 = score1 + 1
            if len(selected) > 1:
                selected = [sel for sel in selected if sel[-1] >= score2]

            if len(selected) == 0:
                continue

            # print selected
            # there are more than two hits forward: omit
            if len({t[1] for t in selected if t[0] == "+"}) > 1:
                continue
            if len({t[1] for t in selected if t[0] == "-"}) > 1:
                continue

            selected = [[fwd, rev] + t for t in selected]
            # print selected
            read_fwd, read_rev, strand, position, kmer_core = selected[-1][:-1]
            if strand == "-":
                kmer = snp_analyzer.get_complementary_seq(kmer)

        return hits

    def get_auc_seqmodel_vs_chipseq(self, model_path, fg_seqs, bg_seqs,
                                    model_type='HT-SELEX', **kwargs):
        '''
        Assess AUC between an MWORDS file from HT-SELEX (or PBM-kmer), and return a AUC value
        :param mwords_path:
        :param fg_seqs:
        :param bg_seqs:
        :return:
        '''
        fg_queries, bg_queries = None, None
        fg_scores, bg_scores = None, None
        peak_center = len(fg_seqs[0]) / 2

        if model_type == 'HT-SELEX':
            print 'reading mwords'
            print model_path
            df = DataFrameAnalyzer.read_mwords_tsv(model_path, sep=kwargs.get('sep', '\t'))

            df = df[df['counts'] > 8] # this is a criteria from Yang
            print df.shape
            if df.shape[0] < 10:
                return None
            motif_length = len(list(df['seq'])[0])
            shape_analyzer = ShapeAnalyzer()
            X_seq = shape_analyzer.get_X_1mer(list(df['seq']))
            clf = MachineLearning.TrainRidgeRegressor(X_seq, np.array(df['rel.affinity']))
            fg_queries = self.mapped_motifs_in_sequences(fg_seqs, motif_length)
            bg_queries = self.mapped_motifs_in_sequences(bg_seqs, motif_length)
            fg_1mer  = shape_analyzer.get_X_1mer(fg_queries['seq'])
            fg_scores = clf.predict(fg_1mer)
            bg_1mer = shape_analyzer.get_X_1mer(bg_queries['seq'])
            bg_scores = clf.predict(bg_1mer)

        elif model_type == 'PBM-kmer':
            kmers = DataFrameAnalyzer.read_tsv(model_path, header=None)
            score_by_kmer = {k: v for k, v in zip(kmers[0], kmers[1])}
            motif_length = len(list(score_by_kmer.keys())[0])
            fg_queries = self.mapped_motifs_in_sequences(fg_seqs, motif_length)
            bg_queries = self.mapped_motifs_in_sequences(bg_seqs, motif_length)
            fg_scores = [score_by_kmer[k] if k in score_by_kmer else 0 for k in fg_queries['seq']]
            bg_scores = [score_by_kmer[k] if k in score_by_kmer else 0 for k in bg_queries['seq']]

        fg_queries['score'] = fg_scores
        bg_queries['score'] = bg_scores

        # print 'grouping pos'
        pos = [max(grp['score']) for peak_id, grp in fg_queries.groupby('peak.id')]
        median_pos = median([abs(peak_center -
                                 list(grp.sort_values('score', ascending=False)['position'])[0] +
                                 motif_length / 2)
                             for peak_id, grp in fg_queries.groupby('peak.id')])
        # print 'grouping neg'
        neg = [max(grp['score']) for peak_id, grp in bg_queries.groupby('peak.id')]
        median_neg = median([abs(peak_center -
                                 list(grp.sort_values('score', ascending=False)['position'])[0] +
                                 motif_length / 2)
                             for peak_id, grp in bg_queries.groupby('peak.id')])
        auroc, auprc = MachineLearning.get_auroc_n_auprc([1] * len(fg_seqs) + [0] * len(bg_seqs), pos + neg)

        return auroc, auprc, median_pos, median_neg

    def get_motif_length(self, model_path, model_type='HT-SELEX', **kwargs):
        motif_length = None
        if model_type == 'HT-SELEX':
            df = DataFrameAnalyzer.read_mwords_tsv(model_path, sep=kwargs.get('sep', '\t'))
            motif_length = len(list(df['seq'])[0])
        elif model_type == 'PBM-kmer':
            kmers = DataFrameAnalyzer.read_tsv(model_path, header=None)
            score_by_kmer = {k: v for k, v in zip(kmers[0], kmers[1])}
            motif_length = len(list(score_by_kmer.keys())[0])
        return motif_length

    # model types can be 'KMER', 'HT-SELEX', 'PBM-KMER'
    def get_auc_seqmodel_vs_chipseq_fast(self, model_path, fg_seqs_or_path, bg_seqs_or_path,
                                    model_type='HT-SELEX', **kwargs):
        '''
        Assess AUC between an MWORDS file from HT-SELEX (or PBM-kmer), and return a AUC value
        :param mwords_path:
        :param fg_seqs: a list of sequences or a string path
        :param bg_seqs:
        :return:
        '''
        from lib.RFacade import RFacade

        fg_path, bg_path = None, None
        if isinstance(fg_seqs_or_path, str):
            fg_seqs = [s[1] for s in FastaAnalyzer.get_fastas(fg_seqs_or_path)]
            bg_seqs = [s[1] for s in FastaAnalyzer.get_fastas(bg_seqs_or_path)]
            fg_path = fg_seqs_or_path
            bg_path = bg_seqs_or_path
        elif isinstance(fg_seqs_or_path, list):
            fg_seqs = fg_seqs_or_path
            bg_seqs = bg_seqs_or_path

        n_fg, n_bg = len(fg_seqs), len(bg_seqs)

        fg_queries, bg_queries = None, None
        fg_scores, bg_scores = None, None
        peak_center = len(fg_seqs[0]) / 2

        print model_type
        if model_type == 'HT-SELEX':
            fg_queries = self.get_scores_seqs_vs_mwords(fg_path, model_path, seqs=fg_seqs, **kwargs)
            bg_queries = self.get_scores_seqs_vs_mwords(bg_path, model_path, seqs=bg_seqs, **kwargs)
        elif model_type == 'PBM-kmer':
            fg_queries = self.get_scores_seqs_vs_kmer(fg_seqs, model_path)
            bg_queries = self.get_scores_seqs_vs_kmer(bg_seqs, model_path)

        if fg_queries is None or fg_queries.shape[0] == 0:
            return n_fg, n_bg, None, None, None, None

        motif_length = self.get_motif_length(model_path, model_type=model_type, **kwargs)

        # print 'grouping pos'
        if model_type == 'PBM-kmer':
            pos = [max(grp['score']) for peak_id, grp in fg_queries.groupby('peak.id')]

            pos_peak_distances = self.get_distances_to_peak_center(fg_queries, peak_center,
                                                                   model_type=model_type)
            median_pos = median(pos_peak_distances)

            # print 'grouping neg'
            neg = [max(grp['score']) for peak_id, grp in bg_queries.groupby('peak.id')]
            neg_peak_distances = self.get_distances_to_peak_center(bg_queries, peak_center,
                                                                   model_type=model_type)
            median_neg = median(neg_peak_distances)
            # DEBUG: test in a little subset
            # pos_subset = [posi for posi in pos if posi != -1]
            # neg_subset = [negi for negi in neg if negi != -1]
            # auroc, auprc = MachineLearning.get_auroc_n_auprc([1] * len(pos_subset) + [0] * len(neg),
            #                                                  pos_subset + neg)
            # print 'median pos scores',  median(pos_subset)
            # print 'median neg scores',  median(neg_subset)
            # print auroc, auprc
            auroc, auprc = MachineLearning.get_auroc_n_auprc([1] * len(fg_seqs) + [0] * len(bg_seqs), pos + neg)
            pval_wilcoxon = RFacade.get_wilcox_test_pval(pos, neg)
            return n_fg, n_bg, auroc, pval_wilcoxon, median_pos, median_neg
        else:
            median_pos = median(self.get_distances_to_peak_center(fg_queries, peak_center))
            median_neg = median(self.get_distances_to_peak_center(bg_queries, peak_center))
            auroc, auprc = MachineLearning.get_auroc_n_auprc([1] * len(fg_seqs) + [0] * len(bg_seqs),
                                                             list(fg_queries['score']) + list(bg_queries['score']))
            pval_wilcoxon = RFacade.get_wilcox_test_pval(list(fg_queries['score']), list(bg_queries['score']))
            print n_fg, n_bg, auroc, pval_wilcoxon, median_pos, median_neg
            return n_fg, n_bg, auroc, pval_wilcoxon, median_pos, median_neg

    def get_distances_to_peak_center(self, queries, peak_center, model_type='HT-SELEX', na_skip=True):
        if model_type == 'HT-SELEX':
            motif_length = list(queries['end'])[0] - list(queries['start'])[0] + 1
            return [abs(peak_center - (start + motif_length / 2)) for start in queries['start']]
        elif model_type == 'PBM-kmer':
            peak_distances = []
            for peak_id, grp in queries.groupby('peak.id'):
                next_pos = list(grp[grp['score'] != -1].sort_values('score', ascending=False)['position'])
                if len(next_pos) == 0 and na_skip:
                    continue
                elif len(next_pos) == 0 and not na_skip:
                    peak_distances.append(None)
                else:
                    peak_distances.append(abs(peak_center - next_pos[0]))
            return peak_distances

    def get_scores_seqs_vs_kmer(self, seqs, model_path_or_kmers_dict, motif_length=8, queries=None):
        kmers, value_by_kmer = None, None
        if isinstance(model_path_or_kmers_dict, str):
            kmers = DataFrameAnalyzer.read_tsv(model_path_or_kmers_dict, header=None)
            value_by_kmer = {k: v for k, v in zip(kmers[0], kmers[1])}

            # if queries is None:
            queries = self.mapped_motifs_in_sequences(seqs, motif_length)
                # remove duplicates (one kmer per sequence)
                # queries['k'] = queries['peak.id'].astype(str) + "_" + queries['seq']
                # queries = queries.drop_duplicates('seq')

            scores = [value_by_kmer[k] if k in value_by_kmer else -1 for k in
                      queries['seq']]
            queries['score'] = scores
            return queries

        else:
            print 'preparing queries'
            kmers = model_path_or_kmers_dict.keys()
            value_by_kmer = model_path_or_kmers_dict
            queries = self.mapped_motifs_in_sequences(seqs, motif_length)
            print 'total queries', queries.shape
            res = []
            counter = 0
            for qi, q in queries.iterrows():
                if counter % 1000 == 0:
                    print counter, queries.shape[0]
                if q['seq'] in value_by_kmer:
                    res.append(list(q) + [value_by_kmer[q['seq']]])
                counter += 1

            res = pd.DataFrame(res, columns=['peak.id', 'position', 'strand', 'seq', 'scores'])
            return res

    @staticmethod
    def get_kmer_representatives(kmers):
        mismatches_by_kmer = {}
        for i, a in enumerate(kmers):
            print i, a
            for j, b in enumerate(kmers):
                if a == b:
                    continue
                else:
                    n_mismatch = min(len(a) - SequenceMethods.get_matches(a, b),
                                     len(a) - SequenceMethods.get_matches(a, SequenceMethods.get_complementary_nt(b)))
                    if n_mismatch <= 1:
                        if not a in mismatches_by_kmer:
                            mismatches_by_kmer[a] = 0
                        mismatches_by_kmer[a] += 1
            # print a, mismatches_by_kmer[a]
        return mismatches_by_kmer

    @staticmethod
    def get_permutations(k):
        bases=['A','T','G','C']
        kmers = [''.join(p) for p in itertools.product(bases, repeat=k)]
        return kmers

    @staticmethod
    def get_pvalue_model(aff, k=1, aff_colname='affinity', stopat=100):
        all = []
        for ri, r in aff.iterrows():
            sel = None
            # print r
            ref_score = r[aff_colname]
            if k == 1:
                options = {r['seq'][:si] + nt + r['seq'][si + 1:] for si in range(len(r['seq'])) for nt in 'ACTG'}
                aff['select'] = aff['seq'].isin(options) & (aff['seq'] != r['seq'])
                sel = aff[aff['select']]
                print ri, sel.shape
            else:
                sel = aff[aff['seq'].apply(SequenceMethods.get_matches, args=[r['seq']])]
            # print sel.head()
            sel['ref'] = ref_score
            sel['diff'] = (sel[aff_colname] - ref_score).abs()
            # print r['seq'], sel.shape
            if ri > stopat:
                break
            all.append(sel)
        all = pd.concat(all)
        print all.head()
        print all.tail()
        return np.nanmean(all['diff']), np.nanstd(all['diff'])



    @staticmethod
    def get_info_gain(fg, bg, K, mm_order, counts_thr, left=None, right=None, cycle=None,
                      count_reverse=False, debug=False, max_homopolymer=-1, calculate=True, overwrite=False,
                      overwrite_mm=None,
                      bkp_dir=None, mm_bkp_dir=None, fastq_ext='.fastq.gz', savelite=True,
                          mm_mode='DIVISION', **kwargs):
        print 'starting method...'
        output_path_kmers = None

        print 'checking final output path...', kwargs.get('bkp_dir', None)
        if bkp_dir is not None:
            output_path_kmers = join(bkp_dir, basename(fg).replace(fastq_ext, '') + "_L" +
                                     str(K) + "_" + "MM" + str(mm_order) + ".tsv.gz")
            # print 'putative output path'
            # print output_path_kmers
            # print exists(output_path_kmers), output_path_kmers
            info_gain_path = output_path_kmers.replace(".tsv.gz", "_IG.tsv.gz")
            # print exists(info_gain_path), info_gain_path

            if exists(output_path_kmers) and exists(info_gain_path) and not overwrite:
                print output_path_kmers
                print 'reading from output...'
                fg_kmers = DataFrameAnalyzer.read_tsv_gz(output_path_kmers)
                info_gain_df = DataFrameAnalyzer.read_tsv_gz(info_gain_path)
                return [fg_kmers, info_gain_df]
            if not calculate:
                print 'the requested file does not exists. Please run the command with calculate=True to generate it'
                return [None] * 2

        # print 'reading fg'
        fg_kmers = KMerCounter.read_fastq(fg, K, left=left, right=right, suffix='k' + str(K), count_reverse=count_reverse,
                                          overwrite=overwrite, bkp_dir=bkp_dir, **kwargs)

        print type(fg_kmers)
        # print fg_kmers
        # print 'reading bg'
        bg_kmers = KMerCounter.read_fastq(bg, K, left=left, right=right, suffix='k' + str(K), count_reverse=count_reverse,
                                          overwrite=overwrite, bkp_dir=mm_bkp_dir, **kwargs)

        # print 'MM bkp dir'
        # print mm_bkp_dir
        mm_bkp_path = join(mm_bkp_dir, basename(bg).replace(fastq_ext, '_' + str(mm_order) + ".pkl"))

        print 'possible MM output path...'
        print mm_bkp_path

        # print bg_kmers.sort_values('counts', ascending=False)

        # checking that for each sequence the counts for the complementary are EXACTLY the same (if count_reverse=True)
        if count_reverse:
            print 'checking that counts in reverse are the same (when count reverse=True)'
            counts_by_kmer = {}
            for ri, r in bg_kmers.iterrows():
                if not r['seq'] in counts_by_kmer:
                    counts_by_kmer[r['seq']] = r['counts']
                    rev = SequenceMethods.get_complementary_seq(r['seq'])
                    if rev in counts_by_kmer:
                        assert counts_by_kmer[rev] == r['counts']
            print 'debug done...'

        mm = SELEXAnalyzer.get_markov_model(bg_kmers['seq'], mm_order, mm_bkp_path,
                                            overwrite=overwrite if overwrite_mm is None else overwrite_mm,
                                            counts=bg_kmers['counts'],
                                            count_reverse=count_reverse, **kwargs)

        if count_reverse:
            for k in mm: # checking that the counts for A == cmp(A) in the background MM frequency dictionary
                assert mm[k] == mm[SequenceMethods.get_complementary_seq(k)]

        min_value = min(mm.values())
        est = []
        sum_fg = sum(fg_kmers['counts'])
        print 'calculating estimated counts per kmer (K=', K, ')...'
        print 'counts threshold', counts_thr

        n_valid_counts = sum(fg_kmers['counts'])

        print '# kmers', fg_kmers.shape
        est = []

        n_valid_counts_kmer_bg = float(sum([mm[k] for k in mm if len(k) == mm_order]))
        n_valid_counts_kmer_minus_1_bg = float(sum([mm[k] for k in mm if len(k) == (mm_order - 1)]))

        print n_valid_counts_kmer_bg, n_valid_counts_kmer_minus_1_bg
        correction_factor = n_valid_counts_kmer_minus_1_bg / n_valid_counts_kmer_bg
        # fg_kmers = fg_kmers[fg_kmers['seq'].isin({'AAAAA', 'AAAAC', 'AAAAT', 'AAAAG'})]


        # will the sum of probabilities for both states and their emission probabilities be one?
        if debug:
            probs_order_n = {k: mm[k] for k in mm if len(k) == mm_order}
            probs_order_n_minus_1 = {k: mm[k] for k in mm if len(k) == mm_order - 1}
            probs_order_n = {k: probs_order_n[k] / n_valid_counts_kmer_bg for k in probs_order_n}
            probs_order_n_minus_1 = {k: probs_order_n_minus_1[k] / n_valid_counts_kmer_minus_1_bg for k in probs_order_n_minus_1 }
            print sum(probs_order_n.values())
            print sum(probs_order_n_minus_1.values())
            # for kmer in SequenceMethods.get_sequence_combinations(5):



        # filter kmers that do not have enoughs support
        print 'total entries (before counts filtering)', fg_kmers.shape
        fg_kmers = fg_kmers[fg_kmers['counts'] >= counts_thr]

        # filter by homopolymers
        if max_homopolymer != -1:
            fg_kmers['max.nt.count'] = [max({i: s.count(i) for i in set(s)}.values()) for s in fg_kmers['seq']]
            fg_kmers = fg_kmers[fg_kmers['max.nt.count'] < max_homopolymer]


        counter = 0
        nrows = fg_kmers.shape[0]
        nrows_100th = nrows / 100
        nrows_10th = nrows / 10
        print nrows_100th

        for ri, r in fg_kmers.iterrows():
            if nrows_10th != 0 and counter % nrows_10th == 0:
                print counter / nrows_10th, 'out of 10', 'nrows=', fg_kmers.shape[0]
            counter += 1
            kmer = r['seq']
            s = kmer[:mm_order - 1]
            factor = 1.0 * mm[s] / float(n_valid_counts_kmer_bg)

            if debug:
                print 'kmer', kmer
                print 'initial factor', s, factor

            for i in range(len(kmer) - mm_order + 1):
                # print i, mm_mode
                next = 0.0
                if mm_mode == 'TRANSITION':
                    s = kmer[i: i + mm_order]
                    next = mm[s] / float(sum([mm[s[:-1] + nt] for nt in 'ACGT']))
                    if debug:
                        print s, mm[s]
                        print s, 'given', s[:-1] + "N", '=', next
                elif mm_mode == 'DIVISION':
                    s = kmer[i: i + mm_order]

                    a = (mm[s] / n_valid_counts_kmer_bg)
                    b = (mm[s[:-1]] / n_valid_counts_kmer_minus_1_bg)

                    # print s, a, n_valid_counts_kmer_bg, b, n_valid_counts_kmer_minus_1_bg
                    next = a / b

                    if debug:
                        print s, mm[s]
                        print s, a, 'given', s[:-1], b, '=', next

                if debug:
                    print factor, '*', next, '=', factor * next
                    print 'done\n'

                factor *= next


            est.append(factor)

        fg_kmers['est'] = est
        fg_kmers['est'] /= correction_factor

        print fg_kmers.head()
        # for some reason to run the DIVISION algorithm we have to divide by 2

        # fg_kmers['est'] /= {5: 2.0, 6: 1.5, 7: 1.35}[K]
        fg_kmers['est.counts'] = fg_kmers['est'] * n_valid_counts
        # DEBUG if this is actually necessary or not
            # fg_kmers['est'] = fg_kmers['est'] / sum(fg_kmers['est'])
            # make the sum(counts) == sum(est)
            # fg_kmers['est'] = fg_kmers['est'] * sum_fg


        # here we filter the reads


        # print fg_kmers[fg_kmers['seq'] == 'AAACA']
        # print bg_kmers[bg_kmers['seq'] == 'AAACA']

        # 0-1 normalization step
        fg_kmers['fg.freq'] = fg_kmers['counts'] / n_valid_counts
        fg_kmers['bg.freq'] = fg_kmers['est.counts'] / n_valid_counts


        fg_kmers['a'] = fg_kmers['fg.freq'] * np.log2(fg_kmers['fg.freq'] / fg_kmers['bg.freq'])
        # print sum(df['a'])

        # calculate affinities according to the cycle
        assert cycle is not None
        fg_kmers['y'] = fg_kmers['fg.freq'] / fg_kmers['bg.freq']
        fg_kmers['affinity'] = (fg_kmers['y'] / max(fg_kmers['y'])) ** (1.0/ (1.0 + (cycle - 1)))
        fg_kmers['se'] = fg_kmers['affinity'] * ((2 / fg_kmers['counts']) ** 0.5)

        info_gain = []
        info_gain_df = SELEXAnalyzer.get_info_gain_from_kmers(fg_kmers, [0, 50, 100])

        output = fg_kmers, info_gain_df
        if output_path_kmers is not None:
            if kwargs.get('savelite', True):
                for c in ['est', 'est.counts', 'fg.freq', 'bg.freq', 'a', 'affinity', 'se']:
                    fg_kmers[c] = fg_kmers[c].map(lambda x: '%1.3E' % x)
            DataFrameAnalyzer.to_tsv_gz(fg_kmers, output_path_kmers)
            DataFrameAnalyzer.to_tsv_gz(info_gain_df, output_path_kmers.replace(".tsv.gz", "_IG.tsv.gz"))
        return output

    @staticmethod
    def get_info_gain_from_kmers(fg_kmers, counts_thr, log=True):
        n_valid_counts = sum(fg_kmers['counts'])
        info_gain = []
        for next_counts_thr in counts_thr:
            sel = fg_kmers[fg_kmers['counts'] >= next_counts_thr]
            sum_fg = min(sum(sel['fg.freq'].astype(float)), 1.0)
            sum_bg = min(sum(sel['bg.freq'].astype(float)), 1.0)

            numerator, denominator = 1 - sum_fg, 1 - sum_bg
            b = 0.0
            if numerator > 0 and denominator > 0:
                b = np.log2(numerator / denominator)
            a = sum(sel['a'].astype(float))

            IG = a + numerator * b
            if log:
                # print '(a, b)', a, numerator, denominator, b
                print 'counts threshold', next_counts_thr
                print '\tnoOfValidRead', n_valid_counts
                print '\tS%i_SUM' % (next_counts_thr), a
                print '\tS%i_MODEL_PROB_SUM' % (next_counts_thr), sum_fg
                print '\tS%i_EXP_PROB_SUM' % (next_counts_thr), sum_bg
                print '\tINFO_GAIN', IG
                print '\tN valid counts', n_valid_counts
            info_gain.append([next_counts_thr, sum_fg, sum_bg, a, b, IG, n_valid_counts])
        info_gain_df = pd.DataFrame(info_gain, columns=['counts.thr', 'sum.fg', 'sub.bg', 'a', 'b', 'IG', 'n.valid.counts'])
        return info_gain_df


    @staticmethod
    def get_kmer_counts(seqs, K, counts=None):
        """
        For each sequence map the number of occurences for every possible k-mer, and then
        return the best expected case sorted by decreasing counts (observations)
        :param seqs:
        :param min_length:
        :param max_length:
        :return:
        """
        counts_by_kmers = {}
        for i, s in enumerate(seqs):
            if i % 1000 == 0:
                print i, len(seqs)
            for si in range(len(s) - K + 1):
                fwd = s[si: si + K]
                rev = SequenceMethods.get_complementary_seq(fwd)
                next = fwd if fwd > rev else rev
                if not next in counts_by_kmers:
                    counts_by_kmers[next] = 0
                counts_by_kmers[next] += 1 if counts is None else counts[i]

        res = pd.DataFrame([[k, counts_by_kmers[k]] for k in counts_by_kmers.keys()],
                           columns=['kmer', 'counts'])
        return res.sort_values('counts', ascending=False)

    @staticmethod
    def get_markov_model(seqs, order, bkp_path, counts=None, stopat=None, count_reverse=False, **kwargs):

        if bkp_path is not None and exists(bkp_path) and not kwargs.get('overwrite', False):
            print 'reading mm from path'
            print bkp_path
            return DataFrameAnalyzer.read_pickle(bkp_path)

        if bkp_path is not None and not exists(bkp_path):
            print 'reading and writing MM to'
            print bkp_path

        def window(seq, n=2):
            "Returns a sliding window (of width n) over data from the iterable"
            "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
            it = iter(seq)
            result = tuple(islice(it, n))
            if len(result) == n:
                yield result
            for elem in it:
                result = result[1:] + (elem,)
                yield result

        # initialize in the order we want to calculate
        mm = {}
        for k in SequenceMethods.get_sequence_combinations(order):
            mm[k] = 0
        for k in SequenceMethods.get_sequence_combinations(order - 1):
            mm[k] = 0

        # get the counts for the order N and order N-1, to handle both TRANSITION and DIVISION algorithms
        print 'calculating MM (count_reverse=%i)' % count_reverse
        for order_i in range(order - 1, order + 1):
            for i, s in enumerate(seqs):
                if stopat is not None:
                    if i >= stopat:
                        print i, 'stopping due to explicit stop.at break'
                        break
                if i % 10000 == 0:
                    print 'counting at order', order_i, i, 'out of', len(seqs)
                for fwd in window(s, order_i):
                    fwd = "".join(fwd)

                    # skip Ns
                    if 'N' in fwd:
                        continue

                    mm[fwd] += 1 if counts is None else counts[i]

                    if count_reverse:
                        rev = SequenceMethods.get_complementary_seq(fwd)
                        mm[rev] += 1 if counts is None else counts[i]

                        # print fwd, rev, mm[fwd], mm[rev]

        # because not all options are able to be concatenated (e.g. AT cannot be ligaed to nothing but T[A/C/G/T])
        if bkp_path is not None and (not exists(bkp_path) or kwargs.get('overwrite', False)):
            print 'markov model parameters written to'
            print bkp_path
            DataFrameAnalyzer.to_pickle(mm, bkp_path)

        print mm['TAGTC'], mm['GACTA']
        print mm['AAATG'], mm['CATTT']
        return mm

    def get_scores_seqs_vs_mwords(self, fa_path, model_path, **kwargs):
        print 'reading mwords'
        print model_path

        seqs = kwargs.get('seqs', None)
        if seqs is None:
            seqs = [s[1] for s in FastaAnalyzer.get_fastas(fa_path)]
        df = DataFrameAnalyzer.read_mwords_tsv(model_path, sep=kwargs.get('sep', '\t'))
        df = df[df['counts'] > 8]  # this is a criteria from Yang

        print 'entries (after filtering)', df.shape
        if df.shape[0] < 10:
            return None

        # print df.head()
        motif_length = len(list(df['seq'])[0])
        shape_analyzer = ShapeAnalyzer()
        X_seq = shape_analyzer.get_X_1mer(list(df['seq']))
        clf = MachineLearning.TrainRidgeRegressor(X_seq, np.array(df['rel.affinity']))

        print 'coefficients as PWM'
        w = clf.coef_
        pwm = pd.DataFrame([w[i * 4: i * 4 + 4] for i in range(len(w) / 4)]).transpose()
        pwm.index = ['A', 'C', 'T', 'G']
        # print pwm

        pssm_path = "/tmp/pssm_tmp_" + str(random.randint(1, 100000)) + "_" + basename(model_path)
        print 'running find pssm'
        hits = find_pssm_hits(pwm, fa_path, use_c_routine=True, as_dataframe=True,
                                 pssm_path=pssm_path)
        remove(pssm_path)
        hits.columns = ['peak.id'] + list(hits.columns[1:])

        motifs_lists = []
        motifs = []
        for ri, r in hits.iterrows():
            next = seqs[ri][r['start'] - 1: r['end'] - 1 + 1].upper()
            if r['strand'] != '+':
                next = SequenceMethods.get_complementary_seq(next)
            motifs.append(next)
            # print seqs[ri]
            # print next, r['strand']
        hits['seq'] = motifs

        # print fg_hits.head()
        # print bg_hits.head()

        hits_1mer = shape_analyzer.get_X_1mer(hits['seq'])
        scores = clf.predict(hits_1mer)
        # print fg_scores
        # print fg_scores.shape
        queries = hits
        queries['score'] = scores
        return queries

    def mapped_motifs_in_sequences(self, seqs, motif_length, double_strand=True):
        table = []
        for si, s in enumerate(seqs):
            if si != 0 and si % 1000 == 0:
                print 'sequences mapped so far', si, s
            for i in range(0, len(s) - motif_length + 1):
                next = s[i: i + motif_length]
                table.append([si, i, "+", next])
                table.append([si, i, "-", SequenceMethods.get_complementary_seq(next)])
        # print 'concatenating into single dataframe...'
        return pd.DataFrame(table, columns=['peak.id', 'position', 'strand', 'seq'])


    def create_kmers_paths(self, aff_data, query_dirs, output_dir, delta_cycle=0,
                           overwrite=False):
        kmer_paths = []
        print 'creating appropiate affinity files from fastq...'
        print ''

        last_cycle_fastq_paths, r0_fastq_paths = self.get_fastq_files(aff_data, query_dirs,
                                                                      delta_cycle=delta_cycle)

        print last_cycle_fastq_paths
        print r0_fastq_paths
        fastq_analyzer = FastQAnalyzer()

        for fastq_paths in (last_cycle_fastq_paths, r0_fastq_paths):

            # when multiple kmer paths are found, base naming in the first one
            kmers_output_path = join(output_dir, basename(fastq_paths[0]))

            # convert reads to kmer counts
            if overwrite or not exists(kmers_output_path):
                print kmers_output_path
                if not exists(output_dir):
                    mkdir(output_dir)

                if overwrite or not exists(kmers_output_path):
                    sequences_by_path = {}
                    for p in fastq_paths:
                        print 'getting sequences for', p
                        sequences_by_path[p] = fastq_analyzer.get_sequences(p, remove_N=True,
                                                                            log=False)
                        print "# unique sequences", len({s for s in sequences_by_path[p]})

                    all_sequences = [s for p in sequences_by_path for s in sequences_by_path[p]]
                    uniq_seqs = {s for p in sequences_by_path for s in sequences_by_path[p]}
                    print '# total sequences (all)', len(all_sequences)
                    print '# uniq sequences (all)', len(uniq_seqs)
                    print 'writing kmer dictionary'
                    fastq_analyzer.write_sequences_as_kmers(all_sequences, kmers_output_path,
                                                            compress=True,
                                                            count_complementary=False)

            kmer_paths.append(kmers_output_path)
        return kmer_paths

    def get_matches(self, a, b, use_degenerative_code=True, min_matches_expected=None):
        assert len(a) == len(b)

        counts = 0
        seq_len = len(a)

        if use_degenerative_code:
            for pi in range(len(a)):
                ai, bi = a[pi], b[pi]
                if bi in self.nuc_dic:
                    if ai == bi:
                        counts += 1
                else: # the else condition evaluates for a nt belonging to a degenerate code
                    if ai in self.iupac_table[bi]:
                        counts += 1
                # this is an optimizer: if we don't get a min amount of mismatches
                # by a given position we simply stop counting and return none.
                # (similar to MOODS).
                if min_matches_expected is not None:
                    if counts < min_matches_expected and (seq_len - pi + 1) < (min_matches_expected - counts):
                        return counts
        else:
            for pi in range(len(a)):
                ai, bi = a[pi], b[pi]
                if ai == bi:
                    counts += 1
                # this is an optimizer: if we don't get a min amount of mismatches
                # by a given position we simply stop counting and return none.
                # (similar to MOODS).
                if min_matches_expected is not None:
                    if counts < min_matches_expected and (seq_len - pi + 1) < (
                        min_matches_expected - counts):
                        return counts
        return counts

    def split_train_test(self, fastq_path):
        '''
        Divide a fastq path into a training and a testing file, for purposes
        related to the MM training and fitting into the best order
        :param fastq_path:
        :return:
        '''
        path1, path2 = tempfile.mkstemp()[1], tempfile.mkstemp()[1]

        n_lines = [1 for i in gzip.open(fastq_path)]
        print n_lines
        # get number of lines in gzip


    def get_scores(self, mwords_fg, mwords_bg, round_i=4,
                   reliability_counts_thrs=8):
        table = []

        # average counts in the background
        mean_counts_bg = mean([mwords_bg[k] for k in mwords_bg])
        print mean_counts_bg
        for k in sorted(mwords_fg.keys(), key=lambda x: -mwords_fg[x]):
            counts_fg = mwords_fg[k]

            if counts_fg < reliability_counts_thrs:
                continue
            # the count is not reliable is its counts are less than a value
            counts_bg = mwords_bg[k] if k in mwords_bg else mean_counts_bg

            score_i = pow(counts_fg / float(counts_bg), 1 / float(round_i))
            table.append([k, counts_fg, counts_bg, score_i])
        df = pd.DataFrame(table, columns=['sequence', 'counts.fg', 'counts.bg', 'score'])
        df = df.sort_values('score', ascending=False)

        # normalize values between 0 and 1
        df['score'] = df['score'] / df['score'].max()
        return df

    def get_complementary_nt(self, nt):
        if nt == "A":
            return "T"
        if nt == "T":
            return "A"
        if nt == "C":
            return "G"
        if nt == "G":
            return "C"
        if nt == "N":
            return "N"

    def get_complementary_seq(self, s):
        return "".join([self.get_complementary_nt(nt) for nt in s][::-1])

    def is_palindromic(self, s):
        return self.get_complementary_seq(s) == s

    @staticmethod
    def get_htselex_paths_by_tf(tf_name):
        htselex_data_dir = '/g/scb2/zaugg/rio/data/htselex_data_jolma_2013'
        return [join(htselex_data_dir, f) for f in listdir(htselex_data_dir) if tf_name + "_" in f]

    def get_input_htselex_fastq_paths(self, aff_data, htselex_fastq_data_dirs, delta_cycle=0):
        '''
        Given an output aff_data object instance and a directory with HT-SELEX data,
        return the appropiate HT-SELEX file
        :param aff_data:
        :param ht_selex_data_dir:
        :return:
        '''
        # print 'query aff file', aff_data.file_name
        htselex_fastq_paths = []
        d = htselex_fastq_data_dirs
        d = d if isinstance(d, list) else [d]
        for fastq_dir in d:
            # print aff_data.barcode
            # print fastq_dir
            htselex_files = [selex_file for selex_file in listdir(fastq_dir)
                              if aff_data.barcode in selex_file and aff_data.tf_name in selex_file and
                              "_" + str(int(aff_data.cycle) + delta_cycle) + "." in selex_file]

            htselex_fastq_paths += [join(fastq_dir,  fastq) for fastq in htselex_files]

        assert len(htselex_fastq_paths) != 0
        return htselex_fastq_paths

    def get_input_zero_cycle_fastq_paths(self, aff_data, htselex_fastq_data_dirs):
        '''
        Given an affinity output table and a set of fastq files related to HT-SELEX,
        infer and return the respective zero-cycle file
        :param aff_data:
        :param htselex_fastq_data_dirs:
        :return:
        '''

        # print 'query aff file', aff_data.file_name
        selected_fastq_paths = []
        d = htselex_fastq_data_dirs
        d = d if isinstance(d, list) else [d]
        for fastq_dir in d:
            # print aff_data.barcode
            # print fastq_dir
            fastq_files = [f for f in listdir(fastq_dir) if "ZeroCycle" in f and aff_data.barcode in f]
            print fastq_dir, fastq_files
            selected_fastq_paths  = selected_fastq_paths + [join(fastq_dir, fastq) for fastq in fastq_files]

        # print aff_data.barcode
        # print selected_fastq_path
        assert len(selected_fastq_paths) != 0
        return selected_fastq_paths

    def get_fastq_files(self, aff_data, htselex_fastq_data_dirs, **kwargs):
        '''
        Look for the respective fastq files that generated this affinity table, and return them
        :param aff_data:
        :param htselex_fastq_data_dirs:
        :return:
        '''

        delta_cycle = kwargs.get('delta_cycle', 0)
        # last cycle
        last_cycle_fastq_paths = self.get_input_htselex_fastq_paths(aff_data,
                                                                  htselex_fastq_data_dirs,
                                                                    delta_cycle=delta_cycle)
        print last_cycle_fastq_paths
        # ZeroCycle (R0)
        r0_fastq_paths = self.get_input_zero_cycle_fastq_paths(aff_data, htselex_fastq_data_dirs)

        print last_cycle_fastq_paths
        print r0_fastq_paths

        return last_cycle_fastq_paths, r0_fastq_paths


    @staticmethod
    def convert_myt1l_to_jaspar(output_dir='/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data/myt1l_selex'):
        # scan for motifs using the pwms provide by SELEX
        data = pd.read_excel('../../data/Table_S5_SELEX_data.xlsx', sheetname='PWMs')
        data = [r.values for ri, r in data.tail(data.shape[0] - 11).iterrows()]
        counter = 0

        pfms = []
        title = None
        rows = []

        for r in data:
            if counter % 5 == 0:
                title = list(r)[0] + "_" + list(r)[1]
            else:
                rows.append(map(int, [v for v in list(r)[1:] if not np.isnan(v)]))
            counter += 1

            if len(rows) == 4:
                pfm = pd.DataFrame(rows)
                pfm.index = ['A', 'C', 'T', 'G']
                pfms.append(pfm)
                rows = []
                print pfm
                MotifConverter.write_jaspar(pfm, title, join(output_dir, title + ".jaspar"))
                print join(output_dir, title + ".jaspar")