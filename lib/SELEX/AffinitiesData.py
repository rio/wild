
import pandas as pd
from os.path import basename

class AffinitiesData:
    """A file that represents the summary affinities as calculated by Yang et al. MSB (2017)

     Attributes:
         Dataframe  A summary table with all values
         Kmer analysis parameters   (cycle number, tf_name, core_motif, etc.)
     """

    def __init__(self, path):
        # compare counts with respect to reference table
        # print path
        self.df = pd.read_csv(path, sep=None, index_col=None, header=None, engine='python')
        self.df.columns = ['mword', 'affinity', 'counts']

        self.file_name = basename(path)
        split = self.file_name.replace(".txt", "").split("_")
        self.dbd_family, self.tf_name, self.barcode,\
        self.core_motif, self.mword_length, self.cycle = split

        self.mword_length = int(self.mword_length)

    def get_total_counts(self):
        return self.df['counts'].sum()

    def intersect(self, aff_data):
        '''
        Intersect with another Aff_data, and return a final dataframe with the
        intersections
        :param aff_data:
        :return:
        '''
        return self.df.merge(aff_data.df, on='mword')

    def get_missing_mwords(self, aff_data):
        """
        Report cases not found in this entry, wrt to the query one
        :param aff_data:
        :return:
        """
        df = self.df.merge(aff_data.df, on='mword', how='outer', indicator=True)
        df = df[df['_merge'] == "left_only"]
        del df['_merge']
        for k in ("affinity_y", 'counts_y'):
            del df[k]
        df = df.rename(columns={"affinity_x": "affinity", "counts_x": 'counts'})
        df['counts'] = df['counts'].astype(int)
        return df

    def __str__(self):
        attrs = vars(self)
        return '\n'.join("%s: %s" % item for item in attrs.items())