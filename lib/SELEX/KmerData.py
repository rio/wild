
import pandas as pd
from os.path import basename

class KmerData:
    """
    A file that represents a table with two parameters: Kmer, and counts for that kmer
     """

    def __init__(self, path):
        # compare counts with respect to reference table
        self.df = pd.read_csv(path, sep='\t', index_col=None, header=None)
        self.df.columns = ['kmer', "counts"]
        self.file_name = basename(path)


    def get_total_counts(self):
        return self.df['counts'].sum()

    def get_n_kmers(self):
        return self.df.shape[0]