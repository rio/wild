'''
Created on 2/4/2018, 2018

@author: Ignacio Ibarra Del Rio

Description:
'''


from lib.utils import *
from lib.DNAshapedTFBS.DNAshapedTFBS import get_jaspar_pssm
from lib.SELEX.SELEXAnalyzer import SELEXAnalyzer
from lib.REMAPAnalyzer import REMAPAnalyzer
from lib.SequenceMethods import SequenceMethods

class CAPSELEXAnalyzer():

    @staticmethod
    def get_kmers_capselex():

        local_path = "../../data/core_motifs_by_tf_pair.tsv"
        cluster_path = join('/g/scb/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/data/core_motifs_by_tf_pair.tsv')
        path = cluster_path if not exists(local_path) else local_path
        df = DataFrameAnalyzer.read_tsv(path)
        df = df[~df['CORE_MOTIF'].str.contains('40N')]
        return df

    @staticmethod
    def get_datasets_description():
        # read CAPSELEX dataframe that contains all the info available
        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/data/PRJEB7934.txt'
        df = DataFrameAnalyzer.read_tsv(p)
        df['filename'] = df['submitted_ftp'].str.split("/").str[-1]
        df['tf.pair.id'] = ["_".join(k.split("_")[:2]) for k in df['filename']]
        df['nt.code'] = df['filename'].str.split("_").str[-1].str.replace(".fastq.gz", '')
        return df

    @staticmethod
    def get_tfs_from_filename(filename):
        return filename.split("_")[:2]

    @staticmethod
    def get_composite_motif_enrichments():
        pwm_path = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/data/pwm_data.txt"
        if not exists(pwm_path):
            pwm_path = join("C:\\Users\\ignacio\\Dropbox\\Eclipse_Projects\\zaugglab\\comb-TF-binding" ,
                            "cap_selex_quantitative_analysis\\data\\pwm_data.txt")
            if not exists(pwm_path):
                pwm_path = '/mnt/c/Users/ignacio/Dropbox/Eclipse_Projects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/data/pwm_data.txt'
        lines = [r for r in open(pwm_path) if r.startswith("Base")]
        rows = [r.strip().split("\t") for r in lines]
        columns, rows = rows[1], rows[2:]
        df = pd.DataFrame(rows, columns=columns)
        df.columns = list(df.columns[:1]) + ['symbol'] + list(df.columns[2:])
        df['filename'] = df['symbol'] + "_" + df['cycle'].str[0] + "_" + df['batch'] + "_" + df['ligand sequence'] + ".fastq.gz"
        return df

    @staticmethod
    def get_capselex_data_summary():
        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/data/cap_selex_data_summary.tsv'
        if not exists(p):
            p = abspath('../../data/cap_selex_data_summary.tsv')
        assert exists(p)
        df = DataFrameAnalyzer.read_tsv(p)
        df['TF_PAIR'] = df['tf1'] + "_" + df['tf2']
        return df

    @staticmethod
    def get_ppms():
        basedir = '../../../cap_selex_quantitative_analysis/data/pfm_jaspar'
        ppms_table = []
        for f in listdir(basedir):
            if not f.endswith('.ppm'):
                continue
            p = join(basedir, f)
            ppms_table.append([f.replace(".ppm", ''), get_jaspar_pssm(p, False)])
        return pd.DataFrame(ppms_table, columns=['capselex.id', 'pssm'])


    @staticmethod
    def get_ppm_by_id(jolma_pfm_id):
        basedir = '../../../cap_selex_quantitative_analysis/data/pfm_jaspar'
        p = join(basedir, jolma_pfm_id + ".ppm")
        if not exists(p):
            return None
        return get_jaspar_pssm(p, False)

    @staticmethod
    def get_zscores_family_pair(fam1, fam2, **kwargs):
        cm = CAPSELEXAnalyzer.get_composite_motif_enrichments()
        cm = cm[cm['families'].str.lower().str.contains(fam1.lower()) & cm['families'].str.lower().str.contains(fam2.lower())]

        filenames = set(cm['filename'])
        all = []
        for fi, f in enumerate(filenames):
            print fi, f, len(filenames)
            zscores = CAPSELEXAnalyzer.get_zscores_cap_vs_ht(f)
            zscores['filename'] = f
            all.append(zscores)

        all = pd.concat(all)
        all = all[~np.isnan(all['z.score'])]
        print all.head()
        print all.tail()

    @staticmethod
    def get_queries_remap_vs_capselex():

        remap_names = REMAPAnalyzer.get_tfs_by_dbd()
        cm = CAPSELEXAnalyzer.get_composite_motif_enrichments()
        dbd_by_name = {}
        for ri, r in cm[cm['experiment type'].str.contains('CAP')].iterrows():
            if not "_" in r['symbol']:
                continue
            for tf, dbd in zip(r['symbol'].split("_"), r['families'].split("_")):
                # print tf, dbd
                if tf in dbd_by_name:
                    continue
                dbd_by_name[tf] = dbd

        queries = []
        for tf in dbd_by_name:
            fam_cap = dbd_by_name[tf].replace('Fox', 'Forkhead')
            if 'homeo' in fam_cap.lower():
                fam_cap = 'Homeo'
            if 'PAX' in fam_cap:
                fam_cap = 'Paired box'
            if 'AP2' in fam_cap:
                fam_cap = 'AP-2'
            if 'NuclReceptor' in fam_cap:
                fam_cap = 'Nuclear receptor'
            if fam_cap == 'Tbox':
                fam_cap = 'T-box'
            found = False
            for fam_remap in set(remap_names['DBD']):
                fam_remap = str(fam_remap)
                if fam_cap.lower() in fam_remap.lower() or fam_remap.lower() in fam_cap.lower():
                    found = True
                    for tf_remap in remap_names[remap_names['DBD'] == fam_remap]['gene.name']:
                        queries.append([tf, fam_cap, tf_remap, fam_remap, tf == tf_remap,
                                        SequenceMethods.levenshtein_iterative(tf, tf_remap)])
            queries.append([tf, fam_cap, None, None, None, None])
            if not found:
                print tf, dbd_by_name[tf]
        queries = pd.DataFrame(queries, columns=['tf.cap', 'fam.cap', 'tf.remap', 'fam.remap', 'same', 'name.mismatches'])
        return queries

    @staticmethod
    def get_zscores_cap_vs_ht(cap_filename='FOXO1_ELK3_2_AS_TTGGGT40NGGC.fastq.gz', k=10, mm_order=5):
        # res = []
        cm = CAPSELEXAnalyzer.get_composite_motif_enrichments()
        queries_bkp = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/data/queries_capselex_FE_generator.tsv.gz'
        df = DataFrameAnalyzer.read_tsv_gz(queries_bkp)
        capsel = df[(df['dataset.type'] == 'CAP-SELEX') & (df['k'] == k)]
        htsel = df[(df['dataset.type'] == 'HT-SELEX') & (df['k'] == k)]
        htsel['cycle'] = htsel['filename'].str.split("_").str[3].str.replace(".fastq.gz", "").astype(int)
        htsel['tf.name'] = htsel['filename'].str.split("_").str[0]

        tf1, tf2 = cap_filename.split("_")[:2]
        ht1_sel = htsel[(htsel['tf.name'] == tf1)].drop_duplicates('filename').sort_values('cycle',
                                                                                           ascending=False).head(1)
        ht2_sel = htsel[(htsel['tf.name'] == tf2)].drop_duplicates('filename').sort_values('cycle',
                                                                                           ascending=False).head(1)

        from lib.SELEX.SELEXAnalyzer import SELEXAnalyzer
        outcap = SELEXAnalyzer.calculate_kmer_enrichments(cap_filename,
                                                          dataset_type='CAP-SELEX',
                                                          k=k, mm_order=mm_order,
                                                          count_reverse=True,
                                                          calculate=False, overwrite=False)

        tf1_filename = list(ht1_sel['filename'])[0]
        tf2_filename = list(ht2_sel['filename'])[0]

        outht1 = SELEXAnalyzer.calculate_kmer_enrichments(tf1_filename,
                                                          dataset_type='HT-SELEX',
                                                          k=k, mm_order=mm_order,
                                                          count_reverse=True,
                                                          calculate=False, overwrite=False)
        outht2 = SELEXAnalyzer.calculate_kmer_enrichments(tf2_filename,
                                                          dataset_type='HT-SELEX',
                                                          k=k, mm_order=mm_order,
                                                          count_reverse=True,
                                                          calculate=False, overwrite=False)
        cap, IG = outcap
        ht1, IG = outht1
        ht2, IG = outht2

        cap['cap'] = cap['affinity']

        ht1['ht1'] = ht1['affinity']
        ht2['ht2'] = ht2['affinity']


        aff = cap[['seq', 'cap']].merge(ht1[['seq', 'ht1']],
                                        on='seq', how='outer').merge(ht2[['seq', 'ht2']],
                                                                     on='seq', how='outer')

        # remove homopolymers
        # filter by homopolymers
        max_homopolymer = 7
        aff['max.nt.count'] = [max({i: s.count(i) for i in set(s)}.values()) for s in aff['seq']]
        aff = aff[aff['max.nt.count'] < max_homopolymer]


        # replace ht1 and ht2 by the best 8mers scores
        affinities_by_kmer_ht1, affinities_by_kmer_ht2 = {}, {}

        htselex_kmer_len = 8
        for affinities_by_kmer, next_ht in zip([affinities_by_kmer_ht1, affinities_by_kmer_ht2], [ht1, ht2]):
            all = []
            for si in range(0, len(list(next_ht['seq'][0])) - htselex_kmer_len + 1):
                next_ht['kmer'] = next_ht['seq'].str[si: si + htselex_kmer_len]
                all.append(next_ht[['kmer', 'affinity']])
            all = pd.concat(all).reset_index(drop=True)
            next_affinities_by_kmer = DataFrameAnalyzer.get_dict(all.groupby('kmer').mean().reset_index(), 'kmer', 'affinity')
            for next_k in next_affinities_by_kmer:
                affinities_by_kmer[next_k] = next_affinities_by_kmer[next_k]

        # aff = aff[aff['seq'].str.contains('ACAAACAG') | aff['seq'].str.contains('GTAAACAG')]

        print 'setting ht1...'
        scores = pd.DataFrame()
        for si in range(0, len(list(aff['seq'])[0]) - 8 + 1):
            print si
            scores[si] = aff['seq'].str[si:si + 8].map(affinities_by_kmer_ht1)
        aff['ht1'] = scores.max(axis=1)

        print 'setting ht2...'
        scores = pd.DataFrame()
        for si in range(0, len(list(aff['seq'])[0]) - 8 + 1):
            print si
            scores[si] = aff['seq'].str[si:si + 8].map(affinities_by_kmer_ht2)
        aff['ht2'] = scores.max(axis=1)

        aff['err.1'] = np.abs(np.where(~np.isnan(aff['cap']), aff['cap'], np.mean(aff['cap'])) -
                              np.where(~np.isnan(aff['ht1']), aff['ht1'], np.mean(aff['ht1'])))
        aff['err.2'] = np.abs(np.where(~np.isnan(aff['cap']), aff['cap'], np.mean(aff['cap'])) -
                              np.where(~np.isnan(aff['ht2']), aff['ht2'], np.mean(aff['ht2'])))
        aff = aff[['seq', 'cap', 'ht1', 'ht2', 'err.1', 'err.2']]

        aff['score'] = aff['cap'] / aff[['ht1', 'ht2']].max(axis=1)
        aff['z.score'] = (aff['score'] - np.mean(aff['score'])) / np.std(aff['score'])

        aff['z.score.1'] = aff['cap'] / aff['ht1']
        aff['z.score.1'] = (aff['z.score.1'] - np.mean(aff['z.score.1'])) / np.std(aff['z.score.1'])

        aff['z.score.2'] = aff['cap'] / aff['ht2']
        aff['z.score.2'] = (aff['z.score.2'] - np.mean(aff['z.score.2'])) / np.std(aff['z.score.2'])

        return aff


    @staticmethod
    def score_multiple_models(sequences, models,
                              starts, lengths, shape_output_dir=None,
                              run_dnashapeR=True, labels=None, debug_df=None):
        '''

        Align and score long sequence with multiple models(
        (Useful to assess effects of TF-TF interactions and independent effects
        :param sequences: it provides a set of input sequences to score
        :param models_list: it provides a set of models to score
        :param start_positions: start position to align the model in the sequence
        :return:
        '''
        from lib.SELEX.SELEXAnalyzer import SELEXAnalyzer
        from lib.ShapeAnalyzer import ShapeAnalyzer

        scores_by_model = []
        seqs_by_model = []
        for i, model in enumerate(models):
            next_shape_dir = join(shape_output_dir, str(i))

            next_start = starts[i]
            next_length =lengths[i]
            print next_start, next_length
            if not exists(next_shape_dir):
                mkdir(next_shape_dir)

            # print sequences
            if next_start is None:
                htselex = SELEXAnalyzer()
                next_seqs = htselex.mapped_motifs_in_sequences(sequences, next_length)
                print next_seqs
            else:
                next_seqs = [s[next_start: next_start + next_length] for s in
                             (sequences['seq']
                              if isinstance(sequences, pd.DataFrame) else sequences)]

            shape_prefix = join(next_shape_dir, str(i)).replace("\\", '/')
            shape_analyzer = ShapeAnalyzer()

            # print next_seqs
            print labels[i]
            print next_seqs
            # if labels[i] == 'FOXO1_TTCAGC20NTA':
            #     exit()

            print len(next_seqs)
            X = shape_analyzer.get_X_1mer_shape_flanks(next_seqs['seq'] if isinstance(next_seqs, pd.DataFrame) else next_seqs,
                                                       shape_prefix,
                                                       run_dnashapeR=run_dnashapeR,
                                                       use_config_file=True,
                                                       normalize=False)


            print len(next_seqs)
            print X.shape
            print X[[c for c in X.columns if c.startswith('MGW.')]].head()

            if debug_df is not None:
                for c in X.columns:
                    if abs(sum(X[c]) - sum(debug_df[c])) > 0.00001:
                        print c, list(X[c])
                        print list(debug_df[c])
                        print sum(X[c]), sum(debug_df[c]), abs(sum(X[c]) - sum(debug_df[c])) > 0.00001
                        assert abs(sum(X[c]) - sum(debug_df[c])) > 0.00001
                        # print X[[c]]
                        # print debug_df[[c]]

                DataFrameAnalyzer.to_tsv(X, "../../data/test1.tsv")
                DataFrameAnalyzer.to_tsv(debug_df, "../../data/test2.tsv")
                # exit()
                # print debug_df[[c for c in debug_df.columns if c.startswith('MGW.')]].head()

            # print X.shape
            # print X.sum(axis=1)


            print X.head()
            next_scores = model.predict(X)
            # print model.predict(X.tail(1))
            # print model.predict(X.head(1))
            scores_by_model.append(next_scores)
            seqs_by_model.append(next_seqs)

            print next_seqs
            print next_scores
            next_seqs['score'] = next_scores
            print next_seqs.sort_values('score', ascending=False)

        res = pd.DataFrame()
        res['seqs.by.model'] = sequences
        print res.head()
        print labels

        print scores_by_model

        for i in range(len(scores_by_model)):
            # res['seq.' + str(i)] = seqs_by_model[i]
            print len(scores_by_model[i])
            res['seq.' + labels[i]] = seqs_by_model[i]
            res['score.' + labels[i]] = scores_by_model[i]

        return res

    @staticmethod
    def plot_scatter_zscores(zscores, output_path, xlab='xlab', ylab='ylab', colorLow='gray', title='title',
                             colorHigh='red', colorMid='white', colorMax=1.0, midPoint=0.5, format='png'):
        from lib.RFacade import RFacade
        zscores['x'] = zscores['err.1']
        zscores['y'] = zscores['err.2']
        if not 'color' in zscores:
            zscores['color'] = zscores['cap']

        plot_df = zscores[['seq', 'x', 'y', 'color']]  # .head(1000)
        # make all the z-scores which are less than zero to low-threshold at zero
        plot_df['size'] = np.where(zscores['z.score'] > 0, zscores['z.score'], 0)

        plot_df['seq.k'] = np.where(plot_df['seq'].apply(SequenceMethods.get_complementary_seq) > plot_df['seq'],
                                    plot_df['seq'],
                                    plot_df['seq'].apply(SequenceMethods.get_complementary_seq))
        plot_df = plot_df.drop_duplicates('seq.k')

        plot_df = plot_df.sort_values('color', ascending=False)
        plot_df = plot_df.sort_values('color', ascending=True)
        print plot_df.shape
        print 'next output path'
        print output_path

        RFacade.plot_scatter_ggrepel(plot_df, output_path, size_min=0.001, size_max=4, w=4, h=5,
                                     breaks_color_range=[0.0, midPoint, colorMax], colorMin=0.0, colorMax=colorMax,
                                     xlab=xlab, midpoint=midPoint,
                                     ylab=ylab,
                                     xmin=0.0, xmax=1.0, ymin=0.0, ymax=1.0,
                                     title=title,
                                     title_size=15, format=format,
                                     colorLow=colorLow, colorHigh=colorHigh, colorMid=colorMid,
                                     show_fill_guide=False,
                                     size_breaks=[0.01, 1, 6, 10], size_breaks_labs=['0.01', '1', '5', '>10'],
                                     add_sizes_bar=True, size_lab='Z-score', color_lab='CAP-SELEX\n[relative aff.]')