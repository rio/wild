'''
Created on Nov 29, 2013

@author: ignacio
'''
from Atom import Atom

class AtomFactory(object):
    '''
    It creates an Atom from a log file
    '''

    @staticmethod
    def GetAtom(s, keep_pdb_data=False):
        """
        it returns an atom from a PDB line
        """
        pdb_atom = Atom()
        d = -1;

        if s.startswith("HETATM"):
            pdb_atom.set_het_atm(True)
        if (len(s) >= 11):
            pdb_atom.set_serial(int(s[7 + d: 7 + d + 5].strip()))
        if (len(s) >= 16):
            pdb_atom.set_name(s[13 + d: 13 + d + 4].strip());
        if (len(s) >= 16):
            pdb_atom.set_alt_loc(s[17 + d]);
        if (len(s) >= 38):
            pdb_atom.set_x(float(s[31 + d: 31 + d + 8].strip()));
        if (len(s) >= 46):
            pdb_atom.set_y(float(s[39 + d: 39 + d + 8].strip()));
        if (len(s) >= 54):
            pdb_atom.set_z(float(s[47 + d: 47 + d + 8].strip()));
        if (len(s) >= 60):
            pdb_atom.set_occupancy(float(s[55 + d: 55 + d + 6]));
        if (len(s) >= 66):
            pdb_atom.set_temp_factor(float(s[61 + d: 61 + d + 6]));
        if (len(s) >= 78):
            pdb_atom.set_element(s[77 + d: 77 + d + 2].strip());
        if (len(s) >= 80):
            pdb_atom.set_charge(s[79 + d: 79 + d + 2].strip());

        if keep_pdb_data:
            pdb_atom.pdb_line = s

        return pdb_atom;

    @staticmethod
    def GetAtomPDIdbAnalyzer(pdbid_analyzer_line):
        """
        it returns an atom from a PDIdb log
        ResName    ResIndex    ChainID    AtmName    Element    
        AtmIndex    Coord_x    Coord_y    Coord_z
        """

        atom = Atom()

        # basic properties
        atom.set_name(pdbid_analyzer_line[3])

        # parent residue
        atom.parent_name = pdbid_analyzer_line[0]
        atom.res_seq = pdbid_analyzer_line[1]
        atom.chain_id = pdbid_analyzer_line[2]
        atom.element = pdbid_analyzer_line[4]

        # set coordinates
        atom.set_x(float(pdbid_analyzer_line[6]))
        atom.set_y(float(pdbid_analyzer_line[7]))
        atom.set_z(float(pdbid_analyzer_line[8]))

        return atom

    @staticmethod
    def ResName(s):
        return s[17:21].strip();
    @staticmethod
    def Serial(s):
        return s[6:11].strip();
    @staticmethod
    def ResSeq(s):
        return s[22:26].strip();
    @staticmethod
    def ICode(s):
        return s[26];
    @staticmethod
    def ChainID(s):
        return s[21];
