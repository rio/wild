'''
Created on Nov 29, 2013

@author: ignacio
'''
from Residue import Residue


class Chain(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''

        self.exceptions = list()
        self.parent = None
        self.chain_id = ""
        self.molecule_type = ""

        # This table is used for quick referencing
        # of the residues during the input reading
        self.residues_dic = {}

        # This list is used for sorting of the residues and structural analysis
        self.residues_list = list()

        # string -> int
        self.residue_counter = {}

        self.last_key = None
        self.first_key = None

    def get_molecule_type(self):
        return self.molecule_type

    def set_molecule_type(self, molecule_type):
        self.molecule_type = molecule_type

    def get_chain_as_str(self, convert_weird_res_to_x):
        output = ""

        for i in range(0, len(self.residues_list)):
            res = self.residues_list[i]
            if(self.is_dna_residue(res.get_name())):
                output += res.get_name()
            else:
                output += "X"
        return output

    def is_dna_chain(self):
        return len(self.residues_list[0].name) < 3

    def is_protein_chain(self):
        return not self.is_dna_chain()

    def get_pdb_id(self):
        return self.parent.get_pdb_id()

    def cut(self, ini, end):
        new_res_list = list()

        for i in range(ini, end):
            new_res_list.append(self.residues_list[i])
        new_chain = Chain()
        new_chain.set_residues_list(new_chain)
        return new_chain

    def cut_using_pdb_numbers(self, ini, end):
        """
        it tries to cut the pdb chain using the ResSeq information
        """
        new_res_list = list()

        for r in self.residues_list:
            if r.res_seq >= ini and r.res_seq <= end:
                new_res_list.append(r)
        new_chain = Chain()
        new_chain.set_residues_list(new_res_list)
        return new_chain

    def set_residues_list(self, res_list):
        self.residues_list = res_list

    def is_dna_residue(self, res_name):
        return (res_name.compareTo("DA") == 0 or
                res_name.compareTo("DC") == 0 or
                res_name.compareTo("DG") == 0 or
                res_name.compareTo("DT") == 0 or
                res_name.compareTo("A") == 0 or
                res_name.compareTo("C") == 0 or
                res_name.compareTo("G") == 0 or
                res_name.compareTo("T") == 0)

    def get_residues_dic(self):
        return self.residues_dic

    def contains_residue(self, res_name):
        return self.residues_dic.has_key(res_name)

    def get_residues_list(self):
        return self.residues_list

    def get_idx_of(self, res):
        return self.residues_list.index(res)

    def is_last_residue(self, res):
        return self.residues_list[-1] == res

    def is_first_residue(self, res):
        return self.residues_list[0] == res

    def get_residue_at(self, i):
        return self.residues_list[i]

    def get_residue_num(self):
        return len(self.residues_list)

    def sort_residues(self):
        sorted(self.residues_list)

    def get_chain_id(self):
        return self.chain_id

    def set_chain_id(self, value):
        self.chain_id = value

    def get_pdb_id1(self):
        return self.parent.get_pdb_id()

    def set_parent(self, value):
        self.parent = value

    def get_atoms_list(self):
        atoms = list()
        for res in self.residues_list:
            atoms = atoms + res.get_atoms_list()
        return atoms

    def set_first_key(self, curr):
        if(self.first_key == None):
            self.first_key = curr
        else:
            if(curr not in self.first_key):
                self.first_key = curr

    def set_last_key(self, curr):
        if(self.last_key == None):
            self.last_key = curr
        else:
            if(curr not in self.last_key):
                self.last_key = curr

    def to_str_as_sequence(self):
        output = ""
        for i in range(0, len(self.residues_list)):
            output += self.residues_list[i].get_name()
        return output

    def add_exception(self, e):
        self.exceptions.append(e)

    def show_info(self, tab_num):
        tabs = ""
        for i in range(0, tab_num):
            tabs += "\t"

        print tabs + "CHAIN ID: " + self.get_chain_id() + "\t" + self.get_molecule_type() + "\t" + str(self.get_residue_num())

        for i in range(0, len(self.residues_list)):
            self.residues_list[i].show_info(tab_num + 1)

    def add_residue(self, residue):
        key = residue.get_res_seq() + residue.get_icode()
        key = key.strip()

        if(self.residues_dic.has_key(key)):
            self.set_last_key(key)
            self.set_first_key(key)
            residue.set_parent(self)
            self.residues_dic[key] = residue
            self.residues_list.append(residue)

    def add_atom(self, chain, atom, res_seq, icode, res_name):
        key = res_seq + icode
        key = key.strip()

        # create new residue
        if not key in self.residues_dic:
            self.set_last_key(key)
            self.set_first_key(key)

            res = Residue(res_name)
            res.parent = chain
            atom.set_parent(res)

            res.set_icode(icode)
            res.res_seq = int(res_seq)
            res.add_atom(atom)

            self.residues_dic[key] = res
            self.residues_list.append(res)
        else:
            curr_res_name = self.residues_dic[key].get_name()
            next_res_name = res_name.upper()

            if not curr_res_name in next_res_name:
                res = self.residues_dic[key]
                res.parent = chain
                res.add_atom(atom)

                # throw exception REPEATED RES SEQ
                # throw new RepeatedResSeqException(Integer.toString(atm.getSerial()));

            res = self.residues_dic[key]
            atom.set_parent(res)
            res.set_icode(icode)
            res.res_seq = int(res_seq)
            res.add_atom(atom)

