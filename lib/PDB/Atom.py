import math
import numpy


class Atom():
    def __init__(self):
        """Initialize most important parameters with a default value"""
        self.__parent = None
        self.__is_het_atm = False
        self.__serial = -1
        self.__name = None
        self.__chain_id = None
        self.__res_seq = None
        self.__parent_name = None
        self.__alt_loc = -1
        self.__x = -1
        self.__y = -1
        self.__z = -1
        self.__pdb_line = None
        self.__occupancy = -1
        self.__temp_factor = -1
        self.__element = None
        self.__charge = -1
        self.__energy = -1
        self.__is_melo_feytmans = False
        self.__melo_feytmans_atom_type = -1

    def get_pdb_line(self):
        return self.__pdb_line

    def set_pdb_line(self, value):
        self.__pdb_line = value

    def get_parent(self):
        return self.__parent

    def get_is_het_atm(self):
        return self.__is_het_atm

    def get_serial(self):
        return self.__serial

    def get_name(self):
        return self.__name
    
    def get_chain_id(self):
        return self.__chain_id

    def get_res_seq(self):
        return self.__res_seq

    def get_parent_name(self):
        return self.__parent_name

    def get_alt_loc(self):
        return self.__alt_loc

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def get_z(self):
        return self.__z

    def get_occupancy(self):
        return self.__occupancy

    def get_temp_factor(self):
        return self.__temp_factor

    def get_element(self):
        return self.__element

    def get_charge(self):
        return self.__charge

    def get_energy(self):
        return self.__energy

    def get_is_melo_feytmans(self):
        return self.__is_melo_feytmans

    def get_melo_feytmans_atom_type(self):
        return self.__melo_feytmans_atom_type

    def set_parent(self, value):
        self.__parent = value

    def set_is_het_atm(self, value):
        self.__is_het_atm = value

    def set_serial(self, value):
        self.__serial = value

    def set_name(self, value):
        self.__name = value

    def set_chain_id(self, value):
        self.__chain_id = value

    def set_res_seq(self, value):
        self.__res_seq = value

    def set_parent_name(self, value):
        self.__parent_name = value

    def set_alt_loc(self, value):
        self.__alt_loc = value

    def set_x(self, value):
        self.__x = value

    def set_y(self, value):
        self.__y = value

    def set_z(self, value):
        self.__z = value

    def set_occupancy(self, value):
        self.__occupancy = value

    def set_temp_factor(self, value):
        self.__temp_factor = value

    def set_element(self, value):
        self.__element = value

    def set_charge(self, value):
        self.__charge = value

    def set_energy(self, value):
        self.__energy = value

    def set_is_melo_feytmans(self, value):
        self.__is_melo_feytmans = value

    def set_melo_feytmans_atom_type(self, value):
        self.__melo_feytmans_atom_type = value

    def get_info_elements(self):
        """
        it returns a tuple with several elements that represent the atom
        information
        """
        return (self.serial, self.get_chain_id(),
                1 if self.is_first_residue() else 0,
                1 if self.is_last_residue() else 0,
                self.get_res_seq(), self.get_res_name(), self.get_name())

    def get_info_headers(self, separator="\t", suffix=""):
        """
        the headers are equivalent to get_info_elements tuple. If one method
        is modified, update this string tuple 
        """
        values = ("SERIAL", "CHAIN ID", "FIRST RES", "LAST RES",
                  "RESSEQ", "RES NAME", "ATOM NAME")
        values = [v + suffix for v in values]
        return values
    
    def is_backbone_atom(self):
        """
        It returns True if the name of the atom is equivalent to a backbone atom
        """
        backbone_names = ("C", "N", "CA", "O")
        return sum([1 if self.name == b else 0 for b in backbone_names]) > 0

    def is_het_atm(self):
        return self.is_het_atm

    def get_pdb_id(self):
        return self.parent.get_pdb_id()

    def get_res_name(self):
        return self.parent.get_name()

    def set_het_atm(self, value):
        self.is_het_atm = value

    def is_melo_feytmans(self):
        return self.is_melo_feytmans

    def set_melo_feytmans(self, value):
        self.is_melo_feytmans = value

    def to_str_modeller(self):
        return self.name + ":" + self.parent.get_res_seq() + ":" + self.parent.get_chain_id()

    def to_str_table_format(self):
        return self.parent.get_chain_id() + ":" + self.parent.get_res_seq() + ":" + self.parent.get_name() + ":" + self.name;

    def to_str_short(self):
        return self.get_serial() + ", " + self.get_name()

    def radians_2_angle(self, rad):
        return rad * 180 / math.pi

    def get_distance(self, other_atm, fast_pow=False, fast_pow_cut_off=None):
        if fast_pow:
            if fast_pow_cut_off is None:
                return ((self.x - other_atm.get_x()) ** 2 +
                        (self.y - other_atm.get_y()) ** 2 +
                        (self.z - other_atm.get_z()) ** 2)

        return numpy.sqrt(numpy.sum(((self.x - other_atm.get_x()) ** 2,
                                    (self.y - other_atm.get_y()) ** 2,
                                    (self.z - other_atm.get_z()) ** 2)))

    def is_close(self, other, upper_cut_off):
        """
        the method tells if the three manhattan distances (X, Y, Z)
        are lower than the given cut off
        """

        return (abs(self.x - other.get_x()) < upper_cut_off and
                abs(self.y - other.get_y()) < upper_cut_off and
                abs(self.z - other.get_z()) < upper_cut_off)

    def is_first_residue(self):
        return self.parent.is_first_residue()

    def is_last_residue(self):
        return self.parent.is_last_residue()

    def compare_to(self, other_atm):
        return self.serial - other_atm.get_serial()

    chain_id = property(get_chain_id, set_chain_id)
    res_seq = property(get_res_seq, set_res_seq)
    parent_name = property(get_parent_name, set_parent_name)
    serial = property(get_serial, set_serial)
    parent = property(get_parent, set_parent)
    is_het_atm = property(get_is_het_atm, set_is_het_atm)
    serial = property(get_serial, set_serial)
    name = property(get_name, set_name)
    chain_id = property(get_chain_id, set_chain_id)
    res_seq = property(get_res_seq, set_res_seq)
    parent_name = property(get_parent_name, set_parent_name)
    alt_loc = property(get_alt_loc, set_alt_loc)
    x = property(get_x, set_x)
    y = property(get_y, set_y)
    z = property(get_z, set_z)
    occupancy = property(get_occupancy, set_occupancy)
    temp_factor = property(get_temp_factor, set_temp_factor)
    element = property(get_element, set_element)
    charge = property(get_charge, set_charge)
    energy = property(get_energy, set_energy)
    is_melo_feytmans = property(get_is_melo_feytmans)
    melo_feytmans_atom_type = property(get_melo_feytmans_atom_type)
    parent_name = property(get_parent_name, set_parent_name)
    res_seq = property(get_res_seq, set_res_seq)
    chain_id = property(get_chain_id, set_chain_id)
    pdb_line = property(get_pdb_line, set_pdb_line)
