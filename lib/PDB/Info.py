'''
Created on Nov 29, 2013

@author: ignacio
'''


class Info(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.pdb_id = ""
        self.name = ""
        self.file = None
        self.model_list = list()

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_file(self):
        return self.file

    def set_file(self, value):
        self.file = value

    def get_pdb_id(self):
        return self.pdb_id

    def set_pdb_id(self, pdb_id):
        self.pdb_id = pdb_id

    def get_default_pdb_chains(self):
        model = self.model_list[0]
        return model.get_chains()

    def get_chains(self, chain_ids):
        model = self.model_list[0]
        chains = list()
        for chain_id in chain_ids:
            chain = model.get_chain_with_id(chain_id)
            chains.append(chain)
        return chains

    def get_chain_range(self, chain_id, start, end):
        model = self.model_list[0]
        chains = list()
        residues = model.get_chain_with_id(chain_id).get_residues_list()
        selected_residues = []
        for r in residues:
            if r.res_seq >= start and r.res_seq<= end:
                selected_residues.append(r)
        return selected_residues
    
    def filter_selected_chains(self, chain_ids):
        """
        the method filters chains that are not present in given ids
        """
        for model in self.model_list:
            model.filter_selected_chains(chain_ids)

    def get_dna_chains(self):
        def_model = self.get_model_list_at(0)
        return def_model.get_dna_chains()

    def get_protein_chains(self):
        def_model = self.get_model_list_at(0)
        return def_model.get_protein_chains()

    def get_default_pdb_chain_at(self, idx):
        chains = self.get_default_pdb_chains()
        return chains.get(idx)

    def get_model_list(self):
        return self.model_list

    def get_model_list_at(self, idx):
        return self.model_list[idx]

    def get_model_list_size(self):
        return len(self.model_list)

    def set_model_list(self, model_list):
        self.model_list = model_list

    def add_model(self, model):
        self.model_list.append(model)

    def show_info(self):
        print "MODELS INFO"
        for i in range(0, len(self.model_list)):
            model = self.model_list[i]
            model.show_info(1)

    def set_distance(self, atm_serial_1, atm_serial_2, distance):
        self.model_list[0].set_interaction_distance(atm_serial_1,
                                                    atm_serial_2, distance)

    def get_distance(self, atm_serial_1, atm_serial_2):
        return self.model_list[0].get_interaction_distance(atm_serial_1,
                                                           atm_serial_2)
