'''
Created on 3/11/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.MyGeneAnalyzer import MyGeneAnalyzer

class HOCOMOCOAnalyzer():

    @staticmethod
    def get_ensembl_by_model(species='human'):
        if species == 'human':
            p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data/motifs_hocomoco/human/HOCOMOCOv11_full_annotation_HUMAN_mono_w_ensembl.tsv'
            df = DataFrameAnalyzer.read_tsv(p)

            myt1l_names = ["Myt1l_200-623_primary", "Myt1l_200-623_secondary",
                           "Myt1l_488-910_primary", "Myt1l_488-910_secondary",
                           "Myt1l_488-910_tertiary", "Myt1l_896-1042_primary"]
            # print ensg.head()
            for myt1l_name in myt1l_names:
                df = df[['Model', 'Transcription factor',
                             'EntrezGene', 'UniProt ID', 'UniProt AC',
                             'ensg']].append(pd.DataFrame([[myt1l_name, 'Myt1l', 23040, 'MYT1L_HUMAN', 'Q9UL68',
                                                            'ENSG00000186487']],
                                                          columns=['Model', 'Transcription factor',
                                                                   'EntrezGene', 'UniProt ID', 'UniProt AC',
                                                                   'ensg']))

            rest_names = ["REST"]
            # print ensg.head()
            for rest_name in rest_names:
                df = df[['Model', 'Transcription factor',
                             'EntrezGene', 'UniProt ID', 'UniProt AC',
                             'ensg']].append(pd.DataFrame([[rest_name, 'REST', 5978, 'REST_HUMAN', 'Q13127',
                                                            'ENSG00000084093']],
                                                          columns=['Model', 'Transcription factor',
                                                                   'EntrezGene', 'UniProt ID', 'UniProt AC',
                                                                   'ensg']))

            ensembl_by_symbol = MyGeneAnalyzer.get_ensembl_by_symbol(df['Transcription factor'])

            df['ensg'] = np.where(df['Transcription factor'].isin(ensembl_by_symbol) &
                                  ~pd.isnull(df['Transcription factor'].map(ensembl_by_symbol)),
                                  df['Transcription factor'].map(ensembl_by_symbol),
                                  df['ensg'])

            # try using the uniprot id
            print('getting ensembl by symbol name...')
            ensembl_by_symbol = MyGeneAnalyzer.get_ensembl_by_symbol(df['Transcription factor'])
            df['ensg'] = np.where(pd.isnull(df['ensg']),
                                  df['Transcription factor'].map(ensembl_by_symbol), df['ensg'])

            symbol_by_ensembl = DataFrameAnalyzer.get_dict(df, 'Model', 'ensg')


            # update specific terms MyGene has not updated
            symbol_by_ensembl['HSFY1_HUMAN.H11MO.0.D'] = 'ENSG00000172468'
            symbol_by_ensembl['ZNF8_HUMAN.H11MO.0.C'] = 'ENSG00000278129'


            return symbol_by_ensembl
            # get ids by ensembl



