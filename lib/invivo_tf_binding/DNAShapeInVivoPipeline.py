

from lib.utils import *
from lib.FastaAnalyzer import FastaAnalyzer
from lib.BiasAwayAnalyzer import BiasAwayAnalyzer
from lib.SequenceMethods import SequenceMethods
from lib.MotifConverter import MotifConverter
from lib.GradientBoostingOptimizer import GradientBoostingOptimizer
from lib.MachineLearning import MachineLearning
from lib.DNAshapedTFBS.argparsing import *

class DNAShapeInVivoPipeline:

    @staticmethod
    def prepare_train_n_test_sequences(fg_id, fg_bed_path, bias_away_dir,
                                       n_kfold=10, fasta_path=None, kfold_i=None,
                                       use_small_bg=False, genome='hg19',
                                       query_bg_fasta_tpm=None,
                                       bg_input_fasta=None,
                                       **kwargs):

        print kwargs.get('overwrite', True)


        code = "hg19"

        fg_fasta_path = None
        if fasta_path is None:
            fg_fasta_path = tempfile.mkstemp()[1]
        else:
            fg_fasta_path = fasta_path

        fasta_analyzer = FastaAnalyzer()
        print exists(fg_bed_path), fg_bed_path

        if True or not exists(fg_fasta_path):
            fasta_analyzer.convert_bed_to_fasta(fg_bed_path,
                                                fg_fasta_path, genome=genome)

        # prepare an input path
        train_test_dir = join(bias_away_dir, fg_id)


        # we shuffle the fasta file, to avoid peaks sorted by genomic location
        shuffled_fg_fasta_path = tempfile.mkstemp()[1]

        fasta_analyzer.scrambled_fasta_order(fg_fasta_path,
                                             shuffled_fg_fasta_path,
                                             random_seed=500)

        print 'fg fasta', fg_fasta_path
        print 'scrambled entries fasta', shuffled_fg_fasta_path


        print kwargs.get('overwrite', True)
        print kwargs.get('overwrite', True)

        print exists(train_test_dir)

        if kwargs.get('overwrite', True) or not exists(train_test_dir):
            print 'running BiasAway...'
            print train_test_dir

            if not exists(train_test_dir):
                makedirs(train_test_dir)

            if not exists(join(train_test_dir, 'foreground')):
                mkdir(join(train_test_dir, 'foreground'))
            if not exists(join(train_test_dir, 'background')):
                mkdir(join(train_test_dir, 'background'))
            bias_away_analyzer = BiasAwayAnalyzer()

            if bg_input_fasta is None:
                bg_input_fasta = '/scratch/rio/wgEncodeCrgMapabilityAlign36mer_100bp.fa'

            print 'use test bg set:', use_small_bg
            if use_small_bg:
                print 'preparing a cover of the bg fasta file...'

                print bg_input_fasta
                bg_fasta_tmp = tempfile.mkstemp()[1] if query_bg_fasta_tpm is None else query_bg_fasta_tpm
                print 'bg file size:', os.path.getsize(bg_fasta_tmp)

                if os.path.getsize(bg_fasta_tmp) == 0:
                    bg_fastas = fasta_analyzer.get_fastas_from_file(bg_input_fasta,
                                                                    stop_at=500000)
                    fasta_analyzer.write_fasta_from_sequences(bg_fastas[:500000],
                                                              bg_fasta_tmp)

                else:
                    print 'skipping file generation because it was generated in previous iteration...'

                    bg_input_fasta = bg_fasta_tmp
            print bg_input_fasta

            # biasaway_input_fasta = '/tmp/wgEncodeCrgMapabilityAlign36mer_100bp.fa'
            tmp_bias_away_input_path = join('/tmp',
                                            basename(bg_input_fasta)) \
                if not 'TMPDIR' in os.environ else join(
                os.environ['TMPDIR'], basename(bg_input_fasta))

            if not exists(tmp_bias_away_input_path):
                print 'copying tmp'
                copy2(bg_input_fasta, tmp_bias_away_input_path)
                print 'done'
            else:
                print 'bg file already exists'

            print tmp_bias_away_input_path
            # biasaway_input_fasta = "/g/scb2/zaugg/rio/data/biasAway_bg_invivo/wgEncodeCrgMapabilityAlign36mer_100bp.fa"

            bias_away_analyzer.split_sequences_into_training_testing_batches(
                shuffled_fg_fasta_path,
                tmp_bias_away_input_path,
                n_kfold,
                train_test_dir,
                query_ki=kfold_i,
                overwrite=False)
        else:
            print 'option 2'
            for f in listdir(join(train_test_dir, 'foreground')):
                if '.bed' in f:
                    continue
                fg_path = join(train_test_dir, 'foreground', f)
                bg_path = join(train_test_dir, 'background', f)
                print fg_path, exists(fg_path)
                print bg_path, exists(bg_path)
                fg_seqs, bg_seqs = [
                    [s[1] for s in fasta_analyzer.get_fastas_from_file(p)]
                    for p in [fg_path, bg_path]]
                fg_gc_content, \
                bg_gc_content = [
                    [SequenceMethods.get_gc_content(s) for s in fg_seqs],
                    [SequenceMethods.get_gc_content(s) for s in bg_seqs]]
                print basename(fg_path), exists(fg_path), exists(
                    bg_path), len(fg_seqs), len(bg_seqs)
                print pearsonr(sorted(fg_gc_content), sorted(bg_gc_content))

                # print i, fg_id, 'BiasAway already exists...'


    @staticmethod
    def prepare_dimo_motifs(fg_id, bias_away_dir, motif_path, output_dir, kfold_i=None,
                            **kwargs):
        code = "hg19"
        assert kfold_i is not None

        kfold_i = str(kfold_i)
        print 'next fg.id', fg_id
        train_test_dir = join(bias_away_dir, fg_id)
        bench_group = 'train'
        dimo_working_dir = join(train_test_dir, 'dimo_analysis',
                                bench_group + "_" + kfold_i)

        homer_tmp_path = join(dimo_working_dir,
                              basename(motif_path).replace(".pfm",
                                                           '_homer.ppm'))
        print homer_tmp_path

        ppm_id = basename(homer_tmp_path).replace('.ppm', "").replace(
            ".pfm", "")
        output_path = join(dimo_working_dir, ppm_id + "_optimized.ppm")
        print output_path

        fg_fasta = join(train_test_dir, 'foreground',
                        bench_group + "_" + kfold_i + ".fa")
        bg_fasta = join(train_test_dir, 'background',
                        bench_group + "_" + kfold_i + ".fa")

        print 'fg', fg_fasta
        print 'bg:', bg_fasta

        if not exists(fg_fasta) or not exists(bg_fasta):
            return
            assert exists(fg_fasta) and exists(bg_fasta)

        print dimo_working_dir

        if not exists(dimo_working_dir):
            makedirs(dimo_working_dir)

        # prepare a dimo input path accordingly to this case
        motif_converter = MotifConverter()
        motif_converter.convert_jaspar_pfm_to_homer_ppm(motif_path,
                                                        homer_tmp_path)
        gbo = GradientBoostingOptimizer()

        fasta_analyzer = FastaAnalyzer()
        seqs_pos = [s[1] for s in fasta_analyzer.get_fastas(fg_fasta)]
        seqs_neg = [s[1] for s in fasta_analyzer.get_fastas(bg_fasta)]


        seqs_pos = seqs_pos[:2000]
        seqs_neg = seqs_neg[:2000]

        print '# pos', len(seqs_pos)
        print '# neg', len(seqs_neg)


        motif_len = pd.read_csv(homer_tmp_path, skiprows=1, sep='\t', header=None).shape[0]

        print 'current motif length: %i' % motif_len
        seqs_pos, seqs_neg = [[s for s in seqs if s >= motif_len] for seqs in seqs_pos, seqs_neg]

        optimized_jaspar_path = gbo.optimize_homer_pwm_with_dimo(seqs_pos, seqs_neg,
                                                                 homer_tmp_path,
                                                                 dimo_working_dir,
                                                                 output_path,
                                                                 **kwargs)

    @staticmethod
    def train_n_test(fg_id, output_dir, code='hg19', job_id=None, tmp_files_dir=None, run_shape=True,
                     train_test_dir=None, **kwargs):

        # print 'train/test'

        fg_fasta_dir = "../../data/fastas"

        # if not exists(big_wig_dir):
        # big_wig_dir = '/g/scb2/zaugg/rio/data/shape_data_gbshape'
        big_wig_dir = '/scratch/rio/zaugglab/shape_data_bgshape'

        shapes = ['HelT', 'ProT', 'MGW', 'Roll', 'HelT2', 'ProT2', 'MGW2',
                  'Roll2']
        bigwigs = [join(big_wig_dir, code + "." + k + ".wig.bw") for k in
                   ["HelT", "MGW", "ProT", "Roll", "HelT.2nd", "MGW.2nd",
                    "ProT.2nd", "Roll.2nd"]]
        helt, mgw, prot, roll, helt2, mgw2, prot2, roll2 = bigwigs

        # fg_id, motif_id, bg_id, kfold_i = r.values.tolist()
        for kfold_i in range(10):

            query_kfoldi = kwargs.get('query_kfoldi', None)


            if query_kfoldi != kfold_i:
                continue
            print query_kfoldi, kfold_i, query_kfoldi == kfold_i

            kfold_i = str(kfold_i)

            print 'next ki', kfold_i

            tmp_files_dir = join(output_dir, fg_id) if \
                tmp_files_dir is None else tmp_files_dir

            classifiers_dir = join(tmp_files_dir, 'classifiers')
            scores_dir = join(tmp_files_dir, 'scores')
            perf_delta_dir = join(tmp_files_dir, 'performance_deltas')

            for next_dir in [classifiers_dir, scores_dir, perf_delta_dir]:
                print next_dir
                if not exists(next_dir):
                    makedirs(next_dir)

            print 'next fg.id', fg_id, kfold_i

            if train_test_dir is None:
                train_test_dir = join(output_dir, 'bias_away_bg', fg_id)

            fg_fasta_train, fg_bed_train,\
            fg_fasta_test, fg_bed_test,\
            bg_fasta_train, bg_bed_train,\
            bg_fasta_test, bg_bed_test = [join(train_test_dir, bench_group,
                                               ml_group + '_' + str(kfold_i) + "." + ext)
                                              for bench_group in ['foreground', 'background'] for ml_group in ['train', 'test']
                                          for ext in 'fa', 'bed']

            # assertion: all fasta and bed files must exist and shall be not empty
            reject = False
            # print fg_fasta_train
            # print fg_fasta_test
            # print bg_fasta_train
            # print bg_fasta_test


            for p in [fg_fasta_train, fg_bed_train, fg_fasta_test, fg_bed_test,
                      bg_fasta_train, bg_bed_train, bg_fasta_test, bg_bed_test]:
                if exists(p):
                    print p
                    lines = [line for line in open(p)]
                    if len(lines) <= 1:
                        print 'reject ML step...'
                        print 'file seems to be emtpy. Please check'
                        print p
                        print len(lines)
                        reject = True
                else:
                    print 'file does not exist...'
                    print p
                    reject = True

            print 'reject?', reject
            if reject:
                continue

            query_motif_path = kwargs.get('query_motif_path', None)
            # print query_motif_path


            if query_motif_path is None:
                # prepare motif path
                dimo_working_dir = join(train_test_dir, 'dimo_analysis', 'train_' + kfold_i)
                dimo_motif_path = join(dimo_working_dir, "output_TF_END.pfm")
                print dimo_working_dir
                dimo_motif_path_start = [join(dimo_working_dir, f) for f in listdir(dimo_working_dir)
                                         if f.endswith("_homer.ppm")]
                if len(dimo_motif_path_start) > 0:
                    dimo_motif_path_start = dimo_motif_path_start[0]
                else:
                    dimo_motif_path_start = None

                print dimo_motif_path


                jaspar_path = dimo_motif_path.replace(".pfm", ".jaspar")
                print dimo_motif_path_start

                motif_id = None
                motif_id_containing_files = [f for f in listdir(dimo_working_dir) if "_homer.ppm" in f]
                if len(motif_id_containing_files) > 0:
                    motif_id = motif_id_containing_files[0].replace("_homer.ppm", "")
                else:
                    motif_id = 'motif.id'

                # generate a motif using as reference the output or the initial PFM from dimo
                p = dimo_motif_path if exists(dimo_motif_path) else dimo_motif_path_start

                if p is not None and exists(p):
                    print 'reading', p
                    motif_ppm = pd.read_csv(p, skiprows=1,
                                            sep=' ' if exists(dimo_motif_path) else '\t',
                                            header=None)
                    print motif_ppm
                    print motif_ppm[motif_ppm.columns[2:]]
                    motif_ppm.columns = [i for i in range(motif_ppm.shape[1])]
                    motif_ppm[motif_ppm.columns[2:]].to_csv(jaspar_path, index=None, header=None, sep=' ')
                else:
                    jaspar_path = kwargs.get('motif_path', None)


                print jaspar_path
                print 'motif path', jaspar_path

                if not exists(jaspar_path):
                    print 'reject: motif path does not exist...'
                    print jaspar_path
                    continue
            else:
                jaspar_path = query_motif_path
                motif_id = basename(query_motif_path).replace(".jaspar", "")


            perf_output_dir = join(output_dir, fg_id, 'performances')
            matrices_dir = join(output_dir, fg_id, 'scores')

            if not exists(perf_output_dir):
                mkdir(perf_output_dir)

            for model in ['1mer', '1mer+shape']:
                print model
                if model == '1mer+shape' and not run_shape:
                    continue

                if kwargs.get('output_basename', None) is None:
                    output_path = join(perf_output_dir, kfold_i + "_" + model + '.tsv.gz')
                else:
                    output_path = join(perf_output_dir, kfold_i + "_" +
                                       kwargs.get('output_basename', None) + '.tsv.gz')

                if not exists(output_path) or kwargs.get('overwrite', False):
                    print 'output path exists...'
                    print output_path

                    res = []
                    # fa fg and bg
                    print model, 'current kfold', kfold_i
                    classifier_path = join(classifiers_dir,
                                           fg_id + "_" + model + "_k" + str(
                                               kfold_i) + ".pkl")
                    # print classifier_path

                    if False and exists(classifier_path):
                        print 'pkl file already exists...'
                        kfold_i += 1
                        continue

                    # print fg_fasta_train, fg_fasta_test, bg_fasta_train, bg_fasta_test,\
                    #     fg_bed_train, fg_bed_test, bg_bed_train, bg_bed_test
                    # y_train, y_test = y[train], y[test]

                    # prepare indexes in each file according to the specific partition
                    # that has been provided by KFold
                    shape_paths = []
                    scores_fg_train = join(scores_dir, model + "_pos_train_" + str(
                        kfold_i) + ".tsv.gz")
                    scores_bg_train = join(scores_dir, model + "_neg_train_" + str(
                        kfold_i) + ".tsv.gz")

                    # print scores_fg_train
                    # print scores_bg_train
                    if kwargs.get('overwrite', False) or not exists(classifier_path):
                        print 'classifier path\n%s' % classifier_path

                        arguments = ("trainPSSM -f {0} " + \
                                     "-i {1} -I {2} " + \
                                     "-b {3} -B {4} " + \
                                     "-o {5} --scoresposout {6} --scoresnegout {7} " + \
                                     (
                                     "-1 {8} {9} {10} {11} " if model == '1mer+shape' else "") + \
                                     (
                                     "-2 {12} {13} {14} {15} " if model == '1mer+shape' else "") +
                                     # ("" if model == '1mer+shape' else "") +
                                     "--threshold 0.0").format(jaspar_path,
                                                               fg_fasta_train,
                                                               fg_bed_train,
                                                               bg_fasta_train,
                                                               bg_bed_train,
                                                               classifier_path,
                                                               scores_fg_train,
                                                               scores_bg_train,
                                                               helt, mgw, prot,
                                                               roll,
                                                               helt2, mgw2, prot2,
                                                               roll2)

                        line = arguments.strip()
                        split = line.split(" ")
                        sys.argv = [sys.argv[0]] + split

                        print sys.argv
                        # running with these arguments
                        arguments = arg_parsing()
                        arguments.func(arguments)


                    scores_by_label = [scores_fg_train, scores_bg_train]
                    print scores_by_label

                    # score in held-out data
                    # positives
                    for lab, fa, bed in zip(['pos_test', 'neg_test'],
                                            [fg_fasta_test, bg_fasta_test],
                                            [fg_bed_test, bg_bed_test]):
                        scale = False
                        scores_path = join(scores_dir,
                                           model + "_" + lab + "_" + str(
                                               kfold_i) + ".tsv.gz")
                        if True or not exists(scores_path):
                            print 'applying classifier...'
                            arguments = ("applyPSSM -f {0} " + \
                                         "-i {1} -I {2} " + \
                                         "-c {3} -o {4} " + \
                                         (
                                         "-1 {5} {6} {7} {8} " if model == '1mer+shape' else "") + \
                                         (
                                         "-2 {9} {10} {11} {12} " if model == '1mer+shape' else "") +
                                         # "{13}" if model == '1mer+shape' else "{13}") +
                                         "--threshold 0.0").format(
                                jaspar_path,
                                fa,
                                bed,
                                classifier_path,
                                scores_path,
                                helt, mgw, prot, roll,
                                helt2, mgw2, prot2, roll2,
                                "-n" if scale else "")

                            # running with these arguments
                            line = arguments.strip()
                            split = line.split(" ")
                            sys.argv = [sys.argv[0]] + split
                            print sys.argv
                            # running with these arguments
                            arguments = arg_parsing()
                            arguments.func(arguments)
                        scores_by_label.append(scores_path)

                    # the amoutn of files here after this step has to be four
                    assert len(scores_by_label) == 4

                    scores_by_label = [p.replace(".tsv.gz", '.matrix.tsv.gz') for p in scores_by_label]
                    scores_pos_train, scores_neg_train, scores_pos_test, scores_neg_test = scores_by_label
                    # once we have the output for positives and negatives, calculate the testing performance
                    fg_train, bg_train = DataFrameAnalyzer.read_tsv_gz(scores_pos_train),\
                                         DataFrameAnalyzer.read_tsv_gz(scores_neg_train)

                    print scores_pos_test
                    print scores_neg_test
                    fg_test, bg_test = DataFrameAnalyzer.read_tsv_gz(
                        scores_pos_test), \
                                                 DataFrameAnalyzer.read_tsv_gz(
                                                     scores_neg_test)

                    y_pred = np.array(
                        list(fg_train['proba']) + list(bg_train['proba']))
                    y_test = np.array(
                        [1] * fg_train.shape[0] + [0] * bg_train.shape[0])
                    auroc, auprc = MachineLearning.get_auroc_n_auprc(y_test, y_pred)

                    res.append([fg_id, motif_id, model,
                                'train', fg_train.shape[0],
                                bg_train.shape[0],
                                kfold_i, auroc, auprc])

                    internal_validation = True
                    if internal_validation:

                        train_df =  pd.concat([fg_train[fg_train.columns[:-1]],
                                               bg_train[bg_train.columns[:-1]]])
                        test_df = pd.concat([fg_test[fg_test.columns[:-1]],
                                             bg_test[bg_test.columns[:-1]]])

                        y_train = [1] * fg_train.shape[0] + [0] * bg_train.shape[0]
                        y_test = [1] * fg_test.shape[0] + [0] * bg_test.shape[0]

                        train_clf = MachineLearning.TrainGradientBoostingClassifier(train_df,
                                                                                    y_train)
                        train_auroc, train_auprc = MachineLearning.assess_model(train_clf,
                                                                                train_df,
                                                                                y_train)
                        print train_auroc, train_auprc
                        test_auroc, test_auprc = MachineLearning.assess_model(train_clf,
                                                                            test_df,
                                                                            y_test)

                        print test_auroc, test_auprc

                    y_pred = np.array(
                        list(fg_test['proba']) + list(bg_test['proba']))
                    y_test = np.array(
                        [1] * fg_test.shape[0] + [0] * bg_test.shape[0])
                    auroc, auprc = MachineLearning.get_auroc_n_auprc(y_test, y_pred)

                    res.append([fg_id, motif_id, model,
                                'test', fg_test.shape[0],
                                bg_test.shape[0],
                                kfold_i, auroc, auprc])

                    res = pd.DataFrame(res, columns=['dataset.id', 'motif.id', 'model',
                                                     'benchmark.group',
                                                     'n.pos', 'n.neg', 'kfold.i', 'auroc',
                                                     'auprc'])
                    print res
                    if kwargs.get('overwrite', False) or not exists(output_path):
                        DataFrameAnalyzer.to_tsv_gz(res, output_path)
                        print 'output written to...'
                        print output_path


                    # return res


                    # Delta AUC -> these are calculated by position
                    if model == '1mer+shape':
                        perf_delta_path = join(perf_delta_dir, kfold_i + ".tsv.gz")
                        train_pos, train_neg,\
                        test_pos, test_neg = [DataFrameAnalyzer.read_tsv_gz(join(matrices_dir,
                                                                                model + "_" +
                                                                                k2 + "_" + k1 + "_" +
                                                                                str(kfold_i) + ".matrix.tsv.gz"))
                                              for k1 in ['train', 'test'] for k2 in ['pos', 'neg']]

                        # remove shape features by position
                        print train_pos.shape
                        n_positions = (train_pos.shape[1] - 2) / 8
                        assert (train_pos.shape[1] - 2) % 8 == 0

                        # compare ref classifier with classifier that adds shape features
                        # in each position
                        shape_cols = list(train_pos.columns[1:-1])
                        print shape_cols
                        deltas_table = []


                        for pi in range(n_positions):

                            shape_cols_pi = [c for i, c in enumerate(shape_cols) if i % n_positions  == pi]
                            print pi, shape_cols_pi

                            aug_train_df = pd.concat([train_pos[['feature.0'] + shape_cols_pi],
                                                  train_neg[['feature.0'] + shape_cols_pi]])
                            aug_test_df = pd.concat([test_pos[['feature.0'] + shape_cols_pi],
                                                 test_neg[['feature.0'] + shape_cols_pi]])
                            ref_train_df = pd.concat([train_pos[['feature.0']], train_neg[['feature.0']]])
                            ref_test_df = pd.concat([test_pos[['feature.0']], test_neg[['feature.0']]])
                            y_train = [1] * train_pos.shape[0] + [0] * train_neg.shape[0]

                            y_test = [1] * test_pos.shape[0] + [0] * test_neg.shape[0]

                            ref_clf = MachineLearning.TrainGradientBoostingClassifier(ref_train_df, y_train)
                            ref_auroc, ref_auprc = MachineLearning.assess_model(ref_clf, ref_test_df, y_test)

                            aug_clf = MachineLearning.TrainGradientBoostingClassifier(aug_train_df, y_train)
                            aug_auroc, aug_auprc = MachineLearning.assess_model(aug_clf, aug_test_df, y_test)
                            deltas_table.append([kfold_i, pi, len(y_train), len(y_test),
                                                 ref_auroc, aug_auroc, ref_auprc, aug_auprc])

                        for t in deltas_table:
                            print t
                        delta_res = pd.DataFrame(deltas_table,
                                                 columns=['k', 'position', 'n.train', 'n.test',
                                                          'auroc.ref', 'auroc.aug', 'auprc.ref', 'auprc.aug'])
                        DataFrameAnalyzer.to_tsv_gz(delta_res, perf_delta_path)
                        print perf_delta_path

                        matrices_dir = join(output_dir, fg_id, 'scores')
