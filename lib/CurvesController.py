'''
Created on Jan 21, 2013

@author: ignacio
'''
from os.path import join, abspath, exists
import os
from lib.utils import *

from os import chdir
# from modeller import *
from os import system
from shutil import copy

class CurvesController():
    def __init__(self):
        self.curves_dir = "/home/rio/Software/curves+"
        if not exists(self.curves_dir):
            self.curves_dir = "/home/ignacio/Software/curves+"

    def run(self):
        # Calculate minor and major grooves in all structures (testing + training)
        self.analyze_with_curves(self.all_structures_dir)
        # Calculate groove width values in models
        self.analyze_with_curves(self.models_dir, is_modeller_model=True)

    def run_curves(self, pdb_code, chain_a_range=None, chain_b_range=None,
                   curves_dir=None):
        # EXECUTE CURVES
        curves_dir = '/g/scb2/zaugg/zaugg_shared/Programs/curves' if curves_dir is None else curves_dir
        curves_bin = join(curves_dir, 'Cur+')
        curves_cmd = curves_bin + " <<!\n"
        input_cmd = " &inp file=" + pdb_code + ", lis=" + pdb_code + ",\n"
        lib_cmd = " lib=" + join(curves_dir, 'standard') + ",&end\n"
        ranges_cmd = "2 1 -1 0 0\n" + chain_a_range + "\n" + chain_b_range + "\n!\n"
        full_cmd = curves_cmd + input_cmd + lib_cmd + ranges_cmd
        print(full_cmd)

        lis_path = pdb_code + ".lis"
        cda_path = pdb_code + ".cda"

        print('about to execute Curves...')
        print(pdb_code + ".lis", exists(pdb_code + ".lis"))
        if not exists(lis_path):
            if exists(lis_path):
                remove(lis_path)
            if exists(cda_path):
                remove(cda_path)

            print(full_cmd)
            system(full_cmd)
        # get the groove table

    # goes inside each dir analyzing the corresponding .pdb structure
    def analyze(self, structures_dir, is_modeller_model=False, queries=None, plot=True):
        system("export KMP_AFFINITY=disabled")
        return_dir = abspath(".")
        startdir = abspath(structures_dir)
        chdir(startdir)

        groove_tables_dic = {}
        for dirname, dirnames, filenames in os.walk(startdir):
            for filename in filenames:
                code = filename.split(".")[0]
                if queries is not None and not code in queries:
                    continue

                if not filename.endswith(".ent") and not filename.endswith('.pdb'):
                    continue
                if '_X.' in filename:
                    continue

                if "curves" in filename:
                    continue

                print(filename)
                splitted = filename.split(".")

                if is_modeller_model:
                    pdb_code = filename
                else:
                    pdb_code = splitted[0]

                pdb_path = join(startdir, filename)
                # get the sequences to map the indexes to extract
                ali_file_path = os.path.join(dirname, filename) + ".ali"
                sequences = None
                print(ali_file_path)
                if not exists(ali_file_path):
                    self.create_alignment_file(pdb_path, ali_file_path)

                if exists(ali_file_path):
                    sequences = self.get_seqs(ali_file_path)[0:2]

                # get pdb_code


                # calculate lens and indexes.
                # It is assumed that all chains have the same length and are perfect complements
                chn_a_len, chn_b_len = len(sequences[0]), len(sequences[1])
                chn_a_start, chn_a_end = 1, chn_a_len
                chn_b_start, chn_b_end = chn_a_len + 1, chn_a_len + chn_b_len

                curves_chn_a_range = str(chn_a_start) + ":" + str(chn_a_end)
                curves_chn_b_range = str(chn_b_end) + ":" + str(chn_b_start)

                # get inside the curr dir
                print('changing location to', dirname)
                chdir(dirname)
                # delete all the older curves files
                # system("rm curves*")

                pdb_path = filename.replace(".ent", ".pdb")
                print(pdb_path)
                if not exists(pdb_path):
                    # remove(pdb_path)
                    copy(filename, pdb_path)

                # EXECUTE CURVES
                curves_cmd = join(self.curves_dir, "Cur+") + " <<!\n"
                input_cmd = " &inp file=" + pdb_code + ", lis=" + pdb_code + ",\n"
                lib_cmd = " lib=" + join(self.curves_dir, "standard") + "\n &end\n"
                ranges_cmd = "2 1 -1 0 0\n" + curves_chn_a_range + "\n" + curves_chn_b_range + "\n!\n"
                full_cmd = curves_cmd + input_cmd + lib_cmd + ranges_cmd
                print(full_cmd)

                lis_path = pdb_code + ".lis"
                cda_path = pdb_code + ".cda"

                print('about to execute Curves...')
                print(pdb_code + ".lis", exists(pdb_code + ".lis"))
                if not exists(lis_path):
                    if exists(lis_path):
                        remove(lis_path)
                    if exists(cda_path):
                        remove(cda_path)

                    system(full_cmd)
                # get the groove table

                groove_table = self.get_groove_table(lis_path)
                groove_tables_dic[pdb_code] = groove_table

                if plot:
                    # create figures
                    print('creating grooves graph')
                    self.create_grooves_graph(groove_table, pdb_code)
                else:
                    continue
                    # return groove_table

        # END go back to the initial dir
        chdir(return_dir)
        # print the dictionary
        self.print_groove_tables_dic(groove_tables_dic)

    def print_groove_tables_dic(self, groove_tables_dic):
        # save groove tables to a final file
        writer = open("groove_tables", "w")
        for key in list(groove_tables_dic.keys()):
            groove_table = groove_tables_dic[key]
            # write the pdb_id
            writer.write(key + "\n")
            # write the table
            [ writer.write(row + "\n") for row in groove_table ]
        writer.close()

    def get_groove_table(self, path):
        reader = open(path)
        groove_tables = list()

        table_found = False
        for line in reader:
            line = line.rstrip()
            if table_found:
                groove_tables.append(line)
            if "  (E) Groove parameters" in line:
                table_found = True
        reader.close()
        return groove_tables

    def get_seqs(self, alignmentFile):
        seqs = list()
        seqs = [s.strip() for s in open(alignmentFile).readlines()[3:]]
        seqs = "".join(seqs)
        seqs = seqs.replace("*", "")
        seqs = seqs.split("/")
        return seqs

    def create_alignment_file(self, pdb_path, output_path):
        """
        It takes a PDB file and convert it into an aligment file
        """
        # log.verbose()

        env = environ()

        env.libs.topology.read(file='$(LIB)/top_heav.lib')

        # Read structure
        m = model(env, file=pdb_path)

        # Append sequence
        aln = alignment(env)
        aln.append_model(m, atom_files=pdb_path, align_codes='all')

        # Write alignment file
        aln.write(file=output_path, alignment_format='PIR')


    def get_float_from(self, line, lastidx):
        """
        it reads the lines declared in the groove table,
        returning a float if the number is found,
        and None is the line was too short (i.e. number does not exist)
        """
        if len(line) < lastidx:
            return None

        cols = line[lastidx - 5:lastidx]

        if "     " in cols:
            return None

        return float(cols)

    def get_char_from(self, line, idx):
        if len(line) >= idx + 1:
            if " " in line[idx]:
                return None
            return line[idx]
        return None

    def get_int_from(self, line, idx):
        if len(line) < idx:
            return None
        cols = line[idx - 2:idx]

        # empty cols
        if "  " in cols:
            return None
        return int(cols)

    def get_lists_from_pair_list(self, pair_list, option="both"):
        x = list()
        y = list()
        print(pair_list)
        for curr_x, nt, curr_y in pair_list:
            x.append(curr_x)
            y.append(curr_y)
        if "both" in option:
            return x, y
        if "x" in option:
            return x
        if "y" in option:
            return y

    def get_grooves_df_from_lis_path(self, lis_path):
        groove_table = self.get_groove_table(lis_path)
        w12_pair_list = list()
        d12_pair_list = list()
        w21_pair_list = list()
        d21_pair_list = list()
        nucleotide_list = list()
        table = []
        for line in groove_table:
            # line = line.rstrip()

            # line is first row: continue
            if ("   Level           W12     D12     W21     D21" in line or
                len(line.strip()) == 0):
                continue

            level = self.get_float_from(line, 7)
            nucleotide = self.get_char_from(line, 9)

            base = 14
            offset = 8
            w12 = self.get_float_from(line, base + offset * 1)
            d12 = self.get_float_from(line, base + offset * 2)
            w21 = self.get_float_from(line, base + offset * 3)
            d21 = self.get_float_from(line, base + offset * 4)

            table.append([level, nucleotide, w12, d12, w21, d21])
            if level is not None: w12_pair_list.append((level, nucleotide, w12))
            if level is not None: d12_pair_list.append((level, nucleotide, d12))
            if level is not None: w21_pair_list.append((level, nucleotide, w21))
            if level is not None: d21_pair_list.append((level, nucleotide, d21))
            if nucleotide is not None: nucleotide_list.append(nucleotide)
        df = pd.DataFrame(table, columns=['level', 'nt', 'w12', 'd12', 'w21', 'd21'])
        return df

    # it creates a minor-major groove width table (x axis)
    def create_grooves_graph(self, groove_table, pdb_id):
        print(pdb_id)

        w12_pair_list = list()
        d12_pair_list = list()
        w21_pair_list = list()
        d21_pair_list = list()
        nucleotide_list = list()
        print(groove_table)
        for line in groove_table:
            # line = line.rstrip()

            print(line)
            # line is first row: continue
            if ("   Level           W12     D12     W21     D21" in line or
                len(line.strip()) == 0):
                continue

            level = self.get_float_from(line, 7)
            nucleotide = self.get_char_from(line, 9)

            base = 14
            offset = 8
            w12 = self.get_float_from(line, base + offset * 1)
            d12 = self.get_float_from(line, base + offset * 2)
            w21 = self.get_float_from(line, base + offset * 3)
            d21 = self.get_float_from(line, base + offset * 4)

            print("values at", level, nucleotide, w12, d12, w21, d21)

            print("MGW", w12)
            if level is not None: w12_pair_list.append((level, nucleotide, w12))
            if level is not None: d12_pair_list.append((level, nucleotide, d12))
            if level is not None: w21_pair_list.append((level, nucleotide, w21))
            if level is not None: d21_pair_list.append((level, nucleotide, d21))
            if nucleotide is not None: nucleotide_list.append(nucleotide)

        print(w12_pair_list)
        print(w21_pair_list)
        if len(w12_pair_list) != 0 and len(w21_pair_list) != 0:
            w12_x = self.get_lists_from_pair_list(w12_pair_list, "x")
            w12_y = self.get_lists_from_pair_list(w12_pair_list, "y")
            w21_x = self.get_lists_from_pair_list(w21_pair_list, "x")
            w21_y = self.get_lists_from_pair_list(w21_pair_list, "y")
            d21_x = self.get_lists_from_pair_list(d21_pair_list, "x")
            d21_y = self.get_lists_from_pair_list(d21_pair_list, "y")
            
            d12_x = self.get_lists_from_pair_list(d12_pair_list, "x")
            d12_y = self.get_lists_from_pair_list(d12_pair_list, "y")
            nt_21 = [[t[0], t[1]] for t in w21_pair_list if t[1] != None]
            print(w12_x)
            print(w12_y)
            print(w21_x)
            print(w21_y)
            (color1, color2) = ('g', 'r')
            (leg1, leg2) = ("major GW", "MGW")
            # define which is minor groove and which is major groove
            for i in w12_x:
                for j in w21_y:
                    if i > j:
                        break
                    else:
                        (tmp1, tmp2) = (w12_x, w12_y)
                        (w12_x, w12_y) = (w21_x, w21_y)
                        (w21_x, w21_y) = (tmp1, tmp2)
                        break

            print('here..')


            # major groove
            print(w12_x)
            print(w12_y)
            print(color1)

            sns.set_style('white')
            fig = plt.figure(figsize=(7, 4))
            # plt.subplot(1, 1, 1)
            print('here...')
            # plt.plot(w12_x, w12_y, color=color1, lw=1.5)
            # plt.scatter(w12_x, w12_y, s=70, color=color1)


            # minor groove
            x, y = w12_x, w12_y
            print(y)
            w12_avr = [yi for yi in w12_y if yi != None]
            print(w12_avr)
            # w12_avr = [(w12_avr[i - 1] + w12_avr[i] + w12_avr[i]) / 3 for i in range(1, 19, 2)]
            w12_avr = [w12_avr[i] for i in range(1, 19, 2)]
            print(w12_avr, len(w12_avr))
            print("\t".join(map(str, y)))
            from scipy.stats import spearmanr

            plt.plot(x, y, color=color2, lw=1.5)
            plt.scatter(x, y, s=70, color=color2)

            # plt.plot(w12_x, w12_y, color='blue', lw=1.5)
            # plt.scatter(w12_x, w12_y, s=70, color='blue')

            # plt.plot(d12_x, d12_y, color='green', lw=1.5)
            # plt.scatter(d12_x, d12_y, s=70, color='green')

            plt.axhline(6.7, color='gray', linestyle='--')
            plt.title("")
            plt.xlabel("position", fontsize=18)
            plt.ylabel("Minor Groove Width [" + "$\AA$" + "]", fontsize=18)

            print(w12_y)

            plt.xticks([x[0] for x in nt_21], [x[1] for x in nt_21])
            plt.xlim([-0.5, 18.5])
            plt.grid(False)
            plt.legend([leg2], loc="lower left")
            # plt.show()
            # plt.legend([leg2], loc="lower left")

            print(nucleotide_list)
            # plt.xticks(range(2, len(nucleotide_list) + 2), nucleotide_list)
            print('here..')

            # ax = plt.axes()
            # ax.set_xlim(1, len(nucleotide_list) + 2)
            # ax.set_ylim(0, 15)

            plt.tight_layout()
            print(abspath(pdb_id))
            savefig(pdb_id)

            plt.close()


