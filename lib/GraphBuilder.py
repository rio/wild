

import pandas as pd
import matplotlib.pyplot as plt
class GraphBuilder:


    def plot_performance_scatter(self, df, x_name, y_name, ax=None):
        '''
        To compare performance values from different approaches in the same plot
        :param df:
        :param x_name:
        :param y_name:
        :param ax:
        :return:
        '''
        df.plot(x=x_name, y=y_name, kind='scatter', ax=ax)
        ax.set_aspect(1)
        plt.xlim([0.5, 1.0])
        plt.ylim([0.5, 1.0])
        plt.plot([0, 1], [0, 1])