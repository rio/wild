'''
Created on Aug 12, 2015

@author: ignacio
'''
import gzip


class PDBReader():

    @staticmethod
    def get_chains(pdb_path, is_gzip=False, omit_ca=True):
        """
        get all the chains that are protein chains in this PDB

        Omit CA makes the method ignore chains that have only CA atoms
        """
        residue_names = {"ALA", "CYS", "ASP", "GLU", "PHE",
                         "GLY", "HIS", "ILE", "LYS", "LEU",
                         "MET", "ASN", "PRO", "GLN", "ARG",
                         "SER", "THR", "VAL", "TRP", "TRY"}
        chains = set()
        for r in (open(pdb_path) if not is_gzip else gzip.open(pdb_path)):
            if r.startswith("ATOM"):
                atm_name = r[13:15]
                if omit_ca and atm_name == "CA":
                    continue
                res = r[17:20]
                if res in residue_names and len(res.strip()) == 3:
                    chains.add(r[21])
        return chains

    @staticmethod
    def get_title(pdb_path):
        for r in gzip.open(pdb_path):
            if r.startswith("TITLE"):
                return r[6:].strip()
        raise Exception("TITLE header not foud in path " + pdb_path)

    @staticmethod
    def get_molecule_name(pdb_path, chain_id, is_gzip=True):
        """
        Get chain data: molecule name, synonyms
        """
        section_found = False
        rows = []

        # collect lines with COMPND segment
        for r in (open(pdb_path) if not is_gzip else gzip.open(pdb_path)):
            if r.startswith("COMPND"):
                section_found = True
            if section_found:
                if r.startswith("COMPND"):
                    rows.append(r)
                else:
                    break
        assert len(rows) > 0

        # check for the line that has the chain IDs

        last_molecule_line = None
        for r, i in zip(rows, list(range(len(rows)))):
            if "MOLECULE:" in r:
                last_molecule_line = r
            if "CHAIN:" in r and " " + chain_id in r[17:]:
                assert last_molecule_line is not None
                return last_molecule_line[21:].strip()[:-1]
        raise Exception("MOLECULE NAME not found in " + pdb_path)

