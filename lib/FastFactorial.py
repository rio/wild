'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
import numpy as np
import operator
import math
from functools import reduce
class FastFactorial:

    def __init__(self):
        self.d = {}


    def factorial(self, n, thr=50000, debug=False):
        res = None
        if not n in self.d:
            if len(self.d) > 0:
                # here we attempt to calculate factorial using the closest element in the dictionary
                best = sorted([[xi - n, xi] for xi in self.d], key=lambda x: abs(x[0]))[0]
                best_diff, best_n = best
                # print 'closest element', best, best_diff, thr, abs(best_diff) < thr
                if abs(best_diff) < thr:
                    # print('calculating factorial of', n, 'using', best_n)
                    if best_n > n: # e.g fact(100) =  fact(105) / prod(101..105)
                        res = self.d[best_n] / reduce(operator.mul, list(range(n + 1, best_n + 1)))
                    else: # e.g fact(11) = fact(10) * prod(10, 11)
                        res = self.d[best_n] * reduce(operator.mul, list(range(best_n + 1, n + 1)))

                    self.d[n] = res
                else:
                    res = math.factorial(n)
                    self.d[n] = res
            else:
                res = math.factorial(n)
                self.d[n] = res
        res = self.d[n]

        if debug:
            assert res == math.factorial(n)
        return res