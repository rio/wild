'''
Created on 12/28/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.path_functions import *

class NotesFacade():

    @staticmethod
    def get_pardir(p):
        return os.path.abspath(os.path.join(p, os.pardir))

    @staticmethod
    def get_notes_directory():
        curdir = abspath(os.curdir)
        done = False
        next = curdir
        n_shots = 3
        while not done and n_shots > 0:
            next = NotesFacade.get_pardir(next)
            print((join(next, 'data')))
            if not exists(join(next, 'data')):
                n_shots -= 1
                continue
            else:
                done = True
        if not done:
            print('error. Please check if data is located as a parent somewhere')
        else:
            print((exists(join(next, 'data')), join(next, 'data')))
            if not exists(join(next, 'data', 'notes')):
                makedirs(join(next, 'data', 'notes'))
            return join(next, 'data', 'notes')

    @staticmethod
    def write(object, basename):
        directory = NotesFacade.get_notes_directory()
        if isinstance(object, pd.DataFrame):
            DataFrameAnalyzer.to_tsv(object, join(directory, basename + ".tsv"), index=True)