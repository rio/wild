
# default STRING DB
library(STRINGdb)

### R code from vignette source 'STRINGdb.Rnw'
get_string_enrichments <- function(names, species=10090){
    
    print('connecting to STRING database')
    string_db <- STRINGdb$new(version="10", species=species,
                              score_threshold=0, input_directory="")
    print('connection successful...')
    names <- toupper(names)
    df <- data.frame(gene=names)
    ###################################################
    ### code chunk number 5: map
    ###################################################
    print('mapping names to database...')
    mapped <- string_db$map( df, "gene", removeUnmappedRows = TRUE )
    
    hits <- mapped$STRING_id
    
    datalist = list()
    print('calculating enrichments...')
    i <- 1
    for(category in c("Process", "Component", "Function", "KEGG", "Pfam", "InterPro")){
        print(category)
        next_enrichments <- string_db$get_enrichment( hits, category = category, methodMT = "fdr", iea = TRUE)
        next_enrichments$category <- category
        datalist[[i]] <- next_enrichments # add it to your list
        i = i + 1
    }
    res = do.call(rbind, datalist)
    return(res)
}
