

# this function was imported into RFacade


#######################################
## Extract a set of SNP flanking sites from HG19
#######################################
suppressMessages(library(biomaRt))
suppressMessages(library('BSgenome.Hsapiens.UCSC.hg19'))
# Convert a set of SNP IDs into a table with chromosome number and position

##############
# the method works with GRCh37
#############
getSNPCoordinates <- function(snp_ids=c("rs1520218")){
    
    snp_mart = useMart("ENSEMBL_MART_SNP", dataset="hsapiens_snp", host="grch37.ensembl.org")
    # snp_mart = useMart("snp",dataset="hsapiens_snp")
    snp_attributes = c("refsnp_id", "chr_name", "chrom_start")
    
    print(snp_ids[1:5])
    snp_locations = getBM(attributes=snp_attributes, filters="snp_filter", 
                          values=snp_ids, mart=snp_mart)
    return(snp_locations)
}

# args = commandArgs(trailingOnly=TRUE)
# input_path = args[1]
# output_path = args[2]
# snps <- read.table(file=input_path, header=FALSE)
# convert table and write output
# df = getSNPCoordinates(snps)
# write.table(df, file=output_path, row.names=FALSE, quote=FALSE)
