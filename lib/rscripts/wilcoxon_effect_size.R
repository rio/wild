# Title     : TODO
# Objective : TODO
# Created by: ignacio
# Created on: 8/4/2019




rFromWilcox<-function(a, b, alternative='g')
{
    N <- length(a) + length(b)
    wilcoxModel <- wilcox.test(a, b, alternative=alternative)
    z <- qnorm(wilcoxModel$p.value)
    print(c('Z=', z))
    r <- z / sqrt(N)
    cat(wilcoxModel$data.name,"Effect Size, r=",r)
    return(r)
}