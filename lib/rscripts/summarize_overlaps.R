
print('loading input libraries...')
print('Rsamtools')
suppressMessages(library("Rsamtools"))
print('genefilter')
suppressMessages(library('genefilter'))
print('GenomicFeatures')
suppressMessages(library("GenomicFeatures"))
print('GenomicAlignments')
suppressMessages(library("GenomicAlignments"))
print('Biocparallel')
suppressMessages(library("BiocParallel"))
print('DESeq2')
suppressMessages(library("DESeq2"))
print('argparse')
suppressMessages(library("argparse", quietly=TRUE))
print('loading libraries completed...')
p <- ArgumentParser()
p$add_argument("--outdir", default='')
p$add_argument("--samples", default='')
p$add_argument('--species', default='mouse')

args <- p$parse_args()
species <- as.character(args$species)

# this step must be modified in case of annotating with other species
if(grepl('mouse', species)){
    gtffile <- "../../data/Mus_musculus.GRCm38.88.gtf"
    library(TxDb.Mmusculus.UCSC.mm10.ensGene)
    txdb <- TxDb.Mmusculus.UCSC.mm10.ensGene
}

print(c('species?', species))
print(c('next time', time))
output_directory = as.character(args$outdir)
print(output_directory)
output_path = paste0(output_directory, '/counts.Rds')

sampleTable <- read.csv(as.character(args$samples), sep="\t")
print(sampleTable)
filenames <- file.path(as.character(sampleTable$symlink))

print('filenames....')
print(filenames)
print(file.exists(filenames))
# verify summary
bamfiles <- BamFileList(filenames, yieldSize=2000000)
print('n bamfiles...')
length(bamfiles)
print(length(sampleTable$SampleID))
print(sampleTable$SampleID)

# update the names of bamfiles (avoid duplicates)
# names(bamfiles) <- as.vector(paste0(as.character(1:length(bamfiles)), '.bam'))
names(bamfiles) <- sampleTable$symlink
print(names(bamfiles))
print(length(names(bamfiles)))
print(length(bamfiles))
ebg <- exonsBy(txdb, by="gene")
se_path = NULL
se_path = file.path(output_path)
print(output_path)
print(c('exists?', file.exists(output_path)))
if(!file.exists(se_path) || TRUE){
    register(MulticoreParam(workers=20))
    print('mapping files...')
    # The following call creates the SummarizedExperiment object with counts:
    se <- summarizeOverlaps(features=ebg, reads=bamfiles,
                            mode="Union",
                            singleEnd=FALSE,
                            ignore.strand=TRUE,
                            fragments=TRUE )
    saveRDS(se, file=se_path)
    print('saving output...')
}