

names(geneList)

class(geneList)

class(alg)

fg <- as.character(read.table('/tmp/fg.tsv', header=F)$V1)
bg <- as.character(read.table('/tmp/bg.tsv', header=F)$V1)

fg_values <- as.vector(rep(1, length(fg)))
names(fg_values) <- fg

bg_values <- as.vector(rep(0, length(bg)))
names(bg_values) <- bg

head(fg_values)
head(bg_values)

allGenes <- c(fg_values, bg_values)
head(allGenes)
tail(allGenes)
class(allGenes)
length(allGenes)

gene_names <- c('ENSMUSG00000007721', 'ENSMUSG00000029467', 'ENSMUSG00000021938', 'ENSMUSG00000028691')
names(values) <- gene_names
values
tgd <- new("topGOdata", 
                    description = "Simple session", ontology = "BP",
                    allGenes = allGenes,
                    # allGenes = geneList,
                    geneSel = function(x) { return(x == 1)},
                    nodeSize=5,
                    annot=annFUN.org, mapping="org.Mm.eg.db", ID = "ENSEMBL" )


attributes(tgd)
allGO = genesInTerm(tgd)
## run tests
resultTopGO.elim <- runTest(tgd, algorithm = "elim", statistic = "Fisher")
resultTopGO.classic <- runTest(tgd, algorithm = "classic", statistic = "Fisher" )
## look at results
results <- GenTable( tgd, Fisher.elim = resultTopGO.elim, 
                     Fisher.classic = resultTopGO.classic,
                     orderBy = "Fisher.elim", topNodes = length(allGO))
head(results)
