


library(biomaRt)

function <- getENSG(input_vector)
# define biomart object
mart <- useMart(biomart = "ensembl", dataset = "hsapiens_gene_ensembl")
# query biomart

df = read.table("/home/rio/data/string/unique.ids.gz", sep=' ')
df['V1']
results <- getBM(attributes = c("ensembl_gene_id", "ensembl_transcript_id", "ensembl_peptide_id", 'external_gene_name'),
                 filters = "ensembl_peptide_id", values = df['V1'],
                 mart = mart)
nrow(results)
write.csv(results, file="/home/rio/data/string/ensp_to_gene_name.tsv", quote=FALSE)