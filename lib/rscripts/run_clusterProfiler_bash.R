print('importing clusterProfiler...')
library("clusterProfiler")
print('importing org.Hs...')
library(org.Hs.eg.db)
print('importing org.Ms...')
library(org.Mm.eg.db)
print('importing argparse...')
library("argparse", quietly=TRUE)

run_clusterProfilerGO <- function(fg, bg, debug=F, species='human', ont='ALL',
                                  remove_geneid=TRUE){
    print('calculating enrichments...')
    org <- ifelse(grepl(species, "human"), "org.Hs.eg.db", "org.Mm.eg.db")
    print(length(unique(fg)))
    print(length(unique(bg)))
    stopifnot(length(intersect(fg, bg)) == 0)
    print(species)
    enrich <- enrichGO(gene = unique(fg),
                       keyType="ENSEMBL",
                       org,
                       ont="ALL",
                       pAdjustMethod = "BH",
                       pvalueCutoff=1.0,
                       qvalueCutoff=1.0)
                       # universe = names(unique(bg))
    print('done. Summarizing')
    if(is.null(enrich))
        return(data.frame())
    print('converting into dataframe')
    df <- data.frame(enrich)
    if(remove_geneid)
        df$geneID <- NULL
    return(df)
}

# read args
p <- ArgumentParser()
p$add_argument("--fg", default=NULL)
p$add_argument("--bg", default=NULL)
p$add_argument("--species", default='mouse')
p$add_argument('--output', default=NULL)
args <- p$parse_args()

print('reading fg...')
fg <- read.table(args$fg, header=T)
# fg <- read.table('/tmp/tmpio3zEY', header=T)
print('reading bg....')
bg <- read.table(args$bg, header=T)
# bg <- read.table('/tmp/tmpH_Y8Fy', header=T)

print(args$species)
fg <- fg$X0
bg <- bg$X0
res <- run_clusterProfilerGO(unique(fg), unique(bg), species=args$species)
print(head(res))
print(class(res))
print('writing to output...')
gz = gzfile(args$output, "w")
write.table(res, gz, sep='\t', row.names = T)
close(gz)
print(args$output)
print('done...')

