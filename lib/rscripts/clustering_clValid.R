
library(clValid)

calculate_stability_measures <- function(df, kmax=10, clMethods=c('pam')){
    print('calculating clustering stability with several measures...')
    # clMethods <- c("hierarchical","kmeans","pam")
    print(clMethods)
    intern <- clValid(df, 2:kmax, clMethods=clMethods,
                      validation="internal", maxitems = nrow(df))
    df_intern <- optimalScores(intern)
    # par(mfrow=c(2,2),mar=c(4,4,3,1))
    # plot(intern, legend=FALSE)
    # plot(nClusters(intern),measures(intern,"Dunn")[,,1],type="n",axes=F,
    #      xlab="",ylab="")
    # legend("center", clusterMethods(intern), col=1:9, lty=1:9, pch=paste(1:9))
    # par(op)
    stab <- clValid(df, 2:kmax, clMethods=clMethods,
                    validation="stability", maxitems = nrow(df))
    df_stab <- optimalScores(stab)
    return(rbind(df_intern, df_stab))
}
