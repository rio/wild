
library(rGREAT)


submitJob <- function(bed_df, species, bg=NULL, rule='basalPlusExt', category=NULL){
    # http://bejerano.stanford.edu/great/public/html/index.php
    print('submitting GREAT job to server...')
    print(dim(bed_df))
    if(!is.null(bg)){
        print('Background provided. Using it as reference set...')
    } else{
        print('Using whole genome as reference...')
    }
    job = submitGreatJob(bed_df, bg=bg, species = species, rule=rule, request_interval = 30) #  base_url = base_url)
    return(job)
}
# http://bejerano.stanford.edu/great/public/html/index.php
runGREATAnalysis <- function(bed_df, species='hg19', bg=NULL, rule='basalPlusExt', category=NULL) {
                             # base_url="http://great.stanford.edu/public/cgi-bin"){
    print(dim(bed_df))
    print(species)
    print(bg)
    job <- submitJob(bed_df, species, bg=bg, rule=rule, category=category)
    if(is.null(category))
        category <- availableCategories(job)
    print('retrieving output (takes ~5min for all categories. The fewer categories. The faster (<1m for a single category))')
    # run great with all possible categories
    tb = getEnrichmentTables(job, category=category)
    df <- do.call("rbind", tb)
    df$category <- vapply(strsplit(rownames(df), '[.]'), "[", "", 1)
    rownames(df) <- 1:nrow(df)
    return(list(as.data.frame(df), job))
}


getPeaksByOntology <- function(job, ontology = NULL, # "Disease Ontology",
                               termID = NULL, output_path='/tmp/tmp_great_output.pdf') { # "DOID:2871"){
    # print(ontology)
    # print(termID)
    print(class(job))
    pdf(output_path, w=10, h=5)
    par(mfrow = c(1, 3))
    print('calculating region-gene associations...')
    res = plotRegionGeneAssociationGraphs(job, ontology = ontology, request_interval=30,
                                          termID = termID)
    dev.off()
    return(as.data.frame(res))
}
