# Title     : TODO
# Objective : TODO
# Created by: Ignacio
# Created on: 5/31/2019



library(fgsea)

get_fgsea <- function(df, feature.name='has.feature', return.fgseaRes=F){

    print('loading data')
    data(examplePathways)

    print('preparing data')
    pathways <- examplePathways[1:1]
    print(table(df[feature.name]))
    pathways[[1]] <- unlist(as.numeric(df[df[feature.name] == 1,]$entrez))
    names(pathways) <- feature.name

    print('running fgsea...')
    fgseaRes <- fgsea(pathways = pathways,
                      stats = ranks,
                      minSize=15,
                      maxSize=10000,
                      nperm=10000)

    if(return.fgseaRes)
        return(fgseaRes)

    res <- as.data.frame(fgseaRes)[colnames(fgseaRes)[1:7]]
    print(res)
    return(res)
}

plot_fgsea <- function(df, output_path, feature.name='has.feature'){
    fgseaRes <- get_fgsea(df, feature.name=feature.name)
    es <- fgseaRes$ES[1]
    NES <- fgseaRes$NES[1]
    padj <- fgseaRes$padj[1]

    print(c(es, NES, padj))
    ranks <- df[!is.na(df$log2FoldChange),]$log2FoldChange
    names(ranks) <- df[!is.na(df$log2FoldChange),]$entrez

    print('preparing data')
    pathways <- examplePathways[1:1]
    pathways[[1]] <- unlist(as.numeric(df[df[feature.name] == 1,]$entrez))
    names(pathways) <- feature.name

    print('importing ggplot2...')
    library(ggplot2)
    pdf(paste0(output_path), w=5, h=3)
    title <- paste('ES=', format(round(es, 2), nsmall = 2),
                                 '\nNES=', format(round(NES, 2), nsmall = 2),
                                 '\nP-adj=', format(padj, format = "e", digits = 2))
    print(plotEnrichment(pathways[[feature.name]], ranks) + labs(title=title))
    print('done plotting. Closing buffer...')
    dev.off()
}