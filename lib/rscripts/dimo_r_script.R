
library(DiMO)
DiMO(
    positive.file="../data/dimo/pos.fa",
    negative.file="../data/dimo/neg.fa",
    pfm.file.name="/tmp/tmplVnOoY",
    output.flag="../data/dimo/SOX_PAX_8",
    epochs=150,
    add.at.five=0,
    add.at.three=0,
    learning.rates=seq(1,0.1,length.out=3)
)
        