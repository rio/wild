library(ggplot2)
library(ggrepel)
library(gridExtra)
# library(extrafont)

plot_performances <- function(df, x_lab, y_lab, output_path, colorMax=NULL, colorMin=NULL,
                              colorLow='blue', colorHigh='red', colorMid='gray', segment_size=1.0,
                              w=4, h=4, title='title', ymin=NULL, ymax=NULL, midpoint=NULL,
                              xmin=NULL, xmax=NULL, draw_diagonal=F, coord_fixed=F, size_min=1, size_max=7,
                              size_lab='size_lab', color_lab='color_lab', ggrepel_fontsize=4,
                              title_size=20, show_colour_guide=F, format='pdf', raster=F,
                              test=F, vert_dotted_xlim=NULL, box.padding=0.2, point.padding=0.2,
                              color_text_values=c("grey", "red"), add_sizes_bar=T, add_colorbar=T,
                              breaks_color_range=NULL, segment_color='grey50', show_fill_guide=T, xticks=NULL, yticks=NULL,
                              horiz_dotted_ylim=NULL, size_breaks=NULL, size_breaks_labs=NULL, save=T,
                              ...){
    print('running ggrepel...')
    # print('here...')
    # print(head(df))
    if(test){
        df <- read.table('/tmp/test_ggrepel.tsv.gz', sep='\t', header=T)
        # df <- data.frame(x=c(1,2,3), y=c(4,5,6), size=c(1,2,3),
        #                  color=c(1,2,3), label=as.character(c('1', '2', 'x')),
        #                  shape=c('A', 'B', 'C'), fill=df$fill)
        xmin <- min(df$x - 1)
        xmax <- max(df$x + 1)
        ymin <- min(df$y + -1)
        ymax <- max(df$y + 1)
        xticks <- NULL
        yticks <- NULL
        x_lab <- 'x.label'
        y_lab <- 'y.label'
        df$label <- as.character(df$label)
        title <- 'title'
        box.padding <- 0.2
        point.padding <- 0.2
        ggrepel_fontsize <- 4
        color_lab <- 'color_lab'
        draw_diagonal <- F
        coord_fixed <- F
        size_max <- 7
        segment_size <- 1.0
        segment_color <- 'grey50'
        add_sizes_bar <- T
        add_colorbar <- T
        title_size <- 20
        show_colour_guide <- F
        size_breaks <- c(1, 2, 5, 10, 20)
        size_breaks_labs <- unlist(lapply(size_breaks, as.character))
        size_lab <- 'size'
        size_min <- 1
        raster <- F
    }
    if(save){
        print('opening output path buffer at:')
        if(grepl(format, 'pdf')){
            pdf(output_path, width=w, height=h, useDingbats=FALSE)
        }
        if(grepl(format, 'png')){
            print(c('format chosen', 'png'))
            png(output_path, width=w, height= h, res=300, units='in')
        }

    }

    p <- NULL

    print(c('size range', size_min, size_max))

    if(!raster){
        p <- ggplot(df) + geom_point(aes(x, y, size = size, col = color, shape=shape)) + theme_bw(base_size = 16)
    } else{
        print('import ggrastr...')
        library("ggrastr")
        p <- ggplot(df) + geom_point_rast(aes(x, y, size = size, col = color, shape=shape)) + theme_bw(base_size = 16)
    }


    p <- p + theme(panel.border = element_blank()) +
        theme(axis.line = element_line(color = 'black')) +
        xlab(x_lab) + ylab(y_lab) + ggtitle(title) +
        scale_size_continuous(range = c(size_min, size_max)) +
        theme(legend.position="bottom", legend.box = "vertical", # text=element_text(family="Arial"),
              plot.title = element_text(size = title_size)) +
        labs(color = color_lab) +
        scale_fill_manual(values = c('blue', 'red'), name = 'Motif status')
    
    p <- p + geom_label_repel(aes(x, y, label=label, fill=fill), size=ggrepel_fontsize,
                              fontface = 'bold', color = 'white', segment.size=segment_size,
                              box.padding = unit(box.padding, "lines"), point.padding = unit(point.padding, "lines"),
                              segment.color = segment_color) 
    p <- p + geom_text_repel(aes(x, y, label=text), size=ggrepel_fontsize,
                              fontface = 'bold', color = 'black', segment.size=segment_size,
                              box.padding = unit(box.padding, "lines"), point.padding = unit(point.padding, "lines"),
                              segment.color = segment_color)

    print('here')
    # print(c('xticks', xticks))
    # print(c('yticks', yticks))
    if(!is.null(xmin)){
        # print(c(xmin, xmax, ymin, ymax))
        if(length(xticks) == 1)
            xticks = NULL
        if(length(yticks) == 1)
            yticks = NULL

        p <- p + scale_x_continuous(limits = c(xmin, xmax), breaks=xticks) +
            scale_y_continuous(limits = c(ymin, ymax), breaks=yticks)
    }
    
    if(!show_fill_guide){
        p <- p + guides(fill=FALSE)
    }
    
    
    # print(head(df))
    print('errors x found?')
    # print("x.std" %in% colnames(df))
    if("x.std" %in% colnames(df)){
        print('setting x err')
        p <- p + geom_errorbarh(data=df, aes(xmin = x - x.std * .5,
                                             xmax = x + x.std * .5,
                                             x=x, y = y, colour = color, height = 0, width=0)) 
    }
    if("y.std" %in% colnames(df)){
        print('setting y err')
        p <- p + geom_errorbar(data=df, aes(ymin = y - y.std * .5,
                                            ymax = y + y.std * .5,
                                            x = x, y=y,
                                            colour = color, height = 0, width=0)) 
    }
    
    # print('custom graphics')

    if(length(unique(df$shape)) == 1)
        p <- p + scale_shape(guide=F)
    if(draw_diagonal){
        print('diagonal...')
        p <- p + geom_abline(intercept = 0, slope = 1)
    }
    # print(c('# of color labels', length(unique(df$color))))
    # print(unique(df$color))
    print(length(unique(df$color)) == 2)
    if(add_colorbar && length(unique(df$color)) == 2){
        p <- p + scale_color_manual(values = color_text_values)
    }
    if(coord_fixed){
        print('fixing coord...')
        p <- p + coord_fixed()
    }
    print(c('add sizes bar', add_sizes_bar))
    if(add_sizes_bar && !is.null(size_breaks) && length(size_breaks) > 1){
        print('adding size breaks...')
        # print(test)
        # print(add_sizes_bar)
        print(size_breaks)
        print(size_breaks_labs)
        # print(c(length(size_breaks), length(size_breaks_labs)))
        # print(size_breaks)
        # print(size_breaks_labs)
        p <- p + scale_size(range = c(1, size_max), breaks = size_breaks, name=size_lab, labels=size_breaks_labs)
    } else{
        print('skipping printing breaks...')
        p <- p + guides(size=FALSE)
    }
    if(!is.null(vert_dotted_xlim)){
        print('vert...')
        p <- p + geom_hline(yintercept=c(vert_dotted_xlim),
                            linetype="dotted")
    }
    if(!is.null(horiz_dotted_ylim)){
        print('horiz...')
        p <- p + geom_vline(xintercept=c(horiz_dotted_ylim),
                            linetype="dotted")
    }
    length(size_breaks)
    length(size_breaks_labs)
    
    if(add_colorbar && !is.null(colorMax)){
        print('setting color...')
        print(c(colorLow, colorMid, colorHigh))
        print(c(colorMin, colorMax))

        if(is.null(midpoint)){
            midpoint = (colorMax - colorMin) / 2.0 + colorMin
        }
        library(scales)
        print(c('midpoint:', midpoint))
        p <- p + scale_colour_gradient2(low=colorLow, high=colorHigh, mid=colorMid,
                                        midpoint=midpoint, limits=c(colorMin, colorMax),
                                        breaks=breaks_color_range, name=color_lab, oob=squish)
    } else{
        p <- p + guides(colour=show_colour_guide)
    }
    print('saving/retrieving plot...')
    if(save){
        print(p)
        print('saved. Closing stream...')
        dev.off()
        print('stream closed...')
    } else{
        print('do not save. Return plot to caller...')
        return(p)
    }
}


plot_multiple <- function(plotList, output_path, ncol=2, nrow=2, w=5, h=5){
    # print(class(plotList))
    for(p in plotList){
        print(class(p))
    }
    print(c(ncol, nrow))
    print(c(w, h))
    print('plotting...')
    print(output_path)
    pdf(output_path, width=w, height=h)
    grid.arrange(grobs=plotList, ncol = ncol, nrow=nrow)
    # grid.arrange(plotList[[1]], plotList[[2]], plotList[[3]], ncol = ncol, nrow=nrow)
    dev.off()
}

get_blank_panel <- function(){
    blank <- grid.rect(gp=gpar(col="white"))
    return(blank)
}

