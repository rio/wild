'''
Created on 2/25/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.Plots import CirclesHeatmap
from lib.SequenceMethods import SequenceMethods
from lib.plot_utils import *
from ast import literal_eval as make_tuple
from lib.PeaksAnalyzer import PeaksAnalyzer
import argparse

# GENRE motif hits
parser = argparse.ArgumentParser(description='Plot motif coenrichments')
parser.add_argument('--coenrichments1', type=str) # mm10, hg19, hg38
parser.add_argument('--coenrichments2', type=str) # mm10, hg19, hg38
parser.add_argument('--coenrichments_list', type=str, default=None) # mm10, hg19, hg38
parser.add_argument('--labels', type=str, default=None) # mm10, hg19, hg38
parser.add_argument('--overwrite', default=False, action='store_true')
parser.add_argument('--outputbasename', type=str, required=True) # mm10, hg19, hg38
parser.add_argument('--nsigmax', type=int, help='show rows with a number of significant columns <= nsigmax(avoid high redundancy)',
                    default=20)
parser.add_argument('--width', type=float, default=11)
parser.add_argument('--height', type=float, default=15)
parser.add_argument('--padj_thr', type=float, default=5e-2)


args = parser.parse_args()
overwrite = args.overwrite
output_basename = args.outputbasename
nsigmax = args.nsigmax
w = args.width
h = args.height
padj_thr = args.padj_thr


print args.coenrichments_list
paths = [args.coenrichments1, args.coenrichments2] if args.coenrichments_list is None else args.coenrichments_list.split(",")
all = []
codes_order = []
for i, p in enumerate(paths):
    print p
    next_code = basename(p.replace(".tsv.gz", '')) if args.labels is None else args.labels.split(",")[i]
    df = DataFrameAnalyzer.read_tsv_gz(p)
    df['k'] = next_code
    codes_order.append(next_code)
    all.append(df)
res = pd.concat(all)
print res.head()
print res.tail()
res['pair.code'] = res['module.A'] + ":" + res['module.B']

plot = False
sig_labels = set()
overlapping_pairs = set()
# print res['code'].value_counts()
plots = []
for k in codes_order:
    # print k
    sel = res[res['k'] == k]
    sel['x'] = sel['or']
    sel['y'] = -np.log10(sel['p.adj.odd.ratio'])
    sel['size'] = sel['y']
    # res['color'] = 1
    sel['shape'] = 'circle'

    has_overlap_list = []
    for ri, r in sel.iterrows():
        a, b = r['top.3.dists'], r['top.3.freqs']
        if isinstance(a, float) or not '(' in a:
            has_overlap_list.append(False)
            continue

        dists, freqs = make_tuple(r['top.3.dists']), make_tuple(r['top.3.freqs'])
        overlap = abs(int(dists[0])) < 10 and int(freqs[0]) >= 30
        has_overlap_list.append(overlap)
    sel['overlap'] = has_overlap_list

    overlapping_pairs = set(sel[sel['overlap']]['pair.code']).union(set(overlapping_pairs))

    print '# discarded overlapping pairs', len(overlapping_pairs)
    sel = sel[~sel['overlap']]

    sel['label'] = np.where((sel['p.adj.odd.ratio'] < padj_thr), sel['module.A'] + ":" + sel['module.B'], '')
    sig_labels = sig_labels.union(set(sel[sel['label'].str.len() != 0]['label']))

print len(plots)

fig = plt.figure(figsize=(w, h))
res = res[res['pair.code'].str.contains('GATA')]
next = res
print next.shape
hm = next.pivot(index='pair.code', columns='k', values='or')
next['minus.log.p.adj'] = -np.log10(next['p.adj.odd.ratio'])
pvals = next.pivot(index='pair.code', columns='k', values='minus.log.p.adj')

hm = hm[hm.index.isin(sig_labels) & ~hm.index.isin(overlapping_pairs)]
pvals = pvals[pvals.index.isin(sig_labels) & ~pvals.index.isin(overlapping_pairs)]

hm = hm[codes_order]
pvals = pvals[codes_order]

# sort by total significance, highlighting by # of significant cases

hm['sig.sum'] = pvals.sum(axis=1)
pvals['sig.sum'] = pvals.sum(axis=1)
hm = hm.sort_values('sig.sum', ascending=True)
pvals = pvals.sort_values('sig.sum', ascending=True)
del hm['sig.sum']
del pvals['sig.sum']

print hm.head()
print pvals.head()

hm['n.sig'] = [sum([pi > -np.log10(0.05) for pi in r.values]) for ri, r in pvals.iterrows()]
pvals['n.sig'] = [sum([pi > -np.log10(0.05) for pi in r.values]) for ri, r in pvals.iterrows()]
hm = hm[hm['n.sig'] <= nsigmax]
pvals = pvals[pvals['n.sig'] <= nsigmax]

print hm.shape, pvals.shape

del hm['n.sig']
del pvals['n.sig']

# filter out cases that are significant just one time
# print hm.shape, pvals.shape
vmax = 10
CirclesHeatmap.make_bubble_heatmap(hm, pvals, quantAmplifier=3.5,
                                   xlab='Treatment', ylab='kmer Module pair', palette_id='Blues',
                                   heatmap_title='Module-coenrichment',
                                   vmin=0, vmax=vmax, cbar_label='-log(p.adj)',
                                   marker='o', legend_title='odds.ratio', tickscolorbar=[0, vmax/2, vmax],
                                   legends_heatmap_ticks=[1, 5, 10, 20], plot_i=0, fig=fig)

plt.tight_layout()
# plt.subplots_adjust(left=.2, bottom=0.2)
savefig(output_basename + "_" + str(nsigmax), png=False)
plt.close()
exit()