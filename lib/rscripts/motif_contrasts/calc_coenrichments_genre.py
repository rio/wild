'''
Created on 25/02/2017, 2018

@author: Ignacio Ibarra Del Rio

Coenrichment calculations for genre modules
This
Description:
'''

from lib.utils import *
from lib.plot_utils import *
from lib.RFacade import RFacade
from lib.FastaAnalyzer import FastaAnalyzer
from scipy.stats.stats import fisher_exact
from ast import literal_eval as make_tuple
from lib.Plots import CirclesHeatmap
from lib.SequenceMethods import SequenceMethods
from lib.PeaksAnalyzer import PeaksAnalyzer
import argparse

# GENRE motif hits
parser = argparse.ArgumentParser(description='Motif comparisons across databases and with different functions for two groups')
parser.add_argument('i', type=int, default=-1)
parser.add_argument('--fasta', type=str, required=False) # mm10, hg19, hg38
parser.add_argument('--outputdir', type=str, default=None) # mm10, hg19, hg38
parser.add_argument('--hitsgenre', type=str, default=None) # mm10, hg19, hg38
parser.add_argument('--onezeroestable', type=str, default=None)
parser.add_argument('--clustersfile', type=str, default=None)
parser.add_argument('--nPeaks', default=None, type=int) # mm10, hg19, hg38
parser.add_argument('--overwrite', default=False, action='store_true')
parser.add_argument('--title', default='GENRE', type=str)
parser.add_argument('--nperm_zscore', default=1, type=int)


args = parser.parse_args()

jobid = args.i
print jobid

hits_genre_path = args.hitsgenre
fasta = args.fasta
one_zeroes_table_path = args.onezeroestable
print one_zeroes_table_path

if hits_genre_path is None and one_zeroes_table_path is None:
    print 'None of the expected inputs was Given (motifs hits OR one-zeroes table. Please verify'
    exit()
else:
    if hits_genre_path is not None and fasta is None and args.nPeaks is None:
        print 'If you give a motif hits table you need to specify the amount of sequences, or give a FASTA file'
        exit()
    print 'input is good. Continue...'

overwrite = args.overwrite
npermutation_zscore = args.nperm_zscore

# RSAT clusters path
cluster_partners_by_tfid = {}
if args.clustersfile is not None:
    for r in open(args.clustersfile):
        k, motif_ids = r.strip().split("\t")
        motif_ids = set(motif_ids.split(","))
        motif_ids = {s for s in motif_ids}
        print k, motif_ids
        for next_id in motif_ids:
            assert not next_id in cluster_partners_by_tfid
            cluster_partners_by_tfid[next_id] = motif_ids
    print len(cluster_partners_by_tfid)

res, nPeaks = None, None
if hits_genre_path is not None:
    assert exists(hits_genre_path)
    headers, sequences = zip(*FastaAnalyzer.get_fastas(fasta))
    nPeaks = len(headers)
    res = DataFrameAnalyzer.read_tsv_gz(hits_genre_path, header=None)
    res.columns = ['header',  'position', 'seq', 'motif.id', 'strand', 'score']

else:
    print 'reading matrix'
    all = []
    tmp_path = one_zeroes_table_path.replace(".csv.gz", "_melted.tsv.gz")
    if not exists(tmp_path):
        res = DataFrameAnalyzer.read_tsv_gz(one_zeroes_table_path, index_col=0, sep=',')
        if nPeaks is None:
            nPeaks = res.shape[0]

        for ci, c in enumerate(res.columns):
            print ci, res.shape[1]
            next = res[[c]]
            next['coordinate'] = next.index
            next['motif.id'] = c
            next = next[next[c] == 1]

            del next[c]
            next = next.reset_index(drop=True)
            next['score'] = 1
            print c, next.shape
            if next.shape[0] == 0:
                continue
            all.append(next)
        res = pd.concat(all)
        DataFrameAnalyzer.to_tsv_gz(res, tmp_path)

    if nPeaks is None:
        nPeaks = args.nPeaks
    res = DataFrameAnalyzer.read_tsv_gz(tmp_path)
    res['motif.id'] = [k.split(".")[0] + "_HUMAN.H11MO." + ".".join(k.split(".")[1:]) for k in res['motif.id']]

print res
genes_by_module = {}


res.columns = ['header'] + list(res.columns)[1:]

hits_by_motif = {motif_id: grp for motif_id, grp in res.groupby('motif.id')}
module_ref = hits_by_motif.keys()[jobid] if jobid != -1 else None
print module_ref

print 'expected output path...'
suffix = '_coenrichments.' + (module_ref if module_ref is not None else '') + '.tsv.gz'
coenrichments_path = hits_genre_path.replace('.tsv.gz', suffix) if hits_genre_path is not None else \
    one_zeroes_table_path.replace(".csv.gz", suffix)
print coenrichments_path
if args.outputdir is not None:
    if not exists(args.outputdir):
        mkdir(args.outputdir)
    coenrichments_path = join(args.outputdir, basename(coenrichments_path))
print coenrichments_path

if overwrite or not exists(coenrichments_path):
    table = []
    # queries = {'GATA'} None
    queries = None
    counter = 0
    queries = [[a, b] for a, b in combinations(hits_by_motif.keys(), 2)]
    if module_ref is not None:
        queries = [[a, b] for a, b in queries if a == module_ref or b == module_ref]
    print len(queries)
    for moduleA, moduleB in queries:
        # discard if they are part of the same cluster
        if module_ref is not None and moduleA != module_ref and moduleB != module_ref:
            continue
        if moduleA in cluster_partners_by_tfid and moduleB in cluster_partners_by_tfid[moduleB]:
            print moduleA, 'and', moduleB, 'are same-cluster. Skip'

        print moduleA, moduleB
        if queries is not None:
            if len(queries) == 2:
                if not moduleA in queries:
                    continue
                if not moduleB in queries:
                    continue
            elif len(queries) == 1:
                if not moduleA in queries and not moduleB in queries:
                    continue
        if moduleA == moduleB:
            continue

        peaksA, peaksB = hits_by_motif[moduleA], hits_by_motif[moduleB]
        # print peaksA.head()
        # print peaksB.head()

        def get_enrichment(peaksA, peaksB, nPeaks):
            a = len(peaksA.intersection(peaksB))
            b = len(peaksB) - a
            c = len(peaksA) - a
            d = nPeaks - a - b - c
            assert a + b + c + d == nPeaks
            odds_ratio, pval = fisher_exact([[a, b], [c, d]], alternative='greater')
            return a, b, c, d, odds_ratio, pval

        a, b, c, d, odd_ratio, pval = get_enrichment(set(peaksA['header']), set(peaksB['header']), nPeaks)

        if a < 0:
            continue
        # print a, b, c, d, odd_ratio, pval
        # print a, b, c, d, odd_ratio
        assert a + b + c + d == nPeaks
        ncommon = a
        na = ncommon + b
        nb = ncommon + c
        nt = nPeaks
        zscores_data = []
        for i in range(ncommon):
            zscores_data.append([1, 1])
            na -= 1
            nb -= 1
        for ai in range(na):
            zscores_data.append([1, 0])
        for bi in range(nb):
            zscores_data.append([0, 1])
        for i in range(nt - len(zscores_data)):
            zscores_data.append([0, 0])

        zscores_data = pd.DataFrame(zscores_data, columns=['is.a', 'is.b'])
        zscores_data['names'] = [i for i in range(zscores_data.shape[0])]

        a, b, c, d, odd_ratio, pval = get_enrichment(
            set(list(zscores_data[zscores_data['is.a'] == True]['names'])),
            set(list(zscores_data[zscores_data['is.b'] == True]['names'])), zscores_data.shape[0])
        # print a, b, c, d, odd_ratio, pval

        odds_ratios_random = []
        zscore = None
        for i in range(npermutation_zscore):
            va, vb = list(zscores_data['is.a']), list(zscores_data['is.b'])
            shuffle(va), shuffle(vb)
            zscores_data['is.a'] = va
            zscores_data['is.b'] = vb
            next_peaksA = set(zscores_data[zscores_data['is.a'] == True]['names'])
            next_peaksB = set(zscores_data[zscores_data['is.b'] == True]['names'])
            # print zscores_data.head()
            # print len(next_peaksA), len(next_peaksB), len(next_peaksA.intersection(next_peaksB))
            # print common.head()
            a_random, b_random, c_random, d_random, odd_ratio_random, pval_random = get_enrichment(next_peaksA, next_peaksB,
                                                                zscores_data.shape[0])
            # print a, b, c, d, a + b + c + d, odd_ratio_random, pval
            odds_ratios_random.append(odd_ratio_random)

        if npermutation_zscore > 0:
            mu, sigma = average(odds_ratios_random), np.std(odds_ratios_random)
            # print 'obs. %f mu %f sigma %f' % (odd_ratio, mu, sigma)
            zscore = (odd_ratio - mu) / sigma
        # print a, b
        # print c, d
        # print odd_ratio, pval

        # common genes between both modules
        print genes_by_module
        for mod in moduleA, moduleB:
            p = join('/g/scb2/zaugg/rio/EclipseProjects/wild/lib',
                     'genre/glossary_description',
                     'Glossary Modules/Glossary_module_' +
                     mod.replace('-III', '(III)').replace("-II", '(II)') + '.xlsx')
            if not exists(p):
                continue
            genes = pd.read_excel(p, index_col=0)
            gene_names = set(genes.columns)
            genes_by_module[mod] = gene_names

        n_common_genes = None
        if moduleA in genes_by_module and moduleB in genes_by_module:
            n_common_genes = len(genes_by_module[moduleA].intersection(genes_by_module[moduleB]))

        # distances between hits
        common_sites = None
        if hits_genre_path is not None:
            common_sites = peaksA.merge(peaksB, on='header')
            # print common_sites.head()
            common_sites['distance'] = common_sites['position_x'] - common_sites['position_y']

        best_dist, best_freq, top_dists, top_freqs = None, None, None, None
        if common_sites is not None:
            counts_by_distance = [[di, fi] for di, fi in common_sites['distance'].value_counts().iteritems()]
            best_dist, best_freq, top_dists, top_freqs = None, None, None, None
            n_top = 6
            if len(counts_by_distance) >= n_top:
                best_dist, best_freq = counts_by_distance[0]
                top_dists, top_freqs = zip(*[[di, fi] for di, fi in counts_by_distance][:n_top])

        table.append([moduleA, moduleB, n_common_genes,
                      args.title, a, b, c, d, odd_ratio, pval, zscore, best_dist, best_freq, top_dists, top_freqs])
        print table[-1]
        # if len(table) > 20:
        #     break
        # break

    res = pd.DataFrame(table, columns=['module.A', 'module.B', 'n.common.tfs',
                                       'bg_id', 'a', 'b', 'c', 'd', 'or', 'p.val', 'zscore', 'best.dist', 'best.freq',
                                       'top.3.dists', 'top.3.freqs'])

    res['p.adj.odd.ratio'] = RFacade.get_bh_pvalues(res['p.val'])
    # print res.head()
    res = res.sort_values('p.val', ascending=True)
    # print res[(res['p.adj.odd.ratio'] < 0.05) & (res['n.common'] < 10)].sort_values('best.freq', ascending=False)
    # print res[res['module.A'].str.contains('bZIP')]
    # print res.sort_values('best.freq', ascending=False)
    DataFrameAnalyzer.to_tsv_gz(res, coenrichments_path)

print coenrichments_path