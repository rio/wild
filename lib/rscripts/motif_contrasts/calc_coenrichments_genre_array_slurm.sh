#!/bin/bash
#SBATCH -J calc_coenrichments_genre
#SBATCH --mem=3000
#SBATCH --qos=normal
#SBATCH -p htc
 
#SBATCH -n 1
#SBATCH -o  /scratch/rio/bsub_output/calc_coenrichments_genre_output_%a.txt
#SBATCH -e  /scratch/rio/bsub_output/calc_coenrichments_genre_error_%a.txt

echo "STARTING JOB"
python calc_coenrichments_genre.py ${SLURM_ARRAY_TASK_ID} --hitsgenre output/sigA_vs_sigB/motif_scores/motif_scores1_HOCOMOCO.tsv.gz --fasta input/cons3groups_FC_sig005.fa --outputdir output/bytf_sig005
echo "FINISHED JOB"

        
# (SLURM):
#Please submit your script with the following command...
# sbatch --array=1-780%100 calc_coenrichments_genre_array_slurm.sh
