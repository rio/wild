

## 
# setwd('/g/scb/zaugg/rio/EclipseProjects/zaugglab/cap_selex_quantitative_analysis/src/03_feature_clustering')
library(cluster)
library("argparse", quietly=TRUE)

p <- ArgumentParser()
p$add_argument("input_file", help="input file", type="character")
p$add_argument('output_file', help="Output path ", type="character")
args <- p$parse_args()

p = args$input_file

print(p)

res = data.frame(id=1:nrow(df))
for(n_medoids in 2:10){
    print(n_medoids)
    pamx <- pam(df, n_medoids)
    pamx # Medoids: '7' and '25' ...
    summary(pamx)
    res[paste0('n.', as.character(n_medoids))] = pamx$clustering
}

library(gplots)

df = read.table(p, header=TRUE)
# df <- head(df, 100)
df <- df[,c('bdnf.1', 'bdnf.6', 'bdnf.10', 'KCl.1', 'KCl.6', 'KCl.10')]
n_medoids <- 10
pamx <- pam(df, n_medoids)
df$cluster <- pamx$clustering
df <- df[order(df$cluster),]
clustnames <- df$cluster
df$cluster <- NULL
m <- as.matrix(df)
heatmap.2(m, labRow = FALSE, trace="none", scale = 'column',
          main = "", col=redgreen(75), RowSideColors = as.character(clustnames), Rowv = NA)

clusters <- hclust(dist(m))

hc.cut <- hcut(m, k = 3, hc_method = "complete")
plot(clusters, which)

library(cluster)
library(reshape2)
library(ggplot2)

matrix <- as.matrix(df[,1:6])

d <- dist(matrix, method='manhattan')
k_range <- 2:10
methds <- c('complete','single','average')
avgS <- matrix(NA,ncol=3,nrow=length(k_range),
               dimnames=list(k_range, methds))
for(k in k_range) 
    for(m in seq_along(methds)) {
        print(c(k, m))
        print('clustering')
        h <- hclust(d, meth=methds[m])
        print('cutting tree...')
        c <- cutree(h,k)
        print('silhouette...')
        s <- silhouette(c,d)
        avgS[k-1,m] <- mean(s[,3])
    }


dt <- melt(avgS)
colnames(dt) <- c("NClusts","Meth","AvgS")
ggplot(dt,aes(x=NClusts,y=AvgS,color=Meth)) + 
    geom_line()

h <- hclust(d, meth='single')
c <- cutree(h, 8)
plot(h)
print('silhouette...')
s <- silhouette(c,d)
avgS[k-1,m] <- mean(s[,3])