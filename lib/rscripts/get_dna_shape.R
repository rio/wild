# In this script I read in a fasta file and produce an output matrix for 4 different shape
# features (Helical twist, Minor Groove Width, Roll and ProT)
# suppressMessages(library(DNAshapeR))
print('loading packages...')
# .libPaths( c( .libPaths(), "C://Users//ignacio//Documents//R//win-library//3.3") )
# library(DNAshapeR)
suppressMessages(library(DNAshapeR))
suppressMessages(library(dplyr))

use_config_file <- FALSE
if(!use_config_file){
    library("argparse", quietly=TRUE)
    # "Convert a fasta file into 4 shape features (Helical twist, Minor Groove Width, Roll and ProT)"
    p <- ArgumentParser()
    p$add_argument("fasta_file", help="The fasta file that is converted", type="character")
    help <- paste(  "The place where the matrices are saved. The script will append",
                    "an identifier for each of the shapes to the argument. For example",
                    "the argument is './tmp/abcd' then it will produce './tmp/abcd-mgw.tsv',",
                    "'./tmp/abcd-prot.tsv' etc.")
    p$add_argument("--output_prefix", help=help, default="./")
    p$add_argument("--gzip", default=1)
    p$add_argument("--normalize", default=0)
    p$add_argument('--inferflanks', default=0)
    p$add_argument("--shapes", default=NULL)
    args <- p$parse_args()
}else{
    print('setting working directory...')
    # setwd('C:\\Users\\ignacio\\Dropbox\\Eclipse_Projects\\zaugglab\\comb-TF-binding\\cap_selex_quantitative_analysis\\src\\09_forkhead_ets_perf_bias_assessment')
    setwd('/g/scb/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/src/09_forkhead_ets_perf_bias_assessment')
    config_file <- "../../data/config_get_DNAshape.txt"
    parms <- read.table(config_file)
    # replace by custom parms
    args <- c()
    parms <- as.vector(parms$V2)
    args$shapes <- as.character(parms[1])
    args$fasta_file <- as.character(parms[5])
    args$gzip <- as.numeric(parms[4])
    args$inferflanks <- as.numeric(parms[6])
    args$normalize <- as.numeric(parms[2])
    args$output_prefix <- as.character(parms[3])
    
}

# this is a comment
# print('reading fasta file')

# debug 1
# args$fasta_file <- 'c:\\users\\ignacio\\appdata\\local\\temp\\tmpqnfxyz'
#debug 2
# args$fasta_file <- 'c:\\users\\ignacio\\appdata\\local\\temp\\tmpjwy2el'
# args$fasta_file <- 'c:\\users\\ignacio\\appdata\\local\\temp\\tmpgq_crx'
# pred <- getShape(args$fasta_file)


# custom: get all DNA-shape features
all_shapes = c('MGW', 'HelT', 'ProT', 'Roll', 'EP', 'Stretch', 'Tilt', 'Buckle',
                'Shear', 'Opening', 'Rise', 'Shift', 'Stagger', 'Slide')
pred <- getShape(args$fasta_file, shapeType=all_shapes)
for(i in 1:length(pred)){
    suffix <- names(pred)[i]
    m <- pred[i]

    for (order in c('1', '2')){
        output_path = paste0(args$output_prefix, "-", order, '-', suffix, ".tsv.gz")
        print(output_path)
        featureVector <- encodeSeqShape(args$fasta_file, pred, paste0(order, '-', c(suffix)), normalize=as.numeric(args$normalize))
        gz = gzfile(output_path, 'w')
        featureVector %>%
            as.data.frame() %>%
            write.table(file=gz, quote=FALSE, sep="\t", col.names=FALSE, row.names=FALSE)
        close(gz)
    }
}

# print(head(pred))
# stopifnot(1 > 2)

# sometimes (specially for SELEX data without flanks), we want to estimate shape features
infer_shapes_at_flanks <- as.numeric(args$inferflanks)
if(infer_shapes_at_flanks){
    p <- "/g/scb/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/data/shapes_inferred.tsv.gz"
    if(!file.exists(p))
        p <- '/home/ignacio/Dropbox/Eclipse_Projects/zaugglab/cap_selex_quantitative_analysis/data/shapes_inferred.tsv.gz'
    if(!file.exists(p))
        p <- "C:\\Users\\ignacio\\Dropbox\\Eclipse_Projects\\zaugglab\\comb-TF-binding\\cap_selex_quantitative_analysis\\data\\shapes_inferred.tsv.gz"

    
    print('checking if inferred shapes exists...')
    stopifnot(file.exists(p))
    inferred_shapes <- read.table(p, header=T)
    
    
    inferred_shapes <- inferred_shapes[, !grepl(".bg", names(inferred_shapes))]
    inf <- inferred_shapes
    
    seqs <- read.table(args$fasta_file)
    seqs <- seqs$V1[!grepl(">", seqs$V1)]
    df <- data.frame(seq=seqs)
    
    seq_length <- nchar(as.character(df$seq[1]))
    df['left.4'] <- substring(df$seq, 0, 4)
    df['right.4'] <- substring(df$seq, seq_length - 3)
    df['left.3'] <- substring(df$seq, 0, 3)
    df['right.3'] <- substring(df$seq, seq_length - 2)

    # to solve merging issues
    df$order <- 1:nrow(df)
    shapes1 <- merge(df, inf[inf$k == '3_2_0',], by.x='left.3', by.y='seq')
    shapes2 <- merge(df, inf[inf$k == '4_1_0',], by.x='left.4', by.y='seq')
    shapes3 <- merge(df, inf[inf$k == '4_0_1',], by.x='right.4', by.y='seq')
    shapes4 <- merge(df, inf[inf$k == '3_0_2',], by.x='right.3', by.y='seq')

    shapes1 <- shapes1[order(shapes1$order), ]
    shapes2 <- shapes2[order(shapes2$order), ]
    shapes3 <- shapes3[order(shapes3$order), ]
    shapes4 <- shapes4[order(shapes4$order), ]
    
    pred$MGW <- cbind(NA, pred$MGW)
    pred$MGW <- cbind(NA, pred$MGW)
    pred$MGW[,3] <- shapes1$MGW.03
    pred$MGW[,4] <- shapes2$MGW.03
    pred$MGW <- cbind(pred$MGW, NA)
    pred$MGW <- cbind(pred$MGW, NA)
    pred$MGW[,ncol(pred$MGW) - 3] <- shapes3$MGW.03
    pred$MGW[,ncol(pred$MGW) - 2] <- shapes4$MGW.03

    #ProT
    pred$ProT <- cbind(NA, pred$ProT)
    pred$ProT <- cbind(NA, pred$ProT)
    pred$ProT[,3] <- shapes1$PROT.03
    pred$ProT[,4] <- shapes2$PROT.03
    pred$ProT <- cbind(pred$ProT, NA)
    pred$ProT <- cbind(pred$ProT, NA)
    pred$ProT[,ncol(pred$ProT) - 3] <- shapes3$PROT.03
    pred$ProT[,ncol(pred$ProT) - 2] <- shapes4$PROT.03

    # Roll    
    pred$Roll <- cbind(NA, pred$Roll)
    pred$Roll <- cbind(NA, pred$Roll)
    pred$Roll[,2] <- shapes1$ROLL.03
    pred$Roll[,3] <- (shapes1$ROLL.04 + shapes2$ROLL.03) / 2
    pred$Roll[,4] <- (shapes2$ROLL.04 + pred$Roll[,4]) / 2
    pred$Roll <- cbind(pred$Roll, NA)
    pred$Roll <- cbind(pred$Roll, NA)
    pred$Roll[,ncol(pred$Roll) - 3] <- (pred$Roll[,ncol(pred$Roll) - 3] + shapes3$ROLL.03) / 2
    pred$Roll[,ncol(pred$Roll) - 2] <- (shapes3$ROLL.04 + shapes4$ROLL.03) / 2
    pred$Roll[,ncol(pred$Roll) - 1] <- shapes4$ROLL.04

    # HelT
    pred$HelT <- cbind(NA, pred$HelT)
    pred$HelT <- cbind(NA, pred$HelT)
    pred$HelT[,2] <- shapes1$HELT.03
    pred$HelT[,3] <- (shapes1$HELT.04 + shapes2$HELT.03) / 2
    pred$HelT[,4] <- (shapes2$HELT.04 + pred$HelT[,4]) / 2
    pred$HelT <- cbind(pred$HelT, NA)
    pred$HelT <- cbind(pred$HelT, NA)
    pred$HelT[,ncol(pred$HelT) - 3] <- (pred$HelT[,ncol(pred$HelT) - 3] + shapes3$HELT.03) / 2
    pred$HelT[,ncol(pred$HelT) - 2] <- (shapes3$HELT.04 + shapes4$HELT.03) / 2
    pred$HelT[,ncol(pred$HelT) - 1] <- shapes4$HELT.04
    
    if(grepl(args$shapes, 'EP')){    #EP
        pred$EP <- cbind(NA, pred$EP)
        pred$EP <- cbind(NA, pred$EP)
        pred$EP[,3] <- shapes1$EP.03
        pred$EP[,4] <- shapes2$EP.03
        pred$EP <- cbind(pred$EP, NA)
        pred$EP <- cbind(pred$EP, NA)
        pred$EP[,ncol(pred$EP) - 3] <- shapes3$EP.03
        pred$EP[,ncol(pred$EP) - 2] <- shapes4$EP.03
    }
}

# pred1 <- pred
# pred2 <- pred

# pred1$MGW
# pred2$MGW

save_matrix <- function(m, suffix, gzip=T){
    output_path = paste0(args$output_prefix, "-", suffix, ".tsv.gz")
    gz = gzfile(output_path, 'w')
    # print(output_path)
    m %>%
        as.data.frame() %>%
        write.table(file=gz, quote=FALSE, sep="\t", col.names=FALSE, row.names=FALSE)
    close(gz)
}

# print('writing output..')
for(feat in strsplit(args$shapes, ',')[[1]]){
    print(feat)
    featureVector <- encodeSeqShape(args$fasta_file, pred, c(feat), normalize=as.numeric(args$normalize))
    # print('saving matrices...')
    # print(head(featureVector))
    save_matrix(featureVector, feat, gzip=args$gzip)
}
