'''
Created on Apr 11, 2016

SNP extraction methods (genotypes, sequence modification, etc.)
@author: ignacio
'''
from os import system, remove, mkdir, listdir, rmdir
from itertools import product, combinations

import subprocess as sp
import os
import glob
import pandas as pd
from os.path import exists, join, abspath, basename
from scipy.stats.stats import spearmanr
from random import shuffle, random, randint
import matplotlib.pyplot as plt
import gzip
import pickle
from .FastaAnalyzer import FastaAnalyzer
from .variables import *

# import readline
from shutil import rmtree

class SNPAnalyzer:
    
    def __init__(self):
        self.nt_group_to_iupac = None
        self.iupac_to_nt_group = None
        self.aa_by_codon = {"TTT":"F", "TTC":"F", "TTA":"L", "TTG":"L",
                           "TCT":"S", "TCC":"S", "TCA":"S", "TCG":"S",
                           "TAT":"Y", "TAC":"Y", "TAA":"STOP", "TAG":"STOP",
                           "TGT":"C", "TGC":"C", "TGA":"STOP", "TGG":"W",
                           "CTT":"L", "CTC":"L", "CTA":"L", "CTG":"L",
                           "CCT":"P", "CCC":"P", "CCA":"P", "CCG":"P",
                           "CAT":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
                           "CGT":"R", "CGC":"R", "CGA":"R", "CGG":"R",
                           "ATT":"I", "ATC":"I", "ATA":"I", "ATG":"M",
                           "ACT":"T", "ACC":"T", "ACA":"T", "ACG":"T",
                           "AAT":"N", "AAC":"N", "AAA":"K", "AAG":"K",
                           "AGT":"S", "AGC":"S", "AGA":"R", "AGG":"R",
                           "GTT":"V", "GTC":"V", "GTA":"V", "GTG":"V",
                           "GCT":"A", "GCC":"A", "GCA":"A", "GCG":"A",
                           "GAT":"D", "GAC":"D", "GAA":"E", "GAG":"E",
                           "GGT":"G", "GGC":"G", "GGA":"G", "GGG":"G",}

    def is_heterozygous(self, vcf_genotype):
        return vcf_genotype[0] != vcf_genotype[2]

    def is_homozygous(self, vcf_genotype):
        return vcf_genotype[0] == vcf_genotype[2]

    def get_genotypes_from_individuals(self, snp_ids, delete_cache=True,
                                       tmp_path="snps_tmp.mat"):
        """
        This method extracts genotypes for SNPs if mentioned in other file
        
        The final output is a dictionary that contains the SNP ids and the
        genotypes for a list of individuals with specified IDs
        """
        #===============================================================
        # Genotypes are loaded on the fly using grep -E "^" with the SNP_IDS
        #===============================================================
        genotypes = {}
        if len(snp_ids) == 0:
            return genotypes
        
        #=======================================================================
        # Performance step
        # divide our genotypes tables into 99 substables with the prefix "rs"
        # so we can check faster in the future
        #=======================================================================
        genotypes_dir = "/home/ignacio/Documents/zaugglab/localQTLs/genotypes"
        subdivided_gt_dir = join(genotypes_dir, "by_rs")
        if not exists(subdivided_gt_dir):
            mkdir(subdivided_gt_dir)
            print("creating temporal files for increasing performance...")
            for i in range(100):
                rs_k = "rs" + str(i)
                subdivided_gt_path = join(subdivided_gt_dir, str(i) + ".txt")
                writer = open(subdivided_gt_path, "w")
                lines_written = 0
                for f in listdir(genotypes_dir):
                    if not ".txt" in f:
                        continue
                    j = 0
                    for r in open(join(genotypes_dir, f)):
                        if j == 0:
                            if lines_written == 0:
                                writer.write(r)
                                lines_written += 1                    
                        else:
                            if r.startswith(rs_k):
                                writer.write(r)
                                lines_written += 1            
                        
                        j += 1
                writer.close()
        
        #=======================================================================
        # Check SNP file, one by one, until we find all our SNPs 
        #=======================================================================
        gt_path = tmp_path
        if exists(gt_path):
            remove(gt_path)

        print("loading genotypes (i.e. 0, 1 or 2) by grepping files (please wait)...")
        print("N=", len(snp_ids), "SNPs")
        # this is the awk generic command for extracting SNPs 
        # from several txt files
        
        #=======================================================================
        # Get all the queried SNP, reading file by file all the subdivided tables 
        #=======================================================================
        for snp_file_id in range(100):
            next_gt_path = join(subdivided_gt_dir, str(snp_file_id) + ".txt")
            assert exists(next_gt_path)
            grep_cmd = "grep -E \"^$CONDITION\" " + next_gt_path
            condition_str = ""
            selected_snps = [s for s in snp_ids if s.startswith("rs" + str(snp_file_id))]
            if len(selected_snps) == 0:
                continue
            for i, snp_id in enumerate(selected_snps):
                condition_str += snp_id + " "
                if i + 1 != len(selected_snps):
                    condition_str += "|"        
            grep_cmd = grep_cmd.replace("$CONDITION", condition_str)
            system(" ".join([grep_cmd, ">>", gt_path]))
        
        rows = [r.strip().split(" ") for r in open(gt_path)]
        if delete_cache and exists(gt_path):
            remove(gt_path)
        # read the first line of a table to get the individual names
        p = "/home/ignacio/Documents/zaugglab/localQTLs/genotypes/snps1.filtered.allGT.txt"
        genotype_headers = [r.strip().split(" ") for r, i in zip(open(p), list(range(1)))][0]
        for r in rows:
            # the grep file prints teh file name before the SNP_ID, separated by a ":" symbol
            snp_id, values = r[0], r[1:]
            if not snp_id in genotypes:
                genotypes[snp_id] = {ind: int(v) for ind, v in zip(genotype_headers, values)}
        return genotypes

    def get_scores_deepbind(self, sequences, deepbind_ids_path, tmp_fasta=None):
        if tmp_fasta is None:
            tmp_fasta = join('../tmp', "tmp_fasta.fa")

        # convert sequences into fasta
        assert sequences is not None
        writer = open(tmp_fasta, "w")
        for i, entry in enumerate(sequences):
            snp_id, seq = entry if len(entry) == 2 else [i, entry]

            writer.write(">" + str(snp_id) + "\n" + seq + "\n")
        writer.close()

        # run DeepBind
        cmd = [deepbind_bin_path, deepbind_ids_path]
        # print cmd
        ps = sp.Popen(['cat', tmp_fasta], stdout=sp.PIPE)
        output = [r for r in sp.Popen(cmd, stdout=sp.PIPE, stdin=ps.stdout,
                                      stderr=sp.PIPE).communicate()[0].split("\n") if len(r) != 0]

        df = pd.DataFrame([[s[0], float(score)] for s, score in zip(sequences, output[1:])],
                          columns=['header', 'score'])
        return df

    def get_motif_width_pfm_homer(self, motif_id):
        base_dir = "/home/ignacio/Documents/zaugglab/jolma_motifs_vs_hqtls/pfms_homer"
        # print motif_id
        # print exists(motif_id)
        pfm_path = join(base_dir, motif_id + ".pfm") if not exists(motif_id) else motif_id
        # get best sequence
        is_gz = pfm_path.endswith(".gz")
        rows = [r.strip().split("\t") for r in
                (open(pfm_path) if not is_gz else gzip.open(pfm_path))][1:]        
        rows = [r for r in rows if len(r) == 4]
        return len(rows)

    def convert_jaspar_pfm_to_homer_ppm(self, pfm_path, ppm_path):
        '''

        Convert a PFM matrix into a PPM matrix

        :param pfm_path: Input PFM path in JASPAR format
        :param ppm_path: Output PPM path in HOMER format
        :return:
        '''
        threshold = -1000000000

        # read file
        rows = [r.strip() for r in open(pfm_path)]
        motif_id = rows[0].split("\t")[0][1:]
        df = pd.DataFrame([list(map(int, r.replace("]", "").replace("[", "").split()[1:]))
                           for r in rows[1:]])

        # convert frequencies to probabilities
        df = df.T
        for row in df.iterrows():
            index, r = row
            row_sum = sum(r)
            values = []
            for ri in r:
                values.append(float(ri) / row_sum)
            df.iloc[index] = values

        # write output file
        writer = open(ppm_path, "w")
        writer.write(">" + motif_id + "\t" + motif_id + "\t" + str(threshold) + "\n")
        for row in df.iterrows():
            index, r = row
            writer.write("\t".join(map(str, r)) + "\n")
        writer.close()

    def generate_mismatches(self, s, d=2):
        '''
        Given a seed sequence, return all sequences with d mismatches in it
        :param d:
        :return:
        '''
        N = len(s)
        letters = 'ACGT'
        pool = list(s)

        for indices in combinations(list(range(N)), d):
            for replacements in product(letters, repeat=d):
                skip = False
                for i, a in zip(indices, replacements):
                    if pool[i] == a: skip = True
                if skip: continue

                keys = dict(list(zip(indices, replacements)))
                yield ''.join([pool[i] if i not in indices else keys[i]
                               for i in range(N)])

    def get_motif_width_ppm_meme(self, ppm_path):
        rows = [r for r in open(ppm_path)]        
        motif_width = None
        for r in rows:
            if "w=" in r:
                motif_width = int(r.split("w=")[1])
        assert motif_width != None
        return motif_width
    
    def convert_pwm_homer_to_meme(self, homer_path, meme_path):
        """
        Take a PWM in HOMER format and convert it into a MEME format
        """
        
        s = """MEME version 4

ALPHABET= ACGT

strands: + -

Background letter frequencies
A 0.250 C 0.250 G 0.250 T 0.250 

MOTIF &name
letter-probability matrix: alength= 4 w= &width\n"""
        rows = [r.strip().split("\t") for r in open(homer_path).readlines()][1:]
        s = s.replace("&name", basename(homer_path))
        s = s.replace("&width", str(len(rows)))
        for r in rows:
            s += " " + " ".join(["%.6f" % ri for ri in map(float, r)]) + "\n"
        writer = open(meme_path, "w")
        writer.write(s)
        writer.close()
        
    def score_snps_with_pwm(self, snps, pwm_path, overwrite=False,
                            randomize=False, software="HOMER", subset_test=None,
                            rm_working_dir=True, basename_id="job", working_dir_id=None): 
        """
        Given a set of SNPs, get the flanking sequences that match our given
        case, and scan for motifs, return the best overlapping motif for each
        SNP
        
        If randomize = True, then we merge all the sequences before scoring
        (FDR control)
        
        Software: MEME or FIMO
        """
        
        
        #=======================================================================
        # Get SNP coordinates
        #=======================================================================
        snp_to_coordinate = self.get_coordinates_snps_biomart(snps,
                                                              input_tmp_path="../tmp/" + basename_id + "_snps.list",
                                                              output_path="../tmp/" + basename_id + "_coordinates.tsv")
        
        if subset_test != None:
            snp_to_coordinate = {s: snp_to_coordinate[s] for s in subset_test}
            
        motif_width = None
        if software == "HOMER":
            motif_width = self.get_motif_width_pfm_homer(pwm_path)
        elif software == "FIMO":
            motif_width = self.get_motif_width_ppm_meme(pwm_path)
            
        #=======================================================================
        # Get sequences flanking our snps, +/- our HOMER motif width
        #=======================================================================
        table = []
        snp_by_coordinate = {}
        for i, snp_id in enumerate(snp_to_coordinate):
            chromosome_id, position = snp_to_coordinate[snp_id]
            start, end = position - motif_width, position + motif_width - 1
            coordinate = "chr" + str(chromosome_id) + ":" + str(start)+ "-" + str(end)
            if coordinate in snp_by_coordinate:
                continue
            table.append([snp_id, "chr" + str(chromosome_id), start, end])
            snp_by_coordinate[coordinate] = snp_id 
        tmp_bed_path = "../tmp/bed_path.bed"
        tmp_fasta_path = "../tmp/fasta_sequences.fa"
        
        print(motif_width)
        if (not exists(tmp_bed_path) and not exists(tmp_fasta_path)) or overwrite:
            writer = open(tmp_bed_path, "w")
            for t in table:
                snp_id, chromosome_id, start, end = list(map(str, t))
                writer.write("\t".join(map(str, [chromosome_id, start, end])) + "\n")
            writer.close()
            fasta_analyzer = FastaAnalyzer()
            fasta_analyzer.convert_bed_to_fasta_hg19(tmp_bed_path, tmp_fasta_path)

        fasta = self.get_fastas_from_file(tmp_fasta_path, uppercase=True)
        
        #=======================================================================
        # Randomize sequences if the user wants to perform an FDR control
        #=======================================================================
        if randomize:
            fasta = [[t[0], self.randomize_sequence(t[1])] for t in fasta]
        
        motif_id = ".".join(basename(pwm_path).split(".")[:-1])
        #=======================================================================
        # Score sequences using HOMER
        #=======================================================================
        output_table = None
        if software == "HOMER":
            working_dir_id = motif_id if working_dir_id is None else working_dir_id
            homer_working_dir = join("../tmp/homer_working_dir", working_dir_id)
            tmp_fasta = "homer_tmp_fasta.fa"
            if not exists(homer_working_dir):
                mkdir(homer_working_dir)         
            print(len(fasta))
            scores = self.get_scores_homer([[t[0], t[1]] for t in fasta],
                                           abspath(pwm_path),
                                           tmp_fasta=tmp_fasta,
                                           working_dir=homer_working_dir,
                                           get_best_sequence=True)
            
            if rm_working_dir:
                rmdir(homer_working_dir)
            
            
            output_table = [[snp_by_coordinate[c], c] + scores[c] for c in scores]
        elif software == "FIMO":
            working_dir_id = motif_id if working_dir_id is None else working_dir_id
            meme_working_dir = join("../tmp/fimo_working_dir", working_dir_id)
            tmp_fasta = "fimo_tmp_fasta.fa"
            if not exists(meme_working_dir):
                mkdir(meme_working_dir)         
            scores = self.get_scores_fimo([[t[0], t[1]] for t in fasta],
                                           abspath(pwm_path),
                                           tmp_fasta=tmp_fasta,
                                           working_dir=meme_working_dir)
            
            if rm_working_dir:
                rmtree(meme_working_dir)

            output_table = [[snp_by_coordinate[c[1]], c[1]] + c[2:]  for c in scores]
                   
        return output_table            
        
    
    def convert_pwm_uniprobe_to_homer(self, input_path, output_path):
        header = input_path.split("/")[-1].split(".")[0]
        probs = [s.strip().split("\t") for s in open(input_path)][2:6]
        output = ""
        for i in range(len(probs[0])):
            output += "\t".join([probs[j][i] for j in range(4)]) + "\n"
            
        writer = open(output_path, "w")
        writer.write(">" + "\t".join([header, header, "-100000000"]) + "\n")
        writer.write(output)
        writer.close()
        
    def get_best_and_worse_pwm_score_homer(self, motif_id):
        base_dir = "/home/ignacio/Documents/zaugglab/jolma_motifs_vs_hqtls/pfms_homer"
        pfm_path = join(base_dir, motif_id + ".pfm") if not exists(motif_id) else motif_id
        
        # get best sequence
        rows = [r.strip().split("\t") for r in open(pfm_path)][1:]
        best_sequence = ""
        for r in rows:
            probabilities = list(map(float, r))
            probabilities = [[i, float(ri)] for i, ri in zip(list(range(len(r))), r)]
            best_nt_i = sorted(probabilities, key=lambda x: x[1], reverse=True)[0][0]
            best_nt = "ACGT"[best_nt_i]
            best_sequence += best_nt            
        best_score = self.get_scores_homer([best_sequence], pfm_path, strand="+")[0][-1]
    
        # get worst sequence
        # get best sequence
        rows = [r.strip().split("\t") for r in open(pfm_path)][1:]
        worst_sequence = ""
        for r in rows:
            probabilities = list(map(float, r))
            probabilities = [[i, float(ri)] for i, ri in zip(list(range(len(r))), r)]
            worst_nt_i = sorted(probabilities, key=lambda x: x[1])[0][0]
            worst_nt = "ACGT"[worst_nt_i]
            worst_sequence += worst_nt            
        worst_score = self.get_scores_homer([worst_sequence], pfm_path, strand="+")[0][-1]
        return float(best_score), float(worst_score)
        
    def get_histone_peaks_from_individuals(self, histone_peak_ids, delete_cache=True,
                                       tmp_path="../output/hpeak_tmp.mat",
                                       use_grep=True,
                                       hpeak_mat=None):
        """
        This method extracts hpeak_data from a big matrix
        
        The final output is a dictionary that contains the hpeak IDs and the
        values for a list of individuals with specified IDs
        """
        #===============================================================
        # Hpeak data is loaded on the fly using grep -E "^" with the Hpeak ids
        #===============================================================
        hpeak_data = {}
        if len(histone_peak_ids) == 0:
            return hpeak_data
        
        hpeak_data_path = hpeak_mat
        if hpeak_data_path is None:
            hpeak_data_path = "/home/ignacio/Documents/zaugglab/localQTLs/RMatrices/hpeak_mat_all_H3K27AC.matrix"
        
        # we might be in the cluster: check the proper path from here
        if not exists(hpeak_data_path):
            hpeak_data_path = "/home/rio/zaugglab/rio/data/localQTLs/RMatrices/hpeak_mat_all_H3K27AC.matrix"
        assert exists(hpeak_data_path)
        
        hpeak_tmp_path = tmp_path
        if not exists(hpeak_tmp_path):
            if use_grep:
                print("loading hpeak data by grepping files...")
                print("# histone peak IDs =", len(histone_peak_ids))
                # this is the awk generic command for extracting SNPs 
                # from several txt files
                
                grep_cmd = "grep -E \"^$CONDITION\" " + hpeak_data_path
                condition_str = ""
                
                # if a generic name is given, we return all instances for a given
                # histone mark (e.g. H3K27AC, without "_12" as in "H3K27AC_12") 
                sep = ("" if len(histone_peak_ids) == 1
                       and not "_" in [s for s in histone_peak_ids] else " ")
                
                for i, hpeak_id in enumerate(histone_peak_ids):
                    condition_str += hpeak_id + sep
                    if i + 1 != len(histone_peak_ids):
                        condition_str += "|"        
                grep_cmd = grep_cmd.replace("$CONDITION", condition_str)
                print(grep_cmd)  
    
                system(" ".join([grep_cmd, ">", hpeak_tmp_path]))
            else:
                # read file using the conventional python I/O reading method
                writer = open(hpeak_tmp_path, "w")
                for r in open(hpeak_data_path):
                    split = r.split(" ")
                    if split[0] in histone_peak_ids:
                        writer.write(r)
                writer.close()
        
        rows = [r.strip().split(" ") for r in open(hpeak_tmp_path)]
        if delete_cache and exists(hpeak_tmp_path):
            remove(hpeak_tmp_path)
        # read the first line of a table to get the individual names
        hpeak_headers = [r.strip().split(" ") for r, i in zip(open(hpeak_data_path), list(range(1)))][0]
        for r in rows:
            # the grep file prints teh file name before the SNP_ID, separated by a ":" symbol
            hpeak_id, values = r[0], r[1:]
            if not hpeak_id in hpeak_data:
                hpeak_data[hpeak_id] = {ind: float(v) for ind, v in zip(hpeak_headers, values)}
                
        return hpeak_data
        
    def dna_sequence_to_protein_sequence(self, dna):
        protein_seq = ""
        start = 0
        if start!= -1:
            while start + 2 < len(dna):
                codon = dna[start: start + 3]
                if codon == "TAG": break;
                protein_seq += self.aa_by_codon[codon] if len(protein_seq) != 0 else "M"
                start += 3
        # check for early break -> if STOP if present, trim sequence according to that
        stop_position = protein_seq.find("STOP")
        return protein_seq[:stop_position] if stop_position != -1 else protein_seq                    
        
    def get_sequences_from_hg19(self, motifs_table, motif_width=None):
        """
        It retrieves sequences using HG19 as a reference, and the current motif width
        
        If motif width is None, it is assumed that the coordinates are given as 
        input at positions 1 and 2
        """
        # write HOMER motifs to tmp BED file. This one will be used later for validation
        output_bed = "../output/tmp.bed"
        writer = open(output_bed, "w")
        for r in motifs_table:
            if motif_width != None:
                writer.write("\t".join(map(str, [r[0], r[1], r[1] + motif_width, r[2]])) + "\n")
            else:
                writer.write("\t".join(map(str, [r[0], r[1], r[2]])) + "\n")
        writer.close()
        
        # intersect with hg19 fasta sequence
        print("retrieving sequences from hg19...")
        hg19_fasta = "/home/ignacio/Documents/zaugglab/hg19/hg19.fa"
        output_fasta = "../output/tmp.fasta"
        self.get_sequences_from_bed(output_bed, hg19_fasta, output_fasta)
        
        sequences = self.get_fastas_from_file(output_fasta)
        sequences = [[r[0], r[1].upper()] for r in sequences]
        assert len(motifs_table) == len(sequences)
        motifs_table = [h + s for h, s in zip(motifs_table, sequences)]
        for h in motifs_table:
            assert h[1] == int(h[-2].split(":")[1].split("-")[0])
        motifs_table = [h[0:4] + [h[-1]] for h in motifs_table]
        motifs_table = [h[0:4] + [self.get_complementary_seq(h[-1]) if h[2] == "-" else h[-1]]
                        for h in motifs_table]                
        return motifs_table

            
    def get_sequences_from_personal_genomes_remote_dir(self, coordinates_table,
                                                  output_sequences_dir,
                                                  personal_genomes_dir=None,
                                                  remote_wd=None,
                                                  job_basename="MYJOB",
                                                  run_job=True,
                                                  scp_fastas_from_server=False,
                                                  stop_at=None,
                                                  tmp_dir="../tmp"):
        """
        Create a bed file that will be used for extracting sequences from a set
        of personal genomes located in the cluster at
        
        /home/rio/zaugglab/zaugg_shared/data/genomes_chromovar3D/
        chromovar3d.stanford.edu/genomes/personal_genomes_fasta
        """

        # delete the following lines    
        username = "rio"
        bsub = "/shared/ibm/platform_lsf/9.1/linux2.6-glibc2.3-x86_64/bin/bsub"
        
        if personal_genomes_dir is None:
            personal_genomes_dir = join("/home/ignacio/scb_cluster/zaugg_shared",
                                        "data/genomes_chromovar3D/chromovar3d.stanford.edu",
                                        "genomes/personal_genomes_fasta")

        if run_job:
            # create directory in webserver if it does not exists        
            if remote_wd is not None:
                if not  exists(remote_wd):
                    mkdir(remote_wd)
            
            # create tmp.bed table
            output_bed = join(tmp_dir, "tmp.bed")
            self.create_bed_file(coordinates_table, output_bed)            
                   
            # extract sequences from personal genomes (list gz files)
            genomes_files = [f for f in listdir(personal_genomes_dir) if f.endswith(".fa")]

            # sort sequences by name (it assures that when loading an even number of files,
            # let's say N files, we have two alleles in N/2 individuals)
            genomes_files = sorted(genomes_files)

            #=======================================================================
            # Prepare a bash job that will extract sequences for each of our individuals
            # and store them in our directories
            #=======================================================================
            bash_cmd = ""
            for i, f in enumerate(genomes_files):
                if not f.endswith(".fa"):
                    continue
                basename = ".".join(f.split(".")[:2])
                output_fasta_local = join(output_sequences_dir, basename + ".fa")
                output_fasta_local_gz = output_fasta_local + ".gz"
                
                # do not run again if we already have 
                if exists(output_fasta_local) or exists(output_fasta_local_gz):
                    continue
    
                print(basename)
                
                fasta_to_bed_cmd = " ".join(["fastaFromBed", "-bed",
                                             output_bed, "-fi",
                                             join(personal_genomes_dir, f),
                                             "-fo", output_fasta_local])
                
                #===============================================================
                # Run command to extract sequences to local directory
                #===============================================================
                print(i, fasta_to_bed_cmd)
                system(fasta_to_bed_cmd)
                bash_cmd += fasta_to_bed_cmd + "\n"
              
        #=======================================================================
        # At this point, the directory output_sequences_dir should have the 
        # extracted files
        #=======================================================================
        
        
        # once the file it is in our local directory, compress it to save space
        if len([f for f in listdir(output_sequences_dir) if f.endswith(".fa")]) > 0:
            system(" ".join(["gzip", join(output_sequences_dir, "*.fa"), ";",
                             "rm", join(output_sequences_dir, "*.fa")]))

        #=======================================================================
        # Load sequences from output files
        #=======================================================================
        sequences_by_individual = {}
        fasta_files = [f for f in listdir(output_sequences_dir)]
        fasta_files = sorted(fasta_files)
        for i, f in enumerate(fasta_files):
            if stop_at is not None and i >= stop_at:
                break
            print("reading sequences # ", i + 1, "/", len(fasta_files), f)
            # genome ID + paternal/maternal
            basename = ".".join(f.split(".")[:2])
            fastas = self.get_fastas_from_file(join(output_sequences_dir, f))
            fastas = {h: s.upper() for h, s in fastas}
            sequences_by_individual[basename] = fastas   
        return sequences_by_individual            
        
    def create_bed_file(self, coordinates_table, bed_output_path):
        """
        CHROMOSOME_ID    START    END
        """
        # write HOMER motifs to tmp BED file. This one will be used later for validation
        is_gzip = bed_output_path.endswith(".gz")
        writer = gzip.open(bed_output_path, "w") if is_gzip else open(bed_output_path, "w") 
        for r in coordinates_table:
            writer.write("\t".join(map(str, [r[0], r[1], r[2]])) + "\n")
        writer.close()
        
    def get_random_sequences_from_homer_ppm(self, ppm_homer_path, n=1000):
        """
        Get a set of random sequences using as input HOMER files
        """
        from random import random
        probabilities = [list(map(float, t)) for t in 
                         [r.split("\t") for r in open(ppm_homer_path)][1:]]
        
        sequences = []
        for i in range(n):
            s = ""
            for j in range(len(probabilities)):
                probabilities_table = sorted([["ACGT"[k], pi] 
                                              for k, pi in zip(list(range(4)),
                                                               probabilities[j])],
                                             key=lambda x: -x[1])
                random_number = random()
                sum_probs = 0.0
                for t in probabilities_table:
                    if random_number <= sum_probs + t[1]:
                        s += t[0]
                        break
                    else:
                        sum_probs += t[1]                
            sequences.append(s)
        
        for s in sequences:
            print(s) 
            
        import sys; sys.exit()
        print(probabilities)
                
    def get_sequences_from_personal_genome(self, coordinates_table,
                                           personal_genome_path, as_dict=False,
                                           output_fasta=None,
                                           overwrite=True,
                                           tmp_dir="../tmp"):
        """
        It retrieves sequences using a personal genome as        
        """
        
        if output_fasta is not None and exists(output_fasta):
            return
        if exists(output_fasta.replace(".fa", ".gz")):
            return

        #=======================================================================
        # # write HOMER motifs to tmp BED file. This one will be used later for validation
        #=======================================================================
        output_bed = join(tmp_dir, "tmp.bed")
        self.create_bed_file(coordinates_table, output_bed)
        
        # intersect with hg19 fasta sequence
        if output_fasta == None:
            output_fasta = join(tmp_dir, "tmp.fasta")
        self.get_sequences_from_bed(output_bed, personal_genome_path, output_fasta)
        
        sequences = self.get_fastas_from_file(output_fasta, as_dict=False)
        
        return
    
 
        for r, c in zip(sequences, coordinates_table):
            header = c[0] + ":" + str(c[1]) + "-" + str(c[2])
            assert r[0] == header
        sequences = [[r[0], r[1].upper()] for r in sequences]
        assert len(coordinates_table) == len(sequences)
        sequences_table = [h + s for h, s in zip(coordinates_table, sequences)]
        for h in sequences_table:
            assert h[1] == h[3].split(":")[1].split("-")[0]
        sequences_table = [h[0:4] + [h[-1]] for h in sequences_table]
        sequences_table = [h[0:4] + [self.get_complementary_seq(h[-1]) if h[2] == "-" else h[-1]]
                        for h in sequences_table]                
        return sequences_table

    def get_sequences_from_bed(self, bed_path, input_fasta, output_fasta):
        """
        Given a BED path and a FASTA path, get FASTAS sequences from it.
        """
        args = ["fastaFromBed", "-bed", bed_path, "-fi", input_fasta,
                "-fo", output_fasta]
        print(args)
        print(" ".join(args))
        args = " ".join(args)
        system(args)

    def get_personal_genome_sequences_at_header(self, personal_genomes_dir, header):
        personal_genome_files = glob.glob(personal_genomes_dir + "/*.fa")
        
        for f in personal_genome_files:
            print(f)
            cmd = ["grep", "-A", "1", header, f, "--no-group-separator"]
            sequences = sp.Popen(cmd, stdout=sp.PIPE).communicate()[0]
            print(len(sequences))
        import sys; sys.exit()
               
    def get_fastas_from_file(self, fasta_path, as_dict=False,
                             uppercase=False, stop_at=None, na_remove=False):

        fasta_analyzer = FastaAnalyzer()
        return fasta_analyzer.get_fastas_from_file(fasta_path, as_dict,
                                                   uppercase, stop_at, na_remove)

    def get_coordinates_snps_biomart(self, snps_ids=None, input_tmp_path=None,
                                     output_path=None):
        
        """
        Create tmp file with input
        """

        if input_tmp_path is None and output_path is None:
            input_tmp_path = "../tmp/snps.list"
            output_path = "../tmp/snps_with_coordinates.txt"
        
        if not exists(input_tmp_path) and snps_ids != None:
            writer = open(input_tmp_path, "w")
            for snp_id in snps_ids:
                writer.write(snp_id + "\n")
            writer.close()
            
        #=======================================================================
        # Here RScript needs to be run
        #=======================================================================
        if not exists(output_path):
            print("Preparing SNP to coordinates...")
            cmd = ["Rscript", "--vanilla", "../src/01_convert_snps_to_coordinates.R",
                   input_tmp_path, output_path]
            sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE).communicate()
            
        # convert SNP IDs to coordinates
        print("reading SNPs from RStudio - BioMart process...")
        rows = [r.strip().split(" ") for r in open(output_path)]
        snp_to_coordinate = {}
        for r in rows[1:]:
            if len(r) > 0 and (str(r[1], "utf-8").isnumeric() or r[1] == "X" or r[1] == "Y"):
                snp_to_coordinate[r[0]] = list(map(int, r[1:]))
        
        return snp_to_coordinate
        
    def get_snp_to_chromosome_qtl_grubert(self, marks={"H3K27AC", "H3K4ME3",
                                                       "H3K4ME1", "DHS", "RNA"},
                                          serialize=True):
        """
        Get a dictionary SNP->ChromosomeID from the QTLs table
        related to them
        """
        
        d = "/home/ignacio/Documents/zaugglab/localQTLs/new_tables_oana"
        if not exists(d):
            d = "/scratch/rio/zaugglab/new_tables_oana"
        
        snp_to_chromosome = None
        pickle_path = join(d, "SNPQTLmatrix.pickle")
        if serialize and exists(pickle_path):
            print("loading SNP coordinates from pickle file...")
            print(pickle_path)
            snp_to_chromosome = pickle.load(open(pickle_path, "rb"))
        else:
            snp_to_chromosome = {}
            for mark in marks:
                path = join(d, "SNPQTLmatrix." + mark + ".gz")
                if not exists(path):
                    continue
                for t in [r.split("\t") for r in gzip.open(path)]:
                    snp_to_chromosome[t[2]] = t[1]
            pickle.dump(snp_to_chromosome, open(path + ".pickle", "wb"))

        return snp_to_chromosome

    def get_significant_motif_matches_grubert_et_al_path(self):
        d = "/home/ignacio/Documents/zaugglab/localQTLs/new_tables_oana/motif_matches/"
        path = join(d, "MotifAnalysisTable.txt.gz")
        return path

    def get_significant_motif_matches_grubert_et_al_headers(self):
        path = self.get_significant_motif_matches_grubert_et_al_path()
        columns = [line for line, i in zip(gzip.open(path), list(range(1)))][0].strip().split("\t")
        return columns

    def get_significant_motif_matches_grubert_et_al(self, motif_id=None,
                                                    marks={"H3K27AC"}, log=False,
                                                    contact_type="Local"):
        """
        Load motifs from Oana's output table, given a motif ID from ENCODE
        (check Fig 6E from Grubert et al.)
        
        contact_type= "Local" or "Distal"
        """

        path = self.get_significant_motif_matches_grubert_et_al_path()
        
        # select by the motif ID we are interested
        if motif_id != None:
            rows = [r.strip().split("\t") for r in gzip.open(path)
                    if motif_id in r.split("\t")[6] and len(r) > 0]
        else:
            rows = [r.strip().split("\t") for r in gzip.open(path) if len(r) > 0]
        
        # filter by histone mark
        rows = [r for r in rows if r[1].split("_")[0] in marks]
        n_initial = len(rows)
        
        # select localQTLs only ("Local", "Distal")
        rows = [r for r in rows if r[-1] == contact_type]
        
        #=======================================================================
        # remove duplicated entries: Some Motif IDs can give more than 
        # one hit per SNP, and that maps to the same hpeaks again 
        #=======================================================================
        selected = {}
        for r in [r for r in rows if float(r[9]) <= 0.05]:
            k = r[6] + r[0] + r[1]
            if not k in selected:                
                selected[k] = []
            selected[k].append(r)
        for k in selected:
            if len(selected[k]) > 1:
                print(k)
                for r in selected[k]:
                    print(r)

        selected = {r[6] + r[0] + r[1] : [] for r in rows if float(r[9]) <= 0.05}
        new_rows = []
        for r in rows:
            k = r[6] + r[0] + r[1]
            if k in selected:
                selected[k].append(r)
                if len(selected[k]) == 1:
                    new_rows.append(r)
            else:
                new_rows.append(r)
        # print n_initial
        # print len(new_rows)
        rows = new_rows
        n_no_duplicates = len(rows)
        
        # filter by crossBH p-values
        significant_cases = [r for r in rows if float(r[9]) <= 0.05]
                
        # single SNPs in this dataset, for selected SNP
        snps, hpeaks = [{r[i] for r in rows} for i in [0, 1]]
        
        if log:
            print("# initial", n_initial)
            print("total relationships (no duplicates)", n_no_duplicates)
            print("total significant relationships", len(significant_cases))
            print("unique SNPs", len(snps))
            print("unique Histone peaks", len(hpeaks))
            
            # check the amount of positive and negative relationships in this dataset
            n_pos = sum([1 for r in rows if float(r[4]) > 0])
            n_neg = sum([1 for r in rows if float(r[4]) < 0])
            print("Positive correlations", n_pos, n_pos / float(n_no_duplicates))
            print("Negative correlations", n_neg, n_neg / float(n_no_duplicates))
                    
        # return the SNPs that have been tested.
        return rows
        
    def get_random_sequence(self, length):
        nucleotides = "ATGC"
        return "".join([nucleotides[randint(0, 3)] for i in range(length)])
        
    def get_poly_n(self, nt, n):
        return "".join([nt for i in range(n)])

    def get_matches_to_consensus_seq(self, seq, consensus_seq):
        """
        Assess how many positions are accepted according to what the consensus
        sequence establishes
        """
        if self.iupac_to_nt_group == None:
            self.create_iupac_to_nt_group()

        matches = 0
        for ai, bi in zip(seq, consensus_seq):
            if ai in self.iupac_to_nt_group[bi]:
                matches += 1
        return matches
    
    def get_non_consensus_score_two_states(self, s, k):
        """
        Calculate the score according to a two states model, for a given
        sequence binding to a long sequence at random position, with two
        possible states 1 (A/T), or 0 (C/G).
        """
        u = 0
        for i in range(0, len(s) - k + 1):
            si = s[i: i + k]
            score = sum([1 if nt == "C" or nt == "G" else 0 for nt in si])
            u += score
        return u
        
    def get_matches(self, a, b):
        matches = 0
        for ai, bi in zip(a, b):
            if ai == bi:
                matches += 1
        return matches
            
    def create_nt_group_to_iupac(self):
        self.nt_group_to_iupac = {3: {}, 2: {}, 1: {}}
        self.nt_group_to_iupac[1]["A"] = "A"
        self.nt_group_to_iupac[1]["C"] = "C"
        self.nt_group_to_iupac[1]["G"] = "G"
        self.nt_group_to_iupac[1]["T"] = "T"
        self.nt_group_to_iupac[1]["N"] = "N"
        
        self.nt_group_to_iupac[2]["AG"] = "R"
        self.nt_group_to_iupac[2]["CT"] = "Y"
        self.nt_group_to_iupac[2]["CG"] = "S"
        self.nt_group_to_iupac[2]["AT"] = "W"
        self.nt_group_to_iupac[2]["GT"] = "K"
        self.nt_group_to_iupac[2]["AC"] = "M"
        
        self.nt_group_to_iupac[3]["CGT"] = "B"
        self.nt_group_to_iupac[3]["AGT"] = "D"
        self.nt_group_to_iupac[3]["ACT"] = "H"
        self.nt_group_to_iupac[3]["ACG"] = "V"
        
    def create_iupac_to_nt_group(self):
        self.iupac_to_nt_group = {}
        self.iupac_to_nt_group["A"] = {"A"}
        self.iupac_to_nt_group["C"] = {"C"}
        self.iupac_to_nt_group["G"] = {"G"}
        self.iupac_to_nt_group["T"] = {"T"}
        self.iupac_to_nt_group["N"] = {"ACGT"}
        
        self.iupac_to_nt_group["R"] = {"AG"}
        self.iupac_to_nt_group["Y"] = {"CT"}
        self.iupac_to_nt_group["S"] = {"CG"}
        self.iupac_to_nt_group["W"] = {"AT"}
        self.iupac_to_nt_group["K"] = {"GT"}
        self.iupac_to_nt_group["M"] = {"AC"}
        
        self.iupac_to_nt_group["B"] = {"CGT"}
        self.iupac_to_nt_group["D"] = {"AGT"}
        self.iupac_to_nt_group["H"] = {"ACT"}
        self.iupac_to_nt_group["V"] = {"ACG"}
        
    def randomize_sequence(self, sequence):
        nucleotides = [nt for nt in sequence]
        shuffle(nucleotides)
        return "".join(nucleotides)

    def nucleotides_group_to_iupac_code(self, sorted_nucleotides_str):
        """
        It returns the assignment for single, double and triple nucleotides groups
        into a IUPAC code
        """  
        if self.nt_group_to_iupac == None:
            self.create_nt_group_to_iupac()
            
        k = sorted_nucleotides_str
        return self.nt_group_to_iupac[len(k)][k]

    def get_scores_homer(self, sequences, ppm_homer_path,
                         tmp_fasta=None,
                         strand="both", log=False, mscore=True, get_best_sequence=False,
                         working_dir=None, as_dataframe=False):
        """
        Using HOMER suite to score a set of sequences using a reference PWM motif.
        
        Report scores accordingly
        
        If mscore == True, the method will report the best score per sequence

        @:param as_dataframe: Try to return a pandas dataframe with all data, if possible
        """

        last_working_directory = abspath(os.curdir)
        if working_dir is not None:
            os.chdir(working_dir)
        # add homer HOMER path to PATH if not found
        if not "homer/bin" in os.environ["PATH"]:
            homer_path = "/home/ignacio/Software/homer/bin"
            os.environ["PATH"] += os.pathsep + homer_path
            # add the cluster path
            homer_path = "/home/rio/zaugglab/rio/Software/homer/bin"
            os.environ["PATH"] += os.pathsep + homer_path
        
        # convert sequences into fasta
        if sequences != None and tmp_fasta is None:
            print('here...')
            tmp_fasta = "../output/tmp_fasta.fasta"

            fasta_analyzer = FastaAnalyzer()
            fasta_analyzer.write_fasta_from_sequences(sequences, tmp_fasta)

        # motif width
        w = self.get_motif_width_pfm_homer(ppm_homer_path)

        print(tmp_fasta, exists(tmp_fasta))
        # homer2 find -s <seq file> -m <motif file>
        assert exists(tmp_fasta)

        cmd = ["homer2", "find", "-i", tmp_fasta, "-m", ppm_homer_path, "-mscore",
               "-strand", strand, "-offset", "0"]
        if get_best_sequence:
            cmd = ["homer2", "find", "-i", tmp_fasta, "-m", ppm_homer_path, "-mscore",
                   "-offset", "0", "-strand", strand]

        print(" ".join(cmd))
        stdoutdata, stderrdata = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE).communicate()

        output = [r for r in stdoutdata.split("\n") if len(r) != 0]
        print(stderrdata)

        # remove tmp fasta file
        # remove(tmp_fasta)
        if log:
            for r in output:
                print(r)

        if get_best_sequence:
            best_scores = {}
            for i, r in enumerate(output[::-1]):
                # retrieve the sequence according to the strand and offset,
                # given HOMER rules for defining them (right to left if +, left to right if -)
                split = r.split("\t")
                # print split[0], str(sequences[i][0])
                assert str(split[0]) == str(sequences[i][0])
                strand = split[4]
                position = int(split[1])
                start = None
                if strand == "+":
                    start = position
                elif strand == "-":
                    start = position + 1 - w
                end = start + w
                seq = sequences[i][1][start:end]                
                if strand == "-":
                    seq = self.get_complementary_seq(seq)
                header, score = split[0], float(split[-1])
                if not header in best_scores:
                    best_scores[header] = [start, strand, sequences[i][1],
                                           self.get_complementary_seq(sequences[i][1]),
                                           seq, score]
                elif score >= best_scores[header][1]:
                    best_scores[header] = [start, strand, sequences[i][1],
                                           self.get_complementary_seq(sequences[i][1]),
                                           seq, score]
                    
            if working_dir is not None:
                os.chdir(last_working_directory)

            if as_dataframe:
                df = pd.DataFrame([[header] + best_scores[header] for header in best_scores],
                                  columns=['id', 'position', 'strand', 'seq.fwd',
                                           'seq.rev', 'motif', 'score'])
                return df

            return best_scores

        #=======================================================================
        # For some reason homer2's output generate all scores in inverse order
        # w.r.t. fasta input
        #=======================================================================
        else:
            best_scores = [t.split("\t") for t in output if len(t) > 5][::-1]

            best_scores = [t[:-1] + [float(t[-1])] for t in best_scores]
            # best_scores = [t[:2] + t[4:] for t in best_scores]
            # delete tmp fasta file after running homer successfully.        
            # remove(tmp_fasta)
            if working_dir is not None:
                os.chdir(last_working_directory)
            return best_scores

    def get_flanking_regions_home_motif(self, homer_scores_dataframe, bp=4):
        '''

        Given a dataframe that contains scores generated by homer, create
        motifs that have +/- bp w.r.t. the motif. If the motif is in the
        minus strand this is solved by mapping properly on that sequence

        :param homer_scores_dataframe:
        :param bp:
        :return: dataframe with variable 'flanking' added
        '''
        df = homer_scores_dataframe
        w = len(df.loc[0]['motif'])
        p = list(df['position'])
        p = [((len(df['seq.fwd'][i]) - w) - pi)
             if df.loc[i]['strand'] == '-' else pi for i, pi in enumerate(p)]
        import sys;
        flankings = []
        for i in range(df.shape[0]):
            selected_seq = df.loc[i]['seq.fwd' if (df.loc[i]['strand'] == '+') else 'seq.rev']
            # print selected_seq, df['strand'][i], df['motif'][i], p[i], p[i] + w, selected_seq[p[i]: p[i] + w]
            motif_w_flankings = selected_seq[p[i] - bp: p[i] + w + bp]
            flankings.append(motif_w_flankings)
            # print df['seq.fwd'][i]
        # df['flanking'] = flankings
        # print df.head()
        return flankings

    def get_scores_fimo(self, sequences, ppm_meme_path,
                         tmp_fasta="../output/tmp_fasta.fasta",
                         working_dir=None):
        """
        Using FIMO suite to score a set of sequences using a reference PWM motif.
        """
        # add MEME path to PATH if not found
        if not "meme" in os.environ["PATH"]:
            meme_path = "/home/ignacio/meme/bin"
            os.environ["PATH"] += os.pathsep + meme_path
            
        # convert sequences into fasta
        if sequences != None:
            writer = open(tmp_fasta, "w")
            for i, entry in enumerate(sequences):
                snp_id, seq = entry if len(entry) == 2 else [i, entry]
                    
                # print snp_id, seq
                writer.write(">" + str(snp_id) + "\n" + seq + "\n")
            writer.close()
        
        # homer2 find -s <seq file> -m <motif file>
        cmd = ["fimo", "-oc", working_dir, ppm_meme_path, tmp_fasta]
        print(" ".join(cmd))
        os.system(" ".join(cmd))

        output = [r.strip().split("\t") for r in open(join(working_dir, "fimo.txt"))][1:]
        return output
    
    def ppm_homer_to_consensus_seq(self, ppm_path, cmp=False):
        """
        Convert to consensus sequence using rules defined by cavener
        http://nar.oxfordjournals.org/content/15/4/1353.long
        """
        ppm = [list(map(float, r)) for r in [r.strip().split("\t") for r in open(ppm_path)][1:]]
        ppm = [{nt: pi for nt, pi in zip("ACGT", p)} for p in ppm]
        
        seq = ""
        
        if cmp:
            ppm = ppm[::-1]
            for i, p in enumerate(ppm):
                ppm[i] = {"TGCA"[j]: p["ACGT"[j]] for j in range(4)}

        #=======================================================================
        # # print ppm
        #=======================================================================
        for p in ppm:
            table = None
            if isinstance(p, dict):
                table = [[nt, p[nt]] for nt, pi in zip("ACGT", p)]
            else:
                table = [[nt, pi] for nt, pi in zip("ACGT", p)]
            table.sort(key=lambda x: x[1], reverse=True)
            # print table
            p = [t[1] for t in table]
            # select first position if pi > 0.5 and pi+1 * 2 < pi
            if p[0] > 0.5 and p[1] * 2 < p[0]:
                i = [0]
            elif p[0] + p[1] > 0.75:
                i = [0, 1]
            elif p[0] + p[1] + p[2] > 0.75:
                i = [0, 1, 2]
            else:
                i = [-1] 
            selected_nt = [table[j][0] if j != -1 else "N" for j in i if i != -1]
            k = "".join(sorted(selected_nt))
            seq += self.nucleotides_group_to_iupac_code(k)
        return seq

    def get_best_scores_matrix_personal_genomes(self, motif_id, coordinates_table):
        """
        Load the scores matrix for each individual that is reported by Oana.
        
        Return a matrix that contains the scores for each position, for each alleles,
        for 75 individuals.
        """
        hmark = "H3K27AC"
        matrix_path = join("/home/ignacio/Documents/zaugglab/motifAnalysisOanaUrsu/motifs",
                           "Motif_analysis_results/Motif_Analysis_" + hmark,
                           "motif.pouya.Motif." + motif_id + "scanThresh0", "Scores",
                           "motif.pouya.Motif." + motif_id + "scanThresh0_$ALLELE$Scores.compiled.gz")
        
        query_coordinates = {t[0] + ":" + str(t[1]) + "-" + str(t[2])
                             for t in coordinates_table}
        
        # for each scores set there are two outputs: Maternal/Paternal alleles
        scores_by_position = {}
        for allele in ("paternal", "maternal"):
            path = matrix_path.replace("$ALLELE$", allele)
            rows = [r.strip().split(" ") for r in gzip.open(path)]
            headers = rows[0][1:]
            for r in rows[1:]:
                position = r[0]
                if not position in query_coordinates:
                    continue
                scores = list(map(float, r[1:]))
                if not position in scores_by_position:
                    scores_by_position[position] = {}
                scores_by_position[position][allele] = [[h, s] for h, s in zip(headers, scores)]
        return scores_by_position                
            
    def get_sequences_from_personal_genomes(self, coordinates, personal_genomes_dir,
                                            output_path=None):
        """
        Given a set of coordinates and a personal genomes directory, extract
        all sequences for all invididuals.
        
        Note: Sequences might be different, and that is the reason we need to 
        extract them all in a custom way.
        
        Output path let us retrieve sequences faster in case we have already
        generated an equivalent file in a previous run.
        """
        #=======================================================================
        # Load from previously generated file
        #=======================================================================
        output_table = None
        if output_path is not None and exists(output_path):
            output_table = pickle.load(open(output_path, "rb"))
        else:
            #===================================================================
            # Generate sequences table by reading personal genomes, one by one
            #===================================================================
            output_table = []
            for f in listdir(personal_genomes_dir):
                personal_genome_path = join(personal_genomes_dir, f)
                seqs = self.get_sequences_from_personal_genome(coordinates,
                                                               personal_genome_path)
                output_table += seqs
            if output_path is not None:
                pickle.dump(output_table, open(output_path))
        return output_table

    def convert_ppm_encode_to_homer(self, ppm_path_encode, ppm_path_homer,
                                    has_header=True, transpose=False,
                                    to_probabilities=False,
                                    basename=None):
        """
        Convert a PPM file as define by encode to another one as defined and
        necessary to run HOMER
        """
        
        # this threshold is necessary to define how low is the score we want.
        threshold = -1000000000
        
        if basename is None:
            basename = ppm_path_encode.split("/")[-1].split(".")[0]
        is_gz = ppm_path_encode.endswith(".gz")
        rows = [r for r in (gzip.open(ppm_path_encode) if is_gz else open(ppm_path_encode))]
        writer = open(ppm_path_homer, "w")
        header = rows[0].split("\t")[0].replace(">", "") if has_header else basename 
        writer.write(">" + header + "\t" + header + "\t" + str(threshold) + "\n")
        
        rows = rows[1:] if has_header else rows
        rows = [r.strip().split("\t") for r in rows]
        
        if transpose:
            rows = [[rows[j][i] for j in range(4)] for i in range(len(rows[0]))]
        for r in rows:
            if to_probabilities:
                r = [float(ri) / sum(map(float, r)) if sum(map(float, r))
                     else 0.0 for ri in r]
            writer.write("\t".join(map(str, r)) + "\n")
        writer.close()
        
    def convert_cisbp_pfm_to_jaspar(self, cisbp_dir, output_path, stop_at=None):
        """
        Convert a set of PWMs from CISBP into JASPAR format
        """
        all_data = []
        for i, f in enumerate(listdir(cisbp_dir)):
            if i % 100 == 0:
                print(i)
            header = ">" + f.replace(".pfm", "")
            lines = [r.strip() for r in open(join(cisbp_dir, f))]
            all_data.append(header + "\n")
            for nt, line in zip("ACGT", lines):
                all_data.append(nt + " [" + " ".join(line.split("\t")) + "]" + "\n")
            if stop_at != None and i >= stop_at:
                break
            
        writer = open(output_path, "w")
        writer.writelines(all_data)
        writer.close()


    def convert_ppm_uniprobe_to_encode(self, input_path, output_path):
        """
        
        """
        rows = [r.strip().split("\t") for r in open(input_path)]
        # select the important lines only (i.e. starts with "A:", "G:", "T:", or "C:"
        
        rows = {r[0][0]: r[1:] for r in rows
                if r[0] == "A:" or r[0] == "C:" or r[0] == "T:" or r[0] == "G:"}
        width = len(rows["A"])
        print(width)
        # transpose
        transposed = [[rows[k][i] for k in ("ACTG")] for i in range(width)]
        for t in transposed:
            print(t)
        
        # transpose
        writer = open(output_path, "w")
        motif_code = basename(input_path.replace(".pwm", ""))
        writer.write(">" + motif_code + "\t" + motif_code + "\t-1000000000\n")
        for r in transposed:
            print(r)
            writer.write("\t".join(r) + "\n")
        writer.close()
        
    def get_snp_alleles(self, snp_ids, delete_cache=True,
                        tmp_path="../output/snp_alleles.txt"):
        """
        This method extracts snp_alleles from reference files
        
        The final output is a dictionary that contains the SNP ID and
        the reference alleles
        """
        #===============================================================
        # Hpeak data is loaded on the fly using grep -E "^" with the Hpeak ids
        #===============================================================
        snp_alleles = {}
        if len(snp_ids) == 0:
            return snp_alleles
        
        gt_data_path = "/home/ignacio/Documents/zaugglab/localQTLs/RMatrices/snps_alleles.tsv"
        
        #=======================================================================
        # performance step:
        # instead of scanning one single file, we'll divide our genotypes
        # into 10 different files with the SNPs classified by "rs + Number",
        # where number is a digit between rs10 and 99
        #=======================================================================
        for i in range(10, 100):
            gt_data_path_i = gt_data_path.replace(".tsv", "_" + str(i) + ".tsv")
            k = "rs" + str(i)
            if not exists(gt_data_path_i):
                print(i, "creating custom allele files...")
                writer = open(gt_data_path_i, "w")
                cmd = " ".join(["grep", "-e", "rs" + str(i), gt_data_path, "|", "awk",
                                "-F", "\"\\t\"", "\'{print $3\"\\t\"$4\"\\t\"$5}\'",
                                ">", gt_data_path_i])
                print(cmd)
                os.system(cmd)
                [writer.write("\t".join(r.split("\t")[2:]))
                 for r in open(gt_data_path) if r.split("\t")[2].startswith(k)]
                writer.close()

        gt_data_tmp_path = tmp_path
        print("attempting to write genotypes to file path")
        print(gt_data_tmp_path)
        if not exists(gt_data_tmp_path):
            print("loading alleles data by grepping files...")
            print("N=", len(snp_ids), "SNP IDs")
            
            # divide the search into 90 sub-searches for SNPs that begin with
            # our required key "rs + DIGIT"
            for rs_digit_i in range(10, 100):
                gt_data_path_i = gt_data_path.replace(".tsv", "_" + str(rs_digit_i) + ".tsv")   
                grep_cmd = "grep -E \"^$CONDITION\" " + gt_data_path_i
                condition_str = ""
                selected_snps = [snp_id for snp_id in snp_ids if snp_id.startswith("rs" + str(rs_digit_i))]
                if len(selected_snps) == 0:
                    continue
                for i, snp_id in enumerate(selected_snps):
                    # some SNPs end with a ",SNPINFO" metadata field, so we need
                    # to check for both cases
                    condition_str += snp_id
                    condition_str += "\t|" + snp_id + ","
                    if i + 1 != len(selected_snps):
                        condition_str += "|"        
                grep_cmd = grep_cmd.replace("$CONDITION", condition_str)
                # print grep_cmd
                system(" ".join([grep_cmd, ">>", gt_data_tmp_path]))
        
        rows = [r.strip().split("\t") for r in open(gt_data_tmp_path)]
        if delete_cache and exists(gt_data_tmp_path):
            remove(gt_data_tmp_path)
        # read the first line of a table to get the individual names
        for r in rows:
            # the grep file prints teh file name before the SNP_ID, separated by a ":" symbol
            snp_id, alleles = r[0], r[1:]
            if "," in snp_id:
                snp_id = snp_id.split(",")[0]
            if len(alleles) == 2:
                snp_alleles[snp_id] = alleles
                
        return snp_alleles
    
    def get_pwm_scores(self, seq, pwm):
        """
        Given a sequence and a PWM, return all the possible alignments of this
        sequence on the SNP.
        """
        scores = pwm.scan_sequence(seq, normalize_score=False,
                                   scan_complementary=True, cutoff=None)
        return scores
    
    def define_high_scoring_allele(self, seq, snp_relative_position, alleles, pwm):
        """
        Using MOODS define the score for two alleles.
        Return a list [1, 0] if MOODS(seq1) > MOODS(seq2), and [0, 1] otherwise
        """
        
        a1, a2 = alleles
        seq1, seq2 = [[c for c in seq] for i in range(2)]
        seq1[snp_relative_position] = a1
        seq2[snp_relative_position] = a2
        seq1, seq2 = ["".join(s) for s in [seq1, seq2]]                  
        
        print(seq1)
        print(seq2)
        score1, score2 = [pwm.scan_sequence(s, normalize_score=False,
                                            scan_complementary=False,
                                            cutoff=None)[0][2][0] for s in [seq1, seq2]]
        # print seq1, alleles[0], score1
        # print seq2, alleles[1], score2
        # print pfm_column
        # print alleles
        
        # check score directionality agreement with PWM read
        
        # PWM frequencies are the same: we cannot decide
        # if pfm_column[a1] == pfm_column[a2]:
        #     return None
        
        if score1 > score2:
            # assert pfm_column[a1] >= pfm_column[a2]
            return [1, 0]
        elif score2 >= score1:
            # assert pfm_column[a2] >= pfm_column[a1]
            return [0, 1]

    def get_scores_snp_sequences_vs_pwm(self, seqs, pfm):
        import MOODS
        seq1, seq2 = seqs
        scores1 = MOODS.search(seq1, [pfm], 100, both_strands=True)
        scores2 = MOODS.search(seq2, [pfm], 100, both_strands=True)
        return scores1, scores2
    
    def get_motifs_aligned_on_snps(self, moods_hits, motif_width, snp_position=99):
        """
        From a set of MOODS hits, return the subset of hits that are overlapping 
        a motif. Scanning is regardless of the strand direction, because MOODS
        reports the initial position. However, this has to be checked up later
        """
        x = snp_position
        # filter motif
        filtered_hits = [h for h in moods_hits
                        if int(h[2]) <= x and int(h[2]) + motif_width >= x]
        return filtered_hits
    
    def get_spearman_pvalue_permutations(self, x, y, n_replicates=100000,
                                         shuffle_x=False, shuffle_y=True):
        """
        Get Spearman significance by permuting our values N_REPLICATES times
        
        Values are sorted, and the reference rho (initial value) is compared)
        """        
        x_tmp = [xi for xi in x] 
        y_tmp = [yi for yi in y]
        ref_rho, ref_pval = spearmanr(x, y)
        rho_values = []
        for i in range(n_replicates):
            if i % 1000 == 0:
                print(i)
            if shuffle_x:
                shuffle(x_tmp)
            if shuffle_y:
                shuffle(y_tmp)
            rho, pval = spearmanr(x_tmp, y_tmp)
            rho_values.append(rho)
        rho_values.sort()
        # plt.hist(rho_values, range=[-1.0, 1.0])
        if ref_rho <= 0:
            return (1 + sum([1 if ri < ref_rho else 0 for ri in rho_values])) / float(n_replicates)        
        return (1 + sum([1 if ri > ref_rho else 0 for ri in rho_values])) / float(n_replicates)
        
    def get_chromosome_motifs_aligned_on_snps_chromosome(self, hits, motif_width, 
                                                         snp_positions):
        """
        Return motifs that are aligned at a given SNP position 
        """        
        sorted_snp_positions = sorted(snp_positions.keys())
        assert len(sorted_snp_positions) > 0
        
        selected_hits = []        

        last_p = 0
        for h in sorted(hits, key=lambda x: x[0]):
            
            motif_start = h[0]
            motif_end = h[0] + motif_width
            seq = h[2]
            
            assert motif_end > motif_start
            
            for j in range(max(last_p - 1, 0), len(sorted_snp_positions)):
                p = sorted_snp_positions[j]
                
                if motif_start > p:
                    continue
                # motif too left does not overlap SNP,
                elif motif_start < p and p <= motif_end:
                    # print motif_start, motif_end, p
                    last_p = j
                    selected_hits.append(h + [p, snp_positions[p]])
                elif motif_end < p:
                    break

        print(len(selected_hits))
        return selected_hits
        
    def get_moods_dataframe_encode(self, moods_output_path):
        # read file using zcat
        p = sp.Popen(["zcat", moods_output_path], stdout=sp.PIPE)
        out, err = p.communicate()
        out = out.split("\n")
        df = [r.strip().split("|") for r in out if len(r) > 0]
        df = [[int(r[0]), r[1], r[2], float(r[3])] for r in df]
        return df
    
    def get_complementary_nt(self, nt):
        if nt == "A":
            return "T"
        if nt == "T":
            return "A"
        if nt == "C":
            return "G"
        if nt == "G":
            return "C"
        if nt == "N":
            return "N"
                
    def get_complementary_seq(self, s):
        return "".join([self.get_complementary_nt(nt) for nt in s][::-1])
