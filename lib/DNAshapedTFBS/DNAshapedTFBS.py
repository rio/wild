#!/usr/bin/python
#*-* coding: utf-8 *-*

""" Train and apply PSSM/TFFM/4-bits + DNAshape classifiers. """



# import the required library to run tffm_module in Python
# TODO: Test if TFFM is installed instead of using local env.
from os.path import join, exists
import sys
import pandas as pd
import tempfile
from os import system
tffm_dirs = ['/home/ignacio/Software/TFFM', '/g/scb2/zaugg/zaugg_shared/Programs/TFFM']
[sys.path.append(d) for d in tffm_dirs if exists(d)]

import imp
try:
    imp.find_module('eggs')
    tffm_found = True
except ImportError:
    tffm_found = False
if tffm_found:
    import tffm_module
import os
from os import remove
PATH = os.path.dirname(os.path.realpath(__file__))
import sys
# Local environment
# TODO: Test if TFFM is installed instead of using local env.
sys.path.append('{0}/./TFFM/'.format(PATH))
from sklearn.externals import joblib
from the_constants import BWTOOL, DNASHAPEINTER
from shapes import *
from utilities import *
from os import environ
import subprocess as sp
import threading

import time






def find_pssm_hits(pssm, seq_file, stop_at=-1, use_c_routine=True,
                   as_dataframe=False, pssm_path=None, **kwargs):
    """ Predict hits in sequences using a PSSM. """
    from operator import itemgetter
    import math
    import Bio.SeqIO
    from Bio.Alphabet import generic_dna
    from Bio.Alphabet.IUPAC import IUPACUnambiguousDNA as unambiguousDNA
    # import tffm_module
    from hit_module import HIT


    hits = []
    records = []
    if seq_file.endswith(".gz"):
        with gzip.open(seq_file, "rt") as handle:
            for record in Bio.SeqIO.parse(handle, "fasta", generic_dna):
                records.append(record)
    else:
        records = [r for r in Bio.SeqIO.parse(seq_file, 'fasta', generic_dna)]
    if use_c_routine:
        df = find_pssm_hits_c(pssm, seq_file, pssm_path=pssm_path, **kwargs)

        print df.head()
        print df.shape
        for ri, r in df.iterrows():
            # normalize the score
            hits.append(HIT(records[ri], r['start'], r['end'],
                            r['strand'], r['score.norm']))
        if as_dataframe:
            table = [[records[ri].id, r['start'], r['end'], r['strand'], r['score.norm'], r['seq']]
                     for ri, r in df.iterrows()]
            # print table[-1]
            hits = pd.DataFrame(table, columns=['description', 'start', 'end',
                                                'strand', 'score', 'seq'])
            return hits
    else:
        print seq_file
        print '# of records', len(records)
        pwm_score_range = pssm.max - pssm.min
        for i, record in enumerate(records):
            record.seq.alphabet = unambiguousDNA()

            results = pssm.search(record.seq, pssm.min)

            results = [[pos, score] for pos,score in results if not math.isnan(score)]
            scores = [[pos, score] for pos, score in results]
            if scores:
                pos_maxi, maxi = max(scores, key=itemgetter(1))
                strand = "+"
                if pos_maxi < 0:
                    strand = "-"
                    pos_maxi = pos_maxi + len(record.seq)

                # normalize the score
                hits.append(HIT(record, pos_maxi + 1, pos_maxi + pssm.length,
                                strand, (maxi - pssm.min) / pwm_score_range))
            if i % 100 == 0:
                print "# sequences being scored:", i

            if stop_at != -1 and i >= stop_at:
                break

        # for h in hits[:10]:
        #     print h
        # exit()

    return hits

def find_pssm_hits_c(pssm, fa_path, tfname='tf', tfclass='class',
                     pssm_path=None, **kwargs):
    unlink_at_end = True
    if pssm_path is not None:
        unlink_at_end = False
    if pssm_path is not None:
        print pssm_path
        if not exists(pssm_path):
            writer = open(pssm_path, 'w')
            for nt in ['A', 'C', 'G', 'T']:
                entry = pssm[nt] if not isinstance(pssm, pd.DataFrame) else pssm.transpose()[nt]
                writer.write("\t".join(map(str, entry)) + "\n")
            writer.close()
    else:
        pssm_path = tempfile.mkstemp()[1]
        writer = open(pssm_path, 'w')
        for nt in ['A', 'C', 'G', 'T']:
            entry = pssm[nt] if not isinstance(pssm, pd.DataFrame) else pssm.transpose()[nt]
            writer.write("\t".join(map(str, entry)) + "\n")
        writer.close()
    # print pssm_path

    bin_path = join("/home/ignacio/Dropbox/Eclipse_Projects/zaugglab",
                    "comb-TF-binding/combinatorial_binding_analysis_invivo/src/pssm_search/pwm_search")
    if not exists(bin_path):
        bin_path = join("/g/scb/zaugg/rio/EclipseProjects/zaugglab",
                        "comb-TF-binding/combinatorial_binding_analysis_invivo/src/pssm_search/pwm_search")

    assert exists(bin_path)

    # uncompress GZ to plain text
    if fa_path.endswith(".gz"):
        print 'uncompressing fasta...'
        new_fa_path = tempfile.mkstemp()[1]
        with open(new_fa_path, 'w') as writer:
            for line in gzip.open(fa_path):
                writer.write(line)
        fa_path = new_fa_path

    # cmd = " ".join(
    #     [bin_path, pssm_path, fa_path, str(-10000), "-b", '-n', tfname, '-c', tfclass,
    #      ">", output_hits])

    threshold = kwargs.get('threshold', -10000)
    args = " ".join([bin_path, pssm_path, fa_path, str(threshold),
                     "-b", '-n', tfname, '-c', tfclass])

    print args
    exit()

    p = sp.Popen(args, stdout=sp.PIPE, shell=True)
    output = p.communicate()[0]

    hits_c = pd.DataFrame([r.split('\t') for r in output.split("\n")[:-1]])

    # print hits_c.head()
    # print hits_c.shape

    hits_c.columns = ['description', 'lab', 'tf.name', 'tf.class',
                      'strand', 'score', 'score.norm', 'start', 'end', 'seq']
    hits_c['score'] = hits_c['score'].astype(float)
    hits_c['score.norm'] = hits_c['score.norm'].astype(float)
    hits_c['start'] = hits_c['start'].astype(int)
    hits_c['end'] = hits_c['end'].astype(int)

    if unlink_at_end:
        os.unlink(pssm_path)
    return hits_c

def find_tffm_hits(xml, seq_file, tffm_kind):
    """ Predict hits in sequences using a TFFM. """
    #import sys
    #sys.path.append("/raid6/amathelier/TFFM+DNAshape/bin/TFFM/")
    import tffm_module
    from constants import TFFM_KIND  # TFFM-framework
    if tffm_kind == 'first_order':
        tffm_kind = TFFM_KIND.FIRST_ORDER
    elif tffm_kind == 'detailed':
        tffm_kind = TFFM_KIND.DETAILED
    else:
        sys.exit('The type of TFFM should be "first_order" or "detailed".')
    tffm = tffm_module.tffm_from_xml(xml, tffm_kind)
    return [hit for hit in
            tffm.scan_sequences(seq_file, only_best=True) if hit]


def construct_classifier_input(foreground, background):
    """ Make list of classes for foreground and background. """
    import numpy as np
    classes = [1.0] * len(foreground) + [0.0] * len(background)
    return np.asmatrix(foreground + background), classes


def fit_classifier(fg_train_hits, fg_train_shapes, bg_train_hits,
                   bg_train_shapes, extension=0, bool4bits=False,
                   data_log_path=None):
    """ Fit the classifier to the training data. """
    from xgboost import XGBClassifier
    from sklearn.ensemble import GradientBoostingClassifier
    fg_train = combine_hits_shapes(fg_train_hits, fg_train_shapes, extension,
            bool4bits)
    bg_train = combine_hits_shapes(bg_train_hits, bg_train_shapes, extension,
            bool4bits)
    data, classification = construct_classifier_input(fg_train, bg_train)
    classifier= XGBClassifier(nthread=1)

    # classifier = GradientBoostingClassifier()

    # save classification data as a single matrix
    # print type(data), len(data[0]), data.shape
    if data_log_path is not None:
        # print 'writing data to', data_log_path
        df = pd.DataFrame(data, columns=['feature.' + str(i) for i in range(data.shape[1])])
        df['classification'] = classification
        df.to_csv(data_log_path, compression='gzip', sep='\t', index=None)

    classifier.fit(data, classification)
    return classifier


def make_predictions(clf, tests, hits, thr):
    """ Predict hits from the tests using the classifier. """
    predictions = {'peak_id': [], 'start': [], 'end': [], 'strand': [],
                   'sequence': [], 'proba': []}
    for indx, proba in enumerate(clf.predict_proba(tests)):
        if proba[1] >= thr:
            hit = hits[indx]
            if hit:
                predictions['peak_id'].append(hit.seq_record.name)
                predictions['start'].append(hit.start)
                predictions['end'].append(hit.end)
                predictions['strand'].append(hit.strand)
                if hit.strand == '-':
                    sequence = ''.join(
                        hit.seq_record.seq[
                            hit.start - 1:hit.end].reverse_complement())
                else:
                    sequence = ''.join(hit.seq_record[hit.start - 1:hit.end])
                predictions['sequence'].append(sequence)
                predictions['proba'].append(proba[1])
    return predictions


def print_predictions(predictions, output):
    """ Print the predictions in the output file. """
    import pandas as pd
    pd_predictions = pd.DataFrame(predictions)
    pd_predictions = pd_predictions[['peak_id', 'start', 'end', 'strand', 'sequence', 'proba']]
    pd_predictions.to_csv(output, compression='gzip', index=None, sep='\t')


def apply_classifier(hits, argu, bool4bits=False, hits_shapes=None):
    """ Apply the DNAshape based classifier. """
    # Two options, 1) doing sequence by sequence but it means doing a lot of
    # bwtool calls and I/O, 2) doing all sequences as one batch but means that
    # we need to associate back probas to hits. I choose 2) to reduce I/O.

    # just create shape features if we are not already giving an input file
    if argu.first_shape is None:
        hits_shapes = None
    if hits_shapes is None:
        if argu.first_shape is not None:
            hits_shapes = get_shapes(hits, argu.in_bed, argu.first_shape,
                                     argu.second_shape, argu.extension, argu.scaled,
                                     fasta_file=argu.in_fasta)
        else:
            hits_shapes = None

    print 'loading classifier located at'
    print argu.classifier
    classifier = joblib.load(argu.classifier)
    if bool4bits:
        tests = combine_hits_shapes(encode_hits(hits), hits_shapes,
                                    argu.extension, bool4bits)
    else:
        tests = combine_hits_shapes(hits, hits_shapes, argu.extension,
                                    bool4bits)
    print '# of hits', len(hits)
    print '# of entries to score', len(tests)
    # Need to print the results by associating the probas to the hits
    print argu.threshold
    predictions = make_predictions(classifier, tests, hits, argu.threshold)

    output_path = argu.output
    df = pd.DataFrame(tests, columns=['feature.' + str(i) for i in
                                     range(len(tests[0]))])

    print output_path.replace(".scores", ".test.tsv.gz")
    df['proba'] = predictions['proba']
    df.to_csv(output_path.replace(".tsv.gz", ".matrix.tsv.gz"),
              compression='gzip', sep='\t', index=None)

    print '# of predictions', len(predictions['proba'])
    print_predictions(predictions, argu.output)


def tffm_apply_classifier(argu):
    """ Apply the TFFM + DNA shape classifier. """
    hits = find_tffm_hits(argu.tffm_file, argu.in_fasta, argu.tffm_kind)
    if hits:
        apply_classifier(hits, argu)
    else:
        with open(argu.output, 'w') as stream:
            stream.write('No hit predicted\n')

import cPickle
def pssm_apply_classifier(argu):
    """ Apply the TFFM + DNA shape classifier. """
    if argu.jasparid:
        pssm = get_jaspar_pssm(argu.jasparid)
    else:
        pssm = get_jaspar_pssm(argu.jasparfile, False)

    hits_shapes = None
    if argu.in_scores is None:
        hits = find_pssm_hits(pssm, argu.in_fasta)
    else:
        hits, shapes = cPickle.load(open(argu.in_scores, 'rb'))
        print "loading -ives"
        # filter the shapes that we need in the bg according to the cases
        # we actually used (i.e. a particular k-fold
        identifiers = {r.strip().replace(">", "") for r in open(argu.in_fasta)
                       if r.startswith(">")}
        # these are the positions for entries we need from the bigger bunch
        idx = [i for i, bg in enumerate(hits)
               if bg.seq_record.id in identifiers]
        hits = [hits[i] for i in idx]
        hits_shapes = [[s[i] for i in idx] for s in shapes]
        # here we must select somehow hits that are valid for this run

    if hits:
        apply_classifier(hits, argu, hits_shapes=hits_shapes)
    else:
        with open(argu.output, 'w') as stream:
            stream.write('No hit predicted\n')


def binary_apply_classifier(argu):
    """ Apply the 4-bits + DNA shape classifier. """
    if argu.jasparid:
        pssm = get_jaspar_pssm(argu.jasparid)
    else:
        pssm = get_jaspar_pssm(argu.jasparfile, False)
    hits = find_pssm_hits(pssm, argu.in_fasta)
    if hits:
        apply_classifier(hits, argu, True)
    else:
        with open(argu.output, 'w') as stream:
            stream.write('No hit predicted\n')


def train_classifier(fg_hits, bg_hits, argu, bool4bits=False,
                     fg_shapes=None, bg_shapes=None):
    """ Train the DNAshape-based classifier. """


    if argu.first_shape is not None:
        if fg_shapes is None:
            fg_shapes = get_shapes(fg_hits, argu.fg_bed, argu.first_shape,
                    argu.second_shape, argu.extension, argu.scaled, fasta_file=argu.fg_fasta,
                                   use_bwtool=True)
        if bg_shapes is None:
            bg_shapes = get_shapes(bg_hits, argu.bg_bed, argu.first_shape,
                    argu.second_shape, argu.extension, argu.scaled, fasta_file=argu.bg_fasta,
                                   use_bwtool=True)
    else:
        fg_shapes, bg_shapes = None, None

    output_path = argu.output
    if bool4bits:
        classifier = fit_classifier(encode_hits(fg_hits), fg_shapes,
                encode_hits(bg_hits), bg_shapes, argu.extension, bool4bits)
    else:
        print 'fitting classifier...'
        classifier = fit_classifier(fg_hits, fg_shapes, bg_hits, bg_shapes,
                argu.extension, bool4bits,
                                    data_log_path=output_path.replace(".pkl",
                                                                      ".train.tsv.gz"))

    print 'scoring train entries for pos and neg'
    print argu.scoresposout, argu.scoresnegout
    if argu.scoresposout is not None and argu.scoresnegout is not None:
        print 'saving scores...'
        fg_train = combine_hits_shapes(fg_hits, fg_shapes, 0, False)
        bg_train = combine_hits_shapes(bg_hits, bg_shapes, 0, False)
        for next_hits, next_train, scores_outpath in zip([fg_hits, bg_hits],
                                                         [fg_train, bg_train],
                                        [argu.scoresposout, argu.scoresnegout]):

            predictions = make_predictions(classifier, next_train, next_hits, argu.threshold)
            df = pd.DataFrame(next_train, columns=['feature.' + str(i) for i in
                                              range(len(next_train[0]))])
            df['proba'] = predictions['proba']
            next_output = scores_outpath.replace(".tsv.gz", ".matrix.tsv.gz")
            print 'writing scores...'
            print next_output
            df.to_csv(next_output, compression='gzip', sep='\t', index=None)
    print 'done...'

    # print 'saving classifier...', output_path
    joblib.dump(classifier, output_path)

def tffm_train_classifier(argu):
    """ Train a TFFM + DNA shape classifier. """
    fg_hits = find_tffm_hits(argu.tffm_file, argu.fg_fasta, argu.tffm_kind)
    bg_hits = find_tffm_hits(argu.tffm_file, argu.bg_fasta, argu.tffm_kind)
    train_classifier(fg_hits, bg_hits, argu)


def pssm_train_classifier(argu):
    """ Train a PSSM + DNA shape classifier. """
    if argu.jasparid:
        pssm = get_jaspar_pssm(argu.jasparid)
    else:
        pssm = get_jaspar_pssm(argu.jasparfile, False)

    fg_shapes, bg_shapes = None, None
    if (argu.in_scores_pos is not None and argu.in_scores_neg is not None):
        # print 'loading scores from previously stored files...'
        print "loading +ives"
        fg_hits, fg_shapes = cPickle.load(open(argu.in_scores_pos, 'rb'))
        print "loading -ives"
        bg_hits, bg_shapes = cPickle.load(open(argu.in_scores_neg, 'rb'))

        # filter the shapes that we need in the bg according to the cases
        # we actually used (i.e. a particular k-fold
        bg_identifiers = {r.strip().replace(">", "") for r in open(argu.bg_fasta)
                          if r.startswith(">")}

        # these are the positions for entries we need from the bigger bunch
        bg_idx = [i for i, bg in enumerate(bg_hits)
                  if bg.seq_record.id in bg_identifiers]
        bg_hits = [bg_hits[i] for i in bg_idx]
        bg_shapes = [[shapes[i] for i in bg_idx] for shapes in bg_shapes]
    else:
        # print 'scoring +ive examples...'
        # print pssm
        fg_hits = find_pssm_hits(pssm, argu.fg_fasta, argu.max)
        # print 'scoring -ive examples...'
        bg_hits = find_pssm_hits(pssm, argu.bg_fasta, argu.max)

    # print 'training...'
    train_classifier(fg_hits, bg_hits, argu, fg_shapes=fg_shapes,
                     bg_shapes=bg_shapes)


def binary_train_classifier(argu):
    """ Train a 4-bits + DNA shape classifier. """
    if argu.jasparid:
        pssm = get_jaspar_pssm(argu.jasparid)
    else:
        pssm = get_jaspar_pssm(argu.jasparfile, False)
    fg_hits = find_pssm_hits(pssm, argu.fg_fasta)
    bg_hits = find_pssm_hits(pssm, argu.bg_fasta)
    print 'applying classifier...'
    train_classifier(fg_hits, bg_hits, argu, True)


##############################################################################
#                               MAIN
##############################################################################
from argparsing import *
if __name__ == "__main__":
    arguments = arg_parsing()
    arguments.func(arguments)
    # import sys
    # from os.path import join
    # if len(sys.argv) == 1:
    #     commands_file = join("/home/ignacio/Dropbox/Eclipse_Projects/zaugglab",
    #                          "mathelier_2016_analysis/tmp/DNAshapedTFBS_commands.txt")
    #     for i, line in enumerate(open(commands_file)):
    #         line = line.strip()
    #         print line
    #         continue
    #         split = line.replace(";", "").split(" ")
    #         sys.argv = [sys.argv[0]] + split
    #
    #         # running with these arguments
    #         arguments = arg_parsing()
    #         arguments.func(arguments)
