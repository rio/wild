BWTOOL = "/g/scb2/zaugg/zaugg_shared/Programs/bwtool/bwtool"
from os.path import exists
BWTOOL = "bwtool" if not exists(BWTOOL) else BWTOOL

DNASHAPEINTER = {'HelT': (30.94, 38.05), 'MGW': (2.85, 6.20),
                 'ProT': (-16.51, -0.03), 'Roll': (-8.57, 8.64)}
