from utilities import *
from the_constants import BWTOOL, DNASHAPEINTER
from lib.BindingSitesAnalyzer import BindingSitesAnalyzer
from lib.FastaAnalyzer import FastaAnalyzer
import os
from os.path import exists, basename, join
from shutil import copy2

def get_scores(in_file, shape=None, scaled=False):
    """ Get DNAshape values on single lines. """
    with open(in_file) as stream:
        scores = []
        for line in stream:
            values = [item for item in line.rstrip().split()[-1].split(',')
                      if not_na(item)]
            values = [eval(value) for value in values]
            if scaled:
                mini, maxi = DNASHAPEINTER[shape]
                values, _, _ = scale01(values, mini, maxi)
            scores.append(values)
        return scores


def combine_hits_shapes(hits, shapes, extension=0, binary_encoding=False):
    """ Combine DNA sequence and shape features.

    The hit scores (PSSM or TFFM) or 4-bits encoding are combined with DNAshape
    features in vectors for classif.

    """
    comb = []
    index = -1

    for hit in hits:
        if hit:
            index += 1
            if shapes:
                hit_shapes = []
                for indx in xrange(len(shapes)):
                    # print index, len(shapes[indx])
                    hit_shapes += shapes[indx][index]

                if (not binary_encoding and
                        (len(hit_shapes) ==
                            len(shapes) * (len(hit) + 2 * extension)
                        )
                   ):
                    comb.append([hit.score] + hit_shapes)
                elif (binary_encoding and
                        (len(hit_shapes) ==
                            len(shapes) * (len(hit) / 4 + 2 * extension)
                        )
                     ):
                    comb.append(hit + hit_shapes)
            elif binary_encoding:
                comb.append(hit)
            else:
                comb.append([hit.score])

    return comb


def get_shapes(hits, bed_file, first_shape, second_shape, extension=0,
        scaled=False, use_bwtool=True, fasta_file=None):
    """ Retrieve DNAshape feature values for the hits. """
    bigwigs = first_shape + second_shape
    print 'bigWig', bigwigs
    print bigwigs
    import subprocess
    import os

    # print bed_file
    # print bed_file
    peaks_pos = get_positions_from_bed(bed_file)
    with open(os.devnull, 'w') as devnull:
        tmp_file = print_extended_hits(hits, peaks_pos, extension)

        if use_bwtool:
            shapes = ['HelT', 'ProT', 'MGW', 'Roll', 'HelT2', 'ProT2', 'MGW2',
                    'Roll2']

            # shapes = ['HelT'] #  'ProT', 'MGW', 'Roll']

            hits_shapes = []
            for indx, bigwig in enumerate(bigwigs):
                if indx >= len(shapes):
                    continue
                # print indx, 'running bwtool', bigwig
                if bigwig:
                    out_file = '{0}.{1}'.format(tmp_file, shapes[indx])


                    # fix: check if the TMPDIR directory exists
                    # this saves speed in the execution, by using RAM for those
                    # files in specific nodes

                    tmp_dir = os.environ['TMPDIR'] if 'TMPDIR' in os.environ else '/tmp'
                    print tmp_dir
                    print exists(tmp_dir)
                    fast_bigwig = join(tmp_dir, basename(bigwig))
                    if not exists(fast_bigwig):
                        print 'copying tmp shape bigwig file to output...'
                        print 'source', bigwig
                        print 'target', fast_bigwig
                        copy2(bigwig, fast_bigwig)
                        print fast_bigwig
                        print 'file copied'
                    bigwig = fast_bigwig

                    cmd = [BWTOOL, 'ex', 'bed', tmp_file, bigwig, out_file]
                    print ' '.join(cmd)
                    subprocess.call([BWTOOL, 'ex', 'bed', tmp_file, bigwig, out_file],
                                    stdout=devnull)
                    if indx < 4:
                        hits_shapes.append(get_scores(out_file, shapes[indx], scaled))
                    else:
                        hits_shapes.append(get_scores(out_file, shapes[indx]))
            subprocess.call(['rm', '-f', '{0}.HelT'.format(tmp_file),
                '{0}.MGW'.format(tmp_file), '{0}.ProT'.format(tmp_file),
                '{0}.Roll'.format(tmp_file), '{0}.HelT2'.format(tmp_file),
                '{0}.MGW2'.format(tmp_file), '{0}.ProT2'.format(tmp_file),
                '{0}.Roll2'.format(tmp_file),tmp_file])
        else:
            bs_analyzer = BindingSitesAnalyzer()
            fasta_analyzer = FastaAnalyzer()
            fastas = fasta_analyzer.get_fastas_from_file(fasta_file)
            shapes = [t[1] for t in bs_analyzer.get_shape_from_sequences([t[1] for t in fastas])]
            shape_ids = ['HelT', 'ProT', 'MGW', 'Roll', 'HelT2', 'ProT2', 'MGW2',
                        'Roll2']
            hits_shapes = [[] for i in range(len(shapes))]
            for i, entry_shapes in enumerate(shapes):
                for j, shape_id in enumerate(shape_ids):
                    if j >= 4:
                        break
                    hits_shapes[i].append(shapes[i][shape_id])

        return hits_shapes
