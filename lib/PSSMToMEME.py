

from os.path import basename

class PSSMToMEME:
    
    def pssm_to_meme(self, input_path, output_path):
        """
        It converts a PSSM into a MEME output file
        """
        
        s = """
MEME version 4

ALPHABET= ACGT
strands: + -

MOTIF &name
log-odds matrix: alength= 4 w= &width\n"""
        rows = [r.strip().split(" ")[1:] for r in open(input_path).readlines()]
        s = s.replace("&name", basename(input_path))
        s = s.replace("&width", str(len(rows)))
        for r in rows:
            s += " ".join(r) + "\n"
        writer = open(output_path, "w")
        writer.write(s)
        writer.close()
        
        
         
