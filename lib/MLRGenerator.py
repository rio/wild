from lib.utils import *
from sklearn.linear_model import LinearRegression, ElasticNet
from sklearn.model_selection import cross_val_score
import numpy as np
from numpy import average
import random
from random import shuffle
from scipy.stats import linregress
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold
from sklearn.linear_model import Ridge
from sklearn.metrics.regression import r2_score
from lib.MachineLearning import MachineLearning

class MLRGenerator:
    def __init__(self):
        self.import_source = False

    def fit(self, x, y):
        # rpy2 imports
        import rpy2
        import rpy2.robjects as robjects
        from rpy2.robjects import pandas2ri
        pandas2ri.activate()  # to convert pandas to R dataframe
        # we just import this source once
        if not self.import_source:
            robjects.r('''
                   source('../src/02_2_train_test_glm.R')
            ''')
            self.import_source = True

        df = [list(xi) for xi, yi in zip(x, y)]

        df = pd.DataFrame(df, columns=['x.' + str(i) for i in range(len(df[0]) - 1)] + ['y'])
        clf = LinearRegression()

        get_r2_glm = robjects.globalenv['get_r2_glm']
        robjects.globalenv['df'] = df

        print(df.head())
        r_sq = get_r2_glm(robjects.globalenv['df'])
        return np.asarray(r_sq)[0]

    def get_r2_closed_form_solution(self, y_pred, y_test):
        # calculate R2 with a closed form solution
        SSres = sum([(x[0] - x[1]) ** 2 for x in zip(y_pred, y_test)])
        SStot = sum([(x - np.mean(y_test)) ** 2 for x in y_test])
        r2 = 1 - (SSres / SStot)
        return r2

    def get_mlr_r2_values_glmnet(self, X_train, y_train, X_test=None, y_test=None,
                                 glm_path=join(RSCRIPTS_DIR, "02_2_train_test_glm.R")):
        """
        Given two paths, training and testing, return R2 values using GLMNET (R package)
        :param train_path:
        :param test_path:
        :return:
        """

        print(glm_path)
        print(exists(glm_path))
        # we just import this source once
        print(exists(glm_path))
        print(os.path.relpath(glm_path, '.'))
        print(self.import_source)
        if not self.import_source:
            robjects.r('''
                   source(\'''' + os.path.relpath(glm_path, '.') + '''\')
            ''')
            self.import_source = True

        df_train = [list(xi) + [yi] for xi, yi in zip(X_train, y_train)]
        if X_test is not None and y_test is not None:
            df_test = [list(xi) + [yi] for xi, yi in zip(X_test, y_test)]
        else:
            df_test = df_train

        df_train = pd.DataFrame(df_train, columns=['x.' + str(i) for i in
                                                   range(len(df_train[0]) - 1)] + ['y'])
        df_test = pd.DataFrame(df_test, columns=['x.' + str(i) for i in
                                                   range(len(df_test[0]) - 1)] + ['y'])

        # print 'opening R'
        r_train_test_r2 = robjects.globalenv['get_train_test_r2_glm']
        robjects.globalenv['df_train'] = df_train
        robjects.globalenv['df_test'] = df_test

        # return train and test element
        # print 'calculating R^2'
        output = r_train_test_r2(robjects.globalenv['df_train'], robjects.globalenv['df_test'])
        return np.asarray(output)

    def one_hot_encoding_to_sequence(self, v):
        return "".join(['ACGT'[i] for i in [j % 4 for j in range(len(v)) if v[j]]])
    def sequence_to_hot_encoding(self, s):
        d = {"A": "1000", "C": "0100", "G": "0010", "T": "0001"}
        return list(map(int, "".join(d[nt] for nt in s)))

    def cross_validation(self, x, y, k=10, seed=500,
                         glm_path="../src/02_2_train_test_glm.R"):
        '''
        Peformn K-fold cross-validations, returning both performance metrics and
        coefficients. If k=None, then perform one analysis only, with all data for
        training and testing
        :param x:
        :param y:
        :param k:
        :param seed:
        :return:
        '''
        # print 'converting X matrix to array...'
        X = np.array(x)
        # y = np.array(y)
        # MIN-MAX normalization
        # print 'normalizing Y values...'
        max_y = max(y)
        min_y = min(y)
        y = np.array([(yi - min_y) / (max_y - min_y) for yi in y])
        # all the lengths in our x matrix have to be the same

        print(y)
        # print 'checking rows in x-matrix...'

        if len({len(xi) for xi in X}) != 1:
            print({len(xi) for xi in X})
            assert len({len(xi) for xi in X}) == 1
        # print 'preparing k-fold groups...'

        kf = KFold(n_splits=k if k is not None else 10,
                   shuffle=True, random_state=seed)

        table = []
        kfold_i = 1

        if k is not None:
            print('Running k-fold cross-validation...')

        for train, test in kf.split(y):
            if k is not None:
                print('# k-fold', kfold_i)
                X_train, X_test, y_train, y_test = X[train], X[test], y[train], y[test]
            else:
                X_train, X_test, y_train, y_test = X, X, y, y
            # print values to tmp dir
            output_glm = self.get_mlr_r2_values_glmnet(X_train, y_train, X_test, y_test,
                                                       glm_path=glm_path)
            r_sq_train, r_sq_test, intercept = output_glm[:3]
            coefficients = list(output_glm[3:])
            table.append([kfold_i, len(y_train), len(y_test), r_sq_train, r_sq_test, intercept] + coefficients)
            kfold_i += 1

            # if k is None we break after the first iteration
            if k is None:
                break

        df = pd.DataFrame(table, columns=['k.fold', 'n.train', 'n.test',
                                          'r.sq.train', 'r.sq.test', 'intercept'] +
                                         ["x." + str(ci) for ci in range(len(coefficients))])
        return df

    def get_rsquared_deltas(self, x_df, y, nfold=None,
                            glm_path="../src/02_2_train_test_glm.R",
                            classifier=None, add_ep=False):
        '''
        Add shape features linearly
        :param x_df: a dataframe that contains both sequence and shape features
        :param y:
        :param k:
        :return:
        '''

        table = []

        x_seq = x_df[[k for k in x_df.columns if k[0] in {'A', 'C', 'G', 'T'}]]
        x_shape = x_df[[k for k in x_df.columns if not k[0] in {'A', 'C', 'G', 'T'}]]

        n_nucleotides = x_seq.shape[1] / 4

        n_shape_positions = sum([k.startswith('MGW.') for k in x_df.columns])

        rsq_seq, rsq_shape = None, None
        if nfold is not None:
            print('calculating using kfold')
            from lib.MachineLearning import MachineLearning
            rsq_seq = MachineLearning.KFoldRidgeRegression(np.array(x_seq), y, k=nfold)['r2.test'].mean()
            rsq_shape = MachineLearning.KFoldRidgeRegression(np.array(x_shape), y, k=nfold)['r2.test'].mean()

        else:
            # get values as calculated by a python-library (sklearn)
            model = classifier.fit(x_seq, y)
            y_pred_all = model.predict(x_seq)
            rsq_seq = r2_score(y, y_pred_all)
            model = classifier.fit(x_shape, y)
            y_pred_all = model.predict(x_shape)
            rsq_shape = r2_score(y, y_pred_all)


        print(rsq_shape)
        print("r.squared.sequence (alone)", rsq_seq)
        print("r.squared.shape (alone)", rsq_shape)

        # print n_nucleotides
        print(n_shape_positions)
        # we can do N_NUCLEOTIDES - 2 modifications
        d = (n_nucleotides - (n_shape_positions) + 1) if n_nucleotides == n_shape_positions else 3
        print(d)
        for i in range(n_shape_positions if n_nucleotides == n_shape_positions else
                       (n_nucleotides - 4)):
            # print i
            # print '# Modifying MLR by adding shape features at position', i
            requested_labels = ['MGW.' + "%02d" % (i + d),
                                'ROLL.' + "%02d" % (i + d), 'ROLL.' + "%02d" % (i + d + 1),
                                'PROT.' + "%02d" % (i + d),
                                'HELT.' + "%02d" % (i + d), 'HELT.' + "%02d" % (i + d + 1),
                                'MGWxMGW.' + "%02d" % (i + d), 'MGWxMGW.' + "%02d" % (i + d + 1),
                                'ROLLxROLL.' + "%02d" % (i + d),
                                'PROTxPROT.' + "%02d" % (i + d), 'PROTxPROT.' + "%02d" % (i + d + 1),
                                'HELTxHELT.' + "%02d" % (i + d)]

            if add_ep:
                requested_labels += ['EP.' + "%02d" % (i + d),
                                     'EPxEP.' + "%02d" % (i + d)]

            # print requested_labels
            groups = ['MGW', 'ROLL', 'PROT', 'HELT', None]
            if add_ep:
                groups = ['MGW', 'ROLL', 'PROT', 'HELT', 'EP', None]
            for group in groups:
                # print group
                if group is not None:
                    subgroup_requested_labels = [k for k in requested_labels if group in k]
                else:
                    subgroup_requested_labels = requested_labels
                x_shapes_i = x_df[[k for k in subgroup_requested_labels if k in x_df]]

                # for shape - shape_i we want all the shape labels but the one we are
                # positioned on
                x_shape_minus_shape_i = x_shape[[k for k in x_shape
                                                 if not k in list(subgroup_requested_labels)]]

                x_aug_n_shape_i = pd.concat([x_seq, x_shapes_i], axis=1)
                # print x_aug.head()
                # when adding the next shape feature, we are ading four individual values at the
                # same time, one for each position
                # print x_aug.head()

                rsq_seq_n_shape_i, rsq_shape_minus_shape_i = None, None
                if nfold is not None:
                    # HERE IT IS IN
                    from lib.MachineLearning import MachineLearning
                    rsq_seq_n_shape_i = MachineLearning.KFoldRidgeRegression(np.array(x_aug_n_shape_i), y, k=nfold)['r2.test'].mean()
                    rsq_shape_minus_shape_i = MachineLearning.KFoldRidgeRegression(np.array(x_shape_minus_shape_i), y, k=nfold)['r2.test'].mean()
                else:
                    # get values as calculated by a python-library (sklearn)
                    model = classifier.fit(x_aug_n_shape_i, y)
                    y_pred_all = model.predict(x_aug_n_shape_i)
                    rsq_seq_n_shape_i = r2_score(y, y_pred_all)
                    model = classifier.fit(x_shape_minus_shape_i, y)
                    y_pred_all = model.predict(x_shape_minus_shape_i)
                    rsq_shape_minus_shape_i = r2_score(y, y_pred_all)


                table.append([i, 'ALL' if group is None else group,
                              rsq_seq, rsq_seq_n_shape_i, rsq_shape, rsq_shape_minus_shape_i])
                # print table[-1]

        df = pd.DataFrame(table, columns=['i', 'group', 'r.sq.seq', 'r.sq.seq.and.shape.i',
                                          'r.sq.shape', 'r.sq.shape.minus.shape.i'])

        df['delta.add.shape'] = ((df['r.sq.seq.and.shape.i'] - df['r.sq.seq'])) * 100  # / sel['r.sq.seq'] * 100).abs()
        df['delta.remove.shape'] = ((df['r.sq.shape.minus.shape.i'] - df['r.sq.shape'])) * 100  # / sel['r.sq.shape'] * 100).abs()
        df['min.change'] = df[['delta.add.shape', 'delta.remove.shape']].abs().min(axis=1)

        df['delta.add.shape.perc'] = ((df['r.sq.seq.and.shape.i'] - df['r.sq.seq'])) * 100 / df['r.sq.seq']
        df['delta.remove.shape.perc'] = ((df['r.sq.shape.minus.shape.i'] - df['r.sq.shape'])) * 100 / df['r.sq.shape']
        df['min.change.perc'] = df[['delta.add.shape.perc', 'delta.remove.shape.perc']].abs().min(axis=1)
        return df

    def get_rsquared_deltas_higher_order(self, x_df, y, n_kfold=10,
                            glm_path="../src/02_2_train_test_glm.R",
                            classifier=None, add_ep=False):
        '''
        Add shape features linearly
        :param x_df: a dataframe that contains both sequence and shape features
        :param y:
        :param k:
        :return:
        '''

        table = []

        x_1mer = x_df[[k for k in x_df.columns if len(k.split(".")[0]) == 1]]
        x_2mer_3mer = x_df[[k for k in x_df.columns if len(k.split(".")[0]) > 1]]

        n_nucleotides = x_1mer.shape[1] / 4

        n_higher_order = sum([len(k.split(".")[0]) > 1 for k in x_df.columns])

        rsq_1mer, rsq_2mer_3mer = None, None
        if classifier is None:
            rsq_1mer = self.cross_validation(x_1mer, y, glm_path=glm_path,
                                            k=None)['r.sq.test'].mean()
            # print rsq_seq
            rsq_higher_order = self.cross_validation(x_2mer_3mer, y, glm_path=glm_path,
                                                     k=None)['r.sq.test'].mean()

        else:
            # get values as calculated by a python-library (sklearn)
            model = classifier.fit(x_1mer, y)
            y_pred_all = model.predict(x_1mer)
            rsq_1mer = r2_score(y, y_pred_all)
            model = classifier.fit(x_2mer_3mer, y)
            y_pred_all = model.predict(x_2mer_3mer)
            rsq_2mer_3mer = r2_score(y, y_pred_all)


        print("r.squared.1mer", rsq_1mer)
        print("r.squared.2mer+3mer", rsq_2mer_3mer)

        print(n_nucleotides)
        print(n_higher_order)
        for i in range(n_nucleotides - 1):
            print('# Modifying MLR by adding shape features at position', i)
            requested_labels = [k for k in x_df.columns if len(k.split(".")[0]) > 1 and int(k.split(".")[1]) == i]
            # print requested_labels
            # print len(requested_labels)

            subgroup_requested_labels = requested_labels
            x_higher_order_i = x_df[[k for k in subgroup_requested_labels if k in x_df]]

            # for shape - shape_i we want all the shape labels but the one we are
            # positioned on
            x_2mer_3mer_order_minus_i = x_2mer_3mer[[k for k in x_2mer_3mer
                                                     if not k in list(subgroup_requested_labels)]]

            x_1mer_n_2mer_3mer_i = pd.concat([x_1mer, x_higher_order_i], axis=1)
            # print x_aug.head()
            # when adding the next shape feature, we are ading four individual values at the
            # same time, one for each position
            # print x_aug.head()

            rsq_1mer_n_2mer_3mer_i, rsq_2mer_3mer_minus_2mer_3mer_i = None, None
            if classifier is None:
                df = self.cross_validation(x_1mer_n_2mer_3mer_i, y, k=None, glm_path=glm_path)
                # test a model that contains all the seq features and shape features at position $
                rsq_1mer_n_2mer_3mer_i = df[ 'r.sq.test'].mean()

                df = self.cross_validation(x_2mer_3mer_order_minus_i, y, k=None, glm_path=glm_path)
                # test a model that contains all the seq features and shape features at position $

                rsq_2mer_3mer_minus_2mer_3mer_i = df['r.sq.test'].mean()
            else:
                # get values as calculated by a python-library (sklearn)
                model = classifier.fit(x_1mer_n_2mer_3mer_i, y)
                y_pred_all = model.predict(x_1mer_n_2mer_3mer_i)
                rsq_1mer_n_2mer_3mer_i = r2_score(y, y_pred_all)
                model = classifier.fit(x_2mer_3mer_order_minus_i, y)
                y_pred_all = model.predict(x_2mer_3mer_order_minus_i)
                rsq_2mer_3mer_minus_2mer_3mer_i = r2_score(y, y_pred_all)


            table.append([i, 'ALL',
                          rsq_1mer, rsq_1mer_n_2mer_3mer_i, rsq_2mer_3mer, rsq_2mer_3mer_minus_2mer_3mer_i])
            # print table[-1]

        df = pd.DataFrame(table, columns=['i', 'group', 'r.sq.1mer', 'r.sq.1mer.and.2mer3mer.i',
                                          'r.sq.2mer3mer', 'r.sq.2mer3mer.minus.2mer3mer.i'])

        df['delta.add.2mer3mer'] = ((df['r.sq.1mer.and.2mer3mer.i'] - df['r.sq.1mer'])) * 100  # / sel['r.sq.seq'] * 100).abs()
        df['delta.remove.2mer3mer'] = ((df['r.sq.2mer3mer.minus.2mer3mer.i'] - df['r.sq.2mer3mer'])) * 100  # / sel['r.sq.shape'] * 100).abs()
        df['min.change'] = df[['delta.add.2mer3mer', 'delta.remove.2mer3mer']].abs().min(axis=1)

        df['delta.add.2mer3mer.perc'] = ((df['r.sq.1mer.and.2mer3mer.i'] - df['r.sq.1mer'])) * 100 / df['r.sq.1mer']
        df['delta.remove.2mer3mer.perc'] = ((df['r.sq.2mer3mer.minus.2mer3mer.i'] - df['r.sq.2mer3mer'])) * 100 / df['r.sq.2mer3mer']
        df['min.change.perc'] = df[['delta.add.2mer3mer.perc', 'delta.remove.2mer3mer.perc']].abs().min(axis=1)
        return df