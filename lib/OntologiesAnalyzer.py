'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.DISEASES import DISEASES
from lib.HPOAnalyzer import HPOAnalyzer
class OntologiesAnalyzer:

    @staticmethod
    def get_obos_by_ontology(ontology_id=None):
        obo_by_ontology = {}

        p =  {'DISEASES': '/g/scb2/zaugg/rio/data/DISEASES/HumanDO.obo.txt',
              'HPO': '/g/scb2/zaugg/rio/data/hpo/hp.obo'}

        for next_ont_id in ['DISEASES', 'HPO', 'GO']:
            if ontology_id is not None and next_ont_id != ontology_id:
                continue
            lines = "".join(open(p[next_ont_id]).readlines())
            terms = {}
            for t in lines.split("\n\n"):
                if not t.startswith('[Term]'):
                    continue
                keys = {}
                for t in t.split("\n"):
                    k = t.split(": ")[0]
                    v = ": ".join(t.split(": ")[1:])
                    keys[k] = v
                goid = keys['id'] if 'id' in keys else None
                name = keys['name'] if 'name' in keys else None
                is_a = keys['is_a'].split(" ! ")[0] if 'is_a' in keys else None
                is_obsolete = keys['is_obsolete'] if 'is_obsolete' in keys else None
                assert not goid in terms
                terms[goid] = [goid, name, is_a, is_obsolete]
            obo_by_ontology[ontology_id] = terms
        return obo_by_ontology


    @staticmethod
    def get_obo_father_terms(obo, k, n=None, log=False):
        hierarchy = []

        if log:
            print(obo[k])

        while obo[k][-2] is not None:
            k = obo[k][-2]
            if log:
                print(obo[k])
            hierarchy.append(k)
            if n is not None and len(hierarchy) >= n + 1:
                break
        return hierarchy

    @staticmethod
    def get_gos(overwrite=False):

        f = os.path.abspath(os.path.join(__file__, os.pardir))
        ontologies_bkp = join(f, '..', 'data', 'ontologies_bkp.pkl')

        if not exists(ontologies_bkp) or overwrite:
            print('load DISEASE ontologies...')
            dis = DISEASES.get_data_human_experiments()
            text = DISEASES.get_data_text_mining()
            dis['ontology.group'] = 'DISEASES'
            text['ontology.group'] = 'DISEASES'

            print('load Human Phenotypes')
            hpo = HPOAnalyzer.get_hpo_phen_to_gene_table()
            hpo.columns = ['disease.id', 'disease.name', 'gene.id', 'gene.name']
            hpo['ontology.group'] = 'HPO'

            df = pd.concat([dis[['gene.name', 'disease.name', 'disease.id', 'ontology.group']],
                            text[['disease.id', 'disease.name', 'gene.name', 'ontology.group']],
                            hpo[['disease.id', 'disease.name', 'gene.name', 'ontology.group']]]).reset_index(drop=True)

            print('load conventional GOs...')

            great_tools_dir = '/g/scb2/zaugg/rio/data/greatTools'
            go = DataFrameAnalyzer.read_tsv_gz(join(great_tools_dir, 'data', 'hg19/genes_by_go.tsv.gz'), header=None)
            names_by_go = DataFrameAnalyzer.get_dict(DataFrameAnalyzer.read_tsv(join(great_tools_dir,
                                                                                     'data', 'hg19/go_names.tsv.txt'),
                                                                                header=None), 0, 1)
            go['name'] = go[1].map(names_by_go)
            df.columns = ['id', 'name', 'gene.name', 'ontology.group']
            go.columns = ['gene.name', 'id', 'name']
            go['ontology.group'] = 'GO'
            df = pd.concat([df, go]).reset_index(drop=True)

            print('grouping GOs by # of genes...')
            n_by_goid = {k: len(set(grp['gene.name'])) for k, grp in df.groupby('id')}
            ontologies = df, n_by_goid
            # print 'Lymphoma term found?', 'DOID:0060058' in selected_goid
            DataFrameAnalyzer.to_pickle(ontologies, ontologies_bkp)

        return DataFrameAnalyzer.read_pickle(ontologies_bkp)
