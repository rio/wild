'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *


class DISEASES:


    def __init__(self):
        self.disease_txt_mining = DISEASES.get_data_text_mining()
        self.diseases_experiments = DISEASES.get_data_human_experiments()

    @staticmethod
    def get_data_text_mining():
        p = '/g/scb2/zaugg/rio/data/DISEASES/human_disease_textmining_full.tsv'
        diseases_text_mining = DataFrameAnalyzer.read_tsv(p, header=None,
                                                          columns=['gene.id', 'gene.name', 'disease.id', 'disease.name',
                                                                   'z.score',
                                                                   'confidence', 'source'])


        return diseases_text_mining

    @staticmethod
    def get_all_data():
        dis = DISEASES.get_data_human_experiments()
        text = DISEASES.get_data_text_mining()
        df = pd.concat([dis[['gene.name', 'disease.name', 'disease.id', 'confidence']],
                        text[['disease.id', 'disease.name', 'gene.name', 'z.score', 'confidence']]]).reset_index(drop=True)
        return df

    @staticmethod
    def get_data_human_experiments():
        p = '/g/scb2/zaugg/rio/data/DISEASES/human_disease_experiments_full.tsv'
        diseases_experiments = DataFrameAnalyzer.read_tsv(p, header=None,
                                                          columns=['gene.id', 'gene.name', 'disease.id', 'disease.name',
                                                                   'source',
                                                                   'evidence', 'confidence'])
        return diseases_experiments

    @staticmethod
    def get_diseases_hierarchy():
        from lib.OntologiesAnalyzer import OntologiesAnalyzer
        OntologiesAnalyzer.get_obos_by_ontology('DISEASES')

    def get_diseases_vs_great(self, great_res, fdr_thr=0.1, category={'text.mining', 'experiments'}, association_type='both'):

        diseases = self.diseases_experiments if not 'text.mining' in category else self.disease_txt_mining

        print('filtering by gene name...')
        great_res['tf'] = np.where(pd.isnull(great_res['tf']), 'NA', great_res['tf'])
        gene_names = set(great_res['tf'])
        print(diseases[['gene.name']].head())
        # print diseases_experiments['gene.name'].value_counts()

        diseases_experiments = diseases[diseases['gene.name'].isin(gene_names)]


        if association_type == 'both':
            sel = []
            for disease_name, grp in diseases_experiments.groupby('disease.id'):
                if grp.shape[0] >= 2:
                    sel.append(grp)
            diseases_experiments = pd.concat(sel).reset_index(drop=True)

        print('filtering by DOs...')
        great_res = great_res[great_res['category'] == 'Disease Ontology']

        print('filtering by TF...')
        great_res['tf'] = np.where(pd.isnull(great_res['tf']), 'NA', great_res['tf'])

        table = []

        print(diseases_experiments.shape[0])
        for peak_group, grp in great_res.groupby('peaks.group'):
            for tf, grp2 in grp.groupby('tf'):
                sig = grp2[grp2['Best_QValue'] < fdr_thr]
                print(peak_group, tf, grp2.shape)
                for confidence_thr in sorted(diseases_experiments['confidence'].value_counts().index):

                    diseases_sel = diseases_experiments[diseases_experiments['confidence'] > confidence_thr]
                    print(confidence_thr, diseases_sel.shape[0])
                    t = [peak_group, tf, confidence_thr, diseases_sel.shape[0],
                         len(set(sig['ID']).intersection(set(diseases_sel['disease.id'])))]
                    print(t)
                    table.append(t)
        res = pd.DataFrame(table, columns=['peak.group', 'tf', 'confidence.thr', 'n.diseases', 'n.intersection'])
        res['fraction'] = res['n.intersection'] / res['n.diseases'].astype(float)
        res['code'] = res['peak.group'] + ":" + res['tf']
        return res


        # plot


