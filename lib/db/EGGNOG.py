'''
Created on 

DESCRIPTION

@author: natalieRomanov
'''

import sys
import sqlite3

from lib.utils import *
from lib.BSubFacade import BSubFacade
from lib.FastaAnalyzer import FastaAnalyzer
import requests
from Bio import Phylo

# from becklab.src.gremlim.wrapper.sqlFacade import sqlFacade
# from becklab.src.gremlim.wrapper.TaxonIDMapper import TaxonIDMapper

from StringIO import StringIO
import urllib2

class EGGNOG(object):

    @staticmethod
    def get_eggnogs_from_uniprot(uniprot_id):
        url = 'https://www.uniprot.org/uniprot/%s.txt' % uniprot_id
        return EGGNOG.get_text_from_url(url)

    @staticmethod
    def get_local_eggnog_v5_dir():
        return "/g/scb2/zaugg/rio/data/eggnogdb_5.0"

    @staticmethod
    def download_per_tax_data(taxid, parms, output_dir=None, version=5):
        if output_dir is None:
            output_dir = join(EGGNOG.get_local_eggnog_v5_dir(), 'per_tax_level')
        assert exists(output_dir)

        for parm in parms:
            ext = 'tar' if ('algs' in parm or 'hmms' in parm) else 'tsv.gz'
            url = 'http://eggnog5.embl.de/download/eggnog_5.0/per_tax_level/%s/%s_%s.%s' % (taxid, taxid, parm, ext)
            print 'downloading eggnog per tax data',  (taxid, parm)
            print url
            print 'into'

            next_output_dir = join(output_dir, str(taxid))
            if not exists(next_output_dir):
                mkdir(next_output_dir)
            next_output_path = join(next_output_dir, basename(url))
            print next_output_path
            if exists(next_output_path) and filesize(next_output_path) == 0:
                remove(next_output_path)
            if not exists(next_output_path):
                print 'downloading file (currently it does not exist...'
                system('wget %s -O %s' % (url, next_output_path))
            else:
                print 'file exists. Skip...'

    @staticmethod
    def get_text_from_url(url):
        output = urllib2.urlopen(url).read()
        request = urllib2.Request(url)
        request.add_header('Accept-encoding', 'gzip')
        response = urllib2.urlopen(request)
        if response.info().get('Content-Encoding') == 'gzip':
            print 'Downloaded url data seems to be compressed...unzipping'
            buf = StringIO(response.read())
            f = gzip.GzipFile(fileobj=buf)
            output = f.read()
        return output

    @staticmethod
    def get_bulk_eggnog_alignments(eggnog_ids, bkp_path, overwrite=False, **kwargs):
        if not exists(bkp_path) or overwrite:
            alignments_by_eggnog = {eggnog_id: EGGNOG.get_alignment_from_eggnog(eggnog_id, **kwargs) for eggnog_id in eggnog_ids}
            DataFrameAnalyzer.to_pickle(alignments_by_eggnog, bkp_path)
        return DataFrameAnalyzer.read_pickle(bkp_path)

    @staticmethod
    def get_members_by_tax(taxid, output_dir=None):
        taxid = str(taxid)
        if output_dir is None:
            output_dir = join(EGGNOG.get_local_eggnog_v5_dir(), 'per_tax_level')
        assert exists(output_dir)
        parm = 'members'
        EGGNOG.download_per_tax_data(taxid, [parm], output_dir)
        ext = 'tar' if ('algs' in parm or 'hmms' in parm) else 'tsv.gz'
        filename = '%s_%s.%s' % (taxid, parm, ext)
        next_output_dir = join(output_dir, str(taxid))
        next_output_path = join(next_output_dir, filename)
        members_df = DataFrameAnalyzer.read_tsv_gz(next_output_path, header=None)
        members_df.columns = ['taxon.id', 'eggnog.id', 'n.proteins', 'n.taxons', 'proteins', 'taxons']
        return members_df


    @staticmethod
    def get_annotations_by_tax(taxid, output_dir=None):
        taxid = str(taxid)
        if output_dir is None:
            output_dir = join(EGGNOG.get_local_eggnog_v5_dir(), 'per_tax_level')
        assert exists(output_dir)
        parm = 'annotations'
        EGGNOG.download_per_tax_data(taxid, [parm], output_dir)
        ext = 'tar' if ('algs' in parm or 'hmms' in parm) else 'tsv.gz'
        filename = '%s_%s.%s' % (taxid, parm, ext)
        next_output_dir = join(output_dir, str(taxid))
        next_output_path = join(next_output_dir, filename)
        annot_df = DataFrameAnalyzer.read_tsv_gz(next_output_path, header=None)
        annot_df.columns = ['taxon.id', 'eggnog.id', 'unknown', 'description']
        return annot_df


    @staticmethod
    def get_eggnog_sequences(eggnog_id):
        taxon_ids_by_lineage = EGGNOG.get_taxon_ids_by_hierarchy_all()
        prefix = 'ENOG50' if not eggnog_id.startswith('KOG') else ''
        aln = EGGNOG.get_alignment_from_eggnog(prefix + eggnog_id)
        print '# sequences before filtering', len(aln)
        return aln

    @staticmethod
    def get_alignment_from_eggnog(eggnog_id, type='raw', version=5):
        url = join('http://eggnogapi%s.embl.de/nog_data/text/%s_alg/%s' % (str(version), type, eggnog_id))
        print 'downloading eggnog id %s' % eggnog_id
        print url
        lines = EGGNOG.get_text_from_url(url).split("\n")

        fastas = []
        seq = None
        header = None
        for r in lines:
            r = r.strip()
            if r.startswith(">"):
                if seq != None and header != None:
                    fastas.append([header, seq])
                seq = ""
                header = r[1:]
            else:
                if seq != None:
                    seq += r
                else:
                    seq = r
        fastas.append([header, seq])
        # assert that all lengths are the same
        assert len({len(s) for h, s in fastas}) == 1
        return fastas

    @staticmethod
    def get_taxid_info(taxid_info_path = None):
        taxid_info_path = taxid_info_path if taxid_info_path is not None else '/g/scb2/zaugg/rio/data/eggnogdb_5.0/e5.taxid_info.tsv'
        df = DataFrameAnalyzer.read_tsv(taxid_info_path)
        return df

    @staticmethod
    def get_taxon_ids_by_hierarchy_all(taxid_info_path=None):
        df = EGGNOG.get_taxid_info(taxid_info_path=taxid_info_path)
        lineage_name_by_taxid = EGGNOG.get_lineage_names_by_lineage_id(taxid_info_path)
        members_by_id = {}
        for ri, r in df.iterrows():
            next_ids = map(int, r['Taxid Lineage'].split(","))
            for i, g in enumerate(next_ids):
                if not g in members_by_id:
                    members_by_id[g] = set()
                members_by_id[g].add(next_ids[-1])
        return members_by_id

    @staticmethod
    def get_taxon_hierarchy_by_taxid(taxid, scopes='taxid', taxid_info_path=None):
        res = []
        taxid = int(taxid)
        df = EGGNOG.get_taxid_info(taxid_info_path=taxid_info_path)
        lineage_name_by_taxid = EGGNOG.get_lineage_names_by_lineage_id(taxid_info_path)
        sel = df[df['# Taxid'] == taxid]
        assert sel.shape[0] == 1
        for g in list(sel['Taxid Lineage']):
            hierarchy_i = 1
            for g in g.split(",")[::-1]:
                next = df[
                    df['Taxid Lineage'].str.startswith(g + ",") | df['Taxid Lineage'].str.contains(',' + g + ",") |
                    df['Taxid Lineage'].str.endswith(',' + g)]
                n = next.shape[0]
                members = {t for t in next['# Taxid']}
                assert len(members) == n
                res.append([int(g), lineage_name_by_taxid[int(g)], next.shape[0], members, hierarchy_i])
                hierarchy_i += 1
        res = pd.DataFrame(res, columns=['lineage.id', 'lineage.name', 'n', 'taxon.ids', 'hierarchy'])
        return res

    @staticmethod
    def get_lineage_id_by_lineage_name(taxid_info_path=None):
        lineage_by_taxid = EGGNOG.get_lineage_names_by_lineage_id()
        return {v: k for k, v in lineage_by_taxid.iteritems()}

    @staticmethod
    def get_lineage_names_by_lineage_id(taxid_info_path=None):
        df = EGGNOG.get_taxid_info(taxid_info_path=taxid_info_path)
        lineage_name_by_taxid = {}
        for ri, r in df.iterrows():
            names, taxids = r['Named Lineage'].replace(", ", "_").split(","),\
                            map(int, r['Taxid Lineage'].replace(", ", "_").split(","))
            assert len(names) == len(taxids)
            for a, b in zip(names, taxids):
                if b in lineage_name_by_taxid:
                    assert lineage_name_by_taxid[b] == a
                    continue
                lineage_name_by_taxid[b] = a
        return lineage_name_by_taxid

    @staticmethod
    def split_uniprot_mappings_by_first_char():
        import string
        for c in string.ascii_uppercase:
            ref_path = '/g/scb2/bork/romanov/eggnog/idmapping.dat.gz'
            output_dir = '/g/scb2/zaugg/rio/data/uniprot_mappings/'
            print c
            cmd =  'zcat %s | grep \'^%s\' | gzip > %s' % (ref_path, c, join(output_dir, 'idmapping_%s.dat.gz' % c))
            print cmd
            system(cmd)

    @staticmethod
    def get_eggnogs_from_uniprot_local(uniprot_id, taxon_id_target, taxon_id_group, data_dir='/g/scb2/zaugg/rio/data/eggnogdb'):
        #
        #  data_dir = '/g/scb2/bork/romanov/eggnog'
        id_mappings_path = join('/g/scb2/zaugg/rio/data/uniprot_mappings/idmapping_%s.dat.gz' % uniprot_id[:2])

        if not exists(id_mappings_path):
            # ref_uniprot_path = '/g/scb2/bork/romanov/eggnog/idmapping.dat.gz'
            ref_uniprot_path = join('/g/scb2/zaugg/rio/data/uniprot_mappings/idmapping_%s.dat.gz' % uniprot_id[:1])
            if not exists(ref_uniprot_path):
                print ref_uniprot_path, 'not found...'
                print 'please run EGGNOG.split_uniprot_mappings_by_first_char (slow method)'
                assert 1 > 2
            main_mapping_cmd = "zcat %s | grep \'^%s\' | gzip > %s" % (ref_uniprot_path, uniprot_id[:2], id_mappings_path)
            print main_mapping_cmd
            print 'mapping for UniProt prefix in main path...'
            system(main_mapping_cmd)
            print 'done...'

        system("zcat %s | grep \'^%s\' > /tmp/%s" % (id_mappings_path, uniprot_id, uniprot_id))

        df = DataFrameAnalyzer.read_tsv('/tmp/%s' % uniprot_id, header=None)
        df = df[df[0].str.contains(uniprot_id)]

        valid_ids = set([s.split(":")[-1] for s in df[2]])

        print '# of valid IDs', len(valid_ids)

        members = EGGNOG.get_members_by_tax(taxon_id_group)
        # filter by taxon id if given
        print 'subsetting nogs mapping table by taxon id...'
        members = members[members['proteins'].astype(str).str.contains(str(taxon_id_target) + ".")]
        members['protein.ids'] = [";".join([".".join(s.split(".")[:3]) for s in r.split(",") if s.startswith(str(taxon_id_target) + ".")])
                                  for r in members['proteins']]

        print 'mapping possible IDs...'
        members['has.valid.id'] = False
        last_hits = 0
        for k in valid_ids:
            # the valid ID cannot be the taxon target ID (redundancy) --> e.g. "9606.9606" is not valid
            # this is part of 'NCBI_TaxID' in the dataframe df
            if str(taxon_id_target) == k:
                continue
            next_k = str(taxon_id_target) + "." + ".".join(k.split(".")[:2])
            next_k_full = str(taxon_id_target) + "." + k

            members['has.valid.id'] = np.where(members['protein.ids'].str.contains(next_k) |
                                           # nog['protein.ids'].str.contains(k.split(".")[1]) |
                                               members['protein.ids'].str.contains(next_k_full), True, members['has.valid.id'])
            n_curr_hits = members[members['has.valid.id']].shape[0]
            if n_curr_hits > last_hits:
                last_hits = n_curr_hits
                print next_k_full, 'has found hits...'


        print members[members['has.valid.id']][['eggnog.id', 'protein.ids']]
        print '# of rows with valid IDs', members[members['has.valid.id']].shape[0]
        return set(members[members['has.valid.id']]['eggnog.id'])

    @staticmethod
    def get_tree_by_id_from_uniprots(uniprot_ids, taxon_id, local=False,
                                     tree_dir='/g/scb2/zaugg/rio/EclipseProjects/zaugglab/becklab/data/eggnog'):
        nogs_by_uniprot = {}
        tree_by_id = {}  # id is UNIPROTID_EGGNOGID # Eukarya or LUCA
        for i, uniprot_id in enumerate(uniprot_ids):
            print i, uniprot_id, 'out of', len(uniprot_ids)
            if not uniprot_id in nogs_by_uniprot:
                if not local:
                    nogs_by_uniprot[uniprot_id] = EGGNOG.get_eggnogs_from_uniprot(uniprot_id)
                    for nog in nogs_by_uniprot[uniprot_id].split("\n"):
                        if 'eggNOG' in nog:
                            print nog
                            eggnog_id = nog.split(";")[1].strip()
                            if not eggnog_id in tree_by_id:
                                tree_by_id[uniprot_id + "_" + eggnog_id] = EGGNOG.get_eggnog_tree_from_id(eggnog_id)
                else:
                    nogs_by_uniprot[uniprot_id] = EGGNOG.get_eggnogs_from_uniprot_local(uniprot_id, taxon_id=taxon_id)
                    print '# nogs mapped', len(nogs_by_uniprot[uniprot_id])
                    assert len(nogs_by_uniprot[uniprot_id]) < 3
                    for eggnog_id in nogs_by_uniprot[uniprot_id]:
                        tree_by_id[uniprot_id + "_" + eggnog_id] = EGGNOG.get_eggnog_tree_from_id(eggnog_id)
        return tree_by_id

    @staticmethod
    def prepare_gremlin_queries_from_eggnog_tree_pairs(tree_by_id, ref_species,
                                                       bkp_queries=None):
        if bkp_queries is None:
            print 'please provide an output bkp path for future reference. e.g. ../../../data/queries_gremlim_eggnog.tsv.gz'
        if not exists(bkp_queries):
            res = []
            i = 0
            ntotal = len([c for c in combinations(tree_by_id, 2)])
            for id1, id2 in combinations(tree_by_id, 2):  # total ~1770
                i += 1
                if i % 10 == 0:
                    print i, 'out of', ntotal
                uniprot1, nog1 = id1.split("_")
                uniprot2, nog2 = id2.split("_")
                print uniprot1, nog1, uniprot2, nog2
                # if nog1 != 'ENOG410XPFM' or nog2 != 'ENOG410XRNW':
                #     continue
                t1, t2 = tree_by_id[id1], tree_by_id[id2]
                names_by_level_t1 = EGGNOG.get_tree_elements_by_level(t1, ref_species)
                names_by_level_t2 = EGGNOG.get_tree_elements_by_level(t2, ref_species)

                for level in names_by_level_t1:
                    g1 = names_by_level_t1[level][0]
                    g2 = {g for k in names_by_level_t2 for g in names_by_level_t2[k][0]}
                    taxons1 = {n.split(".")[0] for n in g1}
                    g2_n_g1 = {g for g in g2 if g.split(".")[0] in taxons1}
                    res.append([id1, id2, ref_species, level, len(g1), len(g2), len(g2_n_g1)])

            res = pd.DataFrame(res, columns=['id.1', 'id.2', 'ref.species', 'level', 'n.1', 'n.2', 'n.common'])
            DataFrameAnalyzer.to_tsv_gz(res, bkp_queries)
        return DataFrameAnalyzer.read_tsv_gz(bkp_queries)

    @staticmethod
    def get_tree_elements_by_level(tree, ref_species):

        sp_by_taxon_id = TaxonIDMapper.instance().mapper

        names_by_level = {}
        level_counter = 0
        all_names = set()
        last_set = set()
        for c in tree.find_clades():
            if str(c) != 'Clade':
                continue
            # print c, len(c.get_terminals()), len(c.get_nonterminals())
            names = {n.name for n in c.get_terminals() if n.name is not None}
            protein_names = {n.name.split(".")[1] for n in c.get_terminals() if n.name is not None}
            taxons = {n.name.split(".")[0] for n in c.get_terminals() if n.name is not None}
            species = {sp_by_taxon_id[int(taxon)] for taxon in taxons if int(taxon) in sp_by_taxon_id}
            names_non_terminal = {n.name.split(".")[1] for n in c.get_nonterminals() if n.name is not None}
            for n in names:
                all_names.add(n)
            if ref_species in species:
                # print ''
                # print len(names), species # len(names_non_terminal)
                # print 'new', {sp for sp in last_set if not sp in species}
                last_set = species

                names_by_level[level_counter] = names
                level_counter += 1
            # if level_counter == 2:
            #     stop()

        for k in names_by_level:
            names_by_level[k] = names_by_level[k], all_names - names_by_level[k]

        # reverse indexes names
        max_idx = max(names_by_level)
        names_by_level_reversed = {}
        for k in names_by_level:
            names_by_level_reversed[max_idx - k] = names_by_level[k]

        names_by_level = names_by_level_reversed
        # remove sets where k and k+1 have exactly the same elements
        i = 0
        while i in names_by_level and i + 1 in names_by_level:
            a, b = names_by_level[i]
            a_next, b_next = names_by_level[i + 1]
            if len(a) == len(a_next) and len(b) == len(b_next):
                del names_by_level[i]
                for j in range(i + 1, max_idx - 1):
                    names_by_level[j] = names_by_level[j + 1]
            i += 1
        return names_by_level


    @staticmethod
    def get_eggnog_tree_from_id(eggnog_id):
        url = 'http://eggnogapi.embl.de/nog_data/text/tree/%s' % eggnog_id
        data = EGGNOG.get_text_from_url(url)
        return Phylo.read(StringIO(data), 'newick')

    def __init__(self, keyword, **kwargs):
        self.restrict_search2enog = kwargs.get('restrict_search2enog', True)
        self.online_mode = kwargs.get('online_mode', True)
        self.keyword = keyword

        print('mapping nog data')
        self.df = EGGNOG.map_nog(self.keyword)

        print('retrieving msa')
        if self.online_mode == True:
            self.msa_dict = EGGNOG.retrieve_eggnog_msa_online(self.df)
            self.tax_dict = EGGNOG.read_taxonomy_dictionary()
            self.msa_dict = EGGNOG.add_taxonomy_information(self.msa_dict, self.tax_dict)
        else:
            self.msa_dict = EGGNOG.retrieve_eggnog_msa_offline(self.df)

        print('rearranging msa')
        self.rearranged_msa = EGGNOG.rearrange_msa(self.df, self.msa_dict)

    @staticmethod
    def map_nog(keyword, **kwargs):
        restrict_search2enog = kwargs.get('restrict_search2enog', True)

        if restrict_search2enog == True:
            standard_tables = ['nog_all_trimmed_alignments_with_alias']
        else:
            standard_tables = ['nog_all_trimmed_alignments_with_alias', 'cog_all_trimmed_alignments_with_alias']

        conn = sqlFacade.connect('eggnog_database.db', folder='/g/scb2/bork/romanov/eggnog/')

        table_dict = sqlFacade.extract_KeywordEntries_from_tables(conn, standard_tables, keyword, 'alias')
        nog_df = table_dict['nog_all_trimmed_alignments_with_alias']
        cog_df = table_dict['cog_all_trimmed_alignments_with_alias']
        df = pd.concat([nog_df, cog_df])
        return df

    @staticmethod
    def retrieve_eggnog_msa_online(df):

        nog_groups = list(df.nog)
        nog_groups = [item.split('.')[1] for item in nog_groups]

        msa_dict = dict((key, list()) for key in nog_groups)
        for nog in nog_groups:
            print(nog)
            f = requests.get('http://eggnogapi.embl.de/nog_data/text/trimmed_alg/' + str(nog))
            msa_list = [str(item) for item in f.text.split('\n')]
            indices = list()
            seqs = list()
            for m in msa_list[:-1]:
                if m.startswith('>') == True:
                    indices.append(m[1:])
                else:
                    seqs.append(m)
            assert (len(indices) == len(seqs))
            msa_data = pd.DataFrame({'protein': indices, 'seq': seqs})
            msa_dict[nog] = msa_data
        return msa_dict

    @staticmethod
    def retrieve_eggnog_msa_offline(df):

        nog_groups = list(df.nog)
        nog_groups = [item.split('.')[1] for item in nog_groups]

        msa_dict = dict((key, list()) for key in nog_groups)

        conn = sqlFacade.connect('eggnog_database.db', folder='/g/scb2/bork/romanov/eggnog/')

        for nog in nog_groups:
            if nog.startswith('ENOG') == True:
                table_dict = sqlFacade.extract_KeywordEntries_from_tables(conn, ['nog_trimmed_alignments'], nog, 'nog')
                nogs = table_dict['nog_trimmed_alignments']
                msa_data = pd.DataFrame(
                    {'protein': list(nogs['protein_id']), 'seq': list(nogs['seq']), 'taxid': list(nogs['tax_name'])})
                msa_dict[nog] = msa_data
            else:
                table_dict = sqlFacade.extract_KeywordEntries_from_tables(conn, ['cog_trimmed_alignments'], nog, 'nog')
                nogs = table_dict['cog_trimmed_alignments']
                msa_data = pd.DataFrame(
                    {'protein': list(nogs['protein_id']), 'seq': list(nogs['seq']), 'taxid': list(nogs['tax_name'])})
                msa_dict[nog] = msa_data
        return msa_dict

    @staticmethod
    def rearrange_msa(df, msa_dict):

        nog_groups = list(df.nog)
        nog_groups = [item.split('.')[1] for item in nog_groups]

        rearranged_msa = dict((key, list()) for key in nog_groups)
        for i, row in df.iterrows():
            protein = row['protein_id']
            nog = row['nog']
            msa_data = msa_dict[nog.split('.')[1]]
            other_ids = list(set(msa_data.protein).difference(set([protein])))
            msa_data.index = msa_data.protein
            rearr_msa_data = msa_data.T[[protein] + other_ids].T
            rearranged_msa[nog.split('.')[1]] = rearr_msa_data

        return rearranged_msa

    @staticmethod
    def add_taxonomy_information(msa_dict, tax_dict):

        for nog in msa_dict.keys():
            print(nog)
            msa_data = msa_dict[nog]
            taxes = [int(item.split('.')[0]) for item in list(msa_data.protein)]

            missing_taxes = list()
            for tax in taxes:
                try:
                    tax_dict[tax]
                except:
                    missing_taxes.append(tax)
            if len(missing_taxes) != 0:
                raise Exception('update taxonomy dictionary')

            msa_data['tax_id'] = pd.Series(list(taxes), index=msa_data.index)
            taxonomy_names = [tax_dict[tax] for tax in taxes]
            msa_data['tax_name'] = pd.Series(taxonomy_names, index=msa_data.index)
            msa_dict[nog] = msa_data

        return msa_dict

    @staticmethod
    def read_taxonomy_dictionary(folder='/g/scb2/zaugg/rio/EclipseProjects/zaugglab/becklab/data'):
        tax_dict = DataFrameAnalyzer.read_pickle(join(folder, 'taxonomy_summary_dictionary.pkl'))
        missing_taxes = {65071: 'Globisporangium ultimum',
                         6085: 'Hydra vulgaris',
                         9478: 'Tarsius syrichta',
                         5085: 'Neosartorya fumigata',
                         9601: 'Pongo abelii'}
        tax_dict.update(missing_taxes)
        return tax_dict


class EGGNOG_CONCATENATOR(object):
    def __init__(self, m1, m2, protein1, protein2):

        self.protein1 = protein1
        self.protein2 = protein2
        self.msa_dict1 = m1.rearranged_msa
        self.msa_dict2 = m2.rearranged_msa

        self.nog_combinations = self.get_nog_combinations()

        for nog1, nog2 in self.nog_combinations:
            concat_df = EGGNOG_CONCATENATOR.concatenate_msa_data(self.msa_dict1, self.msa_dict2, nog1, nog2)
            EGGNOG_CONCATENATOR.generate_concatenated_fasta(concat_df, self.protein1, self.protein2, [nog1, nog2])

    def get_nog_combinations(self):
        msa_dict1, msa_dict2 = self.msa_dict1, self.msa_dict2

        nog_combinations = list()
        for nog1 in msa_dict1.keys():
            for nog2 in msa_dict2.keys():
                nog_combinations.append([nog1, nog2])
        return nog_combinations

    @staticmethod
    def concatenate_msa_data(msa_dict1, msa_dict2, nog1, nog2):

        result = pd.merge(msa_dict1[nog1], msa_dict2[nog2], on='tax_id')

        passed_tax_ids = list()
        filtered_list = list()
        for i, row in result.iterrows():
            tax_id = int(row['tax_id'])
            if tax_id not in passed_tax_ids:
                passed_tax_ids.append(tax_id)
                filtered_list.append(list(row))
        df = pd.DataFrame(filtered_list)
        df.columns = result.columns

        print('# of common species:', len(df))
        return df

    @staticmethod
    def generate_concatenated_fasta(concat_df, protein1, protein2, nogCombination, **kwargs):
        folder = kwargs.get('folder', '/g/scb2/bork/romanov/wp_couplings/data/')

        conc_folder = folder + 'concatenate/'
        if os.path.exists(conc_folder) == False:
            os.makedirs(conc_folder)

        concatenated_fasta = list()
        monomer1_fasta = list()
        monomer2_fasta = list()
        for i, row in concat_df.iterrows():
            monomer1 = '>' + row['protein_x']
            monomer2 = '>' + row['protein_y']
            seq1 = row['seq_x']
            seq2 = row['seq_y']
            concatenated_fasta.append(monomer1 + '_' + monomer2)
            concatenated_fasta.append(seq1 + seq2)

            monomer1_fasta.append(monomer1)
            monomer1_fasta.append(seq1)
            monomer2_fasta.append(monomer2)
            monomer2_fasta.append(seq2)

        export_text = '\n'.join(concatenated_fasta)
        export_text_m1 = '\n'.join(monomer1)
        export_text_m2 = '\n'.join(monomer2)

        o = open(conc_folder + 'concat_' + protein1 + '_' + protein2 + '_' + '_'.join(nogCombination) + '.fasta', 'w')
        o.write(export_text)
        o.close()

        o = open(
            conc_folder + 'concat_' + protein1 + '_' + protein2 + '_' + '_'.join(nogCombination) + '_monomer1.fasta',
            'w')
        o.write(export_text_m1)
        o.close()

        o = open(
            conc_folder + 'concat_' + protein1 + '_' + protein2 + '_' + '_'.join(nogCombination) + '_monomer2.fasta',
            'w')
        o.write(export_text_m2)
        o.close()