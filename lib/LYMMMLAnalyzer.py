'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
import json
class LYMMMLAnalyzer:

    @staticmethod
    def get_expression(gene_name, subdirectory=None, dataset=None):
        error = subdirectory is None or dataset is None
        if error:
            print('please indicate subdirectory (matrix/pheno), and dataset (mmml/mmml_pheno)')
            assert  error

        lymmml_dir = '/g/scb2/zaugg/rio/data/lymmml'
        if not exists(lymmml_dir):
            mkdir(lymmml_dir)
        output_path = join(lymmml_dir, "%s_%s.tsv" % (dataset, gene_name))
        lymmml_path = 'https://lymmml.spang-lab.de/api/v3/data/%s' % subdirectory
        command = 'curl -H "Content-Type: application/json" -d \'{\"dataset\":\"%s\",\"identifiers\":\"%s\"}\' %s > %s' % \
                  (dataset, gene_name, lymmml_path, output_path)
        print(command)
        if not exists(output_path):
            system(command)
        return json.load(open(output_path))

    @staticmethod
    def get_survival_status():
        lymmml_dir = '/g/scb2/zaugg/rio/data/lymmml'
        if not exists(lymmml_dir):
            mkdir(lymmml_dir)
        output_path = join(lymmml_dir, "survival.tsv")

        # get survival values
        cmd = 'curl -H "Content-Type: application/json" -d \'{\"dataset\":\"mmml_pheno\", \"identifiers\":"survival_status\"}\' https://lymmml.spang-lab.de/api/v3/data/pheno > %s' % output_path
        if not exists(output_path):
            system(cmd)
        out = json.load(open(output_path))
        out = pd.DataFrame(out['data'][0]['data'])
        return out

    @staticmethod
    def get_survival_days():
        lymmml_dir = '/g/scb2/zaugg/rio/data/lymmml'
        if not exists(lymmml_dir):
            mkdir(lymmml_dir)
        output_path = join(lymmml_dir, "survival_days.tsv")

        # get survival values
        cmd = 'curl -H "Content-Type: application/json" -d \'{\"dataset\":\"mmml_pheno\", \"identifiers\":"survival_time_days\"}\' https://lymmml.spang-lab.de/api/v3/data/pheno > %s' % output_path
        if not exists(output_path):
            system(cmd)
        out = json.load(open(output_path))
        out = pd.DataFrame(out['data'][0]['data'])
        return out

    @staticmethod
    def get_data_matrix(gene_name, **kwargs):
        data = LYMMMLAnalyzer.get_expression(gene_name, **kwargs)
        return pd.DataFrame(data['data'][0]['data'])

    @staticmethod
    def get_identifier_type():
        return LYMMMLAnalyzer.get_data_matrix('type', subdirectory='pheno', dataset='mmml_pheno')