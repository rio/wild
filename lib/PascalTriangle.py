'''
Created on 

A graph representation of a Pascal Triangle, able to be queried very fast when looking up for terms in the formal row,col
DESCRIPTION

@author: ignacio
'''
from lib.utils import *
import bisect
from bisect import bisect_left

class PascalTriangle():
    def __init__(self):
        self.t = {}
        self.t[0] = {}
        self.t[0][0] = 1
        self.rows = [0]

    def get_pascal_row_values(self, n):
        line = [1]
        for k in range(n):
            line.append(line[k] * (n - k) / (k + 1))
        return line

    def create_pascal_row(self, r):
        if not r in self.t or len(self.t) != r:
            values = self.get_pascal_row_values(r)
            nodes = {}
            for c, v in enumerate(values):
                node = v
                nodes[c] = node
            self.t[r] = nodes
            bisect.insort(self.rows, r)
            # if there is an n - 1 row, update, nodes to that direction

    def print_pascal_row(self, r):
        if r in self.t:
            print(' '.join([str(r) for r in self.t[r]]))
        else:
            self.create_pascal_row(self.t)
            self.print_pascal_row(self.t)

    def take_closest(self, myList, myNumber):
        """
        Assumes myList is sorted. Returns closest value to myNumber.
        If two numbers are equally close, return the smallest number.
        """
        pos = bisect_left(myList, myNumber)
        if pos == 0:
            return myList[0]
        if pos == len(myList):
            return myList[-1]
        before = myList[pos - 1]
        after = myList[pos]
        if after - myNumber < myNumber - before:
            return after
        else:
            return before

    def get_coef_at(self, r, c, recursion_limit=100, log=False):

        # simplest case: the (r, c) pair exists in the triangle
        if r in self.t and c in self.t[r]:
            # if log:
            #     print r, 'and', c, 'are found...'
            return self.t[r][c]

        # get position of closest row
        # best = sorted([[xi - r, xi] for xi in self.t], key=lambda x: abs(x[0]))[0]
        # new version (faster algorithmic complexity
        if log:
            print('taking closest...')
        best = self.take_closest(self.rows, r)
        if log:
            print('done taking closest...')
        # old version
        # best = sorted([[xi - r, xi] for xi in self.t], key=lambda x: abs(x[0]))[0]

        # print best
        best_distance = best - r
        best_row = best

        if log:
            print('r=', best_row, ', distance=', best_distance)
        option = None
        if best_distance < 0:
            # print 'option1'
            # option = 'option1'
            if abs(best_distance) >= recursion_limit:
                if log:
                    print('create pascal row at', r - recursion_limit, 'recursion_limit=%i' % recursion_limit)
                self.create_pascal_row(max(r - recursion_limit, 0))
        elif best_distance > 0:
            # option = 'option2'
            # print 'option2'
            if log:
                print('create pascal row at', r - recursion_limit, 'recursion_limit=%i' % recursion_limit)
            self.create_pascal_row(max(r - recursion_limit, 0))

        # print option, '# closest row', r, ', target column', c
        if log:
            print('solving up...', r, c)
        return self.solve_up(r, c, log=log)

    @staticmethod
    def load_from_bkp(start=0, end=100000, niter=1000, bkp_path=None):
        if bkp_path is None:
            bkp_path = '/g/scb2/zaugg/rio/data/pascal_triangles/pascal_bkp_%i_%i_%i.pkl' % (start, end, niter)
        if not exists(bkp_path):
            p = PascalTriangle()
            for r in range(start, end, niter):
                print(r)
                p.create_pascal_row(r)
            DataFrameAnalyzer.to_pickle(p.t, bkp_path)
        print('reading from input...')
        p = PascalTriangle()

        t = DataFrameAnalyzer.read_pickle(bkp_path)
        for r in t:
            if not r in p.t:
                p.t[r] = t[r]
        return p

    @staticmethod
    def load_pascal_multicore(start, end, iter=1000, ncores=20):
        from lib.ThreadingUtils import ThreadingUtils
        from multiprocessing import Manager
        manager = multiprocessing.Manager()
        return_dict = manager.dict()
        def load_from_bkp_job(start, end, niter, i):
            try:
                p = PascalTriangle.load_from_bkp(start=start, end=end, niter=iter)
                return_dict[i] = p
            except:
                print('loading from backup failed (%i, %i, %i, %i)...' % (start, end, niter, i))

        try:
            ThreadingUtils.run(load_from_bkp_job, [[i * iter, (i + 1) * iter, iter, i] for i in range(1, end / iter)], ncores)
        except:
            print('loading failed...')

        p = PascalTriangle()
        for i in list(return_dict.keys()):
            next_p = return_dict[i]
            print(i, next_p)
            for r in next_p.t:
                if not r in p.t:
                    p.t[r] = next_p.t[r]
        return p

    def solve_up(self, r, c, **kwargs):
        # ending condition
        if kwargs.get('log'):
            print(r, c)
        if r in self.t and c in self.t[r]:
            return self.t[r][c]
        if c == 0 or c == r:
            return 1

        if r == 0 and c == 0:
            # print r, c, self.t[r][c]
            return self.t[r][c]

        if not r in self.t:
            self.t[r] = {}
        if not r - 1 in self.t:
            self.t[r - 1] = {}

        if not c - 1 in self.t[r - 1]:
            if c - 1 != 0: #
                self.t[r - 1][c - 1] = self.solve_up(r - 1, c - 1, **kwargs)
            else:
                self.t[r - 1][c - 1] = 1
        if not c in self.t[r - 1]:
            if c < r:
                self.t[r - 1][c] = self.solve_up(r - 1, c, **kwargs)

        # print (r - 1, c - 1, self.t[r - 1][c - 1])
        # print r - 1, c, self.t[r - 1][c] if c <= (r - 1) else 0
        result = self.t[r - 1][c - 1] + (self.t[r - 1][c] if c < (r - 1) else 1)
        self.t[r][c] = result
        return result










