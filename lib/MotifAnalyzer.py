'''
Created on Jun 2, 2015

@author: ignacio
'''
from lib.utils import *
from os import system
import math
import pandas as pd
from itertools import product
from lib.FastaAnalyzer import FastaAnalyzer
from lib.SequenceMethods import SequenceMethods
# from BioData.PWM import PWM
from os import remove
import numpy as np
from os.path import basename
from Bio import motifs
import tempfile

class MotifAnalyzer(object):
    def scan_multifasta_with_motif_c(self, multifasta, weight_motif_path, 
                                   output_path, randomn=1000, randomlength=1000,
                                   get_results=True, cutoff=0.9, delete_output=False):        
        """
        Scan using a weight PWM and return a table with sequences and 
        scores per position
        
        If the output file exists, the method will just read SNPs from the
        generated output file
        
        The SNPs table that will be returned is the one given by an input set
        of snps. If the table is empty, then nothing is returned.
        """
        
        #=======================================================================
        # define parameters and run C code for getting weight scores
        #=======================================================================
        binary = "/home/ignacio/Dropbox/ProjectsC/PWMAnalyzer/src/pwmanalyzer"
        cmd = " ".join((binary, multifasta, 
                        weight_motif_path, "--print", "1", 
                        "--cutoff", str(cutoff), ">", output_path))
        if multifasta == "RANDOM":
            cmd = " ".join((binary, multifasta, weight_motif_path,
                            str(randomn), str(randomlength), 
                            ">", output_path))
        # print cmd
        system(cmd)
    
        #=======================================================================
        # Allocate results as a table. Remember to delete backup files 
        #=======================================================================
        if get_results:
            results = {}
            curr_k = None
            for r in open(output_path):
                r = r.strip().split("\t")
                if len(r) == 1:
                    curr_k = r[0]
                    results[curr_k] = list()
                else:
                    scores = [float(s) if s != "-" else 0.000 for s in r[1:3]]
                    results[curr_k].append([int(r[0]),] + scores)
    
            return results
        
        if delete_output and output_path is not None:
            remove(output_path)
        
    def get_complementary_nt(self, nt):
        if nt == "A":
            return "T"
        if nt == "T":
            return "A"
        if nt == "C":
            return "G"
        if nt == "G":
            return "C"
        if nt == "N":
            return "N"

    def get_pssm(self, path):
        m = motifs.parse(open(path), "jaspar")
        return m[0].pssm

    def get_ppm(self, path):
        m = motifs.parse(open(path), "jaspar")
        pfm = m.to_dict().values()[0]
        ppm = pfm.counts.normalize(pseudocounts=0.5)
        return ppm

    def get_consensus(self, path):
        m = motifs.parse(open(path), "jaspar")
        pfm = m.to_dict().values()[0]
        ppm = pfm.counts.normalize(pseudocounts=0.5)
        return ppm.consensus

    def get_motif_hits_homer(self, fa_path, motifs_path):
        # given a motif, assess its enrichment in a set of sequences
        output_path = tempfile.mkstemp()[1]
        homer_cmd = " ".join(
            ["homer2", "find", "-i", "\"" + fa_path + "\"", '-m',
             motifs_path, '-offset', '0', '-strand', 'both', "| gzip >",
             "\"" + output_path + "\""])
        print homer_cmd
        system(homer_cmd)
        res = DataFrameAnalyzer.read_tsv_gz(output_path, header=None)
        res.columns = ['ensembl', 'position', 'motif', 'motif.id', 'strand',
                       'pwm.score']
        return res

    def align_biomotifs(self, m1, m2, plot=False):
        '''
        Align two motifs that presumably have a very similar motif, it returns
        the spacing distance m2 has to be moved to align it with m1
        :param m1:
        :param m2:
        :return:
        '''
        summed_length = len(m1) + len(m2)
        best_score, best_pos = None, None
        x, y = [], []
        for dx in range(-len(m2) + 1, len(m1)):
            if dx < 0:
                values1 = [[-1] * abs(dx) + m1.counts[i] for i in range(len(m1.counts))]
            else:
                values1 = [m1.counts[i] + [-1] * abs(dx) for i in range(len(m1.counts))]

            if dx < 0:
                values2 = [m2.counts[i] + [-1] * abs(dx) for i in range(len(m2.counts))]
            else:
                values2 = [[-1] * abs(dx) + m2.counts[i] for i in range(len(m2.counts))]

            df1 = pd.DataFrame(values1)
            df2 = pd.DataFrame(values2)
            score = list((df1 - df2).abs().sum())

            score = [s for s, xi, yi in zip(score, values1[0], values2[0])
                     if xi != -1 and yi != -1]

            # take the positions that were truly aligned
            if len(score) < 5:
                continue
            score = sum(score) / len(score)
            x.append(dx)
            y.append(score)

        results = pd.DataFrame({'dx': x, "distance": y})
        selected_dx = list(results.sort_values('distance', ascending=True)['dx'])[0]
        selected_distance = list(results.sort_values('distance', ascending=True)['distance'])[0]
        if plot:
            import matplotlib.pyplot as plt
            plt.plot(x, y)
            plt.show()

        return selected_distance, selected_dx


    def get_dist_pearson(self, pssm1, pssm2):
        result = pssm1.dist_pearson(pssm2)
        return [1 - result[0], result[1]]

    def get_complementary_seq(self, s):
        return "".join([self.get_complementary_nt(nt) for nt in s][::-1])

    def find_pssm_hits_c(self, pssm, fa_path, pssm_path=None, tfname='tf', tfclass='class'):

        if pssm_path is None or not exists(pssm_path):
            pssm_path = tempfile.mkstemp()[1] if pssm_path is None else pssm_path
            writer = open(pssm_path, 'w')
            for nt in ['A', 'C', 'G', 'T']:
                entry = pssm[nt] if not isinstance(pssm, pd.DataFrame) else pssm.transpose()[nt]
                writer.write("\t".join(map(str, entry)) + "\n")
            writer.close()
        print pssm_path

        bin_path = join("/home/ignacio/Dropbox/Eclipse_Projects/zaugglab",
                        "comb-TF-binding/combinatorial_binding_analysis_invivo/src/pssm_search/pwm_search")
        if not exists(bin_path):
            bin_path = join("/g/scb/zaugg/rio/EclipseProjects/zaugglab",
                            "comb-TF-binding/combinatorial_binding_analysis_invivo/src/pssm_search/pwm_search")
        if not exists(bin_path):
            print exists(bin_path), bin_path
            assert exists(bin_path)

        output_hits = tempfile.mkstemp()[1]
        print 'preparing arguments'
        cmd = " ".join(
            [bin_path, pssm_path, fa_path, str(-10000), "-b", '-n', tfname,
             '-c', tfclass,
             ">", output_hits])

        print 'scores path (expected):', output_hits
        print cmd
        system(cmd)

        print 'scores path (done):', output_hits

        hits_c = pd.read_csv(output_hits, sep='\t', index_col=None, header=None)

        hits_c.columns = ['description', 'lab', 'tf.name', 'tf.class',
                          'strand', 'score', 'score.norm', 'start', 'end',
                          'seq']

        os.unlink(output_hits)
        return hits_c

    def get_ppm_from_sequences(self, sequences):
        # build a reference ppm
        print 'n sequences', len(sequences)
        ppm = [[(sum([s[i] == k for s in sequences]) + 0.25) / float(len(sequences) + 1)
                for k in ['A', 'C', 'G', 'T']] for i in
               range(len(sequences[0]))]
        ppm = pd.DataFrame(ppm, columns=['A', 'C', 'G', 'T'])
        return ppm

    def find_pssm_hits(self, pssm, seq_file,
                       start_at=-1, stop_at=-1, mscore=True,
                       cutoff_thr=None, use_c_routine=True):
        """ Predict hits in sequences using a PSSM. """
        from operator import itemgetter
        import math
        import Bio.SeqIO
        from Bio.Alphabet import generic_dna
        from Bio.Alphabet.IUPAC import IUPACUnambiguousDNA as unambiguousDNA

        hits = []
        records = [r for r in Bio.SeqIO.parse(seq_file, 'fasta', generic_dna)]
        if use_c_routine:
            df = self.find_pssm_hits_c(pssm, seq_file)
            for ri, r in df.iterrows():
                # normalize the score
                hits.append([ri, records[ri], r['start'], r['end'],
                             r['strand'], r['score.norm']])

        else:
            print seq_file
            print '# of records', len(records)
            print 'max score', pssm.max
            print 'min score', pssm.min
            pwm_score_range = pssm.max - pssm.min
            for i, record in enumerate(records):
                if start_at != -1 and i < start_at:
                    continue
                record.seq.alphabet = unambiguousDNA()
                results = pssm.search(record.seq.upper(), pssm.min)
                results = [[pos, score] for pos, score in results if
                           not math.isnan(score)]
                scores = [[pos, score] for pos, score in results]
                if scores:
                    if mscore:
                        pos_maxi, maxi = max(scores, key=itemgetter(1))
                        strand = "+"
                        if pos_maxi < 0:
                            strand = "-"
                            pos_maxi = pos_maxi + len(record.seq)

                        # normalize the score
                        next_score = (maxi - pssm.min) / pwm_score_range
                        hits.append([i, record, pos_maxi + 1,
                                     pos_maxi + pssm.length, strand, next_score])
                    else:
                        for posi, score in scores:
                            strand = "+"
                            if posi < 0:
                                strand = "-"
                                posi = posi + len(record.seq)

                            # normalize the score
                            next_score = (score - pssm.min) / pwm_score_range
                            if cutoff_thr is None or score > cutoff_thr:
                                hits.append(
                                    [i, record, posi + 1, posi + pssm.length,
                                     strand, next_score])
                else:
                    print 'entry at position', i, 'will be skipped (no hits)...'
                if i % 100 == 0:
                    print "# sequences being scored:", i
                if stop_at != -1 and i + 1 >= stop_at:
                    break

            # for h in hits[:10]:
            #     print h
            # exit()

        return hits

    def find_pssm_hits_overlapping_snp(self, pssm, df,
                                       tmp_fa_path=None):
        '''
        A modification of the classic method, in which we want to find the
        best SNP overlapping our motif
        :param pssm:
        :param df:
        :param tmp_fa_path:
        :return:
        '''
        fasta_analyzer = FastaAnalyzer()
        motif_length = len(pssm[0])

        # get the relative range in the reference genome, according to the longest motif
        df['range.start'] = df['snp.pos'] - motif_length
        df['range.end'] = df['snp.pos'] + motif_length - 1

        if not exists(tmp_fa_path):
            bed_path = tempfile.mkstemp()[1]
            fasta_analyzer.create_bed_file(df[['chr', 'range.start', 'range.end']],
                                           bed_path)
            fasta_analyzer.convert_bed_to_fasta_hg19(bed_path, tmp_fa_path)
            print tmp_fa_path

        df['id'] = df['chr'] + ":" + df['range.start'].astype(str) + "-" + df['range.end'].astype(str)
        hits = self.find_pssm_hits(pssm, tmp_fa_path)

        table = []
        for hit in hits:
            # print hit
            i, start, end, strand, score = [hit[0]] + hit[2:6]
            # print start
            start -= 1
            hit_seq = str(hit[1].seq[start: end]).upper()
            # print start, end, strand, hit_seq
            # print hit_seq
            assert len(hit_seq) == len(pssm[0])
            if strand == "-":
                hit_seq = SequenceMethods.get_complementary_seq(hit_seq)
            table.append([str(i), hit[1].id,
                          strand, start, end, hit_seq,
                          score])
        hits_df = pd.DataFrame(table, columns=['i', 'id', 'strand', 'start', 'end',
                                               'sequence', "score"])

        # get the relative SNP position according to the SNP position
        hits_df['snp.rel.pos'] = [motif_length - r['start'] - 1 if r['strand'] == "+"
                                  else r['end'] - motif_length for ri, r in hits_df.iterrows()]

        hits_df['chr'] = [k.split(":")[0] for k in hits_df['id']]
        hits_df['chrom.start'] = [int(r['id'].split(":")[1].split("-")[0]) + r['start']
                                  for ri, r in hits_df.iterrows()]
        hits_df['chrom.end'] = [int(r['id'].split(":")[1].split("-")[0]) +
                                r['start'] + motif_length
                                for ri, r in hits_df.iterrows()]

        return hits_df

    def get_seqs_flanking_snps(self, df, n_flanks, tmp_fa=None, overwrite=True,
                               uppercase=True, genome_path=None):
        assert 'chr' in df.columns and 'snp.pos' in df.columns

        fasta_analyzer = FastaAnalyzer()
        # get the relative range in the reference genome, according to the longest motif
        df['range.start'] = df['snp.pos'] - n_flanks
        df['range.end'] = df['snp.pos'] + n_flanks - 1

        if overwrite or not exists(tmp_fa):
            bed_path = tempfile.mkstemp()[1]
            fasta_analyzer.create_bed_file(
                df[['chr', 'range.start', 'range.end']],
                bed_path)
            fasta_analyzer.convert_bed_to_fasta_hg19(bed_path, tmp_fa,
                                                     genome_path=genome_path)

        print 'reading fastas from file'
        seqs = fasta_analyzer.get_fastas_from_file(tmp_fa, as_dict=True,
                                                   uppercase=uppercase)

        return seqs

    def get_fastas_from_file(self, fasta_path, as_dict=False):
        fastas = []
        seq = None
        header = None
        for r in open(fasta_path):
            r = r.strip()
            if r.startswith(">"):
                if seq != None and header != None:
                    fastas.append([header, seq])
                seq = ""
                header = r[1:]
            else:
                if seq != None:
                    seq += r
                else:
                    seq = r
        
        # append last fasta read by method
        fastas.append([header, seq])
        
        if as_dict:
            return {h: s for h, s in fastas}

        return fastas
    def create_cm_motif(self, ppm_path1_or_ppm1, ppm_path2_or_ppm2, category, distance):
        """
        Merge individual PPM matrices into composite PPM.
        Add 0.25 as probabilities for the intermediate regions.
        """
        # generate PPM1 and PPM2 from PPM files
        if isinstance(ppm_path1_or_ppm1, str):
            ppm1 = [map(float, r.split("\t")[1:]) for r in open(ppm_path1_or_ppm1).readlines()[1:]]
            ppm2 = [map(float, r.split("\t")[1:]) for r in open(ppm_path1_or_ppm1).readlines()[1:]]
        # if given the ppm as Biopython matrices, then parse them accordingly
        else:
            ppm1 = ppm_path1_or_ppm1
            ppm2 = ppm_path2_or_ppm2
            len1, len2 = len(ppm1[0]), len(ppm2[0])
            ppm1 = [[ppm1[j][i] for j in range(4)] for i in range(len1)]
            ppm2 = [[ppm2[j][i] for j in range(4)] for i in range(len2)]

        # by default, both are in forward direction. assign category according to
        # 
        #        (a) 1-> 2->
        #        (b) 2-> 1->
        #        (c) <-1 2->
        #        (d) 2-> <-1
        a, b = None, None
        if category == "a":
            a, b = ppm1, ppm2
        if category == "b":
            a, b = ppm2, ppm1
        if category == "c":
            a, b = self.get_complementary_ppm(ppm1), ppm2
        if category == "d":
            a, b = ppm2, self.get_complementary_ppm(ppm1)
        
        # spacing: add [0.25, 0.25, 0.25, 0.25] lists as given by the spacing dist
        if distance >= 0:
            spacer = [[0.25, 0.25, 0.25, 0.25] for i in range(distance)]
            composite_motif = a + spacer + b
        else:
            if abs(distance) >= len(a):
                # print distance, 'overlaps motif a completely. Omit'
                return None
            if abs(distance) >= len(b):
                # print distance, 'overlaps motif b completely. Omit'
                return None

            flanka = a[distance:]
            flankb = b[:-distance]
            averaged_flank = [[(fai + fbi) / 2 for fai, fbi in zip(fa, fb)] for fa, fb in zip(flanka, flankb)]
            composite_motif = a[:distance] + averaged_flank + b[-distance:]

        composite_motif = pd.DataFrame(composite_motif, columns=['A', 'C', 'G', 'T']).transpose()
        return composite_motif

    def get_complementary_ppm(self, ppm):
        """
        Get the PPM for the complementary sequence
        """
        # DEFAULT: ACGT
        ppm = ppm[::-1]
        for i, col in enumerate(ppm):
            ppm[i] = col[::-1]
        return ppm

    def scan_multifasta_with_motif_python(self, multifasta, pwm):
        """
        Scan sequences with a PWM using Python
        """
        fasta_id = None
        total = 0
        counter = 0
        counter_sequences = 0
        for line in open(multifasta):
            if line.startswith(">"):
                 counter_sequences += 1
            print counter_sequences
        import sys; sys.exit()
        
        probe_length = pwm.get_motif_length()
        for line in open(multifasta):
            counter_sequences += 1
            if counter_sequences % 10000 == 0:
                print counter_sequences
            line = line.strip()
            if line.startswith(">"):
                fasta_id = line
            else:
                for i in range(0, len(line) - probe_length + 1):
                    s = line[i: i + probe_length]
                    if pwm.has_saved_score(s):
                        counter += 1
                    weighted_score = pwm.get_weight_score_from_sequence(s)
                    total += 1
                    # print s, weighted_score
                # print counter, total
        import sys; sys.exit()
        
    def scan_fasta_with_motif_python(self, s, pwm, cutoff=None, start_at=-1,
                                     end_at=-1, use_consensus=False, max_mismatches=None,
                                     consensus_query=None):
        """
        Scan single sequence with pwm
        """
        results = pwm.scan_sequence(s, cutoff=cutoff,
                                    start_at=start_at, end_at=end_at,
                                    use_consensus=use_consensus,
                                    max_mismatches=max_mismatches,
                                    consensus_query=consensus_query)
        return results

    def scan_multifasta_with_selected_seqs(self, multifasta, selected_seqs):
        """
        It goes through a multifasta file, checking for each sequence if there
        are hits in the selected sequences set. Return a dictionary with all
        output data
        """
        pass
        
    def get_seqs_over_cutoff(self, motif, cutoff=0.0):
        """
        It permutes all sequences, looking for dinucleotides whose score is
        over a pre-established cutoff between 0 and 1
        """
        selected = {}
        print "loading motif scores..."
        motif_length = motif.get_motif_length()
        for s in product("ACGT", repeat=motif_length):
            s = "".join(s)
            weighted_score = motif.get_weight_score_from_sequence(s)
            if weighted_score >= cutoff:
                selected[s] = weighted_score
        return selected
    
    def get_motif_length(self, motif_path):
        """
        It returns the length of the analyzed motifs, as an integer
        The number 10 permits filtering empty lines 
        """
        return len([r for r in open(motif_path) if len(r) > 10])