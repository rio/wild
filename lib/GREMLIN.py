'''
Created on 7/7/2019

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.FastaAnalyzer import FastaAnalyzer

class GREMLIN:

    @staticmethod
    def run(aln_path_or_sequences, save_output_to, remove_input=True, **kwargs):
        if isinstance(aln_path_or_sequences, list):
            aln_path = tempfile.mkstemp()[1]
            FastaAnalyzer.write_fasta_from_sequences(aln_path_or_sequences, aln_path, add_headers=False)
        else:
            aln_path = aln_path_or_sequences
        bin_path = '/g/scb2/zaugg/zaugg_shared/Programs/GREMLIN_CPP/gremlin_cpp'
        cmd = "%s -i %s -o %s" % (bin_path, aln_path, save_output_to)
        print(cmd)
        system(cmd)

        if isinstance(aln_path_or_sequences, list) and remove_input:
            remove(aln_path)
