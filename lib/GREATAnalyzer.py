'''
Created on

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.plot_utils import *
from lib.DISEASES import DISEASES
from lib.HPOAnalyzer import HPOAnalyzer
from lib.PeaksAnalyzer import PeaksAnalyzer
from lib.SequenceMethods import SequenceMethods
from lib.REMAPAnalyzer import REMAPAnalyzer
from lib.ThreadingUtils import ThreadingUtils
from lib.GOAnalyzer import GOAnalyzer
from lib.PascalTriangle import PascalTriangle
from lib.RFacade import RFacade
from math import lgamma, fabs
from functools import reduce
from operator import or_
import pybedtools
# from lib.GOCombinatorialBinding import GOCombinatorialBinding

import numba
from numba import jit, njit
from lib.MyGeneAnalyzer import MyGeneAnalyzer

class GREATAnalyzer:
    def __init__(self, ontologies=None, load_pascal_triangle=False):

        if load_pascal_triangle:
            print('loading PASCAL triangle')
            self.pascal = PascalTriangle.load_pascal_multicore(0, 20000, 1000, 10)
        if ontologies is not None:
            self.ontologies = ontologies

    def intersect_peaks_vs_terms(peaks):
        from lib.REMAPAnalyzer import REMAPAnalyzer
        peaks = REMAPAnalyzer.get_remap_peaks('foxo1')

        df = DISEASES.get_data_human_experiments()
        reg_doms = DataFrameAnalyzer.read_tsv('/g/scb2/zaugg/rio/data/greatTools/data/regDoms.out', header=None)
        a, b, n, path = PeaksAnalyzer.intersect_coordinates(peaks[['chr','start', 'end', 'k.summit']],
                                                            reg_doms, additional_args=['-wo'])

        inter = DataFrameAnalyzer.read_tsv(path, columns=['chr', 'start', 'end', 'k'] + list(reg_doms.columns) + ['n'])

        peaks_by_disease = []
        for disease_id, grp in df.groupby('disease.id'):
            print(disease_id, grp.shape[0])
            n_genes = len(set(grp['gene.name']))
            n_peaks = len(set(inter[inter[3].isin(grp['gene.name'])]['k']))
            peaks_by_disease.append([disease_id, a, b, n_genes, n_peaks])
        peaks_by_disease = pd.DataFrame(peaks_by_disease, columns=['id', 'n.peaks', 'n.genes', 'n.genes.id', 'n.peaks.id'])

    def get_peaks(tf_name):
        return REMAPAnalyzer.get_remap_peaks(tf_name)

    def run_great_server(self, peaks, genome='hg19', category=None):
        return GOAnalyzer.run_GREAT(peaks[peaks.columns[:4]], genome=genome, category=category)

    def get_genes_by_goid(self, non_redundant=False, confidence=None):
        dis = DISEASES.get_data_human_experiments()
        text = DISEASES.get_data_text_mining()

        hpo = HPOAnalyzer.get_hpo_phen_to_gene_table()
        hpo.columns = ['disease.id', 'disease.name', 'gene.id', 'gene.name']

        df = pd.concat([dis[['gene.name', 'disease.name', 'disease.id']],
                        text[['disease.id', 'disease.name', 'gene.name']],
                        hpo[['disease.id', 'disease.name', 'gene.name']]]).reset_index(drop=True)
        if confidence is not None:
            df = df[df['confidence'] >= confidence]
        genes_by_goid = {k: set(grp['gene.name']) for k, grp in df.groupby('disease.id')}

        if non_redundant:
            non_red_ids = self.get_non_redudant_disease_ids()
            return {k: genes_by_goid[k] for k in genes_by_goid if k in non_red_ids}
        return genes_by_goid

    def get_go_terms(self):
        if not hasattr(self, 'ontologies'):
            from lib.OntologiesAnalyzer import OntologiesAnalyzer
            self.ontologies = OntologiesAnalyzer.get_gos()
        return self.ontologies

    def get_non_redudant_disease_ids(self, overlap_thr=.9, n_genes_min=10):
        dis = DISEASES.get_data_human_experiments()
        text = DISEASES.get_data_text_mining()

        hpo = HPOAnalyzer.get_hpo_phen_to_gene_table()
        hpo.columns = ['disease.id', 'disease.name', 'gene.id', 'gene.name']

        df = pd.concat([dis[['gene.name', 'disease.name', 'disease.id']],
                        text[['disease.id', 'disease.name', 'gene.name']],
                        hpo[['disease.id', 'disease.name', 'gene.name']]]).reset_index(drop=True)
        genes_by_disease = {k: set(grp['gene.name']) for k, grp in df.groupby('disease.id')}

        output_path = '/g/scb2/zaugg/rio/data/DISEASES/non_redundant_ids_%f_%i.pkl' % (overlap_thr, n_genes_min)
        if not exists(output_path):
            ids_to_remove = set()
            for i, k1 in enumerate(genes_by_disease):
                if i % 100 == 0:
                    print(i, 'out of', len(genes_by_disease))
                res = []
                for k2 in genes_by_disease:
                    if k1 == k2:
                        continue
                    a = genes_by_disease[k1]
                    b = genes_by_disease[k2]
                    na, nb = len(a), len(b)
                    nc = len(a.intersection(b))
                    res.append([k1, k2, na, nb, nc])
                res = pd.DataFrame(res, columns=['k1', 'k2', 'na', 'nb', 'nc'])
                res['overlap'] = res['nc'] / res[['na', 'nb']].min(axis=1)
                sel = res[(res['overlap'] > overlap_thr) & (res['na'] > n_genes_min) & (res['nb'] > n_genes_min)]
                sel_ids = {r['k1'] if r['na'] > r['nb'] else r['k2'] for ri, r in sel.iterrows()}
                for s in sel_ids:
                    ids_to_remove.add(s)
            DataFrameAnalyzer.to_pickle(ids_to_remove, output_path)
        ids_to_remove = DataFrameAnalyzer.read_pickle(output_path)
        return {k for k in genes_by_disease if not k in ids_to_remove}

    def n_ksubsets_more_than_one_pascal(self, n, m, k):  # n = npositive, m = nnegatives, k = ksubset size
        total = 0
        for i in range(1, m + 1):
            next = self.pascal.get_coef_at(n + m - i, k - 1)
            total += next
        return total

    def betafc(self, a, b, x):
        MAXIT = 10000
        FPMIN = 1.0e-30
        EPS = 3.0e-7
        m, m2 = 0, 0
        aa, c, d, delx, h, qab, qam, qap = [0.0] * 8
        qab = a + b
        qap = a + 1.0;
        qam = a - 1.0;
        c = 1.0; # First step of Lentz s method.
        d = 1.0 - qab * x / qap;
        if (fabs(d) < FPMIN):
            d = FPMIN
        d = 1.0 / d
        h = d

        for m in range(1, MAXIT + 1):
            m2 = 2 * m
            aa=m * (b-m) * x / ((qam+m2) * (a+m2))
            d = 1.0 + aa * d
            if (fabs(d) < FPMIN):
                d = FPMIN
            c = 1.0 + aa / c
            if (fabs(c) < FPMIN):
                c = FPMIN
            d = 1.0 / d;
            h *= d * c;
            aa = -(a + m) * (qab + m) * x / ((a + m2) * (qap + m2));
            d = 1.0 + aa * d;
            if (fabs(d) < FPMIN):
                d = FPMIN;
            c = 1.0 + aa / c;
            if (fabs(c) < FPMIN):
                c = FPMIN;
            d = 1.0 / d;
            delx=d * c;
            h *= delx;
            if (fabs(delx -1.0) < EPS):
                break;
        if (m > MAXIT):
            raise ValueError("a or b too big, or MAXIT too small in betacf");
        return h;

    def betai(self, a, b, x):
        bt = None
        if x < 0.0 or x > 1.0:
            raise ValueError('Bad x in routine interval')
        if x == 0 or x == 1.0:
            bt = 0.0
        else:
            bt = np.exp(lgamma(a + b) - lgamma(a) - lgamma(b) + a * np.log(x) + b * np.log(1.0 - x))
        if x < (a + 1.0) /(a + b + 2.0):
            return bt * self.betafc(a, b, x) / a
        else:
            return 1.0 - bt * self.betafc(b, a, 1.0 - x) / b

    def get_binom_pval(self, n, k, p):
        if(k == 0):
            return 1
        return self.betai(k, n - k + 1, p)

    def get_non_overlapping_nt(self, sorted_peaks, fast=True):
        if fast:
            annotation_nt = np.sum(sorted_peaks[2] - sorted_peaks[1])
            return annotation_nt
        else:
            last_chr, last_start, last_end = None, None, None
            annotation_nt = 0
            for ri, r in sorted_peaks.iterrows():
                chrom, start, end = r[0], r[1], r[2]
                # print ri, list(r), dx, annotation_nt
                dx = (end - start)
                if last_chr is None or last_chr != r[0]:
                    annotation_nt += dx
                else:
                    if start <= last_end:
                        # print last_end, start
                        dx = (end - last_end)
                        # print 'non-overlapped range', dx
                    annotation_nt += dx
                last_chr, last_start, last_end = chrom, start, end
                # print 'total nt', annotation_nt
            return annotation_nt

    def map_tfs_to_ontologies(self):
        # get all positive cases (has a TFs in the list)
        from lib.HumanTFs import HumanTFs
        tfs = HumanTFs.get_tf_motifs_cisbp()
        df, n_by_goid = self.get_go_terms()
        tfs = tfs[tfs['Best Motif(s)? (Figure 2A)'] == True]
        df['is.tf'] = df['gene.name'].isin(tfs['HGNC symbol'])
        return df

    def map_ontologies_by_tf_motif_code(self, motif_code):
        '''
        Get all ontologies that contain a member that is mappable to a given motif
        :param motif_code:
        :return:
        '''
        # get all positive cases (has a TFs in the list)
        from lib.HumanTFs import HumanTFs
        tfs = HumanTFs.get_tf_motifs_cisbp()
        df, n_by_goid = self.get_go_terms()
        tfs = tfs[tfs['Best Motif(s)? (Figure 2A)'] == True]
        df = df[df['gene.name'].isin(set(tfs[tfs['CIS-BP ID'] == motif_code]['HGNC symbol']))]
        return df

    def calculate_zscores_local(self, peaks,
                                stopat=None,
                                use_non_redundant_diseases=False,
                                query_ids=None,
                                query_names=None,
                                genome='hg19',
                                reg_doms_path=None,
                                maxtimemin=None,
                                priority_ids=None,
                                use_croutine=False,
                                min_ngenes=None,
                                max_ngenes=None,
                                zscore_column=None,
                                n_perm=1,

                                **kwargs):
        '''
        :param peaks:
        :param stopat:
        :param use_non_redundant_diseases:
        :param query_ids:
        :param query_names:
        :param min_ngenes:
        :param max_ngenes:
        :param zscore_column: a column in the dataframe, FALSE/TRUE, used to indicate how to calculate the Z-scores
        :param zscore_nperm:
        :param kwargs:
        :return:
        '''

        print('\n### Starting Z-scores calculation...')

        # if the input dataframe is emtpy (no peaks), return peaks and finish
        # print peaks.shape
        if peaks.shape[0] == 0:
            print('### return emtpy dataframe...')
            res = pd.DataFrame(columns=['id', 'name', 'n.genes.term', 'n.genes.hits', 'n.peaks.total',
                                        'n.peaks.hits', 'binom.pval',
                                        'a', 'b', 'c', 'd', 'odds.ratio', 'pval'])
            return res

        # if a z.score is calculated then a column was given as input. In that case, subset the peaks into fg and bg
        if zscore_column is not None:
            bg = peaks # [~peaks[zscore_column]]
            peaks = peaks[peaks[zscore_column]]
            print('### positives (by %s criteria)' % zscore_column, peaks.shape[0])
            print('### total (positives and negatives, for downsampling)', bg.shape[0])

        df, n_by_goid = self.get_go_terms()
        # load gene ontotologies

        if use_non_redundant_diseases:
            non_red_diseases = self.get_non_redudant_disease_ids()
            df = df[df['disease.id'].isin(non_red_diseases)].reset_index(drop=True)

        name_by_doid = df.set_index('id')['name'].to_dict()

        print('### reading and parsing regdoms (GREAT data structure that will be overlapped with the peaks)...')
        if not hasattr(self, 'reg_doms'):
            if reg_doms_path is None:
                print('Please provide a reg_doms file (see data/regDoms_by_genome)')
                assert reg_doms_path is not None
            reg_doms = DataFrameAnalyzer.read_tsv(reg_doms_path, header=None)
            reg_doms = SequenceMethods.parse_range2coordinate(reg_doms, [0, 1, 2], 'regdom')
            # sort and count the amount of nucleotides in all considered reg_domains
            reg_doms = reg_doms.sort_values([reg_doms.columns[0], reg_doms.columns[1]], ascending=[True, True])
            total_nt = self.get_non_overlapping_nt(reg_doms, fast=False)
            self.reg_doms = reg_doms
            self.total_nt = total_nt

        a_and_b = None
        if hasattr(self, 'peaks_last_query') and hasattr(self, 'a_and_b_last_query') and kwargs.get('use_tmp'):
            if self.peaks_last_query.equals(peaks) and self.peaks_last_query.shape[0] == peaks.shape[0]:
                a_and_b = self.a_and_b_last_query

        if a_and_b is None:
            print('### prepare regdoms input...')
            from pybedtools import BedTool
            bed_a = BedTool.from_dataframe(self.reg_doms[self.reg_doms.columns[:3]])
            bed_b = BedTool.from_dataframe(peaks[peaks.columns[:3]])
            a_and_b = bed_a.intersect(bed_b, wo=True).to_dataframe()
            pybedtools.cleanup() # remove_all=True)
            self.a_and_b_last_query = a_and_b
            self.peaks_last_query = peaks
        a_and_b = SequenceMethods.parse_range2coordinate(a_and_b, ['name', 'score', 'strand'], 'remap')
        a_and_b = SequenceMethods.parse_range2coordinate(a_and_b, ['chrom', 'start', 'end'], 'regdom')
        n_genes_total = len(set(self.reg_doms[3]))
        n_peaks_total = peaks.shape[0]
        fg_peaks_n_regdom = a_and_b

        if zscore_column is not None:
            print('### prepare regdoms bg...')
            bed_a = BedTool.from_dataframe(self.reg_doms[self.reg_doms.columns[:3]])
            bed_b = BedTool.from_dataframe(bg[bg.columns[:3]])
            a_and_b_bg = bed_a.intersect(bed_b, wo=True).to_dataframe()
            pybedtools.cleanup() # remove_all=True)
            a_and_b_bg = SequenceMethods.parse_range2coordinate(a_and_b_bg, ['name', 'score', 'strand'], 'remap')
            a_and_b_bg = SequenceMethods.parse_range2coordinate(a_and_b_bg, ['chrom', 'start', 'end'], 'regdom')
            n_genes_total = len(set(self.reg_doms[3]))
            bg_peaks_n_regdom = a_and_b_bg
            print('### intersections between positives and regdoms', fg_peaks_n_regdom.shape[0])
            print('### intersections between pos+neg and regdoms', bg_peaks_n_regdom.shape[0])


        selected_goid = list(n_by_goid.keys())
        if query_names is None and min_ngenes != None:
            selected_goid = {k for k in selected_goid if n_by_goid[k] >= min_ngenes}
        if query_names is None and max_ngenes != None:
            selected_goid = {k for k in selected_goid if n_by_goid[k] <= max_ngenes}

        res = []


        # regdoms_in_path = tempfile.mkstemp()[1]
        i = -1
        df = df[df['id'].isin(selected_goid)].reset_index(drop=True)

        # print df.head()
        # print df.shape
        # print df[df['id'] == 'ICD10:C'].head()
        if not hasattr(self, 'go_groups'):
            self.go_groups = {goid: grp for goid, grp in df.groupby('id')}

        reg_doms = self.reg_doms

        if query_ids is not None:
            selected_goid = {goid for goid in self.go_groups if goid in query_ids}

        if priority_ids is not None and len(priority_ids) > 0:
            priority_df = pd.DataFrame([[k, 1] for k in priority_ids], columns=['goid', 'priority'])
            all_df = pd.DataFrame([[k, 0] for k in query_ids], columns=['goid', 'priority'])
            all_df = pd.concat([priority_df, all_df]).drop_duplicates('goid')
            # print all_df.head()
            # print all_df.tail()

            # if the priority IDs are empty zero, then just limit all_df to 50
            print(len(priority_ids))
            if len(priority_ids) == 0:
                all_df = all_df.sample(min(50, all_df.shape[0]))
            else:
                all_df = pd.concat([all_df.head(len(priority_ids)), all_df.tail(len(priority_ids))]).drop_duplicates('goid')

            selected_goid = set(list(all_df['goid']))

        n_go_terms = len(set(selected_goid))
        import subprocess
        print('### iterating through ontology IDs. # of terms = %i' % n_go_terms)

        # in case of calculating Z-scores, keep a dictionary of permutations
        from multiprocessing import Manager
        manager = multiprocessing.Manager()
        sample_by_perm = manager.dict()


        # preparing genes by peak: for permutation purposes
        print('### grouping genes by peak...')

        all = bg_peaks_n_regdom[['remap', 'regdom']].merge(reg_doms, on='regdom')
        all['is.selected'] = all['remap'].map(DataFrameAnalyzer.get_dict(bg, 'k.summit', zscore_column))
        all_remap_ids = set(all['remap'])

        print('### all remap ids. N=%i' % (len(all_remap_ids)))

        genes_by_peak = None
        if kwargs.get('peak_gene_associations_bkp_code', None) is not None:
            print('### preparing peak->gene associations...')
            print('### code (for bkp):', kwargs.get('peak_gene_associations_bkp_code', None))
            print('### generating peak->gene associations for the 1st time')
            peak_gene_assoc_dir = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/combinatorial_binding_analysis_invivo/data/GREAT_peak_gene_associations"
            filename = kwargs.get('peak_gene_associations_bkp_code', None)
            dir_code = "_".join(filename.split("_")[:2])
            bkp_dir = join(peak_gene_assoc_dir, dir_code)
            if not exists(bkp_dir):
                mkdir(bkp_dir)
            bkp_path = join(bkp_dir, filename + ".pkl")
            if not exists(bkp_path):
                print('### generating peak-gene associations (once only) and save it for future reference...')
                t_pg_assoc = get_time_now()
                print(all.shape[0])
                # genes_by_peak = {peak_id: set(grp[3]) for peak_id, grp in all.groupby('remap')}
                genes_by_peak = {peak_id: set() for peak_id in set(all['remap'].unique())}
                for ri, r in all.iterrows():
                    genes_by_peak[r['remap']].add(r[3])
                print('### total time in preparing peak->associations (seconds)', get_timespan_seconds(t_pg_assoc))
                DataFrameAnalyzer.to_pickle(genes_by_peak, bkp_path)

            t_pg_assoc = get_time_now()
            genes_by_peak = DataFrameAnalyzer.read_pickle(bkp_path)
            print('### total time in loading peak->associations (seconds)', get_timespan_seconds(t_pg_assoc))
        else:
            t_pg_assoc = get_time_now()
            # genes_by_peak = {peak_id: set(grp[3]) for peak_id, grp in all.groupby('remap')}
            genes_by_peak = {peak_id: set() for peak_id in set(all['remap'].unique())}
            for ri, r in all.iterrows():
                genes_by_peak[r['remap']].add(r[3])
            print('### total time in preparing peak->associations (seconds)', get_timespan_seconds(t_pg_assoc))
        assert genes_by_peak is not None

        self.genes_by_peak = genes_by_peak

        if maxtimemin is not None:
            print('### max execution time (in minutes):', maxtimemin)
            print('### method will finish after this and return the current output...')

        print('### Permutations per GO: %i' % n_perm)
        print('\n\n')
        t0 = get_time_now()

        all_df = None
        if use_croutine:
            all_df = pd.DataFrame()


        def get_peaks_n_genes_vector(all_remap_ids):
            genes_by_peak = self.genes_by_peak.copy()
            print('all remap ids. N=%i' % (len(all_remap_ids)))
            gene_name_by_id = df.sort_values('gene.name').drop_duplicates('gene.name')['gene.name'].reset_index(
                drop=True).to_dict()
            id_by_gene_name = {gene_name_by_id[k]: k for k in gene_name_by_id}

            all_remap_ids_d = all.drop_duplicates('remap')['remap'].reset_index(drop=True).to_dict()
            id_by_remap_id = {all_remap_ids_d[k]: k for k in all_remap_ids_d}
            all_remap_ids = {id_by_remap_id[k] for k in all_remap_ids}

            all_remap_ids = np.array(list(all_remap_ids))
            for k in genes_by_peak:
                genes_by_peak[k] = {id_by_gene_name[g] for g in genes_by_peak[k] if g in id_by_gene_name}
            genes_by_peak_id = {id_by_remap_id[k]: genes_by_peak[k] for k in genes_by_peak if k in id_by_remap_id}
            genes_by_peak = genes_by_peak_id

            peaks_by_gene_id = {}
            for p in genes_by_peak_id:
                for g in genes_by_peak_id[p]:
                    if not g in peaks_by_gene_id:
                        peaks_by_gene_id[g] = set()
                    peaks_by_gene_id[g].add(p)

            peaks_input = []
            genes_input = []
            for p in sorted(genes_by_peak):
                for g in genes_by_peak[p]:
                    peaks_input.append(p)
                    genes_input.append(g)
            peaks_input = np.array(peaks_input)
            genes_input = np.array(genes_input)

            return peaks_input, genes_input, id_by_gene_name, all_remap_ids, peaks_by_gene_id

        peaks_vector, genes_vector, id_by_gene_name, peaks_ids, peaks_by_gene_id = get_peaks_n_genes_vector(all_remap_ids)

        reg_doms['gene.id'] = reg_doms[3].map(id_by_gene_name)
        id_by_regdom = reg_doms['regdom'].drop_duplicates().reset_index().set_index('regdom')['index'].to_dict()
        reg_doms['regdom.id'] = reg_doms['regdom'].map(id_by_regdom)
        fg_peaks_n_regdom['regdom.id'] = fg_peaks_n_regdom['regdom'].map(id_by_regdom)

        # print(type(peaks_ids), list(peaks_ids)[:5], len(peaks_ids))
        # print('run c routine = %i...' % use_croutine)

        for i, goid in enumerate(selected_goid):
            if query_ids is not None and not goid in query_ids:
                continue
            if not goid in self.go_groups:
                # print 'ID', goid, 'not found in self.go_groups. Skip...'
                res.append([goid] + [None for i in range(18)])
                continue
            grp = self.go_groups[goid]
            go_name = name_by_doid[goid]
            if query_names is not None:
                accept = False
                for s in query_names:
                    if s.lower() in go_name.lower():
                        accept = True
                if not accept:
                    continue

            if maxtimemin is not None and (get_timespan_seconds(t0) / 60.0) > maxtimemin:
                print(get_timespan_minutes(t0), maxtimemin)
                print('stop for loop due to maxtime user stop criteria...')
                break

            if len(res) % (kwargs.get('print_log_n', 250) * 2) == 0:
                perc_completed = '%.1f%%' % (i / len(selected_goid) * 100)
                print('Done %i out of %i ontologies (%s). Current time [min/sec]: %s, %.1f' %
                      (i, len(selected_goid), perc_completed, get_timespan_minutes(t0), get_timespan_seconds(t0)))


            # get regulatory domains associated to ontology of interest, checking the associated genes
            is_reg_dom_gene = reg_doms[3].isin(set(grp['gene.name']))

            sel = reg_doms[is_reg_dom_gene]

            # count nt in non-overlapping regions
            annotation_nt = self.get_non_overlapping_nt(sel, fast=True)

            sel_neg = reg_doms[~is_reg_dom_gene]

            # print goid, n_genes_term

            n_genes_term = len(set(sel[3]))
            n_genes_no_term = len(set(sel_neg[3]))

            # import ipdb; ipdb.set_trace()

            next_inters = fg_peaks_n_regdom[fg_peaks_n_regdom['regdom.id'].isin(set(sel['regdom.id']))]
            n_peaks_hits = len(set(next_inters['remap']))
            n_genes_hits = len(set(sel[sel['regdom.id'].isin(fg_peaks_n_regdom['regdom.id'])][3]))

            next_inters_neg = fg_peaks_n_regdom[fg_peaks_n_regdom['regdom.id'].isin(set(sel_neg['regdom.id']))]
            n_hits_neg = len(set(next_inters_neg['remap']))
            n_genes_neg_hits = len(set(sel_neg[sel_neg['regdom.id'].isin(fg_peaks_n_regdom['regdom.id'])][3]))

            annotation_weight = float(annotation_nt) / self.total_nt
            assert self.total_nt > annotation_weight
            binomP = self.get_binom_pval(n_peaks_total, n_peaks_hits, annotation_weight)

            a, b, c, d = n_genes_hits, n_genes_term - n_genes_hits, n_genes_neg_hits, n_genes_no_term - n_genes_neg_hits
            odds_ratio, pval = fisher_exact([[a, b], [c, d]], alternative='greater')

            t = [goid, go_name, n_genes_term, n_genes_hits, n_peaks_total, n_peaks_hits, annotation_weight, binomP,
                 a, b, c, d, odds_ratio, pval]

            # calculate Z-score

            # print i, goid, 'calculating significance using %i permutations' % n_perm
            genes_term = set(sel[3])

            genes_term_ids = np.array(sel['gene.id'])
            peaks_term_ids = np.array(list({gi for g in genes_term_ids if g in peaks_by_gene_id
                                            for gi in peaks_by_gene_id[g]}))

            genes_term_ids = np.array(genes_term_ids[~np.isnan(genes_term_ids)]).astype(int) #  dtype=int64)
            peaks_term_ids = np.array(peaks_term_ids[~np.isnan(peaks_term_ids)]).astype(int) # , dtype=int64)



            tperm0 = get_time_now()

            # print all.head()
            if kwargs.get('log'):
                print('calculating Z-scores')
                print(n_genes_hits, len(all_remap_ids), n_peaks_total, len(genes_term), len(genes_by_peak))

            # print('permutation genes')
            try:
                mu_genes, sigma_genes, z_genes, z_genes_zt = self.calculate_zscore_downsampling_genes(n_genes_hits, all_remap_ids,
                                                                                                      n_peaks_total, genes_term,
                                                                                                      genes_by_peak, n_perm,
                                                                                                      peaks_vector=peaks_vector,
                                                                                                      genes_vector=genes_vector,
                                                                                                      go_term_ids=genes_term_ids,
                                                                                                      peaks_ids=peaks_ids,
                                                                                                      **kwargs)


                # print('permutation peaks')
                mu_peaks, sigma_peaks, z_peaks, z_peaks_zt = self.calculate_zscore_downsampling_peaks(n_peaks_hits, all_remap_ids,
                                                                                                      n_peaks_total, genes_term,
                                                                                                      genes_by_peak, n_perm,
                                                                                                      peaks_vector=peaks_vector,
                                                                                                      genes_vector=genes_vector,
                                                                                                      go_term_ids=peaks_term_ids,
                                                                                                      peaks_ids=peaks_ids,
                                                                                                      **kwargs)
            except Exception:
                print('failed with GO ID', goid)
                stop()

            # print('done...')
            # print n_perm, 'permutations time m/s', get_timespan_minutes(tperm0), get_timespan_seconds(tperm0)

            # print 'GENES', mu_genes, sigma_genes, z_genes
            # print 'PEAKS', mu_peaks, sigma_peaks, z_peaks
            result = [mu_genes, z_genes, mu_peaks, z_peaks, 'default']
            res.append(t + result)

            # append the results using z.transform
            if kwargs.get('zscore_ztransform', False):
                result = [mu_genes, z_genes_zt, mu_peaks, z_peaks_zt, 'z.transform']
                res.append(t + result)

            if stopat is not None and len(res) > stopat:
                break

        default_cols = ['id', 'name', 'n.genes.term', 'n.genes.hits', 'n.peaks.total',
                        'n.peaks.hits', 'annotation.weight', 'binom.pval',
                        'a', 'b', 'c', 'd', 'odds.ratio', 'pval']

        # print len(res)
        if len(res) > 0 and len(res[0]) > len(default_cols):
            default_cols += ['n.genes.permutations', 'z.score.genes', 'n.peaks.permutations', 'z.score.peaks', 'z.calc.approach']

        res = pd.DataFrame(res, columns=default_cols)
        res['z.score.n.perm'] = n_perm

        # print('delete permutations backup')
        if hasattr(self, 'ids_by_permutation'):
            del self.ids_by_permutation

        time_secs = get_timespan_seconds(t0)
        print('GREAT done. running time', 'time [min/secs]', get_timespan_minutes(t0), time_secs)
        print('Time per GO terms [secs]: %.2f' % (time_secs / res.shape[0]))

        res['n.regions'] = peaks.shape[0]

        if zscore_column is not None and use_croutine:
            outdir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/combinatorial_binding_analysis_invivo/src/peaks_downsampling_intersection_c'
            # DataFrameAnalyzer.to_tsv(all_df, join(join(outdir, 'peaks_many.tsv')), header=None)
            res['id.peak'] = [i for i in range(res.shape[0])]
            n_hits_by_term = res[['id.peak', 'n.peaks.hits', 'n.genes.hits', 'n.peaks.total']]
            del res['id.peak']
            # DataFrameAnalyzer.to_tsv(n_hits_by_term,
            #                          join(join(outdir, 'n_hits_by_term.tsv')), header=None)
            names = pd.DataFrame([c for c in all_df])
            # DataFrameAnalyzer.to_tsv(names, join(outdir, 'names.txt'), header=None)
            return res, all_df, n_hits_by_term, names

        else:
            return res


    def calculate_zscore_downsampling_genes(self, n_genes_hits, all_remap_ids, npeak_downsample, genes_term, genes_by_peak, n_perm,
                                            peaks_vector=None, genes_vector=None, go_term_ids=None, peaks_ids=None,
                                            add_n_genes_hits=True, **kwargs):

        n_peaks_select = npeak_downsample


        # print(peaks_ids[:5], peaks_vector[:5], genes_vector[:5], np.array(list(genes_term_ids))[:5], n_peaks_select)

        # print('# perm', n_perm)
        samples = self.get_samples(peaks_ids, peaks_vector, genes_vector, np.array(list(go_term_ids)),
                                   n_peaks_select, n_perm=n_perm, mode='genes')

        mu, sigma = np.mean(samples), np.std(samples)

        # print(mu, sigma, np.max(samples))
        # print(samples[:5])

        zscore_raw = (n_genes_hits - mu) / sigma

        if kwargs.get('save_bg_distr_plot', False):
            print('GoF p-value (Poisson - Genes)')
            print('# values', len(samples))
            print('values[1:20]', samples[:20])
            print('poisson GoF (p-value):', RFacade.get_gof_pvalue(samples, method=kwargs.get('gof_method', 'poisson')))

        # print(n_genes_hits, samples)
        zscore_ztransform = self.get_zscore_ztransformed_values(n_genes_hits, np.append(samples, n_genes_hits))
        # print 'z.score (default)', zscore_raw
        # print 'z.score (z.transform)', zscore_ztransform
        return mu, sigma, zscore_raw, zscore_ztransform


    @staticmethod
    @jit(nopython=True)
    def count_genes(next_ids, genes_term, peaks, genes):
        mask = np.repeat(0, len(peaks))
        for r in next_ids:
            mask += peaks == r
        mask = np.where(mask > 1, 1, mask)
        n_count = len(set(genes[mask == 1]).intersection(set(genes_term)))
        return n_count

    @staticmethod
    @jit(nopython=True)
    def count_peaks(next_ids, peaks_term, peaks, genes):
        mask = np.repeat(0, len(peaks))
        for r in next_ids:
            mask += peaks == r
        mask = np.where(mask > 1, 1, mask)
        n_count = len(set(peaks[mask == 1]).intersection(set(peaks_term)))
        return n_count

    def get_samples(self,
                       peak_ids,
                       peaks_input,
                       genes_input,
                       ids_term,
                       n_peaks_select,
                       n_perm=1000,
                       log=False,
                       print_log_each=25,
                    mode='genes'):

        out = []

        if not hasattr(self, 'ids_by_permutation'):
            self.ids_by_permutation = {}
            self.ids_by_permutation[n_peaks_select] = {}

        # print('downsampling %i peaks to subsets of size %i' % (len(peak_ids), n_peaks_select))
        # print('n.perm %i' % n_perm)
        for i in np.arange(n_perm):
            if log and i % print_log_each == 0:
                print('# permutations', i)

            next_ids = None
            if i not in self.ids_by_permutation[n_peaks_select]:
                next_ids = np.random.choice(peak_ids, n_peaks_select, replace=False)
                self.ids_by_permutation[n_peaks_select][i] = next_ids
            else:
                next_ids = self.ids_by_permutation[n_peaks_select][i]

            if len(ids_term) != 0:
                if mode == 'genes':
                    # print(next_ids, ids_term)
                    n_count = GREATAnalyzer.count_genes(next_ids, ids_term, peaks_input, genes_input)
                elif mode == 'peaks':
                    n_count = GREATAnalyzer.count_peaks(next_ids, ids_term, peaks_input, genes_input)
            else:
                n_count = 0

            out.append(n_count)

        return np.array(out)

    def calculate_zscore_pascal_triangle(self, N, k, genes_by_peak, genes_term):
        '''
        This implementation uses combinatorics to get the estimated number of genes obtained when trying to obtain our desired expected number of genes (or peaks)
        # implemented with input/ideas from Andrew OHeachteirn
        :param N:
        :param k:
        :param genes_by_peak:
        :return:
        '''
        # convert P->G into G->P
        peaks_by_gene = {}
        for p in genes_by_peak:
            genes = genes_by_peak[p]
            for g in genes:
                if g in genes_term:
                    if not g in peaks_by_gene:
                        peaks_by_gene[g] = set()
                    peaks_by_gene[g].add(p)
        # print peaks_by_gene
        numerators_by_gene = {}
        denominators_by_gene = {}

        previous_results_by_code = {}
        selected_genes = {g: peaks_by_gene[g] for g in peaks_by_gene if len(peaks_by_gene[g]) > 0}
        # print len(selected_genes)

        N = len(N) if not isinstance(N, int) else N
        for gi, g in enumerate(selected_genes):
            m = len(peaks_by_gene[g])
            n = N - m

            if gi % 1500 == 0:
                print('n genes so far', gi)
                print('gi=%i, n=%i, m=%i, k=%i' % (gi, n, m, k))

            code = "_".join(map(str, (n, m, k)))
            res = None
            if not code in previous_results_by_code:
                print(n, m, k)
                previous_results_by_code[code] = self.n_ksubsets_more_than_one_pascal(n, m, k)

            res = previous_results_by_code[code]
            numerators_by_gene[g] = res
            denominators_by_gene[g] = self.pascal.get_coef_at(N, k)

        est_avr = 0.0
        for g in numerators_by_gene:
            numerator, denominator = numerators_by_gene[g], denominators_by_gene[g]
            # print n, d, float(n) / d
            est_avr += float(numerator) / denominator
        print('estimated average', est_avr)
        return est_avr

    def calculate_zscore_downsampling_peaks(self, n_peaks_hits, all_remap_ids, npeak_downsample, genes_term, genes_by_peak, n_perm,
                                            peaks_vector=None, genes_vector=None, go_term_ids=None, peaks_ids=None,
                                            add_n_genes_hits=True, **kwargs):

        n_peaks_select = npeak_downsample

        # print('GO IDS', go_term_ids)
        # print(peaks_ids[:5], peaks_vector[:5], genes_vector[:5], np.array(list(genes_term_ids))[:5], n_peaks_select)
        samples = self.get_samples(peaks_ids, peaks_vector, genes_vector, np.array(list(go_term_ids)),
                                   n_peaks_select, n_perm=n_perm, mode='peaks')

        mu, sigma = np.mean(samples), np.std(samples)

        # print(mu, sigma, samples[:5])
        # print(mu, sigma, np.max(samples))
        # print(samples[:5])

        zscore_raw = (n_peaks_hits - mu) / sigma

        if kwargs.get('save_bg_distr_plot'):
            print('GoF p-value (Poisson - Peaks)')
            print('# values', len(n_peaks))
            print('values[1:20]', n_peaks[:20])
            print('poisson GoF (p-value):', RFacade.get_gof_pvalue(n_peaks, method=kwargs.get('gof_method', 'poisson')))

        z_raw = (n_peaks_hits - mu) / sigma
        zscore_ztransform = self.get_zscore_ztransformed_values(n_peaks_hits, np.append(samples, n_peaks_hits))

        # print 'z.score (default)', z_raw
        # print 'z.score (z.transform)', zscore_ztransform

        return mu, sigma, z_raw, zscore_ztransform

    def get_zscore_ztransformed_values(self, observed, samples):
        if len(set(samples)) == 1:
            return np.nan
        zt = self.get_ztransformed_values(samples, fit_max=True)
        mu = np.mean(zt[~np.isinf(zt)])
        sigma = np.std(zt[~np.isinf(zt)])

        zt_by_observed = {}
        for vi, zi in zip(samples, zt):
            zt_by_observed[vi] = zi
        return (zt_by_observed[observed] - mu) / sigma

    def get_zscore_poisson(self, xi, poisson_lambda):
        return 2 * (math.sqrt(xi) - math.sqrt(poisson_lambda))

    def prepare_tss_create_regdoms(self, genome,
                                   datdir_greattools=None,
                                   outdir=None, **kwargs):
        system('module load HTSlib')

        if datdir_greattools is None:
            datdir_greattools = '/g/scb2/zaugg/rio/data/greatTools/data'

        outdir =  join(datdir_greattools, genome)
        if not exists(outdir):
            mkdir(outdir)

        output_path = join(outdir, 'TSS.in')
        tss = MyGeneAnalyzer.get_gene_tss(genome, **kwargs)
        tss = tss.rename(
            columns={'chr': 'chromosome', 'tss': 'transcription start site', 'pos': 'strand', 'SYMBOL': 'geneName'})
        tss = tss[['chromosome', 'transcription start site', 'strand', 'geneName']]
        tss['strand'] = np.where(tss['strand'], '-', '+')
        tss['transcription start site'] = tss['transcription start site'].astype(int)
        DataFrameAnalyzer.to_tsv(tss, output_path, header=None)


    def create_regdoms(self, tss_in, chrom_sizes, regDoms_out,
                       great_bin=None):
        # tss_in = join(output_dir, 'TSS.in')
        # chrom_sizes = join(outdir, 'chrom.sizes')
        # regDoms_out = join(outdir, 'regDoms.out')

        # this assumes that great is installed in the current operating system.
        # a distribution of create is currently not provided. Please download/compile
        if great_bin is None:
            great_bin = './createRegulatoryDomains' # /g/scb2/zaugg/rio/data/greatTools/createRegulatoryDomains'
        cmd = [great_bin, tss_in, chrom_sizes, 'basalPlusExtension', regDoms_out]
        print(' '.join(cmd))
        system(' '.join(cmd))



    def get_ztransformed_values(self, x, fit_max=True):
        # normalize between zero and one
        n = (x - np.min(x)).astype(float) / (np.max(x) - np.min(x))

        # TO AVOID INFINITY AFTER EXECUTING ARCTANH, THE MAXIMUM VALUE IS CONVERTED TO from one to (1 - 1 / max_n_observed)
        # e.g. if max == 100, then max = 1 - 1 / 100 = 1 - 0.01 = 0.99
        if fit_max:
            n = np.where(n == np.max(n), 1.0 - (1.0 / np.max(x)), n)
        return np.arctanh(n)

    def calculate_zscore_downsampling_croutine(self, all_df, n_hits_by_term, names, n_permutations):

        print('preparing input files and tmp output file...')
        p1 = tempfile.mkstemp()[1]
        p2 = tempfile.mkstemp()[1]
        p3 = tempfile.mkstemp()[1]
        p4 = tempfile.mkstemp()[1]
        DataFrameAnalyzer.to_tsv(all_df, p1, header=None, log=False)
        DataFrameAnalyzer.to_tsv(n_hits_by_term, p2, header=None, log=False)
        names = pd.DataFrame([c for c in all_df])
        DataFrameAnalyzer.to_tsv(names, p3, header=None, log=False)

        bin_path = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/combinatorial_binding_analysis_invivo',
                        'src/peaks_downsampling_intersection_c/intersection_many')
        bin = ' '.join([bin_path, p3, p2, p1, str(n_permutations), '>', p4])
        print(bin)
        print('Calculating Z-scores for %i GO terms' % n_hits_by_term.shape[0])
        print('# permutations', n_permutations)
        print('Running time (est.)', n_hits_by_term.shape[0] * 10, 'seconds')
        t0 = get_time_now()
        system(bin)
        print('done in m/s')
        print(get_timespan_minutes(t0), get_timespan_seconds(t0))
        print('seconds per term')
        print(get_timespan_seconds(t0) / n_hits_by_term.shape[0])

        zscores = DataFrameAnalyzer.read_tsv(p4)
        for p in [p1, p2, p3, p4]:
            remove(p)

        return zscores







