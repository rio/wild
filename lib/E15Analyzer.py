'''
Created on 12/2/2017, 2017

@author: Ignacio Ibarra Del Rio

Description:
'''

from lib.utils import *
from lib.plot_utils import *
from lib.PeaksAnalyzer import PeaksAnalyzer
from lib.SequenceMethods import SequenceMethods
from lib.FastaAnalyzer import FastaAnalyzer
# from lib.MyGeneAnalyzer import MyGeneAnalyzer

class E15Analyzer():

    @staticmethod
    def get_atac_human_neurons_2019():
        basedir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/atac_loess_comparisons_human/extend_0_trimmed_pooledpeaks'
        all_dfs = []
        for f in listdir(basedir):
            p = join(basedir, f)
            if 'counts_norm.tsv.gz' in f:
                continue
            df = DataFrameAnalyzer.read_tsv_gz(p)
            df['treatment'] = f.replace(".tsv.gz", '')
            df = df[df['CHR'].isin({'chr%s' % c for c in list(range(1, 22)) + ['X', 'Y']})]
            all_dfs.append(df)
        data = pd.concat(all_dfs)
        from lib.SequenceMethods import SequenceMethods
        data['CHR'] = data['CHR'].astype(str)
        data['START'] = pd.to_numeric(data['START']).astype(int)
        data['END'] = data['END'].astype(int)
        data = SequenceMethods.parse_range2coordinate(data, names=['CHR', 'START', 'END'], k='k')
        return data

    @staticmethod
    def get_treatment_names():
        return ['bdnf', 'KCl', 'Forskolin']

    @staticmethod
    def get_count_matrices():
        atac = E15Analyzer.get_counts_atac()
        rna = E15Analyzer.get_counts_rna()

        mapping = DataFrameAnalyzer.read_tsv(join('/g/scb/zaugg/rio/EclipseProjects',
                                                  'zaugglab/noh_collaboration/data',
                                                  "rna_to_atac_count_ids.tsv"))
        rna_to_atac = DataFrameAnalyzer.get_dict(mapping, 'rna', 'atac')
        rna.columns = [rna_to_atac[k] for k in rna_to_atac]
        rna = rna[atac.columns]
        assert len(rna.columns) == len(atac.columns)
        return atac, rna

    @staticmethod
    def get_auroc_enrichments_cisbp():
        output_dir = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/aurocs_cisbp_by_motif"
        auroc = DataFrameAnalyzer.read_multiple_tsv_gz(output_dir)
        return auroc

    @staticmethod
    def get_invivo_social_defeat_livia_rnaseq():
        basedir = '/g/scb2/zaugg/rio/data/20180517_liviamarrone/deseq2'
        res = []
        for d in listdir(basedir):
            if not isdir(join(basedir, d)):
                continue
            p = join(basedir, d, 'defeated_vs_sensory.tsv')
            print(exists(p), p)
            df = DataFrameAnalyzer.read_tsv(p, index_col=0)
            df['ensembl'] = df.index
            df['k'] = d
            res.append(df)
        return pd.concat(res).reset_index(drop=True)


    @staticmethod
    def get_ctcf_loop_ruiz_velasco():
        rds_path = '/g/scb/zaugg/leyva/04_mDUE/02_R_DB/resubmission/motif.pairs.npc.witt.rds'
        mm10_basename = join(basename(rds_path).replace(".rds", "_mm10.tsv.gz"))
        bkp_dir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data'
        mm10_path = join(bkp_dir, mm10_basename)
        if not exists(mm10_path):
            from lib.RFacade import RFacade
            from lib.LiftOver import LiftOver
            df = RFacade.get_rds(rds_path)
            df['chr'] = 'chr' + df['chr'].astype(str)
            df = SequenceMethods.parse_range2coordinate(df, ['chr', 'start1', 'end1'], 'k1')
            df = SequenceMethods.parse_range2coordinate(df, ['chr', 'start2', 'end2'], 'k2')

            a = df[['chr', 'start1', 'end1', 'k1']]
            b = df[['chr', 'start2', 'end2', 'k2']]
            a.columns = ['chr', 'start', 'end', 'k']
            b.columns = ['chr', 'start', 'end', 'k']
            all_mm9 = pd.concat([a, b], axis=0)
            liftover_out = '/tmp/mm9_to_mm10.bed'
            LiftOver.convert_coordinates(all_mm9, 'mm9', liftover_out, 'mm10')
            out = DataFrameAnalyzer.read_tsv(liftover_out, header=None, columns=['chr', 'start', 'end', 'k'])
            out = SequenceMethods.parse_range2coordinate(out, ['chr', 'start', 'end'], 'k.mm10')
            mm10_by_mm9 = DataFrameAnalyzer.get_dict(out, 'k', 'k.mm10')
            df['k1.mm10'] = df['k1'].map(mm10_by_mm9)
            df['k2.mm10'] = df['k2'].map(mm10_by_mm9)

            df = SequenceMethods.parse_coordinate2range(df, df['k1.mm10'], ['chr.mm10', 'start1.mm10', 'end1.mm10'])
            df = SequenceMethods.parse_coordinate2range(df, df['k2.mm10'], ['chr.mm10', 'start2.mm10', 'end2.mm10'])
            DataFrameAnalyzer.to_tsv_gz(df, mm10_path)
            # do something with the dataframe
        return DataFrameAnalyzer.read_tsv_gz(mm10_path)

    @staticmethod
    def get_ctcf_peaks_hippocampal_neurons():
        p = '/g/scb2/zaugg/rio/data/PRJNA328227/macs/GSE84174_macs2_CHIP12-INP_12_peaks.bed.gz'
        p_mm10 = p.replace(".bed.gz", '_mm10.bed')
        if not exists(p_mm10):
            df = DataFrameAnalyzer.read_tsv_gz(p)
            df = DataFrameAnalyzer.read_tsv_gz(p, header=None, skiprows=1)
            from lib.LiftOver import LiftOver
            LiftOver.convert_coordinates(df[df.columns[:3]], 'mm9', p_mm10, 'mm10')
            df = DataFrameAnalyzer.read_tsv(p_mm10, header=None, columns=['chr', 'start', 'end'])

    @staticmethod
    def get_dexseq_due(annotation=None):
        # KCL vs bdnf (check comparison)
        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/mapped_tophat2/dexseq_estimateDispersions_zerocount_genes_removed_1_contrast.tsv.gz'
        if annotation is not None:
            p = p.replace("mapped_tophat2", 'mapped_tophat2_' + annotation)
        dexseq = DataFrameAnalyzer.read_tsv_gz(p)
        dexseq['ensembl'] = dexseq['groupID'].str.split(".").str[0]
        symbol_by_ensembl = MyGeneAnalyzer.get_symbol_by_ensembl(set(dexseq['ensembl']), species='mouse')
        dexseq['symbol'] = dexseq['ensembl'].map(symbol_by_ensembl)
        return dexseq

    @staticmethod
    def get_dexseq_due_all(annotation=None):
        # KCL vs bdnf (check comparison)
        df = []
        for t in [1, 6, 10]:
            p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/mapped_tophat2/dexseq_estimateDispersions_zerocount_genes_removed_' + \
                str(t) + '_contrast.tsv.gz'
            if annotation is not None:
                p = p.replace("mapped_tophat2", 'mapped_tophat2_' + annotation)

            print(exists(p), p)
            dexseq = DataFrameAnalyzer.read_tsv_gz(p)
            dexseq = dexseq[dexseq.columns[:7]]
            dexseq['time'] = t
            df.append(dexseq.reset_index(drop=True))
        df = pd.concat(df)
        df['id'] = df['groupID'] + ":" + df['featureID']

        df['ensembl'] = df['groupID'].str.split(".").str[0]
        symbol_by_ensembl = MyGeneAnalyzer.get_symbol_by_ensembl(set(df['ensembl']), species='mouse')
        df['symbol'] = df['ensembl'].map(symbol_by_ensembl)

        df = df[~pd.isnull(df['symbol']) & ~pd.isnull(df['ensembl'])]
        return df

    @staticmethod
    def get_dexseq_due_all_asheatmap():
        df = E15Analyzer.get_dexseq_due_all()
        hm = df.pivot('id', 'time', 'padj')
        hm = pd.DataFrame(-np.log10(hm), index=hm.index, columns=hm.columns)
        hm['max'] = hm.max(axis=1)
        hm = hm.reindex(hm.sort_values('max', ascending=False).index)
        del hm['max']
        return hm

    @staticmethod
    def get_chromvar_enrichments():
        basedir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/chromvar'
        all = []
        for f in listdir(basedir):

            if not 'KCl_CISBP_KCl_closed' in f:
                continue
            p = join(basedir, f)
            if not f.endswith('.tsv'):
                continue
            treatment = f.split("_")[0]
            motif_library = f.replace(treatment + "_", '').replace(".tsv", '')
            print(f, treatment, motif_library)
            df = DataFrameAnalyzer.read_tsv(p)
            df['motif.library'] = motif_library
            df['treatment'] = treatment
            all.append(df)
        all = pd.concat(all)
        return all

    @staticmethod
    def get_sample_description_atac():
        p = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20170921_ATAC_n_RNA_seq',
                 '20171006_sample_info_DiffBind_sixcontrols_pooledpeaks.tsv')
        df = DataFrameAnalyzer.read_tsv(p, sep=',')

        df['bamReads'] = [join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20170921_ATAC_n_RNA_seq/mapped_atac_new',
                               basename(bam)) for bam in df['bamReads']]
        df['Peaks'] = [f.replace('../../data/20170921_ATAC_n_RNA_seq/macs_new',
                                 '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20170921_ATAC_n_RNA_seq/macs_new')
                       for f in df['Peaks']]
        for c in ['bamReads', 'Peaks']:
            for p in df[c]:
                assert exists(p)
        return df

    @staticmethod
    def plot_x_vs_y_log2_counts(counts_x, counts_y, output_path='../../data/figures/geom_repel_test',
                                x_id='chr1:55914401-55914910', y_id='ENSMUSG00000038331',
                                title='x vs y'):
        x = counts_x[counts_x.index == x_id]
        y = counts_y[counts_y.index == y_id]

        assert x.shape[0] == 1
        assert y.shape[0] == 1

        # assert counts_atac.shape[0] == 1 and counts_rna.shape[0] == 1

        df = pd.concat([x, y]).transpose()
        df['color'] = df.index.str.split("_").str[0]
        df['shape'] = df.index.str.split("_").str[1].str.replace(".1", '')
        df['size'] = 1
        df['label'] = ''
        df['x'] = np.log2(df[x_id] + 1)
        df['y'] = np.log2(df[y_id] + 1)
        print(df)

        rho, pval = spearmanr(df['x'], df['y'])

        title = 'ATAC vs ATAC\nrho=%.2g, pval=%.3e' % (rho, pval)

        xmin, xmax = max(min(df['x']), 0), max(df['x']) + .1
        ymin, ymax = max(min(df['y']), 0), max(df['y']) + .1

        from lib.RFacade import RFacade
        RFacade.plot_scatter_ggrepel(df, output_path,
                                     w=6, h=6, title=title,
                                     xlab=x_id + "\nlog2(1 + counts)",
                                     ylab=y_id + "\nlog2(1 + counts)",
                                     xmin=xmin, xmax=xmax,
                                     ymin=ymin, ymax=ymax,
                                     size_lab='time', color_lab='treatment',
                                     size_breaks_labs=['1'], size_breaks=[1])

    @staticmethod
    def get_expr_human_neurons():
        basedir_human = '/g/scb2/zaugg/saver/collaborations/Tony/iNeuron_Rnaseq_Atacseq_summary/data/'
        rna_human_path = '/g/scb2/zaugg/saver/collaborations/Tony/iNeuron_Rnaseq_Atacseq_summary/data/rnaseq_sum.csv.gz'
        expr_human = DataFrameAnalyzer.read_tsv_gz(rna_human_path, sep=',')
        return expr_human

    @staticmethod
    def get_atac_counts_human_neurons():
        basedir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20180910_ATAC_human/atac_loess_comparisons/extend_0_trimmed_pooledpeaks'
        res = []
        for f in listdir(basedir):
            if not 'counts_norm' in f:
                continue
            p = join(basedir, f)
            atac = DataFrameAnalyzer.read_tsv_gz(p)
            print(atac.head())
            res.append(atac)
        atac = pd.concat(res).reset_index(drop=True)
        return atac

    @staticmethod
    def get_atac_human_neurons(version='Mathias', basedir=None):
        if version == 'Anthony':
            basedir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20180910_ATAC_human/atac_loess_comparisons/extend_0_trimmed_pooledpeaks'
            res = []
            for f in listdir(basedir):
                if not f.endswith('.tsv.gz'):
                    continue
                if 'counts_norm' in f:
                    continue
                p = join(basedir, f)
                atac = DataFrameAnalyzer.read_tsv_gz(p)
                print(atac.head())
                atac['treatment'] = f.replace(".tsv.gz", '')
                res.append(atac)
            atac = pd.concat(res).reset_index(drop=True)
            return atac
        elif version == 'Mathias':
            # prepare atac peaks
            if basedir is None:
                basedir = '/g/scb2/zaugg/saver/collaborations/for Nacho/'
            noh = join(basedir, 'res_all_minOlap_2.tsv')
            atac = DataFrameAnalyzer.read_tsv(p)
            atac = SequenceMethods.parse_coordinate2range(atac, atac['peakID'], ['CHR', 'START', 'END'])
            return atac

    @staticmethod
    def get_expr_values(add_names=True, overwrite=False, tophat=True, basedir=None):
        all = []
        if tophat:
            if basedir is None:
                basedir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20180516_rna_tophat'
            all_path = join(basedir, 'all_with_names.tsv.gz')
            if exists(all_path):
                return DataFrameAnalyzer.read_tsv_gz(all_path)

            for d in listdir(basedir):
                if 'all' in d:
                    continue
                for treatment in ['KCl', 'bdnf', 'forskolin']:
                    p = join(basedir, d, treatment + "_vs_control.tsv")
                    print(exists(p), p)
                    df = DataFrameAnalyzer.read_tsv(p, index_col=0)
                    df['treatment'] = treatment + "_" + d.replace("time_", '')
                    df['stimuli'] = treatment
                    df['time'] = int(d.replace("time_", '')[:-1])
                    df['ensembl_gene_id'] = df.index
                    df['ensembl_gene_id'] = df['ensembl_gene_id'].astype(str)
                    all.append(df)

        else:
            for dataset_id in ['KCl_1h', 'KCl_6h', 'KCl_10h',
                               'bdnf_1h', 'bdnf_6h', 'bdnf_10h',
                               'forskolin_1h', 'forskolin_6h', 'forskolin_10h']:
                treatment, timepoint = dataset_id.split("_")
                timepoint = timepoint[:-1]

                scb_dir = 'scb2'
                rna_path = join("/g/" + scb_dir +
                                "/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/PNCRNAPOOL_DE_genes",
                               "deseq_matrix_PNCRNAPOOL_ensGene_Treatment_" +
                               treatment + '_vs_control_' + timepoint + ".tsv")
                rna = DataFrameAnalyzer.read_tsv(rna_path)

                rna['ensembl_gene_id'] = rna.index
                rna['ensembl_gene_id'] = rna['ensembl_gene_id'].astype(str)
                rna['treatment'] = dataset_id.replace("forskolin", 'Forskolin')
                rna['comparison'] = treatment + "_vs_control"
                all.append(rna)

        all = pd.concat(all).reset_index(drop=True)
        if add_names:
            names_by_ensembl_bkp = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration',
                                        'data/PNCRNAPOOL_DE_genes/names_by_ensembl_tophat' + str(tophat) + '.pkl')
            if not exists(names_by_ensembl_bkp) or overwrite:
                name_by_ensembl = MyGeneAnalyzer.get_symbol_by_ensembl(set(all['ensembl_gene_id']),
                                                                     species='mouse')
                DataFrameAnalyzer.to_pickle(name_by_ensembl, names_by_ensembl_bkp)
            name_by_ensembl = DataFrameAnalyzer.read_pickle(names_by_ensembl_bkp)
            all['external_gene_name'] = [name_by_ensembl[k] if k in name_by_ensembl else None
                                         for k in all['ensembl_gene_id']]
            if tophat:
                all_path = join(basedir, 'all_with_names.tsv.gz')
                if not exists(all_path):
                    DataFrameAnalyzer.to_tsv_gz(all, all_path)
        return all

    @staticmethod
    def get_atac_values_w_closest_gene():
        atac = E15Analyzer.get_atac_values()
        close_peaks = E15Analyzer.get_close_peaks_per_gene()

        close_peaks_df = pd.DataFrame([[k] + t for k in close_peaks for t in close_peaks[k]],
                                      columns=['ensembl', 'chr', 'start', 'end', 'distance'])
        close_peaks_df = SequenceMethods.parse_range2coordinate(close_peaks_df, ['chr', 'start', 'end'], 'k')

        close_peaks_df = close_peaks_df.sort_values('distance')
        close_peaks_df = close_peaks_df.drop_duplicates('k')
        close_peaks_df = close_peaks_df[['ensembl', 'distance', 'k']]
        bkp_path = '/tmp/atac_n_close_peaks.pkl'
        if not exists(bkp_path):
            symbol_by_ensembl = MyGeneAnalyzer.get_symbol_by_ensembl(set(close_peaks_df['ensembl']),
                                                                     species='mouse')
            DataFrameAnalyzer.to_pickle(symbol_by_ensembl, bkp_path)
        symbol_by_ensembl = DataFrameAnalyzer.read_pickle(bkp_path)
        atac = atac.merge(close_peaks_df, on='k')
        atac['symbol'] = atac['ensembl'].map(symbol_by_ensembl)
        return atac

    @staticmethod
    def get_hic_scores_tss(gene_name):
        basedir = '/g/scb/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/hic_tss_by_gene_tsv_gz'
        p = join(basedir, gene_name + ".tsv.gz")
        assert exists(p)
        return DataFrameAnalyzer.read_tsv_gz(p)
    @staticmethod
    def get_counts_atac(datadir=None):
        if datadir is None:
            datadir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/atac_loess_comparisons/extend_0_trimmed_pooledpeaks'
        p = join(datadir, 'bdnf_1h.tsv.gz')
        df = DataFrameAnalyzer.read_tsv_gz(p)
        df = SequenceMethods.parse_range2coordinate(df, ['CHR', 'START', 'END'], 'k')
        df = df[list(df.columns)[-25:]]
        df.index = df['k']
        del df['k']
        return df

    @staticmethod
    def get_counts_atac_human_neurons():
        p = '/g/scb2/zaugg/saver/collaborations/Tony/iNeuron_Rnaseq_Atacseq_summary/atac_counts/atac_counts_df.csv'
        df = DataFrameAnalyzer.read_tsv(p, sep=',')
        return df

    @staticmethod
    def get_counts_rna(datadir=None):
        if datadir is None:
            datadir = '/g/scb/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/'
        p = join(datadir, 'PNCRNAPOOL_DE_genes', 'counts.norm.all.tsv.gz')
        df = DataFrameAnalyzer.read_tsv_gz(p)
        return df

    @staticmethod
    def plot_rna_expr(rna, module_id, output_path, vmin=-3, vmax=3,
                      annot=None, w=7, h=5, treatments_order=['bdnf', 'KCl'],
                      xticks_pos=[.5, 1.5, 2.5], xticks_names=['1', '6', '10']):

        sns.set_style('white')
        fig = plt.figure(figsize=(w, h))

        cbar_ax = fig.add_axes([.81, .3, .03, .4])
        for i, k in enumerate(treatments_order):

            plt.subplot(1, 3, i + 1)

            sns.heatmap(rna[[c for c in rna.columns if k in c]],
                        cbar=i==1,
                        cmap='RdBu_r', vmax=vmax, vmin=-vmax,
                        cbar_ax=cbar_ax if i == 1 else None,
                        yticklabels=True,
                        annot=annot[[c for c in rna.columns if k in c]],
                        linewidth=0.5, cbar_kws={'label': 'log2FC'}, fmt='',
                        annot_kws={'size': 4 if rna.shape[0] > 20 else 10})
            plt.title(k)
            plt.yticks(rotation=0)
            if i == 1:
                plt.yticks([])
            else:
                plt.yticks([pi + 0.5 for pi in range(rna.shape[0])],
                           list(rna.index), fontsize=4 if rna.shape[0] > 20 else 10)

            plt.ylabel(module_id if i == 0 else '')
            plt.xlabel('time [h]')
            plt.xticks(xticks_pos, xticks_names, rotation=0)

        # splt.tight_layout()
        # plt.tight_layout()
        fig.tight_layout(rect=[0.15, 0, .9, 1])

        if rna.shape[0] < 5:
            plt.subplots_adjust(bottom=0.5 if rna.shape[0] > 2 else .8)

        # plt.subplots_adjust(left=0.2, right=0.7, bottom=0.2)
        savepdf(output_path)
        plt.close()

    @staticmethod
    def get_n_kmers_by_atac_peak():
        bkp_path = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/n_modules_by_peak.tsv.gz"
        glossary_kmer_dir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/genre_modules_vs_macs_peaks'
        if not exists(bkp_path):
            df = DataFrameAnalyzer.read_multiple_tsv(glossary_kmer_dir)

            res = []
            for peak_id, grp in df.groupby('peak.id'):
                if len(res) % 1000 == 0:
                    print(len(res))
                res.append(grp['module.id'].value_counts().reset_index())
                res[-1].columns = ['module.id', 'counts']
                res[-1]['peak.id'] = peak_id
            res = pd.concat(res).pivot(index='peak.id', columns='module.id', values='counts').fillna(0)
            DataFrameAnalyzer.to_tsv_gz(res, bkp_path, index=True)
        return DataFrameAnalyzer.read_tsv_gz(bkp_path, index_col=0)


    @staticmethod
    def get_tfs_by_genre_module():
        modules_description_dir = join('/g/scb/zaugg/rio/EclipseProjects/wild',
                                       'lib/genre/glossary_description/Glossary Modules')
        module_filenames = [f for f in listdir(modules_description_dir) if f.endswith('.xlsx')]

        tfs_by_module = {}
        for module_filename in module_filenames:
            module_name = module_filename.replace("Glossary_module_", "").replace(".xlsx", '')
            # if module_name != 'NR2E':
            #     continue
            output_path = join("../../data/figures/e15_expr_by_kmer_module", module_name)
            # if exists(output_path + ".pdf"):
            #     continue
            p = '/g/scb/zaugg/rio/EclipseProjects/wild/lib/genre/glossary_description/Glossary Modules/Glossary_module_' + module_name + '.xlsx'

            df = pd.read_excel(p, index_col=0)

            if '(' in module_name:
                module_name = module_name.replace(")", '').replace("(", '-')
            tfs_by_module[module_name] = set()
            for c in df.columns:
                tfs_by_module[module_name].add(c)

        return tfs_by_module

    @staticmethod
    def get_atac_id_200bp_peak_ids_associations():
        '''
        When running genre we divided our peaks into 200bp regions. In this method, we recover the associations
        between 200bp peaks and the respective ATAC-seq peaks (DiffBind)
        :return:
        '''

        bkp_path = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/atac_to_peak_200bp.tsv.gz'

        if not exists(bkp_path):
            from lib.FastaAnalyzer import FastaAnalyzer
            dir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/atac_loess_comparisons/extend_0_pooledpeaks'

            # load ids
            # headers 200bp
            all_headers = set()
            for d in listdir(dir):
                if not isdir(join(dir, d)):
                    continue
                #print join(dir, d)
                for f in listdir(join(dir, d)):
                    if not f.endswith('.fa') or 'dfltBG' in f:
                        continue
                    print(f)
                    headers, seqs = list(zip(*FastaAnalyzer.get_fastas(join(dir, d, f))))
                    all_headers = all_headers.union(set(headers))

            atac_fc = E15Analyzer.get_atac_values()
            ids_200bp = pd.DataFrame(list(all_headers))

            ids_200bp = SequenceMethods.parse_coordinate2range(ids_200bp, ids_200bp[0])
            atac_fc = atac_fc.drop_duplicates('k')
            nA, nB, nCommon, output_bed = PeaksAnalyzer.intersect_coordinates(ids_200bp[['chr', 'start', 'end']],
                                                                              atac_fc[['CHR', 'START', 'END']],
                                                                              additional_args=['-wao'])
            print(nA, nB, nCommon, output_bed)
            res = DataFrameAnalyzer.read_tsv(output_bed, header=None)
            res = SequenceMethods.parse_range2coordinate(res, names=[0, 1, 2], k='peak.id.200bp')
            res = SequenceMethods.parse_range2coordinate(res, names=[3, 4, 5], k='peak.id.atac')
            DataFrameAnalyzer.to_tsv_gz(res, bkp_path)

        return DataFrameAnalyzer.read_tsv_gz(bkp_path)

    @staticmethod
    def get_chromhmm_states_atac():
        bkp_path_atac_vs_chromHMM = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/atac_vs_chromHMM_all.tsv.gz'
        print(exists(bkp_path_atac_vs_chromHMM), bkp_path_atac_vs_chromHMM)
        if not exists(bkp_path_atac_vs_chromHMM):
            p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/Hippocampus_ChromHMM_model/neuron_15_segments.bed'
            df = DataFrameAnalyzer.read_tsv(p, header=None)
            df = SequenceMethods.parse_range2coordinate(df, [0, 1, 2], 'k')
            atac = E15Analyzer.get_atac_values()
            from pybedtools import BedTool
            from lib.LiftOver import LiftOver
            p_mm9 = '/tmp/mm9_liftover'
            uniq = atac[['CHR', 'START', 'END', 'k']].reset_index(drop=True).drop_duplicates('k').reset_index(drop=True)
            LiftOver.convert_coordinates(uniq, 'mm10', p_mm9, 'mm9', overwrite=True)
            mm9_df = DataFrameAnalyzer.read_tsv(p_mm9, header=None, columns=['chr', 'start', 'end', 'k'])
            mm9_df['k.mm10'] = mm9_df['k']
            mm9_df = SequenceMethods.parse_range2coordinate(mm9_df, ['chr', 'start', 'end'], 'k.mm9')
            mm9_by_mm10 = DataFrameAnalyzer.get_dict(mm9_df, 'k.mm10', 'k.mm9')
            atac['k.mm9'] = atac['k'].map(mm9_by_mm10)
            # intersect all and keep it in backgroup
            unique_peaks = atac[['k', 'k.mm9']].drop_duplicates('k')

            unique_peaks = unique_peaks[~pd.isnull(unique_peaks['k.mm9'])]
            unique_peaks = SequenceMethods.parse_coordinate2range(unique_peaks, unique_peaks['k.mm9'],
                                                                  ['CHR.mm9', 'START.mm9', 'END.mm9'])
            chromhmm_peaks = BedTool.from_dataframe(df)
            a_and_b_all = BedTool.from_dataframe(unique_peaks[['CHR.mm9', 'START.mm9',
                                                               'END.mm9', 'k.mm9', 'k']]).intersect(chromhmm_peaks,
                                                                                                    wo=True).to_dataframe()
            a_and_b_all = a_and_b_all[a_and_b_all[a_and_b_all.columns[0]] != 'chrM']

            # replace names with putative state
            state_name = {14: 'Insulator (CTCF)',
                          1: 'Transcripted region',
                          2: 'Strong transcripted region',
                          3: 'Low signal',
                          4: 'Gene region',
                          12: 'Low signal',
                          5: 'Enhancers with gene',
                          6: 'Downstream of TSS',
                          7: 'Active enhancers',
                          8: 'Active TSS',
                          9: 'Upstream of TSS',
                          10: 'Bivalent pomoters',
                          11: 'Repressed polycomb',
                          13: 'Heterochromatin',
                          15: 'Moderate enhancers'}

            a_and_b_all['state.name'] = a_and_b_all['itemRgb'].str[1:].astype(int).map(state_name)
            a_and_b_all = a_and_b_all.rename(columns={'score': 'k'})

            DataFrameAnalyzer.to_tsv_gz(a_and_b_all, bkp_path_atac_vs_chromHMM)
        return DataFrameAnalyzer.read_tsv_gz(bkp_path_atac_vs_chromHMM)

    @staticmethod
    def get_atac_values(trimqilmed_reads=True, datadir=None):
        print('reading atac values...')

        if datadir is None or not exists(join(datadir, 'all_peaks_counts.tsv.gz')):
            dirname = 'extend_0_pooledpeaks' if not trimmed_reads else 'extend_0_trimmed_pooledpeaks'
            atac_dir = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab',
                            'noh_collaboration/data/atac_loess_comparisons/' + dirname)
            all = []

            for f in listdir(atac_dir):
                if not f.endswith(".tsv.gz"):
                    continue
                if not 'h.' in f:
                    continue
                if len(f.split("_")) != 2:
                    continue

                next_path = join(atac_dir, f)
                # print next_path

                df = DataFrameAnalyzer.read_tsv_gz(next_path)
                # print f
                # print df.head()
                # print df[df['padj'] < 0.1].shape

                df['treatment'] = f.replace(".tsv.gz", "")
                all.append(df)

            all = pd.concat(all).reset_index(drop=True)
            all['k'] = all['CHR'] + ":" + all['START'].astype(str)  + "-" + all['END'].astype(str)
            print('saving bkp for later runs...')
            if atac_dir is not None:
                p = join(atac_dir, 'all_peaks_counts.tsv.gz')
                print(exists(p), p)
                if not exists(p):
                    DataFrameAnalyzer.to_tsv_gz(all, p)
            return all
        else:
            p = join(datadir, 'all_peaks_counts.tsv.gz')
            assert exists(p)
            print(p, exists(p))
            return DataFrameAnalyzer.read_tsv_gz(p)

    @staticmethod
    def get_summits_by_diffbind_ranges(diffbind_ranges, treatment='bdnf', time='1h', group='all', peak_size=200,
                                       overwrite=False):
        print('loading summit for diffbind peaks associated to specific treatment...')
        diffbind_macs_intersection = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/DiffBind_consensus_peaks_summits_intersection'
        macs_peaks_dir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20170921_ATAC_n_RNA_seq/macs_new'

        for macs_filename in listdir(macs_peaks_dir):
            if not macs_filename.endswith("trimmed_summits.bed"):
                # print 'bed peaks does not file exists...'
                continue
            # print macs_filename
            k = (("1hr_" if time is None else time + "r_") + ('Bdnf' if treatment == 'all' else treatment.capitalize())).upper()
            print(k, k in macs_filename.upper())
            if not k in macs_filename.upper():
                # print 'not macs files...'
                continue
            if not macs_filename.count('Sample') == 2:
                print('macs file with one sample only...')
                continue


            macs_path = join(macs_peaks_dir, macs_filename)
            macs2_peaks = DataFrameAnalyzer.read_tsv(macs_path, header=None)

            print(macs_path)
            # print macs2_peaks.head()
            bed1 = tempfile.mkstemp()[1]
            bed2 = tempfile.mkstemp()[1]
            intersected_bed = join(diffbind_macs_intersection,
                                   treatment + "_" + group + "_" + time + ".tsv.gz")

            print(exists(intersected_bed), intersected_bed)

            if overwrite or not exists(intersected_bed):
                DataFrameAnalyzer.to_tsv(diffbind_ranges[['CHR', 'START', 'END']], bed1, header=None)
                DataFrameAnalyzer.to_tsv(macs2_peaks[[0, 1, 2]], bed2, header=None)
                print(bed1)
                print(bed2)

                peaks_analyzer = PeaksAnalyzer()
                nA, nB, nCommon = peaks_analyzer.insersect_chip_seq_peaks(bed1, bed2, intersected_bed,
                                                                          additional_args=['-wao'])
                print(nA, nB, nCommon)

                if nCommon == 0:
                    continue

            print(intersected_bed)

            intersected_df = DataFrameAnalyzer.read_tsv(intersected_bed, header=None)
            # print intersected_df.head()
            print(intersected_df.shape)
            intersected_df.columns = ['CHR.diffbind', 'START.diffbind', 'END.diffbind',
                                      'CHR.macs', 'START.summit', 'END.summit', 'n.bp']
            intersected_df['k'] = intersected_df['CHR.diffbind'].astype(str) + ":" + \
                                  intersected_df['START.diffbind'].astype(str) + "-" + \
                                  intersected_df['END.diffbind'].astype(str)
            # print intersected_df.head()

            diffbind_ranges = diffbind_ranges.merge(intersected_df, on='k')
            start_summits, end_summits = [], []
            for ri, r in diffbind_ranges.iterrows():
                if r['n.bp'] != 0:
                    start = int((r['START.summit'] - peak_size / 2))
                    end = int((r['START.summit'] + peak_size / 2))
                else:
                    start = int(((r['START'] + r['END']) / 2.0 - peak_size / 2))
                    end = int(((r['START'] + r['END']) / 2.0 + peak_size / 2))
                start_summits.append(start)
                end_summits.append(end)

            diffbind_ranges['START.summit'] = start_summits
            diffbind_ranges['END.summit'] = end_summits

            bed_path = intersected_bed.replace(".tsv.gz", '.bed')
            # print diffbind_ranges.head(100)

            # print diffbind_ranges['k'].value_counts()
            output_df = diffbind_ranges[['CHR', 'START.summit', 'END.summit', 'k']]
            output_df.columns = list(output_df.columns[:-1]) + ['diffbind.range']
            output_df = SequenceMethods.parse_range2coordinate(output_df, ['CHR', 'START.summit', 'END.summit'], 'summit.range')

            # this step should not change the size of the matrix
            # print output_df.shape
            output_df = output_df.drop_duplicates('summit.range')
            # print output_df.shape

            print(bed_path)
            if overwrite or not exists(bed_path):
                DataFrameAnalyzer.to_tsv(output_df[['CHR', 'START.summit', 'END.summit']],
                                         bed_path, header=None)
            # print output_df.head()
            output_path = bed_path.replace(".bed", '_diffbind_peaks_by_summit.tsv.gz')
            if overwrite or not exists(output_path):
                DataFrameAnalyzer.to_tsv_gz(output_df[['CHR', 'START.summit', 'END.summit', 'diffbind.range', 'summit.range']],
                                            bed_path.replace(".bed", '_diffbind_peaks_by_summit.tsv.gz'))
            return output_df[['CHR', 'START.summit', 'END.summit', 'diffbind.range', 'summit.range']]

    @staticmethod
    def get_gene_coordinates(datadir=None):
        if datadir is None:
            datadir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data'
        df = DataFrameAnalyzer.read_tsv(join(datadir, 'genes_description_w_coordinates.tsv'))
        return df

    @staticmethod
    def calculate_auroc_by_module(treatment, time, module, sortby='log2FC', group='top', **kwargs):
        # print 'loading samples...'
        df = E15Analyzer.get_kmer_scores_by_module(treatment, time, module)
        return E15Analyzer.calculate_auroc_by_dataframe(df, sortby=sortby, group=group, **kwargs)

    @staticmethod
    def calculate_auroc_by_dataframe(df, sortby='log2FC', group='top', **kwargs):
        from lib.MachineLearning import MachineLearning
        # print 'sorting criteria...'
        # print sortby
        # sort by log2FC or by padj (into positives and negatives)
        if sortby == 'padjNlog2FC':
            pos, neg = df[df['log2FC'] >= 0], df[df['log2FC'] < 0]
            pos = pos.sort_values('padj', ascending=True)
            neg = neg.sort_values('padj', ascending=False)

            df = pd.concat([pos, neg])
        elif sortby == 'log2FC':
            df = df.sort_values('log2FC', ascending=False)
        else:
            print('sorting option not found')
        df = df.reset_index(drop=True)

        aurocs = []
        pos, neg = list(df['fg.score']), list(df['bg.score'])
        step = kwargs.get('step', 2500)
        stopat = kwargs.get('stopat', None)
        print('subsetsize =', step)
        start = kwargs.get('start', 1)

        for next in range(start, len(pos), step):
            a, b = None, None
            # print 'next threshold', next
            if group == 'top':
                a = pos[:min(next, len(pos))]
                b = neg[:min(next, len(neg))]
            elif group == 'bottom':
                a = pos[-min(next, len(pos)):]
                b = neg[-min(next, len(neg)):]
            y_pred = a + b
            y_true = list(np.repeat(1, len(a))) + list(np.repeat(0, len(a)))
            auroc, auprc = MachineLearning.get_auroc_n_auprc(y_true, y_pred)
            # auroc = MachineLearning.get_auroc_wilcoxon(y_true, y_pred)
            for i in range(len(a)):
                aurocs.append(auroc)
            if stopat != None and len(aurocs) >= stopat:
                break

        aurocs = aurocs[:df.shape[0]]
        # print len(aurocs)
        if group == 'bottom':
            aurocs = aurocs[::-1]

        df = df.head(len(aurocs))
        df['auroc'] = aurocs
        return df

    @staticmethod
    def get_invivo_social_defeat_livia_microarray():
        uniprot_by_name_final = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20180421_social_defeat_data/uniprot_by_name_final.tsv.gz"
        if not exists(uniprot_by_name_final):
            bkp_path = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20180421_social_defeat_data/merged_data.pkl"
            if not exists(bkp_path):
                expr_filenames = {'pv_.expression_woS1.gct', 'camk_expression.gct', 'sst_expression.wo1731.gct'}
                # we need to load expression tables with RefSeq IDs, to map them properly
                expr_refseq_filenames = {'PV_colldata_woS1_thr6,5.gct', 'camk_thr6,5.gct', 'sst thr7.gct'}

                comp_filenames = {'PV_colldata_woS1_thr6,5.comp.marker.odf', 'camk_thr6,5.comp.marker.odf',
                                  'sst thr7compmarker.odf'}
                basedir = "../../data/20180421_social_defeat_data"
                expr_by_treatment = {}
                expr_refseq_by_treatment = {}
                comp_by_treatment = {}
                for d in listdir(basedir):
                    if not isdir(join(basedir, d)):
                        continue
                    for f in listdir(join(basedir, d)):
                        p = join(basedir, d, f)
                        if f in expr_filenames:
                            print('expression')
                            print(p)
                            df = DataFrameAnalyzer.read_tsv(p, skiprows=2)
                            df['treatment'] = d
                            # print df.head()
                            expr_by_treatment[d] = df
                        elif f in comp_filenames:
                            print('comparison')
                            print(p)
                            df = DataFrameAnalyzer.read_tsv(p, skiprows=16, header=None)
                            columns = [r.strip() for r, i in zip(open(p), list(range(3)))][-1].split("\t")
                            columns[0] = columns[0].split(':')[-1]
                            print(columns)
                            df.columns = columns
                            df['treatment'] = d
                            # print df.head()
                            comp_by_treatment[d] = df
                        elif f in expr_refseq_filenames:
                            print('refseq')
                            print(p)
                            df = DataFrameAnalyzer.read_tsv(p, skiprows=2)
                            columns = [r.strip() for r, i in zip(open(p), list(range(3)))][-1].split("\t")
                            columns[0] = columns[0].split(':')[-1]
                            print(columns)
                            df.columns = columns
                            df['treatment'] = d
                            # print df.head()
                            expr_refseq_by_treatment[d] = df

                for t in comp_by_treatment:
                    print(t)
                    expr = expr_refseq_by_treatment[t]
                    expr_refseq = expr_by_treatment[t]
                    # print expr.head()
                    # print expr_refseq.head()
                    expr_refseq['sum.squares'] = expr_refseq[expr_refseq.columns[2:-1]].sum(axis=1) ** 2
                    # print len(set(expr_refseq['sum.squares'])), expr_refseq.shape
                    # print expr_refseq['sum.squares'].value_counts()
                    expr['sum.squares'] = expr_refseq[expr_refseq.columns[2:-1]].sum(axis=1) ** 2
                    counter = 0
                    descriptions = []
                    for ri, r in expr.iterrows():
                        if counter % 500 == 0:
                            print(counter, expr.shape)
                        q = r['sum.squares']
                        sel = expr_refseq[(expr_refseq['sum.squares'] - q).abs() < 0.0001]
                        # print q, sel.shape
                        if sel.shape[0] > 1:
                            # get the min between all the rows
                            diffs = []
                            for si, s in sel.iterrows():
                                a = [s[c] for c in sel.columns[2:-2]]
                                b = [r[c] for c in expr.columns[2:-2]]
                                diff = sum([(ai - bi) ** 2 for ai, bi in zip(a, b)])
                                diffs.append(diff)
                            sel['diff'] = diffs
                            sel = sel.sort_values('diff', ascending=True)
                            sel = sel.head(1)
                        assert sel.shape[0] == 1
                        counter += 1
                        descriptions.append(list(sel['Description'])[0])
                    expr['Description'] = descriptions
                    expr_refseq_by_treatment[t] = expr

                for t in expr_refseq_by_treatment:
                    print(t)
                    d = expr_refseq_by_treatment[t]
                    print(type(d), d.head())
                    d['desc1'] = [k.split("//")[0].strip() for k in d['Description']]
                    d['desc2'] = [k.split("//")[1].strip() for k in d['Description']]
                    d['desc3'] = [k.split("//")[2].strip() for k in d['Description']]
                    print(expr_refseq_by_treatment[t].head())
                    print(d[d['Description'] != 'NA // NA // NA'].shape)

                ensembl = set([name for d in expr_refseq_by_treatment for name in expr_refseq_by_treatment[d]['NAME']])
                ensembl_by_symbol = MyGeneAnalyzer.get_ensembl_by_symbol(ensembl, species='mouse')
                DataFrameAnalyzer.to_pickle([expr_refseq_by_treatment, comp_by_treatment], bkp_path)

            expr_refseq_by_treatment, comp_by_treatment = DataFrameAnalyzer.read_pickle(bkp_path)
            ensembl = set([name for d in expr_refseq_by_treatment for name in expr_refseq_by_treatment[d]['NAME']])
            names = list(ensembl)
            uniprot_by_name = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20180421_social_defeat_data/uniprot_by_name.tsv.gz"
            if not exists(uniprot_by_name):
                df = []
                for i, g in enumerate(names):
                    if i % 10 == 0:
                        print(i, len(names), i / float(len(names)))
                    r = u.search(g, frmt='tab', limit=3, columns="id, entry name, length, genes")
                    rows = [line.split("\t") for line in r.split("\n") if len(line) != 0]
                    if len(rows) == 0:
                        continue
                    next = pd.DataFrame(rows[1:], columns=rows[0])
                    # print next
                    next['name'] = g
                    df.append(next)
                df = pd.concat(df)
                df = df[df['Entry name'].str.contains('MOUSE')].reset_index(drop=True)
                print(df)
                DataFrameAnalyzer.to_tsv_gz(df, uniprot_by_name)

            df = DataFrameAnalyzer.read_tsv_gz(uniprot_by_name)
            ensembl_by_uniprot = MyGeneAnalyzer.get_ensembl_by_uniprot(set(df['Entry']), species='mouse')
            df['ensembl'] = [ensembl_by_uniprot[k] if k in ensembl_by_uniprot else None for k in df['Entry']]
            df.head()
            ensembl_by_name = DataFrameAnalyzer.get_dict(df, 'name', 'ensembl')
            ensembl_by_name = DataFrameAnalyzer.get_dict(df, 'name', 'ensembl')
            log2FC_invivo = []
            for t in comp_by_treatment:
                print(t)
                next = comp_by_treatment[t]
                print('# sig genes? (p-value < 0.05)')
                print((next['Feature P'] < 0.05).value_counts())
                # print '# sig genes? (Q-value < 0.1)'
                # print (df['Q Value'] < 0.1).value_counts()
                # print 'sorted by BH (ascending)'
                # print df.sort_values('FDR(BH)', ascending=True).head()
                # print 'sorted by Bonferroni (ascending)'
                # print df.sort_values('Bonferroni', ascending=True).head()
                next['group'] = t
                log2FC_invivo.append(next)
                print('\n')

            len(log2FC_invivo)
            log2FC_invivo = pd.concat(log2FC_invivo)

            log2FC_invivo.head()
            log2FC_invivo['ensembl'] = [ensembl_by_name[name] if name in ensembl_by_name else None for name in
                                        log2FC_invivo['Feature']]
            print(log2FC_invivo.head())
            missing = set(log2FC_invivo[pd.isnull(log2FC_invivo['ensembl'])]['Feature'])
            ensembl_by_symbol = MyGeneAnalyzer.get_ensembl_by_symbol(missing, species='mouse')
            log2FC_invivo['ensembl'] = [
                ensembl_by_symbol[k] if (ensembl is None and k in ensembl_by_symbol) else ensembl
                for k, ensembl in zip(log2FC_invivo['Feature'], log2FC_invivo['ensembl'])]
            DataFrameAnalyzer.to_tsv_gz(log2FC_invivo, uniprot_by_name_final)

        return DataFrameAnalyzer.read_tsv_gz(uniprot_by_name_final)

    @staticmethod
    def get_kmer_scores_by_module(treatment, time, module, overwrite=False):
        diffbind_peaks = E15Analyzer.get_summits_diffbind_ranges(treatment, time, overwrite=overwrite)

        k = treatment + '_' + time
        basedir = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/atac_loess_comparisons',
                       'extend_0_trimmed_pooledpeaks/bg_GENRE_permutations/kmer_scores')

        scores_path = join(basedir, module + ".tsv.gz")
        # print scores_path
        scores = DataFrameAnalyzer.read_tsv_gz(scores_path)
        # add the log2FC and padj values for the DiffBind comparison
        log2FC = E15Analyzer.get_summits_diffbind_ranges(treatment, time)

        scores = scores.merge(log2FC[['diffbind.range', 'summit.range', 'log2FC', 'padj', 'treatment']],
                              left_on='description', right_on='summit.range')

        assert scores[pd.isnull(scores['score'])].shape[0] == 0
        # print scores.shape
        # print log2FC.shape
        return scores

    @staticmethod
    def get_peak_annotations_homer(id=None):
        from lib.RFacade import RFacade
        atac = E15Analyzer.get_atac_values()
        basedir = '/home/rio/wp/noh_collaboration/data/atac_loess_comparisons'
        all = []
        for extend_dir in listdir(basedir):
            print(extend_dir)
            if not 'extend_0_trimmed_pooledpeaks' in extend_dir:
                continue
            atac_seq_dir = join(basedir, extend_dir)
            for treatment, grp in atac.groupby('treatment'):
                if id is not None and not treatment in id:
                    continue
                print(treatment, grp.shape)

                output_dir = "/home/rio/wp/noh_collaboration/data/figures/atac/annotation_peaks/" + treatment
                print(output_dir)
                if not exists(output_dir):
                    makedirs(output_dir)

                output_path_img = join(output_dir, treatment.replace(".tsv.gz", ''))
                # if exists(output_path_img + ".png"):
                #     continue

                # if not '1h' in f:
                #     continue
                gained = grp[(grp['log2FoldChange'] > 0) & (grp['padj'] < 0.1)][['CHR', 'START', 'END']]
                closed = grp[(grp['log2FoldChange'] < 0) & (grp['padj'] < 0.1)][['CHR', 'START', 'END']]
                unchanged = grp[grp['padj'] >= 0.1][['CHR', 'START', 'END']]
                print(gained.shape[0])
                print(gained.shape[0])
                print(unchanged.shape[0])
                for next, label in zip([gained, closed, unchanged], ['gained', 'closed', 'unchanged']):

                    output_path = join(atac_seq_dir, treatment + '.' + label + '.peakAnnotations.txt')

                    ann_stats_path = join(atac_seq_dir, treatment + "." + label + '.annStats.txt')
                    if not exists(ann_stats_path) or not exists(output_path):
                        input_bed = tempfile.mkstemp()[1]
                        DataFrameAnalyzer.to_tsv(next, input_bed, header=None)
                        cmd = ' '.join(['annotatePeaks.pl', input_bed, 'mm10', '-annStats',
                                        ann_stats_path, '>', output_path])

                        system(cmd)

                    # read annPath
                    res = DataFrameAnalyzer.read_tsv(output_path)
                    res.columns = ['PeakID'] + list(res.columns[1:])
                    res['treatment'] = treatment
                    res['label'] = label
                    all.append(res)
        all = pd.concat(all)
        return all


    @staticmethod
    def get_kmer_scores_by_module_simple(module):
        basedir = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/atac_loess_comparisons',
                       'extend_0_trimmed_pooledpeaks/bg_GENRE_permutations/kmer_scores')
        scores_path = join(basedir, module + ".tsv.gz")
        # print scores_path
        scores = DataFrameAnalyzer.read_tsv_gz(scores_path)
        return scores

    @staticmethod
    def get_kmer_scores_by_treatment(treatment, time):
        k = treatment + '_' + time
        basedir = '../../data/atac_loess_comparisons/extend_0_pooledpeaks/' + k + '_mm10-dfltBG'
        fg_fasta = join(basedir, k + '.fa')
        bg_bed = join(basedir, k + '_mm10-dfltBG.bed')
        bg_fasta = bg_bed.replace(".bed", '.fa')
        fg_headers, fg_seqs = list(zip(*FastaAnalyzer.get_fastas(fg_fasta)))
        bg_headers, bg_seqs = list(zip(*FastaAnalyzer.get_fastas(bg_fasta)))
        peak_assigments = pd.DataFrame()
        peak_assigments['foreground'] = fg_headers
        peak_assigments['background'] = bg_headers
        bg_by_foreground = DataFrameAnalyzer.get_dict(peak_assigments, 'foreground', 'background')
        scores_path = join(basedir, 'kmer_module_scores.tsv.gz')
        print('reading scores...')
        scores_df = DataFrameAnalyzer.read_tsv_gz(scores_path)

        return scores_df


    @staticmethod
    def get_summits_diffbind_ranges(treatment, time, overwrite=False):
        diffbind_tmp_path = '/tmp/diffbind_peaks_' + treatment + "_" + time + '.tsv.gz'
        if not exists(diffbind_tmp_path) or overwrite:
            atac_df = E15Analyzer.get_atac_values()
            ranges = atac_df[atac_df['treatment'] == (treatment + "_" + time)]

            res = E15Analyzer.get_summits_by_diffbind_ranges(ranges[['CHR', 'START', 'END', 'k']])
            log2FC_by_peak = DataFrameAnalyzer.get_dict(ranges, 'k', 'log2FoldChange')
            padj_by_peak = DataFrameAnalyzer.get_dict(ranges, 'k', 'padj')

            res['log2FC'] = res['diffbind.range'].map(log2FC_by_peak)
            res['padj'] = res['diffbind.range'].map(padj_by_peak)
            res['treatment'] = treatment + "_" + time
            DataFrameAnalyzer.to_tsv_gz(res, diffbind_tmp_path)

        res = DataFrameAnalyzer.read_tsv_gz(diffbind_tmp_path)
        return res

    @staticmethod
    def get_atac_loop_bonev_by_subtype(subtype):
        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/bonev_et_al_2017/1-s2.0-S0092867417311376-mmc3.xlsx'
        p_w_ensg = p.replace(".xlsx", "_%s.tsv" % subtype)
        if not exists(p_w_ensg):
            print(subtype)
            df = pd.read_excel(p, skiprows=2, sheet_name=subtype.upper() + "_based")
            ensg_by_symbol = MyGeneAnalyzer.get_ensembl_by_symbol(set(df['geneName']), species='mouse')
            # add annotations from ENSG
            genes_df = E15Analyzer.get_gene_coordinates()
            for ri, r in genes_df.iterrows():
                ensg_by_symbol[r['external_gene_name']] = r['ensembl_gene_id']
            df['ensg'] = [ensg_by_symbol[k] if k in ensg_by_symbol else np.nan for k in df['geneName']]
            DataFrameAnalyzer.to_tsv(df, p_w_ensg)

        return DataFrameAnalyzer.read_tsv(p_w_ensg)

    @staticmethod
    def get_atac_loop_bonev():
        p = '/g/scb/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/bonev_et_al_2017/contacts_coordinates_cn_based.tsv'
        p_w_ensg = p.replace(".tsv", "_ensg.tsv")
        if not exists(p_w_ensg):
            df = DataFrameAnalyzer.read_tsv(p)
            ensg_by_symbol = MyGeneAnalyzer.get_ensembl_by_symbol(set(df['geneName']), species='mouse')
            # add annotations from ENSG
            genes_df = E15Analyzer.get_gene_coordinates()
            for ri, r in genes_df.iterrows():
                ensg_by_symbol[r['external_gene_name']] = r['ensembl_gene_id']
            df['ensg'] = [ensg_by_symbol[k] if k in ensg_by_symbol else np.nan for k in df['geneName']]
            DataFrameAnalyzer.to_tsv(df, p_w_ensg)

        return DataFrameAnalyzer.read_tsv(p_w_ensg)

    @staticmethod
    def get_atac_seq_peaks_with_loop(extend=1e4, overwrite=False, subtype='cn', data_dir=None):
        if data_dir is None:
            data_dir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data'

        # final_bkp_path = join(data_dir, 'loops_n_atac_bonev_%i.tsv.gz' % (int(extend)))
        final_bkp_path = join(data_dir, 'loops_n_atac_bonev_%s.tsv.gz' % subtype)
        if overwrite or not exists(final_bkp_path):
            bkp_path = join(data_dir, "bonev_et_al_2017/contacts_coordinates_%s_based_n_atac.tsv" % subtype)

            # loops data
            loops_df = DataFrameAnalyzer.read_tsv(join(data_dir,
                                                       'bonev_et_al_2017/1-s2.0-S0092867417311376-mmc3_%s.tsv' % subtype))
            # extend
            loops_df.columns = ['chr', 'startcoordinate', 'endcoordinate'] + list(loops_df.columns[3:])

            if extend is not None:
                loops_df['startcoordinate'] = (loops_df['startcoordinate'] - extend).astype(int)
                loops_df['endcoordinate'] = (loops_df['endcoordinate'] + extend).astype(int)

            loops_df = SequenceMethods.parse_range2coordinate(loops_df,
                                                              ['chr', 'startcoordinate', 'endcoordinate'],
                                                              'k')
            # genes data
            genes_df = DataFrameAnalyzer.read_tsv(join(data_dir, "mm10_refseq_n_ensembl_n_pos.tsv"))

            atac_df = DataFrameAnalyzer.read_tsv(join(data_dir,
                                                      'atac_loess_comparisons/extend_0_trimmed_pooledpeaks/KCl_1h.tsv.gz'))
            atac_df['k'] = atac_df['CHR'] + ":" + atac_df['START'].astype(str) + "-" + atac_df['END'].astype(str)

            peaks_analyzer = PeaksAnalyzer()
            if True or not exists(bkp_path):
                # ATAC-seq peaks
                # get loops and atac seq intersection
                tmp1 = tempfile.mkstemp()[1]
                tmp2 = tempfile.mkstemp()[1]

                DataFrameAnalyzer.to_tsv(loops_df[['chr', 'startcoordinate', 'endcoordinate', 'k', 'geneName']], tmp1, header=None)
                DataFrameAnalyzer.to_tsv(atac_df[['CHR', 'START', 'END', 'k']], tmp2, header=None)

                print(peaks_analyzer.insersect_chip_seq_peaks(tmp1, tmp2, bkp_path , additional_args=['-wao']))
            res = DataFrameAnalyzer.read_tsv(bkp_path , header=None)
            res = res[res[8].str.contains(':')]
            del res[9]
            print(genes_df.head())

            print(res.shape)
            res.columns = ['chr', 'loop.start', 'loop.end', 'loop.id', 'SYMBOL',
                           'chr.atac', 'atac.start', 'atac.end', 'atac.id']
            res = res.merge(genes_df, on='SYMBOL')

            # group loop peaks by chromosome
            # print res['ENH-PROM.id']

            # now we want to extract all peaks that are between ENHANCERS and PROMOTERS
            print(res.head())
            tmp1 = tempfile.mkstemp()[1]
            tmp2 = tempfile.mkstemp()[1]
            print(res.head())
            res['tss'] = res['tss'].astype(int)

            res['start.enhprom'] = [r['loop.start'] if r['loop.start'] < r['tss'] else r['tss'] for ri, r in res.iterrows()]
            res['end.enhprom'] = [r['tss'] if r['loop.start'] < r['tss'] else r['loop.start'] for ri, r in res.iterrows()]
            res['enh-prom.id'] = res['chr_y'] +  ":" + res['start.enhprom'].astype(str) + "-" + res['end.enhprom'].astype(str)
            DataFrameAnalyzer.to_tsv(res[['chr_x', 'start.enhprom', 'end.enhprom', 'enh-prom.id']], tmp1, header=None)
            atac_n_loop_keys = set(res['atac.id'])
            atac_n_no_loop = atac_df[~atac_df['k'].isin(set(atac_n_loop_keys))]
            print(atac_n_no_loop.head())
            DataFrameAnalyzer.to_tsv(atac_n_no_loop[['CHR', 'START', 'END', 'k']], tmp2, header=None)
            print(peaks_analyzer.insersect_chip_seq_peaks(tmp1, tmp2, '/tmp/between_loops', additional_args=['-wao']))

            df_middle_peaks = DataFrameAnalyzer.read_tsv('/tmp/between_loops', header=None)

            del df_middle_peaks[8]
            print(df_middle_peaks.head())
            df_middle_peaks.columns = ['chr', 'start', 'end', 'id', 'atac.chr', 'atac.start', 'atac.end', 'atac.id']
            print(df_middle_peaks.head())
            print(res.head())

            print(len(set(res['enh-prom.id']).union('id')))

            # print res[res['enh-prom.id'] == 'chr12:85486089-85824545']
            # print df_middle_peaks[df_middle_peaks['id'] == 'chr12:85486089-85824545']

            table = []
            res = res.reset_index(drop=True)
            for ri, r in res.iterrows():
                if ri % 100 == 0:
                    print(ri, res.shape[0])
                    # print(ri, res.shape[0])
                sel = df_middle_peaks[df_middle_peaks['id'] == r['enh-prom.id']]
                # print r['enh-prom.id'], sel.shape[0]
                table.append(list(r.values) + [True])
                for di, d in sel.iterrows():
                    table.append(list(d.values)[:3] + [r['loop.id']] + [r['SYMBOL']] + list(d.values)[4:]
                                 + list(r.values)[9:] + [False])
                # for t in table:
                #     print list(t)


            res = pd.DataFrame(table, columns=list(res.columns) + ['is.contact'])

            DataFrameAnalyzer.to_tsv_gz(res, final_bkp_path)
        res = DataFrameAnalyzer.read_tsv_gz(final_bkp_path)
        return res

    @staticmethod
    def get_close_peaks_per_gene(distance_cutoff=1e6, overwrite=False, datadir=None):
        print('loading close peaks per gene')
        if datadir is None:
            datadir = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data"
        close_peaks_bkp = join(datadir, "close_peaks_by_gene_" + str(distance_cutoff) + ".pkl")

        print(exists(close_peaks_bkp), close_peaks_bkp)
        if not exists(close_peaks_bkp) or overwrite:
            coor = E15Analyzer.get_gene_coordinates(datadir=datadir)
            rna = E15Analyzer.get_expr_values(basedir=datadir)
            atac_coordinates_bkp = join(datadir, "atac_extend0_peaks_by_chr.pkl")
            if not exists(atac_coordinates_bkp):
                print('creating coordinates from atac file...')
                atac = E15Analyzer.get_atac_values()
                print(atac.head())
                atac_seq_peaks_by_chr = {}
                for chr in set(atac['CHR']):
                    grp = atac[atac['CHR'] == chr].reset_index(drop=True).drop_duplicates('START')[['CHR', 'START', 'END']]
                    atac_seq_peaks_by_chr[chr] = grp
                print(atac_seq_peaks_by_chr)
                DataFrameAnalyzer.to_pickle(atac_seq_peaks_by_chr, atac_coordinates_bkp)

            atac_seq_peaks_by_chr = DataFrameAnalyzer.read_pickle(atac_coordinates_bkp)

            coor_by_gene = {r['ensembl_gene_id']: [r['seqnames'], r['start'], r['end']]
                            for ri, r in coor.iterrows()}

            close_peaks_by_gene = {}
            for i, gene in enumerate(coor_by_gene):
                print(i, len(coor_by_gene))
                coor_gene = coor_by_gene[gene]
                start_gene, end_gene = coor_gene[1:]
                if not coor_gene[0] in atac_seq_peaks_by_chr:
                    continue
                chr_atac_peaks = atac_seq_peaks_by_chr[coor_gene[0]]
                # print coor_gene, len(chr_atac_peaks)
                # print type(chr_atac_peaks)
                selected = []
                for ri, r in chr_atac_peaks.iterrows():
                    start_peak, end_peak = r.values[1:]
                    d1, d2 = start_peak - start_gene, start_peak - end_gene
                    d3, d4 = end_peak - start_gene, end_peak - end_gene
                    # if sorted coordinates, stop when we are at the right section
                    if d1 > distance_cutoff and d2 > distance_cutoff:
                        break
                    min_dist = min(list(map(abs, [d1, d2, d3, d4])))
                    if min_dist < distance_cutoff:
                        selected.append(list(r.values) + [min_dist])
                        # print gene, selected[-1]
                close_peaks_by_gene[gene] = selected

            # print close_peaks_by_gene['ENSMUSG00000033730']
            DataFrameAnalyzer.to_pickle(close_peaks_by_gene, close_peaks_bkp)

        return DataFrameAnalyzer.read_pickle(close_peaks_bkp)

        # read the RNA coordinates and establish all the IDs that are located at less than a given
        # distance

    @staticmethod
    def get_invivo_rna_expr_values_remove_sample_id(remove_sample_id, overwrite=True):

        all_path = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20180310_RNA_invivo/20180418_contrast_results/all.tsv.gz'
        basedir = '/g/scb2/zaugg/rio/data/2018-03-06-HL2GCBGX5/deseq2'
        ref_marker = None
        if remove_sample_id is not None:
            ref_marker = "no_" + str(remove_sample_id)
            basedir = join(basedir, "all/"  + ref_marker)
            all_path = all_path.replace(".tsv.gz", "_remove_sample_" + str(remove_sample_id) + ".tsv.gz")

        if not exists(all_path) or overwrite:
            all = []
            for f in listdir(basedir):
                # print f
                if not '_vs_' in f or not f.endswith('.tsv.gz'):
                    continue
                p = join(basedir, f)
                print(p)
                c1, c2 = None, None

                if 'Comparison' in f and 'Interaction' in f:
                    c1, c2 = f.replace(".tsv.gz",
                                       '').replace('Comparison.',
                                                   '').split("_Interaction")[0].split("_vs_")
                    group = f.replace(".tsv.gz", '').split("_Interaction.")[1]
                    c1 = group + "." + c1
                    c2 = group + "." + c2
                    marker = ref_marker + ".interaction"
                else:
                    c1, c2 = f.replace(".tsv.gz", '').split("_vs_")
                    marker = ref_marker

                # print marker, c1, c2
                df = DataFrameAnalyzer.read_tsv(p)
                df['ensembl'] = df.index
                df = df.drop_duplicates('ensembl')
                df['deseq2.table'] = marker
                df = df.reset_index(drop=True)
                df['k'] = c1 + ".vs." + c2
                all.append(df)

            res = pd.concat(all).reset_index(drop=True)

            # remove duplicates
            res['k2'] = res['k'] + ":" + res['deseq2.table']
            all = []
            for k, grp in res.groupby('k2'):
                all.append(grp.drop_duplicates('ensembl'))
            res = pd.concat(all).reset_index(drop=True)
            del res['k2']

            # get gene name by ensembl

            symbol_by_ensembl_bkp = "/tmp/symbol_by_ensembl_mouse.pkl"
            if not exists(symbol_by_ensembl_bkp):
                symbol_by_ensembl = MyGeneAnalyzer.get_symbol_by_ensembl(set(res['ensembl']), species='mouse')
                DataFrameAnalyzer.to_pickle(symbol_by_ensembl, symbol_by_ensembl_bkp)
            print('adding gene names...')
            symbol_by_ensembl = DataFrameAnalyzer.read_pickle(symbol_by_ensembl_bkp)
            res['name'] = [symbol_by_ensembl[k] if k in symbol_by_ensembl else k for k in res['ensembl']]
            return res

        res = DataFrameAnalyzer.read_tsv_gz(all_path)
        return res


    @staticmethod
    def get_invivo_rna_expr_values(tophat=True, overwrite=False):

        all_path = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20180310_RNA_invivo/20180418_contrast_results/all.tsv.gz'
        if tophat:
            all_path = all_path.replace(".tsv.gz", "_tophat.tsv.gz")

        if tophat:
            if not exists(all_path) or overwrite:
                all = []
                basedir = '/g/scb2/zaugg/rio/data/2018-03-06-HL2GCBGX5/deseq2'
                for d in listdir(basedir):
                    print(join(basedir, d))
                    for f in listdir(join(basedir, d)):
                        print(f)
                        if not '_vs_' in f or not f.endswith('.tsv'):
                            continue
                        p = join(basedir, d, f)
                        print(p)
                        marker = d

                        c1, c2 = None, None

                        if 'Comparison' in f and 'Interaction' in f:
                            c1, c2 = f.replace(".tsv",
                                               '').replace('Comparison.',
                                                           '').split("_Interaction")[0].split("_vs_")
                            group = f.replace(".tsv", '').split("_Interaction.")[1]
                            c1 = group + "." + c1
                            c2 = group + "." + c2
                            marker = marker + ".interaction"
                        else:
                            c1, c2 = f.replace(".tsv", '').split("_vs_")
                        print(marker, c1, c2)
                        df = DataFrameAnalyzer.read_tsv(p)
                        df['ensembl'] = df.index
                        df['deseq2.table'] = d
                        df = df.reset_index(drop=True)
                        df['k'] = marker + ":" + c1 + ".vs." + c2
                        all.append(df)
                res = pd.concat(all).reset_index(drop=True)
                # get gene name by ensembl
                symbol_by_ensembl = MyGeneAnalyzer.get_symbol_by_ensembl(set(res['ensembl']), species='mouse')
                res['name'] = [symbol_by_ensembl[k] if k in symbol_by_ensembl else k for k in res['ensembl']]
                DataFrameAnalyzer.to_tsv_gz(res, all_path)
        else:
            if not exists(all_path) or overwrite:
                basedir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/20180310_RNA_invivo'
                markers = ['NeuN', 'Sst']
                subgroups = ['Def-Def', 'Def-Sen', 'Sen-Sen']

                res = []
                sel_genes = set()
                order = []
                hits_per_cutoff = []
                for marker in markers:
                    for i, c1 in enumerate(subgroups):
                        for c2 in subgroups[i:]:
                            if c1 == c2:
                                continue
                            p = join(basedir, c1 + "_" + marker + "_vs_" + c2 + "_" + marker + ".tsv")
                            print(p)
                            df = DataFrameAnalyzer.read_tsv(p)
                            df['ensembl'] = df.index
                            df = df.reset_index(drop=True)

                            print(df.head())
                            df['marker'] = marker
                            k = marker + ":" + c1 + ".vs." + c2
                            df['id'] = k
                            order.append(k)
                            for thr in [0.01, 0.05, 0.1, 0.15, 0.2]:
                                sel = df[df['padj'] < thr]
                                if thr == 0.1:
                                    res.append(df)
                                    # print df[df['name'] == 'ENSMUSG00000001379']
                                    for g in df[df['padj'] < thr]['ensembl']:
                                        sel_genes.add(g)

                                hits_per_cutoff.append([thr, c1 + "_vs_" + c2, marker, sel.shape[0]])
                                # print c1, c2, thr, df[df['padj'] < thr].shape[0], set(sel['name'])

                res = pd.concat(res).reset_index(drop=True)
                # get gene name by ensembl
                symbol_by_ensembl = MyGeneAnalyzer.get_symbol_by_ensembl(set(res['ensembl']), species='mouse')
                res['name'] = [symbol_by_ensembl[k] if k in symbol_by_ensembl else k for k in res['ensembl']]
                DataFrameAnalyzer.to_tsv_gz(res, all_path)

        res = DataFrameAnalyzer.read_tsv_gz(all_path)
        return res


    @staticmethod
    def get_mitochondrial_genes_ensembl():
        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/mouse_compartment_knowledge_full.tsv'
        compartments = DataFrameAnalyzer.read_tsv(p, header=None)
        print(compartments.head())
        compartments.columns = ['protein', 'gene', 'GO.ID', 'compartment', 'Source', 'Evidence', 'Confidence']
        print(compartments.shape)
        compartments = compartments[compartments['Confidence'] == 5]
        print(compartments.shape)

        mt_genes = set(compartments[compartments['compartment'].str.lower().str.contains('mitochon')]['protein'])
        # print mt_genes
        print(len(mt_genes))
        ensembl_mt = MyGeneAnalyzer.get_ensemblgene_by_ensemblprotein(mt_genes, species='mouse')
        ensembl_mt = set(ensembl_mt.values()).union(set([mt for mt in mt_genes if 'ENSMUSG' in mt]))
        return ensembl_mt


    @staticmethod
    def get_cisbp_scores_by_motif_id(treatment_query, time_query, motif_id):
        # this is generated by a previous script
        output_dir = "../../../aurocs_cisbp_by_motif"
        if not exists(output_dir):
            mkdir(output_dir)
        # output_path = "../../../aurocs_by_module_and_treatments.tsv.gz"
        # if False and exists(output_path):
        #     return DataFrameAnalyzer.read_multiple_tsv_gz(output_path)

        from lib.HumanTFs import HumanTFs
        all_dataframes = []
        human_tfs = HumanTFs.get_tf_motifs_cisbp()
        human_tfs = human_tfs[human_tfs['Motif evidence'] == 'Direct'].drop_duplicates('CIS-BP ID').reset_index(drop=True)
        # human_tfs = human_tfs[(human_tfs['HGNC symbol'] == 'CTCF') & (human_tfs['Motif source'] == 'JASPAR')].reset_index(drop=True)

        final = []
        for next_i, r in human_tfs.iterrows():
            fdr_thr = 0.1
            # print next_i, 'out of', human_tfs.shape
            # prepare CIS-BP input
            if pd.isnull(r['CIS-BP ID']):
                continue
            else:
                if not isinstance(r['CIS-BP ID'], str):
                    continue
            if not motif_id in r['CIS-BP ID']:
                continue
            print(motif_id, r['CIS-BP ID'])
            basedir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/noh_collaboration/data/atac_loess_comparisons/extend_0_trimmed_pooledpeaks/bg_GENRE_permutations'
            scores_path = join(basedir, 'cisbp_scores', motif_id + ".tsv.gz")
            print(exists(scores_path), scores_path)
            if not exists(scores_path):
                continue
            for query in [[treatment_query, time_query]]:
                print(query)
                treatment, time = query
                k = treatment + "_" + time
                print('next treatment')
                print(k)
                diffbind_tmp_path = '/tmp/diffbind_peaks_' + k + '.tsv.gz'
                atac_df = E15Analyzer.get_atac_values()
                print(atac_df.head())
                next_ranges = atac_df[atac_df['treatment'] == treatment + "_" + time]
                print(next_ranges.shape)
                if not exists(diffbind_tmp_path):
                    res = E15Analyzer.get_summits_by_diffbind_ranges(next_ranges[['CHR', 'START', 'END', 'k']],
                                                                     treatment=treatment, time=time)
                    print(res.shape)
                    DataFrameAnalyzer.to_tsv_gz(res, diffbind_tmp_path)
                res = DataFrameAnalyzer.read_tsv_gz(diffbind_tmp_path)

                log2FC_by_k = DataFrameAnalyzer.get_dict(next_ranges, 'k', 'log2FoldChange')
                padj_by_k = DataFrameAnalyzer.get_dict(next_ranges, 'k', 'padj')

                res['log2FC'] = [log2FC_by_k[c] for c in res['diffbind.range']]
                res['padj'] = [padj_by_k[c] for c in res['diffbind.range']]
                # print res.head()
                print(res[res['padj'] < fdr_thr].shape[0])

                print('# of peaks', len(set(res['summit.range'])))

                print(res.head())
                gained = res[(res['padj'] < 0.1) & (res['log2FC'] > 0)]
                closed = res[(res['padj'] < 0.1) & (res['log2FC'] < 0)]
                print(gained.shape[0])
                print(closed.shape[0])


                for next_df, peak_group in zip([gained, closed], ['gained', 'closed']):
                    print(treatment, time, motif_id, peak_group, next_df.shape)
                    # print module, peak_group
                    scores = DataFrameAnalyzer.read_tsv_gz(scores_path)
                    scores['position'] = scores['start']
                    score_by_description = DataFrameAnalyzer.get_dict(scores, 'sequence name', 'score')
                    position_by_description = DataFrameAnalyzer.get_dict(scores, 'sequence name', 'position')
                    strand_by_description = DataFrameAnalyzer.get_dict(scores, 'sequence name', 'strand')

                    # print 'assigning fg scores...'
                    next_df['fg.score'] = next_df['summit.range'].map(score_by_description)
                    next_df['position'] = next_df['summit.range'].map(position_by_description)
                    next_df['strand'] = next_df['summit.range'].map(strand_by_description)

                    n_with_motif = next_df[next_df['fg.score'] > 0].shape[0]
                    n_group = next_df.shape[0]
                    print(n_with_motif, n_group)

                    # all scores were mapped successfully
                    # print next_df[pd.isnull(next_df['fg.score'])].shape[0]
                    print(next_df[pd.isnull(next_df['fg.score'])].shape[0], next_df.shape[0])

                    # bg_by_fg = DataFrameAnalyzer.get_dict(bg_ids[bg_ids['permutation'] == permutation_id], 'fg', 'bg')
                    bg_ids_by_permutation_dir = join(basedir, k + "_perm0")
                    bg_ids = DataFrameAnalyzer.read_multiple_tsv_gz(bg_ids_by_permutation_dir,
                                                                    query='background_ids_by_permutation')
                    # print bg_ids.head()
                    # print bg_ids.shape
                    # print bg_ids['subset'].value_counts()
                    bg_ids = bg_ids.drop_duplicates('fg').reset_index()
                    bg_by_fg = DataFrameAnalyzer.get_dict(bg_ids, 'fg', 'bg')

                    next_df['summit.bg'] = next_df['summit.range'].map(bg_by_fg)
                    # print 'assigning bg scores...'
                    next_df['bg.score'] = next_df['summit.bg'].map(score_by_description)
                    # ['bg.score'])].shape[0]

                    print(next_df[pd.isnull(next_df['bg.score'])].shape[0], next_df.shape[0])

                    next_df['fg.score'] = np.where(np.isnan(next_df['fg.score']), -1, next_df['fg.score'])
                    final.append(next_df)

        final = pd.concat(final).reset_index(drop=True)
        return final

