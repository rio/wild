'''
Created on 5/3/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from multiprocessing import Process
from threading import Thread

class Threading:
    @staticmethod
    def run(function, input_list, n_threads, output_list=None):
        print 'run function'
        print function
        print 'with input list of len'
        print len(input_list)
        print 'in groups of %d threads' % n_threads

        n_groups = len(input_list) / n_threads + 1
        for group_i in range(n_groups):
            print 'jobs group', group_i
            start, end = group_i * n_threads, (group_i + 1) * n_threads
            # print 'start', start, 'end', end

            threads = [None] * (end - start)
            for i, pi in enumerate(range(start, min(end, len(input_list)))):
                next_args = input_list[pi]
                # print next_kmer
                if output_list is not None:
                    threads[i] = Process(target=function, args=[next_args, output_list, pi])
                else:
                    threads[i] = Process(target=function, args=next_args)
                threads[i].start()

            # print  threads
            print 'joining threads...'
            # do some other stuff
            for i in range(len(threads)):
                if threads[i] is None:
                    continue
                threads[i].join()
