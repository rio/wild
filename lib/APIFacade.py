from . import get_api_result
from .main_script import get_molecule_infomation_from_pdbe_api
from . import variables


class PDBAPIFacade:

    def print_pdb_specific_info(self, pdbid, query='title'):
        # given a particular id print the respective entry that wants to be
        # visualized by the user
        molecule_info = get_molecule_infomation_from_pdbe_api(pdbid)
        print(molecule_info[query])

    def get_pdb_specific_info(self, pdbid, query='title'):
        # given a particular id return the respective entry that wants to be
        # visualized by the user
        molecule_info = get_molecule_infomation_from_pdbe_api(pdbid)
        return molecule_info[query]

    def validate_experimental_method(self, pdbe_dictionary, method='X-ray'):
        pdb_data = pdbe_dictionary[list(pdbe_dictionary.keys())[0]][0]
        return method in pdb_data['experimental_method'][0]

    def get_pfamids(self, pdbid):
        url = pfam_url + pdbid
        print(url)
        molecule_info = get_api_result.return_api_info(url=url)
        # print molecule_info
        print('here', type(molecule_info))
        if isinstance(molecule_info, dict):
            return list(molecule_info[pdbid]['Pfam'].keys())
        return None

class UniProtFacade():

    def get_protein_info(self, gene_name):
        url = variables.api_base_uniprot + "gene=" + gene_name
        print(url)
        molecule_info = get_api_result.return_api_info(url=url)
        # iterate through the molecules and find entity 1
        return molecule_info

    def get_tf_category_from_description(self, description):
        '''
        Given a description for a protein, recover the best TF name that can be associated to it
        :param description:
        :return:
        '''

        keywords_by_tf_name = {
            'Homeodomain': {'Homeobox'},
            'MADS': {'MADS'},
            'Leucine Zipper': {'BZIP', 'leucine zipper'},
            'ETS': {'ETS'},
            'Forkhead': {'Forkhead'},
            'GATA': {'GATA'},
            'Hormone-nuclear receptor': {'Nuclear receptor'},
            'E2F': {'E2F'},
            'Helix-Loop-Helix': {'BHLH'},
            'HTH': {'HTH'},
            'RFX': {'RFX'},
            'Paired': {'Paired'},
            'GCM': {'GCM'},
            'HMG': {'HMG'},
            'TBX': {'TBX'},
            'MYB': {'Myb'},
            'T-box': {'box_'},
            'EGR': {'ERG'},
            'AP2': {'TF_AP'},
            'TEA': {'TEA DNA-binding'},
        }

        matched_tf_names = set()
        for tf_name in keywords_by_tf_name:
            for keyword in keywords_by_tf_name[tf_name]:
                if keyword in description:
                    matched_tf_names.add(tf_name)
        assert len(matched_tf_names) <= 1

        # return the only match or None
        return list(matched_tf_names)[0] if len(matched_tf_names) == 1 else None

dictionary_like_pdbe_api = {'1cbs': [
    {
        "related_structures": [],
        "split_entry": [],
        "title": "CRYSTAL STRUCTURE OF CELLULAR RETINOIC-ACID-BINDING PROTEINS I AND II IN COMPLEX WITH ALL-TRANS-RETINOIC ACID AND A SYNTHETIC RETINOID",
        "release_date": "19950126",
        "experimental_method": [
            "X-ray diffraction"
        ],
    }
]
}

def main():
    import sys

    api = UniProtFacade()
    info = api.get_protein_info("EVX1")
    for t in info:
        print(t)

    sys.exit()
    api = PDBAPIFacade()
    # assuming that I have a list of PDBs, I can iterate through them and ask for all possible
    # for the PFAM IDs
    for pdbid in ["1ee4", '1bl0', '3dcx']:
        print(pdbid, api.get_pfam_ids(pdbid))
    return
    print(api.validate_experimental_method(dictionary_like_pdbe_api, 'X-ray'))

if '__main__' in __name__:
    main()