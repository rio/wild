'''
Created on Nov 7, 2016

Method to manipulate weights given by DeepLIFT

@author: ignacio
'''
from numpy import correlate, array, concatenate
import matplotlib.pyplot as plt
from copy import deepcopy
import numpy as np
import gzip
import pandas as pd
from .motif_plotter import *
from Bio import motifs
from lib.SNPAnalyzer import SNPAnalyzer
from keras.models import model_from_json
from keras.utils import np_utils
from os.path import exists
import os

p = "/home/ahlmanne/src/deeplift"

if not exists(p):
    os.environ['DEEPLIFT_DIR'] = "/home/ahlmanne/src/deeplift"
    if not exists("/home"):
        os.environ["DEEPLIFT_DIR"] = "C:\\Users\ignacio\Software\deeplift"
# import deeplift
# from deeplift.conversion import keras_conversion as kc

class DeepLIFTAnalyzer:
    
    
    def get_weights_from_matrix(self, matrix):
        """
        Scores are given by a nx4 matrix, which contains one value per nt.
        Filter out the three values that are not necessary in each row (zeroes) 
        """
        values = [] # output row
        for r in matrix:
            next_value = sorted(list(r), key=lambda x: -abs(x))[0]
            values.append(next_value)
        return values
    
    def get_best_cross_correlation(self, a, b, show=False, use_sequence=False):
        """
        Get cross-correlations for all possible alignments between two sequences
        return start position at a and b where alignment is maximized
        """
        
        if not isinstance(a, list):
            if use_sequence:
                ref = {"A": 1, "C": 2, "G": 3, "T": 4}
                a = [ref[nt] for nt in a.Sequence]
                b = [ref[nt] for nt in b.Sequence]
            else:
                a = self.get_weights_from_matrix(a.Scores)
                b = self.get_weights_from_matrix(b.Scores)
        

        # get all correlation scores to map best aligned position
        correlations = correlate(a, b, mode="full")
        x = [xi for xi in range(len(correlations))]

        # plot cross-correlations
        if show:
            plt.plot(x, correlations)
            plt.show()
        
        # return position with the best score
        best_position, best_score = sorted([[xi, ci] for xi, ci in zip(x, correlations)],
                               key=lambda t: -t[1])[0]
        return len(b) - (best_position + 1), best_score
    
    def shift_scored_region(self, v, dx):
        """
        Move values and scores for scored sequence dx positions, given as an integer
        (>0 right, <0 left)
        """
        
        q = deepcopy(v)
        if dx > 0:
            q.Scores = concatenate([q.Scores[dx:], [[0, 0, 0, 0] for i in range(dx)]], axis=0)
            q.Sequence = q.Sequence[dx:] + "".join(["N" for i in range(dx)])
        if dx < 0:
            q.Scores = concatenate([[[0, 0, 0, 0] for i in range(-dx)], q.Scores[:dx]], axis=0)
            q.Sequence = "".join(["N" for i in range(-dx)]) + q.Sequence[:dx]
        return q
    
    def get_cross_correlations(self, values, threshold=20):
        table = []
        
        n_total = (len(values) * (len(values) + 1)) / 2
        step = n_total / 100
        print("# estimated comparisons to do:", n_total)
        counter = 0 
        
        values = [self.get_weights_from_matrix(v.Scores) for v in values]
        for i, a in enumerate(values):
            for j, b in enumerate(values):
                if counter % step == 0:
                    print(counter, "out of", n_total, "...")
                if i >= j:
                    continue
                
                best_position, best_score = self.get_best_cross_correlation(values[i],
                                                                            values[j])
                if threshold is None or best_score >= threshold:
                    table.append([i, j, best_position, best_score])
                counter += 1                    
        return table
    
    def read_cnn_entries(self, table_path, stop_at=None):
        location = []
        sequence = []
        scores = []
        
        counter = 0
        with gzip.open(table_path, "rb") as file:
            for line in file:
                elem = line.decode("UTF-8").strip().split("\t")
                location.append(elem[0])
                sequence.append(elem[1])
                scores.append(np.array(elem[2:]).reshape((int((len(elem)-2)/4),4)).astype(np.float))
                counter += 1
                if stop_at is not None and counter >= stop_at:
                    break
        
        data_frame = pd.DataFrame({"Location": location, "Sequence": sequence, "Scores": scores})
        values = [deepcopy(data_frame.iloc[i]) for i in range(data_frame.shape[0])]
        return values
    
    def plot_aligned_tracks(self, a, b, show_cc=False, show_alignment=False,
                            plot_as_shape=False, align=True):
        """
        Plot two tracks only
        """
        print("here...")
        dx, score = self.get_best_cross_correlation(a, b, show=show_cc)
        print("here 2 ...")
        
        print(dx, score)
        fig = plt.figure()
        ax = fig.add_subplot(2, 1, 1)        

        # define the function
        plot_motif = ConsensusMotifPlotter.from_importance_scoring
        
        cbp = self.plot_shape_profile(a) if plot_as_shape else plot_motif(a)
        
        if not plot_as_shape: 
            cbp.plot(ax)
        # print i, j
        q = self.shift_scored_region(b, dx)
        ax = fig.add_subplot(2, 1, 2)
        
        if not align:
            q = b

        cbp = self.plot_shape_profile(q) if plot_as_shape else plot_motif(q) 
        
        if not plot_as_shape:
            cbp.plot(ax)
        
        if show_alignment:
            plt.show()

    def plot_shape_profile(self, a, normalize=False, add_legend=True):
        
        shape_keys = "MGW", "ProT", "Roll", "HelT"
        ylabels = "MWG [angstrom]", "ProT [degrees]", "Roll [degrees]", "HelT [degrees]"
        colors = "#F59547", "#99BA5B", "#A1ACE6", "#7F64A1"
        
        for i in range(4):
            v = [r[i] for r in (a.Shapes if hasattr(a, "Shapes") else a)]
            if normalize:
                vmin, vmax = min(v), max(v)
                v = [(vi - vmin) / (vmax - vmin) for vi in v]
            x = [xi + 0.5 for xi in range(len(v))]
            plt.plot(x, v, color=colors[i], lw=2)
        plt.xlim(0, len(v))
        plt.xticks([i + 0.5 for i in range(len(v))],
                   [i + 1 for i in range(len(v))])
        if add_legend:
            plt.legend(ylabels, fontsize=8)
        
        
    def get_substr(self, v, start, length):
        """
        Return a substr of the current entry
        """
        q = deepcopy(v)
        q.Sequence = q.Sequence[start:start + length]
        q.Scores = q.Scores[start:start + length]
        if hasattr(q, "Shapes"):
            q.Shapes = q.Shapes[start:start + length]
        return q
        
    def plot_sequence(self, entry):
        """
        Plot several tracks:
        """
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        cbp = ConsensusMotifPlotter.from_importance_scoring(entry)
        cbp.plot(ax)
        
        if hasattr(entry, "Shape"):
            self.plot_shape_profile(entry)
        ax.set_title("Reference")
        
    def get_complementary_entry(self, v):
        """
        Reverse the sequence and get the complementary region for an assessed entry  
        """
        q = deepcopy(v)
        
        q.Sequence = self.get_complementary_seq(q.Sequence)
        q.Scores = [[vi[i] for i in (2, 3, 0, 1)] for vi in q.Scores[::-1]]
        
        # DNA shape sometimes is available
        if hasattr(q, 'Shapes'):
            q.Shapes = q.Shapes[::-1]
        
        return q
        
    def get_complementary_nt(self, nt):
        if nt == "A":
            return "T"
        if nt == "T":
            return "A"
        if nt == "C":
            return "G"
        if nt == "G":
            return "C"
        if nt == "N":
            return "N"
                
    def get_complementary_seq(self, s):
        return "".join([self.get_complementary_nt(nt) for nt in s][::-1])
    
    def plot_multiple_tracks(self, tracks, show=False,
                             plot_as_shape=True, labels=None, normalize=False):
        """
        Plot several tracks
        """
        fig = plt.figure(figsize=(10, 15))
        ax = fig.add_subplot(len(tracks), 1, 1)            
        
        for i, next in enumerate(tracks):
            # print i, j
            ax = fig.add_subplot(len(tracks), 1, i + 1)

            if not plot_as_shape:
                cbp = ConsensusMotifPlotter.from_importance_scoring(next)
                cbp.plot(ax)
                plt.ylim([0, max([max(vi) for vi in next.Scores])])
            else:
                self.plot_shape_profile(next, normalize=normalize, add_legend=False)
                
            if labels != None:
                plt.text(0.99, 0.8, labels[i], horizontalalignment='right',
                         verticalalignment='center', transform = ax.transAxes,
                         fontsize=10)
                
            if i == 5:
                plt.ylabel("Relative contribution to CNN score", fontsize=15)
            if i + 1 == len(tracks):
                plt.xlabel("Motif position", fontsize=15)
                
        if show:
            plt.show()

    def plot_aligned_tracks_one_vs_many(self, ref, others, show=False,
                                        align=True, plot_as_shape=True):
        """
        Plot several tracks:
        """
        fig = plt.figure()
        ax = fig.add_subplot(len(others) + 1, 1, 1)
        
        if not plot_as_shape:
            cbp = ConsensusMotifPlotter.from_importance_scoring(ref)
            cbp.plot(ax)
        else:
            self.plot_shape_profile(ref)
        ax.set_title("Reference")
        
            
        
        for i, next in enumerate(others):
            dx, score = self.get_best_cross_correlation(ref, next)
            # print i, j
            q = self.shift_scored_region(next, dx)
            ax = fig.add_subplot(len(others) + 1, 1, i + 2)
            ax.set_title("aligned " + str(i))

            if not plot_as_shape:
                cbp = ConsensusMotifPlotter.from_importance_scoring(q if align else next)
                cbp.plot(ax)
            else:
                self.plot_shape_profile(q if align else next)
                
        if show:
            plt.show()
            
    def plot_aligned_tracks_by_pairs(self, values1, values2, show=False):
        """
        Plot several tracks:
        """
        assert len(values1) == len(values2)
        fig = plt.figure()
        counter = 0
        for a, b in zip(values1, values2):
            ax = fig.add_subplot(len(values1) * 2, 1, counter + 1)
            cbp = ConsensusMotifPlotter.from_importance_scoring(a)
            cbp.plot(ax)
            
            dx, score = self.get_best_cross_correlation(a, b)
            # print i, j
            q = self.shift_scored_region(b, dx)
            ax = fig.add_subplot(len(values1) * 2, 1, counter + 2)
            cbp = ConsensusMotifPlotter.from_importance_scoring(q)
            counter += 1
            
    def score_sequences_with_deep_learning_network(self, sequences_path, model_basename_path,
                                                   output_path, seq_length=None):
        snp_analyzer = SNPAnalyzer()
        fastas = snp_analyzer.get_fastas_from_file(sequences_path,
                                                   uppercase=True, na_remove=True)
        df = pd.DataFrame({'location':[e[0] for e in fastas], 
                           'sequence':[e[1] for e in fastas]})
        
        def parse_alpha_to_seq(sequence):
            output = np.arange(len(sequence))
            for i in range(0, len(sequence)):
                snippet = sequence[i]
                if snippet == 'A':
                    output[i] = 0
                elif snippet == 'C':
                    output[i] = 1
                elif snippet == 'T':
                    output[i] = 2
                elif snippet == 'G':
                    output[i] = 3
                else:
                    raise AssertionError("Cannot handle snippet: " + snippet)
            return output
        
        N = len(df)
        
        if seq_length is None:
            seq_length = len(df["sequence"][0])

        X = np.zeros((N, seq_length, 4))
        sequence = df.sequence
        for idx in range(0, N):
            X[idx] = np_utils.to_categorical(parse_alpha_to_seq(sequence[idx]), 4)
        
        X_train = X.astype('float32')
        print((X_train.shape, 'train samples'))
        
        #===============================================================================
        # # Load the model
        #===============================================================================
        print("loading models...")
        model = model_from_json(open(model_basename_path + '.json').read())
        model.load_weights(model_basename_path +'.h5')
        
        deeplift_model = kc.convert_sequential_model(model,
                                                     mxts_mode=deeplift.blobs.MxtsMode.DeepLIFT)
        find_scores_layer_idx = 0
        
        
        print("scoring with the model...")
        deeplift_contribs_func = deeplift_model.get_target_contribs_func(
                                    find_scores_layer_idx=find_scores_layer_idx,
                                    target_layer_idx=-2)
        
        print(X_train.shape[0])
        print(X_train.shape[1])
        print(X_train.shape[2])
        print(deeplift_contribs_func)
        print("done...")
        scores = np.array(deeplift_contribs_func(task_idx=0,input_data_list=[X_train.reshape((X_train.shape[0], 1, X_train.shape[1], X_train.shape[2]))],
            batch_size=10, progress_update=None))
        score_col = [scores[i,:,:] for i in range(0,scores.shape[0])]
        
        with gzip.open(output_path, "wb") as out:
            for loc, seq, p in zip(df.location, df.sequence, score_col):
                row = []
                r = []
                for pi in p.reshape(seq_length, 4):
                    r += list(pi)
                row += r
                out.write(str.encode(loc + "\t" + seq + "\t" +  ("\t".join(['{0:.3g}'.format(y) for y in row])) + "\n"))
            
    def plot_aggregated_tracks_shape(self, entries):
        """
        
        """
        final_scores = [[] for i in range(len(entries[0].Shapes))]
        for j in range(len(final_scores)): # j refers to columns, i to rows    
            for i in range(4):
                weights_ij = [e.Shapes[j][i] for e in entries]
                scores_nucleotide = sum(weights_ij) / len(entries)
                final_scores[j].append(scores_nucleotide)
                
        print(final_scores)
        self.plot_shape_profile(final_scores)
        plt.xlabel("Position")
        plt.ylabel("DeepLIFT weight (norm. average)")
        
    def plot_aggregated_track(self, values, show=False,
                              rm_negative_weights=True, align=False):
        
        sequences = []
        scores = []
        sequences.append(values[0].Sequence)
        scores.append(values[0].Scores)
        
        #=======================================================================
        # Align with respect to reference case
        #=======================================================================
        for i, next in enumerate(values[1:]):
            dx, score = self.get_best_cross_correlation(values[0], next)
            # print i, j
            q = self.shift_scored_region(next, dx)
            sequences.append(q.Sequence if align else next.Sequence)
            scores.append(q.Scores if align else next.Scores)
        
        final_scores = [[] for i in range(len(sequences[0]))]
        for j in range(len(final_scores)): # j refers to columns, i to rows    
            for i in range(4):
                weights_ij = [s[j][i] for s in scores]
                scores_nucleotide = sum(weights_ij) / len(scores)
                final_scores[j].append(scores_nucleotide)
                
                if i == 1:
                    print(scores[0])
                    print(scores[1])
                    print(scores[2])
                    print(weights_ij)
                    # import sys; sys.exit()

        #===============================================================================
        # Write tmp transfac file
        #===============================================================================
        tmp_transfac_path = "../tmp/transfac_file.txt"
        writer = open(tmp_transfac_path, "w")
        writer.write("ID any_old_name_for_motif_1\nBF species_name_for_motif_1\nP0      A      C      G      T\n")
        for i, s in enumerate(final_scores):
            # pos0 = A
            # pos1 = C
            # pos2 = G
            # pos3 = T
            entry = [s[0], s[1], s[3], s[2]]
            if rm_negative_weights:
                entry = [si for si in entry] # [max(0, si) for si in entry]
            line = str(i + 1).zfill(2) + "     " + "     ".join(map(str, entry)) + "     N\n"
            # print line
            writer.write(line)
        writer.write("XX\n//")
        writer.close()
        
        with open(tmp_transfac_path) as handle:
            m = motifs.parse(handle, "transfac")
        
        fig = plt.figure(figsize=(10, 5))
        ax = fig.add_subplot(111)
        cbp = ConsensusMotifPlotter.from_bio_motif(m[0], scale_info_content=False)
        plt.xticks([0.5 + i for i in range(len(m[0]))], [str(i + 1) for i in range(len(m[0]))])
        cbp.plot(ax)
        plt.xlabel("Position")
        plt.ylabel("DeepLIFT weights [average]")
        
        if show:
            plt.show()   
        
    def get_best_cross_correlations(self, values, i, cc_threshold=0.2):
        """
        Get all elements that have a best cross- correlation value to our reference
        higher than 0.2
        """
        
        ref = values[i]
        table = [[v, self.get_best_cross_correlation(ref, v)[1]] for v in values]
        table = [v for v in table if v[1] > cc_threshold]
        return [t[0] for t in sorted(table, key=lambda x: -x[1])]
            