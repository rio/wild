'''
Created on 1/12/2018, 2018

@author: Ignacio Ibarra Del Rio

# DESeq2 like methods
Description:
'''


from lib.utils import *

class DESeq2Analyzer():

    @staticmethod
    def get_rscript_cluster_path():
        return '/g/easybuild/x86_64/CentOS/7/haswell/software/R/3.6.2-foss-2019b/bin/Rscript'
    @staticmethod
    def run(df, contrasts_list, output_dir, sample_data_path=None, se_path=None, outcounts=None, ref=None, script_path=None, pca_out=None,
            reads_fraction=-1):
        # prepare output dir and move dataframe to that position

        assert se_path is not None and ref is not None
        print('reference', ref)

        # the column treatment has to exist to conduct a comparison
        if not exists(output_dir):
            mkdir(output_dir)

        if sample_data_path is None:
            sample_data_path = abspath(join(output_dir, "sample_data.tsv"))

        DataFrameAnalyzer.to_tsv(df, sample_data_path)

        if script_path is None:
            script_path = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/deseq2_run_basic_contrast.R'

        for factor, c1, c2 in contrasts_list:
            pca_out = pca_out if pca_out is not None else join(output_dir, 'pca_%s_%s_%s.pdf' % (factor, c1, c2))
            cmd = ' '.join([DESeq2Analyzer.get_rscript_cluster_path(), script_path, '--sample_table_path', sample_data_path, '--factor', factor,
                            '--contrast1', c1, '--contrast2', c2, '--sepath', se_path, '--ref', ref,
                            '--outdir', output_dir, '--outcounts', outcounts, '--pcaout', pca_out, '--downsampling', str(reads_fraction)])
            print(cmd)

        return cmd

    @staticmethod
    def summarize_overlaps(df, output_dir, ncores=10, species='mouse', sample_data_path=None):
        # the column treatment has to exist to conduct a comparison
        if not exists(output_dir):
            mkdir(output_dir)

        if sample_data_path is None:
            sample_data_path = abspath(join(output_dir, "sample_data.tsv"))

        DataFrameAnalyzer.to_tsv(df, sample_data_path)

        rscript = DESeq2Analyzer.get_rscript_cluster_path()
        script_path = '/g/scb2/zaugg/rio/EclipseProjects/wild/lib/rscripts/summarize_overlaps.R'
        cmd = ' '.join([rscript, script_path, '--samples', sample_data_path, '--species', species,
                        '--outdir', output_dir])
        print(cmd)
        return cmd





