'''
Created on May 23, 2016

@author: ignacio
'''

# define basic import

import os
from os.path import join, exists, isdir, basename, abspath, dirname, getmtime
from os import remove, listdir, mkdir, rmdir, makedirs, system, chdir
from shutil import copy2, move

# the size of a given path
def filesize(p):
    return os.path.getsize(p)

def clean_tmp(pattern=None):
    from os import stat
    from pwd import getpwuid
    def find_owner(filename):
        return getpwuid(stat(filename).st_uid).pw_name
    paths = [join('/tmp', f) for f in listdir('/tmp') if find_owner(join('/tmp', f)) == 'rio']
    for p in paths:
        if pattern is not None:
            if not pattern in p:
                continue
        if isdir(p):
            system('rm -rf ' + p)
        else:
            remove(p)
    return paths
def get_total_size(paths):
    return sum([get_file_size(p) for p in paths])
def get_file_size(p):
    return os.path.getsize(p)

def get_parent_path(p):
    return os.path.abspath(os.path.join(os.path.dirname(p), ".."))

# these are some basic paths that we define before calling paths
# when we have subfolders, we decide the amount of recursion by two, and nothing else beyond
input_dir = "../input" if exists("../input") else '../../input'
output_dir = "../output" if exists("../output") else '../../output'
tmp_dir = "../tmp" if exists("../tmp") else '../../tmp'
data_dir = "../data" if exists("../data") else '../../data'

# this directory contains a lot of Rscripts we implement to call recurrent functions
RSCRIPTS_DIR = "/home/ignacio/Dropbox/Eclipse_Projects/zaugglab/lib/rscripts"
if not exists(RSCRIPTS_DIR):
    RSCRIPTS_DIR = "/g/scb/zaugg/rio/EclipseProjects/zaugglab/lib/rscripts"
if not exists(RSCRIPTS_DIR):
    RSCRIPTS_DIR = "/mnt/c/Users/ignacio/Dropbox/Eclipse_Projects/zaugglab/lib/rscripts"
if not exists(RSCRIPTS_DIR):
    RSCRIPTS_DIR = r'C:\Users\ignacio\Dropbox\Eclipse_Projects\zaugglab\lib\rscripts'
check = False
if check:
    assert exists(input_dir)
    assert exists(output_dir)
    assert exists(tmp_dir)
    assert exists(data_dir)
