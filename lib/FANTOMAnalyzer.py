'''
Created on 2/1/2018, 2018

@author: Ignacio Ibarra Del Rio

Description:
'''

from lib.utils import *



class FANTOM5Analyzer:

    @staticmethod
    def get_eng_zscores(ensg, zscores_fantom=None):
        if zscores_fantom is None:
            zscores_fantom = FANTOM5Analyzer.get_eng_zscores()

        sel = zscores_fantom[zscores_fantom['ensembl'] == ensg]
        return sel

    @staticmethod
    def get_zscores_dataframe():
        p = '/g/scb/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data/zscores_FANTOM5_hg19.tsv.gz'
        return DataFrameAnalyzer.read_tsv_gz(p)