'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.plot_utils import *

class ColorBar():


    @staticmethod
    def make_color_bar(vmin, vmax, label, cmap, output_path, figsize=[8, 3], axes=[0.05, 0.80, 0.2, 0.04],
                       dx_step=1):
        # Make a figure and axes with dimensions as desired.
        import matplotlib as mpl
        fig = plt.figure(figsize=figsize)
        ax1 = fig.add_axes(axes)
        # Set the colormap and norm to correspond to the data for which
        # the colorbar will be used.
        vmin = vmin
        vmax = vmax
        norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)

        # ColorbarBase derives from ScalarMappable and puts a colorbar
        # in a specified axes, so it has everything needed for a
        # standalone colorbar.  There are many more kwargs, but the
        # following gives a basic continuous colorbar with ticks
        # and labels.
        cb1 = mpl.colorbar.ColorbarBase(ax1, cmap=cmap, ticks=[xi for xi in range(vmin, vmax, dx_step)],
                                        norm=norm, format='%.0f',
                                        orientation='horizontal')
        cb1.set_label(label)
        savepdf(output_path)
        plt.close()