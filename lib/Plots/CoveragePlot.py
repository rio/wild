'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.plot_utils import *
from lib.BAMAnalyzer import BAMAnalyzer


class CoveragePlot:
    @staticmethod
    def make_coverage_plot(paths, coordinates, unit='CPM', **kwargs):
        # ranges have to have the same length
        lengths = set(coordinates[coordinates.columns[2]] - coordinates[coordinates.columns[1]])
        print lengths
        assert len(lengths) == 1

        extend = list(set(coordinates[coordinates.columns[2]] - coordinates[coordinates.columns[1]]))[0] / 2
        print 'extension_size', extend

        # analyze coverage by position in each bam path
        cov_by_file = {}
        libsize_by_file = {}
        for bampath in paths:
            f = basename(bampath)
            libsize = BAMAnalyzer.get_libsize(bampath)
            libsize_by_file[f] = libsize
            print f
            if f in cov_by_file:
                continue
            cov = BAMAnalyzer.get_coverage_per_position(coordinates, bampath, compress_bedtools_out=False)
            cov_by_file[f] = cov


        legends = []

        if unit == 'CPM':
            for i, bampath in enumerate(paths):
                f = basename(bampath)
                next = cov_by_file[f]
                next['cpm'] = next['counts'] / libsize_by_file[f] * 1e6
                mp = next.pivot('coordinate', 'position', 'cpm')
                # calculate average by position
                window = 100
                # plt.plot(range(mp.shape[1] / 10), mp[mp.index.to_series().isin(sel)][mp.columns[1800:2200]].mean(axis=0))
                next_color = kwargs.get('line_colors', 'red')
                if isinstance(next_color, list):
                    next_color = next_color[i]
                plt.plot(range(mp.shape[1]), mp.mean(axis=0).rolling(window).mean(),
                         color=next_color)
                # plt.plot(range(mp.shape[1]), mp.mean(axis=0))
                legends.append(f.split(".")[0])
            plt.ylabel('Counts per million')
            plt.xlabel('Distance to summit (bp)')
            plt.legend(legends, fontsize=7)
            plt.xticks([0, extend, extend * 2], ['-%i' % extend, '0', '%i' % extend])
            sns.despine(right=True, top=True)
