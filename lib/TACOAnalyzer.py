
from lib.utils import *

class TACOAnalyzer():


    def run(self, spec_file, sensitivity=None, output_dir=None):

        if sensitivity is not None:
            lines = [line for line in open(spec_file)]
            lines = ['  Sensitivity = ' + str(sensitivity) + "\n"
                     if '  Sensitivity = ' in line and not line.startswith("#")
                     else line for line in lines]
            new_spec = join(output_dir,
                            basename(spec_file).replace(".spec",
                                                        "_" + str(sensitivity) + ".spec"))
            writer = open(new_spec, 'w')
            for line in lines:
                writer.write(line)
            writer.close()
        print('It only runs in Spinoza and as a guest (caution)')
        print('please run module load R before running python')
        # print system("module load R")
        system('/g/scb2/zaugg/zaugg_shared/Programs/taco/src/taco ' + new_spec)
