'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.FastaAnalyzer import FastaAnalyzer
from lib.SequenceMethods import SequenceMethods


class MSAAnalyzer:
    @staticmethod
    def make_msa(length, n, output_path, mutation_rate=2, residues_aa=None):
        ref_seq = SequenceMethods.get_random_sequence(length, code=SequenceMethods.get_protein_code())

        headers = [str(i) for i in range(n)]
        fa = [[h, SequenceMethods.get_mutated_sequence(ref_seq, mutation_rate)] for h in headers]

        # define couplings (custom query)
        if residues_aa is not None:
            npos = len(list(residues_aa.keys()))
            for i in range(len(fa)):
                next_seq = [aa for aa in fa[i][1]]
                selected = random.choice(list(range(0, npos)))
                for next_pos in residues_aa:
                    next_seq[next_pos] = residues_aa[next_pos][selected]
                next_seq = "".join(next_seq)
                fa[i][1] = next_seq

        # mean sequence identity
        n = 0
        seq_ids = []
        for c in combinations(enumerate(zip(*fa)[1]), r=2):
            n += 1
            i, si = c[0]
            j, sj = c[1]
            seq_id = sum([ai == bi and ai != '-' for ai, bi in zip(si, sj)]) / float(min(len(si), len(sj)))
            seq_ids.append(seq_id)
        print(n, average(seq_ids))

        FastaAnalyzer.write_fasta_from_sequences(fa, output_path)
        print('written msa to')
        print(output_path)


if __name__ == '__main__':
    for L in range(200, 1000, 100):
        n = 200
        # define a coupling at positions 10 and 20
        residues_aa = {40: ['A', 'E'], 90: ['V', 'N']}
        mutation_rate = 0.25
        average_seq_id = 100 - mutation_rate * 100
        MSAAnalyzer.make_msa(L, n, "/scratch/rio/msa_couplings_L%i_n%i_pos40_AE_pos90_VN_SEQID%i.fa" % (L, n, average_seq_id),
                             residues_aa=residues_aa,
                             mutation_rate=int(mutation_rate * L))


    # analyze NUP45 input and reduce it
    p = "/scratch/romanov/NUP145_CHATD_hhblits_align.filt.fas"
    fa = FastaAnalyzer.get_fastas(p)
    # save into bins of STEP
    step = 200
    for i in range(0, len(fa[0][1]), step):
        print(i, i + step)
        next_fa = [[h, s[i: i + step]] for h, s in fa]
        output_path = "/scratch/rio/NUP145_CHATD_hhblits_align_segment_%i-%i.fa" % (i, i + step)
        FastaAnalyzer.write_fasta_from_sequences(next_fa, output_path)

