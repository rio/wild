'''
Created on 4/1/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.SequenceMethods import SequenceMethods
from lib.FastaAnalyzer import FastaAnalyzer
from itertools import product
from lib.SELEX.SELEXAnalyzer import HTSELEXAnalyzer
class KmerMapper():


    @staticmethod
    def query_kmers(data_structure, kmer):
        iupac_table = SequenceMethods.get_iupac_table()
        out = []
        for k in KmerMapper.get_sub_kmers(kmer):
            next = data_structure[k] if k in data_structure else None
            if isinstance(next, pd.DataFrame):
                out.append(next)
        if len(out) == 0:
            empty = pd.DataFrame(columns=['peak.id', 'position', 'strand'])
            return empty
        return pd.concat(out)

    @staticmethod
    def get_sub_kmers(kmer):
        iupac_table = SequenceMethods.get_iupac_table()
        options_by_position = ["".join(list(iupac_table[k])) for k in kmer]
        return [''.join(item) for item in product(*options_by_position)]

    @staticmethod
    def get_data_structure(fa_path, k_query=10, overwrite_bkp=False, start_k=3, end_k=10):
        '''
        This is a pointer data structure that, for each possible kmer, it saves pointers to all relevant position
        where these kmers could exists
        :param sequences:
        :return:
        '''

        headers, seqs = list(zip(*FastaAnalyzer.get_fastas(fa_path)))
        print('lengths of sequences', set([len(s) for s in seqs]))
        # check if we are done
        done = True
        for k in range(start_k, end_k + 1):
            bkp_path = fa_path.replace(".fa", '_kmers_' + str(k) + ".pkl")
            # print exists(bkp_path), bkp_path
            if not exists(bkp_path):
                done = False
        if not done or overwrite_bkp:
            mapper = {}
            base_k = start_k
            for k in range(start_k, end_k + 1):
                bkp_path = fa_path.replace(".fa", '_kmers_' + str(k) + ".pkl")
                if exists(bkp_path):
                    mapper = DataFrameAnalyzer.read_pickle(bkp_path)
                    continue

                print('loading kmers for', k)
                if k == base_k:
                    df = KmerMapper.map_motifs_in_sequences(seqs, k)
                    for i, nt in enumerate(SequenceMethods.get_sequence_combinations(k)):
                        kmer = "".join(nt)
                        # print k, kmer
                        print(k, i, kmer)
                        sel = df[df['seq'] == kmer]
                        del sel['seq']
                        mapper[kmer] = sel
                        DataFrameAnalyzer.to_pickle(mapper, bkp_path)
                else:
                    previous_keys = sorted([previous_k for previous_k in list(mapper.keys()) if len(previous_k) + 1 == k])
                    for ki, previous_k in enumerate(previous_keys):
                        if ki % (len(previous_keys) / 10) == 0: # print in ten batches
                            print(ki, 'kmers out of', len(previous_keys), previous_k)
                        next = pd.DataFrame.copy(mapper[previous_k])
                        # print previous_k, next.shape
                        next['seq'] = [seqs[r['peak.id']][r['position']:r['position'] + k] if r['strand'] == '+' else
                                       SequenceMethods.get_complementary_seq(seqs[r['position']:r['position'] + k])
                                       for ri, r in next.iterrows()]
                        next = next[next['seq'].str.len() == k]
                        for next_kmer, grp in next.groupby('seq'):
                            # print next_kmer, grp.shape[0]
                            # print k, previous_k, next_kmer
                            # print grp
                            # print 'AAAA' in mapper
                            if not next_kmer in mapper:
                                del grp['seq']
                                mapper[next_kmer] = grp
                            else:
                                print(next_kmer)
                                assert not next_kmer in mapper
                    output_mapper = {ki: mapper[ki] for ki in mapper if len(ki) == k}
                    DataFrameAnalyzer.to_pickle(output_mapper, bkp_path)

        bkp_path = fa_path.replace(".fa", '_kmers_' + str(k_query) + ".pkl")
        assert exists(bkp_path)
        print('loading and returning', bkp_path)
        return DataFrameAnalyzer.read_pickle(bkp_path)

    @staticmethod
    def map_motifs_in_sequences(seqs, motif_length, double_strand=True):
        table = []
        for si, s in enumerate(seqs):
            if si % 100 == 0:
                print(si, len(seqs))
            for i in range(0, len(s) - motif_length + 1):
                next = s[i: i + motif_length]
                table.append([si, i, "+", next])
                table.append([si, i, "-", SequenceMethods.get_complementary_seq(next)])
        return pd.DataFrame(table, columns=['peak.id', 'position', 'strand', 'seq'])