'''
Created on 7/22/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.SequenceMethods import SequenceMethods
from lib.BAMAnalyzer import BAMAnalyzer
class TerminalRepressors():

    @staticmethod
    def get_genes_by_tissue():
        genes_by_tissue = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data_2018-07-09_00-00-00/data/gene_subsets_five_tissues_1000_genes.tsv'
        genes_df = DataFrameAnalyzer.read_tsv(genes_by_tissue)
        # convert genes_df into a dictionary
        genes_df = {k: set(genes_df[k]) for k in genes_df.columns}
        print('loading Butte sets')
        # add custom names from Butte's data
        butte_dir = '../../data/gene_sets_custom'
        for f in listdir(butte_dir):
            p = join(butte_dir, f)
            ids = {s.strip() for s in open(p)}
            # print f, len(ids), ids
            genes_df[f.replace(".txt", '')] = ids

        # load IDs for Pancreas and Heart from GTEx directly
        for tissue_id in ['Pancreas', 'Heart - Left Ventricle']:
            p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data_2018-07-09_00-00-00/data/1000_genes_gtex_tissues/$1.tsv.gz'
            next_df = DataFrameAnalyzer.read_tsv_gz(p.replace("$1", tissue_id))
            genes_df[tissue_id.replace('Pancreas', 'pancreas').replace("Heart - Left Ventricle", 'Heart')] = next_df[
                'Gene stable ID']

        return genes_df


    @staticmethod
    def get_myt1l_peaks(extend=2000, sheet_name='MEF_MYT1L_cat_peaks'):
        # plot density plots
        # df = pd.read_excel('../../data/nature21722-s2.xlsx', sheet_name='MEF_A_B_MYT1L_cat_peaks')
        df = pd.read_excel('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data/nature21722-s2.xlsx', sheet_name=sheet_name)
        # df['start.from.summit'] = df[start_lab] + df[summit_lab] - bp
        df['start.from.summit'] = df['chromStart int'] + df['summit int'] - extend
        df['end.from.summit'] = df['start.from.summit'] + extend * 2

        df['chr'] = 'chr' + df['chrom string'].astype(str)
        df = SequenceMethods.parse_range2coordinate(df, ['chr', 'chromStart int', 'chromEnd int'], 'k.original')
        cols_first = ['chr', 'start.from.summit', 'end.from.summit', 'k.original']
        out = df[cols_first + [c for c in df if c not in set(cols_first)]]
        out = SequenceMethods.parse_range2coordinate(out, ['chr', 'start.from.summit', 'end.from.summit'], 'k')
        out = out.drop_duplicates('k')
        return out

    @staticmethod
    def get_h3k4me3_brain_bam_paths():
        p1, p2 = ['/g/scb2/zaugg/rio/data/MouseENCODE/%s.sorted.bam' % k for k in ('ENCFF072OAK', 'ENCFF633VWI')]
        return [p1, p2]

    @staticmethod
    def get_h3k4me3_MEF_bam_paths():
        basedir = '/g/scb2/zaugg/rio/data/GSE26657/mapped_mm10/'

        sorted_bams = [join(basedir, f) for f in listdir(basedir) if
                       f.endswith('.bam') and f in {'ENCFF987QSS.sorted.bam', 'ENCFF214QES.sorted.bam'}]
        print(sorted_bams)
        return sorted_bams

    @staticmethod
    def get_tss(genome, extend=0, **kwargs):
        df = None
        assert genome in {'hg19', 'mm10', 'hg38'}
        datadir = kwargs.get('datadir',
                             '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data_2018-07-09_00-00-00/data')
        if genome == 'mm10':
            df = DataFrameAnalyzer.read_tsv(join(datadir, 'mm10_refseq_n_ensembl_n_pos.tsv'))
            df['start'] = np.where(df['pos'] == 1, df['tss'].astype(int), (df['tss'] - extend).astype(int))
            df['end'] = np.where(df['pos'] == 1, (df['tss'] + extend).astype(int), df['tss'].astype(int))
        elif genome == 'hg19':
            bed_tss_hg19 = None
            bed_tss_hg19 = join(datadir, 'hg19_refseq_n_ensembl_n_pos.tsv')
            df = DataFrameAnalyzer.read_tsv(bed_tss_hg19)
            df['start'] = df['tss'].astype(int) - extend
            df['end'] = (df['start'] + extend * 2).astype(int)
        elif genome == 'hg38':
            bed_tss_hg38 = None
            bed_tss_hg38 = join(datadir, 'hg38_refseq_n_ensembl_n_pos.tsv')
            df = DataFrameAnalyzer.read_tsv(bed_tss_hg38)
            df['start'] = df['tss'].astype(int) - extend
            df['end'] = (df['start'] + extend * 2).astype(int)
        df = df.sort_values(['chr', 'start'], ascending=[True, True])
        return df

    @staticmethod
    def compare_cpm_peaks(coordinates, paths_tissue1, paths_tissue2, n_subsample_fg=None,
                          species='human', extend=2000, npermutation=1000, permutations=None):
        na, nb = len(paths_tissue1), len(paths_tissue2)
        # bg regions are TSS
        tss = TerminalRepressors.get_tss(species=species, extend=extend)

        print('positive...')
        observed, differences = None, None
        fg = []
        for next_bam in paths_tissue1 + paths_tissue2:
            counts_per_peak = BAMAnalyzer.featureCounts(coordinates, next_bam, n_threads=8)
            # print counts_per_peak.head()
            # print counts_per_peak.tail()
            fg.append(counts_per_peak)
        fg = pd.concat(fg)
        hm = fg.pivot('Geneid', 'filename', 'cpm')
        # observed CPM difference

        if n_subsample_fg is not None:
            differences = hm[hm.columns[:na]].mean(axis=1) - hm[hm.columns[na:]].mean(axis=1)
            observed = average(differences)
        else:
            observed_subsamples = []
            for i in range(npermutation):
                next = hm.sample(n_subsample_fg)
                observed_subsamples.append(average(hm[hm.columns[:na]].mean(axis=1) - hm[hm.columns[na:]].mean(axis=1)))
            differences = observed_subsamples
            observed = average(differences)
        print('observed value', observed, len(differences), differences[:5])

        print('background...')
        if permutations is None:
            bg = []
            for next_bam in paths_tissue1 + paths_tissue2:
                counts_per_peak = BAMAnalyzer.featureCounts(tss[['chr', 'start', 'end']], next_bam, n_threads=8)
                bg.append(counts_per_peak)
            bg = pd.concat(bg)
            hm = bg.pivot('Geneid', 'filename', 'cpm')

            permutations = []
            for i in range(npermutation):
                next = hm.sample(coordinates.shape[0] if n_subsample_fg is None else min(n_subsample_fg, hm.shape[0]))
                permutations.append(average(next[next.columns[:na]].mean(axis=1) - next[next.columns[na:]].mean(axis=1)))

        pval = sum([observed >= pi for pi in permutations]) / float(len(permutations)) + 1 / float(npermutation)
        return fg, differences, observed, permutations, pval

    @staticmethod
    def get_reprogramming_tfs():
        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data/ng.3487-S2.xlsx'
        df_true = pd.read_excel(p, sheet_name='true_cases')
        return df_true

    @staticmethod
    def get_predictions_by_approach():
        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data/ng.3487-S2.xlsx'
        df = pd.read_excel(p, sheet_name='long_format')
        table = []
        for method in df.columns[:3]:
            print(method)
            next = df[df.columns[4:]]
            next['method'] = method
            next['tf'] = df[method]
            table.append(next)
        df = pd.concat(table)
        df = df[df['tf'].astype(str) != 'nan']
        df['tf'] = np.where(df['tf'].str.startswith('POU5F1'), 'OCT4', df['tf'])
        return df

    @staticmethod
    def get_performances_three_tools(pred=None):
        df_true = TerminalRepressors.get_reprogramming_tfs()
        tfs_by_prot = {k: set(grp['tf']) for k, grp in df_true.groupby('reprogramming.protocol')}

        if pred is None:
            pred = TerminalRepressors.get_predictions_by_approach()

        res = []
        for reprog_protocol_id, grp in pred.groupby('reprogramming.protocol'):
            true_cases = tfs_by_prot[reprog_protocol_id]
            for method, grp2 in grp.groupby('method'):
                for ni in range(1, len(list(grp2['tf'])) + 1):
                    tfs = list(grp2['tf'])[:ni]
                    n_selected = len(tfs)
                    res.append([reprog_protocol_id, method, n_selected, true_cases, tfs] + TerminalRepressors.get_prediction_performance(true_cases, tfs))
        res = pd.DataFrame(res, columns=['reprogramming.protocol', 'method', 'n.selected', 'true', 'pred', 'n.recovered', 'average.rank', 'perc.recovered'])
        return res

    @staticmethod
    def get_prediction_performance(ranked_true_tfs, ranked_pred_tfs):
        ranks = [i + 1 for i in range(len(ranked_pred_tfs)) if ranked_pred_tfs[i] in set(ranked_true_tfs)]
        n_recovered = len(set(ranked_true_tfs).intersection(ranked_pred_tfs))
        avr_rank = average(ranks)
        perc_recovered = float(n_recovered) / len(ranked_true_tfs) * 100
        return [n_recovered, avr_rank, perc_recovered]
