
from lib.utils import *


class QTLAnalyzer:


    def group_category_and_locations(self, genes_path, locations_path,
                                     group_header, output_path):
        '''
        Given a genes path for a category (COMPLEXES/PATHWAYS), and a gene
        locaitons path, merge both into a single file and write it to output
        :param genes_path:
        :param locations_path:
        :return:
        '''
        # we are going to redefine the script

        # STEPS
        # 1) Get gene coordinates from each gene in our list
        genes_df = pd.read_csv(genes_path, sep='\t')  # , nrows=10)
        locations_df = pd.read_csv(locations_path, sep='\t')  # , nrows=10)
        print(genes_df.head())

        genes_df = genes_df[
            [k for k in genes_df.columns if not k.startswith("NA")]]
        # prepare geneloc -> coordinate dictionary
        print('preparing intersections')
        loc = {str(r['geneid']): list(r[['chr', 'start', 'end']].values)
               for ri, r in locations_df.iterrows()}
        sel_loc = []
        for geneid in genes_df['ENSGG']:
            accept = False
            for k in geneid.split("/"):
                if k in loc:
                    sel_loc.append([k, loc[k]])
                    accept = True
            if not accept:
                sel_loc.append(None)

        print(genes_df.shape)
        print(len(sel_loc))

        # be aware that some genes might not be found in battle's abundanes table.
        for geneid, sel in zip(genes_df['ENSGG'], sel_loc):
            if sel is None:
                print(geneid, sel)
        genes_df = genes_df[
            ['ENSGG', group_header + "ID", group_header + "Name", 'GeneName']]
        df2 = pd.DataFrame([[k] + loc for k, loc in sel_loc],
                           columns=['gene', 'chr', 'start', 'end'])

        genes_df = pd.concat([genes_df, df2], axis=1)
        print(genes_df.head())
        del genes_df['ENSGG']
        genes_df.to_csv(output_path, sep='\t', index=None)

    def intersect_gene_coordinates_with_snps(self, genes_path, snps_path_or_dir,
                                             output_path_or_dir):
        '''
        Perform the analysis with single cases or directory, according to how it was given.
        :param genes_path:
        :param snps_path_or_dir:
        :param output_path_or_dir:
        :return:
        '''
        cmd = " ".join(['/g/software/bin/Rscript', '04_2_1_intersect_snps_with_genes_batch.R',
                    snps_path_or_dir, genes_path, output_path_or_dir])
        print(cmd)
        system(cmd)

    def filter_snps_from_snpdosages(self, dosages_path, snps, output_path,
                                    stop_at=None):
        lines = []

        stream = gzip.open(dosages_path) if dosages_path.endswith(".gz") else open(dosages_path)
        i = 0

        print('about to read')
        for r in stream:
            if i == 0:
                lines.append(r.replace("quant_NA", "NA"))
            elif r.split("\t")[0] in snps:
                lines.append(r)
            i += 1
            if i % 100000 == 0:
                print(i)
            if stop_at is not None and i >= stop_at:
                break

        writer = open(output_path, 'w')
        for line in lines:
            writer.write(line)
        writer.close()

    def filter_individual_from_matrix(self, input, individual_ids, output,
                                      refcolumn='ENSG', nskip_cols=None,
                                      headers=None):
        if headers is None:
            df = pd.read_csv(input, sep='\t', index_col=None)
        else:
            df = pd.read_csv(input, sep='\t', index_col=None, header=None)
            df.columns = [refcolumn] + ['REF', 'ALT'] + headers + ["NA"]
            del df['REF']
            del df['ALT']

        df = df[[refcolumn] + [k for k in df.columns if k in individual_ids]]
        df.to_csv(output, sep='\t', index=None)

    def randomize_by_rows(self, input_path, output_path):
        df = pd.read_csv(input_path, sep='\t', index_col=None)

        headers = df.columns
        genes = df[[k for k in df.columns if 'ENSG' in k]]
        values_df = df[[k for k in df.columns if not 'ENSG' in k]]

        for ri, r in values_df.iterrows():
            v = r.values
            # print list(v)
            shuffle(v)
            # print list(v)
            values_df.iloc[ri] = v

        df2 = pd.concat([genes, values_df], axis=1)
        df2.to_csv(output_path, sep='\t', index=None)

