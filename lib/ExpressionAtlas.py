'''
Created on 2/6/2018, 2018

@author: Ignacio Ibarra Del Rio

Description:
'''

from lib.utils import *
from lib.MyGeneAnalyzer import MyGeneAnalyzer

class ExpressionAtlas:

    @staticmethod
    def get_zscores(**kwargs):
        basedir = '/g/scb2/zaugg/rio/data/expression_atlas'
        f = 'human_tpms_merged_quantile_zscores.tsv.gz'
        df = DataFrameAnalyzer.read_tsv_gz(join(basedir, f), header=None, index_col=None, **kwargs)

        # add columns and headers
        bkp_labels = join(basedir, 'human_tpms_merged_columns_and_index.pkl')
        if not exists(bkp_labels):
            tpms_dir = '/g/scb2/zaugg/rio/data/expression_atlas/tpms'
            merged_tpm_path = join(tpms_dir, '..', 'human_tpms_merged.tsv.gz')
            res = DataFrameAnalyzer.read_tsv_gz(merged_tpm_path, index_col=0)
            index, columns = res.index, res.columns
            DataFrameAnalyzer.to_pickle([index, columns], bkp_labels)
        index, columns = DataFrameAnalyzer.read_pickle(bkp_labels)
        df.index = list(index)[:df.shape[0]]
        df.columns = list(columns)[:df.shape[1]]

        # add gene symbols in last column
        symbols_bkp = "/tmp/symbols.pkl"
        if not exists(symbols_bkp):
            ensembl_to_symbol = MyGeneAnalyzer.get_symbol_by_ensembl(df.index)
            DataFrameAnalyzer.to_pickle(ensembl_to_symbol, symbols_bkp)
        ensembl_to_symbol = DataFrameAnalyzer.read_pickle(symbols_bkp)
        df['symbol'] = [ensembl_to_symbol[idx] if idx in ensembl_to_symbol else None for idx in df.index]
        return df

    @staticmethod
    def get_zscores_hocomoco_subset():
        basedir = '/g/scb2/zaugg/rio/data/expression_atlas'
        f = 'human_tpms_merged_quantile_zscores.tsv.gz'
        print('calculating z-scores or HOCO motifs')
        filtered_path = join(basedir, f.replace(".tsv.gz", "_HOCOMOCO.tsv.gz"))
        if not exists(filtered_path):
            print('preparing subset with TFs only')
            ensg = ExpressionAtlas.get_tfs_hocomoco_with_myt1l()
            print(ensg.head())
            ensembl_hocomoco = MyGeneAnalyzer.get_ensembl_by_symbol(ensg['Transcription factor'])

            zscores = ExpressionAtlas.get_zscores()

            # this is a debug case (reason: HOCOMOCO IDs sometimes are UniProt and don't contain all the info we wanted)
            print(ensembl_hocomoco['CPEB1'])
            print(ensembl_hocomoco['E2F2'])
            print('ENSG00000214575' in set(zscores.index))
            print('ENSG00000007968' in set(zscores.index))
            print('ENSG00000282899' in set(zscores.index))
            print('ENSG00000278129' in set(zscores.index))

            # in this step we will add some indexes that we require but not were added in the first place to the table
            zscores = zscores[zscores.index.isin(set(ensembl_hocomoco.values()) + set('ENSG00000278129'))]
            DataFrameAnalyzer.to_tsv_gz(zscores, filtered_path, index=True)
            print('ENSG00000278129' in set(zscores.index))
            print('saved zscores (Expr. Atlas.) to')
            print(filtered_path)

        print('reading')
        print(filtered_path)
        df = DataFrameAnalyzer.read_tsv_gz(filtered_path, index_col=0)
        # print(list(df.columns))
        return df


        
    @staticmethod
    def get_celltype_column_relationships():
        '''
        Get a default set of associations between celltypes and column name to
        extract columns from the Z-scores dataframe
        :return:
        '''
        column_name_by_celltype = {}
        for tissue, k in zip(['pancreas'], ['pancreas']):
            column_name_by_tissue[k] = tissue

        return column_name_by_celltype



    @staticmethod
    def get_tfs_hocomoco_with_myt1l():
        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/moritz_collaboration/data/motifs_hocomoco/human/HOCOMOCOv11_full_annotation_HUMAN_mono_w_ensembl.tsv'
        ensg = DataFrameAnalyzer.read_tsv(p)
        myt1l_names = ["Myt1l_200-623_primary", "Myt1l_200-623_secondary",
                       "Myt1l_488-910_primary", "Myt1l_488-910_secondary",
                       "Myt1l_488-910_tertiary", "Myt1l_896-1042_primary"]
        for myt1l_name in myt1l_names:
            ensg = ensg[['Model', 'Transcription factor',
                         'EntrezGene', 'UniProt ID',
                         'ensg']].append(pd.DataFrame([[myt1l_name, 'Myt1l', 23040, 'MYT1L_HUMAN',
                                                        'ENSG00000186487']], columns=['Model', 'Transcription factor',
                                                                                      'EntrezGene', 'UniProt ID',
                                                                                      'ensg']))
        return ensg
            
			
