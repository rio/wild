'''
Created on

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.SequenceMethods import SequenceMethods


class ApobecAnalyzer:

    @staticmethod
    def get_chip_seq(chip_seq_dir=None):
        if chip_seq_dir is None:
            chip_seq_dir = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/apobec2_data_analysis/data/ChIPSeq_narrowPeak_files"
        df = []
        for f in listdir(chip_seq_dir):
            if not f.endswith(".narrowPeak"):
                continue
            p = join(chip_seq_dir, f)
            print(exists(p), basename(p))
            next = DataFrameAnalyzer.read_tsv(p, header=None)
            if next.shape[0] == 0:
                continue
            next.columns = ['chr', 'start', 'end'] + list(next.columns[3:])
            next = next.sort_values(['chr', 'start'], ascending=[True, True])
            next['filename'] = f.replace(".narrowPeak", '')
            df.append(next)
        df = pd.concat(df).reset_index(drop=True)
        for c in ['start', 'end']:
            df[c] = df[c].astype(int)
        df = SequenceMethods.parse_range2coordinate(df, ['chr', 'start', 'end'], 'k')
        df['time'] = df['filename'].str.split(".").str[-2].str[:2]
        return df

    @staticmethod
    def get_close_genes_peaks():
        from lib.FastaAnalyzer import FastaAnalyzer
        from lib.MyGeneAnalyzer import MyGeneAnalyzer
        from lib.REMAPAnalyzer import REMAPAnalyzer
        tss = MyGeneAnalyzer.get_gene_tss('mouse')
        tss = tss.sort_values(['chr', 'start'], ascending=[True, True])
        fa = FastaAnalyzer.get_fastas('../../data/all_fg_and_bg.fa')
        peaks = pd.DataFrame()
        peaks['k'] = [t[0] for t in fa if not 'rand' in t[0]]
        peaks = SequenceMethods.parse_coordinate2range(peaks, peaks['k'])
        peaks = peaks.sort_values(['chr', 'start'], ascending=[True, True])
        close = REMAPAnalyzer.get_closest_tss_gene(peaks[['chr', 'start', 'end']], tss)
        return close


    @staticmethod
    def get_rna(datadir=None):
        if datadir is None:
            datadir = "/g/scb2/zaugg/rio/EclipseProjects/zaugglab/apobec2_data_analysis/data"
        bkp_path = join(datadir, "rna.tsv.gz")
        if not exists(bkp_path):
            rna_seq_dir = join(datadir, "RNASeq_deseq")
            df = []
            for f in listdir(rna_seq_dir):
                if not f.endswith(".xls"):
                    continue
                p = join(rna_seq_dir, f)
                next = DataFrameAnalyzer.read_tsv(p)
                print(f, next.shape[0])
                next['filename'] = f.replace(".xls", '')
                df.append(next)
            df = pd.concat(df).reset_index(drop=True)
            df['time'] = df['filename'].str.split("_").str[2]

            # replace refseq by gene_ids
            from lib.MyGeneAnalyzer import MyGeneAnalyzer
            df.columns = ['entrez', 'symbol'] + list(df.columns[2:])
            ensembl_by_refseq = MyGeneAnalyzer.get_gene_ensg_by_entrez(set(df['entrez']), species='mouse')
            df['ensembl'] = df['entrez'].astype(str).map(ensembl_by_refseq)
            DataFrameAnalyzer.to_tsv_gz(df, bkp_path)
        return DataFrameAnalyzer.read_tsv_gz(bkp_path)

    @staticmethod
    def get_bioid():
        return pd.read_excel('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/apobec2_data_analysis/data/A2 BioID Table.xlsx')

    @staticmethod
    def get_flanking_sequences_kmers(module_id, extend=0, final_length=None, lowerflanks=False):

        kmer_scores_dir = '../../data/kmer_scores'
        scores = []
        for f in listdir(kmer_scores_dir):
            if f.startswith(module_id + "_"):
                print(join(kmer_scores_dir, f))
                next = DataFrameAnalyzer.read_tsv_gz(join(kmer_scores_dir, f))
                scores.append(next)
        print(len(scores))
        scores = pd.concat(scores)

        from lib.SequenceMethods import SequenceMethods
        scores = SequenceMethods.parse_coordinate2range(scores, scores['peak.name'].str.replace("_rand", ''),
                                                        ['chr', 'peak.start', 'peak.end'])
        scores = scores[~(scores['chr'] == 'chrM')]
        from lib.FastaAnalyzer import FastaAnalyzer
        fa = FastaAnalyzer.get_fastas('../../data/all_fg_and_bg.fa')
        fa = {t[0]: t[1] for t in fa}

        motif_length = len(list(scores['seq'])[0])
        if extend == 0 and final_length is not None:
            extend = (final_length - motif_length) / 2

        res = []
        for ri, r in scores.iterrows():
            start_no_ext, end_no_ext = r['position'], r['position'] + 8
            start, end = int(max(start_no_ext - extend, 0)), int(min(end_no_ext + extend, len(fa[r['peak.name']])))
            next = fa[r['peak.name']][start: end]
            if r['strand'] == '-':
                next = SequenceMethods.get_complementary_seq(next)
            if lowerflanks:
                next = next[:(start_no_ext - start)].lower() + next[(start_no_ext - start):-((end - end_no_ext) if (end - end_no_ext) > 0 else end)]+\
                       (next[-(end - end_no_ext):].lower() if (end - end_no_ext) > 0 else '')
            res.append([next, r['peak.name'], r['strand'], r['pred'], extend, len(r['seq']), r['distance.to.summit']])
        res = pd.DataFrame(res, columns=['sequence', 'peak.id', 'strand', 'score',
                                         'extend', 'motif.length', 'distance.summit'])
        res['module.id'] = module_id
        return res


    @staticmethod
    def get_flanking_sequences_cisbp(cisbp_id, extend=0, final_length=None, lowerflanks=False,
                                     **kwargs):
        scores_path = '../../data/cisbp_scores/%s.tsv.gz' % cisbp_id
        assert exists(scores_path)
        scores = DataFrameAnalyzer.read_tsv_gz(scores_path)
        from lib.SequenceMethods import SequenceMethods
        scores = SequenceMethods.parse_coordinate2range(scores, scores['sequence name'].str.replace("_rand", ''), ['chr', 'peak.start', 'peak.end'])
        scores['distance.summit'] = (((scores['peak.end'] - scores['peak.start']) / 2.0) - ((scores['start'] + scores['stop']) / 2.0)).abs()
        scores = scores[~(scores['chr'] == 'chrM')]
        from lib.FastaAnalyzer import FastaAnalyzer
        fa = FastaAnalyzer.get_fastas('../../data/all_fg_and_bg.fa')
        fa = {t[0]: t[1] for t in fa}

        motif_length = len(list(scores['matched sequence'])[0])
        if extend == 0 and final_length is not None:
            extend = int((final_length - motif_length) / 2)

        res = []
        ext_coordinate = []
        ext_len = []
        for ri, r in scores.iterrows():
            start_no_ext, end_no_ext = r['start'] - 1, r['stop']
            start, end = max(start_no_ext - extend, 0), min(end_no_ext + extend, len(fa[r['sequence name']]))
            start, end = int(start),  int(end)

            start_ext, end_ext = r['start'] + r['peak.start'] - extend,\
                                 r['peak.start'] + r['start'] + (r['stop'] - r['start'] + 1) + extend
            ext_coordinate.append(r['chr'] + ":" + str(start_ext) + "-" + str(end_ext))
            ext_len.append(end_ext - start_ext + 1)
            if kwargs.get('log'):
                print(start, end)
            next = fa[r['sequence name']][start: end]
            if r['strand'] == '-':
                next = SequenceMethods.get_complementary_seq(next)
            if lowerflanks:
                next = next[:(start_no_ext - start)].lower() + next[(start_no_ext - start):-((end - end_no_ext) if (end - end_no_ext) > 0 else end)]+\
                       (next[-(end - end_no_ext):].lower() if (end - end_no_ext) > 0 else '')
            res.append([next, r['sequence name'], r['strand'], r['score'], r['p-value'],
                        r['q-value'], extend, len(r['matched sequence']), r['distance.summit']])
            # print res[-1]
            # if len(res) == 5:
            #     stop()
        res = pd.DataFrame(res, columns=['sequence', 'peak.id', 'strand', 'score', 'p-value', 'q-value',
                                         'extend', 'motif.length', 'distance.summit'])

        res['region.ext.motif.centered'] = ext_coordinate

        res_ext = res[['region.ext.motif.centered']]
        res_ext = SequenceMethods.parse_coordinate2range(res_ext, res['region.ext.motif.centered'])
        fa_extended = FastaAnalyzer.get_sequences_from_bed(res_ext[['chr', 'start', 'end']], genome='mm10')

        seqs_extended = [s for h, s in fa_extended]
        assert res_ext.shape[0] == len(seqs_extended)

        res['ext.len'] = ext_len
        res['ext.seq'] = seqs_extended
        res['ext.seq'] = [s[:extend - 1].lower() + s[(extend - 1):-(extend + 1)].upper() + s[-(extend + 1):].lower() for s in res['ext.seq']]
        res['cisbp.id'] = cisbp_id
        return res

    @staticmethod
    def get_peaks_with_kmers(file_id, kmer_id):
        bkp_path = join('../../data/kmer_scores', kmer_id + "_" + file_id + ".tsv.gz")
        assert exists(bkp_path)
        return DataFrameAnalyzer.read_tsv_gz(bkp_path)


    @staticmethod
    def get_peaks_with_cisbp_id(cisbp_id):
        bkp_path = join('../../data/cisbp_scores', cisbp_id + ".tsv.gz")
        assert exists(bkp_path)
        return DataFrameAnalyzer.read_tsv_gz(bkp_path)
