

from lib.plot_utils import *
from lib.motif_plotter import ConsensusMotifPlotter
from lib.SequenceMethods import SequenceMethods
from lib.Motif.MotifConverter import MotifConverter

class SequenceGroup:
    def __init__(self):
        self.sequences = []
        self.shifts  = []
        self.model = None
        self.subgroup_sizes = [] # when putting many groups together, we'd like to know the amount per group
    def add_sequence(self, seq, shift, update_model=True):
        self.sequences.append(seq)
        self.shifts.append(shift)
        if update_model:
            self.update_model()
    def convert_to_complementary(self):
        self.sequences = [SequenceMethods.get_complementary_seq(s) for s in self.sequences]
        self.shifts = [-d for d in self.shifts]
        self.update_model()
    @staticmethod
    def get_complementary_group(a):
        b = SequenceGroup()
        b.sequences = [SequenceMethods.get_complementary_seq(s) for s in a.sequences]
        b.shifts = [-d for d in a.shifts]
        d_shift = abs(min(b.shifts))
        b.shifts = [si + d_shift for si in b.shifts]
        b.update_model()
        return b
    def __str__(self):
        s = '# of sequences ' + str(len(self.sequences)) + "\n"
        s += str(self.model) + "\n"
        return s
    def update_model(self):
        if self.model is None:
            pfm = MotifConverter.get_pfm_from_sequences(self.sequences)
            ppm = pfm
            for c in pfm:
                ppm[c] = pfm[c] / sum(pfm[c])
                # print sum(pwm[c])
                assert abs(sum(ppm[c]) - 1.0) < 1e-3
            # print pwm
            self.model = ppm
        else:
            min_shift = abs(min(self.shifts))
            self.shifts = [d + min_shift for d in self.shifts]
            # self.shifts, self.sequences = zip(*sorted([[shift, seq] for shift, seq in zip(self.shifts, self.sequences)],
            #                                           key=lambda x: x[0]))
            model_seqs = [" " * shift + s for s, shift in zip(self.sequences, self.shifts)]
            max_length = max([len(s) for s in model_seqs])
            model_seqs = [s + (" " * (max_length - len(s))) for s in model_seqs]
            # print 'model updated. N=', len(model_seqs)
            # for s in model_seqs:
            #     print s
            # print model_seqs
            pwm = MotifConverter.get_pfm_from_sequences(model_seqs)
            for c in pwm:
                pwm[c] = pwm[c] / sum(pwm[c])
            # print pwm
            self.model = pwm
            # print len(self.sequences)
    def add_sequences(self, seqs, shifts):
        # print len(self.sequences)
        for s, shift in zip(seqs, shifts):
            self.add_sequence(s, shift, update_model=False)
        # print len(self.sequences)
        self.update_model()
    def add_sequence_group(self, other, shift):
        print('# sequences in group (before)', len(self.sequences))
        # print self.sequences
        # print self.shifts
        if shift < 0:
            self.shifts = [d + abs(shift) for d in self.shifts]
            shift = 0
        if len(self.subgroup_sizes) == 0:
            self.subgroup_sizes = [len(self.sequences)]
        self.subgroup_sizes += [len(other.sequences)]
        self.add_sequences(other.sequences, [shift + other.shifts[i] for i in range(len(other.sequences))])
        # print '# sequences in group (after)', len(self.sequences)
        # print self.sequences, self.shifts
    def get_matches(self, seq, max_shift=None):
        # print 'get matches...'
        hits = []
        # print type(self.model)
        if isinstance(self.model, str):
            hits = SequenceMethods.get_best_alignment(seq, self.model, max_shift=max_shift).sort_values('matches', ascending=False)
        else: # DataFrame or PWM
            hits = SequenceMethods.get_best_alignment_seq_vs_pwm(seq, self.model, max_shift=max_shift).sort_values('matches', ascending=False)
        return hits
    def print_model(self):
        model_seqs = [" " * shift + s for s, shift in zip(self.sequences, self.shifts)]
        print('\nSEQS IN MODEL')
        for s in model_seqs:
            print(s)
        # print 'model'
        # print self.model
    @staticmethod
    def save_groups(sequence_groups, output_path='/tmp/single_groups.txt'):
        # summary
        sequence_groups = sorted(sequence_groups, key=lambda x: -len(x.sequences))
        print('# of models', len(sequence_groups))
        with open(output_path, 'w') as writer:
            for g in sequence_groups:
                model_seqs = [" " * shift + s for s, shift in zip(g.sequences, g.shifts)]
                writer.write('>MODEL\n')
                for s in model_seqs:
                    writer.write(s + "\n")
    @staticmethod
    def load_from_file(path):
        entries = []
        new_group = []
        for r in open(path):
            r = r.rstrip()
            if '>' in r:
                if len(new_group) != 0:
                    entries.append(new_group)
                new_group = []
            else:
                new_group.append(r)
        entries.append(new_group)
        sequence_groups = []
        for g in entries:
            next = SequenceGroup()
            shifts = [len(s) - len(s.replace(" ", '')) for s in g]
            g = [s.replace(" ", '') for s in g]
            next.add_sequences(g, shifts)
            sequence_groups.append(next)
        return sequence_groups

    @staticmethod
    def get_sequence_groups_one_mismatch(seqs_sorted_by_fold_enrichment,
                                         algorithm='one-mismatch'):
        # check for each seqeucne in the top of the list, the following ten sequences with at most one mismatch
        # with respect to them
        factor_stop = 0.1
        shift = []
        n_groups_max = -1

        seqs = seqs_sorted_by_fold_enrichment
        n_seqs_start = len(seqs)
        # seqs = [s for s in seqs if 'GGAA' in s]
        sequence_groups = []
        rejected_groups_by_kmer = {}  # this stores sequences with scores too low to be part of a given cluster. Saves computation time
        n_stop = n_seqs_start * (1 - factor_stop)
        while (len(seqs) > n_stop):
            if n_groups_max != -1 and len(sequence_groups) > n_groups_max:
                break
            print('# groups', len(sequence_groups))
            n_seqs_groups = sum([len(g.sequences) for g in sequence_groups])
            print('# sequences', n_seqs_groups, n_seqs_groups / float(n_seqs_start))
            ref = seqs[0]
            seq_group = SequenceGroup()
            seq_group.add_sequence(ref, 0)
            # print seq_group.model
            sequence_groups.append(seq_group)
            one_mismatch_options = {ref[:i] + nt + ref[i + 1:] for nt in 'ATCG' for i in range(len(ref))}
            sel = [[si, s] for si, s in enumerate(seqs) if s in one_mismatch_options]
            for idx, seq in sel:
                seq_group.add_sequence(seq, 0)
                # print seq
                # print seq_group.model
            grouped_indexes = {t[0] for t in sel}
            seqs = [s for si, s in enumerate(seqs) if si not in grouped_indexes]
            print('new group, N=', len(seq_group.sequences))

    @staticmethod
    def save_groups_as_logo(groups, output_path, show_subgroups=False, **kwargs):
        sns.set_style('white')
        matrix_size = np.ceil(len(groups) ** .5)

        n_biggest_subgroup = max([len(g.subgroup_sizes) for g in groups])
        fig = plt.figure(figsize=(kwargs.get('w', 10), kwargs.get('h', 10)))
        for i, g in enumerate(groups):
            if g is None:
                continue
            print(i, len(groups))
            ax = None
            if show_subgroups:
                ax = plt.subplot2grid((n_biggest_subgroup + 1, len(groups)), (n_biggest_subgroup, i))
            else:
                ax = plt.subplot(matrix_size, matrix_size, i + 1)
            model_seqs = [" " * shift + s for s, shift in zip(g.sequences, g.shifts)]
            max_len = max([len(s) for s in model_seqs])
            model_seqs = [s + (" " * (max_len - len(s))) for s in model_seqs]
            cbp = ConsensusMotifPlotter.from_sequences(model_seqs)
            cbp.plot(ax, style='empty')
            if kwargs.get('titles', None) is not None:
                plt.title(str(kwargs.get('titles')[i]))

            if show_subgroups:
                last_subgroup_size = 0
                if len(g.subgroup_sizes) == 0:
                    start = last_subgroup_size
                    end = start + len(g.sequences)
                    max_shift = max(g.shifts[start:end])
                    subgroup_seqs = [" " * shift + s + " " * (max_shift - shift)
                                     for s, shift in zip(g.sequences[start:end],
                                                         g.shifts[start:end])]
                    subgroup_seq_lengths = set([len(s) for s in subgroup_seqs])
                    assert len(subgroup_seq_lengths) == 1

                    ax = plt.subplot2grid((n_biggest_subgroup + 1, len(groups)), (j, i))
                    cbp = ConsensusMotifPlotter.from_sequences(subgroup_seqs)
                    cbp.plot(ax, style='empty')
                    plt.title(str(subgroup_seq_lengths), fontsize=4)
                    last_subgroup_size += subgroup_size
                else:
                    max_shift = max(g.shifts)
                    for j, subgroup_size in enumerate(g.subgroup_sizes):
                        start = last_subgroup_size
                        end = start + subgroup_size
                        subgroup_seqs = [" " * shift + s + " " * (max_shift - shift)
                                         for s, shift in zip(g.sequences[start:end],
                                                             g.shifts[start:end])]
                        subgroup_seq_lengths = set([len(s) for s in subgroup_seqs])
                        # print subgroup_seq_lengths
                        assert len(subgroup_seq_lengths) == 1

                        ax = plt.subplot2grid((n_biggest_subgroup + 1, len(groups)), (j, i))
                        cbp = ConsensusMotifPlotter.from_sequences(subgroup_seqs)
                        cbp.plot(ax, style='empty')
                        plt.plot()
                        plt.title(str(subgroup_seq_lengths), fontsize=4)
                        last_subgroup_size += subgroup_size

        savefig(output_path)
        plt.close()

