'''
Created on 2/5/2018, 2018

@author: Ignacio Ibarra Del Rio

Description:
'''

from lib.utils import *

class LiftOver:
    @staticmethod
    def convert_coordinates(input_bed_or_dataframe, ref_genome_code, output_bed, target_genome_code,
                            unmapped_path=None, overwrite=False, liftover_chains_dir=None, args=[]):

        print(not exists(output_bed), overwrite, not exists(output_bed) or overwrite)
        if not exists(output_bed) or overwrite:
            print('file not exists or overwrite. Run liftover...')
            compress = False
            if output_bed.endswith('.gz'):
                output_bed = output_bed[:-3]
                compress = True
            input_bed = None
            if isinstance(input_bed_or_dataframe, pd.DataFrame):
                tmp_path = tempfile.mkstemp()[1]
                DataFrameAnalyzer.to_tsv(input_bed_or_dataframe, tmp_path, header=None)
                input_bed = tmp_path
            else:
                input_bed = input_bed_or_dataframe

            if unmapped_path is None:
                unmapped_path = input_bed.replace(".bed", '_unmapped.bed') if '.bed' in input_bed else input_bed + "_unmapped"

            if liftover_chains_dir is None:
                liftover_chains_dir = '/g/scb2/zaugg/rio/data/liftover_chains'
            chain_path = join(liftover_chains_dir, ref_genome_code + "To" + target_genome_code.capitalize() + ".over.chain.gz")

            print(chain_path)

            assert exists(chain_path)
            print(input_bed)
            bin = '/g/software/bin/liftOver' if exists('/g') else 'liftOver'
            cmd = [bin] + args + [input_bed, chain_path, output_bed, unmapped_path]
            print(cmd)
            print(" ".join(cmd))
            system(" ".join(cmd))

            if compress:
                system('tar cvzf ' + output_bed + ".gz " + output_bed)
                remove(output_bed)

    @staticmethod
    def get_new_coor_by_old_dictionary(input_bed_or_dataframe, ref_genome_code, output_bed, target_genome_code,
                                       **kwargs):
        LiftOver.convert_coordinates(input_bed_or_dataframe, ref_genome_code, output_bed, target_genome_code,
                                     overwrite=kwargs.get('overwrite', False))



