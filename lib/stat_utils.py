

import scipy
from scipy.stats.stats import spearmanr, pearsonr
from scipy.stats import ranksums, linregress, ttest_ind, binom_test, mannwhitneyu, wilcoxon, percentileofscore
from scipy.stats import fisher_exact
import pandas as pd

def sklearn_to_df(sklearn_dataset):
    df = pd.DataFrame(sklearn_dataset.data, columns=sklearn_dataset.feature_names)
    df['target'] = pd.Series(sklearn_dataset.target)
    return df