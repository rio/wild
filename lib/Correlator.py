
from lib.utils import *
from numpy import correlate, array, concatenate
from scipy.stats import pearsonr

class Correlator:
    def get_best_cross_correlation(self, a, b, show=False, use_sequence=False,
                                   try_invert=False):
        """
        Get cross-correlations for all possible alignments between two sequences
        return start position at a and b where alignment is maximized
        """
        # get all correlation scores to map best aligned position
        correlations = correlate(a, b, mode="full")
        x = [xi for xi in range(len(correlations))]

        # plot cross-correlations
        if show:
            plt.plot(x, correlations)
            plt.show()

        # return position with the best score
        best_position, best_score = \
        sorted([[xi, ci] for xi, ci in zip(x, correlations)],
               key=lambda t: -t[1])[0]

        if try_invert:
            invert, best_pos_inverted,\
            best_score_inverted = self.get_best_cross_correlation(a, b[::-1],
                                                                  try_invert=False)
            if best_score_inverted > best_score:
                return 1, best_pos_inverted, best_score_inverted

        return 0, len(b) - (best_position + 1), best_score

    def get_best_correlation(self, a, b):
        '''
        Calculate correlation coefficients for two vectors, a and b, across different
        positions, and return the highest correlation value (for all positions)

        The length of B has to be larger than the length of B
        :param a:
        :param b:
        :return:
        '''
        correlations = [[i, pearsonr(a, b[i: i + len(a)])[0]] for i in range(len(b) - len(a) + 1)]
        df = pd.DataFrame(correlations, columns=['pos', 'r'])
        df = df.sort_values('r', ascending=False)
        # print correlations[max([c[1] for c in correlations])]
        return list(df.values[0])

    def shift_scored_region(self, v, dx):
        """
        Move values and scores for scored sequence dx positions, given as an integer
        (>0 right, <0 left)
        """
        q = [i for i in v]
        if dx > 0:
            q = q[dx:] + [0 for i in range(dx)]
        if dx < 0:
            q = [0 for i in range(-dx)] + q[:dx]
        return q

    def plot_aggregated_track(self, values, show=False, align=False,
                              output_basename=None, title=None):

        scores = []
        scores.append(values[0])

        # =======================================================================
        # Align with respect to reference case
        # =======================================================================
        for i, next in enumerate(values[1:]):
            invert, dx, score = self.get_best_cross_correlation(values[0], next)
            # print i, j
            if len(next) < len(values[0]):
                next = next + [0 for i in range(len(values[0]) - len(next))]
            q = self.shift_scored_region(next, dx)
            # print len(scores[0]), len(next), dx, len(q)
            scores.append(q)

        # remove flankings if the sum of a given column is zero
        zero_sum = [sum([s[i] for s in scores]) == 0 and len(set([s[i] for s in scores])) == 1
                    for i in range(len(scores[0]))]
        print(zero_sum)
        scores = [[s[i] for i in range(len(zero_sum)) if not zero_sum[i]] for s in scores]
        for s in scores:
            print(s)

        fig = plt.figure(figsize=(8, 5))
        cbar_ax = fig.add_axes([0.9, .66, .01, .2])
        ax = plt.subplot(2, 1, 1)
        hm = [q for q in scores]
        for s in scores:
            print(len(s), s)

        xmin, xmax = 0, len(scores[0])

        # this needs to be removed later
        xmin = 3
        xmax = 20

        sns.heatmap(hm, cbar_ax=cbar_ax, cmap='Reds',
                    cbar_kws={'label': 'feat. w'})
        ax.spines['left'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['top'].set_visible(True)
        ax.spines['right'].set_visible(True)

        # ax.set(aspect=1)
        plt.xticks([])
        plt.yticks([])
        plt.xlim([xmin, xmax])
        plt.ylabel('feature weight')

        from scipy.stats import sem
        averages = []
        err = []
        for j in range(len(scores[0])):
            column = [v[j] for v in scores]
            averages.append(average(column))
            err.append(sem(column))
            print(averages[-1], err[-1], column)

        # err = [np.std([v[j] for v in values]) for j in range(len(values[0]))]
        print(averages)
        print(err)
        if title is not None:
            plt.title(title)
        ax = plt.subplot(2, 1, 2)
        plt.errorbar([i + 0.5 for i in range(len(scores[0]))], averages,
                     yerr=[yi for yi in err])
        plt.xticks([i + 0.5 for i in range(len(q))],
                   [i + 1 if (i) % 2 == 0 else "" for i in range(len(q))])
        plt.xlim([xmin, xmax])
        # plt.ylim([0.0, 0.05])
        plt.xlabel('position')
        plt.ylabel('feature weight')
        ax.spines['left'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        plt.subplots_adjust(left=0.2, right=0.85)

        # plt.show()
        # exit()
        savefig(output_basename)


        if show:
            plt.show()

        plt.close()
        return




        plt.subplot(4, 1, 3)
        for q in scores:
            plt.plot([i + 0.5 for i in range(len(q))], q)
        plt.xticks([i + 0.5 for i in range(len(q))],
                   [i + 1 if (i + 1) % 5 == 0 else "" for i in range(len(q))])
        plt.xlim([3, 23])
        plt.xlabel('position')

        despine_all()
        plt.subplot(4, 1, 4)

        plt.boxplot([[v[j] for v in values] for j in range(len(values[0]))])
        plt.xticks([i + 1.0 for i in range(len(q))],
                   [i + 1 if (i + 1) % 5 == 0 else "" for i in range(len(q))])


        plt.xlabel('position')
        plt.xlim([3.5, 23.5])
        despine_all()

