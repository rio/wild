
from lib.utils import *
from Bio import motifs
import pandas as pd
from os.path import basename, join, exists
import gzip
import tempfile
from shutil import copy2

class MotifConverter:

    def convert_jaspar_pfm_to_homer_ppm(self, pfm_path, ppm_path):
        '''

        Convert a PFM matrix into a PPM matrix

        :param pfm_path: Input PFM path in JASPAR format
        :param ppm_path: Output PPM path in HOMER format
        :return:
        '''
        threshold = -1000000000

        # read file
        rows = [r.strip() for r in open(pfm_path)]
        motif_id = rows[0].split("\t")[0][1:]
        df = pd.DataFrame([map(int, r.replace("]", "").replace("[", "").split()[1:])
                           for r in rows[1:]])

        # convert frequencies to probabilities
        df = df.T

        for row in df.iterrows():
            index, r = row
            row_sum = sum(r)
            values = []
            for ri in r:
                values.append(float(ri) / max(row_sum, 1))
            df.iloc[index] = values

        # write output file
        writer = open(ppm_path, "w")
        writer.write(">" + motif_id + "\t" + motif_id + "\t" + str(threshold) + "\n")
        for row in df.iterrows():
            index, r = row
            writer.write("\t".join(map(str, r)) + "\n")
        writer.close()


    def convert_dimo_to_jaspar(self, dimo_path, jaspar_path):
        ppm_dimo = pd.read_csv(dimo_path, sep=' ', skiprows=1, header=None)
        ppm_dimo = pd.DataFrame([r.values[2:] for ri, r in ppm_dimo.iterrows()],
                                index=['A', 'C', 'G', 'T'])
        self.convert_ppm_to_jaspar_pfm(ppm_dimo, jaspar_path)

    @staticmethod
    def convert_jaspar_to_moods(pfm_jaspar, pfm_moods):
        # read file
        # print pfm_jaspar
        rows = [r.strip() for r in open(pfm_jaspar)]
        motif_id = rows[0].split("\t")[0][1:]
        df = pd.DataFrame([map(int, r.replace("]", "").replace("[", "").split()[1:])
                           for r in rows[1:]])
        df.to_csv(pfm_moods, sep="\t", index=None, header=False)

    def convert_cisbp_pfm_to_jaspar(self, cisbp_dir, output_path, stop_at=None):
        """
        Convert a set of PWMs from CISBP into JASPAR format
        """
        all_data = []
        for i, f in enumerate([f for f in listdir(cisbp_dir)]):
            if i % 100 == 0:
                print i
            header = ">" + f.replace(".pfm", "")
            next_path = join(cisbp_dir, f)
            print next_path
            lines = [r.strip() for r in open(next_path)]
            if len(lines) == 1:
                continue
            print output_path, isdir(output_path)
            if isdir(output_path):
                values = [map(lambda x: int(x * 1000), map(float, r.split("\t")[1:]))
                          for r in lines[1:]]
                output_path_jaspar = join(output_path, f.replace(".txt", ".jaspar"))
                df = pd.DataFrame(values).transpose()
                writer = open(output_path_jaspar, 'w')
                print 'writing', output_path_jaspar
                writer.write(header + "\n")
                for ri, r in df.iterrows():
                    writer.write("ACGT"[ri] + " [" + " ".join(map(str, r.values)) + "]\n")
                writer.close()
            else:
                all_data.append(header.replace(".txt", "") + "\n")
                for nt, line in zip("ACGT", lines):
                    all_data.append(
                        nt + " [" + " ".join(line.split("\t")) + "]" + "\n")
                if stop_at != None and i >= stop_at:
                    break

        if not isdir(output_path):
            writer = open(output_path, "w")
            writer.writelines(all_data)
            writer.close()

    def get_ppm(self, path):
        m = motifs.parse(open(path), "jaspar")
        pfm = m.to_dict().values()[0]
        ppm = pfm.counts.normalize(pseudocounts={'A': 0.01, 'C': 0.01,
                                                 'G': 0.01, 'T': 0.01})
        return ppm

    def convert_ppm_to_pssm(self, ppm_path_jaspar):
        '''
        Use BioPython to get an equivalent version of the program that scores
        sequences, so we can export PSSM and run our pipelines in C
        :param ppm_path_jaspar:
        :return:
        '''


    def convert_sequences_to_jaspar(self, seqs, header, output_path):
        """
        Convert a set of PWMs from CISBP into JASPAR format
        """
        all_data = []
        counts_by_position = {}
        for s in seqs:
            for i, si in enumerate(s):
                if not i in counts_by_position:
                    counts_by_position[i] = {}
                counts_by_position[i][si] = 1 if not si in counts_by_position[i] else counts_by_position[i][si] + 1

        writer = open(output_path, "w")

        writer.write(">" + header + "\n")
        for nt in 'ACGT':
            writer.write(nt + " [")
            print nt
            for i in range(len(seqs[0])):
                freq = counts_by_position[i][nt] if nt in counts_by_position[i] else 0
                writer.write(str(freq))
                if i + 1 != len(seqs[0]):
                    writer.write(" ")
            writer.write("]\n")
        writer.close()

    def convert_ppm_to_homer(self, probabilities,
                             basename=None, transpose=False,
                             threshold=-1000000000):
        """
        Convert a PPM file as define by encode to another one as defined and
        necessary to run HOMER
        """
        print basename
        # this threshold is necessary to define how low is the score we want.

        output_rows = []

        output_rows.append(">" + basename + "\t" + basename + "\t" + str(threshold))

        m = probabilities

        if transpose:
            m = m.transpose()
        for ri, r in m.iterrows():
            output_rows.append("\t".join(map(str, ["%.3f" % ri for ri in r])))
        return output_rows


    def convert_pfm_to_ppm(self, pfm):
        pfm = pd.DataFrame(pfm, columns=[i for i in range(len(pfm[0]))])
        pfm = pfm.rename({0: 'A', 1: 'C', 2: 'G', 3: 'T'})
        for i in range(pfm.shape[1]):
            pfm[i] = pfm[i] / pfm[i].sum()
        ppm = pfm
        return ppm

    def convert_moods_to_jaspar(self, moods_path, jaspar_path):
        pfm = [map(int, r.split("\t")) for r in open(moods_path)]
        ppm = self.convert_pfm_to_ppm(pfm)

        self.convert_ppm_to_jaspar_pfm(ppm, jaspar_path)

    def convert_ppm_to_jaspar_pfm(self, ppm_path_or_ppm, motif_path_jaspar,
                                  multiplier=1000, motif_id=None,
                                  append=False, cast_as_int=True,
                                  add_counts=None):
        # convert pfm to JASPAR

        df = None
        # print ppm_path_or_ppm
        if isinstance(ppm_path_or_ppm, str):
            motif_id = basename(ppm_path_or_ppm).split(".")[0]
            rows = [r for r in (gzip.open(ppm_path_or_ppm)
                                if '.gz' in ppm_path_or_ppm else open(ppm_path_or_ppm))][1:]
            lines = [[int(n * multiplier) if cast_as_int else n * multiplier
                      for n in map(float, r.split("\t"))] for r in rows]
            if len(lines[0]) == 5:
                lines = [r[1:] for r in lines]
            df = pd.DataFrame(lines, columns=['A', 'C', 'G', 'T'])
            df = df.transpose()
        else:
            if isinstance(ppm_path_or_ppm, list):
                rows = ppm_path_or_ppm
                lines = []
                for i, r in rows.iterrows():
                    values = [int(ri * multiplier) for ri in list(r)]
                    lines.append(values)

                df = pd.DataFrame(lines, columns=['A', 'C', 'G', 'T'])
                df = pd.DataFrame([list(r) for i, r in df.transpose().iterrows()])
            else:
                df = (ppm_path_or_ppm * multiplier).astype(int)


        if add_counts is not None:
            df = df + add_counts

        # print df
        if motif_id is None:
            motif_id = basename(motif_path_jaspar).split(".")[0]
        with open(motif_path_jaspar, 'w' if not append else 'a') as writer:
            writer.write(">" + motif_id + "\t" + motif_id + "\n")
            for nt, s in zip('ACGT', [map(int, v) for v in df.values.tolist()]):
                writer.write(nt + "  " + str(s).replace(",", "") + "\n")


    def convert_multijaspar_to_complementary(self, multi_jaspar_path,
                                             output_path):
        motifs_rows = [r for r in open(multi_jaspar_path)]
        motif_entries = [motifs_rows[i * 5: (i * 5) + 5] for i in
                         range(len(motifs_rows) / 5)]

        rows = []
        for entry in motif_entries:
            # for e in entry:
            #     print e,
            header, values = entry[0], entry[1:]
            values = [map(int, v.replace("]", "").split("[")[1].split(" "))[::-1] for v in values]
            # print header, values
            values = values[3], values[2], values[1], values[0]
            rows.append(header)
            for nt, v in zip('ACGT', values):
                rows.append(nt + " [" + " ".join(map(str, v)) + "]\n")
            # for r in rows:
            #     print r,
        writer = open(output_path, 'w')
        for r in rows:
            writer.write(r)
        writer.close()

    def get_ppm_from_bejerano_path(self, bejerano_path):
        lines = [line for line in open(bejerano_path)]
        motif_id, lines = lines[0].split("\t")[0][1:], lines[1:]
        rows = [map(float, r.split("\t")) for r in lines]
        for r in rows:
            print r
        df = pd.DataFrame(rows)
        df.columns = ['A', 'C', 'G', 'T']
        return motif_id, df

    def add_flanks_to_jaspar(self, jaspar_path, n, left=False, right=False):
        rows = [line.strip() for line in open(jaspar_path)]

        header = rows[0]
        values = rows[1:]
        if left:
            values = [v.replace("[", "[" + " ".join(["0" for ni in range(n)]) + " ")
                      for v in values]
        elif right:
            values = [v.replace("]", " " + " ".join(["0" for ni in range(n)])) + "]"
                      for v in values]

        output_path = tempfile.mkstemp()[1]
        with open(output_path, 'w') as writer:
            for line in [header] + values:
                writer.write(line + "\n")
            writer.close()
        return output_path

    def chop_jaspar_motif(self, jaspar_path, n, direction='right', output_dir=None):
        '''
        Remove an arbitrary number of columns from a JASPAR path either left or right
        :param jaspar_path:
        :param n:
        :param direction:
        :return:
        '''

        chopped_path = self.slide_jaspar_motif(jaspar_path,
                                               -n if direction == 'right' else n,
                                               keep_length=True, add_zeroes=False)
        motif_path = join(output_dir,
                          basename(jaspar_path) + "." + direction + "." + str(n))
        if not exists(motif_path):
            copy2(chopped_path, motif_path)

        return motif_path





    def get_motif_length_jaspar(self, jaspar_path):
        rows = [line.strip() for line in open(jaspar_path)]
        header = rows[0]
        values = rows[1:]
        values = [[ri for ri in r[3:].replace("[", "").replace("]", "").split(" ") if ri != ""]
                  for r in values]
        return len(values[0])

    def create_control_motifs(self, pfm_path, output_dir, output_basename=None):
        '''
        Reorder composite motifs using the rules defined by Jolma et al
        (three control per position, switching orientations)
        :param pfm_path:
        :param output_dir:
        :return:
        '''
        m = pd.read_csv(pfm_path, sep='\t', header=None)

        k = basename(pfm_path).replace(".ppm", "")
        ncols = m.shape[1]
        for i in range(ncols):
            # print i
            a = m[range(0, i)]
            b = m[range(i, ncols)]

            # m1 and constant m2
            if a.shape[1] < int(ncols / 3) or b.shape[1] < int(ncols / 3):
                continue

            # right and then left
            a_rev = a[a.columns[::-1]]
            b_rev = b[b.columns[::-1]]

            a_rev['reorder'] = [3, 2, 1, 0]
            a_rev = a_rev.sort_values("reorder").reset_index(drop=True)
            del a_rev['reorder']

            b_rev['reorder'] = [3, 2, 1, 0]
            # print b_rev
            b_rev = b_rev.sort_values("reorder").reset_index(drop=True)
            del b_rev['reorder']

            control1 = pd.concat([b, a], axis=1)
            control2 = pd.concat([a, b_rev], axis=1)
            control3 = pd.concat([a_rev, b], axis=1)

            # print a
            # print b
            # print a_rev
            # print b_rev
            # print 'c1 (right and then left)'
            # print control1
            # print 'c2 (left and then right complement)'
            # print control2
            # print 'c3 (left complement and then right)'
            # print control3

            for j, c in enumerate((control1, control2, control3)):
                output_filename = None
                if output_basename is not None:
                    output_filename = output_basename
                else:
                    output_filename = k
                output_filename += "_" + str(i) + "c" + str(j) + ".moods"
                c.to_csv(join(output_dir, output_filename), sep="\t",
                         rownames=False, header=False, index=None)


    def slide_jaspar_motif(self, jaspar_path, n, keep_length=True, add_zeroes=True):
        rows = [line.strip() for line in open(jaspar_path)]

        header = rows[0]
        values = rows[1:]

        values = [[ri for ri in r[3:].replace("[", "").replace("]", "").split(" ") if ri != ""]
                  for r in values]
        if n > 0:
            # left
            if add_zeroes:
                values = [["0" for ni in range(abs(n))] + v for v in values]
            # after adding two remove two to keep the length
            if keep_length:
                new_v = []
                for v in values:
                    v = v[:-abs(n)]
                    new_v.append(v)
                values = new_v
        elif n < 0:
            # right
            if add_zeroes:
                values = [v + ["0" for ni in range(abs(n))] for v in values]

            if keep_length:
                new_v = []
                for v in values:
                    v = v[-n:]
                    new_v.append(v)
                values = new_v

        output_path = tempfile.mkstemp()[1]
        with open(output_path, 'w') as writer:
            writer.write(header + "\n")
            for line, k in zip(values, "ACGT"):
                writer.write(k + " [" + " ".join(line) + "]\n")
            writer.close()
        return output_path
