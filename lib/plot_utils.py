

print('importing plot functions...')
# classic utils imports we always do
# this is activated if running in the cluster (avoid getting the DISPLAY option error)

from os import pardir
from os.path import exists, abspath, join
if exists('/g/zaugg') or exists('/mnt/znas/icb_zstore01/groups/ml01/workspace/ignacio.ibarra')\
        or exists('/mnt/c/Users/ignacio/Dropbox/'):
    import matplotlib
    matplotlib.use('Agg')
else:
    import matplotlib
    # matplotlib.use('TkAgg')

def switch_interactive_mode():
    plt.switch_backend('Tkagg')
def sim():
    switch_interactive_mode()
def switch_normal_mode():
    plt.switch_backend('Agg')
def snm():
    switch_normal_mode()

# Fix export PDF as text issue
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

from matplotlib import pyplot as plt

import seaborn as sns
import matplotlib.colors as colors
import numpy as np

# SET THE DEFAULT FONT AS ARIAL.
# IF THE FONT IS NOT ADDED, TRY REFRESHING THE FONT-CACHE WITH THE COMMAND
# import matplotlib
# matplotlib.font_manager._rebuild()

fpath = join(plt.rcParams["datapath"], "fonts/ttf/arial.ttf")
from matplotlib import font_manager as fm, rcParams
prop = fm.FontProperties(fname=fpath)
rcParams["font.family"] = "arial"


def savefig(output_basename, dpi=400, suffix_illustrator=None, **kwargs):
    pdf = kwargs.get("pdf", True)
    png = kwargs.get("png", True)

    if pdf: # save always a copy in PDF
        if '.png' in output_basename:
            for format in (".png", ".pdf"):
                print(('saving', output_basename.replace(".png", format)))
                plt.savefig(output_basename.replace(".png", format), dpi=dpi)
                print ('saved at / full path')
                p = abspath(output_basename + format).replace(".png", '')
                print (p)
                print((abspath(join(p, pardir))))
        else:
            for format in (".png", ".pdf"):
                if 'png' in format and not png:
                    continue
                if kwargs.get('log', True):
                    print(('saving', output_basename + format))
                plt.savefig(output_basename + format, dpi=dpi)
                p = abspath(output_basename + format).replace(".png", '')
                p = abspath(output_basename + format).replace(".png", '')
                p = abspath(output_basename + format).replace(".png", '')

                if kwargs.get('log', True):
                    print ('saved at / full path')
                    print (p)
                    print((abspath(join(p, pardir))))
    else:
        if '.png' in output_basename:
            for format in [".png",]:
                print(('saving', output_basename.replace(".png", format)))
                plt.savefig(output_basename.replace(".png", format), dpi=dpi)
                print ('saved at / full path')
                p = abspath(output_basename.replace(".png", format))
                print (p)
                print((abspath(join(p, pardir))))
        else:
            for format in [".png",]:
                if 'png' in format and not png:
                    continue
                print(('saving', output_basename + format))
                plt.savefig(output_basename + format, dpi=dpi)
                p = abspath(output_basename + format)
                if kwargs.get('log', True):
                    print ('saved at / full path')
                    print (p)
                    print((abspath(join(p, pardir))))

    if suffix_illustrator is not None:
        kwargs['png'] = False
        savefig(output_basename + suffix_illustrator, dpi=dpi, suffix_illustrator=None, **kwargs)

def savepng(output_basename, dpi=400, **kwargs):
    savefig(output_basename, dpi=dpi, pdf=False, suffix_illustrator=None)
def savepdf(output_basename, dpi=400, log=False, **kwargs):
    savefig(output_basename, dpi=dpi, png=False, suffix_illustrator=None, log=log)

def sendpdf(path, dest):
    print ('sending sending image...')
    from os import system
    cmd = "scp " + path + " " + dest
    print (cmd)
    system("scp " + path + " " + dest)
    print ('sending done...')

def subplots_adjust(top, bottom, left, right, hspace, wspace):
    subplots_adjust(top=top, bottom=bottom, left=left, right=right,
                        hspace=hspace, wspace=wspace)

def despine_all():
    sns.despine(offset=10, trim=True, top=True, right=True, left=True,
                bottom=True)

def set_plot_labels(**kwargs):
    xlab = kwargs.get("x")
    ylab = kwargs.get("y")
    title = kwargs.get("title")
    if xlab is not None:
        plt.xlabel(xlab)
    if ylab is not None:
        plt.ylabel(ylab)
    if title is not None:
        plt.title(title)

def scatter_simple(x, y, xlab='x', ylab='y', title='title', output_path='/tmp/scatter_simple', ylim=None, xlim=None, **kwargs):
    plt.scatter(x, y, **kwargs)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.title(title)
    if ylim is not None:
        plt.ylim(ylim)
    if xlim is not None:
        plt.xlim(xlim)
    savefig(output_path)
    plt.close()

def distplot_simple(x, output_path='/tmp/scatter_simple', rug=False, color='r'):
    sns.distplot(x, kde=True, rug=rug, color=color);
    if output_path is not None:
        savepdf(output_path)
        plt.close()


def truncate_colormap(cmap, minval=-1, maxval=1, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval,
                                            b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

def remove_top_n_right_ticks(ax):
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

def remove_all_ticks(ax):
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)

def convert_to_hex(rgba_color) :
    red = int(rgba_color.red*255)
    green = int(rgba_color.green*255)
    blue = int(rgba_color.blue*255)
    return '0x{r:02x}{g:02x}{b:02x}'.format(r=red,g=green,b=blue)

def get_legendHandle_for_second_sanity_check_plot(quantAmplifier=None,
                                                  labels=None,
                                                  values=None,
                                                  marker='^',
                                                  fmt='%.1f',
                                                  lw=2.0, min_circle_size=None, max_circle_size=None,
                                                  min_size_default=1.0,
                                                  edgecolor='black',
                                                  color='grey'):
    labels = [0.1, 0.5, 1.0, 1.5] if labels is None else labels

    print(('legends', labels))

    legendHandleList = list()
    labelSize = list()
    for i, quantMem in enumerate(values):
        v = values[i]

        if min_circle_size is not None and max_circle_size is not None:
            scaled = (quantMem - min_circle_size) / (max_circle_size - min_circle_size)
            print((quantMem, min_circle_size, max_circle_size, scaled))
            quantMem = scaled

        print (min_size_default)
        next_size = (quantMem * quantAmplifier) if not isinstance(quantMem, str) else min_size_default * quantAmplifier
        print(('next size', next_size))

        legendHandleList.append(plt.scatter([], [], s=next_size,
                                            color=color, edgecolor=edgecolor,
                                            linewidths=lw if not isinstance(lw, list) else lw[i],
                                            alpha=0.9, marker=marker))
        labelSize.append(fmt % labels[i])
    return legendHandleList, labelSize


