'''
Created on 3/2/2018

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
import PyPDF2
from PyPDF2 import PdfFileReader, PdfFileWriter


class PDFParser:

    @staticmethod
    def concatenate(input_files, output_stream, log=False):
        input_streams = []
        try:
            if isinstance(output_stream, str):
                print('opening output stream')
                print(output_stream)
                print((abspath(output_stream)))
                print('')
                output_stream = open(output_stream, 'wb')
            # First open all the files, then produce the output file, and
            # finally close the input files. This is necessary because
            # the data isn't read from the input files until the write
            # operation. Thanks to
            # https://stackoverflow.com/questions/6773631/problem-with-closing-python-pypdf-writing-getting-a-valueerror-i-o-operation/6773733#6773733
            for input_file in input_files:
                if log:
                    print(input_file)
                input_streams.append(open(input_file, mode="rb"))
            writer = PdfFileWriter()
            for input_stream in input_streams:
                reader = PdfFileReader(input_stream)
                for n in range(reader.getNumPages()):
                    writer.addPage(reader.getPage(n))
            writer.write(output_stream)
        finally:
            for f in input_streams:
                f.close()
        if isinstance(output_stream, str):
            print('output attempted to be written at...')
            print((abspath(output_stream)))

    @staticmethod
    def concatenate_from_directory_png(basedir, output_stream, x=None, y=None, w=0, h=0, stopat=None):
        from fpdf import FPDF
        pdf = FPDF()
        # imagelist is the list with all image filenames
        images_paths = [join(basedir, f) for f in listdir(basedir)]
        for i, image in enumerate(images_paths):
            if stopat is not None and i >= stopat:
                break
            print(image)
            pdf.add_page()
            pdf.image(image, x=x, y=y, h=h, w=w, type='png')
        pdf.output(output_stream, "F")

    @staticmethod
    def concatenate_from_directory(basedir, output_stream, query_basename=None):
        if query_basename is None:
            paths = [join(basedir, f) for f in listdir(basedir)]
        else:
            paths = [join(basedir, f) for f in listdir(basedir) if f.startswith(query_basename)]
        PDFParser.concatenate(paths, output_stream)

    @staticmethod
    def convert_pdf_to_svg(pdf_path):
        svg_path = pdf_path.replace(".pdf", '.svg')
        assert pdf_path != svg_path

    @staticmethod
    def optimize_pdf(pdf_path):
        output_path = pdf_path.replace(".pdf", '_scouroptimized.pdf')
        assert pdf_path != output_path
        cmd = ' '.join(['scour', '-i', pdf_path, '-o', output_path])

    @staticmethod
    def split(input_path):
        filename = basename(input_path).replace(".pdf", '')
        inputpdf = PdfFileReader(open(input_path, "rb"))
        for i in range(inputpdf.numPages):
            output = PdfFileWriter()
            output.addPage(inputpdf.getPage(i))
            output_path = join(input_path.replace(".pdf", "_page_%s.pdf" % i))
            with open(output_path, "wb") as outputStream:
                output.write(outputStream)

    @staticmethod
    def rotate(pdf_path, degrees=None):
        assert degrees is not None
        pdf_in = open(pdf_path, 'rb')
        pdf_reader = PyPDF2.PdfFileReader(pdf_in)
        pdf_writer = PyPDF2.PdfFileWriter()

        for pagenum in range(pdf_reader.numPages):
            page = pdf_reader.getPage(pagenum)
            page.rotateClockwise(-degrees)
            pdf_writer.addPage(page)

        pdf_out = open(pdf_path.replace(".pdf", "_rotated.pdf"), 'wb')
        pdf_writer.write(pdf_out)
        pdf_out.close()
        pdf_in.close()