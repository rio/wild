'''
Created on Jan 29, 2016


@author: ignacio
'''
import itertools
from math import sqrt
from numpy import average
import random
from lib.pyroc import ROCData
from os import listdir
from os.path import join, exists

import gzip
from lib.variables import *
import pandas as pd
import numpy as np

class BindingSitesAnalyzer:
    def __init__(self):
        # load shape ooo
        self.table = self.get_shape_values_table_from_file()
        self.max_min = self.set_max_min_values()
        
    def get_motif_from_multifasta(self, fastas, i, start, end, forward=True):
        """
        Get a substring from start to end, located in the i-th fasta element
        """
        header, seq = fastas[i]
        if not forward:
            return self.get_complementary_seq(seq[start:end])
        return seq[start: end]

    def get_dna_shape_augmented_distance(self, target_shape_profile,
                                         reference_shape_profile):
        """
        it returns a distance between a single motif and a target shape profile
        
        # the returned vector is composed of five features:
        [HAMMING_DISTANCE, LONGEST_IDENTICAL_MOTIF, MGW_RMS, PROT_RMS, ROLL_RMS, HELT_RMS]
        """
        output_vector = []
        target_seq, target_shapes = target_shape_profile
        reference_seq, ref_shapes = reference_shape_profile
        
        # sequence distance
        output_vector.append(self.get_mismatches(target_seq, reference_seq))
        # best local motif
        output_vector.append(self.get_longest_local_match(target_seq, reference_seq))
        
        # shape distance
        for k in ["MGW", "ProT", "Roll", "HelT"]:
            output_vector.append(self.get_rms(target_shapes[k], ref_shapes[k]))

        return output_vector
        
    def get_cisbp_motif_ids(self, tf_id):
        """
        Return motif IDs that belong to a given query
        """
        motifs_table_path = join("/home/ignacio/Documents/zaugglab",
                                 "Homo_sapiens_2015_06_12_9-24_am/TF_Information_all_motifs.txt")
        rows = [r.split("\t") for r in open(motifs_table_path)]
        filtered_rows = [r for r in rows if r[6] == tf_id and r[8] == "D"]
        return {r[3]: r for r in filtered_rows}
        
    def get_distance_to_sequences(self, target_shape, ref_shapes, best=False):
        """
        get the sum of distances between a sequences a and a set of sequences
        
        if best == True, return the min distance, otherwise the sum of distances
        """
        distances = [self.get_dna_shape_augmented_distance(target_shape, b) for b in ref_shapes]
        # select the one with the lowest number of mismatches, and lower rms sum
        if best:
            min_mismatches = min([d[0] for d in distances])
            distances = [x for x in distances if x[0] == min_mismatches]
            best_rms = sorted(distances, key= lambda x: sum(x[1:]))[0]
            return best_rms
        return distances
     
    def get_longest_local_match(self, a, b):
        """
        Given two sequences, return the longest amount of matches that there
        exist between the two sequences (i.e. local alignment)
        """  
        max_count = 0
        count = 0 
        for ai, bi in zip(a, b):
            if ai == bi:
                count += 1
            else:
                max_count = max(max_count, count)
                count = 0
        return max_count
        
    def get_distances_table(self, multifasta, ref_shapes, output_path, overwrite=False):
        """
        Create a table for a set of binding sites being compared against a multifasta.
        Write the best distance per position
        """
        if exists(output_path) and not overwrite:
            distances_table = [r.split("\t") for r in gzip.open(output_path).readlines()[1:]]
            return [list(map(int, r[0:5])) + list(map(float, r[5:])) for r in distances_table]
            
        # CREATE TABLE de novo
        w = [len(s) for s, shapes in ref_shapes][0] # width of the motif
        fastas = self.get_fastas_from_file(multifasta)
        distances_table = []
        for ni, fasta in enumerate(fastas):
            if ni % 50 == 0:
                print(ni, "sequences analyzed...")
            h, s = fasta
            s_cmp = self.get_complementary_seq(s)
            query_shapes = self.get_shape(s)
            query_shapes_cmp = self.get_shape(s_cmp)
            for i in range(0, len(s) - w + 1):
                
                # get subsequences in current position
                si = s[i: i + w]
                si_cmp = s_cmp[i: i + w]
                
                # get shape profiles in current position
                query_shape_si = [si, {k: query_shapes[k][i: i + w] for k in query_shapes}]
                query_shape_si_cmp = [si_cmp, {k: query_shapes_cmp[k][len(s) - w - i: len(s) - i] for k in query_shapes}]

                di, di_cmp = [self.get_distance_to_sequences(x, ref_shapes, best=True)
                              for x in (query_shape_si, query_shape_si_cmp)]
                
                distances_table.append([ni, i, i + w, 1] +  di)
                distances_table.append([ni, len(s) - w - i, len(s) - i, 0] + di_cmp) 
                
            if ni == 100:
                break
        writer = gzip.open(output_path, "w")
        writer.write("\t".join(["FASTA.i", "START", "END",
                                "IS.FORWARD", "BEST.MISMATCHES", "BEST.LONGEST_MOTIF",
                                "BEST.MGW", "BEST.PROT", "BEST.ROLL", "BEST.HELT"]) + "\n")
        for t in distances_table:
            writer.write("\t".join(map(str, t)) + "\n")
        # write table to output (if it does not exist)
        writer.close()
        return distances_table
    
    def set_max_min_values(self):
        """
        Set MAX and MIN values for each shape feature according to values
        """
        max_min = {}
        for k in ("MGW", "ProT", "HelT", "Roll"):
            values = []
            for s in self.table:
                if isinstance(self.table[s][k], list):
                    values += self.table[s][k]
                else:
                    values += [self.table[s][k]]
            max_min[k] = [min(values), max(values)]
        return max_min
    
    def get_shape_from_sequences(self, sequences, normalize=False,
                                 add_second_order=True):
        """
        Iterate through file getting all possible shape values for each single
        case
        """
        return [[s, self.get_shape(s, normalize=normalize,
                                   add_second_order=add_second_order)] for s in sequences]
    
    def convert_to_shapes(self, multifasta_path, output_path, as_table=True):
        """
        Convert a multifasta into a DNA shapes dataset
        """
        fastas = self.get_fastas_from_file(multifasta_path)
        
        shapes = {}
        for header, sequence in fastas:
            shapes[header] = self.get_shape(sequence)
        
        writer = gzip.open(output_path, "w")
        for header in shapes:
            if not as_table:
                writer.write(">" + header + "\n") 
                for shape_id in ["MGW", "ProT", "HelT", "Roll"]:
                    x = shapes[header][shape_id]
                    x = ["%.2f" % xi if xi != None else "NA" for xi in x]
                    writer.write(shape_id + "\t" + ",".join(x) + "\n")
            else: 
                for shape_id in ["MGW", "ProT", "HelT", "Roll"]:
                    writer.write(header + "\t")
                    x = shapes[header][shape_id]
                    x = ["%.2f" % xi if xi != None else "NA" for xi in x]
                    writer.write(shape_id + "\t" + "\t".join(x) + "\n")
        writer.close()

    def append_shape_values_to_dataframe(self, df):
        '''
        Given a dataframe and a set of shapes previously calculated for the same,
        append the basic shape parameters to it
        :param df:
        :param shapes:
        :return:
        '''

        # this method was done for the sequences in the flanking column
        # so we verify its existence before starting
        assert 'flanking' in df

        bs_analyzer = BindingSitesAnalyzer()
        shapes = bs_analyzer.get_shape_from_sequences(df['flanking'],
                                                      add_second_order=False)
        shape_keys = list(shapes[0][1].keys())
        # print shape_keys
        labels = []
        columns = []
        for shape_k in shape_keys:
            n_cols = len(shapes[0][1][shape_k])
            # print shape_k, n_cols
            for i in range(len(shapes[0][1][shape_k])):
                values = [float(s[1][shape_k][i]) if s[1][shape_k][i] is not None
                          else None for s in shapes]
                # print len(values)
                # print values
                # print(type(values))
                column_key = shape_k + "." + str(i)
                labels.append(column_key)
                columns.append(values)


        # print len(columns)
        # print len(labels)
        matrix = np.array(columns).transpose()
        # print matrix
        df2 = pd.DataFrame(matrix, columns=labels)

        df = df.reset_index(drop=True)

        # print df
        df3 = pd.concat([df, df2], axis=1)
        return df3


    def normalize_vector(self, v, min_v, max_v):
        """
        Return a list of values between 0 and 1 for this feature
        """
        return [(vi - min_v) / (max_v - min_v) for vi in v]
    
    def get_permutations(self, k):
        bases=['A','T','G','C']
        kmers = [''.join(p) for p in itertools.product(bases, repeat=k)]
        return kmers
            
    def shuffle_seq(self, seq):
        s = [c for c in seq]
        random.shuffle(s)
        return "".join(s)
        
    def get_rms_against_profile(self, seq, shape_profile):
        shape = self.get_shape(seq)
    
        rms = 0
        for k in ["MGW", "HelT", "ProT", "Roll"]:
            rms += self.get_rms(shape[k], shape_profile[k])
        
        return rms
        
    def get_kmer_code(self, seq, k=1):
        """
        Convert a sequence into a binary code according to Zhou et al model (2015)
        
        (see SI Methods - Sequence and Shape Feature Vectors)
        
        length of output vectors:
            k1 : 4 * L
            k2 : 16 * (L - 1)
            k3 : 64 * (L - 1)
        """
        
        # get permutations of sequence
        
        #########
        # Warning: The bases are ordered in a way that the output of Feature_selection.pl
        # from Zhou and generated vectors here are identical
        #########
        kmers = self.get_permutations(k)
                
        sequence_code = []
        for i in range(0, len(seq) - k + 1):
            target_kmer = seq[i: i + k]
            code = [1 if kmer == target_kmer else 0 for kmer in kmers]
            sequence_code += code

        return sequence_code 
    
    def get_fastas_from_file(self, fasta_path, as_dict=False):
        fastas = []
        seq = None
        header = None
        for r in open(fasta_path):
            r = r.strip()
            if r.startswith(">"):
                if seq != None and header != None:
                    fastas.append([header, seq])
                seq = ""
                header = r[1:]
            else:
                if seq != None:
                    seq += r
                else:
                    seq = r
        
        # append last fasta read by method
        fastas.append([header, seq])
        
        if as_dict:
            return {h: s for h, s in fastas}

        return fastas

    def get_fimo_hits_from_output_path(self, output_path):
        hits = [r.strip().split("\t") for r in gzip.open(output_path)]
        for i, h in enumerate(hits):
            is_forward = h[4] != "-"
            is_reverse = h[5] != "-"
            position = int(h[2])
            scores = [float(h[j]) if h[j] != "-" else None for j in (4, 5)] 
            hits[i] = [h[1], position, h[3], is_forward, is_reverse] + scores
        return hits

    def get_motif_width(self, motif_path):
        """
        It returns the width  of the analyzed motifs, as an integer
        The "10" number permits to filter empty lines 
        """
        return len([r for r in open(motif_path) if len(r) > 10])
    
    def scramble_fasta_sequences(self, input_path, output_path, overwrite=False):
        """
        Randomize fasta sequences and generate a new file with the output
        """
        if exists(output_path) and not overwrite:
            return
        
        # read fasta
        fastas = self.get_fastas_from_file(input_path)
        
        # randomize
        fastas = [[h, ''.join(random.sample(s, len(s)))] for h, s in fastas]
        
        # write output
        writer = open(output_path, "w")
        for h, s in fastas:
            writer.write(">" + h + "\n")
            writer.write(s + "\n")
        writer.close()

    def save_shape_profile(self, shapes, output_path):
        """
        Given a set of shape profiles, save the average by column
        """
        writer = open(output_path, "w")
        for k, shape_k, in zip(["mgw", "prot", "roll", "helt"],
                               ["MGW", "ProT", "Roll", "HelT"]):
            rows = [r[shape_k] for r in [s[1] for s in shapes]]            
            # get average
            by_column = [[] for i in range(len(rows[0]))]
            for i in range(len(rows[0])):
                for j in range(len(rows)):
                    by_column[i].append(rows[j][i])
            
            averages = [average(c) if c[0] != None else None for c in by_column]
            writer.write(shape_k + "\t" + "\t".join(map(str, averages)) + "\n")
        writer.close()

    def get_shape_profile(self, sequences):
        """
        Create shape profile using a set of sequences
        """      
        shapes = self.get_shape_from_sequences(sequences)
        averages_by_feature = {}
        for shape_k in ["MGW", "ProT", "Roll", "HelT"]:
            rows = [r[shape_k] for r in [s[1] for s in shapes]]            
            # get average
            by_column = [[] for i in range(len(rows[0]))]
            for i in range(len(rows[0])):
                for j in range(len(rows)):
                    by_column[i].append(rows[j][i])
            
            averages = [average(c) if c[0] != None else None for c in by_column]
            averages_by_feature[shape_k] = averages
        return averages_by_feature
        
    def read_shape_profile(self, profile_path):
        """
        Given a reference shape profile, retrieve values as a dictionary
        """
        shapes = {}
        for r in open(profile_path):
            r = r.strip().split("\t")
            k, values = r[0], r[1:]
            values = [float(vi) if vi != "None" else None for vi in values]
            shapes[k] = values
        return shapes 
        
    def get_complementary_nt(self, nt):
        if nt == "A":
            return "T"
        if nt == "T":
            return "A"
        if nt == "C":
            return "G"
        if nt == "G":
            return "C"
        if nt == "N":
            return "N"
        
    def get_complementary_seq(self, s):
        return "".join([self.get_complementary_nt(nt) for nt in s][::-1])
    
    def get_shape(self, sequence, normalize=False,
                  add_second_order=True):
        """
        It returns eight vectors
            - four for single shape models
            - four for 1st+2nd shape parameters (suffix "1st+2nd")
        """
        t = self.table
        vectors = {k: [] for k in ("MGW", "ProT", "Roll", "HelT")}
        
        for i in range(len(sequence)):
            s = sequence[i: i + 5]
            next_s = sequence[(i + 1): (i + 1) + 5]

            if len(s) < 5:
                break

            s_has_n = "N" in s
            next_s_has_n = "N" in next_s
            
            # MGW and ProT are easy: grab values from table
            vectors["MGW"].append(t[s]["MGW"] if s in t else None)
            vectors["ProT"].append(t[s]["ProT"] if s in t else None)
            
            # Roll and HelT require taking averaging values. Three conditions apply:
            # 1) first value: take the first base step value from 1st pentamer
            if i == 0:
                vectors["Roll"].append(t[s]["Roll"][0])
                vectors["HelT"].append(t[s]["HelT"][0])                
            
            # 2) second and n-th value: average 2nd from current with 1st from next
            # 3) last value: take the second value from the last pentamer
            if len(next_s) == 5:
                vectors["Roll"].append((t[s]["Roll"][1] + t[next_s]["Roll"][0]) / 2)
                vectors["HelT"].append((t[s]["HelT"][1] + t[next_s]["HelT"][0]) / 2)
            else:
                vectors["Roll"].append(t[s]["Roll"][1])
                vectors["HelT"].append(t[s]["HelT"][1])

        if normalize:
            for i, k in enumerate(("MGW", "ProT", "Roll", "HelT")):
                max_v, min_v = self.max_min[k]
                vectors[k] = self.normalize_vector(vectors[k], max_v, min_v)
            
        # add second order vectors for each shape values
        if add_second_order:
            for i, k in enumerate(("MGW", "ProT", "Roll", "HelT")):
                k2 = k + "1st+2nd"
                x = vectors[k]
                vectors[k2] = [x[i] * x[i + 1] for i in range(len(x) - 1)]

        for i, k in enumerate(("MGW", "ProT", "Roll", "HelT")):
            none_v = [None, None] if i < 2 else [None]
            vectors[k] = none_v + vectors[k] + none_v
            
        return vectors
    
    def get_rms(self, a, b, remove_none=True):
        not_none_positions = [1 if ai != None and bi != None else 0 for ai, bi in zip(a, b)]
        a = [ai for ai, flag in zip(a, not_none_positions) if flag]
        b = [bi for bi, flag in zip(b, not_none_positions) if flag]
        assert len(a) == len(b)

        return sqrt(sum([(ai - bi) ** 2 for ai, bi in zip(a, b)]) / len(a))
    
    def calculate_roc_performance(self, table_positive, table_negative,
                                  table_output_path=None):
        """
        Check the ability to classify between a positive and a negative peak
        using classifier taken from sequence and shape
        """
        
        # sequence only: add the best sequence score for both mismatches and
        # key: Fasta ID (position in multifasta)
        # values: positive-best and negative-best 
        scores = {r[0] : [] for r in table_positive}
        
        if table_output_path is not None:
            writer = open(table_output_path, "w")
            
        for k in scores:
            pos = [x for x in table_positive if x[0] == k]
            neg = [x for x in table_negative if x[0] == k]
            best_pos_seq = min([r[4] + -r[5] for r in pos])
            best_neg_seq = min([r[4] + -r[5] for r in neg])
            # best_pos_seq = min([-r[5] for r in pos])
            # best_neg_seq = min([-r[5] for r in neg])
            
            best_pos_shape = min([sum(r[6:]) for r in pos])
            best_neg_shape = min([sum(r[6:]) for r in neg])

            best_pos_seq_n_shape = min([sum([r[4], r[5]] + r[4:]) for r in pos])
            best_neg_seq_n_shape = min([sum([r[4], r[5]] + r[4:]) for r in neg])
                        
            scores[k] = [best_pos_seq, best_neg_seq,
                         best_pos_shape, best_neg_shape,
                         best_pos_seq_n_shape, best_neg_seq_n_shape]
            # write values to file
            if table_output_path is not None:
                writer.write("\t".join(map(str, [1, best_pos_seq, best_pos_shape])) + "\n")
                writer.write("\t".join(map(str, [0, best_neg_seq, best_neg_shape])) + "\n")
        if table_output_path is not None:
            writer.close()

        # sequence only
        roc1 = ROCData([[1, -scores[k][0]] for k in scores] +
                      [[0, -scores[k][1]] for k in scores])
        # shape only
        roc2 = ROCData([[1, -scores[k][2]] for k in scores] +
                      [[0, -scores[k][3]] for k in scores])
        # sequence + shape (sum)
        roc3 = ROCData([[1, -scores[k][4]] for k in scores] +
                      [[0, -scores[k][5]] for k in scores])
        
        print("SEQ", roc1.auc())
        print("SHAPE", roc2.auc())
        print("SEQ_N_SHAPE", roc3.auc())
           
        import sys; sys.exit()
        
    def scan_shape_profile_through_sequences(self, shape_profile, fastas,
                                             n=None):
        """
        Scan a shape profile across a set of sequences. Save RMS values
        """
        shape_profile_width = len(shape_profile[list(shape_profile.keys())[0]])
        
        shape_keys = "MGW", "HelT", "ProT", "Roll"
        rms_values = []
        counter = 0
        for header, seq in fastas:
            seq_reverse = self.get_complementary_seq(seq)
            shape_forward = self.get_shape(seq)
            shape_reverse = self.get_shape(seq_reverse)
            
            for i in range(0, len(seq) - shape_profile_width):
                start, end = i, i + shape_profile_width + 1
                s = seq[start: end] 
                s_reverse = self.get_complementary_seq(s)
                
                shape_query_forward = {k: shape_forward[k][start: end] for k in shape_forward}
                shape_query_reverse = {k: shape_reverse[k][start: end] for k in shape_reverse}
                    
                total_rms_fwd = 0
                for k in shape_keys:
                    a, b = shape_query_forward[k], shape_profile[k]
                    total_rms_fwd += self.get_rms(a, b, remove_none=True)
                total_rms_reverse = 0
                for k in shape_keys:
                    a, b = shape_query_reverse[k], shape_profile[k] 
                    total_rms_reverse += self.get_rms(a, b, remove_none=True)
                
                rms_values.append([header, i, "+", s, total_rms_fwd])
                rms_values.append([header, i, "-", s_reverse, total_rms_reverse])
            counter += 1
            if n != None and counter > n:
                break
        return rms_values
    
    def convert_pwm_to_pwm(self, input_dir=None, output_dir=None):
        
        # default variables
        cisbp_dir = "/home/ignacio/Documents/zaugglab/Homo_sapiens_2015_06_12_9-24_am/pwms_all_motifs"
        cisbp_output = "/home/ignacio/Documents/zaugglab/Homo_sapiens_2015_06_12_9-24_am/pfms_all_motifs"
        
        if input_dir == None:
            input_dir = cisbp_dir
        if output_dir == None:
            output_dir = cisbp_output

        counter = 0
        for f in listdir(input_dir):
            path = join(input_dir, f)
            rows = [r.strip().split("\t")[1:] for r in open(path)][1:]
            A, C, G, T = [[r[i] for r in rows] for i in range(0, 4)]
        
            output_path = join(output_dir, f.replace(".txt", ".pfm"))
            writer = open(output_path, "w")
            frequencies = [list(map(float, x)) for x in [A, C, G, T]]

            # convert probs to counts, on a column based manner
            counts_by_row = [[] for i in range(4)]
            for j in range(len(frequencies[0])):
                probabilities = [frequencies[i][j] for i in range(4)]
                counts_by_columns = self.probabilities_to_counts(probabilities)
                [counts_by_row[i].append(counts_by_columns[i]) for i in range(4)]
                                          
            [writer.write("\t".join(map(str, row)) + "\n") for row in counts_by_row]
            writer.close()
            counter += 1
            
    def convert_encode_ppm_to_pfm(self, input_path, output_dir):        
        input_path = "/home/ignacio/Documents/zaugglab/encode_motifs/motifs.txt"        
        if output_dir == None:
            output_dir = "/home/ignacio/Documents/zaugglab/encode_motifs/pfm_encode"

        rows = [r for r in open(input_path)]
        
        counter = 0
        for f in listdir(input_dir):
            path = join(input_dir, f)
            rows = [r.strip().split("\t")[1:] for r in open(path)][1:]
            A, C, G, T = [[r[i] for r in rows] for i in range(0, 4)]
        
            output_path = join(output_dir, f.replace(".txt", ".pfm"))
            writer = open(output_path, "w")
            frequencies = [list(map(float, x)) for x in [A, C, G, T]]

            # convert probs to counts, on a column based manner
            counts_by_row = [[] for i in range(4)]
            for j in range(len(frequencies[0])):
                probabilities = [frequencies[i][j] for i in range(4)]
                counts_by_columns = self.probabilities_to_counts(probabilities)
                [counts_by_row[i].append(counts_by_columns[i]) for i in range(4)]
                                          
            [writer.write("\t".join(map(str, row)) + "\n") for row in counts_by_row]
            writer.close()
            counter += 1


    def probabilities_to_counts(self, probabilities, sum_counts=10000):
        counts = []
        remaining_counts = sum_counts
        for i, p in enumerate(probabilities):
            curr_counts = min(int(p * sum_counts), remaining_counts)
            
            # last column: add the remaining reads (normally 1-3 more)
            if i == 3:
                curr_counts = remaining_counts

            remaining_counts -= curr_counts
            counts.append(curr_counts)
        return counts            
    
    def get_hits_per_id(self, distances_table):
        """
        Return the amount of times a hit is repeated in our table (column 1)
        e.g. motifs per peak
        """
        hits_per_id = {}
        for t in distances_table:
            if not t[0] in hits_per_id:
                hits_per_id[t[0]] = 1
            else:
                hits_per_id[t[0]] += 1
        return hits_per_id

    def print_cytoscape_table(self, a, b, n_mismatches=1):
        """
        print as Cytoscape table. Distances indicate mismatches and 
        SEQ1 indicates if the elements belongs to the first dataset (i.e. 'a')
        """
        sequences = [p for p in a] + [m for m in b]
        sequences = [[i, p] for i, p in enumerate(sequences)]
        
        print("\t".join(["ID1", "SEQ1", "ID2", "SEQ2", "D"]))
        for i, s1 in sequences:
            for j, s2 in sequences:
                if i <= j:
                    continue
                mismatches = self.get_mismatches(s1, s2)
                if mismatches <= n_mismatches:
                    print("\t".join(map(str, [i, int(s1 in a),
                                              j, int(s2 in b), n_mismatches])))
                     
    def get_mismatches(self, a, b, as_string=False):
        """
        Return the amount of differences between two strings. If as_string is True,
        it returns a list of 0's (the same) and 1's (mismatch)
        """
        assert len(a) == len(b)
        v = [0 if ai == bi else 1 for ai, bi in zip(a, b)]
        return v if as_string else sum(v)
        
    def get_shape_values_table_from_file(self, table_path="shape_values_dnashapeR.txt"):
        """
        Returns dictionary with all shape values for each DNA pentamer
        """
        dna_shape_table_path = join("../data/shape_values_dnashapeR.txt")
        assert exists(dna_shape_table_path)
        table = {}
        rows = [r.strip().replace("  ", " ") for r in open(dna_shape_table_path)]
        rows = [[r[0], [x for x in r[1:] if len(x) > 0]] for r in [r.split(" ") for r in rows]]
        for seq, values in rows:
            p = (6, 0, 15, 18, 21, 24) # positions for retrieving values from the table (fixed)
            prot, mgw, roll1, roll2, helt1, helt2 = list(map(float, [values[i] for i in p]))
            table[seq] = {"MGW": mgw, "ProT": prot, 
                          "Roll": [roll1, roll2], "HelT": [helt1, helt2]}
            # for the complementary sequence, roll and helt values are swapped
            table[self.get_complementary_seq(seq)] = {"MGW": mgw, "ProT": prot,
                                                      "Roll": [roll2, roll1],
                                                      "HelT": [helt2, helt1]}
        return table
        

