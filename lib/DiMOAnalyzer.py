
import os
from .path_functions import *
from lib.FastaAnalyzer import FastaAnalyzer
import pandas as pd

class DiMOAnalyzer:

    def optimize_ppm(self, pos_seqs, neg_seqs,
                     ppm_path,
                     tmp_script_name=join(RSCRIPTS_DIR, "dimo_r_script.R"),
                     working_dir='../tmp',
                     output_basename = 'output_TF'):
        '''
        It runs the DiMO algorithm for AUC optimization between (+) and (-)
        sequences. It writes out an optimized version of our PWM.

        Default parameters have to be checked (check ?DiMO in R for more info).
        :param pos_seqs_fasta_path:
        :param neg_seqs_fasta_path:
        :param pwm_path:
        :param output_basename:
        :return: The path to the optimized PWM (check if exists after executing)
        '''


        if not exists(working_dir):
            mkdir(working_dir)

        all_seqs = [pos_seqs, neg_seqs]
        labels = ['pos', 'neg']
        for seqs, label in zip(all_seqs, labels):
            # print label
            if isinstance(seqs, list):
                # load no more than 1000 seqs (DiMO runs weirdly)
                # seqs = seqs[:1000]
                fasta_analyzer = FastaAnalyzer()
                tmp_dir = '/tmp' if not 'TMPDIR' in os.environ else os.environ['TMPDIR']
                fasta_analyzer.write_fasta_from_sequences(seqs,
                                                          join(tmp_dir, label + '.fa'),
                                                          uppercase=True)

        paths = [join(tmp_dir, label + '.fa') for label in labels]
        # print paths

        output_basename = join(working_dir, output_basename)
        cmd_exec = " ".join(['Rscript', join(RSCRIPTS_DIR, 'dimo_r_script.R'),
                            paths[0], paths[1], ppm_path, output_basename])

        print(cmd_exec)
        # run command



        # do not run DiMo for now
        return


        os.system(cmd_exec)

    def convert_ppm_homer_to_dimo(self, input_ppm_path, output_ppm_path):
        '''
        Convert between HOMER and DiMO formats (PPMs).
        :param input_ppm_path:
        :param output_ppm_path:
        :return:
        '''
        print('converting ppm from HOMER to DiMO format...')
        rows = [r.strip().split("\t") for r in open(input_ppm_path)]
        matrix = pd.DataFrame([r for r in rows[1:]]).transpose()

        header = rows[0][0].replace(">", "")
        with open(output_ppm_path, 'w') as writer:
            writer.write("> " + header + "\n")
            for r, nt in zip(matrix.iterrows(), 'ACGT'):
                index, value = r
                writer.write(nt + " |\t" + "\t".join(map(str, value.tolist())) + "\n")

    def convert_ppm_dimo_to_homer(self, input_ppm_dimo_path, output_ppm_jaspar_path,
                                   header=None):
        '''

        It writes an HOMER2 input file using as format
        :param input_ppm_path:
        :param output_ppm_path:
        :return:
        '''

        rows = [r.strip() for r in open(input_ppm_dimo_path)]
        header, rows = rows[0].replace(">", "").strip() if header is None else header, rows[1:]
        matrix = [[ri for ri in r.split("|")[1].split(" ") if len(ri) != 0] for r in rows]
        matrix = pd.DataFrame(matrix).transpose()
        with open(output_ppm_jaspar_path, 'w') as writer:
            writer.write(">" + header + "\t" + header + "\t-10000000000\n")
            for r in matrix.iterrows():
                index, value = r
                writer.write("\t".join(value.tolist()) + "\n")