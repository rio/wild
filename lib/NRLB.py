'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *

class NRLB():

    @staticmethod
    def run(data_config_path,
            nrlb_config_path='/g/scb2/zaugg/rio/EclipseProjects/zaugglab/NRLB/NRLBConfig.txt',
            working_dir='/g/scb2/zaugg/rio/EclipseProjects/zaugglab/NRLB',
            output_dir=None):

        lastwd = abspath(os.curdir)
        chdir(working_dir)

        if output_dir is not None:
            # copy the binaries and paths into the output directory
            if not exists(output_dir):
                makedirs(output_dir)
            for k in ['tmp', 'R0Models', 'NRLBModels']:
                if not exists(join(output_dir, k)):
                    mkdir(join(output_dir, k))
            copy2(nrlb_config_path, output_dir + "/.")
            copy2(data_config_path, output_dir + "/.")
            chdir(output_dir)
            system('pwd')
            for k in ['runNRLB.sh', 'bin']:
                next = join('/g/scb2/zaugg/rio/EclipseProjects/zaugglab/NRLB/', k)
                if not exists(k):
                    system('ln -s ' + next + ' .')

        print('Running NRLB with following parms')
        print('DATA args:', data_config_path)
        print('NRLB args', nrlb_config_path)

        system('./runNRLB.sh ' + data_config_path + " " + nrlb_config_path)
        chdir(lastwd)
        # prepare a proper background using the generic JAVA file


    @staticmethod
    def prepare_data_config(prot_name, fg, bg, variable_region_length=20, kmin=1, kmax=4,
                            left_flank=None, right_flank=None, overwrite=True,
                            data_config_output='/g/scb2/zaugg/rio/EclipseProjects/zaugglab/NRLB/DataConfig_userdefined.txt'):

        if left_flank == None and right_flank == None:
            print('please provide flank sequences.')
            assert left_flank != None and right_flank != None

        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/NRLB/DataConfig_generic.txt'

        lines = [r for r in open(p)]
        flags = {'$PROTNAME': prot_name,
                 '$R0PATH': bg,
                 '$R1PATH': fg,
                 '$OVERWRITE': str(overwrite).lower(),
                 '$VARREGIONLENGTH': str(variable_region_length),
                 '$R0MINK': str(kmin),
                 '$R0MAXK': str(kmax),
                 '$LEFTFLANK': left_flank,
                 '$RIGHTFLANK': right_flank}

        for k in flags:
            lines = [r.replace(k, flags[k]) for r in lines]
        writer = open(data_config_output, 'w')
        for r in lines:
            writer.write(r)
        writer.close()

        return data_config_output


    @staticmethod
    def prepare_nrlb_config(nmodes=1,
                            nrlb_config_output='/g/scb2/zaugg/rio/EclipseProjects/zaugglab/NRLB/NRLBConfig_userdefined.txt'):
        p = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/NRLB/NRLBConfig_generic.txt'

        nrlb_config_output = nrlb_config_output.replace(".txt", "_M%i.txt" % nmodes)
        lines = [r for r in open(p)]
        flags = {'$NMODES': str(nmodes)}
        for k in flags:
            lines = [r.replace(k, flags[k]) for r in lines]
        writer = open(nrlb_config_output, 'w')
        for r in lines:
            writer.write(r)
        writer.close()

        return nrlb_config_output



