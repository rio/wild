package printKmers;

import java.util.HashMap;

public class Kmer {
	byte length;
	int rep;

	public Kmer(int length, int rep) {
		this.length = (byte) length;
		this.rep = rep;
	}

	public Kmer(String repr) {
		length = (byte) repr.length();
		rep = Letters.getInt(repr);
	}

	public int getCount(HashMap<Kmer, int[]> counters, int cycle) {
		if (counters == null) System.out.println(" "+ counters + " "+ this.rep);
		int[] counts = counters.get(this);
		int[] RCcounts = counters.get(reverse_comp());
		int rc = 0;
		if (counts != null) rc += counts[cycle*2]+counts[cycle*2+1];
		if (RCcounts != null /*&& reverse_comp().toString().compareTo(toString()) != 0*/) rc += RCcounts[cycle*2]+RCcounts[cycle*2+1];
		return rc;
	}
	
	public int getRealCount(HashMap<Kmer, int[]> counters, int cycle) {
		int[] counts = counters.get(this);
		if (counts != null) return counts[cycle*2]+counts[cycle*2+1];
		return 0;
	}
	
	public double getNonZeroFreq(HashMap<Kmer, int[]> counters, int[] total, int cycle) {
		return (double)(getCount(counters, cycle)+2) / (double)(2*(total[cycle*2]+total[cycle*2+1]+Math.pow(4, length)));
	}
	
	public double getEstimatedRatio(HashMap<Kmer, int[]> counters, int[] total, int cycle, double[][] markovModel, 
			HashMap<Kmer, int[]> mm_counters, int[] mm_total, int order) {
		if (counters == null) System.out.println(counters);
		double ratio = getNonZeroRealFreq(counters, total, cycle) / getEstimatedFreq(mm_counters, mm_total, markovModel, order);
		return Math.pow(ratio, 1.0/(double)cycle);
	}

	public double getEstimatedRealFreq(HashMap<Kmer, int[]> counters, int[] total, double[][] markovModel, int order) {
		double rc = getNonZeroFreq(counters, total, 0);
		for (int i = order; i < toString().length(); i++) {
			rc *= markovModel[new Kmer(toString().substring(i-order,i)).rep][Letters.char_to_int(toString().charAt(i))];
		}
		return rc;
	}
	
	public double getEstimatedFreq(HashMap<Kmer, int[]> counters, int[] total, double[][] markovModel, int order) {
		if (Letters.isPalindrome(toString())) return getEstimatedRealFreq(counters, total, markovModel, order);
		if (counters == null) System.out.println(counters);
		return getEstimatedRealFreq(counters, total, markovModel, order) + reverse_comp().getEstimatedRealFreq(counters, total, markovModel, order);
	}
	
	public double getNonZeroRealFreq(HashMap<Kmer, int[]> counters, int[] total, int cycle) {
		return (double)(getRealCount(counters, cycle)+1) / (double)((total[cycle*2]+total[cycle*2+1]+Math.pow(4, length)));
	}	
	
	public Kmer reverse_comp() {
		return new Kmer(length, reverse_comp_rep());
	}

	public int reverse_comp_rep() {
		int copy = ~rep;
		int comp_rep = 0;
		for (int i = 0; i < length; i++) {
			comp_rep <<= 2;
			comp_rep |= (copy & 3);
			copy >>= 2;
		}
		return comp_rep;
	}

	@Override
	public String toString() {
		int copy = rep;
		String Line = "";
		for (int i = 0; i < length; i++) {
			byte num = (byte) (copy & 3);
			Line = Letters.byte_to_char(num) + Line;
			copy >>= 2;
		}
		return Line;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() != this.getClass())
			return false;
		Kmer other = (Kmer) obj;

		if (rep != other.rep)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return rep;
	}

}
