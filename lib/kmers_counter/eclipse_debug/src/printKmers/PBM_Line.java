package printKmers;
import java.util.Vector;
import java.util.HashMap;

public class PBM_Line implements Comparable<PBM_Line> {
		String line;
		double score;
		double intensity;
		int rank;
		boolean rc;

		// Simple constructor for line and rank
		public PBM_Line(String line, int rank) {
			this.line = line;
			this.rank = rank;
		}

		// Another simple constructor
        public PBM_Line(String line, double intensity, int rank,  Vector<double[][]> matrix) {
        	this.line = line;
            this.intensity = intensity;
            this.rank = rank;
        }
		
    	private void exit(){
    		System.exit(0);
    	}
    	
    	private void print(Object o){
    		System.out.println(o);
    	}
        // Return the best alignment of consensus, with flanks1 positions on the flanks
        // Up to the number of give mismatches
        // If filter, ignore if number of occurences > 1
		public int setAlign(String consensus, int flanks1, int mismatch, boolean filter) {
			double max = 0; int imax = 0; boolean temp_rc = false;
			int count = 0;
			
			// this variable is called middle and it map apparently the mid position of our string...
			int middle = (int)Math.floor((line.length()-consensus.length())/2);
			
			// e.g. if read length is 20 and core len is 6, then it is 7 
			
			//print(middle);
			for (int i = 0; i <= line.length()-consensus.length(); i++) {
				double matches = Letters.countMatches(line.substring(i,i+consensus.length()), consensus);
				
				// increase a basic counter if the amount of matches is greater than the threshold
				if (matches >= consensus.length() - mismatch)
					count++;
				
				// it seems to select arbitrarily the last read.
				if (matches > max || (matches == max && Math.abs(imax-middle) > Math.abs(i-middle))) {
					max = matches; imax = i;
				}
			}

			// if the amount of sites in fwd was less than two, then we accept this read
			if (count < 2)
				count = 0;

			String lll = Letters.reverse_comp(line);
			for (int i = 0; i <= line.length()-consensus.length(); i++) {
				// print(lll.substring(i,i+consensus.length()));
				double matches = Letters.countMatches(lll.substring(i,i+consensus.length()), consensus);
				if (matches >= consensus.length() - mismatch)
					count++;
				if (matches > max || (matches == max && Math.abs(imax-middle) > Math.abs(i-middle))) {
					max = matches; imax = i; temp_rc = true;
				}
			}
			this.rank = imax;
			this.rc = temp_rc;
			
			// the best score in this sequence is not enough, or we have two matches
			if (max < consensus.length() - mismatch || (filter && count > 1))
				this.rank = Integer.MIN_VALUE;

			return count;
		}

		// Merge reverse complement k-mers
		public static PBM_Line[] mergeRC(PBM_Line[] kmers) {
			HashMap<String, Integer> converged = new HashMap<String, Integer>();
			for (int i = 0; i < kmers.length; i++) {
				if (converged.containsKey(kmers[i].line)) System.out.println("Something is wrong!!");
				if (converged.containsKey(Letters.reverse_comp(kmers[i].line)))
					converged.put(Letters.reverse_comp(kmers[i].line), converged.get(Letters.reverse_comp(kmers[i].line)) + kmers[i].rank);
				else converged.put(kmers[i].line, kmers[i].rank);
			}
			PBM_Line[] newKmers = new PBM_Line[converged.size()];
			String[] keys = converged.keySet().toArray(new String[0]);
			for (int i = 0; i < keys.length; i++)
				newKmers[i] = new PBM_Line(keys[i], converged.get(keys[i]));
			return newKmers;
		}
		

		@Override
		public String toString() {
			return line;
		}

		@Override
		public int compareTo(PBM_Line o) {
			return (int) Math.signum(o.score - score);
		}

	}
