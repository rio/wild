package printKmers;

import java.util.Vector;

public class Letters {

	public static boolean validLine(String line) {
		boolean valid = true;
		for (int i = 0; i < line.length() && valid; i++)
			valid = valid && validLetter(line.charAt(i));
		return valid;
	}
	
	public static int countMatches(String a, String b) {
		int matches = 0;
		for (int i = 0; i < Math.min(a.length(), b.length()); i++)
			matches += ((charFit(a.charAt(i),b.charAt(i))) ? 1 : 0);
		return matches;
	}

	public static boolean isPalindrome(String str) {
		if (str.compareTo(reverse_comp(str)) == 0) return true;
		return false;
	}
	
	public static int getInt(String repr) {
		int rep = 0;
		for (int i = 0; i < repr.length(); i++) {
			rep <<= 2;
			rep |= Letters.char_to_int(repr.charAt(i));
		}
		return rep;
	}

	public static String getString(int num, int k) {
		String rc = "";
		for (int i = 0; i < k; i++) {
			rc = int_to_char(num %4) + rc;
			num = num /4;
		}
		return rc;
	}
	
	private static boolean validLetter(char a) {
		if (a == 'A' || a == 'C' || a == 'G' || a == 'T')
			return true;
		return false;
	}
	
	public static int char_to_int(char ch) {
		return char_to_byte(ch);
	}

	public static byte char_to_byte(char ch) {
		switch (ch) {
		case 'A':
			return 0;
		case 'C':
			return 1;
		case 'G':
			return 2;
		case 'T':
			return 3;
		default:
			return -1;
		}
	}

	public static char int_to_char(int num) {
		return byte_to_char((byte) num);
	}

	public static char byte_to_char(byte num) {
		switch (num) {
		case 0:
			return 'A';
		case 1:
			return 'C';
		case 2:
			return 'G';
		case 3:
			return 'T';
		default:
			return ' ';
		}
	}
	
	public static char complement_char(char ch) {
		switch (ch) {
		case 'A':
			return 'T';
		case 'C':
			return 'G';
		case 'G':
			return 'C';
		case 'T':
			return 'A';
		case 'R':
			return 'Y';
		case 'Y':
			return 'R';
		case 'S':
			return 'S';
		case 'W':
			return 'W';
		case 'K':
			return 'M';
		case 'M':
			return 'K';
		case 'B':
			return 'V';
		case 'V':
			return 'B';
		case 'D':
			return 'H';
		case 'H':
			return 'D';
		case 'N':
			return 'N';
		default:
			return ' ';
		}
	}
	
	public static String reverse_comp(String str) {
		String rc = "";
		for (int i = str.length()-1; i >= 0; i--)
			rc = rc + complement_char(str.charAt(i));
		return rc;
	}
	
	public static boolean fit(String[] str1, String str2, int mismatchTh) {
		boolean fit = false;
		for (int i = 0; i < str1.length; i++)
			fit = fit || fit(str1[i], str2, mismatchTh);
		return fit;
	}
	
	public static boolean fit(String str1, String str2, int mismatchTh) {
		boolean fit = false;

		for (int l = 0; l < 2; l++) {
		String orgString = str1;
		str1 = "NN" + str1 + "NN";
		str2 = (l == 0 ? str2 : reverse_comp(str2));
		if (str1.length() >= str2.length()) {
			for (int i = 0; i < str1.length() - str2.length() + 1; i++)
				if (fit(str1, str2, i, mismatchTh)) fit = true;
		} else {
			for (int i = 0; i < str2.length() - str1.length(); i++)
				if (fit(str1, str2.substring(i, i+str1.length()), 0, mismatchTh)) fit = true;
		}
		str1 = orgString;
		}
		
		return fit;
	}
	
	public static boolean fit(String str1, String str2, int j, int mismatchTh) {
		int mismatch = 0;
		for (int i = 0; i < str2.length(); i++)
			if (!charFit(str2.charAt(i), str1.charAt(i+j))) mismatch++;
	
		return (mismatch <= mismatchTh);
	}

	public static boolean charFit(char d, char i) {
		if (d=='A' && (i=='A' || i=='W' || i=='M' || i=='R' || i=='D' || i=='H' || i=='V' || i=='N'))
			return true;
		if (d=='C' && (i=='C' || i=='S' || i=='M' || i=='Y' || i=='B' || i=='H' || i=='V' || i=='N'))
			return true;
		if (d=='G' && (i=='G' || i=='S' || i=='K' || i=='R' || i=='B' || i=='D' || i=='V' || i=='N'))
			return true;
		if (d=='T' && (i=='T' || i=='W' || i=='K' || i=='Y' || i=='B' || i=='D' || i=='H' || i=='N'))
			return true;
		return false;
	}


       public static char[] getLetters( char i) {
		Vector<Character> rc = new Vector<Character>();
                if ((i=='A' || i=='W' || i=='M' || i=='R' || i=='D' || i=='H' || i=='V' || i=='N'))
                        rc.add('A');
                if ((i=='C' || i=='S' || i=='M' || i=='Y' || i=='B' || i=='H' || i=='V' || i=='N'))
                        rc.add('C');
                if ((i=='G' || i=='S' || i=='K' || i=='R' || i=='B' || i=='D' || i=='V' || i=='N'))
                        rc.add('G');
                if ((i=='T' || i=='W' || i=='K' || i=='Y' || i=='B' || i=='D' || i=='H' || i=='N'))
                        rc.add('T');
		char[] rc2 = new char[rc.size()];
		for (int j = 0; j < rc.size(); j++) rc2[j] = rc.get(j);
                return rc2;
        }



}
