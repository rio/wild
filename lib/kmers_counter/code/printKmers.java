package printKmers;

import java.io.IOException;

public class printKmers {

	public static void main(String[] args) throws IOException {
		System.out.println(
				"USAGE: java -jar printKmers.jar <output filename> "
				+ "<core_sequence> <cycle> <flanks> <th1> <th2> "
				+ "<a list of HT-SELEXfiles ordered from first cycle>");
		
		int k_max = 8; int k_min = 4; int numarg = 6;
        String[] argv = new String[args.length-numarg];
        for (int i = numarg; i < args.length; i++) argv[i-numarg] = args[i];

        HT_SELEX selex_data = new HT_SELEX(k_max, k_min, argv);

        for (int curr_k = k_max; curr_k >= k_min; curr_k--) {
            if (!selex_data.count_Kmers(curr_k, null)) {
                    System.out.println("ERROR : int countin kmers!");
                    return;
            }
        }

        int c = Integer.parseInt(args[2]);

        int mm_order = 5;
        selex_data.markovModel(mm_order);

        String[] words = argv[argv.length - 1].split("/");
        selex_data.gene = words[words.length-1];
        selex_data.printKmers(c, Integer.parseInt(args[3]), Integer.parseInt(args[4]), 
        		Integer.parseInt(args[5]), false, args[1], args[0]);
        System.out.println("Wrote to file " + args[0]);

	}
}
