package printKmers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Arrays;

public class HT_SELEX {

	int num_of_cycles;
	int current_k;
	int k_max;
	int k_min;
	int mm_order;
	public int []oligo_nums;
	String gene;
	String[] files;
	public HashMap<Integer, HashMap<Kmer, int[]>> counters;
	public HashMap<Integer, int[]> totals;
	double[][] model;
	double[][] mm;

	public HT_SELEX(int k_max, int k_min, String[] args) {
		this.k_max = k_max;
		this.k_min = k_min;
		num_of_cycles = args.length - 1;
		counters = new HashMap<Integer, HashMap<Kmer, int[]>>();
		totals = new HashMap<Integer, int[]>();
		files = new String[args.length];
		for (int i = 0; i < args.length; i++) files[i] = args[i];
		gene = "";
	}
	
	public boolean count_Kmers(int k, PrintWriter out) {
		if (counters.containsKey(k)) return true;
		if (k == current_k) {
		} else if (k == current_k - 1) {
			this.current_k = k;
			generate_counters_from_existing_conters();
		} else {
			this.current_k = k;
			try {
				for (int i = 0; i < num_of_cycles; i++)
					count_kmer_for_cycle(files[i], i, out);
			} catch (FileNotFoundException e) {
				System.out.println("ERROR : " + e.getMessage());
				return false;
			} catch (IOException e) {
				System.out.println("ERROR : " + e.getMessage());
				return false;
			}
		}
		return true;
	}
	
	public void markovModel(int order) {
		mm_order = order;
		mm = new double[(int)Math.pow(4, order)][4];
		
		for (int i = 0; i < Math.pow(4, order+1); i++) {
			Kmer kmer = new Kmer(order+1, i);
			mm[new Kmer(kmer.toString().substring(0,order)).rep][Letters.char_to_int(kmer.toString().charAt(order))] += kmer.getRealCount(counters.get(order+1), 0);
		}
		for (int i = 0; i < Math.pow(4, order); i++) {
			double total = 0; 
			for (int j = 0; j < 4; j++) total += (mm[i][j]+1);
			for (int j = 0; j < 4; j++) mm[i][j] = (mm[i][j]+1) / total;
		}
	}
	
	private void generate_counters_from_existing_conters() {
		// generate temp hash counters
		HashMap<Kmer, int[]> tmp_counters = new HashMap<Kmer, int[]>();
		int[] total = new int[num_of_cycles * 2];
		// for (start at k_rep==0 and increment to 4^current_k)
		double max_rep = Math.pow(4, current_k);
		for (int rep = 0; rep < max_rep; rep++) {
			Kmer kmer = new Kmer(current_k, rep);
			int[] arr = new int[num_of_cycles * 2];
			for (int letter = 0; letter < 4; letter++) {
				// adding letter to the left e.g. ATC -> AATC
				Kmer kmer_left = new Kmer(current_k + 1, rep
						| (letter << 2 * current_k));

				int[] prev_arr_left = counters.get(current_k + 1)
						.get(kmer_left);
				if (prev_arr_left == null)
					prev_arr_left = new int[num_of_cycles * 2];
				// adding letter to the right e.g. ATC -> ATCA
				Kmer kmer_right = new Kmer(current_k + 1, (rep << 2) | letter);

				int[] prev_arr_right = counters.get(current_k + 1).get(
						kmer_right);
				if (prev_arr_right == null)
					prev_arr_right = new int[num_of_cycles * 2];
				// // first cycle.
				for (int i = 0; i < num_of_cycles; i++) {
					// from left end
					arr[2 * i] += prev_arr_right[i];
					// others
					arr[2 * i + 1] += prev_arr_left[2 * i + 1]
							+ prev_arr_left[2 * i];
				}

				// // second cycle.
				// from left end
				// arr[2] += prev_arr_right[2];
				// others
				// arr[3] += prev_arr_left[3] + prev_arr_left[2];
			}
			tmp_counters.put(kmer, arr);
			for (int i = 0; i < arr.length; i++)
				total[i] += arr[i];
		}
		counters.put(current_k, tmp_counters);
		totals.put(current_k, total);
	}

        public void printKmers(int c, int side, int ham1, int ham2, boolean all, String core, 
        		String dir) throws IOException {

            PBM_Line[] kmers = parseOneFlanksHam(c, core, side, ham1, ham2);

            if (true) { //Letters.isPalindrome(core)) {
                kmers = PBM_Line.mergeRC(kmers); // was part of the code
            }
            PBM_Line[] toPrint = new PBM_Line[kmers.length];
            double max = 0;
            for (int i = 0; i < kmers.length; i++) {
                   double freq0 = new Kmer(kmers[i].line).getEstimatedFreq(counters.get(mm_order), totals.get(mm_order),  mm, mm_order);
                   if (!Letters.isPalindrome(kmers[i].line))
                           freq0 +=  new Kmer(Letters.reverse_comp(kmers[i].line)).getEstimatedFreq(counters.get(mm_order), totals.get(mm_order),  mm, mm_order);
                   toPrint[i] = new PBM_Line(kmers[i].line, 0);
                   toPrint[i].score = Math.pow((double)kmers[i].rank / freq0, 1/(double)c); toPrint[i].rank = kmers[i].rank;
                   if (toPrint[i].score > max) max = toPrint[i].score;
          	}

            Arrays.sort(toPrint);

            PrintWriter out = new PrintWriter(new FileWriter(dir));
            for (int i = 0; i < toPrint.length; i++)
                    out.write(toPrint[i].line + "\t" + (toPrint[i].score / max) + "\t" + toPrint[i].rank + "\n");

            out.close();
    }
        
		private void count_kmer_for_cycle(String filename, int counters_index, PrintWriter out)
		throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));

		String line;
		HashMap<Kmer, int[]> tmp_counters = new HashMap<Kmer, int[]>();
		int[] total = totals.get(current_k);
		int oligos = 0;
		if (total == null)
			total = new int[num_of_cycles * 2];

		if (counters.get(current_k) != null)
			tmp_counters = counters.get(current_k);

		// initialize mask to 1111...111 (2k times)
		int mask = 3;
		for (int i = 1; i < current_k; i++) {
			mask <<= 2;
			mask |= 3;
		}
		while ((line = br.readLine()) != null) {
			// process the line.
			String[] sp = line.split("\t");
			line = sp[0];
			if (!Letters.validLine(line)) continue;
			int mult = 1; 
//			if (sp.length > 1) mult = Integer.parseInt(sp[1]);
			oligos += mult;
			if (line.length() < current_k) {
				continue;
			}

			// Count k-mer only once
			HashMap<Kmer, Boolean> kmers_once = new HashMap<Kmer, Boolean>();

			// first kmer in line (from left edge)
			int rep = Letters.char_to_int(line.charAt(0));
			for (int i = 1; i < current_k; i++) {
				rep <<= 2;
				rep |= Letters.char_to_int(line.charAt(i));
			}

			Kmer kmer = new Kmer(current_k, rep);
			if (!tmp_counters.containsKey(kmer)) {
				int[] arr = new int[num_of_cycles * 2];
				tmp_counters.put(kmer, arr);
			}
			tmp_counters.get(kmer)[counters_index * 2] = tmp_counters.get(kmer)[counters_index * 2]
					+ mult;
			total[counters_index * 2] += mult;
			kmers_once.put(kmer, true);
			// rest of kmers
			for (int j = current_k; j < line.length(); j++) {
				rep <<= 2;
				rep &= mask;
				rep |= Letters.char_to_int(line.charAt(j));
				kmer = new Kmer(current_k, rep);
				if (true/*!kmers_once.containsKey(kmer)*/) {
				if (!tmp_counters.containsKey(kmer)) {
					int[] arr = new int[num_of_cycles * 2];
					tmp_counters.put(kmer, arr);
				}
				tmp_counters.get(kmer)[counters_index * 2 + 1] = tmp_counters
						.get(kmer)[counters_index * 2 + 1]
						+ mult;
				total[counters_index * 2 + 1] += mult;
				kmers_once.put(kmer, true);
				}
			}
		}
		counters.put(current_k, tmp_counters);
		totals.put(current_k, total);
		br.close();
		if (out != null) out.write(filename +"\t"  + oligos);
	}

	
	public PBM_Line[] parseOneFlanksHam(int cycle, String seed, int flanks, int mis1, int mis2) throws FileNotFoundException, IOException {
		HashMap<String, Integer> line_list = parseOneFlanksHash(cycle, seed, flanks, flanks, mis1, mis2);
		PBM_Line[] kmers = new PBM_Line[line_list.size()];
		String[] keys = line_list.keySet().toArray(new String[0]);
		for (int i = 0; i < keys.length; i++) {
			kmers[i] = new PBM_Line(keys[i], line_list.get(keys[i]));
			kmers[i].score = line_list.get(keys[i]);
		}
		return kmers;
	}

	
	public HashMap<String, Integer> parseOneFlanksHash(int cycle, String seed, int flanks1, int flanks2, int mismatch1, int mismatch2)
	throws FileNotFoundException, IOException {

	String full_path = files[cycle];

	BufferedReader br = new BufferedReader(new FileReader(full_path));
	String line;
	HashMap<String, Integer> line_list = new HashMap<String, Integer>();
	
	int k = flanks1 + flanks2 + seed.length();

	while ((line = br.readLine()) != null) {

		String[] sp = line.split("\t");
		String probe = sp[0];
		if (!Letters.validLine(probe)) continue;
		int mult = 1;

		if (probe.length() < k) {
			continue;
		}
		PBM_Line l = new PBM_Line(probe, 0);

		// Relaxing threshold for identifying binding sites
		int count = l.setAlign(seed, flanks1, mismatch1, true);

		if (count < 2)
			l.setAlign(seed, flanks1, mismatch2, false);

		if (l.rank < flanks1 || l.rank > l.line.length()-seed.length()-flanks2) l.rank = Integer.MIN_VALUE;
		
		if (l.rank > Integer.MIN_VALUE) {
			String lll = l.rc ? Letters.reverse_comp(l.line) : l.line;
			String f = lll.substring(l.rank-flanks1,l.rank+seed.length()+flanks2);
			if (!line_list.containsKey(f)) line_list.put(f, 0);
			line_list.put(f, line_list.get(f) + mult);
		}

	}

	br.close();
	return line_list;
}
	


}
