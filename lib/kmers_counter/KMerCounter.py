
from os.path import join
from lib.FastQAnalyzer import FastQAnalyzer
from os import system
import pandas as pd
# from lib.SELEX.HTSELEXAnalyzer import HTSELEXAnalyzer
from lib.SequenceMethods import SequenceMethods
from os.path import exists, basename
from lib.DataFrameAnalyzer import DataFrameAnalyzer
import subprocess as sp
from math import floor

class KMerCounter():

    def __init__(self):
        self.iupac_table = {"A": {"A"}, "C": {"C"}, "T": {"T"}, "G": {"G"},
                       "R": {"A", 'G'}, "Y": {"C", 'T'},
                       "W": {"G", 'C'}, "S": {"A", 'T'},
                       "K": {"G", 'T'}, "M": {"A", 'C'},
                       "B": {"C", 'G', 'T'},
                       "D": {"A", 'G', 'T'},
                       "H": {"C", 'G', 'T'},
                       "V": {"A", 'C', 'G'},
                       "N": {"C", 'G', 'T', 'A'}}
        self.nuc_dic = {'A', 'C', 'G', 'T'}

    @staticmethod
    def read_fastq(fastq_path, K, left=None, right=None, bkp_dir=None, suffix='', iterative=True,
                   count_reverse=False, overwrite=False, fill_zeroes=False, **kwargs):

        stop_at = str(kwargs.get('stop_at', 'all'))
        bkp_path = join(bkp_dir, basename(fastq_path).replace(".fastq.gz", "_K" + str(K) + "_" + stop_at +
                                                              ("_REVERSE%i" % (count_reverse)) +
                                                              ("_" + suffix if len(suffix) > 0 else '') + ".tsv.gz"))\
            if bkp_dir is not None else None

        print bkp_path
        if exists(bkp_path) and not overwrite:
            print 'reading bkp from'
            print bkp_path
            return DataFrameAnalyzer.read_tsv_gz(bkp_path)

        seqs = FastQAnalyzer.get_sequences(fastq_path, **kwargs)
        df = pd.DataFrame(seqs, columns=['seq'])

        # trim left and right if not ready
        if left is not None:
            df['seq'] = df['seq'].str[left:]
        if left is not None:
            df['seq'] = df['seq'].str[:-right]
        # print df.head()

        # just one possible length in the reads is valid
        print set(df['seq'].str.len().values)
        assert len(set(df['seq'].str.len().values)) == 1
        read_length = list(set(df['seq'].str.len().values))[0]

        output = None
        print 'converting into kmers (iterative=%i)...' % iterative
        if not iterative:
            for i in range(read_length - K + 1):
                df[str(i) + ".fwd"] = df['seq'].str[i: i + K]
                df[str(i) + ".rev"] = df['seq'].str[i: i + K].apply(SequenceMethods.get_complementary_seq)

            # remove the sequence label
            df = df[df.columns[1:]]
            res = df.apply(pd.value_counts)
            res['sum'] = res.sum(axis=1).astype(int)
            d = res[['sum']].to_dict()['sum']
            # print d
            output = KMerCounter.from_dict(d)
        else:
            d = {}
            counter = 0
            print 'Counting kmers, (count_reverse=%i)' % count_reverse
            for ri, r in df.iterrows():
                if counter % 250000 == 0:
                    print K, counter, 'out of', df.shape[0], '(count_reverse=%i)' % count_reverse
                counter += 1
                seq = r['seq']
                for i in range(read_length - K + 1):
                    s = seq[i: i + K]
                    fwd = s
                    d[fwd] = d[fwd] + 1 if fwd in d else 1
                    if count_reverse:
                        rev = SequenceMethods.get_complementary_seq(s)
                        d[rev] = d[rev] + 1 if rev in d else 1

            output = KMerCounter.from_dict(d)

        if bkp_path is not None and (not exists(bkp_path) or overwrite):
            DataFrameAnalyzer.to_tsv_gz(output, bkp_path)

        if fill_zeroes:
            print 'filling zeroes'
            zeroes = []
            kmers = SequenceMethods.get_sequence_combinations(K)
            seq_ids = set(output['seq'])
            for next in kmers:
                if not next in seq_ids:
                    zeroes.append([next, 0])
            zeroes_df = pd.DataFrame(zeroes, columns=['seq', 'counts'])
            output = pd.concat([output, zeroes_df]).reset_index(drop=True)

        return output

    @staticmethod
    def from_dict(kmers_dict):
        print 'converting Kmers dictionary to dataframe'
        df = pd.DataFrame.from_dict({'counts': kmers_dict})
        df['seq'] = df.index
        # remove dots
        df = df[~df['seq'].str.contains('\.')]
        df = df.reset_index(drop=True).sort_values("counts", ascending=False)
        df = df[df.columns[::-1]]
        return df

    def get_kmer_counts(self, bg_counts_path, fg_counts_path, output_path,
                        core_motif, cycle, n_flanks, threshold1=0, threshold2=0,
                        binary_path=None):
        '''
        run java process to calculate counts
        :param bg_counts_path:
        :param fg_counts_path:
        :param core_motif:
        :param cycle:
        :param n_flanks:
        :param output_path:
        :param threshold1:
        :param threshold2:
        :return:
        '''

        print fg_counts_path
        assert exists(fg_counts_path)
        assert exists(bg_counts_path)
        java_bin = '/g/scb2/zaugg/zaugg_shared/Programs/java/jre1.8.0_144/bin/java'
        if not exists(java_bin):
            java_bin = 'java'
        jar_path = "../../../lib/kmers_counter/printKmers.jar" if binary_path is None else binary_path
        cmd = " ".join(map(str, [java_bin, '-jar', jar_path, output_path, core_motif, cycle,
                                 n_flanks, threshold1, threshold2]  + [ bg_counts_path ] * cycle + [fg_counts_path]))

        print cmd
        exit()
        # exit()
        system(cmd)

    @staticmethod
    def get_kmer_counts_by_length(K, read_counts_dataframe):
        counts_dict = {}
        for ri, r in read_counts_dataframe.iterrows():
            seq, counts = r.values
            for i in range(len(seq) - K + 1):
                kmer = seq[i: i + K]
                if not kmer in counts_dict:
                    counts_dict[kmer] = 0
                counts_dict[kmer] += counts
        return counts_dict



    def get_kmer_counts_cmd(self, bg_counts_path, fg_counts_path, output_path,
                        core_motif, cycle, n_flanks, threshold1=0, threshold2=0):
        '''
        run java process to calculate counts
        :param bg_counts_path:
        :param fg_counts_path:
        :param core_motif:
        :param cycle:
        :param n_flanks:
        :param output_path:
        :param threshold1:
        :param threshold2:
        :return:
        '''

        print fg_counts_path
        assert exists(fg_counts_path)
        assert exists(bg_counts_path)
        jar_path = "../../../lib/kmers_counter/printKmers.jar"
        cmd = " ".join(map(str, ['java', '-jar', basename(jar_path), basename(output_path), core_motif, cycle,
                                 n_flanks, threshold1, threshold2]  + [ basename(bg_counts_path) ] * cycle + [basename(fg_counts_path) ]))
        print cmd
        return cmd

    def count_occurences_mword(self, kmer, kmer_path, complementary=True):
        '''
        Given a Kmer and a list of fastq paths, get the occurences of that kmer in those files
        :param kmer:
        :param fastq_paths:
        :param complementary:
        :return:
        '''
        print 'ocurrences of', kmer, 'in', kmer_path
        print kmer, kmer_path
        o = sp.Popen(["cat", kmer_path], stdout=sp.PIPE)
        if complementary:
            kmer_rev = SequenceMethods.get_complementary_seq(kmer)
            o = sp.Popen(['grep', "-E", kmer + "|" + kmer_rev],
                         stdin=o.stdout, stdout=sp.PIPE).communicate()
        else:
            o = sp.Popen(['grep', kmer], stdin=o.stdout, stdout=sp.PIPE).communicate()
        n = 0
        if len(o[0].split("\n")) > 0:
            n = int(o[0].split("\n")[0].split("\t")[-1])
        return n

    def verify_mword_counts(self, aff_data, query_dirs,
                            delta_cycle=0):
        '''

        Given a set of fastq files check the likelihood of those reads having at least
        the same amount of reads as declared in the main software function

        :param query_dirs:
        :param aff_data:
        :return:
        '''

        htselex_analyzer = HTSELEXAnalyzer()
        last_cycle_fastq_paths, r0_fastq_paths = htselex_analyzer.get_fastq_files(aff_data,
                                                                                  query_dirs,
                                                                                  delta_cycle=delta_cycle)

        print last_cycle_fastq_paths

        fastq_analyzer = FastQAnalyzer()
        for ri, r in aff_data.df.iterrows():
            if ri > 5:
                break
            mword, affinity, counts = r
            print mword
            total_counts = fastq_analyzer.count_occurences_kmer(mword, last_cycle_fastq_paths)
            assert total_counts > counts

        pass

    def create_read_counts_table(self, fastq_path, output_path_or_stream, filters=None,
                                 left_flank=None, right_flank=None, n_variable=None):
        '''
        Get a file with two parameters: the read and the overall count across all reads
        :param fastq_path:
        :param output_path:
        :param: filter: Filter all sequences that contain these characters
        :return:
        '''

        fastq_analyzer = FastQAnalyzer()

        sequences = fastq_analyzer.get_sequences(fastq_path)

        if filters is not None:
            for fi in filters:
                sequences = [s for s in sequences if not fi in s]
        print "# reads after filtering", len(sequences)

        if left_flank is not None:
            print 'filtering by left flank...'
            sequences = [s[len(left_flank):] for s in sequences if s.startswith(left_flank)]

        if right_flank is not None:
            print 'filtering by right flank...'
            sequences = [s[:n_variable] for s in sequences if s[n_variable:].startswith(right_flank)]

        counts = {}
        for s in sequences:
            if not s in counts:
                counts[s] = 0
            counts[s] += 1


        stream = open(output_path_or_stream, 'w') if isinstance(output_path_or_stream, str) else output_path_or_stream
        with stream as writer:
            for k in sorted(counts.keys(), key=lambda x: -counts[x]):
                writer.write("\t".join([k, str(counts[k])]) + "\n")

    def contains_non_canonical_base(self, s):
        return sum([1 for nt in s if nt not in {'A', 'C', 'G', 'T'}]) != len(s)
    def get_allowed_mismatches_threshold(self, motif):

        contains_non_canonical_bases = self.contains_non_canonical_base(motif)
        len_core = -1
        if contains_non_canonical_bases:
            len_core = sum([1.0 / len(self.iupac_table[nt]) for nt in motif])
            print len_core
        else:
            len_core = len(motif)
        return int((len_core - 4) / 2) + 1

    def get_matches(self, a, b, use_degenerative_code=False):
        assert len(a) == len(b)

        counts = 0
        seq_len = len(a)

        if use_degenerative_code:
            for pi in range(len(a)):
                ai, bi = a[pi], b[pi]
                if bi in self.nuc_dic:
                    if ai == bi:
                        counts += 1
                else:  # the else condition evaluates for a nt belonging to a degenerate code
                    if ai in self.iupac_table[bi]:
                        counts += 1
        else:
            for pi in range(len(a)):
                ai, bi = a[pi], b[pi]
                if ai == bi:
                    counts += 1
        return counts

    def get_mwords(self, sequences, core_motif, n_flanks, thr1, thr2):
        mwords = {}
        for si, s in enumerate(sequences):
            count, rank, temp_rc = self.get_align(s, core_motif,
                                                          n_flanks, 0,
                                                          filter=True)
            if count < 2:
                count, rank, temp_rc = self.get_align(s, core_motif,
                                                              n_flanks, 0,
                                                              filter=False)
            if rank < n_flanks or rank > len(s) - len(core_motif) - n_flanks:
                rank = -4000
            if rank >= 0:
                selected = s[rank - n_flanks: rank + len(core_motif) + n_flanks]
                if temp_rc:
                    selected = SequenceMethods.get_complementary_seq(s)[
                               rank - n_flanks: rank + len(
                                   core_motif) + n_flanks]
                # print "selected sequence", si, selected
                mwords[selected] = mwords[selected] + 1 if selected in mwords else 1
        return mwords

    def get_align(self, seq, consensus, flanks1, mismatch, filter=False):
        max = 0
        imax = 0
        temp_rc = False
        count = 0
        rank = -4000
        middle = int(floor((len(seq) - len(consensus) ) / 2 ))

        # scan in forward direction
        for i in range(len(seq) - len(consensus) + 1):
            query = seq[i:i + len(consensus)]
            matches = self.get_matches(query, consensus)
            if matches >= len(consensus) - mismatch:
                count += 1
            if matches > max or (matches == max and abs(imax - middle)) > abs(i - middle):
                max = matches
                imax =i

        if count < 2:
            count = 0

        seq = SequenceMethods.get_complementary_seq(seq)
        for i in range(len(seq) - len(consensus) + 1):
            query = seq[i:i + len(consensus)]
            matches = self.get_matches(query, consensus)
            if matches >= len(consensus) - mismatch:
                count += 1
            if matches > max or (matches == max and abs(imax - middle)) > abs(i - middle):
                max = matches
                imax = i
                temp_rc = True

        rank = imax
        if max < len(consensus) - mismatch or (filter and count > 1):
            rank = -4000
        return [count, rank, temp_rc]
