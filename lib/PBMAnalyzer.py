'''
Created on 2/2/2018, 2018

@author: Ignacio Ibarra Del Rio

Description:
'''

from lib.utils import *
from lib.DataFrameAnalyzer import DataFrameAnalyzer

class PBMAnalyzer:

    @staticmethod
    def get_kmer_from_file(p, kmer_length_cuttof=8, cutoff=0.35):
        df = DataFrameAnalyzer.read_tsv(p, header=None)
        df.columns = ['kmer.forward', 'kmer.reverse', 'E-score', 'score2', 'score3']
        return df[(df['kmer.forward'].str.len() == kmer_length_cuttof) & (df['E-score'] >= cutoff)]

    @staticmethod
    def get_paths():
        print('WARNING: BAR15A has not been added to the libraries. Please check...')
        # assert 1 > 2
        basedir = '/g/scb2/zaugg/rio/data/UniProbe'
        bkp_path = join(basedir, 'paths.pkl')
        if not exists(bkp_path):
            paths = []
            for d in listdir(basedir):
                if not isdir(join(basedir, d)):
                    continue
                paths += [join(basedir, d, f) for f in listdir(join(basedir, d)) if f.endswith('_11111111.txt') or f.endswith('_top_enrichment.txt')]
            DataFrameAnalyzer.to_pickle(paths, bkp_path)
        return DataFrameAnalyzer.read_pickle(bkp_path)

    @staticmethod
    def get_paths_by_tf_name(tf_name):
        paths = PBMAnalyzer.get_paths()
        found = [p for p in paths if  tf_name.lower() in p.lower()]
        print('paths with k found', len(found))
        return found

    @staticmethod
    def get_pbm_data_from_tf_name(tf_name):
        found = PBMAnalyzer.get_paths_by_tf_name(tf_name)
        if len(found) > 0:
            df = []
            for p in found:
                print(p)
                df.append(PBMAnalyzer.get_kmer_from_file(p, kmer_length_cuttof=8, cutoff=-np.inf))
            return sorted(df, key=lambda x: -x.shape[0])[0]
        else:
            return None
