'''
#########
# To start a job
#########
# 1.1) run with a zero index to generate submission script
# python demo.py 0
# 1.2) alternatively, run with --ngroup to generate subitem processing (e.g. 100 jobs, each processing 10 items)
# python demo.py 0 --ngroup 10
# 1.3) if you want to debug, run with any index different than 1, or a query name
# python demo.py 1
# python demo.py 1 --query $FILENAMEPATTERNTODEBUG
# a submission script is created in the same directory
2) run the script
./submit_job_slurm.sh
OR call the slurm command manually (copy from standard output in step 1))
# sbatch --array=1-100 demo_array_slurm.sh > submit_job_slurm.log
@author: ignacio
'''
from lib.utils import *
from lib.BSubFacade import BSubFacade

def function_to_parallelize(jobid=None, **kwargs):
    ##############
    # Add whatever action you want to do

    # In the most simple way, you can read an constant input file and process the i-th element (given by jobid)
    input_files = [join('somedirectory', str(i)) for i in range(10)]

    # also, you can use a custom query in case of debugging (e.g. string matching in the filename)
    query = kwargs.get('query', None)

    # also, running by group (each jobid will handle ngroup items). By default, just one input per item will be handled
    ngroup = kwargs.get('ngroup', 1)
    start = jobid * ngroup
    end = start + ngroup
    # MAIN FOR LOOP
    for i, next_item in input_files:
        if query is None:
            if i < start or i >= end:
                continue
        else:
            if not query in next_item:
                continue
        #########################
        # PROCESS YOUR FILE HERE
        #########################

if __name__ == "__main__":
    BSubFacade.prepare_jobarray_scripts(__file__, function_to_parallelize,
                                        n_jobs=100,
                                        # n_jobs_max=10, maximum number of jobs to run at the same time
                                        mem=5000,
                                        qos='high',
                                        bsub_output_dir='/scratch/myusername/bsub_output',
                                        queue='htc',
                                        # ntasks=15) # number of additional subprocesses to run (in case of threading)
                                        time='00:15:00')