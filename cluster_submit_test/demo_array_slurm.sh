#!/bin/bash
#SBATCH -J demo
#SBATCH --mem=5000
#SBATCH --qos=high
#SBATCH -p htc
#SBATCH -t 00:15:00 
#SBATCH -n 1
#SBATCH -o  /scratch/rio/bsub_output/demo_output_%a.txt
#SBATCH -e  /scratch/rio/bsub_output/demo_error_%a.txt

echo "STARTING JOB"
python demo.py ${SLURM_ARRAY_TASK_ID} --ngroup 1
echo "FINISHED JOB"

        
# (SLURM):
#Please submit your script with the following command...
# sbatch --array=1-100 demo_array_slurm.sh > submit_job_slurm.log