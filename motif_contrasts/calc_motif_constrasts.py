
from lib.utils import *
from lib.plot_utils import *

# Motif related libraries (functions, db, etc.)
from lib.Motif import *

# Fasta GENRE, and annotation parsers
from lib.FastaAnalyzer import FastaAnalyzer
from lib.SELEX.SELEXAnalyzer import HTSELEXAnalyzer
from lib.MyGeneAnalyzer import MyGeneAnalyzer

# Rparses for plotting and statistics
from lib.RFacade import RFacade

# handling args
import argparse
parser = argparse.ArgumentParser(description='Motif comparisons across db and with different functions for two groups')
parser.add_argument('--genome', type=str, required=True) # mm10, hg19, hg38
parser.add_argument('--label1', type=str, required=True)
parser.add_argument('--label2', type=str, required=True)
parser.add_argument('--bed1', type=str, default=None)
parser.add_argument('--bed2', type=str, default=None)
parser.add_argument('--fasta1', type=str, default=None)
parser.add_argument('--fasta2', type=str, default=None)
parser.add_argument('--outputdir', type=str, default='/tmp')
parser.add_argument("--motif_bkp_dir", type=None, default=None, help='a directory where backup motif hits are stored')
parser.add_argument('--nvolcano', type=int, default=10)
parser.add_argument('--nheatmap', type=int, default=55, help='top and bottom cases to show in heatmap vizualiation')
parser.add_argument('--wheatmap', type=int, default=13)
parser.add_argument('--hheatmap', type=int, default=10)
parser.add_argument('--covariates', type=str, default=None)
parser.add_argument('--motifdb', type=str, default=None, help='Define a custom motif database to be used (e.g. JASPAR')
parser.add_argument('--genre_kmer_i', type=int, default=None, help='If RUNNING GENRE, then calculate a single kmer')
parser.add_argument('--runcontrast', default=False, action='store_true')

args = parser.parse_args()
label1 = args.label1
label2 = args.label2
bed_path1 =  args.bed1
bed_path2 = args.bed2
fa_path1 =  args.fasta1
fa_path2 = args.fasta2
genre_kmer_i = args.genre_kmer_i
motifs_bkp_dir = args.motif_bkp_dir

covariates_path = args.covariates
query_motifdb = args.motifdb

covariates = None
if covariates_path is not None:
    print 'covariates file declared. Reading from input'
    print covariates_path
    covariates = DataFrameAnalyzer.read_tsv_gz(covariates_path)

# convert bed to fasta paths (only if required. Otherwise work with FASTA only)
output_dir = join(args.outputdir, label1 + "_vs_" + label2)
if not exists(output_dir):
    mkdir(output_dir)
print output_dir
if args.fasta1 is None and args.fasta2 is None:
    assert args.bed1 is not None and args.bed2 is not None
    fa_path1 =  join(output_dir, basename(bed_path1).replace(".bed", '.fa'))
    fa_path2 =  join(output_dir, basename(bed_path2).replace(".bed", '.fa'))
# this step converts BED to FASTA if required
if not exists(fa_path1) or not exists(fa_path2):
    for bed, fasta in [[bed_path1, fa_path1], [bed_path2, fa_path2]]:
        FastaAnalyzer.convert_bed_to_fasta(bed, fasta, genome=args.genome)

# DEFINE MOTIF DATABASES AND COMPARISONS
# These are the encapsulated function methods to be used for contrasts during this script (Strategy Pattern)
# See MotifContrastFunction and MotifQueries for details
assert args.genome in MotifQueries.queries_by_genome
queries = MotifQueries.queries_by_genome[args.genome]

motif_db_order = ['HOCOMOCO', 'JASPAR', 'CISBP', 'GENRE']
# For each motif database, map motifs, and obtain metrics.
all_plots = [] # collect plots to be plotted at the last step
results_by_k = {} # save results table by motif DB and metric
plot_volcanoes = True
plot_volcanoes_by_motifdb = True
plot_volcanoes_all = True
overwrite_motif_contrasts = args.runcontrast
symbol_by_ensg = {} # save symbols by ENSG as we check across motif db

ncols = max([len(functions) for motif_db, annot_path, functions in queries.values()])
for motif_db in motif_db_order:
    print motif_db, query_motifdb

    if query_motifdb is not None and query_motifdb != motif_db:
        print 'skip...'
        continue

    print 'Motif scoring step'
    print 'Next motif DB', motif_db

    motif_db_path, annotation_path, functions = queries[motif_db]

    motifs_dir = join(output_dir, 'motif_scores')
    if not exists(motifs_dir):
        mkdir(motifs_dir)

    motif_hits_bkp1 =  join(motifs_dir, 'motif_scores1_' + motif_db + '.tsv.gz')
    motif_hits_bkp2 =  join(motifs_dir, 'motif_scores2_' + motif_db + '.tsv.gz')
    output_path = join(motifs_dir, 'all_contrasts_' + motif_db + ".tsv.gz")

    # Motif scoring step (HOMER, FIMO or GENRE)
    # AUROC
    # in this example we use homer and mscore=True (ie. one motif per sequence, without p-value restriction
    # Fold-change: run with mscore=False
    overwrite = False
    mscore = False


    if not exists(output_path) or overwrite_motif_contrasts:
        motif_hits = None
        if motifs_bkp_dir is None:
            if not 'GENRE' in motif_db: # This is the conventional HOMER run (PWM vs sequences)
                print fa_path1
                print fa_path2

                headers1, seqs1 = zip(*FastaAnalyzer.get_fastas(fa_path1, uppercase=True))
                hits1, hits2 =  [MotifAnalyzer.get_motif_hits_homer(fa, motif_db_path, output_path=bkp,
                                                                   overwrite=overwrite, mscore=mscore)
                                                                   for fa, bkp in [[fa_path1, motif_hits_bkp1],
                                                                                   [fa_path2, motif_hits_bkp2]]]

            else: # This is a custom GENRE run, because motif mapping is based on 8mer matching (kmers vs sequences)
                if overwrite or not exists(motif_hits_bkp1) or not exists(motif_hits_bkp2):
                    headers1, seqs1 = zip(*FastaAnalyzer.get_fastas(fa_path1, uppercase=True))
                    headers2, seqs2 = zip(*FastaAnalyzer.get_fastas(fa_path2, uppercase=True))
                    htselex = HTSELEXAnalyzer()
                    hits = [[], []]
                    for i, headers, seqs, bkp_path in [[0, headers1, seqs1, motif_hits_bkp1],
                                                       [1, headers2, seqs2, motif_hits_bkp2]]:

                        print 'here...'
                        exit()

                        # assuming we will use always the same sequences for 8-mer calculation, convert them into non-redundant 8-mers
                        genre_optimize = False
                        queries = None
                        print 'genre optimize?', genre_optimize
                        if genre_optimize:
                            print 'preparing motifs'
                            queries = htselex.mapped_motifs_in_sequences(seqs, 8)
                            # remove duplicates (one kmer per sequence)
                            queries['k'] = queries['peak.id'].astype(str) + "_" + queries['seq']
                            queries = queries.drop_duplicates('seq')

                        module_queries = [[module_i, module_filename] for module_i, module_filename in enumerate(listdir(motif_db_path))][::-1]
                        for module_i, module_filename in module_queries:
                            kmer_id = module_filename.replace(".kmer", '')
                            if genre_kmer_i is not None and genre_kmer_i != module_i:
                                continue
                            print module_i, 'of 108', module_filename
                            next_bkp_path = bkp_path.replace(".tsv.gz", '_' + kmer_id + ".tsv.gz")
                            print exists(next_bkp_path), next_bkp_path
                            if not exists(next_bkp_path):
                                res = htselex.get_scores_seqs_vs_kmer(seqs, join(motif_db_path, module_filename),
                                                                      queries=queries)
                                res['header'] = [headers[peak_i] for peak_i in res['peak.id']]
                                res['motif.id'] = kmer_id
                                # group by best
                                res = pd.concat([grp.sort_values('score', ascending=False).head(1) for h, grp in res.groupby('header')])
                                DataFrameAnalyzer.to_tsv_gz(res, next_bkp_path)
                                # print res.head()
                                # print res.shape
                            else:
                                print 'GENRE path exists. Load from output...'
                            res = DataFrameAnalyzer.read_tsv_gz(next_bkp_path)

                            hits[i].append(res)
                        next_hits = pd.concat(hits[i])

                        # save all hits once we have them all
                        if genre_kmer_i is None:
                            DataFrameAnalyzer.to_tsv_gz(next_hits, bkp_path)


                hits1 = DataFrameAnalyzer.read_tsv_gz(motif_hits_bkp1)
                hits1['select.motif.p.val'] = hits1['score'] != -1
                hits2 = DataFrameAnalyzer.read_tsv_gz(motif_hits_bkp2)
                hits2['select.motif.p.val'] = hits2['score'] != -1

            hits1['group'] = label1
            hits2['group'] = label2
            motif_hits = pd.concat([hits1, hits2])
        else: # read motif hits from a bkp directory
            all_hits1 = []
            all_hits2 = []
            headers1, seqs1 = zip(*FastaAnalyzer.get_fastas(fa_path1, uppercase=True))
            headers2, seqs2 = zip(*FastaAnalyzer.get_fastas(fa_path2, uppercase=True))
            headers1 = set(headers1)
            headers2 = set(headers2)

            for di, d in enumerate(listdir(motifs_bkp_dir)):
                print di, d
                all_hits = DataFrameAnalyzer.read_multiple_tsv_gz(join(motifs_bkp_dir, d), query=motif_db)
                if all_hits is None:
                    continue
                all_hits1.append(all_hits[all_hits['header'].isin(headers1)])
                all_hits2.append(all_hits[all_hits['header'].isin(headers2)])

            hits1 = pd.concat(all_hits1)
            hits2 = pd.concat(all_hits2)
            hits1['group'] = label1
            hits2['group'] = label2

            print hits1.shape
            print hits2.shape
            motif_hits = pd.concat([hits1, hits2])

        # Motif contrast step
        # (Custom methods can be extended in MotifContrastFunction class)
        # Already provided: AUROC, Motif odds.ratio, fold change and GLM (logistic, with/without covariates)
        if not exists(output_path) or overwrite_motif_contrasts:
            all_res = []
            for function in functions:
                print 'next contrast:', function
                res = None
                try:
                    res = MotifContrast.run(motif_hits, function=function, contrast_order=[label1, label2],
                                            covariates=covariates)
                except Exception:
                    print 'skipping calculation in custom function (were covariates given as input?)'
                    res = None
                if res is None:
                    continue
                res['p.adj'] = RFacade.get_bh_pvalues(res['p.val'])
                print '# significant according to test performed (p.adj < 0.05)'
                print (res['p.adj'] < 0.05).value_counts()
                res['motif.database'] = motif_db
                all_res.append(res)
            all_res = pd.concat(all_res)
            DataFrameAnalyzer.to_tsv_gz(all_res, output_path)

    print 'reading motifs...'
    all_res = DataFrameAnalyzer.read_tsv_gz(output_path)

    all_res['motif.id'] = all_res['motif.id'].str.replace(".txt", '')
    # parse motif IDs to ENSG identifiers and protein symbols
    if annotation_path is not None:
        annotation = DataFrameAnalyzer.read_tsv(annotation_path) if annotation_path is not None else None
        if motif_db == 'CISBP': # CISBP has motif with direct evidence (D) and inferred (I)
            annotation = annotation[annotation['TF_Status'] == 'D']

        # print annotation.head()
        # convert_unitprot to
        if motif_db == 'HOCOMOCO':
            out = MyGeneAnalyzer.get_gene_ensg_by_entrez(annotation['EntrezGene'],
                                                         species={'hg19': 'human', 'hg38': 'human', 'mm10': 'mouse'}[args.genome])
            print annotation.shape[0], len(out)
            annotation['ensg'] = annotation['EntrezGene'].map(out)
        motif_id_header_by_db = {'HOCOMOCO': 'Model', 'CISBP': 'Motif_ID', 'JASPAR': 'jaspar.id'}
        ensg_header_by_db = {'HOCOMOCO': 'ensg', 'CISBP': 'DBID', 'JASPAR': 'ensg'}
        symbol_header_by_db = {'HOCOMOCO': 'Transcription factor', 'CISBP': 'TF_Name', 'JASPAR': 'symbol'}
        all_res['ensg'] = all_res['motif.id'].map(DataFrameAnalyzer.get_dict(annotation,
                                                                               motif_id_header_by_db[motif_db],
                                                                               ensg_header_by_db[motif_db]))
        all_res['symbol'] = all_res['motif.id'].map(DataFrameAnalyzer.get_dict(annotation,
                                                                             motif_id_header_by_db[motif_db],
                                                                             symbol_header_by_db[motif_db]))

        symbol_by_ensg = dict(symbol_by_ensg.items() + DataFrameAnalyzer.get_dict(all_res, 'ensg', 'symbol').items())

    # prepare plots for each comparison type, showing up to n significant case
    n_show = args.nvolcano # amount of significant cases to show in each side
    n_heatmap = args.nheatmap
    plots = []

    print all_res['score.type'].value_counts()
    for metric, grp in all_res.groupby('score.type'):
        k = motif_db + "_" + metric
        # group multiple motifs with same ENSG
        if 'ensg' in grp:
            print 'grouping by ENSG'
            grp = pd.concat([grp2.sort_values('p.adj', ascending=True).head(1) for h, grp2 in grp.groupby('ensg')])

        if not exists(join(output_dir, 'figures')):
            mkdir(join(output_dir, 'figures'))
        output_figure = join(output_dir, 'figures', k)
        # if exists(output_figure + ".pdf"):
        #     continue
        grp['x'] = grp['score']
        grp['y'] = -np.log10(grp['p.adj'])

        grp['color'] = (grp['y'] > -np.log10(0.05)).astype(str)
        grp['size'] = grp['y']
        # LABEL significant cases
        grp = grp.sort_values('y', ascending=False)
        grp['label'] = [r['motif.id' if not 'symbol' in grp else 'symbol']
                        if r['y'] > -np.log10(0.05) else '' for ri, r in grp.iterrows()]

        grp['xy'] = grp['x'] * grp['y']
        print grp.head()
        xbottom, xtop = grp.sort_values('xy', ascending=True)['x'].values[n_show],\
                        grp.sort_values('xy', ascending=True)['x'].values[-n_show]
        print xbottom, xtop
        # remove labels if not part of top/bottom N
        grp['label'] = [lab if xi <= xbottom or xi >= xtop else '' for xi, lab in zip(grp['x'], grp['label'])]

        # grp = grp.sort_values('x', ascending=True)
        # grp['label'] = [r['motif.id' if not 'symbol' in grp else 'symbol']
        #                 if r['y'] > -np.log10(0.05) else '' for ri, r in grp.iterrows()]
        # grp['label'] = [lab if i < n_show or (grp.shape[0] - i) < n_show else ''
        #                 for i, lab in enumerate(grp['label'])]
        grp['shape'] = ''
        size_breaks = [1, 10, 30, 50, 100] # default breaks
        size_breaks_labs = ['1', '10', '30', '50', '>=100']

        # Define axes limits
        xmax_abs = np.ceil(max(grp['x'].abs()))
        ymax = max([yi for yi in grp['y'] if yi != np.inf])
        xmin, xmax = 0.0, 1.0
        if metric != 'AUROC':
            xmin, xmax = -xmax_abs, xmax_abs
            # print xmin, xmax
        if metric == 'odds.ratio':
            xmin = 0
            xmax = xmax_abs

        print metric
        grp = grp.sort_values('y', ascending=False)
        print grp[['x', 'y', 'size', 'color', 'shape', 'label']].head()
        print grp[['x', 'y', 'size', 'color', 'shape', 'label']].tail()
        results_by_k[k] = grp
        if plot_volcanoes:
            print 'plotting volcano (single)'
            p = RFacade.plot_scatter_ggrepel(grp[['x', 'y', 'size', 'color', 'shape', 'label']],
                                             join(output_dir, 'figures', k),
                                             xmin=xmin, xmax=xmax,
                                             title=k, xlab=metric, ylab='-log(p.adj)',
                                             color_lab='significant', size_lab='-log(p.adj)',
                                             size_breaks=size_breaks, size_breaks_labs=size_breaks_labs,
                                             ymax=ymax, w=6, h=6, save=False, save_tmp=False)
            plots.append(p)
    if len(plots) < ncols: # ADD blank panels to fill row
        plots += [RFacade.plot_scatter_ggrepel(grp.head(0), # [['x', 'y', 'size', 'color', 'shape', 'label']].head(0),
                                             join(output_dir, 'figures', k),
                                             xmin=xmin, xmax=xmax,
                                             title='', xlab='', ylab='', save_tmp=False,
                                             color_lab='', size_lab='', size_breaks_labs=size_breaks_labs,
                                             size_breaks=size_breaks, ymax=ymax, w=6, h=6, save=False)] * (ncols - len(plots))
    if plot_volcanoes_by_motifdb:
        print len(plots)
        print 'saving all plots in motif database', motif_db
        out_merged = join(output_dir, 'figures', motif_db + ".pdf")
        print out_merged
        RFacade.plot_multiple_ggrepel(plots, out_merged, ncol=ncols, w=20, h=5, nrow=1)
    all_plots += plots

if plot_volcanoes_all:
    print 'saving all...'
    RFacade.plot_multiple_ggrepel(all_plots, join(output_dir, 'figures', 'all_scatters.pdf'), ncol=ncols, w=20, h=20, nrow=4)

# prepare a heatmap with all the results by common genes
# CISBP and HOCOMOCO can be grouped together, JASPAR and GENRE need to wait
top_n = n_heatmap
fig = plt.figure(figsize=(args.wheatmap, args.hheatmap))
all_results = None
print results_by_k.keys()
minmax_by_metric = {'AUROC': [.25, .75], 'odds.ratio': {0, 2},
                    'fold.change': [-3, 3], 'GENRE_AUROC': [.25, .75], 'GENRE_glm_logit': [-400, 400]}
for i, metric in enumerate(['AUROC', 'fold.change', 'odds.ratio', 'GENRE_AUROC', 'GENRE_glm_logit']):
    if metric == 'GENRE_glm_logit' and not k in results_by_k:
        continue
    plt.subplot(1, 5, i + 1)
    if metric == 'AUROC':
        dfs = pd.concat([results_by_k['HOCOMOCO_AUROC'], results_by_k['CISBP_AUROC'], results_by_k['JASPAR_AUROC']])
    elif 'GENRE_AUROC' in metric:
        dfs = pd.concat([results_by_k['GENRE_AUROC']])
    elif 'GENRE_glm_logit' in metric:
        dfs = pd.concat([results_by_k['GENRE_glm_logit']])
    else:
        dfs = pd.concat([results_by_k['HOCOMOCO_' + metric]])

    hm = dfs.pivot('ensg' if not 'GENRE' in metric else 'motif.id', 'motif.database', 'score')
    symbols = dfs.pivot('ensg' if not 'GENRE' in metric else 'motif.id', 'motif.database', 'p.adj')

    hm.columns = [c + "_" + metric for c in hm.columns]
    symbols.columns = [c + "_" + metric + "_p.adj" for c in symbols.columns]
    if not 'GENRE' in metric:
        if all_results is None:
            all_results = hm
        else:
            all_results = pd.concat([all_results, hm], axis=1)
        all_results = pd.concat([all_results, symbols], axis=1)

    hm.columns = [c.replace("_" + metric, "") for c in hm.columns]
    symbols.columns = [c.replace("_" + metric + "_p.adj" , "") for c in symbols.columns]

    for c in symbols:
        symbols[c] = RFacade.get_pval_asterisks(symbols[c])
    hm['best'] = hm.max(axis=1)
    symbols['best'] = hm['best']
    hm.index = [symbol_by_ensg[k] if k in symbol_by_ensg else k for k in hm.index]
    hm = pd.concat([hm.sort_values('best', ascending=False).head(top_n),
                    hm.sort_values('best', ascending=False).tail(top_n)])
    symbols = pd.concat([symbols.sort_values('best', ascending=False).head(top_n),
                         symbols.sort_values('best', ascending=False).tail(top_n)])
    del hm['best']
    del symbols['best']
    vmin, vmax = minmax_by_metric[metric]
    print vmin, vmax
    sns.heatmap(hm, annot=symbols, fmt='', yticklabels=True, cbar_kws={'label': metric}, cmap='RdBu_r',
                annot_kws={'size': 5}, vmin=vmin, vmax=vmax)
    plt.title(metric)
    plt.yticks(fontsize=6)
fig.suptitle(label1 + "_vs_" + label2)
plt.tight_layout()
plt.subplots_adjust(top=.9)
savefig(join(output_dir, 'figures', 'heatmap'), png=False)
plt.close()

# append and save final results by gene name
all_results.insert(0, 'symbol', [symbol_by_ensg[k] if k in symbol_by_ensg else None for k in all_results.index])
DataFrameAnalyzer.to_tsv_gz(all_results, join(output_dir, 'results_by_eng.tsv.gz'), index=True)

