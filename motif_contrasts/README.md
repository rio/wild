# README - Motif Contrasts
## Motivation
1. More than one motif database is available to assess a comparison between any two sequence groups. These databases are redundant and sometimes individual members are missing between databases. Also, PWM information content can affect performance metrics.
2. Enrichment metrics to decide a motif enrichment/depletion between two sequence groups are based on models that rely on specific assumptions
    (i) set imbalances
    (ii) sequence length and GC control
    (iii) reasonable sizes and no confounding factors.

A way to observe several metrics together and compare them is desirable.

## Solution

The script `calc_motif_contrasts.py` implements a motif comparison between any two sequence groups (from `FASTA` or `BED` input files), against four motif databases: `JASPAR/CIS-BP/HOCOMOCO` and `GENRE` (8mer TF modules).

Default metrics for comparison are provided, and are easily exendable. Thus, users can define their own metrics and compare them. By default, AUROC, fold-change and odds-ratio are provided. As a toy example, a custom GLM R function is provided (GC corrected logistic regression).

## Running (spinoza)

To run please locate the running script and execute the following command

```
python calc_motif_constrasts.py --bed1 your_bed1 --bed2 your_bed2 --label1 A --label2 2 --outputdir some_output_directory --genome hg19
```
Alternatively, it can be run from `FASTA` paths
```
python calc_motif_constrasts.py --fasta1 your_path1 --fasta2 your_path2 --label1 A --label2 2 --outputdir some_output_directory --genome hg19
```

### Example

```
python calc_motif_constrasts.py --fasta1 input/bdnf_1h_gained.fa --fasta2 input/KCl_1h_gained.fa --label1 bdnf_1h --label2 KCl_1h --outputdir output --genome mm10
```

```
python calc_motif_constrasts.py --bed1 input/GSE40129.gata3.mcf7_peaks_hg38.bed --bed2 input/GSE40129.gata3.mcf7_peaks_hg19-dfltBG_hg38.bed --label1 gata3 --label2 genre --outputdir output --genome hg38
```

## Output

The output is a set of volcano plots for the defined metrics and a heatmap for the metrics that can be aggregated across databases for common genes. Please refer to the following link for Figure examples
https://tinyurl.com/y8v8jro2

In addition, a set of output tables with the individual and aggregated scores per gene (best across all redundant motifs in each database) are provided .

## Covariates and custom contrast functions

One useful feature of this script is that it allows the definition of custom constrasts with external data provided as input, to assess classification performance changes with little effort. Features such as sequence conservation/length, read counts, and GenomeTrack features can be added for each fasta header, and be included in new models (e.g. covariate, interaction terms, etc.).

As an example, let's consider the %GC content of our `FASTA` headers, given as an additional input parameter

`python calc_motif_constrasts.py --fasta1 input/Neuron.fa --fasta2 input/Neuron_bg.fa --label1 Neu --label2 NeuBG --outputdir output --genome hg19`**`--covariates input/covariates_example_hg19.tsv.gz`**

This dataframe will be received by default in all `MotifContrastFunction` calls, for each motif. New functions can be implemented here in order to handle, skip, or process the covariates. In addition, users can invoke `R` calls in external scripts, within Python, which just delegates the task to an external function implemented somewhere else. 

The only obligatory requirement is that the method implemented must return a `score` and a `p.value`, within a `pandas.DataFrame` format. The underlying assumption is that the function will receive motifs scores and labels for a specific motif.id.

```
class MotifContrastFunction:

    ########
    # ...
    # Default methods are here
    # ...
    ########
    
    # This is a new, user defined method
    @staticmethod
    def get_r_glm_logistic_pvalue(input, alternative='greater', **kwargs):
        '''
        Return a chisq and a pvalue of a model that assesses label ~ score + gc.content versus label ~ gc.content (binomial)
        :param input:
        :param alternative:
        :param kwargs:
        :return:
        '''
        covariates = kwargs.get('covariates', None)
        if covariates is None:
            print 'this function requires covariates to be defined.' \
                  'Please add them to the input, calculate them locally, or remove this function from the library'
            return Exception
        # add covariate to generic input dataframe before calling R
        covariates_dict = DataFrameAnalyzer.get_dict(covariates, 'header', 'gc.content')
        input['gc.content'] = input['header'].map(covariates_dict)
        
        r = rpy2.robjects.r
        if(not r['exists']('glm_logistic')[0]):
            print 'importing function'
            r.source('/g/scb/zaugg/rio/EclipseProjects/wild/lib/rscripts/motif_contrasts.R')
        
        # here we call a custom R function
        loglk, pval = r['glm_logistic'](pandas2ri.py2ri(input))
        return pd.DataFrame([['glm_logit', loglk, pval]],
                            columns=['score.type', 'score', 'p.val'])
```

This custom method invoked R, by calling a custom function
```
library('lmtest')
glm_logistic <- function(df){
    # print(head(df))
    lm <- glm(label ~ score + gc.content, family = 'binomial', data = df)
    lm_null <- glm(label ~ gc.content, family = 'binomial', data = df)
    res <- lrtest(lm, lm_null)
    chisq <- tail(res$`Chisq`, 1)
    pval <- tail(res$`Pr(>Chisq)`, 1)
    return(c(chisq, pval))
}
```

Finally, this function needs to be defined in the `MotifQueries` class in each genome/database that is allowed to use it (in this version, it is added to GENRE).

## Misc
### Installation
- `wild` repository must be installed. See https://git.embl.de/rio/wild
- Datasets and software are all part of the default `zaugg_shared` and `spinoza` frameworks. If you encounter bugs, please let me know.

### Running time
Less than 10 minutes (1000 vs 1000 sequences in one CPU). `GENRE` motif mapping is the slowest step. Second and further executions handle backup files and improve running time for new contrasts.

### Other genomes
To run in other genomes (e.g. `dm6`), you can execute the script `FASTA` files with `hg19` or `mm10` as genomes, and consider mainly the `GENRE` and `JASPAR` provided outputs (mammals and vertebrates). From the `JASPAR` or `EntrezGene` IDs, mapping needs to be done to the organism specific-genes.

`mm10`, `hg19` and `hg38` are defined as default genomes for Bed file mapping (if input are bed files). To add more genomes, please request or add respective paths to the `FastaAnalyzer` class.

## K-mer module coenrichments
In the case of `GENRE` and `narrowPeak` sequences (e.g. TF ChIP-seq or ATAC-seq), it is desirable to explore tethering or combinatorial binding. For this, the extent of co-enrichment is asessed using a script that implements an enrichment test for binding sites for TF1 and TF2 using a Zero-or-One Motif Occurence model (ZOOPS, see Mariani et al 2017 for details).

To run, and assuming that you calculated motif scores from the previous script, you can calculate co-enrichment values for samples using the following script, using as input the `FASTA` file and the `GENRE` module scores that were generated in the `calc_motif_contrasts` script.
```
python calc_coenrichments_genre.py --hitsgenre output/gata3_vs_genre/motif_scores1_GENRE.tsv.gz --fasta output/gata3_vs_genre/GSE40129.gata3.mcf7_peaks_hg38.fa --overwrite
```
```
python calc_coenrichments_genre.py --hitsgenre output/gata3_vs_genre/motif_scores2_GENRE.tsv.gz --fasta output/gata3_vs_genre/GSE40129.gata3.mcf7_peaks_hg19-dfltBG_hg38.fa --overwrite
```
Each script will generate a set of odds ratio calculations for each sequences set. To plot them all together as a BubbleHeatmap, for visualization, the script `plot_coenrichments_genre` can be used by running
```
python plot_coenrichments_genre.py --coenrichments_list output/gata3_vs_genre/motif_scores1_GENRE_coenrichments.tsv.gz,output/gata3_vs_genre/motif_scores2_GENRE_coenrichments.tsv.gz --outputbasename output/gata3_vs_genre/bubblemap --labels A,B --nsigmax 4 --width 9 --height 10 --padj_thr 5e-5
```
Several flags in this script control the amount of samples to show in the heatmap, the coloring parameters, figure sizes, etc. Please see `--help` for more information

### New contrasts/requests

If you use this script, please let me know if you would like to try additional input parameters or contrasts, or if you encounter bugs.
