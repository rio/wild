## Main Description 

This repo includes Python wrappers and functions for Data Science from Python, with partial calls to R. ```https://git.embl.de/rio/wild```

### Installation (typical install time = 5-10 minutes)
1) Download the repository to your favorite local directory
```
git clone git@git.embl.de:rio/wild.git
```
2) Update your local PYTHONPATH variable, by adding the path where `wild` was downloaded e.g. by adding the following line in `~/.bashrc`.
```
export PYTHONPATH=/my/path/to/wild:$PYTHONPATH
```
Alternatively, you can use `pip` to add the package to your environment, using the command
```
pip install -e /my/path/to/wild --upgrade
```

### Dependencies
`wild` requires Python (2.x) as well as the following packages:
- `pandas`
- `numpy`
- `matplotlib`
- `seaborn`
- `skicit-learn`
- `mygene`. To install, run `conda install -c bioconda mygene`

Other methods use specific libraries. If you see a missing dependency you need to clarify please let me know. e.g.
- `PDFParser`uses `PyPDF2`
- `GOAnalyzer` call `clusterProfiler`, `topGO` and `StringDB`. According to the call, one or several should be installed

###  Usage
The simplest way to import this library into your scripts is to declare the relevant class in the beginning of your scripts. E.g.

```
# read dataframe
from lib.DataFrameAnalyzer import DataFrameAnalyzer
df = DataFrameAnalyzer.read_tsv_gz('/path/to/table.tsv.gz')

# read fasta file
from lib.FastaAnalyzer import FastaAnalyzer
fastas = FastaAnalyzer.get_fastas_from_file('/path/to/multifasta.fa')
```


Some useful and general features
- `BSubFacade` Prepare submission jobs for the slurm/bsub clusters
    - demo at `cluster_submit_test/demo.py`
- `DataFrameAnalyzer` Manipulate data frames. Single and multiple tables can be loaded as directories or individual files
    - e.g. See methods `DataFrameAnalyzer.read_tsv_gz` or `DataFrameAnalyzer.read_multiple_tsv_gz`
- `PDFParser` PDF concatenator, to merge many figures into a single file
- `ThreadingUtils` to run methods in multicore.
- Calculate qvals from pvals (`RFacade.get_qvals`)
- Other simple R statistical tests (`RFacade.get_wilcox_pval`)
- Fit simple `MachineLearning` models (`MachineLearning`)
- Call HOMER, DiMO, DNAShapeR, TACO, etc. (`HOMERAnalyzer`, `DiMOAnalyzer`, `ShapeAnalyzer`)

### Genomics
- `MyGeneAnalyzer` get IDs between databases/species/names
    - `get_ensemblgene_by_ensemblprotein`
    - `get_name_by_ensembl`
    - `get_uniprot_by_ensembl`
- `SequenceMethods` manipulate sequence data
    - `parse_range2coordinate`
    - `parse_coordinate2range`
    - `get_matches` using IUPAC code (e.g. W = A or T, etc.)
- `DESeq2Analyzer` prepare generic comparisons for `DESeq2`
    - it uses a template script that is modified live, according to the input dataframe, contrast types, etc.
- `FastaAnalyzer`, `BedAnalyzer`, manipulate fasta/bed files
    - `convert_bed_to_fasta` Dataframe to sequences
- `PeaksAnalyzer` Intersect peak sets using bedtools
- `HumanTFs` database from Lambert et al 2018
- `REMAPAnalyzer` get peak coordinates/sequences from REMAP
- `LitfOver` convert coordinates between genomes
- `GOAnalyzer` get GO terms for specific ontology databases (topGO, clusterProfiler and String).
    - `rGREAT` can be run as an independent method (see `GOAnalyzer.run_GREAT`)
- `GENRE` prepares background sequence or runs GENRE (Mariani et al 2017)
- `BiasAwayAnalyzer` Generate sequence backgrounds with BiasAway
- Run sequence or PWM scans with PWMs or PBM k-mer data
    - `MotifAnalyzer`, `SELEX.SELEXAnalyzer`
- `MotifConverter` Motif format conversion methods
    - e.g `MotifConverter.convert_jaspar_pfm_to_homer_ppm`

### Plotting
- `RFacade.plot_scatter_ggrepel` ggrepel from python
- `Plots.CircleHeatmap` to make dotplots see function `make_bubble_heatmap` to
- `lib.motif_plotter`. Originally developed by Constantin Ahlmann-Eltze (see https://git.embl.de/ahlmanne/motif_plottter)

Please, let me know if these methods are useful for you, or you encounter errors when running them (e-mail: ignacio.ibarra@embl.de). Some of them might require updates in to local files (e.g. genomes, R binaries)